<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

//pre defined configs...
/*api configs*/
/*define('SUNCASH_API_URL', 'https://prod.mysuncash.com/api/merchant.php');
define('SUNCASH_API_CHP_URL', 'https://prod.mysuncash.com/api/chp.php');
define('SUNCASH_BUSINESS_LOGIN_API_URL', 'https://prod.mysuncash.com/api/business.php?');
define('SUNCASH_CHECKOUT', 'https://prod.mysuncash.com/api/checkout.php?');*/

//this is dev
define('SUNCASH_API_URL', 'https://dev.mysuncash.com/api/merchant.php');
define('SUNCASH_API_CHP_URL', 'https://dev.mysuncash.com/api/chp.php');
define('SUNCASH_BUSINESS_LOGIN_API_URL', 'https://dev.mysuncash.com/api/business.php?');
define('SUNCASH_CHECKOUT', 'https://dev.mysuncash.com/api/checkout.php?');//



define('SUNCASH_CHARITY_LOGIN_API_URL', 'https://dev.mysuncash.com/api/charity.php?');
define('SUNCASH_CHARITY_API_URL', 'https://dev.mysuncash.com/services/index.php/business_bp/');
define('SUNCASH_CUSTOMER_API_URL', 'https://dev.mysuncash.com/api/chp.php');

define('SUNCASH_CUSTOMER_PORTAL_URL', 'https://localhost/customer/signup/');//change per env

define('ENV', 'DEV');
//cenpos prod
// define('CENPOS_USERNAME', 'integration');
// define('CENPOS_PASSWORD', 'BO#U5#zEb1');
// define('CENPOS_MERCHANT_ID', '400002003');
// define('CENPOST_MERCHANT_SECRETKEY','cd95f803212a5c740149a71433147f8d');
// define('CENPOST_MERCHANT_ID','qjWSfyL/yIdBrJugCfhkH7iTPCizz/uIXU//0mk89Ck=');

//cenpos dev
define('CENPOS_USERNAME', 'Integration');
define('CENPOS_PASSWORD', 'fR$5h8KA1');
define('CENPOS_MERCHANT_ID', '400001929');

define('CENPOST_MERCHANT_SECRETKEY','a0c70a0d5aa451bfc02f17e9199e41e6');
define('CENPOST_MERCHANT_ID','8115iItI79q9AKXGLA0deTagqF05ZbVrkblBJotMeg0=');

// 8115iItI79q9AKXGLA0deWiDl8x+tPd3vq5uLvcPraI=
define('CENPOS_SITEVERIFY_URL','https://www.cenpos.net/simplewebpay/cards/?app=genericcontroller&action=siteVerify');
define('CENPOS_PROCESS_URL','https://www.cenpos.net/simplewebpay/cards/api/UseToken');
define('CENPOS_GETTOKKEN_URL','https://www.cenpos.net/simplewebpay/cards/api/GetToken');
define('CENPOS_CONVERTCRYPTO_URL','https://www.cenpos.net/simplewebpay/cards/api/ConvertCrypto');

//service account for api login bypass.
define('SERVICE_ACCOUNT_USERNAME_DEV', 'test105');//for business
define('SERVICE_ACCOUNT_PASSWORD_DEV', '123456');//for business

define('SERVICE_ACCOUNT_USERNAME_PROD', 'B123');//for business
define('SERVICE_ACCOUNT_PASSWORD_PROD', 'b123456A');//for business

//service account for api customer bypass.
define('SERVICE_ACCOUNT_CUSTOMER_USERNAME_DEV', '14243346702');//for customer
define('SERVICE_ACCOUNT_CUSTOMER_PASSWORD_DEV', '1515');//for customer

define('SERVICE_ACCOUNT_CUSTOMER_USERNAME_PROD', '16808000608');//for customer
define('SERVICE_ACCOUNT_CUSTOMER_PASSWORD_PROD', '1515');//for customer

//charity
define('SERVICE_ACCOUNT_CHARITY_USERNAME_DEV', 'testcharity');//for CHARITY
define('SERVICE_ACCOUNT_CHARITY_PASSWORD_DEV', '1234567');//for CHARITY

define('SERVICE_ACCOUNT_CHARITY_USERNAME_PROD', 'zcharity');//for CHARITY
define('SERVICE_ACCOUNT_CHARITY_PASSWORD_PROD', '123456');//for CHARITY

//cenpos dev
define('MERCHANTKEY_DEV', 'a15d411bdbdfffa46c50cb57958bd68fef35d5eacbfcaba0dc21a8caf03ac995');
//define for allowed merchant to have sunpass fee..
const SUNPASS_MERCHANTS_KEYS = ['902dec6e0b8135f1b3fdc4a6a67379ffd78ed100a1ed38458cdb7c1f8d94d806', 'eb510843beadfe4dbad2dd6d37979460634c8c9c9002a33595b4964670d1497b'];

define('PAYPAL_CLIENTID', 'https://www.paypal.com/sdk/js?client-id=Ab8cEbhg0S_S53Ejk5LgoTiBUn8lFy1jIYpgtEq1aqeu78cYqKfY9ED8218LuILpzjZSb0fX7lCdkwBZ&disable-funding=credit,card');

define('AMAZON_IS_SANDBOX', 'true');

