<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
/*$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;*/

//custom route.
/*use Luthier\Core\Route;

Route::home('welcome@index'); // Default controller

Route::get('admin', ['uses' => 'admin@index',  'namespace' => 'admin']);
Route::post('login', ['uses' => 'admin@login',  'namespace' => 'admin']);

//end of custom route
$route = Route::register(); */
$route['default_controller'] = 'suncashmecontroller';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['(:any)']= "suncashmecontroller/index/$1";
$route['(:any)/([0-9][0-9.]*[0-9])']= "suncashmecontroller/index/$1/$2";

$route['referral/(:any)']= "suncashmecontroller/referral/$1";
$route['customers/referral_process']= "suncashmecontroller/referral_process";


$route['info/faq']= "suncashmecontroller/faq";
$route['info/terms']= "suncashmecontroller/terms";
$route['info/contacts']= "suncashmecontroller/contacts";
//signup 

$route['wallet/login']= "login/login/index";
$route['wallet/login-charity']= "login/login/index_charity";
$route['wallet/logout'] = "customer/logout";
$route['wallet/dashboard'] = "customer/dashboard";


$route['card/main']= "payment/card_main";
$route['card/mainpage']= "payment/card_mainpage";
$route['voucher/main']= "payment/voucher_main";
$route['sanddollar/main']= "payment/sanddollar_main";
$route['sanddollarqr/main']= "payment/sanddollarqr_main";


$route['wallet/logout'] = "customer/logout";

$route['payment/get_promo_discount'] = "payment/get_promo_discount";
$route['view/scan_qr']= "suncashmecontroller/scan_qr_form";

$route['card/revalidation/(:any)/(:any)'] = "payment/revalidate_whitelist/$1/$2";
$route['checkout/verify_form/(:any)'] = "checkout_card/verify_form/$1";

//for cc
$route['payment/cc_login_page']= "login/login/customer_login";
$route['payment/cc_customerloginprocess']= "login/login/cc_login_process";
$route['payment/cc_payment']= "payment/login/cc_login_process";
$route['payment/logout'] = "customer/logout";
$route['payment/cc_payment_page']= "suncashme_card/cc_payment_page";
$route['payment/process_verify_card']= "suncashme_card/process_verify_card";
$route['payment/delete_linked_card']= "suncashme_card/delete_linked_card";
$route['payment/process_link_card']= "suncashme_card/process_link_card";
$route['payment/process_cc_wallet']= "suncashme_card/process_cc_wallet";
$route['payment/process_verify_upload_requirements']= "suncashme_card/process_verify_upload_requirements";
$route['wallet/logout_cc'] = "customer/logout_cc";





$route['checkout/link_card/(:any)']= "checkout_card/link_card_form/$1";
$route['checkout/link_card_process']= "checkout_card/link_card_process";
$route['checkout/verify_card'] = "checkout_card/verify_card_form";
$route['checkout/verify_card_process']= "checkout_card/verify_card_process";

//check for merchants.
$route['checkout/link_card/(:any)']= "checkout_card/link_card_form/$1";

// $routes['checkoutv2/(:any)'] = "merchant_checkout/checkout/$1";

$routes['testcheckout/test'] = "merchant_checkout/testurl";



$route['sanddollar/login']= "login/login/index_sanddollar";
$route['sanddollar/logout'] = "customer/logout";

//amazon
$routes['checkoutv2/(:any)'] = "merchant_checkout/checkout/$1";
$route['amazon/main']= "payment/amazon_main";
$route['paypal/main']= "payment/paypal_main";
$route['voucher/main']= "payment/voucher_main";


$route['payment/sd/status_check/(:any)']= "payment/check_status/$1";

$route['payment/sd/check_status_cfid/(:any)']= "payment/check_status_cfid/$1";
// $route['payment/sd/check_status_cfid/(:any)/(:any)']= "payment/check_status_cfid/$1/$2";

$route['checkout/payment_details/(:any)']= "checkout/payment_details/$1";

$route['payment/saveBusinessSandDollarContact']= "payment/saveBusinessSandDollarContact";
