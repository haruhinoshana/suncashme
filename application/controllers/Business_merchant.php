<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends CI_Controller {// put filename of controller on class name 

	function __construct(){//where can put dynamic functions and models....
        parent::__construct();
   		$this->session->flashdata('msg');
   		//load model.
      	$this->load->model('core/db');//for transactions and other eloquent db features
      	$this->load->model('core/Qr');
   		//$this->load->model('user');
   		//$this->load->model('Value_sets_global','global');
   		$this->assetHelper = new AssetHelper\AssetHelper();
    }

	public function checkout($merchant_key,$reference_id){
		//dd($reference_id);
		//dd($_SESSION['sunpass_order']);
		//dd($root_path);
		if(!empty($merchant_key) && !empty($reference_id)){
			//if empty fields
			$arr=[
				'method'=>'get_client_checkout_details',
				'P01'=>$merchant_key,
				'P02'=>$reference_id,
				'return_mode'=>'json'
			];
		    $api_fee_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
		    //dd($api_fee_result);
		    $api_fee_data=json_decode($api_fee_result,true);
		    //dd($api_fee_data);		
		    if($api_fee_data['Success']=='YES'){
		   	//check if pending...
		   	if($api_fee_data['ResponseMessage']['status']!='PENDING'){
		   		redirect('');
		   	}
		    //process order as individual array
		    $ItemName=[];
		    $ItemQty=[];
		    $ItemPrice=[];
		    if(!empty($api_fee_data['ResponseMessage']['details'])){
		    	for ($i=0; $i <count($api_fee_data['ResponseMessage']['details']) ; $i++) { 
		    		# code...
				    $ItemName[$i]=$api_fee_data['ResponseMessage']['details'][$i]['item_name'];
				    $ItemQty[$i]=$api_fee_data['ResponseMessage']['details'][$i]['qty'];
				    $ItemPrice[$i]=$api_fee_data['ResponseMessage']['details'][$i]['price'];		    		
		    	}
		    }
			$return_data=[
			'merchant_client_id'=>$api_fee_data['ResponseMessage']['merchant_client_id'],
			'MerchantKey'=>$api_fee_data['ResponseMessage']['merchant_key'],
			'MerchantName'=>$api_fee_data['ResponseMessage']['merchant_name'],
			'Amount'=>$api_fee_data['ResponseMessage']['amount'],
			'total_amount'=>$api_fee_data['ResponseMessage']['total_amount'],
			'OrderID'=>$api_fee_data['ResponseMessage']['order_id'],
			'CallbackURL'=>$api_fee_data['ResponseMessage']['callback_url'],
			'ItemName'=>$ItemName,
			'ItemQty'=>$ItemQty,
			'ItemPrice'=>$ItemPrice,
			'OrderReference'=>$reference_id,
			'profile_pic'=>$api_fee_data['ResponseMessage']['profile_pic'],
			'reference_id'=>$reference_id,
			'sunpass_fee'=>$api_fee_data['ResponseMessage']['sunpass_fee'],
			'facility_fee'=>$api_fee_data['ResponseMessage']['facility_fee'],
			//'auth',				
			];
			$data['reference_id']=$reference_id;

			//dd($return_data);

			$data['post_data'] = $return_data;

		    $arr=[
		    	'method'=>'login_business',
	            'P01'=> SERVICE_ACCOUNT_USERNAME_DEV,
	            'P02'=> SERVICE_ACCOUNT_PASSWORD_DEV,
		    ];
		    //$arr_tosend['url']="login_business?".http_build_query($arr);

		    //dd($arr_tosend);
		    $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
		    $api_bt_data=json_decode($api_bt_result,true);
		    //dd($api_bt_data);
		    $data['login_data']='';
		    if($api_bt_data['ResponseCode']=='YES'){
		    $data['login_data']=$api_bt_data['ResponseMessage'];
		    }


			//get payment settings. 
		       $arr=[
		         'method'=>'get_checkout_methods_byid',
		            'P01'=> $data['login_data']['SessionID'],
		            'P02'=> $api_fee_data['ResponseMessage']['merchant_client_id'],
		       ];
		       // $arr_tosend['url']="login_business?".http_build_query($arr);

		       // dd($arr_tosend);
		       //dd($arr);
		       $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
		       $api_bt_data=json_decode($api_bt_result,true);
		       //dd($api_bt_result);
		       $data['selected_settings_data']='';
		       if($api_bt_data['Success']=='YES'){
		       $api_bt_data['ResponseMessage'][0] =isset($api_bt_data['ResponseMessage'][0]) ? $api_bt_data['ResponseMessage'][0] : '';
		       $data['selected_settings_data']=$api_bt_data['ResponseMessage'][0];                     
		       } 
		    $data['sunpass_fee']=0.00;
		    $data['facility_fee']=0.00;
			//get sunpass fee$$$$.
			if(in_array($merchant_key,SUNPASS_MERCHANTS_KEYS)){
			$data['sunpass_fee'] = $api_fee_data['ResponseMessage']['sunpass_fee']>0 ? number_format($api_fee_data['ResponseMessage']['sunpass_fee'],2) : 0.00;
			$data['facility_fee'] =$api_fee_data['ResponseMessage']['facility_fee']>0 ?  number_format($api_fee_data['ResponseMessage']['facility_fee'],2): 0.00;
			}

			//session return for safari....
			$data['post_data']['pf']=0.00;
			$data['post_data']['tf']=0.00;
			$data['post_data']['fee']=0.00;
			$data['post_data']['sunpass_fee']=$data['sunpass_fee'];
			$data['post_data']['facility_fee']=$data['facility_fee'];
			$data['post_data']['subtotal']=$return_data['Amount'];
			$data['post_data']['total_amount']=$return_data['Amount']+$data['sunpass_fee']+$data['facility_fee'];
			$data['post_data']['promo_code']='';
			$data['post_data']['discount']=0.00;
			//$data['post_data']['subtotal'] = $_SESSION['post_data']['subtotal'];

			$this->session->set_userdata('post_data',$return_data);
	  		$_SESSION['post_data']['pf'] = 0.00;
	  		$_SESSION['post_data']['tf'] = 0.00;
	  		$_SESSION['post_data']['fee'] = 0.00;
	  		$_SESSION['post_data']['sunpass_fee'] = $data['sunpass_fee'];
	  		$_SESSION['post_data']['facility_fee'] = $data['facility_fee'];
	  		$_SESSION['post_data']['subtotal'] = $_SESSION['post_data']['Amount'];
	  		$_SESSION['post_data']['total_amount']=$return_data['Amount']+$data['sunpass_fee']+$data['facility_fee'];
	  		$_SESSION['post_data']['promo_code'] = '';
	  		$_SESSION['post_data']['discount'] = 0.00;
	  		$data['subtotal'] = $_SESSION['post_data']['subtotal'];
			//site verify
			$arr=[
				'merchant'=>CENPOST_MERCHANT_ID,
				'secretkey'=>CENPOST_MERCHANT_SECRETKEY,
				'email'=>'suncashme@gmail.com',
				'ip'=>'127.0.0.1',
			];
			$verify_post=$this->assetHelper->api_requestv2(CENPOS_SITEVERIFY_URL,$arr);
			$verify_post=json_decode($verify_post,true);	
			//dd($verify_post);
			$data['verify_params']=$verify_post;
			$this->blade->view('checkout',$data);
		    } else {
			redirect('');	    	
		    }	


		} else {
			redirect('');
		}
		
    }
    public function checkoutv2(){
        
    }
}//end of controller 