<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checkout extends CI_Controller {// put filename of controller on class name 

	function __construct(){//where can put dynamic functions and models....
        parent::__construct();
   		$this->session->flashdata('msg');
   		//load model.
      	$this->load->model('core/db');//for transactions and other eloquent db features
      	$this->load->model('core/qr');
   		//$this->load->model('user');
   		//$this->load->model('Value_sets_global','global');
   		$this->assetHelper = new AssetHelper\AssetHelper();
   		
	}
	public function payment_details($params){//method then routefind
		$params_decoded = base64_decode($params);

		//dd($params_decoded );
		list($payment_reference_id,$reference_id,$status,$paymentmethod)=explode("||",$params_decoded);
		$data['payment_reference_id']=$payment_reference_id;
		$data['reference_id']=$reference_id;
		$data['status']=$status;
		$data['paymentmethod']=$paymentmethod;

		if($paymentmethod =='card'){
			$paymentmethod_val ='Credit/Debit Card Payment';
		}else if($paymentmethod =='wallet'){
			$paymentmethod_val ='SunCash Wallet Account';	
		}else if($paymentmethod =='sanddollar'){
			$paymentmethod_val ='SandDollar Payment';		
		}else if($paymentmethod =='paypal'){
			$paymentmethod_val ='Paypal';	
		}else if($paymentmethod =='amazomn'){
			$paymentmethod_val ='Amazon Checkout';
		}else if($paymentmethod =='voucher'){
			$paymentmethod_val ='SunCash Voucher';
		}else{
			return 'Invalid Payment Method';
		}

		if($status =='success'){
			$status ='COMPLETED';
		}else{
			$status ='FAILED';
		}
		
		$arr=[
			'method'=>'get_checkout_details_by_refid',
			//'P01'=>$merchant_key,
			'P01'=>$reference_id,
			'P02'=>$payment_reference_id,
			'P03'=>$paymentmethod,
			'return_mode'=>'json'
		];
				// dd($arr);
		$api_fee_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
		// dd($data);
		$api_fee_data=json_decode($api_fee_result,true);
		// dd($api_fee_data);		
		if($api_fee_data['Success']=='YES'){
			//check if pending...
			// if($api_fee_data['ResponseMessage']['status']!='PROCESSED'){
			// 	redirect('');
			// }
			//process order as individual array
			$ItemName=[];
			$ItemQty=[];
			$ItemPrice=[];
				if(!empty($api_fee_data['ResponseMessage']['details'])){
					for ($i=0; $i <count($api_fee_data['ResponseMessage']['details']) ; $i++) { 
						# code...
						$ItemName[$i]=$api_fee_data['ResponseMessage']['details'][$i]['item_name'];
						$ItemQty[$i]=$api_fee_data['ResponseMessage']['details'][$i]['qty'];
						$ItemPrice[$i]=$api_fee_data['ResponseMessage']['details'][$i]['price'];		    		
					}
				}
				$return_data=[
				'merchant_client_id'=>$api_fee_data['ResponseMessage']['merchant_id'],
				// 'MerchantKey'=>$api_fee_data['ResponseMessage']['merchant_key'],
				// 'MerchantName'=>$api_fee_data['ResponseMessage']['merchant_name'],
				'Amount'=>$api_fee_data['ResponseMessage']['amount'],
				'Total'=>$api_fee_data['ResponseMessage']['total_amount'],
				'OrderID'=>$api_fee_data['ResponseMessage']['order_id'],
				'CallbackURL'=>$api_fee_data['ResponseMessage']['callback_url'],
				'ItemName'=>$ItemName,
				'ItemQty'=>$ItemQty,
				'ItemPrice'=>$ItemPrice,
				'OrderReference'=>$reference_id,
				'reference_id'=>$reference_id,
				'payment_method'=>$paymentmethod_val,
				'customer_name'=>$api_fee_data['ResponseMessage']['customer_name'],
				'customer_mobile'=>$api_fee_data['ResponseMessage']['customer_mobile'],
				'customer_email'=>$api_fee_data['ResponseMessage']['customer_email'],
				// 'source'=>$api_fee_data['ResponseMessage']['source'],
				'created_date'=>$api_fee_data['ResponseMessage']['created_date'],	
				'status' =>	$status	
				];
				$data['reference_id']=$reference_id;

				//dd($return_data);

				$data['post_data'] = $return_data;

				//get POS status
				$arr=[
					'method'=>'pos_get_transactions',
					'P01'=>$reference_id,
					'P02'=>$api_fee_data['ResponseMessage']['merchant_id'],
					'return_mode'=>'json'
				];
					// dd($arr);
				$api_fee_result=$this->assetHelper->api_requestv2(SUNCASH_POS_URL,$arr);
				// dd($api_fee_result);
				$api_fee_data=json_decode($api_fee_result,true);
				//dd($api_fee_data);		
				if($api_fee_data['ResponseCode']=='00000'){
					$trans_stats =$api_fee_data['ResponseMessage']['Transaction Status'];
							// dd($trans_stats);
					if($trans_stats=='PENDING'){

						//update POS logs
						$arr=[
							'method'=>'pos_update_payment_status',
							'R1'=>$reference_id,
							'return_mode'=>'json'
						];
						$api_fee_result=$this->assetHelper->api_requestv2(SUNCASH_POS_URL,$arr);
						// dd($api_fee_result);
						$api_fee_data=json_decode($api_fee_result,true);
						// dd($api_fee_data);		
						if($api_fee_data['ResponseCode']=='00000'){

							$this->blade->view('checkout_result_page',$data);
						}else{
							$arr=[
								'msg'=>'Somethng went wrong1!',
								'success'=>false
							];
						}
					}
					$this->blade->view('checkout_result_page',$data);
				}else{
					$arr=[
						'msg'=>'No POS Logs found!',
						'success'=>false
					];

					echo json_encode($arr);
					exit;					
				}
		}else{
			$arr=[
				'msg'=>'NO record found!',
				'success'=>false
			];
		}

		
	}
}