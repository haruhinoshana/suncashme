<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checkout_card extends CI_Controller {// put filename of controller on class name 

	function __construct(){//where can put dynamic functions and models....
        parent::__construct();
   		$this->session->flashdata('msg');
   		//load model.
      	$this->load->model('core/db');//for transactions and other eloquent db features
      	$this->load->model('core/qr');
		$this->assetHelper = new AssetHelper\AssetHelper();
	}
	public function link_card_form($params){//method then routefind //get fee
		$arr = base64_decode($params);
		$arr = explode('||',$arr);
		//dd($arr);
		//dd(count($arr));
		if(count($arr)<2){
			$arr=[  
				'msg'=>'Invalid key.',
				'success'=>false
			];
			  
			echo json_encode($arr);
			exit;  
		}
		list($merchant_key,$merchant_customer_id)=$arr;
		//site verify
		$arr=[
			'merchant'=>CENPOST_MERCHANT_ID,
			'secretkey'=>CENPOST_MERCHANT_SECRETKEY,
			'email'=>'suncashme@gmail.com',
			'ip'=>'127.0.0.1',
		];
		$verify_post=$this->assetHelper->api_requestv2(CENPOS_SITEVERIFY_URL,$arr);
		$verify_post=json_decode($verify_post,true);	
		//dd($verify_post);
		$data['verify_params']=$verify_post;
		$data['merchant_key']=$merchant_key;
		$data['merchant_customer_id']=$merchant_customer_id;
		//$data['rn_amount'] = $this->get_random_amount(0.00,2.00);
		$this->blade->view('card-link-page',$data);

	}
	public function link_card_process(){

		$arr=[
		'method'=>'get_suntag_shortcode',
		'P01'=> $_POST['merchant_key'],//$this->session->userdata('SessionID'),
		'return_mode' => 'json'
		];
	    $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
		$api_bt_data=json_decode($api_bt_result,true);	
		// dd($api_bt_result);
	    $data['suntag_data']='';
	    if($api_bt_data['Success']=='YES'){
		$data['suntag_data']=$api_bt_data['ResponseMessage'];
		}else{
			$arr=[	
				'msg'=>'Something went wrong!',
				'success'=>false
			];
			echo json_encode($arr);
			exit;
		}    
		$invoice = $data['suntag_data']['suntag_shortcode'].''.strtotime(date('YmdHis'));



		$card_id_url='';
		if(!empty($_POST['card_id_upload'])){
			//die($key);	
			//ci upload.
			$filename="".date("Y")."_".time()."_".rand().".png";
			$decodedData = base64_decode($_POST['card_id_upload']);
			// APPPATH will give you application folder path

			if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
				$root_path = dirname(dirname(dirname(__FILE__)));
				$tosend=$root_path.'\assets\img\card_to_verify\ids\\'.$filename;
			} else {
				$root_path = dirname(dirname(dirname(dirname(__FILE__))));
				$tosend=$root_path.'/html/assets/img/card_to_verify/ids/'.$filename;
			}

			$is_ok1=file_put_contents($tosend,$decodedData);
			chmod($tosend, 0777);
			if (!$is_ok1) {
				//$this->upload->display_errors();
				$arr=[	
					'msg'=>'Upload failed2',
					'success'=>false
				];
				echo json_encode($arr);
				exit;
			}

			$card_id_url= base_url()."assets/img/card_to_verify/ids/".$filename;
		}

		$credit_card_url = '';
		if(!empty($_POST['credit_card_upload'])){
			//die($key);
			//ci upload.
			//$config = array();

			$filename="".date("Y")."_".time()."_".rand().".png";
			$decodedData = base64_decode($_POST['credit_card_upload']);

			if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
				$root_path = dirname(dirname(dirname(__FILE__)));
				$tosend=$root_path.'\assets\img\card_to_verify\cc\\'.$filename;
			} else {
				$root_path = dirname(dirname(dirname(dirname(__FILE__))));
				$tosend=$root_path . '/html/assets/img/card_to_verify/cc/'.$filename;
			}

			// APPPATH will give you application folder path
			$is_ok2=file_put_contents($tosend,$decodedData);
			chmod($tosend, 0777);
			if (!$is_ok2) {
				//$this->upload->display_errors();
				$arr=[	
					'msg'=>'Upload failed2',
					'success'=>false
				];
				
				echo json_encode($arr);
				exit;
			}
			$credit_card_url=base_url()."assets/img/card_to_verify/cc/".$filename;
		}
		$ccwithid_card_url='';
		if(!empty($_POST['cc_with_card_upload'])){


			$filename="".date("Y")."_".time()."_".rand().".png";
			$decodedData = base64_decode($_POST['cc_with_card_upload']);

			if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
				$root_path = dirname(dirname(dirname(__FILE__)));
				$tosend=$root_path.'\assets\img\card_to_verify\ccwithid\\'.$filename;
			} else {
				$root_path = dirname(dirname(dirname(dirname(__FILE__))));
				$tosend=$root_path . '/html/assets/img/card_to_verify/ccwithid/'.$filename;
			}
			// APPPATH will give you application folder path
			$is_ok3=file_put_contents($tosend,$decodedData);
			chmod($tosend, 0777);
			if (!$is_ok3) {
				//$this->upload->display_errors();
				$arr=[	
					'msg'=>'Upload failed3',
					'success'=>false
				];
				
				echo json_encode($arr);
				exit;
			}

			$ccwithid_card_url=base_url()."assets/img/card_to_verify/ccwithid/".$filename;
		}

		//----------------- convert crypto 0 payment----------------------
		$arr=[
				'amount'=>0.00,
				'type'=>'Sale',
				'invoicenumber'=>$invoice,
				'email'=>'suncashme@gmail.com',
				'secretkey'=>CENPOST_MERCHANT_SECRETKEY,
				'merchant'=>CENPOST_MERCHANT_ID,
				'tokenid'=>$_POST['tokenid'],
				// 'isrecaptcha'=>false,
		];
		$verifying_postp='';
		$vp='';
		$verifying_postp=$this->assetHelper->api_requestv2(CENPOS_SITEVERIFY_URL,$arr);
		//dd($verify_post2);
		$verifying_postp=json_decode($verifying_postp,true);
		$vp=$verifying_postp['Data'];

		$arr_post=[
			'verifyingpost'=>$vp,
			'tokenid'=>$_POST['tokenid'],
		];
		$process_payment1=$this->assetHelper->api_requestv2(CENPOS_PROCESS_URL,$arr_post);
		//----------------- convert crypto 0 payment----------------------
		//----------------- convert crypto process----------------------
		$arr=[
		'secretkey'=>CENPOST_MERCHANT_SECRETKEY,
		'merchant'=>CENPOST_MERCHANT_ID,
		'customercode'=>$_POST['merchant_customer_id'],
		'tokenid'=>$_POST['tokenid'],
		];
		//dd($arr);
		$verifying_post2='';
		$verify_post2=$this->assetHelper->api_requestv2(CENPOS_SITEVERIFY_URL,$arr);
		$verify_post2=json_decode($verify_post2,true); 
		if($verify_post2['Result']==0){
		
			//dd($verify_post2);
			$arr_post2=[
			'verifyingpost'=>$verify_post2['Data'],
			'tokenid'=>$_POST['tokenid'],
			];
			// dd($arr_post2);
			$process_link_card12=$this->assetHelper->api_requestv2(CENPOS_CONVERTCRYPTO_URL,$arr_post2);
			$process_link_card=json_decode($process_link_card12,true);
			if($process_link_card['Result']==0){
			// -----------------end convert crypto process----------------------

				//----------------- convert crypto random amount payment----------------------
				$rn_amount = $this->get_random_amount(0.01,1.99);
				$invoice = $data['suntag_data']['suntag_shortcode'].''.strtotime(date('YmdHis'));
				$arr2=[
						'amount'=>$rn_amount,
						'type'=>'Sale',
						'invoicenumber'=>$invoice,
						'email'=>'suncashme@gmail.com',
						'secretkey'=>CENPOST_MERCHANT_SECRETKEY,
						'merchant'=>CENPOST_MERCHANT_ID,
						'tokenid'=>$process_link_card['TokenId'],
						// 'isrecaptcha'=>false,
				];
				$verifying_post_payment='';
				$vp2='';
				$verifying_post_payment1=$this->assetHelper->api_requestv2(CENPOS_SITEVERIFY_URL,$arr2);
				$verifying_post_payment=json_decode($verifying_post_payment1,true);
					if($verifying_post_payment['Result']==0){

						//saving logs. verifying_post_payment...
						$arr_logs=[
							'method'=>'create_suncashme_checkout_log',
							'P01'=>$_POST['merchant_key'],//MerchantKey
							'P02'=>$invoice,//order_id
							'P03'=>'',//reference_number
							'P04'=>'card-link',//type
							'P05'=>'success',//response
							'P06'=>json_encode($arr2),//response
							'P07'=>json_encode($verifying_post_payment1),//response
							'P08'=>$verifying_post_payment['Message'],//response
							'return_mode'=>'json'
						];
						$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_logs);



						$vp2=$verifying_post_payment['Data'];

						$arr_post2=[
							'verifyingpost'=>$vp2,
							'tokenid'=>$process_link_card['TokenId'],
						];
						$process_payment_rnamount1=$this->assetHelper->api_requestv2(CENPOS_PROCESS_URL,$arr_post2);
						$process_payment_rnamount=json_decode($process_payment_rnamount1,true);
		
						//----------------- convert crypto 0 payment----------------------
						$_POST['card_number']=str_replace("*","",$_POST['card_number']);
						if($process_payment_rnamount['Result']==0){
							$arr=[
							'method'=>'save_merchant_customer_creditcard',
							'P01'=> $_POST['merchant_key'],//$this->session->userdata('SessionID'),
							'P02'=> $_POST['merchant_customer_id'],
							'P03'=> $process_link_card['TokenId'],//Converted Crypto
							'P04'=> $_POST['card_type'],//CardType
							'P05'=> $_POST['card_number'],//CardLastFourDigits
							'P06'=> substr($invoice, -6),//otp(invoice last 6 digits) must inplement sale transaction with 0.00 amount to
							'P07'=>'0',//fixed to zero 0
							'P08'=> $_POST['name_on_card'],//NameOnCard
							'P09'=> $card_id_url,
							'P10'=> $credit_card_url,
							'P11'=> $ccwithid_card_url,
							'P12'=>$_POST['expiration'],
							'P13'=>'',
							'P14'=>'',
							'P15'=>'',
							'P16'=>$rn_amount,
							'P17'=>$process_payment_rnamount['ReferenceNumber'],//cenpos reference number
							'P18'=>$invoice,//order id with suntag
							'return_mode' => 'json'
							];
							$save_card_process1=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
							// dd($arr);
							$save_card_process=json_decode($save_card_process1,true);

							if($save_card_process['Success']=='YES'){
								//saving logs....
								$arr_logs=[
									'method'=>'create_suncashme_checkout_log',
									'P01'=>$_POST['merchant_key'],//MerchantKey
									'P02'=>$invoice,//order_id
									'P03'=>'',//reference_number
									'P04'=>'card-link',//type
									'P05'=>'success',//response
									'P06'=>json_encode($arr),//response
									'P07'=>json_encode($save_card_process1),//response
									'P08'=>'Card Successfully Linked',//response
									'return_mode'=>'json'
								];
								$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_logs);


								// $params1=base64_encode($_POST['merchant_key'].'||'.$save_card_process['ResponseMessage']['cd']['id']);								
								$arr=[  
									// 'url'=>base_url("checkout_card/verify_form_page/'.$params1.'"),
									// 'msg'=>$api_bt_data['ResponseMessage'],
									'msg'=>'Successfully Submitted.',
									'success'=>true
								];
								echo json_encode($arr);
								exit;        
							}  else {

								//saving logs....
								$arr_logs=[
									'method'=>'create_suncashme_checkout_log',
									'P01'=>$_POST['merchant_key'],//MerchantKey
									'P02'=>$invoice,//order_id
									'P03'=>'',//reference_number
									'P04'=>'card-link',//type
									'P05'=>'failed',//response
									'P06'=>json_encode($arr),//response
									'P07'=>json_encode($save_card_process1),//response
									'P08'=>'Card Link Failed',//response
									'return_mode'=>'json'
								];
								$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_logs);
								// dd($save_logs);
								$arr=[  
									'msg'=>$save_card_process['ResponseMessage'],
									'success'=>false
								];

								echo json_encode($arr);
								exit;  
							}
						}else{

							$arr_logs=[
								'method'=>'create_suncashme_checkout_log',
								'P01'=>$_POST['merchant_key'],//MerchantKey
								'P02'=>$invoice,//order_id
								'P03'=>'',//reference_number
								'P04'=>'card-link',//type
								'P05'=>'failed',//response
								'P06'=>json_encode($arr_post2),//response
								'P07'=>json_encode($process_payment_rnamount1),//response
								'P08'=>$api_bt_data['ResponseMessage'],//response
								'return_mode'=>'json'
							];
							$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_logs);

							$arr=[	
								'msg'=>$process_payment_rnamount['Message'],
								// 'data'=>$process_payment_rnamount,
								'success'=>false
							];

							echo json_encode($arr);	
						}
						
					}else{

						$arr_logs=[
								'method'=>'create_suncashme_checkout_log',
								'P01'=>$_POST['merchant_key'],//MerchantKey
								'P02'=>$invoice,//order_id
								'P03'=>'',//reference_number
								'P04'=>'card-link',//type
								'P05'=>'failed',//response
								'P06'=>json_encode($arr2),//response
								'P07'=>json_encode($verifying_post_payment),//response
								'P08'=>$process_payment_rnamount['Message'],//response
								'return_mode'=>'json'
							];
						$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_logs);						
						$arr=[	
							'msg'=>$verifying_post_payment['Message'],
							// 'data'=>$verifying_post_payment,
							'success'=>false
						];

						echo json_encode($arr);		
					}
				
			}else{

				$arr_logs=[
					'method'=>'create_suncashme_checkout_log',
					'P01'=>$_POST['merchant_key'],//MerchantKey
					'P02'=>$invoice,//order_id
					'P03'=>'',//reference_number
					'P04'=>'card-link',//type
					'P05'=>'success',//response
					'P06'=>json_encode($arr2),//response
					'P07'=>$process_link_card12,//response
					'P08'=>$process_link_card['Message'],//response
					'return_mode'=>'json'
				];
				$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_logs);

				$arr=[	
					'msg'=>$process_link_card['Message'],
					// 'data'=>$process_link_card,
					'success'=>false
				];

				echo json_encode($arr);
			}
		} else{///
			$arr=[	
				'msg'=>$verify_post2['Message'],
				// 'data'=>$verify_post2,
				'success'=>false
			];

			echo json_encode($arr);
		}
	}
	public function verify_card_form(){//method then routefind //get fee
		$this->blade->view('card-link-verification');
	}
	public function process_verify_card(){
		$verifiction_code=hex2bin($_POST['v']);
		$post_verification_code=$_POST['verification'];
		//validate verification code.
		if($verifiction_code!=$post_verification_code){
		  $arr=[  
			'msg'=>'Invalid Verification code.',
			'success'=>false
		  ];
		  
		  echo json_encode($arr);
		  exit;  
		}
	
		$arr=[
		  'method'=>'verify_customer_creditcard',
		  'P01'=> $this->session->userdata('SessionID'),
		  'P02'=> $this->input->post('c'),
		  'return_mode' => 'json'
		];
		$api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_CUSTOMER_API_URL,$arr);
		// dd($api_bt_result);
		$api_bt_data=json_decode($api_bt_result,true);
	
		
		if($api_bt_data['ResponseCode']=='0000'){
		  $arr=[  
			'msg'=>$api_bt_data['ResponseMessage'],
			'success'=>true
		  ];
		  
		  echo json_encode($arr);
		  exit;        
		}  else {
		  $arr=[  
			'msg'=>$api_bt_data['ResponseMessage'],
			'success'=>false
		  ];
		  
		  echo json_encode($arr);
		  exit;  
		}
	}
	public function verify_card_process(){
			//dd($_POST);
			if(empty($_POST)){
				$arr=[	
					'msg'=>'safari not working af',					
					'success'=>false
				];
				
				echo json_encode($arr);
				exit;
			}

			//required post
			$required_input=[
				'last4digits',
				'card_type_w',
				'card_name_w',
				'expiration',
				'first_name',
				'last_name',
				'middle_name',
				'merchant_key',
				'merchant_customer_id',
				'gender',
				'dob',
				'address',
				'city',
				'island',
				'country',
				'card_mobile_number',
				'card_email',
				'card_id_upload',
				'credit_card_upload',
				'cc_with_card_upload',
			];
			$label=[
				'last4digits'=>'Last 4 digits',
				'card_type_w'=>'Card Type',
				'card_name'=>'Card Name',
				//'card_id'=>'Card ID',
				'card_mobile_number'=>'Mobile',
				'card_email'=>'Email',
				'card_id_upload'=>'Upload ID Picture',
				'credit_card_upload'=>'Upload Credit Card Picture.',
				'cc_with_card_upload'=>'Upload Credit Card with ID Picture.',				
			];					
		
			$validated_fields=$this->assetHelper->checkpostfields($required_input,$_POST,$label);
			//dd($validated_fields);
			if(!empty($validated_fields)){
				$arr=[	
					'msg'=>'field required '.$validated_fields,
					'success'=>false
				];
				
				echo json_encode($arr);
				exit;				
			}

		$card_id_url = '';
		if(!empty($_POST['card_id_upload'])){
			//die($key);	
			 //ci upload.
			$filename="".date("Y")."_".time()."_".rand().".png";
			$decodedData = base64_decode($_POST['card_id_upload']);
			// APPPATH will give you application folder path

			if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
			    $root_path = dirname(dirname(dirname(__FILE__)));
			    $tosend=$root_path.'\assets\img\whitelist_picture\ids\\'.$filename;
			} else {
			    $root_path = dirname(dirname(dirname(dirname(__FILE__))));
			    $tosend=$root_path.'/html/assets/img/whitelist_picture/ids/'.$filename;
			}

			$is_ok1=file_put_contents($tosend,$decodedData);
			if (!$is_ok1) {
				//$this->upload->display_errors();
				$arr=[	
					'msg'=>'Upload failed2',
					'success'=>false
				];
				
				echo json_encode($arr);
				exit;
			}

/*			 $config['image_library'] = 'gd2';
			 
			 $config['upload_path'] = './assets/img/whitelist_picture/ids';
			 $config['allowed_types'] = '*';	//gif|jpg|png|PNG|jpeg|JPG|JPEG
			 $config['file_name'] = $filename;
			 $config['encrypt_name']= FALSE;
			

			if(!is_dir($config['upload_path'])) {
			 	mkdir($config['upload_path'], 0777, TRUE);
			}

			 //load ci upload						
			 $this->upload->initialize($config);
			 //$this->upload->do_upload($key);
			if ( ! $this->upload->do_upload("card_id_upload")) {
				$this->upload->display_errors();
				$arr=[	
					'msg'=>$this->upload->display_errors(),
					'success'=>false
				];
				
				echo json_encode($arr);
				exit;
			} 	*/

			$card_id_url= base_url()."assets/img/whitelist_picture/ids/".$filename;
		} 	

		$credit_card_url = '';
		if(!empty($_POST['credit_card_upload'])){
			//die($key);	
			 //ci upload.
			 //$config = array();

			$filename="".date("Y")."_".time()."_".rand().".png";
			$decodedData = base64_decode($_POST['credit_card_upload']);

			if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
			    $root_path = dirname(dirname(dirname(__FILE__)));
			    $tosend=$root_path.'\assets\img\whitelist_picture\cc\\'.$filename;
			} else {
			    $root_path = dirname(dirname(dirname(dirname(__FILE__))));
			    $tosend=$root_path . '/html/assets/img/whitelist_picture/cc/'.$filename;
			}

			// APPPATH will give you application folder path
			$is_ok2=file_put_contents($tosend,$decodedData);
			if (!$is_ok2) {
				//$this->upload->display_errors();
				$arr=[	
					'msg'=>'Upload failed2',
					'success'=>false
				];
				
				echo json_encode($arr);
				exit;
			}
			$credit_card_url=base_url()."assets/img/whitelist_picture/cc/".$filename;
		} 

		$ccwithid_card_url='';
		if(!empty($_POST['cc_with_card_upload'])){


			$filename="".date("Y")."_".time()."_".rand().".png";
			$decodedData = base64_decode($_POST['cc_with_card_upload']);

			if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
			    $root_path = dirname(dirname(dirname(__FILE__)));
			    $tosend=$root_path.'\assets\img\whitelist_picture\ccwithid\\'.$filename;
			} else {
			    $root_path = dirname(dirname(dirname(dirname(__FILE__))));
			    $tosend=$root_path . '/html/assets/img/whitelist_picture/ccwithid/'.$filename;
			}
			// APPPATH will give you application folder path
			$is_ok3=file_put_contents($tosend,$decodedData);
			if (!$is_ok3) {
				//$this->upload->display_errors();
				$arr=[	
					'msg'=>'Upload failed3',
					'success'=>false
				];
				
				echo json_encode($arr);
				exit;
			}

			$ccwithid_card_url=base_url()."assets/img/whitelist_picture/ccwithid/".$filename;
		} 


		//after upload post it on suncash....
	    $arr=[
	    	'method'=>'login_business',
	        'P01'=> SERVICE_ACCOUNT_USERNAME_DEV,
	        'P02'=> SERVICE_ACCOUNT_PASSWORD_DEV,
	    ];
	    //$arr_tosend['url']="login_business?".http_build_query($arr);

	    //dd($arr_tosend);
	    $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
	    $api_bt_data=json_decode($api_bt_result,true);
	    //dd($api_bt_data);
	    $data['login_data']='';
	    if($api_bt_data['ResponseCode']=='YES'){
	    $data['login_data']=$api_bt_data['ResponseMessage'];                     
	    }	

	    //trim 
	    $_POST['last4digits']=ltrim($_POST['last4digits'], '*'); 
			//get payment settings. 
			//
		$card_mobile = $this->clean($this->security->xss_clean($this->input->post('card_mobile_number')));
				
       	$arr=[
         	'method'=>'save_for_verification_card',
			'P01'=> $data['login_data']['SessionID'],
			'P02'=> $_POST['merchant_key'],//$this->session->userdata('SessionID'),
			'P03'=>  $_POST['merchant_customer_id'],
            'P04'=> $_POST['last4digits'],
            'P05'=> $_POST['card_type_w'],
            'P06'=> $_POST['card_name'],
			'P07'=> $_POST['expiration'],
			'P08'=>$_POST['first_name'],
			'P09'=>$_POST['last_name'],
			'P10'=>$_POST['middle_name'],
			'P11'=>$_POST['card_mobile_number'],
			'P12'=>$_POST['card_email'],
			'P13'=>$_POST['gender'],
			'P14'=>$_POST['dob'],
			'P15'=>$_POST['address'],
			'P16'=>$_POST['city'],
			'P17'=>$_POST['island'],
			'P18'=>$_POST['country'],
            'P19'=> $card_id_url,
            'P20'=> $credit_card_url,
            'P21'=> $ccwithid_card_url,
       	];
       	//dd($arr);
       	// $arr_tosend['url']="login_business?".http_build_query($arr);

       	// dd($arr_tosend);
       	//dd($arr);
       	$api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
       	//dd($api_bt_result);
      	$api_bt_data=json_decode($api_bt_result,true);
	    if($api_bt_data['Success']=='YES'){
				$arr=[	
					//'msg'=>$api_bt_data['ResponseMessage'],
					'msg'=>'Request Successfully submitted.',
					'success'=>true
				];
				
				echo json_encode($arr);
				exit;
	    } else {
				$arr=[	
					'msg'=>$api_bt_data['ResponseMessage'],
					//'msg'=>'Request Successfully submitted.',
					'success'=>false
				];
				
				echo json_encode($arr);
				exit;	    	
	    }
	}

	private function get_random_amount($min,$max){
		$number =  mt_rand ($min*10, $max*10) / 10;
		
		if($number==0.00 || $number==0 || $number<=0){
		$this->get_random_amount($min,$max);
		//$number = number_format($number,"2",".","");
		} else {
		$number = number_format($number,"2",".","");
		}
		return $number;

	}
	//return sucess to checkoutorder
	public function verify_form_page($params){
		// echo base64_encode("a15d411bdbdfffa46c50cb57958bd68fef35d5eacbfcaba0dc21a8caf03ac995||159");
		//dd($params);
		$arr = base64_decode($params);
		//dd($arr);
		$arr = explode('||',$arr);
		//dd($arr);
		//dd(count($arr));
		if(count($arr)<2){
			$arr=[  
				'msg'=>'Invalid key.',
				'success'=>false
			];
			  
			echo json_encode($arr);
			exit;  
		}
		list($merchant_key,$cpid)=$arr;
		$data['merchant_key'] =$merchant_key;
		// $data['refid'] =$refid;
		$data['cpid'] =$cpid;
		$arr=[
		   'method'=>'get_forapprovalcard',
		   'P01'=> $merchant_key,
		   'P02'=> $cpid,//$this->session->userdata('SessionID'),
		   'return_mode'=>'json'
		];
		//get information
		$api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
		// dd($api_bt_result);
		$api_bt_data=json_decode($api_bt_result,true);
		$data['cdata']=[];
		if($api_bt_data['Success']=='YES'){
			if($api_bt_data['ResponseMessage']['invalid_attempts']<3){
				$data['cdata']=$api_bt_data['ResponseMessage'];
			} else if($api_bt_data['ResponseMessage']['invalid_attempts']==3) {
				redirect("checkout_card/exceed_attempt_page");
			}
			
		}  else {
			redirect("checkout_card/card_not_found");
		}

		$this->blade->view('card-link-verify-external-page',$data);
	}
	public function verify_form($params){
		// echo base64_encode("a15d411bdbdfffa46c50cb57958bd68fef35d5eacbfcaba0dc21a8caf03ac995||159");
		// dd($params);
		$arr = base64_decode($params);
		
		$arr = explode('||',$arr);
		// $return_link = base64_decode($params);
		// dd($return_link);
		//dd(count($arr));
		if(count($arr)<4){
			$arr=[  
				'msg'=>'Invalid key.',
				'success'=>false
			];
			  
			echo json_encode($arr);
			exit;  
		}
		list($merchant_key,$refid,$merchant_customer_id,$cpid)=$arr;
		$data['merchant_key'] =$merchant_key;
		$data['reference_id'] =$refid;
		$data['merchant_customer_id'] =$merchant_customer_id;
		$data['cpid'] =$cpid;
		
		$return_link =base64_encode($merchant_key.'||'.$merchant_customer_id);
		// dd($return_link);
		$sunccess_link =base64_encode($merchant_key.'||'.$merchant_customer_id.'||'.$merchant_customer_id);
		$arr=[
		   'method'=>'get_forapprovalcard',
		   'P01'=> $merchant_key,
		   'P02'=> $cpid,//$this->session->userdata('SessionID'),
		   'return_mode'=>'json'
		];
		//get information
		$api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
		// dd($api_bt_result);
		$api_bt_data=json_decode($api_bt_result,true);
		$data['cdata']=[];
		if($api_bt_data['Success']=='YES'){
			if($api_bt_data['ResponseMessage']['invalid_attempts']<3){
				$data['cdata']=$api_bt_data['ResponseMessage'];
				// redirect("merchant_checkout/checkoutv2/$return_link");
			} else if($api_bt_data['ResponseMessage']['invalid_attempts']==3) {
				// dd("Attempts exceeded.");
				redirect("checkout_card/exceed_attempt_page");
			}
			
		}  else {
			//return to checnkt page
			redirect("merchant_checkout/checkoutv2/$return_link");
		}
		// dd($data);
		$this->blade->view('card-link-verify-page',$data);
	}
	public function process_verify(){
		$t=unserialize(base64_decode($_POST['t']));
		// dd($t);
		$required_input=[
			'amount',
		];
		$label=[
			'amount'=>'Amount',	
		];					
	
		$validated_fields=$this->assetHelper->checkpostfields($required_input,$_POST,$label);
		//dd($validated_fields);
		if(!empty($validated_fields)){
			$arr=[	
				'msg'=>'field required '.$validated_fields,
				'success'=>false
			];
			
			echo json_encode($arr);
			exit;				
		}
		//process card approval
		$arr=[
			'method'=>'process_verify_link_card',
			'P01'=>$t['merchant_key'],
			'P02'=>$t['id'],
			'P03'=>$_POST['amount'],
			'return_mode'=>'json'
		];
		//dd($arr);
		$api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
		// dd($api_bt_result);
	    $api_bt_data=json_decode($api_bt_result,true);
		if($api_bt_data['Success']=='YES'){
				$arr=[	
					'msg'=>$api_bt_data['ResponseMessage'],		
					'success'=>true
				];
				
				echo json_encode($arr);
				exit;
		} else {
				$arr=[	
					'msg'=>$api_bt_data['ResponseMessage'],
					//'msg'=>'Request Successfully submitted.',
					'success'=>false
				];
				
				echo json_encode($arr);
				exit;	    	
		}

	}
	public function exceed_attempt_page(){//method then routefind //get fee
			$this->blade->view('card-link-exceed-attempt-page');
	}	
	public function card_not_found(){//method then routefind //get fee
			$this->blade->view('card-not-found-page');
	}		
}
