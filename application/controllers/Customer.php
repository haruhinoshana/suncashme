<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends CI_Controller {// put filename of controller on class name 

	function __construct(){//where can put dynamic functions and models....
        parent::__construct();
   		$this->session->flashdata('msg');
   		//load model.
   		// $this->load->model('user');
   		// $this->load->model('Value_sets_global','global');
   		
	}
	public function index(){//method then routefind
		//$this->users->CheckIfLogin(); //check if user is login may use if youre login
      
		$data['title']='Dashboard';
	   $data['payment_data'] = $this->session->userdata('post_data');
      //dd($data['payment_data']);
		$this->blade->view('customer.dashboard.dashboard',$data);	//for blade view calling

	}

	public function logout(){
      //dd($_SESSION);
      $this->session->unset_userdata('SessionID');
      $this->session->unset_userdata('custoner_info');
      $this->session->unset_userdata('post_data');
      session_destroy();
      
      redirect($_SESSION['client_id']);   
   }  
	public function logout_cc(){
      $sc = $_SESSION['suntag_shortcode'];
      $this->session->unset_userdata('SessionID_cc');
      $this->session->unset_userdata('custoner_info_cc');
      $this->session->unset_userdata('post_data');
      session_destroy();
      
      redirect(base_url($sc));   
   }  


}