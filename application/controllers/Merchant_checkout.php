<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Merchant_checkout extends CI_Controller {// put filename of controller on class name 

	function __construct(){//where can put dynamic functions and models....
        parent::__construct();
   		$this->session->flashdata('msg');
   		//load model.
      	$this->load->model('core/db');//for transactions and other eloquent db features
      	$this->load->model('core/Qr');
   		//$this->load->model('user');
   		//$this->load->model('Value_sets_global','global');
		$this->assetHelper = new AssetHelper\AssetHelper();
		   

		$this->amazonpay_config = array(
				'public_key_id' => 'AGWGBAZVOO5IESX4GE6WM6UG',//add to _settings or constant
				'private_key'   => 'AmazonPay_AGWGBAZVOO5IESX4GE6WM6UG.pem',//add to _settings or constant
				'region'        => 'US',
				'sandbox'       => true
		); 
    }
	public function checkoutv2($params){
		// dd($_GET);
        $arr = base64_decode($params);
        $arr = explode('||',$arr);
		if(count($arr)<3){
			$arr=[  
				'msg'=>'Invalid request.',
				'success'=>false
			];
			  
			echo json_encode($arr);
			exit;  
        }
        
        list($merchant_key,$reference_id,$merchant_customer_id) = $arr;
		if(!empty($merchant_key) && !empty($reference_id)){
            //get credit card of  merchant customer.
            $data['customer_merchant_cards']= '';
            //dd($merchant_customer_id);
            if($merchant_customer_id!='x'){
                $data['customer_merchant_cards']=$this->getcustomercard($merchant_key,$merchant_customer_id);
				$data['customer_merchant_sdcards']=$this->getsdcustomercard($merchant_key,$merchant_customer_id);
            }
			//get sandollar card.
			//dd($data['customer_merchant_sdcards']);
			$data['IsSdcardempty'] = 1;
			if(!empty($data['customer_merchant_sdcards'])){
			$data['IsSdcardempty'] =0;
			}

			// dd($data['IsSdcardempty']);
			$arr=[
				'method'=>'get_client_checkout_details',
				'P01'=>$merchant_key,
				'P02'=>$reference_id,
				'return_mode'=>'json'
			];
		    $api_fee_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
		    $api_fee_data=json_decode($api_fee_result,true);		
		    if($api_fee_data['Success']=='YES'){
				if($api_fee_data['ResponseMessage']['status']!='PENDING'){
					redirect('');
				}
					//process order as individual array
					$ItemName=[];
					$ItemQty=[];
					$ItemPrice=[];
					if(!empty($api_fee_data['ResponseMessage']['details'])){
						for ($i=0; $i <count($api_fee_data['ResponseMessage']['details']) ; $i++) { 
							# code...
							$ItemName[$i]=$api_fee_data['ResponseMessage']['details'][$i]['item_name'];
							$ItemQty[$i]=$api_fee_data['ResponseMessage']['details'][$i]['qty'];
							$ItemPrice[$i]=$api_fee_data['ResponseMessage']['details'][$i]['price'];		    		
						}
					}
					$return_data=[
					'merchant_client_id'=>$api_fee_data['ResponseMessage']['merchant_client_id'],
					'MerchantKey'=>$api_fee_data['ResponseMessage']['merchant_key'],
					'MerchantName'=>$api_fee_data['ResponseMessage']['merchant_name'],
					'Amount'=>$api_fee_data['ResponseMessage']['amount'],
					'total_amount'=>$api_fee_data['ResponseMessage']['total_amount'],
					'OrderID'=>$api_fee_data['ResponseMessage']['order_id'],
					'CallbackURL'=>$api_fee_data['ResponseMessage']['callback_url'],
					'ItemName'=>$ItemName,
					'ItemQty'=>$ItemQty,
					'ItemPrice'=>$ItemPrice,
					'OrderReference'=>$reference_id,
					'profile_pic'=>$api_fee_data['ResponseMessage']['profile_pic'],
					'reference_id'=>$reference_id,
					'sunpass_fee'=>$api_fee_data['ResponseMessage']['sunpass_fee'],
					'facility_fee'=>$api_fee_data['ResponseMessage']['facility_fee'],
					'merchant_customer_id'=>$api_fee_data['ResponseMessage']['merchant_customer_id'],
					'payment_method'=>$api_fee_data['ResponseMessage']['payment_method'],	
					'merchant_customer_fullname'=>$api_fee_data['ResponseMessage']['merchant_customer_fullname'],	
					'merchant_customer_mobile'=>$api_fee_data['ResponseMessage']['merchant_customer_mobile'],					
					'merchant_customer_email'=>$api_fee_data['ResponseMessage']['merchant_customer_email'],					
					//'auth',				
					];
					$data['reference_id']=$reference_id;

					//dd($return_data);

					$data['post_data'] = $return_data;

					$arr=[
						'method'=>'login_business',
						'P01'=> SERVICE_ACCOUNT_USERNAME_DEV,
						'P02'=> SERVICE_ACCOUNT_PASSWORD_DEV,
					];
					//$arr_tosend['url']="login_business?".http_build_query($arr);

					//dd($arr_tosend);
					$api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
					$api_bt_data=json_decode($api_bt_result,true);
					//dd($api_bt_data);
					$data['login_data']='';
					if($api_bt_data['ResponseCode']=='YES'){
					$data['login_data']=$api_bt_data['ResponseMessage'];
					}

					
					$arr_guest=[
						'method'=>'get_cc_guest',
						'P01'=> $api_fee_data['ResponseMessage']['merchant_key'],
					];

					$api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr_guest);
					$api_bt_data=json_decode($api_bt_result,true);
					$data['is_cc_guest']='';
					if($api_bt_data['Success']=='YES'){
						$data['is_cc_guest']=$api_bt_data['ResponseMessage']['is_cc_guest_enable'];
					}
			
					//get payment settings. 
					$arr=[
						'method'=>'get_checkout_methods_byid',
							'P01'=> $data['login_data']['SessionID'],
							'P02'=> $api_fee_data['ResponseMessage']['merchant_client_id'],
					];
					// $arr_tosend['url']="login_business?".http_build_query($arr);

					// dd($arr_tosend);
					//dd($arr);
					$api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
					$api_bt_data=json_decode($api_bt_result,true);
					//dd($api_bt_result);
					$data['selected_settings_data']='';
					if($api_bt_data['Success']=='YES'){
					$api_bt_data['ResponseMessage'][0] =isset($api_bt_data['ResponseMessage'][0]) ? $api_bt_data['ResponseMessage'][0] : '';
					$data['selected_settings_data']=$api_bt_data['ResponseMessage'][0];                     
					} 
					$data['sunpass_fee']=0.00;
					$data['facility_fee']=0.00;
					//get sunpass fee$$$$.
					if(in_array($merchant_key,SUNPASS_MERCHANTS_KEYS)){
					$data['sunpass_fee'] = $api_fee_data['ResponseMessage']['sunpass_fee']>0 ? number_format($api_fee_data['ResponseMessage']['sunpass_fee'],2) : 0.00;
					$data['facility_fee'] =$api_fee_data['ResponseMessage']['facility_fee']>0 ?  number_format($api_fee_data['ResponseMessage']['facility_fee'],2): 0.00;
					}

					//session return for safari....
					$data['post_data']['pf']=0.00;
					$data['post_data']['tf']=0.00;
					$data['post_data']['fee']=0.00;
					$data['post_data']['sunpass_fee']=$data['sunpass_fee'];
					$data['post_data']['facility_fee']=$data['facility_fee'];
					$data['post_data']['subtotal']=$return_data['Amount'];
					$data['post_data']['total_amount']=$return_data['Amount']+$data['sunpass_fee']+$data['facility_fee'];
					$data['post_data']['promo_code']='';
					$data['post_data']['discount']=0.00;
					//$data['post_data']['subtotal'] = $_SESSION['post_data']['subtotal'];

					$this->session->set_userdata('post_data',$return_data);
					$_SESSION['post_data']['pf'] = 0.00;
					$_SESSION['post_data']['tf'] = 0.00;
					$_SESSION['post_data']['fee'] = 0.00;
					$_SESSION['post_data']['sunpass_fee'] = $data['sunpass_fee'];
					$_SESSION['post_data']['facility_fee'] = $data['facility_fee'];
					$_SESSION['post_data']['subtotal'] = $_SESSION['post_data']['Amount'];
					$_SESSION['post_data']['total_amount']=$return_data['Amount']+$data['sunpass_fee']+$data['facility_fee'];
					$_SESSION['post_data']['promo_code'] = '';
					$_SESSION['post_data']['discount'] = 0.00;
					$data['subtotal'] = $_SESSION['post_data']['subtotal'];
					//site verify
					$arr=[
						'merchant'=>CENPOST_MERCHANT_ID,
						'secretkey'=>CENPOST_MERCHANT_SECRETKEY,
						'email'=>'suncashme@gmail.com',
						'ip'=>'127.0.0.1',
					];
					$verify_post=$this->assetHelper->api_requestv2(CENPOS_SITEVERIFY_URL,$arr);
					$verify_post=json_decode($verify_post,true);	
					//dd($verify_post);
					$data['verify_params']=$verify_post;

					//amazon
					$payload = array(
						'webCheckoutDetails' => array(
							'checkoutReviewReturnUrl' => 'https://localhost/suncashme/merchant_checkout/checkoutv2/'.$params.'?status=review', //review
							'checkoutResultReturnUrl' => 'https://localhost/suncashme/merchant_checkout/checkoutv2/'.$params.'?status=pay',//complete
						),
						'storeId' => 'amzn1.application-oa2-client.ced8ec472a0644e0ae67c7c2de8c767c',
						'merchantMetadata' => array(
							'merchantReferenceId' =>$reference_id,//checkoutreferenceid
							'merchantStoreName' =>$api_fee_data['ResponseMessage']['merchant_name'],
							'noteToBuyer' => 'Thank you for your order!'
						),
					);
					//dd($payload);
					$headers = array('x-amz-pay-Idempotency-Key' => uniqid());
					$requestResult = [
						'error' => 1,
						'msg' => 'Error. Can not create checkout session.',
						'signature' => null,
						'payload' => null
					];

					$client = new Amazon\Pay\API\Client($this->amazonpay_config);
					$resultCheckOut = $client->createCheckoutSession($payload, $headers);
					$resultSignature = $client->generateButtonSignature($payload);
					if($resultCheckOut['status'] !== 201) {

						$arr=[  
							'msg'=>'Invalid Request',
							'amz_msg'=>$requestResult,
							'success'=>false
						];
						
						echo json_encode($arr);
						exit;  
					}else {


						$arr=[
							'method'=>'get_payment_fee',	
							'P01' =>$api_fee_data['ResponseMessage']['merchant_key'], //Merchant API Key
							'P02' =>$data['post_data']['total_amount'], //Mobile No
							'P03' =>'AMAZON', //passcode	
							'P04' =>'BUSINESS',	   
							'return_mode'=>'json'
						];
							// dd($arr);
						$fees=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
						$amazon_fees=json_decode($fees,true);
						$data['amazon_fees']='';
						if($amazon_fees['Success']=='YES'){
							$data['amazon_fees']=$amazon_fees['ResponseMessage'];
						}
						// dd($amazon_fees);
						// $total_due= $data['amazon_fees']['total_due'];
						$total_convenience_fee= $data['amazon_fees']['total_convenience_fee'];
						$iscard_fee_on_merch=$data['amazon_fees']['iscard_fee_on_merch'];
						$total_due= $data['amazon_fees']['total_due'];
						if($iscard_fee_on_merch>0){
							$ccfee=0.00;
						}else{
							$ccfee=$total_convenience_fee;
						}
							
						$this->session->set_userdata('amz_total',$total_due);
						// $this->session->set_userdata('amz_cc_fee',$ccfee);
						// $this->session->set_userdata('amz_amount',$total_due);
						$this->session->set_userdata('amz_cc_ismerch',$iscard_fee_on_merch);
						$this->session->set_userdata('amz_sig',$resultSignature);
						$this->session->set_userdata('amz_payload',$payload);
						$this->session->set_userdata('amz_checkout',$resultCheckOut);
						$this->session->set_userdata('amz_id',$headers);
						$data['signature']= $resultSignature;
						$data['payload']= $payload;
						$data['amz_order_deatils']= '';
						$data['amz_status']=isset($_GET['status']) ? $_GET['status'] : '';
						if(isset($_GET['amazonCheckoutSessionId'])){
							$this->session->set_userdata('amz_sess',$_GET['amazonCheckoutSessionId']);
							$payload='';
							$headers = array('x-amz-pay-Idempotency-Key' => $this->session->userdata('amz_id'));

							try {	
							$checkoutSessionId = $_GET['amazonCheckoutSessionId'];
							//dd($this->amazonpay_config);
							$client = new Amazon\Pay\API\Client($this->amazonpay_config);
							$result = $client->getCheckoutSession($checkoutSessionId);
								if ($result['status'] === 200) {
									$response = json_decode($result['response'], true);
									//dd($response);
									$data['amz_order_deatils']=$response;
									$checkoutSessionState = $response['statusDetails']['state'];
									$chargeId = $response['chargeId'];
									$chargePermissionId = $response['chargePermissionId'];

									if($_GET['status']=='review'){
									$buyer['buyername'] = $response['buyer']['name'];
									$buyer['buyeremail'] = $response['buyer']['email'];
									$this->session->set_userdata('amz_sess_buyer',$buyer);
									}
								} else {
									// echo 'status=' . $result['status'] . '; response=' . $result['response'] . "\n";
						
									$arr=[  
										'msg'=>'Invalid Request',
										'amz_msg'=>$result['response'],
										'success'=>false
									];
									
									echo json_encode($arr);
									exit;  
								}
							} catch (\Exception $e) {
								// handle the exception
								echo $e . "\n";
							}
						}
						// dd($data);

						$this->load->library('Mobile_Detect');
						if($this->mobile_detect->isMobile()){
							$loc="app";
						}else{
							$loc="web";
						}
						$data['location']=$loc;		
						
						$arr=[
							'method'=>'sanDollarPendingTransaction',
							'P01'=> $api_fee_data['ResponseMessage']['merchant_client_id'],
							'P02'=> $api_fee_data['ResponseMessage']['merchant_key'], //Merchant API Key
							'P03'=>	$return_data['Amount']+$data['sunpass_fee']+$data['facility_fee'],
							'P04'=>	'CHECKOUT',
							'P05'=>'BUSINESS_PAYMENT',
							'P06'=>'',
							'P07'=>'',
							'P08'=>'',			
							'P09'=>'',		
							'P10'=>$reference_id,									
							'return_mode'=>'json'
						];
						// dd($arr);
						$api_fee_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
						$sanddata=json_decode($api_fee_result,true);
						//dd($api_fee_result);
						$data['sand_data']='';
						if($sanddata['Success']=='YES'){
							$data['sand_data']=$sanddata['ResponseMessage'];  
							if(!empty($sanddata['ResponseMessage']['referenceid'])){
								$data['sand_data']['referenceid'] =  $sanddata['ResponseMessage']['referenceid'];
							} else {
								$data['sand_data'] =  $sanddata['ResponseMessage']['data'];
							}
							

						} 	
						$arr=[
							'method'=>'get_sanddollar_settings',	
							'P01' => $data['login_data']['SessionID'], //Merchant API Key
							'P02' =>$api_fee_data['ResponseMessage']['merchant_client_id'], //Mobile No  
							'return_mode'=>'json'
						];
							// dd($arr);
						$fees=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
						$amazon_fees=json_decode($fees,true);
						$data['sd_settings']='';
						if($amazon_fees['Success']=='YES'){
							$data['sd_settings']=$amazon_fees['ResponseMessage'];
						}
						// dd($data['sd_settings']);


						// dd($data);
						$this->blade->view('checkoutv2',$data);
					}



				} else {
			redirect('');	    	
		    }	


		} else {
			redirect('');
		}
		
    }
    private function getcustomercard($merchant_key,$merchant_customer_id){

		// get card info
		$arr=[
			'method'=>'get_merchant_customer_creditcard',
			'P01'=> $merchant_key,
			'P02'=> $merchant_customer_id,
			'return_mode' => 'json'
		  ];
		  //dd($arr);
		  $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
		  $api_bt_data=json_decode($api_bt_result,true);
		  //dd($api_bt_data);
		  $ccdata='';
		  if($api_bt_data['Success']=='YES'){
			 $ccdata=$api_bt_data['ResponseMessage'];
          }
          //dd($ccdata);
		  //prefer cards/

            return $ccdata;
	}
	public function process_nontokenization(){
		//dd($_POST);
		if(empty($_POST['tokenid'])){

			$arr=[
				'msg'=>'Please fill up all fields',
				'success'=>false
			];
			echo json_encode($arr);
        }
		//recall api staging on saving..
		$arr=[
			'method'=>'get_client_checkout_details',
			'P01'=>$_POST['m'],
			'P02'=>$_POST['r'],
			'return_mode'=>'json'
		];
	    $api_fee_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
	    //dd($api_fee_result);
	    $api_fee_data=json_decode($api_fee_result,true);
	    //dd($api_fee_result);		
	    if($api_fee_data['Success']=='YES'){
	   	//check if pending...
 		if($api_fee_data['ResponseMessage']['status']!='PENDING'){
 	   		redirect('');
 	   	}
	    //process order as individual array
	    $ItemName=[];
	    $ItemQty=[];
	    $ItemPrice=[];
	    if(!empty($api_fee_data['ResponseMessage']['details'])){
	    	for ($i=0; $i <count($api_fee_data['ResponseMessage']['details']) ; $i++) {
	    		# code...
			    $ItemName[$i]=$api_fee_data['ResponseMessage']['details'][$i]['item_name'];
			    $ItemQty[$i]=$api_fee_data['ResponseMessage']['details'][$i]['qty'];
			    $ItemPrice[$i]=$api_fee_data['ResponseMessage']['details'][$i]['price'];
	    	}
	    }
		$return_data=[

			'trans_type'=>$api_fee_data['ResponseMessage']['trans_type'],//Auth or Sale

		];
		}

		$post_data =$return_data;

		if(Ucfirst($api_fee_data['ResponseMessage']['trans_type'])== 'Sale'){
			$this->process_nontokken_sale();
		}else if(Ucfirst($api_fee_data['ResponseMessage']['trans_type'])== 'Auth'){
			$this->process_nontokken_auth();
		}else{
			$arr=[
				'msg'=>'Invalid Transaction Type',
				'success'=>false
			];
			echo json_encode($arr);
		}
	}
    public function process_nontokken_auth(){//nontokkenized Auh/pending payment
        
		if(empty($_POST['tokenid'])){

			$arr=[
				'msg'=>'Please fill up all fields',
				'success'=>false
			];
			echo json_encode($arr);
        }
		//recall api staging on saving..
		$arr=[
			'method'=>'get_client_checkout_details',
			'P01'=>$_POST['m'],
			'P02'=>$_POST['r'],
			'return_mode'=>'json'
		];
	    $api_fee_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
	    //dd($api_fee_result);
	    $api_fee_data=json_decode($api_fee_result,true);
	    //dd($api_fee_result);		
	    if($api_fee_data['Success']=='YES'){
	   	//check if pending...
 		if($api_fee_data['ResponseMessage']['status']!='PENDING'){
 	   		redirect('');
 	   	}
	    //process order as individual array
	    $ItemName=[];
	    $ItemQty=[];
	    $ItemPrice=[];
	    if(!empty($api_fee_data['ResponseMessage']['details'])){
	    	for ($i=0; $i <count($api_fee_data['ResponseMessage']['details']) ; $i++) {
	    		# code...
			    $ItemName[$i]=$api_fee_data['ResponseMessage']['details'][$i]['item_name'];
			    $ItemQty[$i]=$api_fee_data['ResponseMessage']['details'][$i]['qty'];
			    $ItemPrice[$i]=$api_fee_data['ResponseMessage']['details'][$i]['price'];
	    	}
	    }
		$return_data=[
		'merchant_client_id'=>$api_fee_data['ResponseMessage']['merchant_client_id'],
		'MerchantKey'=>$api_fee_data['ResponseMessage']['merchant_key'],
		'MerchantName'=>$api_fee_data['ResponseMessage']['merchant_name'],
		'Amount'=>$api_fee_data['ResponseMessage']['amount'],
		'total_amount'=>$api_fee_data['ResponseMessage']['total_amount'],
		'OrderID'=>$api_fee_data['ResponseMessage']['order_id'],
		'CallbackURL'=>$api_fee_data['ResponseMessage']['callback_url'],
		'ItemName'=>$ItemName,
		'ItemQty'=>$ItemQty,
		'ItemPrice'=>$ItemPrice,
		'OrderReference'=>$_POST['r'],
		'profile_pic'=>$api_fee_data['ResponseMessage']['profile_pic'],
		'reference_id'=>$_POST['r'],
		'sunpass_fee'=>$api_fee_data['ResponseMessage']['sunpass_fee'],
		'facility_fee'=>$api_fee_data['ResponseMessage']['facility_fee'],
		'processing_fee'=>$api_fee_data['ResponseMessage']['processing_fee'],
		'transaction_fee'=>$api_fee_data['ResponseMessage']['transaction_fee'],
		'discount'=>$api_fee_data['ResponseMessage']['discount'],
		'iscard_fee_on_merch'=>$api_fee_data['ResponseMessage']['iscard_fee_on_merch'],
		//'auth',
		];
		}

		$post_data =$return_data;
		
		//backdoor conveniene fee
		$arr=[
	    	'method'=>'login_business',
	        'P01'=> SERVICE_ACCOUNT_USERNAME_DEV,
	        'P02'=> SERVICE_ACCOUNT_PASSWORD_DEV,
	    ];
	    //$arr_tosend['url']="login_business?".http_build_query($arr);

	    //dd($arr_tosend);
	    $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
	    $api_bt_data=json_decode($api_bt_result,true);
	    //dd($api_bt_data);
	    $data['login_data']='';
	    if($api_bt_data['ResponseCode']=='YES'){
	    $data['login_data']=$api_bt_data['ResponseMessage'];
	    }

       //get transaction fees. 
       // $client_record_id = $_SESSION['client_record_id'];
       $arr=[
         'method'=>'get_convenience_fee',
            'P01'=> $data['login_data']['SessionID'],
            'P02'=> $api_fee_data['ResponseMessage']['merchant_client_id'],
       ];

       // $arr_tosend['url']="login_business?".http_build_query($arr);

       //dd($arr);
       //print_r($arr);die;
       $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
       $api_bt_data=json_decode($api_bt_result,true);
       //dd($api_bt_result);
       //var_dump($api_bt_result);die;
       $data['transaction_fee_data']='';
       if($api_bt_data['Success']=='YES'){
       $api_bt_data['ResponseMessage']=isset($api_bt_data['ResponseMessage']) ? $api_bt_data['ResponseMessage'] : '';
       $transaction_fee_data=$api_bt_data['ResponseMessage'];
       } 
       $amount =str_replace(',', '', $api_fee_data['ResponseMessage']['amount']); //? $_POST['amount']  : $this->session->userdata('post_data')['Amount'];
       //dd($amount);
       $total= 0.00;
       $pf=0.00;
       $tf=0.00;
       $upf=0.00;
       $utf=0.00;
       $isenable='';
	     if(!empty($transaction_fee_data)){
	       	   $isenable=$transaction_fee_data['is_enable_cc_fee'];
		       if($transaction_fee_data['is_enable_cc_fee']==1){
		       		$gtot =number_format($amount,2,'.','')+$api_fee_data['ResponseMessage']['sunpass_fee']+$api_fee_data['ResponseMessage']['facility_fee'];

			       if($transaction_fee_data['cc_processing_fee_isfixed']==1){//fixed
			       		//$fee+= $transaction_fee_data['cc_processing_fee'];
			       		$pf=$transaction_fee_data['cc_processing_fee'];
			       		$upf=$transaction_fee_data['cc_processing_fee'];
			       		//$total += floatval($amount)+floatval($fee);
			       } else if($transaction_fee_data['cc_processing_fee_isfixed']==0){
			       		$percentage= $transaction_fee_data['cc_processing_fee']/100;
			       		$total_percent=number_format($gtot,2,'.','')*number_format($percentage,2,'.','');
			       		//echo $amount;
			       		//echo $fee;
			       		$pf=$total_percent;
			       		$upf=$total_percent;
			       }
			       //apply fee...
			       if($transaction_fee_data['transaction_fee_isfixed']==1){//fixed
			       		//$fee+= $transaction_fee_data['transaction_fee'];
			       		$tf=$transaction_fee_data['transaction_fee'];
			       		$utf=$transaction_fee_data['transaction_fee'];
			       		//$total += floatval($amount)+floatval($fee);
			       } else if($transaction_fee_data['transaction_fee_isfixed']==0){//percent
			       		$percentage=$transaction_fee_data['transaction_fee']/100;
			       		$total_percent=number_format($gtot,2,'.','')*number_format($percentage,2,'.','');
			       		//echo $fee."<br>";
			       		$tf=$total_percent;
			       		$utf=$total_percent;
			       }
		       } else {
		       		//
		       		$isenable=$transaction_fee_data['is_enable_cc_fee'];
		       		$gtot =$amount+$api_fee_data['ResponseMessage']['sunpass_fee']+$api_fee_data['ResponseMessage']['facility_fee'];

			       if($transaction_fee_data['cc_processing_fee_isfixed']==1){//fixed
			       		//$fee+= $transaction_fee_data['cc_processing_fee'];
			       		$upf=$transaction_fee_data['cc_processing_fee'];
			       		//$total += floatval($amount)+floatval($fee);
			       } else if($transaction_fee_data['cc_processing_fee_isfixed']==0){
			       		$percentage= $transaction_fee_data['cc_processing_fee']/100;
			       		$total_percent=number_format($gtot,2,'.','')*number_format($percentage,2,'.','');
			       		//echo $amount;
			       		//echo $fee;
			       		$upf=$total_percent;
			       }
			       //apply fee...
			       if($transaction_fee_data['transaction_fee_isfixed']==1){//fixed
			       		//$fee+= $transaction_fee_data['transaction_fee'];
			       		$utf=$transaction_fee_data['transaction_fee'];
			       		//$total += floatval($amount)+floatval($fee);
			       } else if($transaction_fee_data['transaction_fee_isfixed']==0){//percent
			       		$percentage=$transaction_fee_data['transaction_fee']/100;
			       		$total_percent=number_format($gtot,2,'.','')*number_format($percentage,2,'.','');
			       		//echo $fee."<br>";
			       		$utf=$total_percent;
			       }
		       }
	       }

		//dd($post_data);
		//detect if ios af.
		$this->load->library('Mobile_Detect');
		$pre_total = $post_data['Amount']-$post_data['discount'];
		if($pre_total<=0){
			$pre_total=0.00;
        }
        // $api_fee_data['ResponseMessage']['iscard_fee_on_merch']=1;//default to 1 charge to merch card fee
		if($api_fee_data['ResponseMessage']['iscard_fee_on_merch']==1){ //1 means disabled and convenience fee should be charged to  merchant/customer merchant account
			$grand_total =$pre_total+$post_data['sunpass_fee']+$post_data['facility_fee'];
		} else {
			$grand_total =$pre_total+$post_data['sunpass_fee']+$post_data['facility_fee']+number_format($upf,2,'.','')+number_format($utf,2,'.','');
		}
		$grand_total = number_format($grand_total, 2, '.', '');
		//dd($this->mobile_detect);
		$arr=[];


		if($grand_total <= 0){

			$arr=[	
				'msg'=>'Amount should be greather than 0',
				'success'=>false
			];
			
			echo json_encode($arr);
			exit;

		} 


		$arr=[
		'amount'=>$grand_total,
		'type'=>'Auth',
		'invoicenumber'=>$post_data['OrderID'],
		'email'=>'suncashme@gmail.com',
		'secretkey'=>CENPOST_MERCHANT_SECRETKEY,
		'merchant'=>CENPOST_MERCHANT_ID,
		'tokenid'=>$_POST['tokenid'],
		'isrecaptcha'=>false,
		];
		$verifying_post='';
		$verify_post1=$this->assetHelper->api_requestv2(CENPOS_SITEVERIFY_URL,$arr);
		//dd($verify_post2);
		$verify_post=json_decode($verify_post1,true);
		//result verify
		if($verify_post['Result']==0){
			$arr_post=[
				'method'=>'create_suncashme_checkout_log',
				'P01'=>$post_data['MerchantKey'],//MerchantKey
				'P02'=>$post_data['OrderID'],//order_id
				'P03'=>'',//reference_number
				'P04'=>'cenpos',//type
				'P05'=>'success',//response
				'P06'=>json_encode($arr),//response
				'P07'=>$verify_post1,//response
				'P08'=>$verify_post['Message'],//response
				'return_mode'=>'json'	
			];
			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);
			$verifying_post=$verify_post['Data'];

			$arr_post2=[
				'verifyingpost'=>$verify_post['Data'],
				'tokenid'=>$_POST['tokenid'],
			];
			//dd($arr_post2);
			$process_payment1=$this->assetHelper->api_requestv2(CENPOS_PROCESS_URL,$arr_post2);
			$process_payment=json_decode($process_payment1,true);


			if($process_payment['Result']==0){//

				//update status ..
				$arr_update=[
					'method'=>'update_checkout_transaction',
					'P01'=>$post_data['OrderReference'],//reference
					'P02'=>'RESERVED',//status Reserved pending payments in cenpos
					'P03'=>$process_payment['ReferenceNumber'],
					'return_mode'=>'json'
				];
				//  dd($arr_update);
				$update_status=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr_update);

				//update fees and total via api ....
				$arr_update_fee=[
					'method'=>'update_checkout_transaction_fee',
					'P01'=>$_POST['r'],//reference 
					'P02'=>number_format($upf,2,'.',''),//status 
					'P03'=>number_format($utf,2,'.',''),//status 
					'P04'=>$grand_total,//$total,//status 
					'P05'=>'0.00',//number_format($_POST['discount'],2,'.',''),//status 
					'P06'=>'0.00',//$_POST['promo_code'],//status 
					'P07'=>$api_fee_data['ResponseMessage']['iscard_fee_on_merch'],
					'return_mode'=>'json'	
				];
				// dd($arr_update_fee);
				$update_status2=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr_update_fee);
				//$update_status2
				//update fees and total via api ....
				$arr_update_ccinfo=[
					'method'=>'update_checkout_transaction_ccinfo',
					'P01'=>$_POST['r'],//reference 
					'P02'=>$_POST['card_number'],//type
					'P03'=>$_POST['name_on_card'],//response
					'P04'=>$_POST['card_type'],//response
					'return_mode'=>'json'	
				];
				// dd($arr_update_ccinfo);
				$update_ccinfo=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr_update_ccinfo);       

				//saving logs....
				$arr_post=[
					'method'=>'create_suncashme_checkout_log',
					'P01'=>$post_data['MerchantKey'],//MerchantKey
					'P02'=>$post_data['OrderID'],//order_id
					'P03'=>$process_payment['ReferenceNumber'],//reference_number
					'P04'=>'cenpos',//type
					'P05'=>'success',//response
					'P06'=>json_encode($arr_post2),//response
					'P07'=>$process_payment1,//response
					'P08'=>$process_payment['Message'],//response
					'return_mode'=>'json'
				];
				$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);

				unset($_SESSION['post_data']);
				$reference_num = $process_payment['ReferenceNumber']."||".$_POST['r'];
		

				$params=base64_encode($reference_num."||success||card");
				$arr=[
					'msg'=>$process_payment['Message'],
					'reference'=>$process_payment['ReferenceNumber'],
					'url'=>$post_data["CallbackURL"].$params,
					'payment_method'=>'Card',
					'success'=>true
				];

				echo json_encode($arr);
			} else {
				//saving logs....
				$arr_post=[
					'method'=>'create_suncashme_checkout_log',
					'P01'=>$post_data['MerchantKey'],//MerchantKey
					'P02'=>$post_data['OrderID'],//order_id
					'P03'=>'',//reference_number
					'P04'=>'cenpos',//type
					'P05'=>'failed',//response
					'P06'=>json_encode($arr_post2),//response
					'P07'=>$process_payment1,//response
					'P08'=>$process_payment['Message'],//response
					'return_mode'=>'json'
				];
				$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);

				//unset($_SESSION['post_data']);

				$arr=[
					'msg'=>$process_payment['Message'],
					'success'=>false
				];
				echo json_encode($arr);
			}
		} else{///
			$arr_post=[
				'method'=>'create_suncashme_checkout_log',
				'P01'=>$post_data['MerchantKey'],//MerchantKey
				'P02'=>$post_data['OrderID'],//order_id
				'P03'=>'',//reference_number
				'P04'=>'cenpos',//type
				'P05'=>'failed',//response
				'P06'=>json_encode($arr),//response
				'P07'=>$verify_post1,//response
				'P08'=>$verify_post['Message'],//response
				'return_mode'=>'json'
			];
			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);

			$arr=[
				'msg'=>$verify_post['Message'],
				'data'=>$verify_post,
				'success'=>false
			];

			echo json_encode($arr);
		}


	}
	public function process_nontokken_sale(){//nontokkenized sale payment
        
		if(empty($_POST['tokenid'])){

			$arr=[
				'msg'=>'Please fill up all fields',
				'success'=>false
			];
			echo json_encode($arr);
        }
		//recall api staging on saving..
		$arr=[
			'method'=>'get_client_checkout_details',
			'P01'=>$_POST['m'],
			'P02'=>$_POST['r'],
			'return_mode'=>'json'
		];
	    $api_fee_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
	    //dd($api_fee_result);
	    $api_fee_data=json_decode($api_fee_result,true);
	    //dd($api_fee_result);		
	    if($api_fee_data['Success']=='YES'){
	   	//check if pending...
 		if($api_fee_data['ResponseMessage']['status']!='PENDING'){
 	   		redirect('');
 	   	}
	    //process order as individual array
	    $ItemName=[];
	    $ItemQty=[];
	    $ItemPrice=[];
	    if(!empty($api_fee_data['ResponseMessage']['details'])){
	    	for ($i=0; $i <count($api_fee_data['ResponseMessage']['details']) ; $i++) {
	    		# code...
			    $ItemName[$i]=$api_fee_data['ResponseMessage']['details'][$i]['item_name'];
			    $ItemQty[$i]=$api_fee_data['ResponseMessage']['details'][$i]['qty'];
			    $ItemPrice[$i]=$api_fee_data['ResponseMessage']['details'][$i]['price'];
	    	}
	    }
		$return_data=[
		'merchant_client_id'=>$api_fee_data['ResponseMessage']['merchant_client_id'],
		'MerchantKey'=>$api_fee_data['ResponseMessage']['merchant_key'],
		'MerchantName'=>$api_fee_data['ResponseMessage']['merchant_name'],
		'Amount'=>$api_fee_data['ResponseMessage']['amount'],
		'total_amount'=>$api_fee_data['ResponseMessage']['total_amount'],
		'OrderID'=>$api_fee_data['ResponseMessage']['order_id'],
		'CallbackURL'=>$api_fee_data['ResponseMessage']['callback_url'],
		'ItemName'=>$ItemName,
		'ItemQty'=>$ItemQty,
		'ItemPrice'=>$ItemPrice,
		'OrderReference'=>$_POST['r'],
		'profile_pic'=>$api_fee_data['ResponseMessage']['profile_pic'],
		'reference_id'=>$_POST['r'],
		'sunpass_fee'=>$api_fee_data['ResponseMessage']['sunpass_fee'],
		'facility_fee'=>$api_fee_data['ResponseMessage']['facility_fee'],
		'processing_fee'=>$api_fee_data['ResponseMessage']['processing_fee'],
		'transaction_fee'=>$api_fee_data['ResponseMessage']['transaction_fee'],
		'discount'=>$api_fee_data['ResponseMessage']['discount'],
		'iscard_fee_on_merch'=>$api_fee_data['ResponseMessage']['iscard_fee_on_merch'],
		//'auth',
		];
		}

		$post_data =$return_data;
		
		//backdoor conveniene fee
		$arr=[
	    	'method'=>'login_business',
	        'P01'=> SERVICE_ACCOUNT_USERNAME_DEV,
	        'P02'=> SERVICE_ACCOUNT_PASSWORD_DEV,
	    ];
	    //$arr_tosend['url']="login_business?".http_build_query($arr);

	    //dd($arr_tosend);
	    $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
	    $api_bt_data=json_decode($api_bt_result,true);
	    //dd($api_bt_data);
	    $data['login_data']='';
	    if($api_bt_data['ResponseCode']=='YES'){
	    $data['login_data']=$api_bt_data['ResponseMessage'];
	    }

       //get transaction fees. 
       // $client_record_id = $_SESSION['client_record_id'];
       $arr=[
         'method'=>'get_convenience_fee',
            'P01'=> $data['login_data']['SessionID'],
            'P02'=> $api_fee_data['ResponseMessage']['merchant_client_id'],
       ];

       // $arr_tosend['url']="login_business?".http_build_query($arr);

       //dd($arr);
       //print_r($arr);die;
       $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
       $api_bt_data=json_decode($api_bt_result,true);
       //dd($api_bt_result);
       //var_dump($api_bt_result);die;
       $data['transaction_fee_data']='';
       if($api_bt_data['Success']=='YES'){
       $api_bt_data['ResponseMessage']=isset($api_bt_data['ResponseMessage']) ? $api_bt_data['ResponseMessage'] : '';
       $transaction_fee_data=$api_bt_data['ResponseMessage'];
       } 
       $amount =str_replace(',', '', $api_fee_data['ResponseMessage']['amount']); //? $_POST['amount']  : $this->session->userdata('post_data')['Amount'];
       //dd($amount);
       $total= 0.00;
       $pf=0.00;
       $tf=0.00;
       $upf=0.00;
       $utf=0.00;
       $isenable='';
	     if(!empty($transaction_fee_data)){
	       	   $isenable=$transaction_fee_data['is_enable_cc_fee'];
		       if($transaction_fee_data['is_enable_cc_fee']==1){
		       		$gtot =number_format($amount,2,'.','')+$api_fee_data['ResponseMessage']['sunpass_fee']+$api_fee_data['ResponseMessage']['facility_fee'];

			       if($transaction_fee_data['cc_processing_fee_isfixed']==1){//fixed
			       		//$fee+= $transaction_fee_data['cc_processing_fee'];
			       		$pf=$transaction_fee_data['cc_processing_fee'];
			       		$upf=$transaction_fee_data['cc_processing_fee'];
			       		//$total += floatval($amount)+floatval($fee);
			       } else if($transaction_fee_data['cc_processing_fee_isfixed']==0){
			       		$percentage= $transaction_fee_data['cc_processing_fee']/100;
			       		$total_percent=number_format($gtot,2,'.','')*number_format($percentage,2,'.','');
			       		//echo $amount;
			       		//echo $fee;
			       		$pf=$total_percent;
			       		$upf=$total_percent;
			       }
			       //apply fee...
			       if($transaction_fee_data['transaction_fee_isfixed']==1){//fixed
			       		//$fee+= $transaction_fee_data['transaction_fee'];
			       		$tf=$transaction_fee_data['transaction_fee'];
			       		$utf=$transaction_fee_data['transaction_fee'];
			       		//$total += floatval($amount)+floatval($fee);
			       } else if($transaction_fee_data['transaction_fee_isfixed']==0){//percent
			       		$percentage=$transaction_fee_data['transaction_fee']/100;
			       		$total_percent=number_format($gtot,2,'.','')*number_format($percentage,2,'.','');
			       		//echo $fee."<br>";
			       		$tf=$total_percent;
			       		$utf=$total_percent;
			       }
		       } else {
		       		//
		       		$isenable=$transaction_fee_data['is_enable_cc_fee'];
		       		$gtot =$amount+$api_fee_data['ResponseMessage']['sunpass_fee']+$api_fee_data['ResponseMessage']['facility_fee'];

			       if($transaction_fee_data['cc_processing_fee_isfixed']==1){//fixed
			       		//$fee+= $transaction_fee_data['cc_processing_fee'];
			       		$upf=$transaction_fee_data['cc_processing_fee'];
			       		//$total += floatval($amount)+floatval($fee);
			       } else if($transaction_fee_data['cc_processing_fee_isfixed']==0){
			       		$percentage= $transaction_fee_data['cc_processing_fee']/100;
			       		$total_percent=number_format($gtot,2,'.','')*number_format($percentage,2,'.','');
			       		//echo $amount;
			       		//echo $fee;
			       		$upf=$total_percent;
			       }
			       //apply fee...
			       if($transaction_fee_data['transaction_fee_isfixed']==1){//fixed
			       		//$fee+= $transaction_fee_data['transaction_fee'];
			       		$utf=$transaction_fee_data['transaction_fee'];
			       		//$total += floatval($amount)+floatval($fee);
			       } else if($transaction_fee_data['transaction_fee_isfixed']==0){//percent
			       		$percentage=$transaction_fee_data['transaction_fee']/100;
			       		$total_percent=number_format($gtot,2,'.','')*number_format($percentage,2,'.','');
			       		//echo $fee."<br>";
			       		$utf=$total_percent;
			       }
		       }
	       }

		//dd($post_data);
		//detect if ios af.
		$this->load->library('Mobile_Detect');
		$pre_total = $post_data['Amount']-$post_data['discount'];
		if($pre_total<=0){
			$pre_total=0.00;
        }
        // $api_fee_data['ResponseMessage']['iscard_fee_on_merch']=1;//default to 1 charge to merch card fee
		if($api_fee_data['ResponseMessage']['iscard_fee_on_merch']==1){ //1 means disabled and convenience fee should be charged to  merchant/customer merchant account
			$grand_total =$pre_total+$post_data['sunpass_fee']+$post_data['facility_fee'];
		} else {
			$grand_total =$pre_total+$post_data['sunpass_fee']+$post_data['facility_fee']+number_format($upf,2,'.','')+number_format($utf,2,'.','');
		}
		$grand_total = number_format($grand_total, 2, '.', '');
		//dd($this->mobile_detect);
		$arr=[];

		if($grand_total <= 0){

			$arr=[	
				'msg'=>'Amount should be greather than 0',
				'success'=>false
			];
			
			echo json_encode($arr);
			exit;

		} 

		$arr=[
		'amount'=>$grand_total,
		'type'=>'Sale',
		'invoicenumber'=>$post_data['OrderID'],
		'email'=>'suncashme@gmail.com',
		'secretkey'=>CENPOST_MERCHANT_SECRETKEY,
		'merchant'=>CENPOST_MERCHANT_ID,
		'tokenid'=>$_POST['tokenid'],
		'isrecaptcha'=>false,
		];
		$verifying_post='';
		$verify_post1=$this->assetHelper->api_requestv2(CENPOS_SITEVERIFY_URL,$arr);
		//dd($verify_post2);
		$verify_post=json_decode($verify_post1,true);
		//result verify
		if($verify_post['Result']==0){
			$arr_v1t=[
				'method'=>'create_suncashme_checkout_log',
				'P01'=>$post_data['MerchantKey'],//MerchantKey
				'P02'=>$post_data['OrderID'],//order_id
				'P03'=>'',//reference_number
				'P04'=>'cenpos',//type
				'P05'=>'success',//response
				'P06'=>json_encode($arr),//response
				'P07'=>$verify_post1,//response
				'P08'=>$verify_post['Message'],//response
				'return_mode'=>'json'	
			];
			$save_log1s=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_v1t);

			$verifying_post=$verify_post['Data'];

			$arr_post2=[
				'verifyingpost'=>$verify_post['Data'],
				'tokenid'=>$_POST['tokenid'],
			];
			//dd($arr_post2);
			$process_payment1=$this->assetHelper->api_requestv2(CENPOS_PROCESS_URL,$arr_post2);
			$process_payment=json_decode($process_payment1,true);


			if($process_payment['Result']==0){//

				//update status ..
				$arr_update=[
					'method'=>'update_checkout_transaction',
					'P01'=>$post_data['OrderReference'],//reference
					'P02'=>'PROCESSED',//status Reserved pending payments in cenpos
					'P03'=>$process_payment['ReferenceNumber'],
					'return_mode'=>'json'
				];
				//  dd($arr_update);
				$update_status=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr_update);

				//update fees and total via api ....
				$arr_update_fee=[
					'method'=>'update_checkout_transaction_fee',
					'P01'=>$_POST['r'],//reference 
					'P02'=>number_format($upf,2,'.',''),//status 
					'P03'=>number_format($utf,2,'.',''),//status 
					'P04'=>$grand_total,//$total,//status 
					'P05'=>'0.00',//number_format($_POST['discount'],2,'.',''),//status 
					'P06'=>'0.00',//$_POST['promo_code'],//status 
					'P07'=>$api_fee_data['ResponseMessage']['iscard_fee_on_merch'],
					'return_mode'=>'json'	
				];
				// dd($arr_update_fee);
				$update_status2=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr_update_fee);
    
				$arr_post3=[
					'method'=>'create_suncashme_cenpos_transaction_detail',
					'P01'=>$api_fee_data['ResponseMessage']['merchant_key'],//MerchantKey
					'P02'=>$api_fee_data['ResponseMessage']['order_id'],//order_id
					'P03'=>$process_payment['ReferenceNumber'],//reference_number
					'P04'=>$_POST['card_number'],//type
					'P05'=>$_POST['name_on_card'],//response
					'P06'=>$_POST['card_type'],//response
					'P07'=>number_format($post_data['Amount'],2,'.',''),//response
					'P08'=>number_format($upf,2,'.',''),//response,//response
					'P09'=>number_format($utf,2,'.',''),//response,//response
					'P10'=>number_format($utf,2,'.','')+number_format($upf,2,'.',''),//totall 
					'P11'=>0.00,//forsuncash
					'P12'=>0.0,//for ticketing only
					'P13'=>0.00,//billpay fee
					'P14'=>0.00,//vat
					'P15'=>'checkout',//source checkout
					'P16'=>0.00,//source checkout
					'P17'=>$api_fee_data['ResponseMessage']['iscard_fee_on_merch'],
					'P18'=>'',//notes
					'P19'=>'',
					'P20'=>'',
					'P21'=>'',
					'return_mode'=>'json'
				];

				$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post3);
				//saving logs....
				$arr_post_check_log=[
					'method'=>'create_suncashme_checkout_log',
					'P01'=>$post_data['MerchantKey'],//MerchantKey
					'P02'=>$post_data['OrderID'],//order_id
					'P03'=>$process_payment['ReferenceNumber'],//reference_number
					'P04'=>'cenpos',//type
					'P05'=>'success',//response
					'P06'=>json_encode($arr_post2),//response
					'P07'=>$process_payment1,//response
					'P08'=>$process_payment['Message'],//response
					'return_mode'=>'json'
				];
				$save_logs2=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post_check_log);
				//saving logs....
				$arr_post_logs=[
					'method'=>'create_suncashme_checkout_log',
					'P01'=>$post_data['MerchantKey'],//MerchantKey
					'P02'=>$post_data['OrderID'],//order_id
					'P03'=>$process_payment['ReferenceNumber'],//reference_number
					'P04'=>'cenpos',//type
					'P05'=>'success',//response
					'P06'=>json_encode($arr_post3),//response
					'P07'=>$save_logs,//response
					'P08'=>'Guest Sale Successful',//response
					'return_mode'=>'json'
				];
				$save_log3=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post_logs);

				//email merchant process payment	
				$arr=[
				'method'=>'get_suntag_shortcode',
				'P01'=>$post_data['MerchantKey'],//$this->session->userdata('SessionID'),
				'return_mode' => 'json'
				];
				$api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
				$api_bt_data=json_decode($api_bt_result,true);	
				// dd($api_bt_result);
				$data['suntag_data']='';
				if($api_bt_data['Success']=='YES'){
				$data['suntag_data']=$api_bt_data['ResponseMessage'];
				}else{
						$arr=[	
							'msg'=>'Something went wrong!',
							'success'=>false
						];
						echo json_encode($arr);
						exit;
				}  
				$cfee_email = $post_data['iscard_fee_on_merch'] ==1 ? $cfee_email=0.00 : $cfee_email=number_format($upf,2,'.','')+number_format($utf,2,'.','');
				$arr=[
					"customer_name"=>$_POST['card_number'],
					"amount"=>number_format($post_data['Amount'],2,'.',''),
					"transaction_id"=>$post_data['OrderID'],//cenpos
					"dba_name"=>$data['suntag_data']['dba_name'],
					"creation_date"=>date('M d, Y'),
					"reference_num"=>$process_payment['ReferenceNumber'],
					"notes"=>'',
					"balance"=>'',
					"processing_fee"=>number_format($upf,2,'.',''),
					"transaction_fee"=>number_format($utf,2,'.',''),
					"fee"=>0.00,
					"vat"=>0.00,
					"cfee"=>$cfee_email,
					"tfee"=>0.00,
					"total"=>number_format($grand_total,2),  //amount +fee+vat
					"msg_above"=>"<h2>".$_POST['card_number']." paid <span class='amount'>".number_format($grand_total,2)." BSD</span>. </h2>",
					"msg_bottom"=>"",
					"email"=>$data['suntag_data']['business_email_address'],
					"view"=>'email_message_completed.blade.php',
					"title"=>'Completed Payment',
					"profile_pic" => $this->session->userdata("profile_pic"),
					"iscard_fee_on_merch"=>$post_data['iscard_fee_on_merch']
				];
				// dd($arr);
				$this->confirm_email($arr);


				unset($_SESSION['post_data']);
				$reference_num = $process_payment['ReferenceNumber']."||".$_POST['r'];
		

				$params=base64_encode($reference_num."||success||card");
				$arr=[
					'msg'=>$process_payment['Message'],
					'reference'=>$process_payment['ReferenceNumber'],
					'url'=>$post_data["CallbackURL"].$params,
					'payment_method'=>'Card',
					'success'=>true
				];

				echo json_encode($arr);
			} else {
				//saving logs....
				$arr_post=[
					'method'=>'create_suncashme_checkout_log',
					'P01'=>$post_data['MerchantKey'],//MerchantKey
					'P02'=>$post_data['OrderID'],//order_id
					'P03'=>'',//reference_number
					'P04'=>'cenpos',//type
					'P05'=>'failed',//response
					'P06'=>json_encode($arr_post2),//response
					'P07'=>$process_payment1,//response
					'P08'=>$process_payment['Message'],//response
					'return_mode'=>'json'
				];
				$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);

				//unset($_SESSION['post_data']);

				$arr=[
					'msg'=>$process_payment['Message'],
					'success'=>false
				];
				echo json_encode($arr);
			}
		} else{///
			$arr_v1t=[
				'method'=>'create_suncashme_checkout_log',
				'P01'=>$post_data['MerchantKey'],//MerchantKey
				'P02'=>$post_data['OrderID'],//order_id
				'P03'=>'',//reference_number
				'P04'=>'cenpos',//type
				'P05'=>'failed',//response
				'P06'=>json_encode($arr),//response
				'P07'=>$verify_post1,//response
				'P08'=>$verify_post['Message'],//response
				'return_mode'=>'json'
			];
			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_v1t);

			$arr=[
				'msg'=>$verify_post['Message'],
				'data'=>$verify_post,
				'success'=>false
			];

			echo json_encode($arr);
			exit;
		}


	}
	public function process_tokenization(){
		//dd($_POST);
			if(empty($_POST['ccwu'])){

				$arr=[
					'msg'=>'Please select/choose card first.',
					'success'=>false
				];
				echo json_encode($arr);
				exit;
			}
			$_POST['ccwu_decoded'] = base64_decode($_POST['ccwu']);
			//recall api staging on saving..
			$arr=[
				'method'=>'get_client_checkout_details',
				'P01'=>$_POST['m'],
				'P02'=>$_POST['r'],
				'return_mode'=>'json'
			];
			$api_fee_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
			//dd($api_fee_result);
			$api_fee_data=json_decode($api_fee_result,true);
			//dd($api_fee_result);		
			if($api_fee_data['Success']=='YES'){
			//check if pending...
			if($api_fee_data['ResponseMessage']['status']!='PENDING'){
				redirect('');
			}
			//process order as individual array
			$ItemName=[];
			$ItemQty=[];
			$ItemPrice=[];
			if(!empty($api_fee_data['ResponseMessage']['details'])){
				for ($i=0; $i <count($api_fee_data['ResponseMessage']['details']) ; $i++) {
					# code...
					$ItemName[$i]=$api_fee_data['ResponseMessage']['details'][$i]['item_name'];
					$ItemQty[$i]=$api_fee_data['ResponseMessage']['details'][$i]['qty'];
					$ItemPrice[$i]=$api_fee_data['ResponseMessage']['details'][$i]['price'];
				}
			}
			$return_data=[
			'trans_type'=>$api_fee_data['ResponseMessage']['trans_type'],//Auth or Sale
			];
		}
		$post_data =$return_data;



		if(Ucfirst($api_fee_data['ResponseMessage']['trans_type'])=='Sale'){
			//die("sale");
			$this->process_tokken_sale();
		}else if(Ucfirst($api_fee_data['ResponseMessage']['trans_type'])=='Auth'){
			//die("auth");
			$this->process_tokken_auth();
		}else{
			//die("nothing");
			$arr=[
				'msg'=>'Invalid Transaction Type',
				'success'=>false
			];
			echo json_encode($arr);
		}

	}
	//FOR SALE tokenized payments
	private function process_tokken_sale(){//for tokenized payment
		//recall api staging on saving..
		$arr=[
			'method'=>'get_client_checkout_details',
			'P01'=>$_POST['m'],
			'P02'=>$_POST['r'],
			'return_mode'=>'json'
		];
	    $api_fee_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
	    //dd($api_fee_result);
	    $api_fee_data=json_decode($api_fee_result,true);
	    //dd($api_fee_result);		
	    if($api_fee_data['Success']=='YES'){
	   	//check if pending...
 		if($api_fee_data['ResponseMessage']['status']!='PENDING'){
 	   		redirect('');
 	   	}
	    //process order as individual array
	    $ItemName=[];
	    $ItemQty=[];
	    $ItemPrice=[];
	    if(!empty($api_fee_data['ResponseMessage']['details'])){
	    	for ($i=0; $i <count($api_fee_data['ResponseMessage']['details']) ; $i++) {
	    		# code...
			    $ItemName[$i]=$api_fee_data['ResponseMessage']['details'][$i]['item_name'];
			    $ItemQty[$i]=$api_fee_data['ResponseMessage']['details'][$i]['qty'];
			    $ItemPrice[$i]=$api_fee_data['ResponseMessage']['details'][$i]['price'];
	    	}
	    }
		$return_data=[
		'merchant_client_id'=>$api_fee_data['ResponseMessage']['merchant_client_id'],
		'MerchantKey'=>$api_fee_data['ResponseMessage']['merchant_key'],
		'MerchantName'=>$api_fee_data['ResponseMessage']['merchant_name'],
		'Amount'=>$api_fee_data['ResponseMessage']['amount'],
		'total_amount'=>$api_fee_data['ResponseMessage']['total_amount'],
		'OrderID'=>$api_fee_data['ResponseMessage']['order_id'],
		'CallbackURL'=>$api_fee_data['ResponseMessage']['callback_url'],
		'ItemName'=>$ItemName,
		'ItemQty'=>$ItemQty,
		'ItemPrice'=>$ItemPrice,
		'OrderReference'=>$_POST['r'],
		'profile_pic'=>$api_fee_data['ResponseMessage']['profile_pic'],
		'reference_id'=>$_POST['r'],
		'sunpass_fee'=>$api_fee_data['ResponseMessage']['sunpass_fee'],
		'facility_fee'=>$api_fee_data['ResponseMessage']['facility_fee'],
		'processing_fee'=>$api_fee_data['ResponseMessage']['processing_fee'],
		'transaction_fee'=>$api_fee_data['ResponseMessage']['transaction_fee'],
		'discount'=>$api_fee_data['ResponseMessage']['discount'],
		'iscard_fee_on_merch'=>$api_fee_data['ResponseMessage']['iscard_fee_on_merch'],
		//'auth',
		];
		}

		$post_data =$return_data;		
		//backdoor conveniene fee
		$arr=[
	    	'method'=>'login_business',
	        'P01'=> SERVICE_ACCOUNT_USERNAME_DEV,
	        'P02'=> SERVICE_ACCOUNT_PASSWORD_DEV,
	    ];
	    //$arr_tosend['url']="login_business?".http_build_query($arr);

	    //dd($arr_tosend);
	    $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
	    $api_bt_data=json_decode($api_bt_result,true);
	    //dd($api_bt_data);
	    $data['login_data']='';
	    if($api_bt_data['ResponseCode']=='YES'){
	    $data['login_data']=$api_bt_data['ResponseMessage'];
	    }

       //get transaction fees. 
       // $client_record_id = $_SESSION['client_record_id'];
       $arr=[
         'method'=>'get_convenience_fee',
            'P01'=> $data['login_data']['SessionID'],
            'P02'=> $api_fee_data['ResponseMessage']['merchant_client_id'],
       ];

       // $arr_tosend['url']="login_business?".http_build_query($arr);

       //dd($arr);
       //print_r($arr);die;
       $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
       $api_bt_data=json_decode($api_bt_result,true);
       //dd($api_bt_result);
       //var_dump($api_bt_result);die;
       $data['transaction_fee_data']='';
       if($api_bt_data['Success']=='YES'){
       $api_bt_data['ResponseMessage']=isset($api_bt_data['ResponseMessage']) ? $api_bt_data['ResponseMessage'] : '';
       $transaction_fee_data=$api_bt_data['ResponseMessage'];
       } 
       $amount =str_replace(',', '', $api_fee_data['ResponseMessage']['amount']); //? $_POST['amount']  : $this->session->userdata('post_data')['Amount'];
       //dd($amount);
       $total= 0.00;
       $pf=0.00;
       $tf=0.00;
       $upf=0.00;
       $utf=0.00;
       $isenable='';
       //$transaction_fee_data['is_enable_cc_fee']=1;
	     if(!empty($transaction_fee_data)){
	       	   $isenable=$transaction_fee_data['is_enable_cc_fee'];
		       if($transaction_fee_data['is_enable_cc_fee']==1){
		       		$gtot =number_format($amount,2,'.','')+$api_fee_data['ResponseMessage']['sunpass_fee']+$api_fee_data['ResponseMessage']['facility_fee'];

			       if($transaction_fee_data['cc_processing_fee_isfixed']==1){//fixed
			       		//$fee+= $transaction_fee_data['cc_processing_fee'];
			       		$pf=$transaction_fee_data['cc_processing_fee'];
			       		$upf=$transaction_fee_data['cc_processing_fee'];
			       		//$total += floatval($amount)+floatval($fee);
			       } else if($transaction_fee_data['cc_processing_fee_isfixed']==0){
			       		$percentage= $transaction_fee_data['cc_processing_fee']/100;
			       		$total_percent=number_format($gtot,2,'.','')*number_format($percentage,2,'.','');
			       		//echo $amount;
			       		//echo $fee;
			       		$pf=$total_percent;
			       		$upf=$total_percent;
			       }
			       //apply fee...
			       if($transaction_fee_data['transaction_fee_isfixed']==1){//fixed
			       		//$fee+= $transaction_fee_data['transaction_fee'];
			       		$tf=$transaction_fee_data['transaction_fee'];
			       		$utf=$transaction_fee_data['transaction_fee'];
			       		//$total += floatval($amount)+floatval($fee);
			       } else if($transaction_fee_data['transaction_fee_isfixed']==0){//percent
			       		$percentage=$transaction_fee_data['transaction_fee']/100;
			       		$total_percent=number_format($gtot,2,'.','')*number_format($percentage,2,'.','');
			       		//echo $fee."<br>";
			       		$tf=$total_percent;
			       		$utf=$total_percent;
			       }
		       } else {
		       		//
		       		$isenable=$transaction_fee_data['is_enable_cc_fee'];
		       		$gtot =$amount+$api_fee_data['ResponseMessage']['sunpass_fee']+$api_fee_data['ResponseMessage']['facility_fee'];

			       if($transaction_fee_data['cc_processing_fee_isfixed']==1){//fixed
			       		//$fee+= $transaction_fee_data['cc_processing_fee'];
			       		$upf=$transaction_fee_data['cc_processing_fee'];
			       		//$total += floatval($amount)+floatval($fee);
			       } else if($transaction_fee_data['cc_processing_fee_isfixed']==0){
			       		$percentage= $transaction_fee_data['cc_processing_fee']/100;
			       		$total_percent=number_format($gtot,2,'.','')*number_format($percentage,2,'.','');
			       		//echo $amount;
			       		//echo $fee;
			       		$upf=$total_percent;
			       }
			       //apply fee...
			       if($transaction_fee_data['transaction_fee_isfixed']==1){//fixed
			       		//$fee+= $transaction_fee_data['transaction_fee'];
			       		$utf=$transaction_fee_data['transaction_fee'];
			       		//$total += floatval($amount)+floatval($fee);
			       } else if($transaction_fee_data['transaction_fee_isfixed']==0){//percent
			       		$percentage=$transaction_fee_data['transaction_fee']/100;
			       		$total_percent=number_format($gtot,2,'.','')*number_format($percentage,2,'.','');
			       		//echo $fee."<br>";
			       		$utf=$total_percent;
			       }
		       }
	       }

		//dd($post_data);
		//detect if ios af.
		$this->load->library('Mobile_Detect');
		$pre_total = $post_data['Amount']-$post_data['discount'];
		if($pre_total<=0){
			$pre_total=0.00;
        }
        // $api_fee_data['ResponseMessage']['iscard_fee_on_merch']=1;//default to 1 charge to merch card fee
		if($api_fee_data['ResponseMessage']['iscard_fee_on_merch']==1){ //1 means disabled and convenience fee should be charged to  merchant/customer merchant account
			$grand_total =$pre_total+$post_data['sunpass_fee']+$post_data['facility_fee'];
		} else {
			$grand_total =$pre_total+$post_data['sunpass_fee']+$post_data['facility_fee']+number_format($upf,2,'.','')+number_format($utf,2,'.','');
		}


		$grand_total = number_format($grand_total, 2, '.', '');
		// dd($grand_total);
		//dd($this->mobile_detect);
		$arr=[];
		//get card info
		$arr_get=[
			'method'=>'get_merchant_customercard_details',
			'P01'=>$post_data['MerchantKey'],//MerchantKey,
			'P02'=>$_POST['ccwu_decoded'],
			'return_mode'=>'json'
		];
		// dd($arr_get);
		$get_linkedcard_info=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr_get);
		// dd($get_linkedcard_info);
		$get_linkedcard_info_data=json_decode($get_linkedcard_info,true);  
		//dd($get_linkedcard_info_data);
		//process verify post.

		if($grand_total <= 0){

			$arr=[	
				'msg'=>'Amount should be greather than 0',
				'success'=>false
			];
			
			echo json_encode($arr);
			exit;

		} 

		$arr=[
		'amount'=>$grand_total,
		'type'=>Ucfirst($api_fee_data['ResponseMessage']['trans_type']), //SALE
		'invoicenumber'=>$post_data['OrderID'],
		'secretkey'=>CENPOST_MERCHANT_SECRETKEY,
		'merchant'=>CENPOST_MERCHANT_ID,
		'customercode'=>$get_linkedcard_info_data['ResponseMessage']['merchant_customer_id'],
		'tokenid'=>$get_linkedcard_info_data['ResponseMessage']['token_id'],
		];
		$verifying_post='';
		$verify_post1=$this->assetHelper->api_requestv2(CENPOS_SITEVERIFY_URL,$arr);
		//dd($verify_post2);
		$verify_post=json_decode($verify_post1,true);
		//result verify
		if($verify_post['Result']==0){
			$arr_post1=[
				'method'=>'create_suncashme_checkout_log',
				'P01'=>$post_data['MerchantKey'],//MerchantKey
				'P02'=>$post_data['OrderID'],//order_id
				'P03'=>'',//reference_number
				'P04'=>'cenpos',//type
				'P05'=>'success',//response
				'P06'=>json_encode($arr),//response
				'P07'=>$verify_post1,//response
				'P08'=>$verify_post['Message'],//response
				'return_mode'=>'json'	
			];
			$save_logs1=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post1);
			$verifying_post=$verify_post['Data'];

			$arr_post2=[
				'verifyingpost'=>$verify_post['Data'],
				'tokenid'=>$get_linkedcard_info_data['ResponseMessage']['token_id'],
			];
			//dd($arr_post2);
			$process_payment1=$this->assetHelper->api_requestv2(CENPOS_PROCESS_URL,$arr_post2);
			$process_payment=json_decode($process_payment1,true);


			if($process_payment['Result']==0){//

				//update status ..
				$arr_update=[
					'method'=>'update_checkout_transaction',
					'P01'=>$post_data['OrderReference'],//reference
					'P02'=>'PROCESSED',//status
					'P03'=>$process_payment['ReferenceNumber'],//for reference in sale
					'return_mode'=>'json'
				];
				// dd($arr_update);
				$update_status=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr_update);

				//update fees and total via api ....
				$arr_update_fee=[
					'method'=>'update_checkout_transaction_fee',
					'P01'=>$_POST['r'],//reference 
					'P02'=>number_format($upf,2,'.',''),//status 
					'P03'=>number_format($utf,2,'.',''),//status 
					'P04'=>$grand_total,//$total,//status 
					'P05'=>'0.00',//number_format($_POST['discount'],2,'.',''),//status 
					'P06'=>'0.00',//$_POST['promo_code'],//status 
					'P07'=>$api_fee_data['ResponseMessage']['iscard_fee_on_merch'],
					'return_mode'=>'json'	
				];
				// dd($arr_update_fee);
				$update_status2=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr_update_fee);
				// dd($update_status2);
				$arr_post_save=[
					'method'=>'create_suncashme_cenpos_transaction_detail',
					'P01'=>$post_data['MerchantKey'],//MerchantKey
					'P02'=>$post_data['OrderID'],//order_id
					'P03'=>$process_payment['ReferenceNumber'],//reference_number
					'P04'=>$get_linkedcard_info_data['ResponseMessage']['card_last_four_digits'],//card_last_four_digits
					'P05'=>$get_linkedcard_info_data['ResponseMessage']['cardholder_name'],//name
					'P06'=>$get_linkedcard_info_data['ResponseMessage']['card_type'],//card_type
					'P07'=>number_format($post_data['Amount'],2,'.',''),//response
					'P08'=>number_format($upf,2,'.',''),//response,//response
					'P09'=>number_format($utf,2,'.',''),//response,//response
					'P10'=>number_format($post_data['transaction_fee']+$post_data['processing_fee']+$post_data['sunpass_fee']+$post_data['facility_fee'],2,'.',''),//totall of all fee master fee tne fee
					'P11'=>0.00,//sunpass_fee
					'P12'=>0.00,//facility_fee
					'P13'=>0.00,//billpay fee
					'P14'=>0.00,//vat
					'P15'=>'checkout',//source checkout
					'P16'=>0.00,//discount
					'P17'=>$api_fee_data['ResponseMessage']['iscard_fee_on_merch'],
					'P18'=>'',//notes
					'P19'=>'',//$_POST['name_customer_card'],
					'P20'=>'',//$_POST['email_card'],
					'P21'=>'',//$this->clean($_POST['mobile_card']),
					'return_mode'=>'json'
				];
				// dd($arr_post);
				$save_logs2=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post_save);
				// dd($save_logs);

				//saving logs....
				$arr_post3=[
					'method'=>'create_suncashme_checkout_log',
					'P01'=>$post_data['MerchantKey'],//MerchantKey
					'P02'=>$post_data['OrderID'],//order_id
					'P03'=>$process_payment['ReferenceNumber'],//reference_number
					'P04'=>'cenpos',//type
					'P05'=>'success',//response
					'P06'=>json_encode($arr_post2),//response
					'P07'=>$process_payment1,//response
					'P08'=>$process_payment['Message'],//response
					'return_mode'=>'json'
				];
				$save_logs3=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post3);
				//saving logs....
				$arr_post4=[
					'method'=>'create_suncashme_checkout_log',
					'P01'=>$post_data['MerchantKey'],//MerchantKey
					'P02'=>$post_data['OrderID'],//order_id
					'P03'=>$process_payment['ReferenceNumber'],//reference_number
					'P04'=>'cenpos',//type
					'P05'=>'success',//response
					'P06'=>json_encode($arr_post_save),//response
					'P07'=>$save_logs2,//response
					'P08'=>'Tokennized Sale Successful',//response
					'return_mode'=>'json'
				];
				$save_logs4=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post4);

				$arr_update_fee=[
					'method'=>'update_checkout_transaction_fee',
					'P01'=>$_POST['r'],//reference 
					'P02'=>number_format($upf,2,'.',''),//status 
					'P03'=>number_format($utf,2,'.',''),//status 
					'P04'=>$grand_total,//$total,
					'P05'=>'0.00',//number_format($_POST['discount'],2,'.',''),//status 
					'P06'=>'0.00',//$_POST['promo_code'],//status 
					'P07'=>$api_fee_data['ResponseMessage']['iscard_fee_on_merch'],
					'return_mode'=>'json'	
				];

				$update_status=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr_update);
				$cfee_email = $api_fee_data['ResponseMessage']['iscard_fee_on_merch'] ==1 ? $cfee_email=0.00 : $cfee_email=number_format($upf,2,'.','')+number_format($utf,2,'.','');
				//email merchant process payment
				$arr=[
				'method'=>'get_suntag_shortcode',
				'P01'=>$post_data['MerchantKey'],//$this->session->userdata('SessionID'),
				'return_mode' => 'json'
				];
				$api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
				$api_bt_data=json_decode($api_bt_result,true);	
				// dd($api_bt_result);
				$data['suntag_data']='';
				if($api_bt_data['Success']=='YES'){
				$data['suntag_data']=$api_bt_data['ResponseMessage'];
				}else{
						$arr=[	
							'msg'=>'Something went wrong!',
							'success'=>false
						];
						echo json_encode($arr);
						exit;
				}  

				$arr=[
					"customer_name"=>$get_linkedcard_info_data['ResponseMessage']['cardholder_name'],
					"amount"=>number_format($post_data['Amount'],2,'.',''),
					"transaction_id"=>$post_data['OrderID'],//cenpos
					"dba_name"=>$data['suntag_data']['dba_name'],
					"creation_date"=>date('M d, Y'),
					"reference_num"=>$process_payment['ReferenceNumber'],
					"notes"=>'',
					"balance"=>'',
					"processing_fee"=>number_format($upf,2,'.',''),
					"transaction_fee"=>number_format($utf,2,'.',''),
					"fee"=>0.00,
					"vat"=>0.00,
					"cfee"=>$cfee_email,
					"tfee"=>0.00,
					"total"=>number_format($grand_total,2),  //amount +fee+vat
					"msg_above"=>"<h2>".$get_linkedcard_info_data['ResponseMessage']['cardholder_name']." paid <span class='amount'>".number_format($grand_total,2)." BSD</span>. </h2>",
					"msg_bottom"=>"",
					"email"=>$data['suntag_data']['business_email_address'],
					"view"=>'email_message_completed.blade.php',
					"title"=>'Completed Payment',
					"profile_pic" => $this->session->userdata("profile_pic"),
					"iscard_fee_on_merch"=>$post_data['iscard_fee_on_merch']
				];
				// dd($arr);
				$this->confirm_email($arr);

				unset($_SESSION['post_data']);
				//$reference_num=$process_payment['ReferenceNumber']
				//if(in_array($post_data['MerchantKey'],SUNPASS_MERCHANTS_KEYS)){
					$reference_num = $process_payment['ReferenceNumber']."||".$_POST['r'];
				//}

				$params=base64_encode($reference_num."||success||card");
				$arr=[
					'msg'=>$process_payment['Message'],
					'reference'=>$process_payment['ReferenceNumber'],
					'url'=>$post_data["CallbackURL"].$params,
					'payment_method'=>'Card',
					'success'=>true
				];

				echo json_encode($arr);
			} else {
				//saving logs....
				$arr_post=[
					'method'=>'create_suncashme_checkout_log',
					'P01'=>$post_data['MerchantKey'],//MerchantKey
					'P02'=>$post_data['OrderID'],//order_id
					'P03'=>'',//reference_number
					'P04'=>'cenpos',//type
					'P05'=>'failed',//response
					'P06'=>json_encode($arr_post2),//response
					'P07'=>$process_payment1,//response
					'P08'=>$process_payment['Message'],//response
					'return_mode'=>'json'
				];
				$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);

				//unset($_SESSION['post_data']);

				$arr=[
					'msg'=>$process_payment['Message'],
					'success'=>false
				];
				echo json_encode($arr);
			}
		} else{///
			$arr_post=[
				'method'=>'create_suncashme_checkout_log',
				'P01'=>$post_data['MerchantKey'],//MerchantKey
				'P02'=>$post_data['OrderID'],//order_id
				'P03'=>'',//reference_number
				'P04'=>'cenpos',//type
				'P05'=>'failed',//response
				'P06'=>json_encode($arr),//response
				'P07'=>$verify_post1,//response
				'P08'=>$verify_post['Message'],//response
				'return_mode'=>'json'
			];
			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);

			$arr=[
				'msg'=>$verify_post['Message'],
				'data'=>$verify_post,
				'success'=>false
			];

			echo json_encode($arr);
			exit;
		}			

	}
	//FOR AUTH tokenized payments
	private function process_tokken_auth(){//for tokenized payment
		//recall api staging on saving..
		$arr=[
			'method'=>'get_client_checkout_details',
			'P01'=>$_POST['m'],
			'P02'=>$_POST['r'],
			'return_mode'=>'json'
		];
	    $api_fee_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
	    //dd($api_fee_result);
	    $api_fee_data=json_decode($api_fee_result,true);
	    //dd($api_fee_result);		
	    if($api_fee_data['Success']=='YES'){
	   	//check if pending...
 		if($api_fee_data['ResponseMessage']['status']!='PENDING'){
 	   		redirect('');
 	   	}
	    //process order as individual array
	    $ItemName=[];
	    $ItemQty=[];
	    $ItemPrice=[];
	    if(!empty($api_fee_data['ResponseMessage']['details'])){
	    	for ($i=0; $i <count($api_fee_data['ResponseMessage']['details']) ; $i++) {
	    		# code...
			    $ItemName[$i]=$api_fee_data['ResponseMessage']['details'][$i]['item_name'];
			    $ItemQty[$i]=$api_fee_data['ResponseMessage']['details'][$i]['qty'];
			    $ItemPrice[$i]=$api_fee_data['ResponseMessage']['details'][$i]['price'];
	    	}
	    }
		$return_data=[
		'merchant_client_id'=>$api_fee_data['ResponseMessage']['merchant_client_id'],
		'MerchantKey'=>$api_fee_data['ResponseMessage']['merchant_key'],
		'MerchantName'=>$api_fee_data['ResponseMessage']['merchant_name'],
		'Amount'=>$api_fee_data['ResponseMessage']['amount'],
		'total_amount'=>$api_fee_data['ResponseMessage']['total_amount'],
		'OrderID'=>$api_fee_data['ResponseMessage']['order_id'],
		'CallbackURL'=>$api_fee_data['ResponseMessage']['callback_url'],
		'ItemName'=>$ItemName,
		'ItemQty'=>$ItemQty,
		'ItemPrice'=>$ItemPrice,
		'OrderReference'=>$_POST['r'],
		'profile_pic'=>$api_fee_data['ResponseMessage']['profile_pic'],
		'reference_id'=>$_POST['r'],
		'sunpass_fee'=>$api_fee_data['ResponseMessage']['sunpass_fee'],
		'facility_fee'=>$api_fee_data['ResponseMessage']['facility_fee'],
		'processing_fee'=>$api_fee_data['ResponseMessage']['processing_fee'],
		'transaction_fee'=>$api_fee_data['ResponseMessage']['transaction_fee'],
		'discount'=>$api_fee_data['ResponseMessage']['discount'],
		'iscard_fee_on_merch'=>$api_fee_data['ResponseMessage']['iscard_fee_on_merch'],
		//'auth',
		];
		}

		$post_data =$return_data;
		//backdoor conveniene fee
		$arr=[
	    	'method'=>'login_business',
	        'P01'=> SERVICE_ACCOUNT_USERNAME_DEV,
	        'P02'=> SERVICE_ACCOUNT_PASSWORD_DEV,
	    ];
	    //$arr_tosend['url']="login_business?".http_build_query($arr);

	    //dd($arr_tosend);
	    $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
	    $api_bt_data=json_decode($api_bt_result,true);
	    //dd($api_bt_data);
	    $data['login_data']='';
	    if($api_bt_data['ResponseCode']=='YES'){
	    $data['login_data']=$api_bt_data['ResponseMessage'];
	    }

       //get transaction fees. 
       // $client_record_id = $_SESSION['client_record_id'];
       $arr=[
         'method'=>'get_convenience_fee',
            'P01'=> $data['login_data']['SessionID'],
            'P02'=> $api_fee_data['ResponseMessage']['merchant_client_id'],
       ];

       // $arr_tosend['url']="login_business?".http_build_query($arr);

       //dd($arr);
       //print_r($arr);die;
       $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
       $api_bt_data=json_decode($api_bt_result,true);
       //dd($api_bt_result);
       //var_dump($api_bt_result);die;
       $data['transaction_fee_data']='';
       if($api_bt_data['Success']=='YES'){
       $api_bt_data['ResponseMessage']=isset($api_bt_data['ResponseMessage']) ? $api_bt_data['ResponseMessage'] : '';
       $transaction_fee_data=$api_bt_data['ResponseMessage'];
       } 
       $amount =str_replace(',', '', $api_fee_data['ResponseMessage']['amount']); //? $_POST['amount']  : $this->session->userdata('post_data')['Amount'];
       //dd($amount);
       $total= 0.00;
       $pf=0.00;
       $tf=0.00;
       $upf=0.00;
       $utf=0.00;
       $isenable='';
       //$transaction_fee_data['is_enable_cc_fee']=1;
	     if(!empty($transaction_fee_data)){
	       	   $isenable=$transaction_fee_data['is_enable_cc_fee'];
		       if($transaction_fee_data['is_enable_cc_fee']==1){
		       		$gtot =number_format($amount,2,'.','')+$api_fee_data['ResponseMessage']['sunpass_fee']+$api_fee_data['ResponseMessage']['facility_fee'];

			       if($transaction_fee_data['cc_processing_fee_isfixed']==1){//fixed
			       		//$fee+= $transaction_fee_data['cc_processing_fee'];
			       		$pf=$transaction_fee_data['cc_processing_fee'];
			       		$upf=$transaction_fee_data['cc_processing_fee'];
			       		//$total += floatval($amount)+floatval($fee);
			       } else if($transaction_fee_data['cc_processing_fee_isfixed']==0){
			       		$percentage= $transaction_fee_data['cc_processing_fee']/100;
			       		$total_percent=number_format($gtot,2,'.','')*number_format($percentage,2,'.','');
			       		//echo $amount;
			       		//echo $fee;
			       		$pf=$total_percent;
			       		$upf=$total_percent;
			       }
			       //apply fee...
			       if($transaction_fee_data['transaction_fee_isfixed']==1){//fixed
			       		//$fee+= $transaction_fee_data['transaction_fee'];
			       		$tf=$transaction_fee_data['transaction_fee'];
			       		$utf=$transaction_fee_data['transaction_fee'];
			       		//$total += floatval($amount)+floatval($fee);
			       } else if($transaction_fee_data['transaction_fee_isfixed']==0){//percent
			       		$percentage=$transaction_fee_data['transaction_fee']/100;
			       		$total_percent=number_format($gtot,2,'.','')*number_format($percentage,2,'.','');
			       		//echo $fee."<br>";
			       		$tf=$total_percent;
			       		$utf=$total_percent;
			       }
		       } else {
		       		//
		       		$isenable=$transaction_fee_data['is_enable_cc_fee'];
		       		$gtot =$amount+$api_fee_data['ResponseMessage']['sunpass_fee']+$api_fee_data['ResponseMessage']['facility_fee'];

			       if($transaction_fee_data['cc_processing_fee_isfixed']==1){//fixed
			       		//$fee+= $transaction_fee_data['cc_processing_fee'];
			       		$upf=$transaction_fee_data['cc_processing_fee'];
			       		//$total += floatval($amount)+floatval($fee);
			       } else if($transaction_fee_data['cc_processing_fee_isfixed']==0){
			       		$percentage= $transaction_fee_data['cc_processing_fee']/100;
			       		$total_percent=number_format($gtot,2,'.','')*number_format($percentage,2,'.','');
			       		//echo $amount;
			       		//echo $fee;
			       		$upf=$total_percent;
			       }
			       //apply fee...
			       if($transaction_fee_data['transaction_fee_isfixed']==1){//fixed
			       		//$fee+= $transaction_fee_data['transaction_fee'];
			       		$utf=$transaction_fee_data['transaction_fee'];
			       		//$total += floatval($amount)+floatval($fee);
			       } else if($transaction_fee_data['transaction_fee_isfixed']==0){//percent
			       		$percentage=$transaction_fee_data['transaction_fee']/100;
			       		$total_percent=number_format($gtot,2,'.','')*number_format($percentage,2,'.','');
			       		//echo $fee."<br>";
			       		$utf=$total_percent;
			       }
		       }
	       }

		//dd($post_data);
		//detect if ios af.
		$this->load->library('Mobile_Detect');
		$pre_total = $post_data['Amount']-$post_data['discount'];
		if($pre_total<=0){
			$pre_total=0.00;
		}
		// dd($api_fee_data['ResponseMessage']['iscard_fee_on_merch']);
		// $api_fee_data['ResponseMessage']['iscard_fee_on_merch']=1;//default to 1 charge to merch card fee
		if($api_fee_data['ResponseMessage']['iscard_fee_on_merch']==1){ //1 means disabled and convenience fee should be charged to  merchant/customer merchant account
			$grand_total =$pre_total+$post_data['sunpass_fee']+$post_data['facility_fee'];
		} else {
			$grand_total =$pre_total+$post_data['sunpass_fee']+$post_data['facility_fee']+number_format($upf,2,'.','')+number_format($utf,2,'.','');
		}


		$grand_total = number_format($grand_total, 2, '.', '');
		//dd($this->mobile_detect);
		$arr=[];
		//get card info
		$arr_get=[
			'method'=>'get_merchant_customercard_details',
			'P01'=>$post_data['MerchantKey'],//MerchantKey,
			'P02'=>$_POST['ccwu_decoded'],
			'return_mode'=>'json'
		];
		// dd($arr_get);
		$get_linkedcard_info=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr_get);
		// dd($get_linkedcard_info);
		$get_linkedcard_info_data=json_decode($get_linkedcard_info,true);  
		// dd($get_linkedcard_info_data);
		//process verify post.
		//dd(get_user_agent());
		if($this->mobile_detect->isiOS() || $this->mobile_detect->isSafari() || $this->mobile_detect->isiPhone()){

		if($grand_total <= 0){

			$arr=[	
				'msg'=>'Amount should be greather than 0',
				'success'=>false
			];
			
			echo json_encode($arr);
			exit;

		} 

		$arr=[
			'amount'=>$grand_total,
			'invoicenumber'=>$post_data['OrderID'],
			'type'=>Ucfirst($api_fee_data['ResponseMessage']['trans_type']), //AUTH
			// 'email'=>'suncashme@gmail.com',
			'secretkey'=>CENPOST_MERCHANT_SECRETKEY,
			'merchant'=>CENPOST_MERCHANT_ID,
			'customercode'=>$get_linkedcard_info_data['ResponseMessage']['merchant_customer_id'],
			'tokenid'=>$get_linkedcard_info_data['ResponseMessage']['token_id'],
			];
		} else {
			$arr=[
			'amount'=>$grand_total,
			'invoicenumber'=>$post_data['OrderID'],
			'type'=>Ucfirst($api_fee_data['ResponseMessage']['trans_type']), //AUTH
			// 'email'=>'suncashme@gmail.com',
			'secretkey'=>CENPOST_MERCHANT_SECRETKEY,
			'merchant'=>CENPOST_MERCHANT_ID,
			'customercode'=>$get_linkedcard_info_data['ResponseMessage']['merchant_customer_id'],
			'tokenid'=>$get_linkedcard_info_data['ResponseMessage']['token_id'],
			];			
		}
		// $arr=[
		// 'amount'=>$grand_total,
		// 'type'=>$api_fee_data['ResponseMessage']['trans_type'], //AUTH
		// 'invoicenumber'=>$post_data['OrderID'],
		// 'secretkey'=>CENPOST_MERCHANT_SECRETKEY,
		// 'merchant'=>CENPOST_MERCHANT_ID,
		// 'customercode'=>$get_linkedcard_info_data['ResponseMessage']['merchant_customer_id'],
		// 'tokenid'=>$get_linkedcard_info_data['ResponseMessage']['token_id'],
		// ];
		$verifying_post='';
		$verify_post1=$this->assetHelper->api_requestv2(CENPOS_SITEVERIFY_URL,$arr);
		//dd($verify_post2);
		$verify_post=json_decode($verify_post1,true);
		//result verify
		if($verify_post['Result']==0){
			$arr_post=[
				'method'=>'create_suncashme_checkout_log',
				'P01'=>$post_data['MerchantKey'],//MerchantKey
				'P02'=>$post_data['OrderID'],//order_id
				'P03'=>'',//reference_number
				'P04'=>'cenpos',//type
				'P05'=>'success',//response
				'P06'=>json_encode($arr),//response
				'P07'=>$verify_post1,//response
				'P08'=>$verify_post['Message'],//response
				'return_mode'=>'json'	
			];
			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);
			$verifying_post=$verify_post['Data'];

			$arr_post2=[
				'verifyingpost'=>$verify_post['Data'],
				'tokenid'=>$get_linkedcard_info_data['ResponseMessage']['token_id'],
			];
			//dd($arr_post2);
			$process_payment1=$this->assetHelper->api_requestv2(CENPOS_PROCESS_URL,$arr_post2);
			$process_payment=json_decode($process_payment1,true);

			if($process_payment['Result']==0){//

				//update status ..
				$arr_update=[
					'method'=>'update_checkout_transaction',
					'P01'=>$post_data['OrderReference'],//reference
					'P02'=>'RESERVED',//status
					'P03'=>$process_payment['ReferenceNumber'],//for 
					'return_mode'=>'json'
				];
				// dd($arr_update);
				$update_status=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr_update);

				//update fees and total via api ....
				$arr_update_fee=[
					'method'=>'update_checkout_transaction_fee',
					'P01'=>$_POST['r'],//reference 
					'P02'=>number_format($upf,2,'.',''),//status 
					'P03'=>number_format($utf,2,'.',''),//status 
					'P04'=>$grand_total,//$total,//status 
					'P05'=>'0.00',//number_format($_POST['discount'],2,'.',''),//status 
					'P06'=>'0.00',//$_POST['promo_code'],//status 
					'P07'=>$api_fee_data['ResponseMessage']['iscard_fee_on_merch'],
					'return_mode'=>'json'	
				];
				// dd($arr_update_fee);
				$update_status2=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr_update_fee);
				
				$arr_update_ccinfo=[
					'method'=>'update_checkout_transaction_ccinfo',
					'P01'=>$_POST['r'],//reference 
					'P02'=>str_pad($get_linkedcard_info_data['ResponseMessage']['card_last_four_digits'], 16, "*", STR_PAD_LEFT),//type
					'P03'=>$get_linkedcard_info_data['ResponseMessage']['card_last_four_digits'],//response
					'P04'=>$get_linkedcard_info_data['ResponseMessage']['card_type'],//response
					'return_mode'=>'json'	
				];
				// dd($arr_update_ccinfo);
				$update_ccinfo=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr_update_ccinfo);       


				//saving logs....
				$arr_post_check_log=[
					'method'=>'create_suncashme_checkout_log',
					'P01'=>$post_data['MerchantKey'],//MerchantKey
					'P02'=>$post_data['OrderID'],//order_id
					'P03'=>$process_payment['ReferenceNumber'],//reference_number
					'P04'=>'cenpos',//type
					'P05'=>'success',//response
					'P06'=>json_encode($arr_post2),//response
					'P07'=>$process_payment1,//response
					'P08'=>$process_payment['Message'],//response
					'return_mode'=>'json'
				];
				$save_logs2=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post_check_log);

				unset($_SESSION['post_data']);

				$reference_num = $process_payment['ReferenceNumber']."||".$_POST['r'];
				$params=base64_encode($reference_num."||success||card");

				$arr=[
					'msg'=>$process_payment['Message'],
					'reference'=>$process_payment['ReferenceNumber'],
					'url'=>$post_data["CallbackURL"].$params,
					'payment_method'=>'Card',
					'success'=>true
				];

				echo json_encode($arr);
			} else {
				//saving logs....
				$arr_post=[
					'method'=>'create_suncashme_checkout_log',
					'P01'=>$post_data['MerchantKey'],//MerchantKey
					'P02'=>$post_data['OrderID'],//order_id
					'P03'=>'',//reference_number
					'P04'=>'cenpos',//type
					'P05'=>'failed',//response
					'P06'=>json_encode($arr_post),//response
					'P07'=>json_encode($process_payment),//response
					'P08'=>$process_payment['Message'],//response
					'return_mode'=>'json'
				];
				$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);

				//unset($_SESSION['post_data']);

				$arr=[
					'msg'=>$process_payment['Message'],
					'success'=>false
				];
				echo json_encode($arr);
			}
		} else{///
			$arr_post=[
				'method'=>'create_suncashme_checkout_log',
				'P01'=>$post_data['MerchantKey'],//MerchantKey
				'P02'=>$post_data['OrderID'],//order_id
				'P03'=>'',//reference_number
				'P04'=>'cenpos',//type
				'P05'=>'failed',//response
				'P06'=>json_encode($arr),//response
				'P07'=>$verify_post1,//response
				'P08'=>$verify_post['Message'],//response
				'return_mode'=>'json'
			];
			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);
			$arr=[
				'msg'=>$verify_post['Message'],
				'data'=>$verify_post,
				'success'=>false
			];
			echo json_encode($arr);
			exit;

		}
	}
	public function testurl(){//method then routefind //get fee
		$arr=[
			'merchant'=>CENPOST_MERCHANT_ID,
			'secretkey'=>CENPOST_MERCHANT_SECRETKEY,
			'email'=>'suncashme@gmail.com',
			'ip'=>'127.0.0.1',
		];
		$verify_post=$this->assetHelper->api_requestv2(CENPOS_SITEVERIFY_URL,$arr);
		$verify_post=json_decode($verify_post,true);
        $data['verify_params']=$verify_post;
        // $data='';
		$this->blade->view('testurl',$data);

	}	
	public function remove_card(){
		$id = base64_decode($this->input->post('_c'));
		$arr=[
			'method'=>'delete_customer_creditcard',
			'P01'=> $this->input->post('m'),
			'P02'=> $id,
			'return_mode' => 'json'
		  ];
		  //dd($arr);
		  $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
		  $api_bt_data=json_decode($api_bt_result,true);
		  //dd($api_bt_result);
		  $ccdata='';
		  if($api_bt_data['Success']=='YES'){
			 //$ccdata=$api_bt_data['ResponseMessage'];
			$arr=[
				'msg'=>$api_bt_data['ResponseMessage'],
				'success'=>true
			];
			echo json_encode($arr);
			exit;
          } else {
			$arr=[
				'msg'=>$api_bt_data['ResponseMessage'],
				'success'=>false
			];
			echo json_encode($arr);
			exit;
		  }
	}
	public function confirm_email($arr){
		// dd($arr);
		$to = $arr['email'];
		$subj = $arr['title'];
		$attachment_arr='';
		$template_html = $arr['view']; //views/templates/mail/
		//dd($template_html);
		//email receipt..
		$this->assetHelper->send_email($arr,$to,$subj,$template_html,$attachment_arr);
	}
	public function amazon_review_process(){

		//UPDATE SESSION response will redirect to defined checkoutReviewReturnUrl in payload create session

		$this->session->set_userdata('amz_mobile',$_POST['amazon_mobile']);
		//call fee here
	


		$checkoutSessionId = $this->session->userdata('amz_sess');
		$payload = array(
			'paymentDetails' => array(
				'paymentIntent' => 'AuthorizeWithCapture',
				'canHandlePendingAuthorization' => false,
				'chargeAmount' => array(
					'amount' => $_POST['total_due'],
					'currencyCode' => 'USD'
				),
			),
			'merchantMetadata' => array(
				'merchantReferenceId' => $_POST['dba_name'],//checkoutreferenceid
				'merchantStoreName' =>'SunCash Checkout',
				'noteToBuyer' => 'Thank you for your order!',
			)
		);

		try {
			$checkoutSessionId = $checkoutSessionId;
			$client = new Amazon\Pay\API\Client($this->amazonpay_config);
			$result = $client->updateCheckoutSession($checkoutSessionId, $payload);
			// dd($result);
			if ($result['status'] === 200) {
				$response = json_decode($result['response'], true);
				$amazonPayRedirectUrl = $response['webCheckoutDetails']['amazonPayRedirectUrl'];
				// echo "amazonPayRedirectUrl=$amazonPayRedirectUrl\n";
				$arr=[
					'msg'=>'success update.',
					'url'=>$amazonPayRedirectUrl,
					'success'=>true
				];
				echo json_encode($arr);
				exit;

			} else {
				$arr=[
					'msg'=>'status=' . $result['status'] . '; response=' . $result['response'] . "",
					'url'=>'',
					'success'=>false
				];
				echo json_encode($arr);
				exit;
			}
		} catch (\Exception $e) {
			// handle the exception
			$arr=[
				'msg'=>$e,
				'url'=>'',
				'success'=>false
			];
			echo json_encode($arr);
			exit;
		}
	}

	public function process_amazon_checkoutv2(){
		if(empty($_POST['amazon_email']) || empty($_POST['amazon_fullname'])){

			$arr=[	
				'msg'=>'Please fill up all fields',
				'success'=>false
			];
			
			echo json_encode($arr);
			exit;
		} 
		$arr=[
			'method'=>'get_client_checkout_details',
			'P01'=>$_POST['m'],
			'P02'=>$_POST['r'],
			'return_mode'=>'json'
		];
		$api_fee_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
		$api_fee_data=json_decode($api_fee_result,true);	
		if($api_fee_data['Success']=='YES'){
			
			$ItemName=[];
			$ItemQty=[];
			$ItemPrice=[];
			if(!empty($api_fee_data['ResponseMessage']['details'])){
				for ($i=0; $i <count($api_fee_data['ResponseMessage']['details']) ; $i++) { 
					# code...
					$ItemName[$i]=$api_fee_data['ResponseMessage']['details'][$i]['item_name'];
					$ItemQty[$i]=$api_fee_data['ResponseMessage']['details'][$i]['qty'];
					$ItemPrice[$i]=$api_fee_data['ResponseMessage']['details'][$i]['price'];		    		
				}
			}
			$return_data=[
			'merchant_client_id'=>$api_fee_data['ResponseMessage']['merchant_client_id'],
			'MerchantKey'=>$api_fee_data['ResponseMessage']['merchant_key'],
			'MerchantName'=>$api_fee_data['ResponseMessage']['merchant_name'],
			'Amount'=>$api_fee_data['ResponseMessage']['amount'],
			'total_amount'=>$api_fee_data['ResponseMessage']['total_amount'],
			'OrderID'=>$api_fee_data['ResponseMessage']['order_id'],
			'CallbackURL'=>$api_fee_data['ResponseMessage']['callback_url'],
			'ItemName'=>$ItemName,
			'ItemQty'=>$ItemQty,
			'ItemPrice'=>$ItemPrice,
			'OrderReference'=>$_POST['r'],
			'profile_pic'=>$api_fee_data['ResponseMessage']['profile_pic'],
			'reference_id'=>$_POST['r'],
			'sunpass_fee'=>$api_fee_data['ResponseMessage']['sunpass_fee'],
			'facility_fee'=>$api_fee_data['ResponseMessage']['facility_fee'],
			'processing_fee'=>$api_fee_data['ResponseMessage']['processing_fee'],
			'transaction_fee'=>$api_fee_data['ResponseMessage']['transaction_fee'],
			'discount'=>$api_fee_data['ResponseMessage']['discount'],
			'payment_method'=>$api_fee_data['ResponseMessage']['payment_method'],	
			'merchant_customer_fullname'=>$api_fee_data['ResponseMessage']['merchant_customer_fullname'],	
			'merchant_customer_mobile'=>$api_fee_data['ResponseMessage']['merchant_customer_mobile'],					
			'merchant_customer_email'=>$api_fee_data['ResponseMessage']['merchant_customer_email'],				
			];
		}
		$post_data =$return_data;

		//completeCheckoutSession
		$payload = array(
			'chargeAmount' => array(
				'amount' =>$_POST['total_due'],//amount +cc fee
				'currencyCode' => 'USD'
			),
		);

		try {
			$checkoutSessionId = $this->session->userdata('amz_sess');//chargeid respo form update
			$headers = array('x-amz-pay-Idempotency-Key' => uniqid());
			$client = new Amazon\Pay\API\Client($this->amazonpay_config);
			$result = $client->completeCheckoutSession($checkoutSessionId,$payload, $headers);
			// dd($result);
			if ($result['status'] === 200) {
				$response = json_decode($result['response'], true);
				// dd($response);
				$state = $response['statusDetails']['state'];
				$reasonCode = $response['statusDetails']['reasonCode'];
				$reasonDescription = $response['statusDetails']['reasonDescription'];
				// echo "state=$state; reasonCode=$reasonCode; reasonDescription=$reasonDescription\n";
			} else {
				// check the error
				// echo 'status=' . $result['status'] . '; response=' . $result['response'] . "\n";
				$arr=[
					'msg'=>'status=' . $result['status'] . '; response=' . $result['response'] . "",
					'url'=>'',
					'success'=>false
				];
				echo json_encode($arr);
				exit;

			}
		} catch (\Exception $e) {
			// handle the exception
			echo $e . "\n";
		}

		//process items...
		$order_details='';
		if(!empty($post_data['ItemName'])){
			$len = count($post_data['ItemName']);
			for ($i=0; $i <count($post_data['ItemName']) ; $i++) { 
				# code...
				if($len - 1==$i){
					$order_details.=$post_data['ItemName'][$i]."|".$post_data['ItemQty'][$i]."|".$post_data['ItemPrice'][$i]."";
				} else {
					$order_details.=$post_data['ItemName'][$i]."|".$post_data['ItemQty'][$i]."|".$post_data['ItemPrice'][$i]."~";
				}
			}
		}
		$arr=[
		   'method'=>'get_payment_fee',	
		   'P01' =>$post_data['MerchantKey'], //Merchant API Key
		   'P02' =>$api_fee_data['ResponseMessage']['amount'], //Mobile No
		   'P03' =>'AMAZON', //passcode	   
		   	'P04' =>'BUSINESS',
		   'return_mode'=>'json'
		];
		$fees=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
		$fees_data=json_decode($fees,true);
		// dd($arr);
		$data['fees_data']='';
		if($fees_data['Success']=='YES'){
		$data['fees_data']=$fees_data['ResponseMessage'];
		}
		//dd($data['fees_data']);
		$arr_process=[
		'method'=>'process_amazon_checkout',	
		'P01' =>$post_data['MerchantKey'], //Merchant API Key
		'P02' =>$this->clean($_POST['amazon_mobile']), //Mobile No
		'P03' =>$_POST['amazon_fullname'], //passcode
		'P04' =>$_POST['amazon_email'], //email
		'P05' =>$api_fee_data['ResponseMessage']['amount'], //amount
		'P06' =>'AMAZON',//AMAZON
		'P07' =>$_POST['r'], //checkout refid
		'P08' =>$order_details,//order details "item_name|qty|price" 
		'P09'=>$response['chargeId'],//amazon refrence id
		'P10' =>$data['fees_data']['processing_fee'], //pf
		'P11' =>$data['fees_data']['transaction_fee'],//tf  
		'P12' =>$data['fees_data']['iscard_fee_on_merch'],//is merch//1 or 0  		
		'P13' =>$_POST['total_due'],
		'return_mode'=>'json'
		];
		$proces_paypal_x=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr_process);
		$proces_amazon=json_decode($proces_amazon_x,true);
		// dd($arr);
		if($proces_amazon['Success']=="YES"){

			$reference_number=$response['chargeId'];

			//update fees and total via api af....
			$arr_update=[
				'method'=>'update_checkout_transaction_fee',
				'P01'=>$_POST['r'],//reference 
				'P02'=>$data['fees_data']['processing_fee'],//status 
				'P03'=>$data['fees_data']['transaction_fee'],//status 
				'P04'=>$_POST['total_due'],//status 
				'P05'=>0.00,//status 
				'P06'=>0.00,//status 
				'P07'=>$data['fees_data']['iscard_fee_on_merch'],
				'return_mode'=>'json'	
			];
			//dd($arr_update);
			$update_status=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr_update);
			// dd($update_status);

			$arr_update=[
				'method'=>'update_checkout_transaction',
				'P01'=>$_POST['r'],//reference 
				'P02'=>'PROCESSED',//status 
				'return_mode'=>'json'	
			];
			$update_status=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr_update);



			$arr=[
				'method'=>'get_suntag_shortcode',
				'P01'=>$post_data['MerchantKey'],
				'return_mode' => 'json'
			];
			$api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
			$api_bt_data=json_decode($api_bt_result,true);	
			// dd($api_bt_result);
			$data['suntag_data']='';
			if($api_bt_data['Success']=='YES'){
				$data['suntag_data']=$api_bt_data['ResponseMessage'];
			}else{
				$arr=[	
					'msg'=>'Something went wrong!',
					'success'=>false
				];
				echo json_encode($arr);
				exit;
			}  
			//$cfee_email = $post_data['iscard_fee_on_merch'] ==1 ? $cfee_email=0.00 : $cfee_email=number_format($upf,2,'.','')+number_format($utf,2,'.','');
			//email merchant process payment
			$arr=[
				"customer_name"=>$api_fee_data['ResponseMessage']['merchant_customer_fullname'],
				"amount"=>number_format($post_data['Amount'],2,'.',''),
				"transaction_id"=>$post_data['OrderID'],//cenpos
				"dba_name"=>$this->session->userdata('dba_name'),
				"creation_date"=>date('M d, Y'),
				"reference_num"=>$reference_number,//referece ng paypal
				"notes"=>'',
				"balance"=>'',
				"processing_fee"=>0.00,
				"transaction_fee"=>0.00,
				"fee"=>0.00,
				"vat"=>0.00,
				"cfee"=>0.00,
				"tfee"=>0.00,
				"total"=>number_format($post_data['total_amount'],2),  //amount +fee+vat
				"msg_above"=>"<h2>".$api_fee_data['ResponseMessage']['merchant_customer_fullname']." paid <span class='amount'>".number_format($post_data['total_amount'],2)." BSD</span>. </h2>",
				"msg_bottom"=>"",
				"email"=>$data['suntag_data']['business_email_address'],
				"view"=>'email_message_completed.blade.php',
				"title"=>'Completed Payment',
				"profile_pic" => $this->session->userdata("profile_pic"),
				"iscard_fee_on_merch"=>'',
			];
			//dd($arr);
			$this->confirm_email($arr);			

			$arr_post=[
				'method'=>'create_suncashme_checkout_log',
				'P01'=>$post_data['MerchantKey'],//MerchantKey
				'P02'=>$post_data['OrderID'],//order_id
				'P03'=>$reference_number,//reference_number
				'P04'=>'amazon',//type
				'P05'=>'success',//response
				'P06'=>$arr_process,//request
				'P07'=>json_encode($proces_amazon),//response
				'P08'=>'Payment Successful',//response
				'return_mode'=>'json'	
			];
			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);
			//dd($save_logs);
			unset($_SESSION['post_data']);
			$params=base64_encode($reference_number."||".$_POST['r']."||success||amazon");
			$arr=[	
				'msg'=>'Successfully Paid',
				'url'=>$post_data["CallbackURL"].$params,
				'success'=>true
			];
			
			echo json_encode($arr);	
		
		} else {
			$arr_post=[
				'method'=>'create_suncashme_checkout_log',
				'P01'=>$post_data['MerchantKey'],//MerchantKey
				'P02'=>$post_data['OrderID'],//order_id
				'P03'=>'',//reference_number
				'P04'=>'amazon',//type
				'P05'=>'failed',//response
				'P06'=>$arr_process,//response
				'P07'=>json_encode($proces_amazon),//response
				'P08'=>'Payment Failed',//response
				'return_mode'=>'json'	
			];
			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);

			$arr=[	
				'msg'=>$proces_amazon['ResponseMessage'],
				'success'=>false
			];
			
			echo json_encode($arr);		
		}
	}
	public function get_fees_payment(){//$suncashme payment
		$arr=[
		   'method'=>'get_payment_fee',	
		   'P01' =>$_POST['MerchantKey'], //Merchant API Key
		   'P02' =>$_POST['total_due'], //Mobile No
		   'P03' =>'AMAZON', //passcode 
		   'P04' =>'BUSINESS',		   
		   'return_mode'=>'json'
		];
		$fees=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
		$fees_data=json_decode($fees,true);
		// dd($arr);
		$data['fees_data']='';
		if($fees_data['Success']=='YES'){
			$data['fees_data']=$fees_data['ResponseMessage'];
		}
	   $processing_fee =$data['fees_data']['processing_fee'];
	   $transaction_fee =$data['fees_data']['transaction_fee'];
	   $total_due =$data['fees_data']['total_due'];
	   $is_charge_to_merch =$data['fees_data']['iscard_fee_on_merch'];

		$convenience_total=number_format($transaction_fee,2,'.','')+number_format($processing_fee,2,'.','');
	    $cc_fee=$convenience_total;
	   if($is_charge_to_merch>0){
		$cc_fee=0.00;

	   }
		$arr=[	
			'pf'=>number_format($processing_fee,2,'.',''),
			'tf'=>number_format($transaction_fee,2,'.',''),
			'ccfee'=>number_format($cc_fee,2,'.',''),
			'total'=>number_format($total_due,2,'.',''),
			'is_merch'=>$is_charge_to_merch,
			'success'=>true
		];
		
		echo json_encode($arr);
		exit;   


	}
	public function clean($string) {
    	// $this->assetHelper->session_checker();
        $string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.
        return preg_replace('/[^A-Za-z0-9]/', '', $string); // Removes special chars.
    }
	public function process_paypal_checkout(){
		// dd($_POST);
		// response[purchase_units][0][payments][captures][0][status]:
		if($_POST['status']!='COMPLETED'){
			$paypal_post1=[
				'method'=>'create_suncashme_checkout_log',
				'P01'=>$_POST['m'],//MerchantKey
				'P02'=>$_POST['r'],//order_id
				'P03'=>'',//reference_number
				'P04'=>'paypal',//type
				'P05'=>'failed',//response
				'P06'=>$_POST['total_due'],//response
				'P07'=>json_encode($_POST['response']),//response
				'P08'=>'PayPal Request Failed',//response
				'return_mode'=>'json'	
			];
			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$paypal_post1);
			
			echo json_encode($arr);	
			exit;	
			$arr=[	
				'msg'=>'Payment Failed. Please try again!',
				'success'=>false
			];
			
			echo json_encode($arr);
			exit;
		}
		if(empty($_POST['mobile'])){

			$arr=[	
				'msg'=>'Please fill up all fields',
				'success'=>false
			];
			
			echo json_encode($arr);
			exit;
		} 

		if(empty($_POST['response'])){

			$arr=[	
				'msg'=>'Null Response From Paypal',
				'success'=>false
			];
			
			echo json_encode($arr);
			exit;
		} 	

			
			

		$arr=[
			'method'=>'get_client_checkout_details',
			'P01'=>$_POST['m'],
			'P02'=>$_POST['r'],
			'return_mode'=>'json'
		];
		$api_fee_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
		$api_fee_data=json_decode($api_fee_result,true);	
		if($api_fee_data['Success']=='YES'){
			
			$ItemName=[];
			$ItemQty=[];
			$ItemPrice=[];
			if(!empty($api_fee_data['ResponseMessage']['details'])){
				for ($i=0; $i <count($api_fee_data['ResponseMessage']['details']) ; $i++) { 
					# code...
					$ItemName[$i]=$api_fee_data['ResponseMessage']['details'][$i]['item_name'];
					$ItemQty[$i]=$api_fee_data['ResponseMessage']['details'][$i]['qty'];
					$ItemPrice[$i]=$api_fee_data['ResponseMessage']['details'][$i]['price'];		    		
				}
			}
			$return_data=[
			'merchant_client_id'=>$api_fee_data['ResponseMessage']['merchant_client_id'],
			'MerchantKey'=>$api_fee_data['ResponseMessage']['merchant_key'],
			'MerchantName'=>$api_fee_data['ResponseMessage']['merchant_name'],
			'Amount'=>$api_fee_data['ResponseMessage']['amount'],
			'total_amount'=>$api_fee_data['ResponseMessage']['total_amount'],
			'OrderID'=>$api_fee_data['ResponseMessage']['order_id'],
			'CallbackURL'=>$api_fee_data['ResponseMessage']['callback_url'],
			'ItemName'=>$ItemName,
			'ItemQty'=>$ItemQty,
			'ItemPrice'=>$ItemPrice,
			'OrderReference'=>$_POST['r'],
			'profile_pic'=>$api_fee_data['ResponseMessage']['profile_pic'],
			'reference_id'=>$_POST['r'],
			'sunpass_fee'=>$api_fee_data['ResponseMessage']['sunpass_fee'],
			'facility_fee'=>$api_fee_data['ResponseMessage']['facility_fee'],
			'processing_fee'=>$api_fee_data['ResponseMessage']['processing_fee'],
			'transaction_fee'=>$api_fee_data['ResponseMessage']['transaction_fee'],
			'discount'=>$api_fee_data['ResponseMessage']['discount'],
			'payment_method'=>$api_fee_data['ResponseMessage']['payment_method'],	
			'merchant_customer_fullname'=>$api_fee_data['ResponseMessage']['merchant_customer_fullname'],	
			'merchant_customer_mobile'=>$api_fee_data['ResponseMessage']['merchant_customer_mobile'],					
			'merchant_customer_email'=>$api_fee_data['ResponseMessage']['merchant_customer_email'],				
			];
		}
		$post_data =$return_data;

		//process items...
		$order_details='';
		if(!empty($post_data['ItemName'])){
			$len = count($post_data['ItemName']);
			for ($i=0; $i <count($post_data['ItemName']) ; $i++) { 
				# code...
				if($len - 1==$i){
					$order_details.=$post_data['ItemName'][$i]."|".$post_data['ItemQty'][$i]."|".$post_data['ItemPrice'][$i]."";
				} else {
					$order_details.=$post_data['ItemName'][$i]."|".$post_data['ItemQty'][$i]."|".$post_data['ItemPrice'][$i]."~";
				}
			}
		}
		//log paypal req
		$paypal_post=[
			'method'=>'create_suncashme_checkout_log',
			'P01'=>$post_data['MerchantKey'],//MerchantKey
			'P02'=>$post_data['OrderID'],//order_id
			'P03'=>$_POST['paypal_reference_number'],//reference_number
			'P04'=>'paypal',//type
			'P05'=>'success',//response
			'P06'=>$_POST['total_due'],//response
			'P07'=>json_encode($_POST['response']),//response
			'P08'=>'PayPal Request Success',//response
			'return_mode'=>'json'	
		];
		$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$paypal_post);
		$arr=[
		   'method'=>'get_payment_fee',	
		   'P01' =>$post_data['MerchantKey'], //Merchant API Key
		   'P02' =>$api_fee_data['ResponseMessage']['amount'], //Mobile No
		   'P03' =>'PAYPAL_SUNCASHME', //passcode	   
		   'P04' =>'BUSINESS',
		   'return_mode'=>'json'
		];
		$fees=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
		$fees_data=json_decode($fees,true);
		// dd($arr);
		$data['fees_data']='';
		if($fees_data['Success']=='YES'){
			$data['fees_data']=$fees_data['ResponseMessage'];
		}
		//dd($data['fees_data']);
		$arr_process=[
			'method'=>'process_paypal_checkout',	
			'P01' =>$post_data['MerchantKey'], //Merchant API Key
			'P02' =>$this->clean($api_fee_data['ResponseMessage']['merchant_customer_mobile']),//$this->clean($_POST['mobile']), //Mobile No
			'P03' =>$_POST['fullname'], //passcode
			'P04' =>$_POST['email'], //email
			'P05' =>$api_fee_data['ResponseMessage']['amount'], //amount
			'P06' =>'AMAZON',//AMAZON
			'P07' =>$_POST['r'], //checkout refid
			'P08' =>$order_details,//order details "item_name|qty|price" 
			'P09'=>$_POST['paypal_reference_number'],//amazon refrence id
			'P10' =>$data['fees_data']['processing_fee'], //pf
			'P11' =>$data['fees_data']['transaction_fee'],//tf  
			'P12' =>$data['fees_data']['iscard_fee_on_merch'],//is merch//1 or 0  		
			'P13' =>$_POST['total_due'],
			'return_mode'=>'json'
		];
				// dd($arr_process);
		$proces_paypal_x=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr_process);
		$proces_paypal=json_decode($proces_paypal_x,true);
		// dd($arr);
		if($proces_paypal['Success']=="YES"){

			$reference_number=$_POST['paypal_reference_number'];

			//update fees and total via api af....
			$arr_update=[
				'method'=>'update_checkout_transaction_fee',
				'P01'=>$_POST['r'],//reference 
				'P02'=>$data['fees_data']['processing_fee'],//status 
				'P03'=>$data['fees_data']['transaction_fee'],//status 
				'P04'=>$_POST['total_due'],//status 
				'P05'=>0.00,//status 
				'P06'=>0.00,//status 
				'P07'=>$data['fees_data']['iscard_fee_on_merch'],
				'return_mode'=>'json'	
			];
			//dd($arr_update);
			$update_status=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr_update);
			// dd($update_status);

			$arr_update=[
				'method'=>'update_checkout_transaction',
				'P01'=>$_POST['r'],//reference 
				'P02'=>'PROCESSED',//status 
				'return_mode'=>'json'	
			];
			$update_status=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr_update);



			$arr=[
				'method'=>'get_suntag_shortcode',
				'P01'=>$post_data['MerchantKey'],
				'return_mode' => 'json'
			];
			$api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
			$api_bt_data=json_decode($api_bt_result,true);	
			// dd($api_bt_result);
			$data['suntag_data']='';
			if($api_bt_data['Success']=='YES'){
				$data['suntag_data']=$api_bt_data['ResponseMessage'];
			}else{
				$arr=[	
					'msg'=>'Something went wrong!',
					'success'=>false
				];
				echo json_encode($arr);
				exit;
			}  
			$cfee_email = $data['fees_data']['iscard_fee_on_merch']==1 ? $cfee_email=0.00 : $cfee_email=number_format($data['fees_data']['processing_fee'],2,'.','')+number_format($data['fees_data']['transaction_fee'],2,'.','');
			//email merchant process payment
			$arr=[
				"customer_name"=>$_POST['fullname'],
				"amount"=>number_format($post_data['Amount'],2,'.',''),
				"transaction_id"=>$post_data['OrderID'],//cenpos
				"dba_name"=>$this->session->userdata('dba_name'),
				"creation_date"=>date('M d, Y'),
				"reference_num"=>$reference_number,//referece ng paypal
				"notes"=>'',
				"balance"=>'',
				"processing_fee"=>0.00,
				"transaction_fee"=>0.00,
				"fee"=>0.00,
				"vat"=>0.00,
				"cfee"=>$cfee_email,
				"tfee"=>0.00,
				"total"=>number_format($_POST['total_due'],2),  //amount +fee+vat
				"msg_above"=>"<h2>".$_POST['fullname']." paid <span class='amount'>".number_format($_POST['total_due'],2)." BSD</span>. </h2>",
				"msg_bottom"=>"",
				"email"=>$data['suntag_data']['business_email_address'],
				"view"=>'email_message_completed.blade.php',
				"title"=>'Completed Payment',
				"profile_pic" => $this->session->userdata("profile_pic"),
				"iscard_fee_on_merch"=>$data['fees_data']['iscard_fee_on_merch'],
			];
			//dd($arr);
			$this->confirm_email($arr);			

			$arr_post=[
				'method'=>'create_suncashme_checkout_log',
				'P01'=>$post_data['MerchantKey'],//MerchantKey
				'P02'=>$post_data['OrderID'],//order_id
				'P03'=>$reference_number,//reference_number
				'P04'=>'paypal',//type
				'P05'=>'success',//response
				'P06'=>$arr_process,//request
				'P07'=>json_encode($proces_paypal),//response
				'P08'=>'Payment Successful',//response
				'return_mode'=>'json'	
			];
			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);
			//dd($save_logs);
			unset($_SESSION['post_data']);
			$params=base64_encode($reference_number."||".$_POST['r']."||success||paypal");
			$arr=[	
				'msg'=>'Successfully Paid',
				'url'=>$post_data["CallbackURL"].$params,
				'success'=>true
			];
			
			echo json_encode($arr);	
			exit;
		} else {
			$arr_post=[
				'method'=>'create_suncashme_checkout_log',
				'P01'=>$post_data['MerchantKey'],//MerchantKey
				'P02'=>$post_data['OrderID'],//order_id
				'P03'=>'',//reference_number
				'P04'=>'paypal',//type
				'P05'=>'failed',//response
				'P06'=>$arr_process,//response
				'P07'=>json_encode($proces_paypal),//response
				'P08'=>'Payment Failed',//response
				'return_mode'=>'json'	
			];
			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);

			$arr=[	
				'msg'=>$proces_paypal['ResponseMessage'],
				'success'=>false
			];
			
			echo json_encode($arr);	
			exit;	
		}
	}
	private function getsdcustomercard($merchant_key,$merchant_customer_id){

		// get card info
		$arr=[
			'method'=>'getBusinessSandDollarContactByCustomer',
			'P01'=> $merchant_key,		
			'P02'=> $merchant_customer_id,
			'return_mode' => 'json'
		  ];
		//   dd($arr);
		  $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
		  		//  dd($api_bt_result);
		  $api_bt_data=json_decode($api_bt_result,true);
		//  dd($api_bt_data);
		  $ccdata='';
		  if($api_bt_data['Success']=='YES'){
			 $ccdata=$api_bt_data['ResponseMessage'];
          }
          //dd($ccdata);
		  //prefer cards/

            return $ccdata;
	}
	public function remove_sdcard(){
		$id = base64_decode($this->input->post('_c'));
		$arr=[
			'method'=>'deleteBusinessSandDollarContact',
			'P01'=> $this->input->post('m'),
			'P02'=> $id,
			'return_mode' => 'json'
		  ];
		//   dd($arr);
		  $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
		  $api_bt_data=json_decode($api_bt_result,true);
		  //dd($api_bt_result);
		  $ccdata='';
		  if($api_bt_data['Success']=='YES'){
			 //$ccdata=$api_bt_data['ResponseMessage'];
			$arr=[
				'msg'=>$api_bt_data['ResponseMessage'],
				'success'=>true
			];
			echo json_encode($arr);
			exit;
          } else {
			$arr=[
				'msg'=>$api_bt_data['ResponseMessage'],
				'success'=>false
			];
			echo json_encode($arr);
			exit;
		  }
	}	
	//checkout
	public function process_voucher_payment(){
		//recall api staging on saving..
		$arr=[
			'method'=>'get_client_checkout_details',
			'P01'=>$_POST['m'],
			'P02'=>$_POST['r'],
			'return_mode'=>'json'
		];
	    $api_fee_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
	    //dd($api_fee_result);
	    $api_fee_data=json_decode($api_fee_result,true);
	    //dd($api_fee_data);		
	    if($api_fee_data['Success']=='YES'){

		$order_details = '' ; 
		if(!empty($api_fee_data['ResponseMessage']['details'])){
			$len = count($api_fee_data['ResponseMessage']['details']);
			for ($i=0; $i <count($api_fee_data['ResponseMessage']['details']) ; $i++) { 
				# code...
				if($len - 1==$i){
					$order_details.=$api_fee_data['ResponseMessage']['details'][$i]['item_name']."|".$api_fee_data['ResponseMessage']['details'][$i]['qty']."|".$api_fee_data['ResponseMessage']['details'][$i]['price']."";	
				} else {
					$order_details.=$api_fee_data['ResponseMessage']['details'][$i]['item_name']."|".$api_fee_data['ResponseMessage']['details'][$i]['qty']."|".$api_fee_data['ResponseMessage']['details'][$i]['price']."~";	
				}
				
			}
		}
			$total_amount = $api_fee_data['ResponseMessage']['amount']+$api_fee_data['ResponseMessage']['sunpass_fee']+$api_fee_data['ResponseMessage']['sunpass_fee']+$api_fee_data['ResponseMessage']['facility_fee']+$api_fee_data['ResponseMessage']['processing_fee']+$api_fee_data['ResponseMessage']['transaction_fee'];// prior to change kung may fee na need o wala sunpass fee sure mron ok 
			$return_data=[
			'merchant_client_id'=>$api_fee_data['ResponseMessage']['merchant_client_id'],
			'MerchantKey'=>$api_fee_data['ResponseMessage']['merchant_key'],
			'MerchantName'=>$api_fee_data['ResponseMessage']['merchant_name'],
			'Amount'=>$api_fee_data['ResponseMessage']['amount'],
			'total_amount'=>$api_fee_data['ResponseMessage']['total_amount'],
			'OrderID'=>$api_fee_data['ResponseMessage']['order_id'],
			'CallbackURL'=>$api_fee_data['ResponseMessage']['callback_url'],
			// 'ItemName'=>$ItemName,
			// 'ItemQty'=>$ItemQty,
			// 'ItemPrice'=>$ItemPrice,
			'OrderReference'=>$_POST['r'],
			'profile_pic'=>$api_fee_data['ResponseMessage']['profile_pic'],
			'reference_id'=>$_POST['r'],
			'sunpass_fee'=>$api_fee_data['ResponseMessage']['sunpass_fee'],
			'facility_fee'=>$api_fee_data['ResponseMessage']['facility_fee'],
			'processing_fee'=>$api_fee_data['ResponseMessage']['processing_fee'],
			'transaction_fee'=>$api_fee_data['ResponseMessage']['transaction_fee'],
			'discount'=>$api_fee_data['ResponseMessage']['discount'],
			'iscard_fee_on_merch'=>$api_fee_data['ResponseMessage']['iscard_fee_on_merch'],
			'order_details'=>$order_details,
			'payment_method'=>$api_fee_data['ResponseMessage']['payment_method'],	
			'merchant_customer_fullname'=>$api_fee_data['ResponseMessage']['merchant_customer_fullname'],	
			'merchant_customer_mobile'=>$api_fee_data['ResponseMessage']['merchant_customer_mobile'],					
			'merchant_customer_email'=>$api_fee_data['ResponseMessage']['merchant_customer_email'],	
			];
		}

		$post_data =$return_data;
		//get voucher info here
		$arr_voucher=[
			'method'=>'voucherInquiry',
			'P01'=> $_POST['m'],
			'P02'=> $this->input->post('voucher_number'),
			'return_mode' => 'json'
		];
		$api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr_voucher);
		// dd($api_bt_result);
		$api_bt_data=json_decode($api_bt_result,true);
		$voucher_amount=0.00;
		if($api_bt_data['Success']=='YES' ){
			$voucher_amount=$api_bt_data['ResponseMessage']['amount'];
			if($voucher_amount<$total_amount){
				$arr=[
					'msg'=>'Voucher value not enough',
					'success'=>false
				];
				echo json_encode($arr);
				exit;				
			}
		} else  {
			$arr=[
				'msg'=>$api_bt_data['ResponseMessage'],
				'success'=>false
			];
			echo json_encode($arr);
			exit;	
		}

		$arr=[
		   'method'=>'suncashCheckOutVoucherPayment',
		   'P01'=> $_POST['m'],
		   'P02'=> $post_data['OrderID'],
		   'P03'=> $_POST['voucher_mobile_number'],
		   'P04'=> $post_data['order_details'],
		   'P05'=> $total_amount,
		   'P06'=> $_POST['voucher_number'],
		   'P07'=> $_POST['voucher_pin'],
		   'P08'=> $voucher_amount,
		   'P09'=> $voucher_amount,	
		   'P10'=> $voucher_amount,		   	   
		   'return_mode'=>'json'
		  ];
	   $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
	   $api_bt_data=json_decode($api_bt_result,true);
	   if($api_bt_data['Success']=='YES'){
		//update payment detail status

		$reference_number=$api_bt_data['ResponseMessage']['TransactionId'];
		$arr_update=[
			'method'=>'update_checkout_transaction',
			'P01'=>$post_data['OrderReference'],//reference
			'P02'=>'PROCESSED',//status
			'P03'=>'',//status
			'return_mode'=>'json'
		];
		$update_status=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr_update);	

		//email merchant process payment

		$arr=[
			'method'=>'get_suntag_shortcode',
			'P01'=>$_POST['m'],//$this->session->userdata('SessionID'),
			'return_mode' => 'json'
		];
		$api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
		$api_bt_data=json_decode($api_bt_result,true);	
		// dd($api_bt_result);
		$data['suntag_data']='';
		if($api_bt_data['Success']=='YES'){
		$data['suntag_data']=$api_bt_data['ResponseMessage'];
		}else{
				$arr=[	
					'msg'=>'Something went wrong!',
					'success'=>false
				];
				echo json_encode($arr);
				exit;
		}  		
		$arr=[
			"customer_name"=>$api_fee_data['ResponseMessage']['merchant_customer_fullname'],
			"amount"=>number_format($post_data['Amount'],2,'.',''),
			"transaction_id"=>$post_data['OrderID'],//cenpos
			"dba_name"=>$data['suntag_data']['dba_name'],
			"creation_date"=>date('M d, Y'),
			"reference_num"=>$reference_number,
			"notes"=>'',
			"balance"=>'',
			"processing_fee"=>0.00,
			"transaction_fee"=>0.00,
			"fee"=>0.00,
			"vat"=>0.00,
			"cfee"=>0.00,
			"tfee"=>0.00,
			"total"=>number_format($total_amount,2),  //amount +fee+vat
			"msg_above"=>"<h2>".$api_fee_data['ResponseMessage']['merchant_customer_fullname']." paid <span class='amount'>".number_format($total_amount,2)." BSD</span>. </h2>",
			"msg_bottom"=>"",
			"email"=>$data['suntag_data']['business_email_address'],
			"view"=>'email_message_completed.blade.php',
			"title"=>'Completed Payment',
			"profile_pic" => $this->session->userdata("profile_pic"),
			"iscard_fee_on_merch"=>0
		];
		//dd($arr);
		$this->confirm_email($arr);



		//final process
		unset($_SESSION['post_data']);
		$reference_num = $reference_number."||".$_POST['r'];
		$params=base64_encode($reference_num."||success||voucher");
		$arr=[
			'msg'=>'Successfully Purchased',
			'reference'=>$reference_number,
			'url'=>$post_data["CallbackURL"].$params,
			'payment_method'=>'Voucher',
			'success'=>true
		];

		echo json_encode($arr);

	   } else {
		   $arr=[
			   'msg'=>$api_bt_data['ResponseMessage'],
			   'success'=>false
		   ];
		   echo json_encode($arr);
		   exit;
	   }
	}
}//end controller