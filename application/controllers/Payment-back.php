<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends CI_Controller {// put filename of controller on class name 

	function __construct(){//where can put dynamic functions and models....
        parent::__construct();
   		$this->session->flashdata('msg');
   		//load model.
      	$this->load->model('core/db');//for transactions and other eloquent db features
      	$this->load->model('core/Qr');
   		//$this->load->model('user');
   		//$this->load->model('Value_sets_global','global');
   		$this->assetHelper = new AssetHelper\AssetHelper();
   		
	}
	public function index(){//method then routefind
		if($this->input->post('payment_method')=='cash'){
			///dd($_SESSION);
			$this->session->set_userdata('post_data',$_POST);
			$arr=[	
				'url'=>base_url('payment/cash'),
				'success'=>true
			];
			
			echo json_encode($arr);
			exit;	
		} else if($this->input->post('payment_method')=='wallet'){
			$this->session->set_userdata('post_data',$_POST);
				$url=base_url('wallet/login');
			if(isset($_SESSION['customer_info'])){
				$url=base_url('payment/wallet');
			} 

			$arr=[	
				'url'=>$url,
				'success'=>true
			];
			
			echo json_encode($arr);
			exit;	
		}
	}
	public function wallet(){//method then routefind //get fee
		//dd($_SESSION);
		$data['payment_data'] = $this->session->userdata('post_data');
		if(isset($_SESSION['SessionID'])){
		$amount = 	str_replace( ',', '', $this->session->userdata('post_data')['amount']);
	    $arr=[
	    	'method'=>'get_business_fee',
            'P01'=> $amount,
            'P02'=>  'MONEYTRANSFER',
            'P03'=>	'',//$this->session->userdata('suntag_shortcode'),
            'return_mode'=>'json'
	    ];
	    //$arr_tosend['url']="login_business?".http_build_query($arr);

	    //dd($arr);
	    $api_fee_result=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr);
	    //dd($api_fee_result);
	    $api_fee_data=json_decode($api_fee_result,true);
	    //dd($api_bt_data);
	    $data['fee_data']='';
	    if($api_fee_data['ResponseCode']=='0000'){
	    $data['fee_data']=$api_fee_data['ResponseMessage'];                     
	    }	
	    $data['Balance'] = $this->assetHelper->get_balance($_SESSION['SessionID']);


	    //dd($data['fee_data']);
		$this->blade->view('wallet-page',$data);
		} else {
		redirect('');
		}
	}
	public function cash(){//method then routefind
		//$data['payment_data'] = $this->session->userdata('post_data');

		$data['payment_data'] = $this->session->userdata('post_data');
		$amount = 	str_replace( ',', '', $this->session->userdata('post_data')['amount']);
	    $arr=[
	    	'method'=>'get_business_fee',
            'P01'=> $amount,
            'P02'=>  'MONEYTRANSFER',
            'P03'=>	'',//$this->session->userdata('suntag_shortcode'),
            'return_mode'=>'json'
	    ];
	    //$arr_tosend['url']="login_business?".http_build_query($arr);

	    //dd($arr);
	    $api_fee_result=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr);
	    //dd($api_fee_result);
	    $api_fee_data=json_decode($api_fee_result,true);
	    //dd($api_bt_data);
	    $data['fee_data']='';
	    if($api_fee_data['ResponseCode']=='0000'){
	    $data['fee_data']=$api_fee_data['ResponseMessage'];                     
	    }	

	    //dd($data['fee_data']);		
		$this->blade->view('cash-page',$data);

	}
	public function process_cash(){
		//get temp tokken...
		//dd($_POST);
		$mobile = $this->clean($this->security->xss_clean($this->input->post('mobile')));
		$auth_key=$this->assetHelper->create_tokken();
	    $arr=[
	    	'method'=>'create_cash_business_billpay',
	    	'P01'=>$auth_key['temp_auth'],//$auth_key['temp_auth'],
            'P02'=> $this->session->userdata('client_record_id'),//merch id
            'P03'=> $this->session->userdata('suntag_shortcode'),//suntag
            'P04'=> str_replace( ',', '',$this->input->post('amount')),//amount+fee+vat
            'P05'=> $this->input->post('reference_num'),
            'P06'=> $this->input->post('notes'),
            'P07'=>	$this->input->post('fname'),
            'P08'=>	$this->input->post('lname'),
            'P09'=>	$this->input->post('email'),
            'P10'=>	$mobile,
            'P11'=>	$this->input->post('is_email'),
            'P12'=>	$this->input->post('is_sms'),
            'return_mode'=>'json'

	    ];

	     //dd($arr);

	    $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr);
	  	//dd($api_bt_result);
	    $api_bt_data=json_decode($api_bt_result,true);
	    //dd($api_bt_data);
         if($api_bt_data['ResponseCode']=='0000'){
         	//email...
         	//transaction_id
			//qr_code
			//barcode
			//mobile
			//email
			//creation_date
			if($this->input->post('is_email')==1){
			$qr=Qr::GenerateQr($api_bt_data['ResponseMessage'],'',140);
			$barcode=Qr::GenerateBarcode128($api_bt_data['ResponseMessage'],'','');
			$qr= base64_decode($qr->generate());
			$barcode= base64_decode($barcode);	
			$total=	(float)$this->input->post('amount')+(float)$this->input->post('hid_fee')+(float)$this->input->post('hid_vat');	
			$arr=[
				"amount"=>$this->input->post('amount'),
				"transaction_id"=>$api_bt_data['ResponseMessage'],
				"fee"=>$this->input->post('hid_fee'),
				"vat"=>$this->input->post('hid_vat'),
				"total_amount"=>$total,
				//"qr_code"=>'<img src="data:'.$qr->getContentType().';base64,'.$qr->generate().'" />',
				//"barcode"=>'<img src="data:image/png;base64,'.$barcode.'" />',
				"creation_date"=>date('M d, Y'),
				"email"=>$this->input->post('email'),
				"mobile"=>$this->input->post('mobile'),
				"view"=>'email_payment_code.blade.php',
				"title"=>'Payment Code',
				"profile_pic" => $this->session->userdata("ImageURL")
				//<img src="data:'.$qr->getContentType().';base64,'.$qr->generate().'" />
			];

	        $image_attachment[0]['img_string']=$qr;
	        $image_attachment[0]['img_name']='qr_code';
	        $image_attachment[1]['img_string']=$barcode;
	        $image_attachment[1]['img_name']='barcode';

			$this->pending_email_cash($arr,$image_attachment);

			}
			$arr=[	
				'msg'=>$api_bt_data['ResponseMessage'],
				'success'=>true
			];
			
			echo json_encode($arr);
			exit;         	
         } else {
			$arr=[	
				'msg'=>$api_bt_data['ResponseMessage'],
				'success'=>false
			];
			
			echo json_encode($arr);
			exit;
         }		
	}	

	public function process_wallet(){
		//get temp tokken...
		//dd($_POST);
		$auth_key=$this->assetHelper->create_tokken();
		//check money
		$current_balance = $this->assetHelper->get_balance($_SESSION['SessionID']);

		if($current_balance<$this->input->post('amount_val')){
			$arr=[	
				'msg'=>'Ooops! Sorry you do not have enough balance to continue this transaction. Please reload you wallet account.',
				'current_balance'=>number_format($current_balance,2),
				'success'=>false
			];
			
			echo json_encode($arr);
			exit;
		}

	    $arr=[
	    	'method'=>'create_wallet_business_billpay',
	    	'P01'=>$auth_key['temp_auth'],//'a84cdf754f7708b374f41ff82fcded41d997d167',//	    	
            'P02'=> $this->session->userdata('client_record_id'),//merch id
            'P03'=> $this->session->userdata('suntag_shortcode'),//suntag
            'P04'=> $this->session->userdata('customer_info')['CustomerName'],//customer name
            'P05'=> $this->session->userdata('customer_info')['MobileNumber'],
            'P06'=> $this->session->userdata('customer_info')['Email'],//email?            
            'P07'=> $this->input->post('amount'),//amount
            'P08'=> $this->input->post('reference_num'),
            'P09'=> $this->input->post('notes'),
			"P10"=>$this->input->post('hid_fee'),
			"P11"=>$this->input->post('hid_vat'),            
            'return_mode'=>'json'

	    ];
	    //dd($arr);
	

	    $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr);
	  	//dd($api_bt_result);
	    $api_bt_data=json_decode($api_bt_result,true);

         if($api_bt_data['ResponseCode']=='0000'){
         	//email customer.
			$arr=[
				"customer_name"=>$this->session->userdata('customer_info')['CustomerName'],
				"amount"=>number_format($this->input->post('amount'),2),
				"transaction_id"=>$api_bt_data['ResponseMessage'],
				"dba_name"=>$this->session->userdata('dba_name'),
				"creation_date"=>date('M d, Y'),
				"reference_num"=>$this->input->post('reference_num'),
				"notes"=>$this->input->post('notes'),
				"balance"=>$this->input->post('balance'),
				"fee"=>number_format($this->input->post('hid_fee'),2),
				"vat"=>number_format($this->input->post('hid_vat'),2),
				"total"=>number_format($this->input->post('amount_val'),2),  //amount +fee+vat
				"msg_above"=>"<h2>You've sent <span class='amount'>".number_format($this->input->post('amount'),2)." BSD</span> to ".$this->session->userdata('dba_name').".</h2>",
				"msg_bottom"=>"<p>Transaction is <span class='status'>PENDING</span> until ".$this->session->userdata('dba_name')." accepts payment!</p>",
				"email"=>$this->session->userdata('customer_info')['Email'],
				"view"=>'email_message.blade.php',
				"title"=>'Pending Payment',
				"profile_pic" => $this->session->userdata("ImageURL")
			];

			$this->pending_email($arr);
			//email merchant
			$arr=[
				"customer_name"=>$this->session->userdata('dba_name'),
				"amount"=>number_format($this->input->post('amount_val'),2),
				"transaction_id"=>$api_bt_data['ResponseMessage'],
				"dba_name"=>$this->session->userdata('dba_name'),
				"creation_date"=>date('M d, Y'),
				"reference_num"=>$this->input->post('reference_num'),
				"notes"=>$this->input->post('notes'),
				"balance"=>$this->input->post('balance'),
				"fee"=>$this->input->post('hid_fee'),
				"vat"=>$this->input->post('hid_vat'),
				"total"=>$this->input->post('amount_val'),  //amount +fee+vat
				"msg_above"=>"<h2>".$this->session->userdata('customer_info')['CustomerName']." sent you <span class='amount'>".$this->input->post('amount')." BSD</span></h2>",
				"msg_bottom"=>"<p>Transaction is <span class='status'>PENDING</span> until ".$this->session->userdata('dba_name')." accepts payment!</p>",
				"email"=>$this->session->userdata('email'),
				"view"=>"email_message_merchant.blade.php",
				"title"=>'Incoming Payment',
				"profile_pic" => $this->session->userdata("profile_pic")
			];

			$this->pending_email($arr);


			$arr=[	
				'msg'=>$api_bt_data['ResponseMessage'],
				'current_balance'=>'',
				'success'=>true
			];
			
			echo json_encode($arr);
			exit;         	
         } else {
			$arr=[	
				'msg'=>$api_bt_data['ResponseMessage'],
				'current_balance'=>'',
				'success'=>false
			];
			
			echo json_encode($arr);
			exit;

         }		
	}

	public function process_fee(){
		    $arr=[
		    	'method'=>'get_business_fee',
	            'P01'=> $this->input->post('amount'),
	            'P02'=>  'MONEYTRANSFER',
	            'P03'=>	'',//$this->session->userdata('suntag_shortcode'),
	            'return_mode'=>'json'
		    ];
		    //$arr_tosend['url']="login_business?".http_build_query($arr);

		    //dd($arr);
		    $api_fee_result=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr);
		    //dd($api_fee_result);
		    $api_fee_data=json_decode($api_fee_result,true);
		    //dd($api_bt_data);
		    $data['fee_data']='';
		    if($api_fee_data['ResponseCode']=='0000'){
		    $data['fee_data']=$api_fee_data['ResponseMessage'];                     
		    }	
			$arr=[	
				'fee_data'=>$data['fee_data'],
				'success'=>true
			];
			
			echo json_encode($arr);
			exit;
	}

	public function pending_email($arr){
		$to = $arr['email'];
		$subj = $arr['title'];
		$attachment_arr='';
        $template_html = $arr['view']; //views/templates/mail/
		//email receipt..
		$this->assetHelper->send_email($arr,$to,$subj,$template_html,$attachment_arr);		
	}

	public function pending_email_cash($arr,$image_attachment){
		$to = $arr['email'];
		$subj = $arr['title'];
		$attachment_arr='';
        $template_html = $arr['view']; //views/templates/mail/

		//email receipt..
		$this->assetHelper->send_email($arr,$to,$subj,$template_html,$attachment_arr,$image_attachment);		
	}

	public function sample_email(){

/*		$qr=Qr::GenerateQr('00000312','',140);
		$barcode=Qr::GenerateBarcode128('00000312','','');
		$qr= base64_decode($qr->generate());
		$barcode= base64_decode($barcode);
		$arr=[
			"amount"=>"100.00",
			"transaction_id"=>'00000312',
			//"qr_code"=>'<img src="data:'.$qr->getContentType().';base64,'.$qr->generate().'" />',
			//"barcode"=>'<img src="data:image/png;base64,'.$barcode.'" />',
			"creation_date"=>date('M d, Y'),
			"email"=>'rolandjhaymoris@yahoo.com',
			"mobile"=>'09178435554',
			"view"=>'email_payment_code.blade.php',
			"title"=>'Pending Payment',
			//"profile_pic" => $this->session->userdata("ImageURL")
			//<img src="data:'.$qr->getContentType().';base64,'.$qr->generate().'" />
		];*/
/*        $image_attachment[0]['img_string']=$qr;
        $image_attachment[0]['img_name']='qr_code';
        $image_attachment[1]['img_string']=$barcode;
        $image_attachment[1]['img_name']='barcode';*/

			// $arr=[
			// 	"customer_name"=>'Maner2',
			// 	"amount"=>'2.00',
			// 	"transaction_id"=>'32434456',
			// 	"dba_name"=>'',
			// 	"creation_date"=>'03 Nov 2018',
			// 	"reference_num"=>'4353455',
			// 	"notes"=>'Hye',
			// 	"balance"=>'33555',
			// 	"fee"=>'1.00',
			// 	"vat"=>'0.36',
			// 	"total"=>'4.00',  //amount +fee+vat
			// 	"msg_above"=>"<h2>Zel sent you <span class='amount'>5 BSD</span></h2>",
			// 	"msg_bottom"=>"<p>Transaction is <span class='status'>PENDING</span> until ".$this->session->userdata('dba_name')." accepts payment!</p>",
			// 	"email"=>'zelbuado@gmail.com',
			// 	"view"=>"email_message_merchant.blade.php",
			// 	"title"=>'Incoming Payment',
			// 	"profile_pic" => $this->session->userdata("profile_pic")
			// ];
			
			        	//email customer.
			// $arr=[
			// 	"customer_name"=>'Hazel Buado',
			// 	"amount"=>'2.00',
			// 	"transaction_id"=>'32434456',
			// 	"dba_name"=>'',
			// 	"creation_date"=>'03 Nov 2018',
			// 	"reference_num"=>'4353455',
			// 	"notes"=>'Hye',
			// 	"balance"=>'33555',
			// 	"fee"=>'1.00',
			// 	"vat"=>'0.36',
			// 	"total"=>'4.00',  //amount +fee+vat
			// 	"msg_above"=>"<h2>Zel sent you <span class='amount'>5 BSD</span></h2>",
			// 	"msg_bottom"=>"<p>Transaction is <span class='status'>PENDING</span> until ".$this->session->userdata('dba_name')." accepts payment!</p>",
			// 	"email"=>'zelbuado@gmail.com',
			// 	"view"=>'email_message_completed.blade.php',
			// 	"title"=>'Con Payment',
			// 	"profile_pic" => $this->session->userdata("ImageURL")
			// ];
			// 
			$arr=[
				"customer_name"=>'Hazel Buado',
				"amount"=>'2.00',
				"transaction_id"=>'32434456',
				"dba_name"=>'Test111',
				"creation_date"=>'03 Nov 2018',
				"reference_num"=>'32434456',
				"notes"=>'Waterbill',
				"balance"=>'32.00',
				"fee"=>'1.00',
				"vat"=>'0.36',
				"total"=>'3.36',  //amount +fee+vat
				"msg_above"=>"<h2>You've sent <span class='amount'> 4.36 BSD</span> to Test111</h2>",
				"msg_bottom"=>"<p>Transaction is <span class='status'>PENDING</span> until Test111 accepts payment!</p>",
				"email"=>'nairbnorb@yahoo.com',
				"view"=>'email_message.blade.php',
				"title"=>'Pending Payment',
				"profile_pic" => $this->session->userdata("ImageURL")
			];
			$image_attachment='';

		$this->pending_email_cash($arr,$image_attachment);


		$this->blade->view('templates.email.email_message');
	}

	public function checkout_form(){
		$data['tokken'] = $this->assetHelper->create_tokken();
		$this->blade->view('checkout-sample-page',$data);
	}
	public function cash_payment_success(){
		// $data['tokken'] = $this->assetHelper->create_tokken();
			$data['post_data']=$this->session->userdata('post_data');	
			$data['cash_posted_data']=$this->session->userdata('cash_posted_data');
			$data['cash_payment_code']=$this->session->userdata('cash_payment_code');		
			//unset($_SESSION['post_data']);
			//unset($_SESSION['cash_posted_data']);
			//unset($_SESSION['cash_payment_code']);
		$this->blade->view('cash_payment_success',$data);
	}
	public function successpage_redirect($reference_number,$status,$payment_method,$callbackurl){
		//dd($reference_number);
		// $data['tokken'] = $this->assetHelper->create_tokken();
		$data['callbackurl']=base64_decode(urldecode($callbackurl));
		$data['reference_number']=$reference_number;
		$data['status']=$status;
		$data['payment_method']=$payment_method;
		$this->blade->view('success_page',$data);
	}
	public function checkout($merchant_key,$reference_id){
		//dd($_POST);1
		if(!empty($merchant_key) && !empty($reference_id)){
			//if empty fields
			$arr=[
				'method'=>'get_client_checkout_details',
				'P01'=>$merchant_key,
				'P02'=>$reference_id,
				'return_mode'=>'json'
			];
			//dd($arr);
		    $api_fee_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
		    //dd($api_fee_result);
		    $api_fee_data=json_decode($api_fee_result,true);
		    //dd($api_fee_data);		
		    if($api_fee_data['Success']=='YES'){
		   	//check if pending...
		   	if($api_fee_data['ResponseMessage']['status']!='PENDING'){
		   		redirect('');
		   	}
		    //process order as individual array shit
		    $ItemName=[];
		    $ItemQty=[];
		    $ItemPrice=[];
		    if(!empty($api_fee_data['ResponseMessage']['details'])){
		    	for ($i=0; $i <count($api_fee_data['ResponseMessage']['details']) ; $i++) { 
		    		# code...
				    $ItemName[$i]=$api_fee_data['ResponseMessage']['details'][$i]['item_name'];
				    $ItemQty[$i]=$api_fee_data['ResponseMessage']['details'][$i]['qty'];
				    $ItemPrice[$i]=$api_fee_data['ResponseMessage']['details'][$i]['price'];		    		
		    	}
		    }
			$return_data=[
			'merchant_client_id'=>$api_fee_data['ResponseMessage']['merchant_client_id'],
			'MerchantKey'=>$api_fee_data['ResponseMessage']['merchant_key'],
			'MerchantName'=>$api_fee_data['ResponseMessage']['merchant_name'],
			'Amount'=>$api_fee_data['ResponseMessage']['total_amount'],
			'OrderID'=>$api_fee_data['ResponseMessage']['order_id'],
			'CallbackURL'=>$api_fee_data['ResponseMessage']['callback_url'],
			'ItemName'=>$ItemName,
			'ItemQty'=>$ItemQty,
			'ItemPrice'=>$ItemPrice,
			'OrderReference'=>$reference_id,
			'profile_pic'=>$api_fee_data['ResponseMessage']['profile_pic'],
			'reference_id'=>$reference_id,
			//'auth',				
			];
			$data['reference_id']=$reference_id;

			//dd($return_data);

			$data['post_data'] = $return_data;

		    $arr=[
		    	'method'=>'login_business',
	            'P01'=> SERVICE_ACCOUNT_USERNAME_DEV,
	            'P02'=> SERVICE_ACCOUNT_PASSWORD_DEV,
		    ];
		    //$arr_tosend['url']="login_business?".http_build_query($arr);

		    //dd($arr_tosend);
		    $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
		    $api_bt_data=json_decode($api_bt_result,true);
		    //dd($api_bt_data);
		    $data['login_data']='';
		    if($api_bt_data['ResponseCode']=='YES'){
		    $data['login_data']=$api_bt_data['ResponseMessage'];                     
		    }


			//get payment settings. 
		       $arr=[
		         'method'=>'get_checkout_methods_byid',
		            'P01'=> $data['login_data']['SessionID'],
		            'P02'=> $api_fee_data['ResponseMessage']['merchant_client_id'],
		       ];
		       // $arr_tosend['url']="login_business?".http_build_query($arr);

		       // dd($arr_tosend);
		       //dd($arr);
		       $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
		       $api_bt_data=json_decode($api_bt_result,true);
		       //dd($api_bt_result);
		       $data['selected_settings_data']='';
		       if($api_bt_data['Success']=='YES'){
		       $api_bt_data['ResponseMessage'][0] =isset($api_bt_data['ResponseMessage'][0]) ? $api_bt_data['ResponseMessage'][0] : '';
		       $data['selected_settings_data']=$api_bt_data['ResponseMessage'][0];                     
		       } 

			//session....
			$this->session->set_userdata('post_data',$return_data);
			
			$this->blade->view('checkout',$data);
		    } else {
/*			$arr=[	
				'msg'=>'Not a Valid Order.',
				'success'=>false
			];
			
			echo json_encode($arr);
			exit;	*/	
			redirect('');	    	
		    }	
/*		$required_fields=[
			'MerchantKey',
			'MerchantName',
			'Amount',
			'OrderID',
			'CallbackURL',
			'ItemName',
			'ItemQty',
			'ItemPrice',
			'auth',				
			];
			$is_validated=$this->assetHelper->checkpostfields($required_fields,$_POST);

			if(!empty($is_validated)){
			$arr=[	
				'fee_data'=>'Following parmeters are mandatory: '.$is_validated,
				'success'=>false
			];
			
			echo json_encode($arr);
			exit;				
			}*/
			//todo: auth if valid..
			//check merchant if valid 
			

		} else {
			redirect('');
		}
		
	}


	public function process_payment(){

		if(empty($_POST['tokenid'])){

			$arr=[	
				'msg'=>'Please fill up all fields',
				'success'=>false
			];
			
			echo json_encode($arr);

		} 
		$post_data =$this->session->userdata('post_data');
			
		//dd($post_data);Hazel, 10:54 AM

//suncashme123
		$arr=[
		'amount'=>$post_data['Amount'],
		'invoicenumber'=>$post_data['OrderID'],
		'type'=>'Sale',
		'email'=>'suncashme@gmail.com',
		'secretkey'=>CENPOST_MERCHANT_SECRETKEY,
		'merchant'=>CENPOST_MERCHANT_ID,
		'tokenid'=>$_POST['tokenid'],
		];
		//dd($arr);
		$verifying_post='';
		$verify_post=$this->assetHelper->api_requestv2(CENPOS_SITEVERIFY_URL,$arr);
		$verify_post=json_decode($verify_post,true);
		if($verify_post['Result']==0){
			$arr_post=[
				'method'=>'create_suncashme_checkout_log',
				'P01'=>$post_data['MerchantKey'],//MerchantKey
				'P02'=>$post_data['OrderID'],//order_id
				'P03'=>'',//reference_number
				'P04'=>'cenpos',//type
				'P05'=>'success',//response
				'P06'=>json_encode($arr),//response
				'P07'=>json_encode($verify_post),//response
				'return_mode'=>'json'	
			];
			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);


			$verifying_post=$verify_post['Data'];
		} else{///
			$arr_post=[
				'method'=>'create_suncashme_checkout_log',
				'P01'=>$post_data['MerchantKey'],//MerchantKey
				'P02'=>$post_data['OrderID'],//order_id
				'P03'=>'',//reference_number
				'P04'=>'cenpos',//type
				'P05'=>'failed',//response
				'P06'=>json_encode($arr),//response
				'P07'=>json_encode($verify_post),//response
				'return_mode'=>'json'	
			];
			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);

			$arr=[	
				'msg'=>$verify_post['Message'],
				'success'=>false
			];
			
			echo json_encode($arr);			
		}
		$arr_post=[
			'verifyingpost'=>$verifying_post,
			'tokenid'=>$_POST['tokenid'],
		];
		$process_payment=$this->assetHelper->api_requestv2(CENPOS_PROCESS_URL,$arr_post);


		$process_payment=json_decode($process_payment,true);
		//dd($process_payment);
		if($process_payment['Result']==0){
			//update status ..
			$arr_update=[
				'method'=>'update_checkout_transaction',
				'P01'=>$post_data['OrderReference'],//reference 
				'P02'=>'PROCESSED',//status 
				'return_mode'=>'json'	
			];
			$update_status=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr_update);
			//save transaction details....
			$arr_post=[
				'method'=>'create_suncashme_cenpos_transaction_detail',
				'P01'=>$post_data['MerchantKey'],//MerchantKey
				'P02'=>$post_data['OrderID'],//order_id
				'P03'=>$process_payment['ReferenceNumber'],//reference_number
				'P04'=>$_POST['card_number'],//type
				'P05'=>$_POST['name_on_card'],//response
				'P06'=>$_POST['card_type'],//response
				'P07'=>$post_data['Amount'],//response
				'return_mode'=>'json'	
			];
			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);			


			//saving logs....
			$arr_post=[
				'method'=>'create_suncashme_checkout_log',
				'P01'=>$post_data['MerchantKey'],//MerchantKey
				'P02'=>$post_data['OrderID'],//order_id
				'P03'=>$process_payment['ReferenceNumber'],//reference_number
				'P04'=>'cenpos',//type
				'P05'=>'success',//response
				'P06'=>json_encode($arr_post),//response
				'P07'=>json_encode($process_payment),//response
				'return_mode'=>'json'	
			];
			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);
			//dd($save_logs);
			unset($_SESSION['post_data']);
			$arr=[	
				'msg'=>$process_payment['Message'],
				'reference'=>$process_payment['ReferenceNumber'],
				'payment_method'=>'Card',
				'success'=>true
			];
			
			echo json_encode($arr);	
		} else {
			//saving logs....
			$arr_post=[
				'method'=>'create_suncashme_checkout_log',
				'P01'=>$post_data['MerchantKey'],//MerchantKey
				'P02'=>$post_data['OrderID'],//order_id
				'P03'=>'',//reference_number
				'P04'=>'cenpos',//type
				'P05'=>'failed',//response
				'P06'=>json_encode($arr_post),//response
				'P07'=>json_encode($process_payment),//response
				'return_mode'=>'json'	
			];
			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);


			$arr=[	
				'msg'=>$process_payment['Message'],
				'success'=>false
			];
			
			echo json_encode($arr);				
		}

	}

	public function generate_passcode(){
		$required_fields=[
		'MerchantKey',
		'mobile',
		'pin',				
		];
		$is_validated=$this->assetHelper->checkpostfields($required_fields,$_POST);	

		if(!empty($is_validated)){
		$arr=[	
			'msg'=>'Following parmeters are mandatory: '.$is_validated,
			'success'=>false
		];
		
		echo json_encode($arr);
		exit;				
		}

		$arr_post=[
			'method'=>'generate_passcode',
			'P01'=>$this->clean($_POST['mobile']),//
			'P02'=>$_POST['pin'],//
			'P03'=>$_POST['MerchantKey'],//
			'return_mode'=>'json'	
		];
		$get_passcode=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr_post);	
		//dd($get_passcode);
		$get_passcode = json_decode($get_passcode,true);
		if($get_passcode['Success']=='YES'){
			$arr=[	
				'msg'=>'Valid Account.',
				'data'=>$get_passcode,
				'success'=>true
			];
			
			echo json_encode($arr);
			exit;	
		} else {
			$arr=[	
				'msg'=>'Not a Valid Account.',
				'success'=>false
			];
			
			echo json_encode($arr);
			exit;	
		}


	}

	public function process_suncash_payment(){
		if(empty($_POST['mobile']) || empty($_POST['passcode'])){

			$arr=[	
				'msg'=>'Please fill up all fields',
				'success'=>false
			];
			
			echo json_encode($arr);

		} 
		$post_data=$this->session->userdata('post_data');
			
		//dd($post_data);
		//process items...
		$order_details='';
		if(!empty($post_data['ItemName'])){
			$len = count($post_data['ItemName']);
			for ($i=0; $i <count($post_data['ItemName']) ; $i++) { 
				# code...
				if($len - 1==$i){
					$order_details.=$post_data['ItemName'][$i]."|".$post_data['ItemQty'][$i]."|".$post_data['ItemPrice'][$i]."";	
				} else {
					$order_details.=$post_data['ItemName'][$i]."|".$post_data['ItemQty'][$i]."|".$post_data['ItemPrice'][$i]."~";	
				}
				
			}
		}
		//dd($order_details);
		$arr=[
		   'method'=>'suncash_checkout',	
		   'P01' =>$post_data['MerchantKey'], //Merchant API Key
		   'P02' =>$this->clean($_POST['mobile']), //Mobile No
		   'P03' =>$_POST['passcode'], //passcode
		   'P04' =>$post_data['Amount'], //amount
		   'P05' =>'BSD',//currency
		   'P06' =>$post_data['OrderID'], //reference no/order no
		   'P07' =>$order_details,//order details "item_name|qty|price" 
		   'P08'=>$post_data['OrderReference'],
		   'return_mode'=>'json'
		];
		//dd($arr);
		$process_wallet=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
		$process_wallet=json_decode($process_wallet,true);
		//dd($process_wallet);
		if($process_wallet['Success']=="YES"){

			//dd($process_payment);
			//saving logs....
			$reference_number=$process_wallet['ResponseMessage']['TransactionId'];
			$arr_update=[
				'method'=>'update_checkout_transaction',
				'P01'=>$post_data['OrderReference'],//reference 
				'P02'=>'PROCESSED',//status 
				'return_mode'=>'json'	
			];
			$update_status=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr_update);


			$arr_post=[
				'method'=>'create_suncashme_checkout_log',
				'P01'=>$post_data['MerchantKey'],//MerchantKey
				'P02'=>$post_data['OrderID'],//order_id
				'P03'=>$reference_number,//reference_number
				'P04'=>'wallet',//type
				'P05'=>'failed',//response
				'P06'=>json_encode($arr),//request
				'P07'=>json_encode($process_wallet),//response
				'return_mode'=>'json'	
			];
			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);
			//dd($save_logs);
			unset($_SESSION['post_data']);
			$arr=[	
				'msg'=>'Successfully paid',
				'reference'=>$reference_number,
				'success'=>true
			];
			
			echo json_encode($arr);	
			
		} else {
			$arr_post=[
				'method'=>'create_suncashme_checkout_log',
				'P01'=>$post_data['MerchantKey'],//MerchantKey
				'P02'=>$post_data['OrderID'],//order_id
				'P03'=>'',//reference_number
				'P04'=>'wallet',//type
				'P05'=>'failed',//response
				'P06'=>json_encode($arr_post),//response
				'P07'=>json_encode($process_wallet),//response
				'return_mode'=>'json'	
			];
			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);

			$arr=[	
				'msg'=>$process_wallet['ResponseMessage'],
				'success'=>false
			];
			
			echo json_encode($arr);		
		}
	
	}

	public function payment_process_sample(){
		$post_data = $_POST;
		$order_details='';
		if(!empty($post_data['ItemName'])){
			$len = count($post_data['ItemName']);
			for ($i=0; $i <count($post_data['ItemName']) ; $i++) { 
				# code...
				if($len - 1==$i){
					$order_details.=$post_data['ItemName'][$i]."|".$post_data['ItemQty'][$i]."|".$post_data['ItemPrice'][$i]."";	
				} else {
					$order_details.=$post_data['ItemName'][$i]."|".$post_data['ItemQty'][$i]."|".$post_data['ItemPrice'][$i]."~";	
				}
				
			}
		}
		$arr=[
		   'method'=>'payment',	
		   'P01' =>$post_data['MerchantKey'], //Merchant API Key
		   'P02' =>$_POST['MerchantName'], //Mobile No
		   'P03' =>$_POST['Amount'], //passcode
		   'P04' =>$post_data['OrderID'], //amount
		   'P05' =>$post_data['CallbackURL'],//currency
		   'P06' =>$order_details, //reference no/order no
		   'return_mode'=>'json'
		];
		//dd($arr);
		$process_sample=$this->assetHelper->api_requestv2(SUNCASH_CHECKOUT,$arr);
		$process_sample=json_decode($process_sample,true);
		//dd($process_sample);
		if($process_sample['Success']=="YES"){
			$arr=[	
				'msg'=>'Successfully process.',
				'url'=>$process_sample['ResponseMessage']['url'],
				'data'=>$process_sample['ResponseMessage'],
				'success'=>true
			];
			
			echo json_encode($arr);		
		} else {
			$arr=[	
				'msg'=>$process_sample['ResponseMessage'],
				'success'=>false
			];
			
			echo json_encode($arr);		
		}

	}

	public function sample_success($params){
		$params = base64_decode($params);
		list($reference_id,$status,$payment_method) = explode('||',$params);

		$data['reference_id'] = $reference_id;
		$data['status'] = $status;
		$data['payment_method'] = $payment_method;

		$this->blade->view('checkout_return-sample-page',$data);
	}
	public function cancel_page($params,$callbackurl){
		$params = base64_decode($params);
		//dd($params);
		list($reference_id,$status,$payment_method) = explode('||',$params);
		//dd();
		$data['callbackurl']=base64_decode(urldecode($callbackurl));
		$data['reference_id'] = $reference_id;
		$data['status'] = $status;
		$data['payment_method'] = $payment_method;
		//dd($data)
		$this->blade->view('cancelled_page',$data);
	}
	public function cancel_order(){
		$arr_update=[
			'method'=>'update_checkout_transaction',
			'P01'=>$_POST['reference_id'],//reference 
			'P02'=>'CANCELLED',//status 
			'return_mode'=>'json'	
		];
		$update_status=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr_update);
		$update_status=json_decode($update_status,true);
		if($update_status['Success']=='YES'){
			$params=base64_encode($_POST['reference_id']."||failed||cancelled");
			
			$arr=[	
				'msg'=>$update_status['ResponseMessage'],
				'url'=>base_url('payment/cancel_page/'),
				'params'=>$params,
				'success'=>true
			];
			
			echo json_encode($arr);		
		} else {

			$arr=[	
				'msg'=>$update_status['ResponseMessage'],
				'success'=>false
			];
			
			echo json_encode($arr);					
		}

	}


	public function process_cash_checkout(){
		//get temp tokken...
		//dd($_POST);
		$mobile = $this->clean($this->security->xss_clean($this->input->post('mobile_cash')));
		$auth_key=$this->assetHelper->create_tokken();
		//dd($this->session->userdata('post_data'));
	    $arr=[
	    	'method'=>'create_cash_business_billpay',
	    	'P01'=>$auth_key['temp_auth'],//$auth_key['temp_auth'],
            'P02'=> $this->session->userdata('post_data')['merchant_client_id'],//merch id
            'P03'=> $this->session->userdata('post_data')['MerchantName'],//suntag
            'P04'=> str_replace( ',', '',$this->session->userdata('post_data')['Amount']),//amount+fee+vat
            'P05'=> $this->session->userdata('post_data')['reference_id'],
            'P06'=> '',//$this->input->post('notes'),
            'P07'=>	$this->input->post('firstName'),
            'P08'=>	$this->input->post('lastName'),
            'P09'=>	$this->input->post('email'),
            'P10'=>	$mobile,
            'P11'=>	$this->input->post('is_email'),
            'P12'=>	$this->input->post('is_sms'),
            'return_mode'=>'json'

	    ];

	     //dd($arr);

	    $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr);
	  	//dd($api_bt_result);
	    $api_bt_data=json_decode($api_bt_result,true);
	    //dd($api_bt_data);
         if($api_bt_data['ResponseCode']=='0000'){
         	//email...
         	//transaction_id
			//qr_code
			//barcode
			//mobile
			//email
			//creation_date
			if($this->input->post('is_email')==1){
			$qr=Qr::GenerateQr($api_bt_data['ResponseMessage'],'',140);
			$barcode=Qr::GenerateBarcode128($api_bt_data['ResponseMessage'],'','');
			$qr= base64_decode($qr->generate());
			$barcode= base64_decode($barcode);	
			$total=	(float)$this->session->userdata('post_data')['Amount'];//+(float)$this->input->post('hid_fee')+(float)$this->input->post('hid_vat');	
			$arr=[
				"amount"=>$this->session->userdata('post_data')['Amount'],
				"transaction_id"=>$api_bt_data['ResponseMessage'],
				"fee"=>'0.00',//$this->input->post('hid_fee'),
				"vat"=>'0.00',//$this->input->post('hid_vat'),
				"total_amount"=>$total,
				//"qr_code"=>'<img src="data:'.$qr->getContentType().';base64,'.$qr->generate().'" />',
				//"barcode"=>'<img src="data:image/png;base64,'.$barcode.'" />',
				"creation_date"=>date('M d, Y'),
				"email"=>$this->input->post('email'),
				"mobile"=>$this->input->post('mobile_cash'),
				"view"=>'email_payment_code.blade.php',
				"title"=>'Payment Code',
				"profile_pic" => $this->session->userdata("post_data")['profile_pic'],
				//<img src="data:'.$qr->getContentType().';base64,'.$qr->generate().'" />
			];

	        $image_attachment[0]['img_string']=$qr;
	        $image_attachment[0]['img_name']='qr_code';
	        $image_attachment[1]['img_string']=$barcode;
	        $image_attachment[1]['img_name']='barcode';

			$this->pending_email_cash($arr,$image_attachment);

			}
			$this->session->set_userdata('cash_posted_data',$_POST);
			$this->session->set_userdata('cash_payment_code',$api_bt_data['ResponseMessage']);
			$arr=[	
				'msg'=>$api_bt_data['ResponseMessage'],
				'url'=>base_url('payment/cash_payment_success'),
				'success'=>true
			];
			
			echo json_encode($arr);
			exit;         	
         } else {
			$arr=[	
				'msg'=>$api_bt_data['ResponseMessage'],
				'success'=>false
			];
			
			echo json_encode($arr);
			exit;
         }		
	}


	public function clean($string) {
    	// $this->assetHelper->session_checker();
        $string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.
        return preg_replace('/[^A-Za-z0-9]/', '', $string); // Removes special chars.
    }				
}