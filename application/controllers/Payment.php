<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//namespace AmazonPay;
class Payment extends CI_Controller {// put filename of controller on class name 

	function __construct(){//where can put dynamic functions and models....
		parent::__construct();
		include 'vendor/autoload.php';
   		$this->session->flashdata('msg');
   		//load model.
      	$this->load->model('core/db');//for transactions and other eloquent db features
      	$this->load->model('core/Qr');
   		//$this->load->model('user');
   		//$this->load->model('Value_sets_global','global');
		$this->assetHelper = new AssetHelper\AssetHelper();
		$this->amazonpay_config = array(
			'public_key_id' => 'AGWGBAZVOO5IESX4GE6WM6UG',//add to _settings or constant
			'private_key'   => 'AmazonPay_AGWGBAZVOO5IESX4GE6WM6UG.pem',//add to _settings or constant
			'region'        => 'US',
			'sandbox'       => true
		);    
	}
	//for business payment method
	public function index(){//method then routefind
		// dd($_POST);
		if($this->input->post('payment_method')=='wallet' || $this->input->post('payment_method')=='card'){
			$required_input=[
				'amount',
				'payment_method',
				'reference_num'
			];
			$label=[
				'amount'=>'Amount',
				'payment_method'=>'Payment Method',
				'reference_num'=>'Reference Number'
			];
			$validated_fields=$this->assetHelper->checkpostfields($required_input,$_POST,$label);
			//dd($validated_fields);
			if(!empty($validated_fields)){
				$arr=[
					'msg'=>'field required '.$validated_fields,
					'success'=>false
				];

				echo json_encode($arr);
				exit;
			}
		} else {
			$required_input=[
				'amount',
				'payment_method',
			];
			$label=[
				'amount'=>'Amount',
				'payment_method'=>'Payment Method',
			];
			$validated_fields=$this->assetHelper->checkpostfields($required_input,$_POST,$label);
			//dd($validated_fields);
			if(!empty($validated_fields)){
				$arr=[
					'msg'=>'field required '.$validated_fields,
					'success'=>false
				];

				echo json_encode($arr);
				exit;
			}
		}

		//chc



		if($this->input->post('payment_method')=='cash'){
			///dd($_SESSION);
			$this->session->set_userdata('post_data',$_POST);
			$arr=[	
				'url'=>base_url('payment/cash'),
				'success'=>true
			];
			
			echo json_encode($arr);
			exit;	
		} else if($this->input->post('payment_method')=='wallet'){
			$this->session->set_userdata('post_data',$_POST);
				$url=base_url('wallet/login');
			if(isset($_SESSION['customer_info'])){
				$url=base_url('payment/wallet');
			} 

			$arr=[	
				'url'=>$url,
				'success'=>true
			];
			
			echo json_encode($arr);
			exit;	
		} else if($this->input->post('payment_method')=='voucher'){
			$this->session->set_userdata('post_data',$_POST);
			$url=base_url('voucher/main');

			$arr=[	
				'url'=>$url,
				'success'=>true
			];
			
			echo json_encode($arr);
			exit;	
		} else if($this->input->post('payment_method')=='sanddollar'){
			$this->session->set_userdata('post_data',$_POST);
			$url=base_url('sanddollar/main');
			// $this->session->set_userdata('post_data',$_POST);
				// $url=base_url('sanddollar/login');
			// if(isset($_SESSION['sand_customer_info'])){
			// 	$url=base_url('payment/wallet_sanddollar');
			// } 
			$arr=[	
				'url'=>$url,
				'success'=>true
			];
			
			echo json_encode($arr);
			exit;	
		} else if($this->input->post('payment_method')=='sanddollarqr'){
			$this->session->set_userdata('post_data',$_POST);
				$url=base_url('sanddollarqr/main');
			$arr=[	
				'url'=>$url,
				'success'=>true
			];
			
			echo json_encode($arr);
			exit;
		}else if($this->input->post('payment_method')=='amazon'){
			$this->session->set_userdata('post_data',$_POST);
				$url=base_url('amazon/main');
			$arr=[	
				'url'=>$url,
				'success'=>true
			];
			
			echo json_encode($arr);
			exit;
		}else if($this->input->post('payment_method')=='paypal'){
			$this->session->set_userdata('post_data',$_POST);
				$url=base_url('paypal/main');
			$arr=[	
				'url'=>$url,
				'success'=>true
			];
			
			echo json_encode($arr);
			exit;
		}  else if($this->input->post('payment_method')=='card'){
			//check first if order id exist.
		    $arr=[
		    	'method'=>'login_business',
	            'P01'=> SERVICE_ACCOUNT_USERNAME_DEV,
	            'P02'=> SERVICE_ACCOUNT_PASSWORD_DEV,
		    ];
		    //$arr_tosend['url']="login_business?".http_build_query($arr);

		    //dd($arr_tosend);
		    $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
		    $api_bt_data=json_decode($api_bt_result,true);
		    //dd($api_bt_data);
		    $data['login_data']='';
		    if($api_bt_data['ResponseCode']=='YES'){
		    $data['login_data']=$api_bt_data['ResponseMessage'];                     
		    }	


	       $arr=[
	         'method'=>'validate_payment_reference_id',
	            'P01'=> $data['login_data']['SessionID'],
	            'P02'=> $_POST['reference_num'],
	       ];
	       $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
	       $api_bt_data=json_decode($api_bt_result,true);
	       if($api_bt_data['Success']=='NO'){
				$arr=[	
					'msg'=>'Reference Id already Exist.',
					'success'=>false
				];
				
				echo json_encode($arr);
				exit;	       	
	       }

			$this->session->set_userdata('post_data',$_POST);
			$url=base_url('payment/cc_login_page');

			$arr=[	
				'url'=>$url,
				'success'=>true
			];
			
			echo json_encode($arr);
			exit;	
		}
	}
	public function wallet(){//method then routefind //get fee
		if($this->session->userdata('suntag_shortcode')==$this->session->userdata('customer_info')['CustomerTag']){
			$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Cant transact with same suntag. </div>');
			redirect('wallet/login');	
		}
		$data['payment_data'] = $this->session->userdata('post_data');
		if(isset($_SESSION['SessionID'])){
		$amount = 	str_replace( ',', '', $this->session->userdata('post_data')['amount']);
	    $arr=[
	    	'method'=>'get_business_fee',
            'P01'=> $amount,
            'P02'=>  'SUNCASHME_MONEYTRANSFER',
            'P03'=>	'',//$this->session->userdata('suntag_shortcode'),
            'return_mode'=>'json'
	    ];
	    //$arr_tosend['url']="login_business?".http_build_query($arr);
	    //dd($_SESSION);
	    //dd($arr);
	    $api_fee_result=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr);
	    //dd($api_fee_result);
	    $api_fee_data=json_decode($api_fee_result,true);
	    //dd($api_bt_data);
	    $data['fee_data']='';
	    if($api_fee_data['ResponseCode']=='0000'){
	    $data['fee_data']=$api_fee_data['ResponseMessage'];
	    }	
	    $data['Balance'] = $this->assetHelper->get_balance($_SESSION['SessionID']);


	    //dd($data['fee_data']);
		$this->blade->view('wallet-page',$data);
		} else {
		redirect('');
		}
	}

	public function cc_wallet(){//method then routefind //get fee
		// if($this->session->userdata('suntag_shortcode')==$this->session->userdata('customer_info_cc')['CustomerTag']){
		// 	$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Cant transact with same suntag. </div>');
		// 	redirect('wallet/login');	
		// }
		$this->assetHelper->session_checker();
		$data['payment_data'] = $this->session->userdata('post_data');
		if(isset($_SESSION['SessionID_cc'])){
		$amount = 	str_replace( ',', '', $this->session->userdata('post_data')['amount']);
	    $arr=[
	    	'method'=>'get_business_fee',
            'P01'=> $amount,
            'P02'=>  'SUNCASHME_MONEYTRANSFER',
            'P03'=>	'',//$this->session->userdata('suntag_shortcode'),
            'return_mode'=>'json'
	    ];
	    //$arr_tosend['url']="login_business?".http_build_query($arr);
	    //dd($_SESSION);
	    //dd($arr);
	    $api_fee_result=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr);
	    //dd($api_fee_result);
	    $api_fee_data=json_decode($api_fee_result,true);
	    //dd($api_bt_data);
	    $data['fee_data']='';
	    if($api_fee_data['ResponseCode']=='0000'){
	    $data['fee_data']=$api_fee_data['ResponseMessage'];
	    }	
	    $data['Balance'] = $this->assetHelper->get_balance($_SESSION['SessionID_cc']);



		// cc
		$trans_type = $_SESSION['tag']=='CUSTOMER' ? 'CUSTOMER' : 'BUSINESS';
		$user_type = $_SESSION['tag']=='CUSTOMER' ? $_SESSION['suntag_shortcode'] : $_SESSION['merchant_key'];
		$arr=[
			'method'=>'get_payment_fee',
			'P01'=> $user_type,//mercahntkey
			'P02'=>	$amount,
			'P03'=> 'AMAZON',//'SUNCASHMECARD',
			'P04'=> $trans_type,
			'return_mode'=>'json'
		];
			// dd($arr);	
		$api_fee_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
		// dd($api_fee_result);
		$api_fee_data=json_decode($api_fee_result,true);
		$data['convenience_data']='';
		if($api_fee_data['Success']=='YES'){
		$data['convenience_data']=$api_fee_data['ResponseMessage'];                 
		}
		$is_merch=$data['convenience_data']['iscard_fee_on_merch'];
		$ccfee =$data['convenience_data']['total_convenience_fee'];
		if($is_merch>0){
			$ccfee ="0.00";
		}
		$data['ccfee']=$ccfee ; //show value in display




		//get cc data if have card loaded.
		    $arr=[
			'method'=>'get_customer_creditcard',
			'P01'=> $this->session->userdata('SessionID_cc'),
			'P02'=> $this->session->userdata('customer_info_cc')['CustomerId'],
			'P03'=> '',
			'return_mode' => 'json'
			];
			// dd($arr);
			$api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr);
			$api_bt_data=json_decode($api_bt_result,true);
		// dd($api_bt_data);
			$data['cc_data']='';
			if($api_bt_data['ResponseCode']=='0000'){
			$data['cc_data']=$api_bt_data['ResponseMessage'];
			}


		//site verify
		$arr=[
			'merchant'=>CENPOST_MERCHANT_ID,
			'secretkey'=>CENPOST_MERCHANT_SECRETKEY,
			'email'=>'suncashme@gmail.com',
			'ip'=>'127.0.0.1',
		];
		$verify_post=$this->assetHelper->api_requestv2(CENPOS_SITEVERIFY_URL,$arr);
		$verify_post=json_decode($verify_post,true);	
		//dd($verify_post);
		$data['verify_params']=$verify_post;
		$data['customer_id']=$this->session->userdata('customer_info_cc')['CustomerId'];//."_".date('YmdHis')


			// dd($data);
	    // dd($data['fee_data']);
		$this->blade->view('customer-card-page',$data);
		} else {
		redirect('suncashme/'.$this->session->userdata('suntag_shortcode'));
		}
	}

	public function wallet_sanddollar(){//method then routefind //get fee
		if($this->session->userdata('suntag_shortcode')==$this->session->userdata('sand_customer_info')['CustomerTag']){
			$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Cant transact with same suntag. </div>');
			redirect('sanddollar/login');	
		}
		$data['payment_data'] = $this->session->userdata('post_data');
		// dd($this->session->userdata);
		if(isset($_SESSION['Sand_SessionID'])){
		$amount = 	str_replace( ',', '', $this->session->userdata('post_data')['amount']);

	    // dd($data['Balance']);
		$this->blade->view('sanddollar-wallet-page',$data);
		} else {
		redirect('');
		}
	}
	public function voucher_main(){
	
		if(!empty($this->session->userdata('post_data'))){
			$data['payment_data'] = $this->session->userdata('post_data');
			//dd($data['payment_data']);
			$amount = 	str_replace( ',', '', $this->session->userdata('post_data')['amount']);
			$arr=[
				'method'=>'get_business_fee',
				'P01'=> $amount,
				'P02'=>  'SUNCASHME_MONEYTRANSFER',
				'P03'=>	'',//$this->session->userdata('suntag_shortcode'),
				'return_mode'=>'json'
			];
			//$arr_tosend['url']="login_business?".http_build_query($arr);

			//dd($arr);
			$api_fee_result=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr);
			//dd($api_fee_result);
			$api_fee_data=json_decode($api_fee_result,true);
			//dd($api_bt_data);
			$data['fee_data']='';
			if($api_fee_data['ResponseCode']=='0000'){
			$data['fee_data']=$api_fee_data['ResponseMessage'];
			                    
			}
			// dd($data);
			$this->blade->view('voucher-page',$data);	
			
		
		}
	}
	public function sanddollar_main(){
	// dd($_SESSION);
		if(!empty($this->session->userdata('post_data'))){
			$data['payment_data'] = $this->session->userdata('post_data');
			// dd($data['payment_data']);
			$amount = 	str_replace( ',', '', $this->session->userdata('post_data')['amount']);
			$arr=[
				'method'=>'get_business_fee',
				'P01'=> $amount,
				'P02'=>  'SUNCASHME_MONEYTRANSFER',
				'P03'=>	'',//$this->session->userdata('suntag_shortcode'),
				'return_mode'=>'json'
			];
			$api_fee_result=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr);
			$api_fee_data=json_decode($api_fee_result,true);
			$data['fee_data']='';
			if($api_fee_data['ResponseCode']=='0000'){
			$data['fee_data']=$api_fee_data['ResponseMessage'];                   
			}
			$this->load->library('Mobile_Detect');
			if($this->mobile_detect->isMobile()){
				$loc="app";
			}else{
				$loc="web";
			}
			$data['location']=$loc;


		
			if($_SESSION['tag']=='MERCHANT'){
				$transaction_type ='BUSINESS_PAYMENT';
				$key =$_SESSION['key'];
			}else{
				$transaction_type ='CUSTOMER_PAYMENT';
					$arr=[
						'method'=>'login_business',
						'P01'=> SERVICE_ACCOUNT_USERNAME_DEV,
						'P02'=> SERVICE_ACCOUNT_PASSWORD_DEV,
					];
					//$arr_tosend['url']="login_business?".http_build_query($arr);

					//dd($arr_tosend);
					$api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
					$api_bt_data=json_decode($api_bt_result,true);
					//dd($api_bt_data);
					$data['login_data']='';
					if($api_bt_data['ResponseCode']=='YES'){
					$data['login_data']=$api_bt_data['ResponseMessage'];                     
					}	

					$key=$data['login_data']['merchant_key'];
			}





			$mobile = $this->clean($this->security->xss_clean($data['payment_data']['mobile']));
			//get reference 
			$arr=[
				'method'=>'sanDollarPendingTransaction',
				'P01'=> $_SESSION['client_record_id'],
				'P02'=> $key,
				'P03'=>	$amount,
				'P04'=>	'SUNCASHME',
				'P05'=>$transaction_type,
				'P06'=>$data['payment_data']['name'] ,
				'P07'=>$data['payment_data']['email'] ,
				'P08'=> $mobile,			
				'P09'=>$data['payment_data']['notes'] ,							
				'return_mode'=>'json'
			];
			$api_fee_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
					// dd($api_fee_result);
			$sanddata=json_decode($api_fee_result,true);
			// dd($sanddata);
			$data['sand_data']='';
			if($sanddata['Success']=='YES'){
				$data['sand_data']=$sanddata['ResponseMessage'];  
				if(!empty($sanddata['ResponseMessage']['referenceid'])){
					$data['sand_data']['referenceid'] =  $sanddata['ResponseMessage']['referenceid'];
				} else {
					$data['sand_data'] =  $sanddata['ResponseMessage']['data'];
				}
				

			} 	



			// dd(	$data);
			$this->blade->view('sanddollar-page',$data);	
			
		
		}
	}
	public function check_status($reference_id){
		header('Content-Type: text/event-stream');
		header('Cache-Control: no-cache'); // recommended to prevent caching of event data.

		$arr=[
			'method'=>'getSanDollarReferenceStatus',
			'P01'=> $_SESSION['client_record_id'],
			'P02'=> $_SESSION['key'],
			'P03'=> $reference_id,
			'return_mode' => 'json'
		];
		$api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
		// dd($api_bt_result);
		$api_bt_data=json_decode($api_bt_result,true);

		if($api_bt_data['Success']=='YES'){
			$arr =[
				'status'=>$api_bt_data['ResponseMessage']['status'],
				'cron_ref_id'=>$api_bt_data['ResponseMessage']['cron_ref_id'],
				'transaction_id'=>$api_bt_data['ResponseMessage']['transaction_id']
			];
			echo "data:".json_encode($arr)."\n\n";
			//echo "\n\n";
			ob_flush();
			flush();     
		}  else {
			echo 'data:1';
			echo "\n\n";
			ob_flush();
			flush(); 
		}		
	}
	public function check_status_cfid($reference_id){//for checkout
		header('Content-Type: text/event-stream');
		header('Cache-Control: no-cache'); // recommended to prevent caching of event data.

		$arr=[
			'method'=>'getSanDollarReferenceStatus',
			'P01'=> $_SESSION['client_record_id'],
			'P02'=> $_SESSION['key'],
			'P03'=> $reference_id,
			'return_mode' => 'json'
		];
		$api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
		// dd($api_bt_result);
		$api_bt_data=json_decode($api_bt_result,true);

		if($api_bt_data['Success']=='YES'){
			
			$params=base64_encode($api_bt_data['ResponseMessage']['cron_ref_id']."||".$reference_id."||success||sanddollar");

			$arr =[
				'status'=>$api_bt_data['ResponseMessage']['0']['status'],
				'cron_ref_id'=>$api_bt_data['ResponseMessage']['0']['cron_ref_id'],
				'params'=>$params
			];
			echo "data:".json_encode($arr)."\n\n";;
			//echo "\n\n";
			ob_flush();
			flush();     
		}  else {
			echo 'data:1';
			echo "\n\n";
			ob_flush();
			flush(); 
		}		
	}	
	public function sanddollarqr_main(){
	
		if(!empty($this->session->userdata('post_data'))){
			$data['payment_data'] = $this->session->userdata('post_data');
			$amount = 	str_replace( ',', '', $this->session->userdata('post_data')['amount']);
			$this->blade->view('sanddollarqr-page',$data);	
			
		
		}
	}	
	public function amazon_main(){
	
		if(!empty($this->session->userdata('post_data'))){
			$data['payment_data'] = $this->session->userdata('post_data');
			//set session
			//$this->session->set_userdata('amz_amount',$this->session->userdata('payment_data')['amount']);
			// dd($data['payment_data']);
			$reference_num =$this->session->userdata('post_data')['reference_num'];
			// dd($reference_num);
			$amount = 	str_replace( ',', '', $this->session->userdata('post_data')['amount']);


			$trans_type = $_SESSION['tag']=='CUSTOMER' ? 'CUSTOMER' : 'BUSINESS';
			$user_type = $_SESSION['tag']=='CUSTOMER' ? $_SESSION['suntag_shortcode'] : $_SESSION['merchant_key'];
			$arr=[
				'method'=>'get_payment_fee',
				'P01'=> $user_type,//mercahntkey
				'P02'=>	$amount,
				'P03'=> 'AMAZON',
				'P04'=> $trans_type,
				'return_mode'=>'json'
			];
			$api_fee_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
			// dd($api_fee_result);
			$api_fee_data=json_decode($api_fee_result,true);
			$data['convenience_data']='';
			if($api_fee_data['Success']=='YES'){
			$data['convenience_data']=$api_fee_data['ResponseMessage'];                 
			}
			$is_merch=$data['convenience_data']['iscard_fee_on_merch'];
			$ccfee =$data['convenience_data']['total_convenience_fee'];
			if($is_merch>0){
				$ccfee ="0.00";
			}
			$data['ccfee']=number_format($ccfee,2) ; //show value in display

	


			$arr=[
				'method'=>'get_business_fee',
				'P01'=> $amount,
				'P02'=>  'SUNCASHME_MONEYTRANSFER',
				'P03'=>	'',
				'return_mode'=>'json'
			];
			$api_fee_result=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr);

			$api_fee_data=json_decode($api_fee_result,true);
			$data['fee_data']='';
			if($api_fee_data['ResponseCode']=='0000'){
			$data['fee_data']=$api_fee_data['ResponseMessage'];
			                    
			}			


		  	// $processing_fee =$data['fee_data']['processing_fee'];
			// $transaction_fee =$data['fee_data']['transaction_fee'];
			// $total_due =$data['fee_data']['total_due'];
			// $is_charge_to_merch =$data['fee_data']['iscard_fee_on_merch'];

			// $convenience_total=number_format($transaction_fee,2,'.','')+number_format($processing_fee,2,'.','');
			// $cc_fee=$convenience_total;
			// if($is_charge_to_merch>0){
			// 	$cc_fee=0.00;
			// }

			// $data['pf']=number_format($processing_fee,2,'.','');
			// $data['tf']=number_format($transaction_fee,2,'.','');
			// $data['ccfee']=$cc_fee;
			// $data['total']=number_format($total_due,2,'.','');
			// $data['is_merch']=$is_charge_to_merch;
			//amazon
			$suntag=$_SESSION['suntag_shortcode'];
			$payload = array(
				'webCheckoutDetails' => array(
					'checkoutReviewReturnUrl' => 'http://localhost/suncashme/payment/amazon_main?status=review', //review
					'checkoutResultReturnUrl' => 'http://localhost/suncashme/payment/amazon_main?status=pay',//complete
				),
				'storeId' => 'amzn1.application-oa2-client.ced8ec472a0644e0ae67c7c2de8c767c',
				'merchantMetadata' => array(
					'merchantReferenceId' =>$reference_num,//checkoutreferenceid
					'merchantStoreName' =>'SunCashME',
					'noteToBuyer' => 'Thank you for your order!'
				)
			);
			// dd($payload);
			$headers = array('x-amz-pay-Idempotency-Key' => uniqid());
			$requestResult = [
				'error' => 1,
				'msg' => 'Error. Can not create checkout session.',
				'signature' => null,
				'payload' => null
			];

			$client = new Amazon\Pay\API\Client($this->amazonpay_config);
			$resultCheckOut = $client->createCheckoutSession($payload, $headers);
			$resultSignature = $client->generateButtonSignature($payload);
			if($resultCheckOut['status'] !== 201) {

				$arr=[  
					'msg'=>'Invalid Request',
					'amz_msg'=>$requestResult,
					'success'=>false
				];
				
				echo json_encode($arr);
				exit;  
			}else {

				$this->session->set_userdata('amz_sig',$resultSignature);
				$this->session->set_userdata('amz_payload',$payload);
				$this->session->set_userdata('amz_checkout',$resultCheckOut);
				$this->session->set_userdata('amz_id',$headers);
				$data['signature']= $resultSignature;
				$data['payload']= $payload;
				$data['amz_order_deatils']= '';
				$data['amz_status']=isset($_GET['status']) ? $_GET['status'] : '';
				if(isset($_GET['amazonCheckoutSessionId'])){
					$this->session->set_userdata('amz_sess',$_GET['amazonCheckoutSessionId']);
					$payload='';
					$headers = array('x-amz-pay-Idempotency-Key' => $this->session->userdata('amz_id'));

					try {	
					$checkoutSessionId = $_GET['amazonCheckoutSessionId'];
					// dd($this->amazonpay_config);
					$client = new Amazon\Pay\API\Client($this->amazonpay_config);
					$result = $client->getCheckoutSession($checkoutSessionId);
						if ($result['status'] === 200) {
							$response = json_decode($result['response'], true);
							//dd($response);
							$data['amz_order_deatils']=$response;
							$checkoutSessionState = $response['statusDetails']['state'];
							$chargeId = $response['chargeId'];
							$chargePermissionId = $response['chargePermissionId'];

							if($_GET['status']=='review'){
							$buyer['buyername'] = $response['buyer']['name'];
							$buyer['buyeremail'] = $response['buyer']['email'];
							$this->session->set_userdata('amz_sess_buyer',$buyer);
							}
						} else {
							// echo 'status=' . $result['status'] . '; response=' . $result['response'] . "\n";
				
							$arr=[  
								'msg'=>'Invalid Request',
								'amz_msg'=>$result['response'],
								'success'=>false
							];
							
							echo json_encode($arr);
							exit;  
						}
						//dd($this->session->userdata('amz_amount'));
						if($this->session->userdata('amz_amount')>0){
							$data['payment_data']['amount']=$this->session->userdata('amz_amount');
						}
						
					} catch (\Exception $e) {
						// handle the exception
						echo $e . "\n";
					}
				} else {
						$this->session->set_userdata('amz_amount',0.00);
						$data['payment_data'] = $this->session->userdata('post_data');
				}
				// dd($data);
				$this->blade->view('amazon-page',$data);	
			}
		}
	}
	public function paypal_main(){
	
		if(!empty($this->session->userdata('post_data'))){

			$data['payment_data'] = $this->session->userdata('post_data');
			$amount = 	str_replace( ',', '', $this->session->userdata('post_data')['amount']);


			$trans_type = $_SESSION['tag']=='CUSTOMER' ? 'CUSTOMER' : 'BUSINESS';
			$user_type = $_SESSION['tag']=='CUSTOMER' ? $_SESSION['suntag_shortcode'] : $_SESSION['merchant_key'];
			$arr=[
				'method'=>'get_payment_fee',
				'P01'=> $user_type,//mercahntkey
				'P02'=>	$amount,
				'P03'=> 'AMAZON',
				'P04'=> $trans_type,
				'return_mode'=>'json'
			];
				// dd($arr);
			$api_fee_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
			// dd($api_fee_result);
			$api_fee_data=json_decode($api_fee_result,true);
			$data['convenience_data']='';
			if($api_fee_data['Success']=='YES'){
			$data['convenience_data']=$api_fee_data['ResponseMessage'];                 
			}
			$is_merch=$data['convenience_data']['iscard_fee_on_merch'];
			$ccfee =$data['convenience_data']['total_convenience_fee'];
			if($is_merch>0){
				$ccfee ="0.00";
			}
			$data['ccfee']=$ccfee ; //show value in display

			$arr=[
				'method'=>'get_business_fee',
				'P01'=> $amount,
				'P02'=>  'SUNCASHME_MONEYTRANSFER',
				'P03'=>	'',
				'return_mode'=>'json'
			];
			$api_fee_result=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr);
			$api_fee_data=json_decode($api_fee_result,true);
			$data['fee_data']='';
			if($api_fee_data['ResponseCode']=='0000'){
			$data['fee_data']=$api_fee_data['ResponseMessage'];
			                    
			}
			// dd($data);
			$this->blade->view('paypal-page',$data);	
		}
	}
	public function link_sandcard(){
		//get temp tokken...
		//dd($_POST);
	    $arr=[
	    	'method'=>'customerSandDollarPairDevice',
	    	'P01'=> $_SESSION['SessionID'],
            'P02'=> $this->input->post('PIN'),
			'P03'=> $this->input->post('OTP'),
			'P04'=> $this->input->post('card_number'),
			'P05'=>	$this->session->userdata('sand_customer_info')['CustomerId'],
            'return_mode'=>'json'

		];
		// dd($arr);
		$api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr);
	    $api_bt_data=json_decode($api_bt_result,true);
		$data['linked_data']='';
		if($api_bt_data['ResponseCode']=='0000'){
		$data['linked_data']=$api_bt_data['ResponseMessage'];				
			//dd($data['linked_data']);
			$arr=[	
				'msg'=>$api_bt_data['ResponseMessage'],
				'success'=>true
			];
			
			echo json_encode($arr);
			exit;         	
         } else {
			$arr=[	
				'msg'=>$api_bt_data['ResponseMessage'],
				'success'=>false
			];
			
			echo json_encode($arr);
			exit;
         }		
	}		
	// public function sanddollar_usecard_process(){
	// 	//get temp tokken...
	// 	// dd($_SESSION);
	// 	// if($_SESSION['tag']=='CUSTOMER'){
	// 		// $mobile= $_SESSION['mobile_suntag'];	
			
	// 	// }else{
	// 		$mobile= $_SESSION['sand_customer_info']['MobileNumber'];	
	// 	// }
	// 	// dd($mobile);
	// 	// $receiver_link = 'suncashvc2'.'%2b'.$mobile.'bs.nzia';
	// 	$user_type = $_SESSION['tag']=='CUSTOMER' ? $_SESSION['suntag_shortcode'] : $_SESSION['merchant_key'];
	//     $arr=[
	//     	'method'=>'sandDollarGenerateTransactionID',
	//     	'P01'=> $_SESSION['Sand_SessionID'],
    //         'P02'=> 'SunCashMe',
    //         'P03'=> 'SEND',//$this->input->post('type')
    //         'return_mode'=>'json'

	// 	];
	// 	// dd($arr);
	// 	$api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr);
	// 			// dd($api_bt_result);
	//     $api_bt_data=json_decode($api_bt_result,true);
	// 	$data['sandtransid']='';
	// 	if($api_bt_data['ResponseCode']=='0000'){
	// 	$data['sandtransid']=$api_bt_data['ResponseMessage'];				
	// 	}
	// 	// if($this->input->post('can_setup_cusomtername')==0){
	// 	// 	$customname='true';
	// 	// }else{
	// 	// 	$customname='false';
	// 	// }
	// 	//suncashvc2 639954754833@bs.nzia
	// 	//suncashvc2 639954754833@bs.nzia
	// 	// dd('suncashvc2 '.$mobile.'@bs.nzia');
	// 	$value = 'suncashvc2+';
	// 	$url_encode = urlencode($value);
	// 	$x= $url_encode.$mobile."@bs.nzia";
	// 	// dd($x);
	//     $arr_process=[
	//     	'method'=>'sandDollarPay',
	//     	'P01'=> $_SESSION['Sand_SessionID'],
    //         'P02'=> $x, 
    //         'P03'=> $this->input->post('amount'),
    //         'P04'=> $data['sandtransid']['transaction_id'],
    //         'P05'=>'false',
    //         'return_mode'=>'json'

	// 	];
	// 		// $arr_tosend['url']="sandDollarPay?".http_build_query($arr_process);
	// 	// dd($arr_process);
	// 	$api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_process);
	// 	// dd($arr_process);
	// 	$api_bt_data=json_decode($api_bt_result,true);
	// 	$data['sanddollar_data']='';
    //      if($api_bt_data['ResponseCode']=='0000'){
			 
	// 		$data['sanddollar_data']=$api_bt_data['ResponseMessage'];

	// 		$arr_post=[
	// 			'method'=>'create_suncashme_checkout_log',
	// 			'P01'=>$user_type,//MerchantKey
	// 			'P02'=>$data['sandtransid']['transaction_id'],//order_id
	// 			'P03'=>'',//reference_number
	// 			'P04'=>'sanddollar-payment',//type
	// 			'P05'=>'success',//response
	// 			'P06'=>json_encode($arr_process),//request
	// 			'P07'=>json_encode($api_bt_result),//response
	// 			'P08'=>'Payment Successful',//response
	// 			'return_mode'=>'json'	
	// 		];
	// 		$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);


	// 		$arr=[	
	// 			'msg'=>$api_bt_data['ResponseMessage'],
	// 			'success'=>true
	// 		];
			
	// 		echo json_encode($arr);
	// 		exit;         	
    //      } else {

	// 		$arr_post=[
	// 			'method'=>'create_suncashme_checkout_log',
	// 			'P01'=>$user_type,//MerchantKey
	// 			'P02'=>$data['sandtransid']['transaction_id'],//order_id
	// 			'P03'=>'',//reference_number
	// 			'P04'=>'sanddollar-payment',//type
	// 			'P05'=>'success',//response
	// 			'P06'=>json_encode($arr_process),//request
	// 			'P07'=>json_encode($api_bt_result),//response
	// 			'P08'=>'Payment Successful',//response
	// 			'return_mode'=>'json'	
	// 		];
	// 		$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);


	// 		$arr=[	
	// 			'msg'=>$api_bt_data['ResponseMessage'],
	// 			'success'=>false
	// 		];
			
	// 		echo json_encode($arr);
	// 		exit;
    //      }		
	// }			
	public function card_main(){//method then routefind //get fee
		if($_SESSION['tag']=='CUSTOMER'){
			if($_SESSION['kyc_type']!='full'){
				redirect('');
			}

		}

		if(!empty($this->session->userdata('post_data'))){
		$data['payment_data'] = $this->session->userdata('post_data');
		//dd($data['payment_data']);
		$amount = 	str_replace( ',', '', $this->session->userdata('post_data')['amount']);
	    $arr=[
	    	'method'=>'get_business_fee',
	        'P01'=> $amount,
	        'P02'=>  'SUNCASHME_MONEYTRANSFER',
	        'P03'=>	'',//$this->session->userdata('suntag_shortcode'),
	        'return_mode'=>'json'
	    ];
	    //$arr_tosend['url']="login_business?".http_build_query($arr);

	    //dd($arr);
	    $api_fee_result=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr);
	    //dd($api_fee_result);
	    $api_fee_data=json_decode($api_fee_result,true);
	    //dd($api_bt_data);
	    $data['fee_data']='';
	    if($api_fee_data['ResponseCode']=='0000'){
	    $data['fee_data']=$api_fee_data['ResponseMessage'];                     
	    }	
		//site verify
		$arr=[
			'merchant'=>CENPOST_MERCHANT_ID,
			'secretkey'=>CENPOST_MERCHANT_SECRETKEY,
			'email'=>'suncashme@gmail.com',
			'ip'=>'127.0.0.1',
		];
		$verify_post=$this->assetHelper->api_requestv2(CENPOS_SITEVERIFY_URL,$arr);
		$verify_post=json_decode($verify_post,true);	
		//dd($verify_post);
		$data['verify_params']=$verify_post;
		$this->blade->view('card-page',$data);
		} else {
			redirect('');
		}
	}

	public function card_mainpage(){//method then routefind //get fee
		if($_SESSION['tag']=='CUSTOMER'){
			if($_SESSION['kyc_type']!='full'){
				redirect('');
			}

		}

		if(!empty($this->session->userdata('post_data'))){
		$data['payment_data'] = $this->session->userdata('post_data');
		//dd($data['payment_data']);
		$amount = 	str_replace( ',', '', $this->session->userdata('post_data')['amount']);
	    $arr=[
	    	'method'=>'get_business_fee',
	        'P01'=> $amount,
	        'P02'=>  'SUNCASHME_MONEYTRANSFER',
	        'P03'=>	'',//$this->session->userdata('suntag_shortcode'),
	        'return_mode'=>'json'
	    ];
	    //$arr_tosend['url']="login_business?".http_build_query($arr);

	    //dd($arr);
	    $api_fee_result=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr);
	    //dd($api_fee_result);
	    $api_fee_data=json_decode($api_fee_result,true);
	    //dd($api_bt_data);
	    $data['fee_data']='';
	    if($api_fee_data['ResponseCode']=='0000'){
	    $data['fee_data']=$api_fee_data['ResponseMessage'];                     
	    }	
		//site verify
		$arr=[
			'merchant'=>CENPOST_MERCHANT_ID,
			'secretkey'=>CENPOST_MERCHANT_SECRETKEY,
			'email'=>'suncashme@gmail.com',
			'ip'=>'127.0.0.1',
		];
		$verify_post=$this->assetHelper->api_requestv2(CENPOS_SITEVERIFY_URL,$arr);
		$verify_post=json_decode($verify_post,true);	
		//dd($verify_post);
		$data['verify_params']=$verify_post;
		$this->blade->view('customer-card-page',$data);
		} else {
			redirect('');
		}
	}	
	public function card_success(){//method then routefind //get fee

	    //dd($data['fee_data']);
		$this->blade->view('card_payment_success',$data);

	}	
	public function cash(){//method then routefind
		//$data['payment_data'] = $this->session->userdata('post_data');

		$data['payment_data'] = $this->session->userdata('post_data');
		$amount = 	str_replace( ',', '', $this->session->userdata('post_data')['amount']);
	    $arr=[
	    	'method'=>'get_business_fee',
            'P01'=> $amount,
            'P02'=>  'SUNCASHME_MONEYTRANSFER',
            'P03'=>	'',//$this->session->userdata('suntag_shortcode'),
            'return_mode'=>'json'
	    ];
	    //$arr_tosend['url']="login_business?".http_build_query($arr);

	    //dd($arr);
	    $api_fee_result=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr);
	    //dd($api_fee_result);
	    $api_fee_data=json_decode($api_fee_result,true);
	    //dd($api_bt_data);
	    $data['fee_data']='';
	    if($api_fee_data['ResponseCode']=='0000'){
	    $data['fee_data']=$api_fee_data['ResponseMessage'];                     
	    }	

	    //dd($data['fee_data']);		
		$this->blade->view('cash-page',$data);

	}
	public function process_cash(){
		//get temp tokken...
		//dd($_POST);
		$mobile = $this->clean($this->security->xss_clean($this->input->post('mobile')));
		$auth_key=$this->assetHelper->create_tokken();
	    $arr=[
	    	'method'=>'create_cash_business_billpay',
	    	'P01'=>$auth_key['temp_auth'],//$auth_key['temp_auth'],
            'P02'=> $this->session->userdata('client_record_id'),//merch id
            'P03'=> $this->session->userdata('suntag_shortcode'),//suntag
            'P04'=> str_replace( ',', '',$this->input->post('amount')),//amount+fee+vat
            'P05'=> $this->input->post('reference_num'),
            'P06'=> $this->input->post('notes'),
            'P07'=>	$this->input->post('fname'),
            'P08'=>	$this->input->post('lname'),
            'P09'=>	$this->input->post('email'),
            'P10'=>	$mobile,
            'P11'=>	$this->input->post('is_email'),
            'P12'=>	$this->input->post('is_sms'),
            'P13'=>	0.00,
            'P14'=>	0.00,
            'P15'=>	0.00,
            'return_mode'=>'json'

	    ];

	     //dd($arr);

	    $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr);
	  	// dd($api_bt_result);
	    $api_bt_data=json_decode($api_bt_result,true);
	    //dd($api_bt_data);
         if($api_bt_data['ResponseCode']=='0000'){
         	//email...
         	//transaction_id
			//qr_code
			//barcode
			//mobile
			//email
			//creation_date
			if($this->input->post('is_email')==1){
			$qr=Qr::GenerateQr($api_bt_data['ResponseMessage'],'',140);
			$barcode=Qr::GenerateBarcode128($api_bt_data['ResponseMessage'],'','');
			$qr= base64_decode($qr->generate());
			$barcode= base64_decode($barcode);	
			$total=	(float)$this->input->post('amount')+(float)$this->input->post('hid_fee')+(float)$this->input->post('hid_vat');	
			$arr=[
				"amount"=>$this->input->post('amount'),
				"transaction_id"=>$api_bt_data['ResponseMessage'],
				"fee"=>$this->input->post('hid_fee'),
				"vat"=>$this->input->post('hid_vat'),
				"total_amount"=>$total,
				//"qr_code"=>'<img src="data:'.$qr->getContentType().';base64,'.$qr->generate().'" />',
				//"barcode"=>'<img src="data:image/png;base64,'.$barcode.'" />',
				"creation_date"=>date('M d, Y'),
				"email"=>$this->input->post('email'),
				"mobile"=>$this->input->post('mobile'),
				"view"=>'email_payment_code.blade.php',
				"title"=>'Payment Code',
				"profile_pic" => $this->session->userdata("ImageURL")
				//<img src="data:'.$qr->getContentType().';base64,'.$qr->generate().'" />
			];

	        $image_attachment[0]['img_string']=$qr;
	        $image_attachment[0]['img_name']='qr_code';
	        $image_attachment[1]['img_string']=$barcode;
	        $image_attachment[1]['img_name']='barcode';

			$this->pending_email_cash($arr,$image_attachment);

			}
			$arr=[	
				'msg'=>$api_bt_data['ResponseMessage'],
				'success'=>true
			];
			
			echo json_encode($arr);
			exit;         	
         } else {
			$arr=[	
				'msg'=>$api_bt_data['ResponseMessage'],
				'success'=>false
			];
			
			echo json_encode($arr);
			exit;
         }		
	}	

	public function process_wallet(){
		//get temp tokken...
		//dd($_POST);
		$auth_key=$this->assetHelper->create_tokken();
		//check money
		$current_balance = $this->assetHelper->get_balance($_SESSION['SessionID']);
		$amt=	str_replace( ',', '', $this->input->post('amount_val'));// total amount with fees
		$amt1=	str_replace( ',', '', $this->input->post('amount'));// total amount with fees
		// $amt=$this->input->post('amount_val');
		if($this->input->post('amount')==0){
			$arr=[	
				'msg'=>'Amount should be greater than 0',
				'success'=>false,
			];
			
			echo json_encode($arr);
			exit;			
		}
		if($_SESSION['tag']=='CUSTOMER'){
		// entered amount only// the fees will be deducted to customer
			$amt=str_replace( ',', '', $this->input->post('amount'));
		}
		if($current_balance<$amt ){
			
			$arr=[	
				'msg'=>'Ooops! Sorry you do not have enough balance to continue this transaction. Please reload you wallet account.',
				'current_balance'=>number_format($current_balance,2),
				'success'=>false
			];
			
			echo json_encode($arr);
			exit;
		}
		// if($this->session->userdata('suntag_shortcode')==$amt){
			
		// 	$arr=[	
		// 		'msg'=>'Ooops! Sorry you do not have enough balance to continue this transaction. Please reload you wallet account.',
		// 		'current_balance'=>number_format($current_balance,2),
		// 		'success'=>false
		// 	];
			
		// 	echo json_encode($arr);
		// 	exit;
		// }
		if($_SESSION['tag']=='CUSTOMER'){
			//process_business_payment
			//dd($arr);
			// P01: session
			// P02: payee customer id 
			// P03: suntag
			// P04: payor card_number//mobile nung nka session
			// P05: amount
			// P06: reference_number
			// P07 : Customer Name //sessino
			// P08: Payee Name  //suntag name
			// P09:  Customer Email//session
			// P10: source
			// P11: notes
			$arr_r=[
				'method'=>'process_business_payment',
				'P01'=> $this->session->userdata('SessionID'),//'a84cdf754f7708b374f41ff82fcded41d997d167',//
				'P02'=> $this->session->userdata('client_record_id'),//merch id or custoemr id
				'P03'=> $this->session->userdata('suntag_shortcode'),//suntag
				'P04'=> $this->session->userdata('customer_info')['MobileNumber'],//card
				'P05'=> $this->input->post('amount'),
				'P06'=> $this->input->post('reference_num'),
				'P07'=>$this->session->userdata('customer_info')['CustomerName'],
				'P08'=>$this->session->userdata('dba_name'),
				'P09'=>$this->session->userdata('customer_info')['Email'],
				"P10"=>'suncashme-wallet',
				"P11"=>$this->input->post('notes'),
				'return_mode'=>'json'
			];

			
		} else {
			$arr_r=[
				'method'=>'create_wallet_business_billpay',
				'P01'=>$auth_key['temp_auth'],//'a84cdf754f7708b374f41ff82fcded41d997d167',//
				'P02'=> $this->session->userdata('client_record_id'),//merch id
				'P03'=> $this->session->userdata('suntag_shortcode'),//suntag
				'P04'=> $this->session->userdata('customer_info')['CustomerName'],//customer name
				'P05'=> $this->session->userdata('customer_info')['MobileNumber'],
				'P06'=> $this->session->userdata('customer_info')['Email'],//email?
				'P07'=> $this->input->post('amount'),
				'P08'=>$this->input->post('reference_num'),
				'P09'=>$this->input->post('notes'),
				"P10"=>$this->input->post('hid_fee'),
				"P11"=>$this->input->post('hid_vat'),  
				'P12'=>$this->input->post('name'),
				"P13"=>$this->input->post('email'),
				"P14"=>$this->clean($_POST['mobile']),
				'return_mode'=>'json'
			];
			
		}
		// dd($arr_r);



	    $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_r);
	  	//dd($api_bt_result);
	    $api_bt_data=json_decode($api_bt_result,true);
		//dd($api_bt_data);
		if($api_bt_data['ResponseCode']=='0000'){
			//email customer.
			 
		$TransactionID = $_SESSION['tag']=='CUSTOMER' ? $api_bt_data['ResponseMessage']['TransactionID'] : $api_bt_data['ResponseMessage'];
		$arr=[
			"customer_name"=>$this->session->userdata('customer_info')['CustomerName'],
			"amount"=>number_format($amt,2),
			"transaction_id"=>$TransactionID,
			"dba_name"=>$this->session->userdata('dba_name'),
			"creation_date"=>date('M d, Y'),
			"reference_num"=>$this->input->post('reference_num'),
			"notes"=>$this->input->post('notes'),
			"balance"=>$this->input->post('balance'),
			"fee"=>number_format($this->input->post('hid_fee'),2),
			"vat"=>number_format($this->input->post('hid_vat'),2),
			"total"=>number_format($this->input->post('amount_val'),2),  //amount +fee+vat
			"msg_above"=>"<h2>You've sent <span class='amount'>".number_format($amt,2)." BSD</span> to ".$this->session->userdata('dba_name').".</h2>",
			"msg_bottom"=>"<p>Transaction is <span class='status'>PENDING</span> until ".$this->session->userdata('dba_name')." accepts payment!</p>",
			"email"=>$this->session->userdata('customer_info')['Email'],
			"view"=>'email_message.blade.php',
			"title"=>'Pending Payment',
			"profile_pic" => $this->session->userdata("ImageURL")
		];
		//dd($arr);	
		$this->pending_email($arr);
		//email merchant
		$arr=[
			"customer_name"=>$this->session->userdata('dba_name'),
			"amount"=>number_format($this->input->post('amount_val'),2),
			"transaction_id"=>$TransactionID,
			"dba_name"=>$this->session->userdata('dba_name'),
			"creation_date"=>date('M d, Y'),
			"reference_num"=>$this->input->post('reference_num'),
			"notes"=>$this->input->post('notes'),
			"balance"=>$this->input->post('balance'),
			"fee"=>$this->input->post('hid_fee'),
			"vat"=>$this->input->post('hid_vat'),
			"total"=>number_format($this->input->post('amount_val'),2),  //amount +fee+vat
			"msg_above"=>"<h2>".$this->session->userdata('customer_info')['CustomerName']." sent you <span class='amount'>".number_format($this->input->post('amount_val'),2)." BSD</span></h2>",
			"msg_bottom"=>"<p>Transaction is <span class='status'>PENDING</span> until ".$this->session->userdata('dba_name')." accepts payment!</p>",
			"email"=>$this->session->userdata('email'),
			"view"=>"email_message_merchant.blade.php",
			"title"=>'Incoming Payment',
			"profile_pic" => $this->session->userdata("profile_pic")
		];

		$this->pending_email($arr);

		$user_type = $_SESSION['tag']=='CUSTOMER' ? $_SESSION['suntag_shortcode'] : $_SESSION['merchant_key'];

		$arr_post=[
			'method'=>'create_suncashme_checkout_log',
			'P01'=>$user_type,//MerchantKey
			'P02'=>$this->input->post('reference_num'),//order_id
			'P03'=>$TransactionID,//reference_number
			'P04'=>'wallet-payment',//type
			'P05'=>'success',//response
			'P06'=>json_encode($arr_r),//request
			'P07'=>json_encode($api_bt_data),//response
			'P08'=>'Payment Successful',//response
			'return_mode'=>'json'
		];
			// dd($arr_post);
		$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);
			dd($save_logs);
		//get t idvia tag
		$tid=$api_bt_data['ResponseMessage'];
		if($_SESSION['tag']=='CUSTOMER'){
			$tid=$api_bt_data['ResponseMessage']['TransactionID'];	
		} 
		$arr=[
			'msg'=>$tid,
			'current_balance'=>'',
			'success'=>true
		];

		echo json_encode($arr);
		exit;
		} else {
		$user_type = $_SESSION['tag']=='CUSTOMER' ? $_SESSION['suntag_shortcode'] : $_SESSION['merchant_key'];

		$arr_post=[
			'method'=>'create_suncashme_checkout_log',
			'P01'=>$user_type,//MerchantKey
			'P02'=>$this->input->post('reference_num'),//order_id
			'P03'=>'',//reference_number
			'P04'=>'wallet-payment',//type
			'P05'=>'failed',//response
			'P06'=>json_encode($arr_r),//request
			'P07'=>json_encode($api_bt_data),//response
			'P08'=>'Payment Failed',//response
			'return_mode'=>'json'	
		];
		$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);



		$arr=[	
			'msg'=>$tid,
			'current_balance'=>'',
			'success'=>false,
		];
		
		echo json_encode($arr);
		exit;

		}			
	}

	public function process_fee(){
		
		    $arr=[
		    	'method'=>'get_business_fee',
	            'P01'=> $this->input->post('amount'),
	            'P02'=>  'SUNCASHME_MONEYTRANSFER',
	            'P03'=>	'',//$this->session->userdata('suntag_shortcode'),
	            'return_mode'=>'json'
		    ];
		    //$arr_tosend['url']="login_business?".http_build_query($arr);

		    //dd($arr);
		    $api_fee_result=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr);
		    //dd($api_fee_result);
		    $api_fee_data=json_decode($api_fee_result,true);
		    //dd($api_bt_data);
		    $data['fee_data']='';
		    if($api_fee_data['ResponseCode']=='0000'){
		    $data['fee_data']=$api_fee_data['ResponseMessage'];
		    }
			$arr=[
				'fee_data'=>$data['fee_data'],
				'success'=>true
			];

			echo json_encode($arr);
			exit;
	}
	public function process_fee_data(){
			$trans_type = $_SESSION['tag']=='CUSTOMER' ? 'CUSTOMER' : 'BUSINESS';
			$user_type_logs = $_SESSION['tag']=='CUSTOMER' ? $_SESSION['suntag_shortcode'] : $_SESSION['merchant_key'];
			$amount =str_replace(',', '', $_POST['amount']);
			$arr=[
				'method'=>'get_payment_fee',
				'P01'=> $user_type_logs,//mercahntkey
				'P02'=>	$amount,
				'P03'=> 'AMAZON',
				'P04'=> $trans_type,
				'return_mode'=>'json'
			];
			$api_fee_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
			// dd($api_fee_result);
			$api_fee_data=json_decode($api_fee_result,true);
			$data['convenience_data']='';
			if($api_fee_data['Success']=='YES'){
			$data['convenience_data']=$api_fee_data['ResponseMessage'];                 
			}

			$is_merch=$data['convenience_data']['iscard_fee_on_merch'];
			$tf=$data['convenience_data']['transaction_fee'];
			$pf=$data['convenience_data']['processing_fee'];
			$ccfee=$data['convenience_data']['total_convenience_fee'];
			$total=$data['convenience_data']['total_due'];
			if($is_merch>0){
				$ccfee =0.00;
			}


		    $arr=[
		    	'method'=>'get_business_fee',
	            'P01'=> $this->input->post('amount'),
	            'P02'=>  'SUNCASHME_MONEYTRANSFER',
	            'P03'=>	'',//$this->session->userdata('suntag_shortcode'),
	            'return_mode'=>'json'
		    ];
			$api_fee_result2=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr);
						// dd($api_fee_result2);
		    $api_fee_data=json_decode($api_fee_result2,true);
		    $data['fee_data']='';
		    if($api_fee_data['ResponseCode']=='0000'){
		    	$data['fee_data']=$api_fee_data['ResponseMessage'];
			}
			//change session for amount
			$this->session->set_userdata('amz_amount',$this->input->post('amount'));
			$arr=[
				// 'convenience_data'=>$data['convenience_data'],
				'pf'=>$pf,
				'tf'=>$tf,
				'ccfee'=>$ccfee,
				'total'=>$total,
				'fee_data'=>$data['fee_data'],
				'success'=>true
			];

			echo json_encode($arr);
			exit;
	}
	public function get_fees_customer(){
		$arr=[
			'method'=>'get_fees_customer',
			'P01'=> $this->input->post('amount'),
			'P02'=>  'LOAD',
			'P03'=>	'',//$this->session->userdata('suntag_shortcode'),
			'return_mode'=>'json'
		];
		//$arr_tosend['url']="login_business?".http_build_query($arr);

		//dd($arr);
		$api_fee_result=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr);
		//dd($api_fee_result);
		$api_fee_data=json_decode($api_fee_result,true);
		//dd($api_bt_data);
		$data['fee_data']='';
		if($api_fee_data['ResponseCode']=='0000'){
		$data['fee_data']=$api_fee_data['ResponseMessage'];
		}
		$arr=[
			'fee_data'=>$data['fee_data'],
			'success'=>true
		];

		echo json_encode($arr);
		exit;
	}


	public function pending_email($arr){
		$to = $arr['email'];
		$subj = $arr['title'];
		$attachment_arr='';
        $template_html = $arr['view']; //views/templates/mail/
		//email receipt..
		$this->assetHelper->send_email($arr,$to,$subj,$template_html,$attachment_arr);		
	}

	public function pending_email_cash($arr,$image_attachment){
		$to = $arr['email'];
		$subj = $arr['title'];
		$attachment_arr='';
        $template_html = $arr['view']; //views/templates/mail/

		//email receipt..
		$this->assetHelper->send_email($arr,$to,$subj,$template_html,$attachment_arr,$image_attachment);		
	}

	public function sample_email(){

/*		$qr=Qr::GenerateQr('00000312','',140);
		$barcode=Qr::GenerateBarcode128('00000312','','');
		$qr= base64_decode($qr->generate());
		$barcode= base64_decode($barcode);
		$arr=[
			"amount"=>"100.00",
			"transaction_id"=>'00000312',
			//"qr_code"=>'<img src="data:'.$qr->getContentType().';base64,'.$qr->generate().'" />',
			//"barcode"=>'<img src="data:image/png;base64,'.$barcode.'" />',
			"creation_date"=>date('M d, Y'),
			"email"=>'rolandjhaymoris@yahoo.com',
			"mobile"=>'09178435554',
			"view"=>'email_payment_code.blade.php',
			"title"=>'Pending Payment',
			//"profile_pic" => $this->session->userdata("ImageURL")
			//<img src="data:'.$qr->getContentType().';base64,'.$qr->generate().'" />
		];*/
/*        $image_attachment[0]['img_string']=$qr;
        $image_attachment[0]['img_name']='qr_code';
        $image_attachment[1]['img_string']=$barcode;
        $image_attachment[1]['img_name']='barcode';*/

			// $arr=[
			// 	"customer_name"=>'Maner2',
			// 	"amount"=>'2.00',
			// 	"transaction_id"=>'32434456',
			// 	"dba_name"=>'',
			// 	"creation_date"=>'03 Nov 2018',
			// 	"reference_num"=>'4353455',
			// 	"notes"=>'Hye',
			// 	"balance"=>'33555',
			// 	"fee"=>'1.00',
			// 	"vat"=>'0.36',
			// 	"total"=>'4.00',  //amount +fee+vat
			// 	"msg_above"=>"<h2>Zel sent you <span class='amount'>5 BSD</span></h2>",
			// 	"msg_bottom"=>"<p>Transaction is <span class='status'>PENDING</span> until ".$this->session->userdata('dba_name')." accepts payment!</p>",
			// 	"email"=>'zelbuado@gmail.com',
			// 	"view"=>"email_message_merchant.blade.php",
			// 	"title"=>'Incoming Payment',
			// 	"profile_pic" => $this->session->userdata("profile_pic")
			// ];
			
			        	//email customer.
			// $arr=[
			// 	"customer_name"=>'Hazel Buado',
			// 	"amount"=>'2.00',
			// 	"transaction_id"=>'32434456',
			// 	"dba_name"=>'',
			// 	"creation_date"=>'03 Nov 2018',
			// 	"reference_num"=>'4353455',
			// 	"notes"=>'Hye',
			// 	"balance"=>'33555',
			// 	"fee"=>'1.00',
			// 	"vat"=>'0.36',
			// 	"total"=>'4.00',  //amount +fee+vat
			// 	"msg_above"=>"<h2>Zel sent you <span class='amount'>5 BSD</span></h2>",
			// 	"msg_bottom"=>"<p>Transaction is <span class='status'>PENDING</span> until ".$this->session->userdata('dba_name')." accepts payment!</p>",
			// 	"email"=>'zelbuado@gmail.com',
			// 	"view"=>'email_message_completed.blade.php',
			// 	"title"=>'Con Payment',
			// 	"profile_pic" => $this->session->userdata("ImageURL")
			// ];
			// 
			$arr=[
				"customer_name"=>'Hazel Buado',
				"amount"=>'2.00',
				"transaction_id"=>'32434456',
				"dba_name"=>'Test111',
				"creation_date"=>'03 Nov 2018',
				"reference_num"=>'32434456',
				"notes"=>'Waterbill',
				"balance"=>'32.00',
				"fee"=>'1.00',
				"vat"=>'0.36',
				"total"=>'3.36',  //amount +fee+vat
				"msg_above"=>"<h2>You've sent <span class='amount'> 4.36 BSD</span> to Test111</h2>",
				"msg_bottom"=>"<p>Transaction is <span class='status'>PENDING</span> until Test111 accepts payment!</p>",
				"email"=>'nairbnorb@yahoo.com',
				"view"=>'email_message.blade.php',
				"title"=>'Pending Payment',
				"profile_pic" => $this->session->userdata("ImageURL")
			];
			$image_attachment='';

		$this->pending_email_cash($arr,$image_attachment);


		$this->blade->view('templates.email.email_message');
	}

	public function checkout_form(){
		$data['tokken'] = $this->assetHelper->create_tokken();
		$this->blade->view('checkout-sample-page',$data);
	}
	public function cash_payment_success(){
		// $data['tokken'] = $this->assetHelper->create_tokken();
			$data['post_data']=$this->session->userdata('post_data');	
			$data['cash_posted_data']=$this->session->userdata('cash_posted_data');
			$data['cash_payment_code']=$this->session->userdata('cash_payment_code');		
			//unset($_SESSION['post_data']);
			//unset($_SESSION['cash_posted_data']);
			//unset($_SESSION['cash_payment_code']);
		$this->blade->view('cash_payment_success',$data);
	}
	public function successpage_redirect($reference_number,$status,$payment_method,$callbackurl){
		//dd($reference_number);
		// $data['tokken'] = $this->assetHelper->create_tokken();
		$data['callbackurl']=base64_decode(urldecode($callbackurl));
		$data['reference_number']=$reference_number;
		$data['status']=$status;
		$data['payment_method']=$payment_method;
		$params=base64_encode($data['reference_number']."||".$data['status']."||".$data['payment_method']);
		redirect($data['callbackurl'].$params, 'refresh');
		//$this->blade->view('success_page',$data);
	}
	public function checkout($merchant_key,$reference_id){
		//dd($reference_id);
		//dd($_SESSION['sunpass_order']);
		//dd($root_path);
		if(!empty($merchant_key) && !empty($reference_id)){
			//if empty fields
			$arr=[
				'method'=>'get_client_checkout_details',
				'P01'=>$merchant_key,
				'P02'=>$reference_id,
				'return_mode'=>'json'
			];
		    $api_fee_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
		    // dd($api_fee_result);
		    $api_fee_data=json_decode($api_fee_result,true);
		    //dd($api_fee_data);		
		    if($api_fee_data['Success']=='YES'){
		   	//check if pending...
		   	if($api_fee_data['ResponseMessage']['status']!='PENDING'){
		   		redirect('');
		   	}
		    //process order as individual array
		    $ItemName=[];
		    $ItemQty=[];
		    $ItemPrice=[];
		    if(!empty($api_fee_data['ResponseMessage']['details'])){
		    	for ($i=0; $i <count($api_fee_data['ResponseMessage']['details']) ; $i++) { 
		    		# code...
				    $ItemName[$i]=$api_fee_data['ResponseMessage']['details'][$i]['item_name'];
				    $ItemQty[$i]=$api_fee_data['ResponseMessage']['details'][$i]['qty'];
				    $ItemPrice[$i]=$api_fee_data['ResponseMessage']['details'][$i]['price'];		    		
		    	}
		    }
			$return_data=[
			'merchant_client_id'=>$api_fee_data['ResponseMessage']['merchant_client_id'],
			'MerchantKey'=>$api_fee_data['ResponseMessage']['merchant_key'],
			'MerchantName'=>$api_fee_data['ResponseMessage']['merchant_name'],
			'Amount'=>$api_fee_data['ResponseMessage']['amount'],
			'total_amount'=>$api_fee_data['ResponseMessage']['total_amount'],
			'OrderID'=>$api_fee_data['ResponseMessage']['order_id'],
			'CallbackURL'=>$api_fee_data['ResponseMessage']['callback_url'],
			'ItemName'=>$ItemName,
			'ItemQty'=>$ItemQty,
			'ItemPrice'=>$ItemPrice,
			'OrderReference'=>$reference_id,
			'profile_pic'=>$api_fee_data['ResponseMessage']['profile_pic'],
			'reference_id'=>$reference_id,
			'sunpass_fee'=>$api_fee_data['ResponseMessage']['sunpass_fee'],
			'facility_fee'=>$api_fee_data['ResponseMessage']['facility_fee'],
			'payment_method'=>$api_fee_data['ResponseMessage']['payment_method'],
			//'auth',				
			];
			$data['reference_id']=$reference_id;

			//dd($return_data);

			$data['post_data'] = $return_data;

		    $arr=[
		    	'method'=>'login_business',
	            'P01'=> SERVICE_ACCOUNT_USERNAME_DEV,
	            'P02'=> SERVICE_ACCOUNT_PASSWORD_DEV,
		    ];
		    //$arr_tosend['url']="login_business?".http_build_query($arr);

		    //dd($arr_tosend);
		    $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
		    $api_bt_data=json_decode($api_bt_result,true);
		    //dd($api_bt_data);
		    $data['login_data']='';
		    if($api_bt_data['ResponseCode']=='YES'){
		    $data['login_data']=$api_bt_data['ResponseMessage'];
		    }


			//get payment settings. 
		       $arr=[
		         'method'=>'get_checkout_methods_byid',
		            'P01'=> $data['login_data']['SessionID'],
		            'P02'=> $api_fee_data['ResponseMessage']['merchant_client_id'],
		       ];
		       // $arr_tosend['url']="login_business?".http_build_query($arr);

		       // dd($arr_tosend);
		       //dd($arr);
		       $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
		       $api_bt_data=json_decode($api_bt_result,true);
		       //dd($api_bt_result);
		       $data['selected_settings_data']='';
		       if($api_bt_data['Success']=='YES'){
		       $api_bt_data['ResponseMessage'][0] =isset($api_bt_data['ResponseMessage'][0]) ? $api_bt_data['ResponseMessage'][0] : '';
		       $data['selected_settings_data']=$api_bt_data['ResponseMessage'][0];                     
		       } 
		    $data['sunpass_fee']=0.00;
		    $data['facility_fee']=0.00;
			//get sunpass fee$$$$.
			if(in_array($merchant_key,SUNPASS_MERCHANTS_KEYS)){
			$data['sunpass_fee'] = $api_fee_data['ResponseMessage']['sunpass_fee']>0 ? number_format($api_fee_data['ResponseMessage']['sunpass_fee'],2) : 0.00;
			$data['facility_fee'] =$api_fee_data['ResponseMessage']['facility_fee']>0 ?  number_format($api_fee_data['ResponseMessage']['facility_fee'],2): 0.00;
			}

			//session return for safari....
			$data['post_data']['pf']=0.00;
			$data['post_data']['tf']=0.00;
			$data['post_data']['fee']=0.00;
			$data['post_data']['sunpass_fee']=$data['sunpass_fee'];
			$data['post_data']['facility_fee']=$data['facility_fee'];
			$data['post_data']['subtotal']=$return_data['Amount'];
			$data['post_data']['total_amount']=$return_data['Amount']+$data['sunpass_fee']+$data['facility_fee'];
			$data['post_data']['promo_code']='';
			$data['post_data']['discount']=0.00;
			//$data['post_data']['subtotal'] = $_SESSION['post_data']['subtotal'];

			$this->session->set_userdata('post_data',$return_data);
	  		$_SESSION['post_data']['pf'] = 0.00;
	  		$_SESSION['post_data']['tf'] = 0.00;
	  		$_SESSION['post_data']['fee'] = 0.00;
	  		$_SESSION['post_data']['sunpass_fee'] = $data['sunpass_fee'];
	  		$_SESSION['post_data']['facility_fee'] = $data['facility_fee'];
	  		$_SESSION['post_data']['subtotal'] = $_SESSION['post_data']['Amount'];
	  		$_SESSION['post_data']['total_amount']=$return_data['Amount']+$data['sunpass_fee']+$data['facility_fee'];
	  		$_SESSION['post_data']['promo_code'] = '';
	  		$_SESSION['post_data']['discount'] = 0.00;
	  		$data['subtotal'] = $_SESSION['post_data']['subtotal'];
			//site verify
			$arr=[
				'merchant'=>CENPOST_MERCHANT_ID,
				'secretkey'=>CENPOST_MERCHANT_SECRETKEY,
				'email'=>'suncashme@gmail.com',
				'ip'=>'127.0.0.1',
			];
			$verify_post=$this->assetHelper->api_requestv2(CENPOS_SITEVERIFY_URL,$arr);
			$verify_post=json_decode($verify_post,true);	
			//dd($verify_post);
			$data['verify_params']=$verify_post;
			
			//amazon
			$params= $merchant_key.'/'.$reference_id;
			$payload = array(
				'webCheckoutDetails' => array(
					'checkoutReviewReturnUrl' => 'https://dev.suncash.me/payment/checkout/'.$params.'?status=review', //review
					'checkoutResultReturnUrl' => 'https://dev.suncash.me/payment/checkout/'.$params.'?status=pay',//complete
				),
				'storeId' => 'amzn1.application-oa2-client.ced8ec472a0644e0ae67c7c2de8c767c',
				'merchantMetadata' => array(
					'merchantReferenceId' =>$reference_id,//checkoutreferenceid
					'merchantStoreName' =>$api_fee_data['ResponseMessage']['merchant_name'],
					'noteToBuyer' => 'Thank you for your order!'
				),
			);
			//dd($payload);
			$headers = array('x-amz-pay-Idempotency-Key' => uniqid());
			$requestResult = [
				'error' => 1,
				'msg' => 'Error. Can not create checkout session.',
				'signature' => null,
				'payload' => null
			];

			$client = new Amazon\Pay\API\Client($this->amazonpay_config);
			$resultCheckOut = $client->createCheckoutSession($payload, $headers);
			$resultSignature = $client->generateButtonSignature($payload);
			if($resultCheckOut['status'] !== 201) {

				$arr=[  
					'msg'=>'Invalid Request',
					'amz_msg'=>$requestResult,
					'success'=>false
				];
				
				echo json_encode($arr);
				exit;  
			}else {		
				$arr=[
						'method'=>'get_payment_fee',	
						'P01' =>$api_fee_data['ResponseMessage']['merchant_key'], //Merchant API Key
						'P02' =>$data['post_data']['total_amount'], //Mobile No
						'P03' =>'AMAZON', //passcode
						'P04' =>'BUSINESS',		   
						'return_mode'=>'json'
					];
						// dd($arr);
					$fees=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
					$amazon_fees=json_decode($fees,true);
					$data['amazon_fees']='';
					if($amazon_fees['Success']=='YES'){
						$data['amazon_fees']=$amazon_fees['ResponseMessage'];
					}
					// dd($amazon_fees);
					// $total_due= $data['amazon_fees']['total_due'];
					$total_convenience_fee= $data['amazon_fees']['total_convenience_fee'];
					$iscard_fee_on_merch=$data['amazon_fees']['iscard_fee_on_merch'];
					$total_due= $data['amazon_fees']['total_due'];
					if($iscard_fee_on_merch>0){
						$ccfee=0.00;
					}else{
						$ccfee=$total_convenience_fee;
					}
						
					$this->session->set_userdata('amz_total',$total_due);
					// $this->session->set_userdata('amz_cc_fee',$ccfee);
					// $this->session->set_userdata('amz_amount',$total_due);
					$this->session->set_userdata('amz_cc_ismerch',$iscard_fee_on_merch);
					$this->session->set_userdata('amz_sig',$resultSignature);
					$this->session->set_userdata('amz_payload',$payload);
					$this->session->set_userdata('amz_checkout',$resultCheckOut);
					$this->session->set_userdata('amz_id',$headers);
					$data['signature']= $resultSignature;
					$data['payload']= $payload;
					$data['amz_order_deatils']= '';
					$data['amz_status']=isset($_GET['status']) ? $_GET['status'] : '';
					if(isset($_GET['amazonCheckoutSessionId'])){
						$this->session->set_userdata('amz_sess',$_GET['amazonCheckoutSessionId']);
						$payload='';
						$headers = array('x-amz-pay-Idempotency-Key' => $this->session->userdata('amz_id'));

						try {	
						$checkoutSessionId = $_GET['amazonCheckoutSessionId'];
						//dd($this->amazonpay_config);
						$client = new Amazon\Pay\API\Client($this->amazonpay_config);
						$result = $client->getCheckoutSession($checkoutSessionId);
							if ($result['status'] === 200) {
								$response = json_decode($result['response'], true);
								//dd($response);
								$data['amz_order_deatils']=$response;
								$checkoutSessionState = $response['statusDetails']['state'];
								$chargeId = $response['chargeId'];
								$chargePermissionId = $response['chargePermissionId'];

								if($_GET['status']=='review'){
								$buyer['buyername'] = $response['buyer']['name'];
								$buyer['buyeremail'] = $response['buyer']['email'];
								$this->session->set_userdata('amz_sess_buyer',$buyer);
								}
							} else {
								// echo 'status=' . $result['status'] . '; response=' . $result['response'] . "\n";
					
								$arr=[  
									'msg'=>'Invalid Request',
									'amz_msg'=>$result['response'],
									'success'=>false
								];
								
								echo json_encode($arr);
								exit;  
							}
						} catch (\Exception $e) {
							// handle the exception
							echo $e . "\n";
						}
					}
					// dd($data);
					// $this->blade->view('checkoutv2',$data);
				}
			//sanddollar
			$this->load->library('Mobile_Detect');
			if($this->mobile_detect->isMobile()){
				$loc="app";
			}else{
				$loc="web";
			}
			$data['location']=$loc;		
										// dd($data);
			$arr=[
				'method'=>'sanDollarPendingTransaction',
				'P01'=> $api_fee_data['ResponseMessage']['merchant_client_id'],
				'P02'=> $api_fee_data['ResponseMessage']['merchant_key'], //Merchant API Key
				'P03'=>	$return_data['Amount']+$data['sunpass_fee']+$data['facility_fee'],
				'P04'=>	'CHECKOUT',
				'P05'=>'BUSINESS_PAYMENT',
				'P06'=>'',
				'P07'=>'',
				'P08'=>'',			
				'P09'=>'',		
				'P10'=>$reference_id,									
				'return_mode'=>'json'
			];
			// dd($arr);
			$api_fee_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
			$sanddata=json_decode($api_fee_result,true);
			//dd($api_fee_result);
			$data['sand_data']='';
			if($sanddata['Success']=='YES'){
				$data['sand_data']=$sanddata['ResponseMessage'];  
				if(!empty($sanddata['ResponseMessage']['referenceid'])){
					$data['sand_data']['referenceid'] =  $sanddata['ResponseMessage']['referenceid'];
				} else {
					$data['sand_data'] =  $sanddata['ResponseMessage']['data'];
				}
				

			} 	
				//	dd($data);
			$this->blade->view('checkout',$data);
		    } else {
			redirect('');	    	
		    }	


		} else {
			redirect('');
		}
		
	}

/*	public function reinitialize_session(){
		//session_start();
		$this->session->set_userdata('post_data',$_POST);
		//dd($_SESSION['post_data']);
			$arr=[	
				'session'=>$_SESSION,
				'success'=>true
			];
			
			echo json_encode($arr);
			exit;
	}*/

	public function process_payment(){//checkout credit card process.

		if(empty($_POST['tokenid'])){

			$arr=[
				'msg'=>'Please fill up all fields',
				'success'=>false
			];
			echo json_encode($arr);
		}
		//recall api staging on saving..
		$arr=[
			'method'=>'get_client_checkout_details',
			'P01'=>$_POST['m'],
			'P02'=>$_POST['r'],
			'return_mode'=>'json'
		];
	    $api_fee_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
	    $api_fee_data=json_decode($api_fee_result,true);	
	    if($api_fee_data['Success']=='YES'){

	    //process order as individual array
	    $ItemName=[];
	    $ItemQty=[];
	    $ItemPrice=[];
	    if(!empty($api_fee_data['ResponseMessage']['details'])){
	    	for ($i=0; $i <count($api_fee_data['ResponseMessage']['details']) ; $i++) {
	    		# code...
			    $ItemName[$i]=$api_fee_data['ResponseMessage']['details'][$i]['item_name'];
			    $ItemQty[$i]=$api_fee_data['ResponseMessage']['details'][$i]['qty'];
			    $ItemPrice[$i]=$api_fee_data['ResponseMessage']['details'][$i]['price'];
	    	}
	    }
		$return_data=[
			'merchant_client_id'=>$api_fee_data['ResponseMessage']['merchant_client_id'],
			'MerchantKey'=>$api_fee_data['ResponseMessage']['merchant_key'],
			'MerchantName'=>$api_fee_data['ResponseMessage']['merchant_name'],
			'Amount'=>$api_fee_data['ResponseMessage']['amount'],
			'total_amount'=>$api_fee_data['ResponseMessage']['total_amount'],
			'OrderID'=>$api_fee_data['ResponseMessage']['order_id'],
			'CallbackURL'=>$api_fee_data['ResponseMessage']['callback_url'],
			'ItemName'=>$ItemName,
			'ItemQty'=>$ItemQty,
			'ItemPrice'=>$ItemPrice,
			'OrderReference'=>$_POST['r'],
			'profile_pic'=>$api_fee_data['ResponseMessage']['profile_pic'],
			'reference_id'=>$_POST['r'],
			'sunpass_fee'=>$api_fee_data['ResponseMessage']['sunpass_fee'],
			'facility_fee'=>$api_fee_data['ResponseMessage']['facility_fee'],
			'processing_fee'=>$api_fee_data['ResponseMessage']['processing_fee'],
			'transaction_fee'=>$api_fee_data['ResponseMessage']['transaction_fee'],
			'discount'=>$api_fee_data['ResponseMessage']['discount'],
			'iscard_fee_on_merch'=>$api_fee_data['ResponseMessage']['iscard_fee_on_merch'],
			//'auth',
			];
		}

		$post_data =$return_data;
		
		//backdorr conveniene fee
			$arr=[
	    	'method'=>'login_business',
	        'P01'=> SERVICE_ACCOUNT_USERNAME_DEV,
	        'P02'=> SERVICE_ACCOUNT_PASSWORD_DEV,
	    ];
	    //$arr_tosend['url']="login_business?".http_build_query($arr);

	    //dd($arr_tosend);
	    $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
	    $api_bt_data=json_decode($api_bt_result,true);
	    //dd($api_bt_data);
	    $data['login_data']='';
	    if($api_bt_data['ResponseCode']=='YES'){
	    $data['login_data']=$api_bt_data['ResponseMessage'];
	    }

       //get transaction fees. 
       // $client_record_id = $_SESSION['client_record_id'];
       $arr=[
         'method'=>'get_convenience_fee',
            'P01'=> $data['login_data']['SessionID'],
            'P02'=> $api_fee_data['ResponseMessage']['merchant_client_id'],
       ];

       // $arr_tosend['url']="login_business?".http_build_query($arr);

       //dd($arr);
       //print_r($arr);die;
       $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
       $api_bt_data=json_decode($api_bt_result,true);
       //dd($api_bt_result);
       //var_dump($api_bt_result);die;
       $data['transaction_fee_data']='';
       if($api_bt_data['Success']=='YES'){
       $api_bt_data['ResponseMessage']=isset($api_bt_data['ResponseMessage']) ? $api_bt_data['ResponseMessage'] : '';
       $transaction_fee_data=$api_bt_data['ResponseMessage'];
       } 
       $amount =str_replace(',', '', $api_fee_data['ResponseMessage']['amount']); //? $_POST['amount']  : $this->session->userdata('post_data')['Amount'];
       //dd($amount);
       $total= 0.00;
       $pf=0.00;
       $tf=0.00;
       $upf=0.00;
       $utf=0.00;
       $isenable='';
	     if(!empty($transaction_fee_data)){
	       	   $isenable=$transaction_fee_data['is_enable_cc_fee'];
		       if($transaction_fee_data['is_enable_cc_fee']==1){
		       		$gtot =number_format($amount,2,'.','')+$api_fee_data['ResponseMessage']['sunpass_fee']+$api_fee_data['ResponseMessage']['facility_fee'];

			       if($transaction_fee_data['cc_processing_fee_isfixed']==1){//fixed
			       		//$fee+= $transaction_fee_data['cc_processing_fee'];
			       		$pf=$transaction_fee_data['cc_processing_fee'];
			       		$upf=$transaction_fee_data['cc_processing_fee'];
			       		//$total += floatval($amount)+floatval($fee);
			       } else if($transaction_fee_data['cc_processing_fee_isfixed']==0){
			       		$percentage= $transaction_fee_data['cc_processing_fee']/100;
			       		$total_percent=number_format($gtot,2,'.','')*number_format($percentage,2,'.','');
			       		//echo $amount;
			       		//echo $fee;
			       		$pf=$total_percent;
			       		$upf=$total_percent;
			       }
			       //apply fee...
			       if($transaction_fee_data['transaction_fee_isfixed']==1){//fixed
			       		//$fee+= $transaction_fee_data['transaction_fee'];
			       		$tf=$transaction_fee_data['transaction_fee'];
			       		$utf=$transaction_fee_data['transaction_fee'];
			       		//$total += floatval($amount)+floatval($fee);
			       } else if($transaction_fee_data['transaction_fee_isfixed']==0){//percent
			       		$percentage=$transaction_fee_data['transaction_fee']/100;
			       		$total_percent=number_format($gtot,2,'.','')*number_format($percentage,2,'.','');
			       		//echo $fee."<br>";
			       		$tf=$total_percent;
			       		$utf=$total_percent;
			       }
		       } else {
		       		//
		       		$isenable=$transaction_fee_data['is_enable_cc_fee'];
		       		$gtot =$amount+$api_fee_data['ResponseMessage']['sunpass_fee']+$api_fee_data['ResponseMessage']['facility_fee'];

			       if($transaction_fee_data['cc_processing_fee_isfixed']==1){//fixed
			       		//$fee+= $transaction_fee_data['cc_processing_fee'];
			       		$upf=$transaction_fee_data['cc_processing_fee'];
			       		//$total += floatval($amount)+floatval($fee);
			       } else if($transaction_fee_data['cc_processing_fee_isfixed']==0){
			       		$percentage= $transaction_fee_data['cc_processing_fee']/100;
			       		$total_percent=number_format($gtot,2,'.','')*number_format($percentage,2,'.','');
			       		//echo $amount;
			       		//echo $fee;
			       		$upf=$total_percent;
			       }
			       //apply fee...
			       if($transaction_fee_data['transaction_fee_isfixed']==1){//fixed
			       		//$fee+= $transaction_fee_data['transaction_fee'];
			       		$utf=$transaction_fee_data['transaction_fee'];
			       		//$total += floatval($amount)+floatval($fee);
			       } else if($transaction_fee_data['transaction_fee_isfixed']==0){//percent
			       		$percentage=$transaction_fee_data['transaction_fee']/100;
			       		$total_percent=number_format($gtot,2,'.','')*number_format($percentage,2,'.','');
			       		//echo $fee."<br>";
			       		$utf=$total_percent;
			       }
		       }
	       }
		$this->load->library('Mobile_Detect');
		$pre_total = $post_data['Amount']-$post_data['discount'];
		if($pre_total<=0){
			$pre_total=0.00;
		}

		$iscard_fee_on_merch = 0;
		if($isenable==1){ //1 naka on ung fees sa customer
			$grand_total =$pre_total+$post_data['sunpass_fee']+$post_data['facility_fee']+$post_data['processing_fee']+$post_data['transaction_fee'];
			$iscard_fee_on_merch = 0; //nasa customer fees
		} else {

			$grand_total =$pre_total+$post_data['sunpass_fee']+$post_data['facility_fee'];
			$iscard_fee_on_merch = 1;//nasa mercahnt fees kukunin
		}			
		$grand_total = number_format($grand_total, 2, '.', '');
		$arr=[];
		if($grand_total<=0){
			//failed here
			$arr=[
                'msg'=>'Amount should be greather than 0',
				'success'=>false
			];

			echo json_encode($arr);

		}


		if($this->mobile_detect->isiOS() || $this->mobile_detect->isSafari() || $this->mobile_detect->isiPhone()){
			$arr=[
			'amount'=>$grand_total,
			'invoicenumber'=>$post_data['OrderID'],
			'type'=>'Sale',
			'email'=>'suncashme@gmail.com',
			'secretkey'=>CENPOST_MERCHANT_SECRETKEY,
			'merchant'=>CENPOST_MERCHANT_ID,
			'tokenid'=>$_POST['tokenid'],
			'isrecaptcha'=>false,
			];
		} else {
			$arr=[
			'amount'=>$grand_total,
			'invoicenumber'=>$post_data['OrderID'],
			'type'=>'Sale',
			'email'=>'suncashme@gmail.com',
			'secretkey'=>CENPOST_MERCHANT_SECRETKEY,
			'merchant'=>CENPOST_MERCHANT_ID,
			'tokenid'=>$_POST['tokenid'],
			];			
		}
		$verifying_post='';
		$verify_post=$this->assetHelper->api_requestv2(CENPOS_SITEVERIFY_URL,$arr);
		$verify_post=json_decode($verify_post,true);
		if($verify_post['Result']==0){
			$arr_post=[
				'method'=>'create_suncashme_checkout_log',
				'P01'=>$post_data['MerchantKey'],//MerchantKey
				'P02'=>$post_data['OrderID'],//order_id
				'P03'=>'',//reference_number
				'P04'=>'cenpos',//type
				'P05'=>'success',//response
				'P06'=>json_encode($arr),//response
				'P07'=>json_encode($verify_post),//response
				'P08'=>'',//response
				'return_mode'=>'json'	
			];
			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);


			$verifying_post=$verify_post['Data'];
		} else{///
			$arr_post=[
				'method'=>'create_suncashme_checkout_log',
				'P01'=>$post_data['MerchantKey'],//MerchantKey
				'P02'=>$post_data['OrderID'],//order_id
				'P03'=>'',//reference_number
				'P04'=>'cenpos',//type
				'P05'=>'failed',//response
				'P06'=>json_encode($arr),//response
				'P07'=>json_encode($verify_post),//response
				'P08'=>'',//response
				'return_mode'=>'json'
			];
			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);

			$arr=[
				'msg'=>$verify_post['Message'],
				'data'=>$verify_post,
				'success'=>false
			];

			echo json_encode($arr);
		}
		$arr_post=[
			'verifyingpost'=>$verifying_post,
			'tokenid'=>$_POST['tokenid'],
		];
		$process_payment1=$this->assetHelper->api_requestv2(CENPOS_PROCESS_URL,$arr_post);
		$process_payment=json_decode($process_payment1,true);
		if($process_payment['Result']==0){
			//saving logs....
			$arr_post=[
				'method'=>'create_suncashme_checkout_log',
				'P01'=>$post_data['MerchantKey'],//MerchantKey
				'P02'=>$post_data['OrderID'],//order_id
				'P03'=>$process_payment['ReferenceNumber'],//reference_number
				'P04'=>'cenpos',//type
				'P05'=>'success',//response
				'P06'=>json_encode($arr_post),//response
				'P07'=>$process_payment1,//response
				'P08'=>$process_payment['Message'],//response
				'return_mode'=>'json'
			];
			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);
			//update status ..
			$arr_update=[
				'method'=>'update_checkout_transaction',
				'P01'=>$post_data['OrderReference'],//reference
				'P02'=>'PROCESSED',//status
				'P03'=>'',//status
				'return_mode'=>'json'
			];
			$update_status=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr_update);
			//save transaction details....

			$arr_post_trans=[
				'method'=>'create_suncashme_cenpos_transaction_detail',
				'P01'=>$post_data['MerchantKey'],//MerchantKey
				'P02'=>$post_data['OrderID'],//order_id
				'P03'=>$process_payment['ReferenceNumber'],//reference_number
				'P04'=>$_POST['card_number'],//type
				'P05'=>$_POST['name_on_card'],//response
				'P06'=>$_POST['card_type'],//response
				'P07'=>number_format($post_data['Amount'],2,'.',''),//response
				'P08'=>number_format($upf,2,'.',''),//response,//response
				'P09'=>number_format($utf,2,'.',''),//response,//response
				'P10'=>number_format($post_data['transaction_fee']+$post_data['processing_fee']+$post_data['sunpass_fee']+$post_data['facility_fee'],2,'.',''),//totall of all fee master fee tne fee
				'P11'=>$post_data['sunpass_fee'],//forsuncash
				'P12'=>$post_data['facility_fee'],//for ticketing only
				'P13'=>0.00,//billpay fee
				'P14'=>0.00,//vat
				'P15'=>'checkout',//source checkout
				'P16'=>number_format($post_data['discount'], 2, '.', ''),//source checkout
				'P17'=>$iscard_fee_on_merch,
				'P18'=>'',//notes
				'P19'=>$_POST['name_customer_card'],
				'P20'=>$_POST['email_card'],
				'P21'=>$this->clean($_POST['mobile_card']),
				'return_mode'=>'json'
			];
			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post_trans);

			//saving logs....
			$arr_post=[
				'method'=>'create_suncashme_checkout_log',
				'P01'=>$post_data['MerchantKey'],//MerchantKey
				'P02'=>$post_data['OrderID'],//order_id
				'P03'=>$process_payment['ReferenceNumber'],//reference_number
				'P04'=>'cenpos',//type
				'P05'=>'success',//response
				'P06'=>json_encode($arr_post_trans),//response
				'P07'=>$save_logs,//response
				'P08'=>'',//response
				'return_mode'=>'json'
			];
			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);
            $cfee_email = $post_data['iscard_fee_on_merch'] ==1 ? $cfee_email=0.00 : $cfee_email=number_format($upf,2,'.','')+number_format($utf,2,'.','');
			$arr=[
			'method'=>'get_suntag_shortcode',
			'P01'=>$post_data['MerchantKey'],//$this->session->userdata('SessionID'),
			'return_mode' => 'json'
			];
			$api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
			$api_bt_data=json_decode($api_bt_result,true);	
			// dd($api_bt_result);
			$data['suntag_data']='';
			if($api_bt_data['Success']=='YES'){
			$data['suntag_data']=$api_bt_data['ResponseMessage'];
			}else{
					$arr=[	
						'msg'=>'Something went wrong!',
						'success'=>false
					];
					echo json_encode($arr);
					exit;
			}  

			$arr=[
				"customer_name"=>$_POST['name_customer_card'],
				"amount"=>number_format($post_data['Amount'],2,'.',''),
				"transaction_id"=>$post_data['OrderID'],//cenpos
				"dba_name"=>$data['suntag_data']['dba_name'],
				"creation_date"=>date('M d, Y'),
				"reference_num"=>$process_payment['ReferenceNumber'],
				"notes"=>'',
				"balance"=>'',
				"processing_fee"=>number_format($upf,2,'.',''),
				"transaction_fee"=>number_format($utf,2,'.',''),
				"fee"=>0.00,
				"vat"=>0.00,
				"cfee"=>$cfee_email,
				"tfee"=>0.00,
				"total"=>number_format($grand_total,2),  //amount +fee+vat
				"msg_above"=>"<h2>".$_POST['name_customer_card']." paid <span class='amount'>".number_format($grand_total,2)." BSD</span>. </h2>",
				"msg_bottom"=>"",
				"email"=>$data['suntag_data']['business_email_address'],
				"view"=>'email_message_completed.blade.php',
				"title"=>'Completed Payment',
				"profile_pic" => $this->session->userdata("profile_pic"),
				"iscard_fee_on_merch"=>$post_data['iscard_fee_on_merch']
			];

			$this->confirm_email($arr);
			unset($_SESSION['post_data']);
			$reference_num = $process_payment['ReferenceNumber']."||".$_POST['r'];
			$params=base64_encode($reference_num."||success||card");
			$arr=[
				'msg'=>$process_payment['Message'],
				'reference'=>$process_payment['ReferenceNumber'],
				'url'=>$post_data["CallbackURL"].$params,
				'payment_method'=>'Card',
				'success'=>true
			];

			echo json_encode($arr);
		} else {
			//saving logs....
			$arr_post=[
				'method'=>'create_suncashme_checkout_log',
				'P01'=>$post_data['MerchantKey'],//MerchantKey
				'P02'=>$post_data['OrderID'],//order_id
				'P03'=>'',//reference_number
				'P04'=>'cenpos',//type
				'P05'=>'failed',//response
				'P06'=>json_encode($arr_post),//response
				'P07'=>$process_payment1,//response
				'P08'=>'',//response
				'return_mode'=>'json'
			];
			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);
			$arr=[
				'msg'=>$process_payment['Message'],
				'success'=>false
			];
			echo json_encode($arr);
		}

	}
	public function process_checkout_creditcard(){//checkout credit card process.

		if(empty($_POST['tokenid'])){

			$arr=[
				'msg'=>'Please fill up all fields',
				'success'=>false
			];
			echo json_encode($arr);
		}
		//recall api staging on saving..
		$arr=[
			'method'=>'get_client_checkout_details',
			'P01'=>$_POST['m'],
			'P02'=>$_POST['r'],
			'return_mode'=>'json'
		];
	    $api_fee_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
	    //dd($api_fee_result);
	    $api_fee_data=json_decode($api_fee_result,true);
	    //dd($api_fee_result);		
	    if($api_fee_data['Success']=='YES'){
	   	//check if pending...

	    $ItemName=[];
	    $ItemQty=[];
	    $ItemPrice=[];
	    if(!empty($api_fee_data['ResponseMessage']['details'])){
	    	for ($i=0; $i <count($api_fee_data['ResponseMessage']['details']) ; $i++) {
	    		# code...
			    $ItemName[$i]=$api_fee_data['ResponseMessage']['details'][$i]['item_name'];
			    $ItemQty[$i]=$api_fee_data['ResponseMessage']['details'][$i]['qty'];
			    $ItemPrice[$i]=$api_fee_data['ResponseMessage']['details'][$i]['price'];
	    	}
	    }
		$return_data=[
		'merchant_client_id'=>$api_fee_data['ResponseMessage']['merchant_client_id'],
		'MerchantKey'=>$api_fee_data['ResponseMessage']['merchant_key'],
		'MerchantName'=>$api_fee_data['ResponseMessage']['merchant_name'],
		'Amount'=>$api_fee_data['ResponseMessage']['amount'],
		'total_amount'=>$api_fee_data['ResponseMessage']['total_amount'],
		'OrderID'=>$api_fee_data['ResponseMessage']['order_id'],
		'CallbackURL'=>$api_fee_data['ResponseMessage']['callback_url'],
		'ItemName'=>$ItemName,
		'ItemQty'=>$ItemQty,
		'ItemPrice'=>$ItemPrice,
		'OrderReference'=>$_POST['r'],
		'profile_pic'=>$api_fee_data['ResponseMessage']['profile_pic'],
		'reference_id'=>$_POST['r'],
		'sunpass_fee'=>$api_fee_data['ResponseMessage']['sunpass_fee'],
		'facility_fee'=>$api_fee_data['ResponseMessage']['facility_fee'],
		'processing_fee'=>$api_fee_data['ResponseMessage']['processing_fee'],
		'transaction_fee'=>$api_fee_data['ResponseMessage']['transaction_fee'],
		'discount'=>$api_fee_data['ResponseMessage']['discount'],
		'iscard_fee_on_merch'=>$api_fee_data['ResponseMessage']['iscard_fee_on_merch'],
		//'auth',
		];
		}

		$post_data =$return_data;
		
		//backdoor conveniene fee
			$arr=[
	    	'method'=>'login_business',
	        'P01'=> SERVICE_ACCOUNT_USERNAME_DEV,
	        'P02'=> SERVICE_ACCOUNT_PASSWORD_DEV,
	    ];
	    //$arr_tosend['url']="login_business?".http_build_query($arr);

	    //dd($arr_tosend);
	    $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
	    $api_bt_data=json_decode($api_bt_result,true);
	    //dd($api_bt_data);
	    $data['login_data']='';
	    if($api_bt_data['ResponseCode']=='YES'){
	    $data['login_data']=$api_bt_data['ResponseMessage'];
	    }

       //get transaction fees. 
       // $client_record_id = $_SESSION['client_record_id'];
       $arr=[
         'method'=>'get_convenience_fee',
            'P01'=> $data['login_data']['SessionID'],
            'P02'=> $api_fee_data['ResponseMessage']['merchant_client_id'],
       ];

       // $arr_tosend['url']="login_business?".http_build_query($arr);

       //dd($arr);
       //print_r($arr);die;
       $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
       $api_bt_data=json_decode($api_bt_result,true);
       //dd($api_bt_result);
       //var_dump($api_bt_result);die;
       $data['transaction_fee_data']='';
       if($api_bt_data['Success']=='YES'){
       $api_bt_data['ResponseMessage']=isset($api_bt_data['ResponseMessage']) ? $api_bt_data['ResponseMessage'] : '';
       $transaction_fee_data=$api_bt_data['ResponseMessage'];
       } 
       $amount =str_replace(',', '', $api_fee_data['ResponseMessage']['amount']); //? $_POST['amount']  : $this->session->userdata('post_data')['Amount'];
       //dd($amount);
       $total= 0.00;
       $pf=0.00;
       $tf=0.00;
       $upf=0.00;
       $utf=0.00;
       $isenable='';
	     if(!empty($transaction_fee_data)){
	       	   $isenable=$transaction_fee_data['is_enable_cc_fee'];
		       if($transaction_fee_data['is_enable_cc_fee']==1){
		       		$gtot =number_format($amount,2,'.','')+$api_fee_data['ResponseMessage']['sunpass_fee']+$api_fee_data['ResponseMessage']['facility_fee'];

			       if($transaction_fee_data['cc_processing_fee_isfixed']==1){//fixed
			       		//$fee+= $transaction_fee_data['cc_processing_fee'];
			       		$pf=$transaction_fee_data['cc_processing_fee'];
			       		$upf=$transaction_fee_data['cc_processing_fee'];
			       		//$total += floatval($amount)+floatval($fee);
			       } else if($transaction_fee_data['cc_processing_fee_isfixed']==0){
			       		$percentage= $transaction_fee_data['cc_processing_fee']/100;
			       		$total_percent=number_format($gtot,2,'.','')*number_format($percentage,2,'.','');
			       		//echo $amount;
			       		//echo $fee;
			       		$pf=$total_percent;
			       		$upf=$total_percent;
				   }
			       //apply fee...
			       if($transaction_fee_data['transaction_fee_isfixed']==1){//fixed
			       		//$fee+= $transaction_fee_data['transaction_fee'];
			       		$tf=$transaction_fee_data['transaction_fee'];
			       		$utf=$transaction_fee_data['transaction_fee'];
			       		//$total += floatval($amount)+floatval($fee);
			       } else if($transaction_fee_data['transaction_fee_isfixed']==0){//percent
			       		$percentage=$transaction_fee_data['transaction_fee']/100;
			       		$total_percent=number_format($gtot,2,'.','')*number_format($percentage,2,'.','');
			       		//echo $fee."<br>";
			       		$tf=$total_percent;
			       		$utf=$total_percent;
			       }
		       } else {
		       		//
		       		$isenable=$transaction_fee_data['is_enable_cc_fee'];
		       		$gtot =$amount+$api_fee_data['ResponseMessage']['sunpass_fee']+$api_fee_data['ResponseMessage']['facility_fee'];

			       if($transaction_fee_data['cc_processing_fee_isfixed']==1){//fixed
			       		//$fee+= $transaction_fee_data['cc_processing_fee'];
			       		$upf=$transaction_fee_data['cc_processing_fee'];
			       		//$total += floatval($amount)+floatval($fee);
			       } else if($transaction_fee_data['cc_processing_fee_isfixed']==0){
			       		$percentage= $transaction_fee_data['cc_processing_fee']/100;
			       		$total_percent=number_format($gtot,2,'.','')*number_format($percentage,2,'.','');
			       		//echo $amount;
			       		//echo $fee;
			       		$upf=$total_percent;
			       }
			       //apply fee...
			       if($transaction_fee_data['transaction_fee_isfixed']==1){//fixed
			       		//$fee+= $transaction_fee_data['transaction_fee'];
			       		$utf=$transaction_fee_data['transaction_fee'];
			       		//$total += floatval($amount)+floatval($fee);
			       } else if($transaction_fee_data['transaction_fee_isfixed']==0){//percent
			       		$percentage=$transaction_fee_data['transaction_fee']/100;
			       		$total_percent=number_format($gtot,2,'.','')*number_format($percentage,2,'.','');
			       		//echo $fee."<br>";
			       		$utf=$total_percent;
			       }
		       }
	       }

		// dd($isenable);
		//detect if ios af.
		$this->load->library('Mobile_Detect');
		$pre_total = $post_data['Amount']-$post_data['discount'];
		if($pre_total<=0){
			$pre_total=0.00;
		}
		$iscard_fee_on_merch = 0;
		if($isenable==1){ //1 means disabled and convenience fee should be charged to  merchant account
			$grand_total =$pre_total+$post_data['sunpass_fee']+$post_data['facility_fee']+$post_data['processing_fee']+$post_data['transaction_fee'];
			$iscard_fee_on_merch = 0;

		} else {
			$grand_total =$pre_total+$post_data['sunpass_fee']+$post_data['facility_fee'];
			$iscard_fee_on_merch = 1;
		}
		
		$grand_total = number_format($grand_total, 2, '.', '');
		//dd($this->mobile_detect);
		$arr=[];

		//dd(get_user_agent());
		if($this->mobile_detect->isiOS() || $this->mobile_detect->isSafari() || $this->mobile_detect->isiPhone()){
			$arr=[
			'amount'=>$grand_total,
			'invoicenumber'=>$post_data['OrderID'],
			'type'=>'Sale',
			'email'=>'suncashme@gmail.com',
			'secretkey'=>CENPOST_MERCHANT_SECRETKEY,
			'merchant'=>CENPOST_MERCHANT_ID,
			'tokenid'=>$_POST['tokenid'],
			'isrecaptcha'=>false,
			];
		} else {
			$arr=[
			'amount'=>$grand_total,
			'invoicenumber'=>$post_data['OrderID'],
			'type'=>'Sale',
			'email'=>'suncashme@gmail.com',
			'secretkey'=>CENPOST_MERCHANT_SECRETKEY,
			'merchant'=>CENPOST_MERCHANT_ID,
			'tokenid'=>$_POST['tokenid'],
			];			
		}
		//recompute tf and pf 


		//dd($arr);
		$verifying_post='';
		$verify_post=$this->assetHelper->api_requestv2(CENPOS_SITEVERIFY_URL,$arr);
		$verify_post=json_decode($verify_post,true);
		if($verify_post['Result']==0){
			$arr_post=[
				'method'=>'create_suncashme_checkout_log',
				'P01'=>$post_data['MerchantKey'],//MerchantKey
				'P02'=>$post_data['OrderID'],//order_id
				'P03'=>'',//reference_number
				'P04'=>'cenpos',//type
				'P05'=>'success',//response
				'P06'=>json_encode($arr),//response
				'P07'=>json_encode($verify_post),//response
				'return_mode'=>'json'	
			];
			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);


			$verifying_post=$verify_post['Data'];
		} else{///
			$arr_post=[
				'method'=>'create_suncashme_checkout_log',
				'P01'=>$post_data['MerchantKey'],//MerchantKey
				'P02'=>$post_data['OrderID'],//order_id
				'P03'=>'',//reference_number
				'P04'=>'cenpos',//type
				'P05'=>'failed',//response
				'P06'=>json_encode($arr),//response
				'P07'=>json_encode($verify_post),//response
				'return_mode'=>'json'
			];
			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);

			$arr=[
				'msg'=>$verify_post['Message'],
				'data'=>$verify_post,
				'success'=>false
			];

			echo json_encode($arr);
		}
		$arr_post=[
			'verifyingpost'=>$verifying_post,
			'tokenid'=>$_POST['tokenid'],
		];
		$process_payment=$this->assetHelper->api_requestv2(CENPOS_PROCESS_URL,$arr_post);


		$process_payment=json_decode($process_payment,true);
		//dd($process_payment);
		//$s='';
		if($process_payment['Result']==0){//

			//update status ..
			$arr_update=[
				'method'=>'update_checkout_transaction',
				'P01'=>$post_data['OrderReference'],//reference
				'P02'=>'PROCESSED',//status
				'return_mode'=>'json'
			];
			$update_status=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr_update);
			//save transaction details....

			// $post_data['processing_fee']=!empty($post_data['processing_fee']) ? $post_data['processing_fee']  : 0.00;
			// $post_data['transaction_fee']=!empty($post_data['transaction_fee']) ? $post_data['transaction_fee']  : 0.00;

			$arr_post=[
				'method'=>'create_suncashme_cenpos_transaction_detail',
				'P01'=>$post_data['MerchantKey'],//MerchantKey
				'P02'=>$post_data['OrderID'],//order_id
				'P03'=>$process_payment['ReferenceNumber'],//reference_number
				'P04'=>$_POST['card_number'],//type
				'P05'=>$_POST['name_on_card'],//response
				'P06'=>$_POST['card_type'],//response
				'P07'=>number_format($post_data['Amount'],2,'.',''),//response
				'P08'=>number_format($upf,2,'.',''),//response,//response
				'P09'=>number_format($utf,2,'.',''),//response,//response
				'P10'=>number_format($post_data['transaction_fee']+$post_data['processing_fee']+$post_data['sunpass_fee']+$post_data['facility_fee'],2,'.',''),//totall of all fee master fee tne fee
				'P11'=>$post_data['sunpass_fee'],//forsuncash
				'P12'=>$post_data['facility_fee'],//for ticketing only
				'P13'=>0.00,//billpay fee
				'P14'=>0.00,//vat
				'P15'=>'checkout',//source checkout
				'P16'=>number_format($post_data['discount'], 2, '.', ''),//source checkout
				'P17'=>$iscard_fee_on_merch,
				'P18'=>'',//notes
				'P19'=>$_POST['name_customer_card'],
				'P20'=>$_POST['email_card'],
				'P21'=>$this->clean($_POST['mobile_card']),
				'return_mode'=>'json'
			];

			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);

			//saving logs....
			$arr_post=[
				'method'=>'create_suncashme_checkout_log',
				'P01'=>$post_data['MerchantKey'],//MerchantKey
				'P02'=>$post_data['OrderID'],//order_id
				'P03'=>$process_payment['ReferenceNumber'],//reference_number
				'P04'=>'cenpos',//type
				'P05'=>'success',//response
				'P06'=>json_encode($arr_post),//response
				'P07'=>json_encode($process_payment),//response
				'P08'=>$process_payment['Message'],//response
				'return_mode'=>'json'
			];
			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);
            $cfee_email = $post_data['iscard_fee_on_merch'] ==1 ? $cfee_email=0.00 : $cfee_email=number_format($upf,2,'.','')+number_format($utf,2,'.','');
			//email merchant process payment
			$arr=[
				"customer_name"=>$_POST['name_customer_card'],
				"amount"=>number_format($post_data['Amount'],2,'.',''),
				"transaction_id"=>$post_data['OrderID'],//cenpos
				"dba_name"=>$this->session->userdata('dba_name'),
				"creation_date"=>date('M d, Y'),
				"reference_num"=>$process_payment['ReferenceNumber'],
				"notes"=>'',
				"balance"=>'',
				"processing_fee"=>number_format($upf,2,'.',''),
				"transaction_fee"=>number_format($utf,2,'.',''),
				"fee"=>0.00,
				"vat"=>0.00,
				"cfee"=>$cfee_email,
				"tfee"=>0.00,
				"total"=>number_format($grand_total,2),  //amount +fee+vat
				"msg_above"=>"<h2>".$_POST['name_customer_card']." paid <span class='amount'>".number_format($grand_total,2)." BSD</span>. </h2>",
				"msg_bottom"=>"",
				"email"=>$this->session->userdata('email'),
				"view"=>'email_message_completed.blade.php',
				"title"=>'Completed Payment',
				"profile_pic" => $this->session->userdata("profile_pic"),
				"iscard_fee_on_merch"=>$post_data['iscard_fee_on_merch']
			];
			//dd($arr);
			$this->confirm_email($arr);
	
			//var_dump($update_status);die;
			unset($_SESSION['post_data']);
			//$reference_num=$process_payment['ReferenceNumber']
			//if(in_array($post_data['MerchantKey'],SUNPASS_MERCHANTS_KEYS)){
				$reference_num = $process_payment['ReferenceNumber']."||".$_POST['r'];
			//}

			$params=base64_encode($reference_num."||success||card");
			$arr=[
				'msg'=>$process_payment['Message'],
				'reference'=>$process_payment['ReferenceNumber'],
				'url'=>$post_data["CallbackURL"].$params,
				'payment_method'=>'Card',
				'success'=>true
			];

			echo json_encode($arr);
		} else {
			//saving logs....
			$arr_post=[
				'method'=>'create_suncashme_checkout_log',
				'P01'=>$post_data['MerchantKey'],//MerchantKey
				'P02'=>$post_data['OrderID'],//order_id
				'P03'=>'',//reference_number
				'P04'=>'cenpos',//type
				'P05'=>'failed',//response
				'P06'=>json_encode($arr_post),//response
				'P07'=>json_encode($process_payment),//response
				'return_mode'=>'json'
			];
			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);

			//unset($_SESSION['post_data']);

			$arr=[
				'msg'=>$process_payment['Message'],
				//'data'=>$process_payment,
				'success'=>false
			];
			echo json_encode($arr);
		}

	}

	public function process_card(){// suncashme payment api eto b ung luma nyan uu
		//var_dump($_SESSION);die;
		if(empty($_POST['tokenid'])){

			$arr=[	
				'msg'=>'Please fill up all fields',
				'success'=>false
			];
			
			echo json_encode($arr);

		} 
		$post_data =$this->session->userdata('post_data');
		//dd($post_data);
		//dd($_POST);
		// $pre_total = $post_data['amount']

		// if($pre_total<=0){
		// 	$pre_total=0.00;
		// }
		//check if fee is enabled.
		$arr=[
	    	'method'=>'login_business',
	        'P01'=> SERVICE_ACCOUNT_USERNAME_DEV,
	        'P02'=> SERVICE_ACCOUNT_PASSWORD_DEV,
	    ];
	    //$arr_tosend['url']="login_business?".http_build_query($arr);

	    //dd($arr_tosend);
	    $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
	    $api_bt_data=json_decode($api_bt_result,true);
	    //dd($api_bt_data);
	    $data['login_data']='';
	    if($api_bt_data['ResponseCode']=='YES'){
	    $data['login_data']=$api_bt_data['ResponseMessage'];
	    }


		$arr=[
			'method'=>'validate_payment_reference_id',
			'P01'=> $data['login_data']['SessionID'],
			'P02'=> $_POST['reference_num'],
		];
		$api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
		$api_bt_data=json_decode($api_bt_result,true);
		if($api_bt_data['Success']=='NO'){
			$arr=[	
				'msg'=>'Reference Id already Exist.',
				'success'=>false
			];
			
			echo json_encode($arr);
			exit;	       	
		}


       //get transaction fees. .
       $client_record_id = $_SESSION['client_record_id'];
       $arr=[
         'method'=>'get_convenience_fee',
            'P01'=> $data['login_data']['SessionID'],
            'P02'=> $client_record_id,
       ];

       // $arr_tosend['url']="login_business?".http_build_query($arr);

       //dd($arr);
       //print_r($arr);die;
       $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
       $api_bt_data=json_decode($api_bt_result,true);
       //dd($api_bt_result);
       //var_dump($api_bt_result);die;
       $data['transaction_fee_data']='';
       if($api_bt_data['Success']=='YES'){
       $api_bt_data['ResponseMessage']=isset($api_bt_data['ResponseMessage']) ? $api_bt_data['ResponseMessage'] : '';
       $transaction_fee_data=$api_bt_data['ResponseMessage'];
       } 
       $amount =str_replace(',', '', $_POST['amount']); //? $_POST['amount']  : $this->session->userdata('post_data')['Amount'];
       //dd($amount);
		$total= 0.00;
		$pf=0.00;
		$tf=0.00;
		$upf=0.00;
		$utf=0.00;
		$isenable='';
		if(!empty($transaction_fee_data)){
					$isenable=$transaction_fee_data['is_enable_cc_fee'];
					if($transaction_fee_data['is_enable_cc_fee']==1){

						$gtot =number_format($amount,2,'.','')+number_format($_POST['hid_vat'],2,'.','')+number_format($_POST['hid_fee'],2,'.','');

						if($transaction_fee_data['cc_processing_fee_isfixed']==1){//fixed
							//$fee+= $transaction_fee_data['cc_processing_fee'];
							$pf=$transaction_fee_data['cc_processing_fee'];
							$upf=$transaction_fee_data['cc_processing_fee'];
							//$total += floatval($amount)+floatval($fee);
						} else if($transaction_fee_data['cc_processing_fee_isfixed']==0){
							$percentage= $transaction_fee_data['cc_processing_fee']/100;
							$total_percent=number_format($gtot,2,'.','')*number_format($percentage,2,'.','');
							//echo $amount;
							//echo $fee;
							$pf=$total_percent;
							$upf=$total_percent;
						}
						//apply fee...
						if($transaction_fee_data['transaction_fee_isfixed']==1){//fixed
							//$fee+= $transaction_fee_data['transaction_fee'];
							$tf=$transaction_fee_data['transaction_fee'];
							$utf=$transaction_fee_data['transaction_fee'];
							//$total += floatval($amount)+floatval($fee);
						} else if($transaction_fee_data['transaction_fee_isfixed']==0){//percent
							$percentage=$transaction_fee_data['transaction_fee']/100;
							$total_percent=number_format($gtot,2,'.','')*number_format($percentage,2,'.','');
							//echo $fee."<br>";
							$tf=$total_percent;
							$utf=$total_percent;
						}
					} else {
						//
						$isenable=$transaction_fee_data['is_enable_cc_fee'];
						$gtot =$amount+$_POST['hid_vat']+$_POST['hid_fee'];

						if($transaction_fee_data['cc_processing_fee_isfixed']==1){//fixed
							//$fee+= $transaction_fee_data['cc_processing_fee'];
							$upf=$transaction_fee_data['cc_processing_fee'];
							//$total += floatval($amount)+floatval($fee);
						} else if($transaction_fee_data['cc_processing_fee_isfixed']==0){
							$percentage= $transaction_fee_data['cc_processing_fee']/100;
							$total_percent=number_format($gtot,2,'.','')*number_format($percentage,2,'.','');
							//echo $amount;
							//echo $fee;
							$upf=$total_percent;
						}
						//apply fee...
						if($transaction_fee_data['transaction_fee_isfixed']==1){//fixed
							//$fee+= $transaction_fee_data['transaction_fee'];
							$utf=$transaction_fee_data['transaction_fee'];
							//$total += floatval($amount)+floatval($fee);
						} else if($transaction_fee_data['transaction_fee_isfixed']==0){//percent
							$percentage=$transaction_fee_data['transaction_fee']/100;
							$total_percent=number_format($gtot,2,'.','')*number_format($percentage,2,'.','');
							//echo $fee."<br>";
							$utf=$total_percent;
						}

					}

			}

			//dd($iscard_fee_on_merch);
			//check if fee is enabled  end

			$_POST['hid_tfee']=!empty($_POST['hid_tfee']) ? $_POST['hid_tfee'] : 0.00;//response
			$_POST['hid_pfee']=!empty($_POST['hid_pfee']) ? $_POST['hid_pfee'] : 0.00;//response		

			// is_enable_cc_fee ==isenable
			// $isenable=0 convenience fee disabled on settings else 1
			// iscard_fee_on_merch =0-customer | 1-merchant cc fee charge to 
			$iscard_fee_on_merch = 0;
			$cfee_email = 0.00;
			// if($isenable==1 && $_SESSION['tag']=='MERCHANT'){
			// 	$iscard_fee_on_merch=0;
			// 	$grand_total =$amount+$_POST['hid_pfee']+$_POST['hid_tfee']+$_POST['hid_vat']+$_POST['hid_fee'];
			// 	//for email cfee 
			// 	$cfee_email = number_format($upf,2,'.','')+number_format($utf,2,'.','');
			// } else if ($isenable==0 && $_SESSION['tag']=='MERCHANT') {
			// 	//fee charge on merchant  and customer merchant
			// 	$iscard_fee_on_merch=1;//charge on merchant the fee
			// 	$grand_total =$amount+$_POST['hid_vat']+$_POST['hid_fee'];
			// 	//for email cfee 
			// 	$cfee_email =0.00;
			// }
			if($isenable==1 && $_SESSION['tag']=='MERCHANT'){
				$iscard_fee_on_merch=0;
				$grand_total =$amount+$_POST['hid_pfee']+$_POST['hid_tfee']+$_POST['hid_vat']+$_POST['hid_fee'];
				//for email cfee 
				$cfee_email = number_format($upf,2,'.','')+number_format($utf,2,'.','');
			} else if ($isenable==0 && $_SESSION['tag']=='MERCHANT') {
				//fee charge on merchant  and customer merchant
				$iscard_fee_on_merch=1;//charge on merchant the fee
				$grand_total =$amount+$_POST['hid_vat']+$_POST['hid_fee'];
				//for email cfee 
				$cfee_email =0.00;
			}
			
			//no convenience fee if is customer merchant temporary
			if($isenable==0 && $_SESSION['tag']=="CUSTOMER"){
				$utf=$_POST['hid_tfee'];
				$upf=$_POST['hid_pfee'];
				$grand_total =$amount+$_POST['hid_pfee']+$_POST['hid_tfee']+$_POST['hid_vat']+$_POST['hid_fee'];
				$iscard_fee_on_merch =0;
			}
			// dd($iscard_fee_on_merch);
			$grand_total = number_format($grand_total, 2, '.', '');
			//dd($grand_total);
			$auth_key=$this->assetHelper->create_tokken();
			//check money
			//$current_balance = $this->assetHelper->get_balance($_SESSION['SessionID']);
			//detect if merchant or customer.
			$user_type = $_SESSION['tag']=='CUSTOMER' ? $_SESSION['suntag_shortcode'] : $_SESSION['merchant_key'];
			//dd($grand_total);
			//$grand_total=50.01;

			if($grand_total <= 0){

				$arr=[	
					'msg'=>'Amount should be greather than 0',
					'success'=>false
				];
				
				echo json_encode($arr);
				exit;

			} 



			$arr=[
			'amount'=>$grand_total,
			'invoicenumber'=>$_POST['reference_num'],
			'type'=>'Sale',
			'email'=>'suncashme@gmail.com',
			'secretkey'=>CENPOST_MERCHANT_SECRETKEY,
			'merchant'=>CENPOST_MERCHANT_ID,
			'tokenid'=>$_POST['tokenid'],
			'type3d'=>"FunctionAuto",
			'cardinalReturn'=>"handle3DSecure",
			];
			//dd($arr);
			$verifying_post='';
			$verify_post=$this->assetHelper->api_requestv2(CENPOS_SITEVERIFY_URL,$arr);
			$verify_post=json_decode($verify_post,true);
			if($verify_post['Result']==0){
				$arr_post=[
					'method'=>'create_suncashme_checkout_log',
					'P01'=>$user_type,//MerchantKey
					'P02'=>$_POST['reference_num'],//order_id
					'P03'=>'',//reference_number
					'P04'=>'cenpos',//type
					'P05'=>'success',//response
					'P06'=>json_encode($arr),//response
					'P07'=>json_encode($verify_post),//response
					'return_mode'=>'json'	
				];
				$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);


				$verifying_post=$verify_post['Data'];
			} else{///
				$arr_post=[
					'method'=>'create_suncashme_checkout_log',
					'P01'=>$user_type,//MerchantKey
					'P02'=>$_POST['reference_num'],//order_id
					'P03'=>'',//reference_number
					'P04'=>'cenpos',//type
					'P05'=>'failed',//response
					'P06'=>json_encode($arr),//response
					'P07'=>json_encode($verify_post),//response
					'return_mode'=>'json'
				];
				$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);

				$arr=[
					'msg'=>$verify_post['Message'],
					'success'=>'false'
				];

				echo json_encode($arr);
			}
			$arr_post=[
				'verifyingpost'=>$verifying_post,
				'tokenid'=>$_POST['tokenid'],
				'type3d'=>"FunctionAuto",
				'cardinalReturn'=>"handle3DSecure",
			];
			$process_payment=$this->assetHelper->api_requestv2(CENPOS_PROCESS_URL,$arr_post);
			//var_dump($process_payment);die;

			$process_payment=json_decode($process_payment,true);
			if($process_payment['Result']==21){
				$arr=[
					'msg'=>'3dsecure af',
					'data'=>$process_payment,
					'view'=>html_entity_decode($process_payment['View3D']),
					'success'=>'3d'
				];

				echo json_encode($arr);
				exit;
			}
			if($process_payment['Result']==0){
				//saving logs....
				$arr_post=[
					'method'=>'create_suncashme_checkout_log',
					'P01'=>$user_type,//MerchantKey
					'P02'=>$_POST['reference_num'],//order_id
					'P03'=>$process_payment['ReferenceNumber'],//reference_number
					'P04'=>'cenpos',//type
					'P05'=>'success',//response
					'P06'=>json_encode($arr_post),//response
					'P07'=>json_encode($process_payment),//response
					'return_mode'=>'json'	
				];
				$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);


				//dd($utf."".$upf);
				$arr_post=[
					'method'=>'create_suncashme_cenpos_transaction_detail',
					'P01'=>$user_type,//MerchantKey
					'P02'=>$_POST['reference_num'],//order_id
					'P03'=>$process_payment['ReferenceNumber'],//reference_number
					'P04'=>$_POST['card_number'],//type
					'P05'=>$_POST['name_on_card'],//response
					'P06'=>$_POST['card_type'],//response
					'P07'=>$amount,//response
					'P08'=>number_format($utf,2,'.',''), //transaction fee fee
					'P09'=>number_format($upf,2,'.',''),//processing fee
					'P10'=>number_format($_POST['total_fee'],2,'.',''),//totall of all fee master fee tne fee
					'P11'=>0.00,//forsuncash
					'P12'=>0.00,//for ticketing only
					'P13'=>$_POST['hid_fee'],//billpay fee
					'P14'=>$_POST['hid_vat'],//vat
					'P15'=>'payment',//source checkout/payment/terminal/donation
					'P16'=>0.00,//discount
					'P17'=>$iscard_fee_on_merch,
					'P18'=>$_POST['notes'],
					'P19'=>$_POST['name'],
					'P20'=>$_POST['email'],	
					'P21'=>$this->clean($_POST['mobile']),		
					'return_mode'=>'json'	
				];
			// dd($arr_post);
				$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);			
				//dd($save_logs);

				//saving logs....
				$arr_post=[
					'method'=>'create_suncashme_checkout_log',
					'P01'=>$user_type,//MerchantKey
					'P02'=>$_POST['reference_num'],//order_id
					'P03'=>$process_payment['ReferenceNumber'],//reference_number
					'P04'=>'cenpos',//type
					'P05'=>'success',//response
					'P06'=>json_encode($arr_post),//response
					'P07'=>json_encode($save_logs),//response
					'P08'=>$process_payment['Message'],//response
					'return_mode'=>'json'
				];
				$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);

				//email merchant
				$arr=[
					"customer_name"=>$_POST['name'],
					"amount"=>$amount,
					"transaction_id"=>$_POST['reference_num'],//cenpos
					"dba_name"=>$this->session->userdata('dba_name'),
					"creation_date"=>date('M d, Y'),
					"reference_num"=>$process_payment['ReferenceNumber'],
					"notes"=>$_POST['notes'],
					"balance"=>'',
					"processing_fee"=>number_format($upf,2,'.',''),
					"transaction_fee"=>number_format($utf,2,'.',''),
					"fee"=>$_POST['hid_fee'],
					"vat"=>$_POST['hid_vat'],
					"cfee"=>$cfee_email,
					"tfee"=>number_format($_POST['hid_fee'],2,'.','')+number_format($_POST['hid_vat'],2,'.',''),
					"total"=>number_format($grand_total,2),  //amount +fee+vat
					"msg_above"=>"<h2>".$_POST['name']." paid <span class='amount'>".number_format($grand_total,2)." BSD</span>. </h2>",
					"msg_bottom"=>"",
					"email"=>$this->session->userdata('email'),
					"view"=>'email_message_completed.blade.php',
					"title"=>'Completed Payment',
					"profile_pic" => $this->session->userdata("profile_pic"),
					"iscard_fee_on_merch"=>$iscard_fee_on_merch
				];
				//dd($arr);
				$this->confirm_email($arr);

				//dd($save_logs);
				//
				//dd($_SESSION);
				unset($_SESSION);

				$url = base_url('payment/card_success');


				$arr=[
					'msg'=>$process_payment['Message'],
					'reference'=>$process_payment['ReferenceNumber'],
					'card_success'=>$url ,
					'payment_method'=>'Card',
					'success'=>'success'
				];

				echo json_encode($arr);
			} else {
				//saving logs....
				$arr_post=[
					'method'=>'create_suncashme_checkout_log',
					'P01'=>$user_type,//MerchantKey
					'P02'=>$_POST['reference_num'],//order_id
					'P03'=>'',//reference_number
					'P04'=>'cenpos',//type
					'P05'=>'failed',//response
					'P06'=>json_encode($arr_post),//response
					'P07'=>json_encode($process_payment),//response
					'return_mode'=>'json'	
				];
				$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);
				//save the card processing fee and transaction fee... via api ...
				//if(in_array($_SESSION['merchant_key'],SUNPASS_MERCHANTS_KEYS)){
				//$reference_number=$process_wallet['ResponseMessage']['TransactionId'];


				//dd($update_status);

				//}
				$arr=[
					'msg'=>$process_payment['Message'],
					'success'=>'false'
				];

				echo json_encode($arr);
			}
	}


	public function generate_passcode(){
		$required_fields=[
		'MerchantKey',
		'mobile',
		'pin',				
		];
		$is_validated=$this->assetHelper->checkpostfields($required_fields,$_POST);	

		if(!empty($is_validated)){
		$arr=[	
			'msg'=>'Following parmeters are mandatory: '.$is_validated,
			'success'=>false
		];
		
		echo json_encode($arr);
		exit;				
		}

		$arr_post=[
			'method'=>'generate_passcode',
			'P01'=>$this->clean($_POST['mobile']),//
			'P02'=>$_POST['pin'],//
			'P03'=>$_POST['MerchantKey'],//
			'return_mode'=>'json'	
		];
		$get_passcode=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr_post);	
		//dd($get_passcode);
		$get_passcode = json_decode($get_passcode,true);
		if($get_passcode['Success']=='YES'){
			$arr=[	
				'msg'=>'Valid Account.',
				'data'=>$get_passcode,
				'success'=>true
			];
			
			echo json_encode($arr);
			exit;	
		} else {
			$arr=[	
				'msg'=>'Not a Valid Account.',
				'success'=>false
			];
			
			echo json_encode($arr);
			exit;	
		}


	}

	public function process_suncash_payment(){
		if(empty($_POST['mobile']) || empty($_POST['passcode'])){

			$arr=[	
				'msg'=>'Please fill up all fields',
				'success'=>false
			];
			
			echo json_encode($arr);

		} 

		$arr=[
			'method'=>'get_client_checkout_details',
			'P01'=>$_POST['m'],
			'P02'=>$_POST['r'],
			'return_mode'=>'json'
		];
	    $api_fee_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
	    //dd($api_fee_result);
	    $api_fee_data=json_decode($api_fee_result,true);
	    //dd($api_fee_result);		
	    if($api_fee_data['Success']=='YES'){
	   	//check if pending...
/*	   	if($api_fee_data['ResponseMessage']['status']!='PENDING'){
	   		redirect('');
	   	}*/
	    //process order as individual array
	    $ItemName=[];
	    $ItemQty=[];
	    $ItemPrice=[];
	    if(!empty($api_fee_data['ResponseMessage']['details'])){
	    	for ($i=0; $i <count($api_fee_data['ResponseMessage']['details']) ; $i++) { 
	    		# code...
			    $ItemName[$i]=$api_fee_data['ResponseMessage']['details'][$i]['item_name'];
			    $ItemQty[$i]=$api_fee_data['ResponseMessage']['details'][$i]['qty'];
			    $ItemPrice[$i]=$api_fee_data['ResponseMessage']['details'][$i]['price'];		    		
	    	}
	    }
		$return_data=[
		'merchant_client_id'=>$api_fee_data['ResponseMessage']['merchant_client_id'],
		'MerchantKey'=>$api_fee_data['ResponseMessage']['merchant_key'],
		'MerchantName'=>$api_fee_data['ResponseMessage']['merchant_name'],
		'Amount'=>$api_fee_data['ResponseMessage']['amount'],
		'total_amount'=>$api_fee_data['ResponseMessage']['total_amount'],
		'OrderID'=>$api_fee_data['ResponseMessage']['order_id'],
		'CallbackURL'=>$api_fee_data['ResponseMessage']['callback_url'],
		'ItemName'=>$ItemName,
		'ItemQty'=>$ItemQty,
		'ItemPrice'=>$ItemPrice,
		'OrderReference'=>$_POST['r'],
		'profile_pic'=>$api_fee_data['ResponseMessage']['profile_pic'],
		'reference_id'=>$_POST['r'],
		'sunpass_fee'=>$api_fee_data['ResponseMessage']['sunpass_fee'],
		'facility_fee'=>$api_fee_data['ResponseMessage']['facility_fee'],
		'processing_fee'=>$api_fee_data['ResponseMessage']['processing_fee'],
		'transaction_fee'=>$api_fee_data['ResponseMessage']['transaction_fee'],
		'discount'=>$api_fee_data['ResponseMessage']['discount'],
		//'auth',
		];
		}

		$post_data =$return_data;



		//$post_data=$this->session->userdata('post_data');

		//dd($post_data);
		//process items...
		$order_details='';
		if(!empty($post_data['ItemName'])){
			$len = count($post_data['ItemName']);
			for ($i=0; $i <count($post_data['ItemName']) ; $i++) { 
				# code...
				if($len - 1==$i){
					$order_details.=$post_data['ItemName'][$i]."|".$post_data['ItemQty'][$i]."|".$post_data['ItemPrice'][$i]."";
				} else {
					$order_details.=$post_data['ItemName'][$i]."|".$post_data['ItemQty'][$i]."|".$post_data['ItemPrice'][$i]."~";
				}
			}
		}
		//dd($order_details);
		$arr=[
		   'method'=>'suncash_checkout',	
		   'P01' =>$post_data['MerchantKey'], //Merchant API Key
		   'P02' =>$this->clean($_POST['mobile']), //Mobile No
		   'P03' =>$_POST['passcode'], //passcode
		   'P04' =>$post_data['total_amount'], //amount
		   'P05' =>'BSD',//currency
		   'P06' =>$post_data['OrderID'], //reference no/order no
		   'P07' =>$order_details,//order details "item_name|qty|price" 
		   'P08'=>$post_data['OrderReference'],
		   'return_mode'=>'json'
		];
		//dd($arr);
		$process_wallet_x=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
		$process_wallet=json_decode($process_wallet_x,true);
		//dd($process_wallet_x);
		if($process_wallet['Success']=="YES"){

			//dd($process_payment);
			//saving logs....
			$reference_number=$process_wallet['ResponseMessage']['TransactionId'];


			$arr_update=[
				'method'=>'update_checkout_transaction',
				'P01'=>$post_data['OrderReference'],//reference 
				'P02'=>'PROCESSED',//status 
				'return_mode'=>'json'	
			];
			$update_status=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr_update);

			$arr=[
				'method'=>'get_suntag_shortcode',
				'P01'=>$post_data['MerchantKey'],//$this->session->userdata('SessionID'),
				'return_mode' => 'json'
			];
			$api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
			$api_bt_data=json_decode($api_bt_result,true);	
			// dd($api_bt_result);
			$data['suntag_data']='';
			if($api_bt_data['Success']=='YES'){
			$data['suntag_data']=$api_bt_data['ResponseMessage'];
			}else{
					$arr=[	
						'msg'=>'Something went wrong!',
						'success'=>false
					];
					echo json_encode($arr);
					exit;
			}  
            //$cfee_email = $post_data['iscard_fee_on_merch'] ==1 ? $cfee_email=0.00 : $cfee_email=number_format($upf,2,'.','')+number_format($utf,2,'.','');
			//email merchant process payment
			$arr=[
				"customer_name"=>$_POST['mobile'],
				"amount"=>number_format($post_data['Amount'],2,'.',''),
				"transaction_id"=>$post_data['OrderID'],//cenpos
				"dba_name"=>$this->session->userdata('dba_name'),
				"creation_date"=>date('M d, Y'),
				"reference_num"=>$reference_number,//referece ng wallet
				"notes"=>'',
				"balance"=>'',
				"processing_fee"=>0.00,
				"transaction_fee"=>0.00,
				"fee"=>0.00,
				"vat"=>0.00,
				"cfee"=>0.00,
				"tfee"=>0.00,
				"total"=>number_format($post_data['total_amount'],2),  //amount +fee+vat
				"msg_above"=>"<h2>".$_POST['mobile']." paid <span class='amount'>".number_format($post_data['total_amount'],2)." BSD</span>. </h2>",
				"msg_bottom"=>"",
				"email"=>$data['suntag_data']['business_email_address'],
				"view"=>'email_message_completed.blade.php',
				"title"=>'Completed Payment',
				"profile_pic" => $this->session->userdata("profile_pic"),
				"iscard_fee_on_merch"=>'',
			];
			//dd($arr);
			$this->confirm_email($arr);			

			$arr_post=[
				'method'=>'create_suncashme_checkout_log',
				'P01'=>$post_data['MerchantKey'],//MerchantKey
				'P02'=>$post_data['OrderID'],//order_id
				'P03'=>$reference_number,//reference_number
				'P04'=>'wallet',//type
				'P05'=>'success',//response
				'P06'=>json_encode($arr),//request
				'P07'=>json_encode($process_wallet),//response
				'P08'=>'Payment Successful',//response
				'return_mode'=>'json'	
			];
			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);
			//dd($save_logs);
			unset($_SESSION['post_data']);
			$params=base64_encode($reference_number."||".$_POST['r']."||success||wallet");
			$arr=[	
				'msg'=>'Successfully paid',
				'url'=>$post_data["CallbackURL"].$params,
				'success'=>true
			];
			
			echo json_encode($arr);	
			
		} else {
			$arr_post=[
				'method'=>'create_suncashme_checkout_log',
				'P01'=>$post_data['MerchantKey'],//MerchantKey
				'P02'=>$post_data['OrderID'],//order_id
				'P03'=>'',//reference_number
				'P04'=>'wallet',//type
				'P05'=>'failed',//response
				'P06'=>json_encode($arr),//response
				'P07'=>json_encode($process_wallet),//response
				'P08'=>'Payment Failed',//response
				'return_mode'=>'json'	
			];
			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);

			$arr=[	
				'msg'=>$process_wallet['ResponseMessage'],
				'success'=>false
			];
			
			echo json_encode($arr);		
		}
	
	}

	public function payment_process_sample(){
		$post_data = $_POST;
		$order_details='';
		if(!empty($post_data['ItemName'])){
			$len = count($post_data['ItemName']);
			for ($i=0; $i <count($post_data['ItemName']) ; $i++) { 
				# code...
				if($len - 1==$i){
					$order_details.=$post_data['ItemName'][$i]."|".$post_data['ItemQty'][$i]."|".$post_data['ItemPrice'][$i]."";	
				} else {
					$order_details.=$post_data['ItemName'][$i]."|".$post_data['ItemQty'][$i]."|".$post_data['ItemPrice'][$i]."~";	
				}
				
			}
		}
		$arr=[
		   'method'=>'payment',	
		   'P01' =>$post_data['MerchantKey'], //Merchant API Key
		   'P02' =>$_POST['MerchantName'], //Mobile No
		   'P03' =>$_POST['Amount'], //passcode
		   'P04' =>$post_data['OrderID'], //amount
		   'P05' =>$post_data['CallbackURL'],//currency
		   'P06' =>$order_details, //reference no/order no
		   'return_mode'=>'json'
		];
		//dd($arr);
		$process_sample=$this->assetHelper->api_requestv2(SUNCASH_CHECKOUT,$arr);
		$process_sample=json_decode($process_sample,true);
		//dd($process_sample);
		if($process_sample['Success']=="YES"){
			$arr=[	
				'msg'=>'Successfully process.',
				'url'=>$process_sample['ResponseMessage']['url'],
				'data'=>$process_sample['ResponseMessage'],
				'success'=>true
			];
			
			echo json_encode($arr);		
		} else {
			$arr=[	
				'msg'=>$process_sample['ResponseMessage'],
				'success'=>false
			];
			
			echo json_encode($arr);		
		}

	}

	public function sample_success($params){
		$params = base64_decode($params);
		list($reference_id,$status,$payment_method) = explode('||',$params);

		$data['reference_id'] = $reference_id;
		$data['status'] = $status;
		$data['payment_method'] = $payment_method;

		$this->blade->view('checkout_return-sample-page',$data);
	}
	public function cancel_page($params,$callbackurl){
		$params = base64_decode($params);
		$callbackurl=base64_decode($callbackurl);
		$callbackurl=urldecode($callbackurl);
		list($payment_reference,$reference_id,$status,$payment_method) = explode('||',$params);
		//dd();
		$data['callbackurl']=$callbackurl;
		$data['payment_reference'] = $payment_reference;
		$data['reference_id'] = $reference_id;
		$data['status'] = $status;
		$data['payment_method'] = $payment_method;
		//dd($data);
		$this->blade->view('cancelled_page',$data);
	}
	public function cancel_order(){
		$arr_update=[
			'method'=>'update_checkout_transaction',
			'P01'=>$_POST['reference_id'],//reference 
			'P02'=>'CANCELLED',//status 
			'P03'=>'',//status 
			'return_mode'=>'json'	
		];
		// dd($arr_update);
		$update_status=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr_update);
		$update_status=json_decode($update_status,true);
				// dd($update_status);
		if($update_status['Success']=='YES'){
			$params=base64_encode("0000||".$_POST['reference_id']."||failed||cancelled");
			$arr=[	
				'msg'=>$update_status['ResponseMessage'],
				'url'=>base_url('payment/cancel_page/'),
				'params'=>$params,
				'success'=>true
			];
			
			echo json_encode($arr);		
		} else {

			$arr=[	
				'msg'=>$update_status['ResponseMessage'],
				'success'=>false
			];
			
			echo json_encode($arr);					
		}

	}


	public function process_cash_checkout_old(){
		//get temp tokken...
		//dd($_POST);
		$mobile = $this->clean($this->security->xss_clean($this->input->post('mobile_cash')));
		$auth_key=$this->assetHelper->create_tokken();
		//dd($this->session->userdata('post_data'));
	    $arr=[
	    	'method'=>'create_cash_business_billpay',
	    	'P01'=>$auth_key['temp_auth'],//$auth_key['temp_auth'],
            'P02'=> $this->session->userdata('post_data')['merchant_client_id'],//merch id
            'P03'=> $this->session->userdata('post_data')['MerchantName'],//suntag
            'P04'=> str_replace( ',', '',$this->session->userdata('post_data')['Amount']),//amount+fee+vat
            'P05'=> $this->session->userdata('post_data')['reference_id'],
            'P06'=> '',//$this->input->post('notes'),
            'P07'=>	$this->input->post('firstName'),
            'P08'=>	$this->input->post('lastName'),
            'P09'=>	$this->input->post('email'),
            'P10'=>	$mobile,
            'P11'=>	$this->input->post('is_email'),
            'P12'=>	$this->input->post('is_sms'),
            'return_mode'=>'json'

	    ];

	     //dd($arr);

	    $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr);
	  	//dd($api_bt_result);
	    $api_bt_data=json_decode($api_bt_result,true);
	    //dd($api_bt_data);
         if($api_bt_data['ResponseCode']=='0000'){
         	//email...
         	//transaction_id
			//qr_code
			//barcode
			//mobile
			//email
			//creation_date
			if($this->input->post('is_email')==1){
			$qr=Qr::GenerateQr($api_bt_data['ResponseMessage'],'',140);
			$barcode=Qr::GenerateBarcode128($api_bt_data['ResponseMessage'],'','');
			$qr= base64_decode($qr->generate());
			$barcode= base64_decode($barcode);	
			$total=	(float)$this->session->userdata('post_data')['Amount'];//+(float)$this->input->post('hid_fee')+(float)$this->input->post('hid_vat');	
			$arr=[
				"amount"=>$this->session->userdata('post_data')['Amount'],
				"transaction_id"=>$api_bt_data['ResponseMessage'],
				"fee"=>'0.00',//$this->input->post('hid_fee'),
				"vat"=>'0.00',//$this->input->post('hid_vat'),
				"total_amount"=>$total,
				//"qr_code"=>'<img src="data:'.$qr->getContentType().';base64,'.$qr->generate().'" />',
				//"barcode"=>'<img src="data:image/png;base64,'.$barcode.'" />',
				"creation_date"=>date('M d, Y'),
				"email"=>$this->input->post('email'),
				"mobile"=>$this->input->post('mobile_cash'),
				"view"=>'email_payment_code.blade.php',
				"title"=>'Payment Code',
				"profile_pic" => $this->session->userdata("post_data")['profile_pic'],
				//<img src="data:'.$qr->getContentType().';base64,'.$qr->generate().'" />
			];

	        $image_attachment[0]['img_string']=$qr;
	        $image_attachment[0]['img_name']='qr_code';
	        $image_attachment[1]['img_string']=$barcode;
	        $image_attachment[1]['img_name']='barcode';

			$this->pending_email_cash($arr,$image_attachment);

			}
			$this->session->set_userdata('cash_posted_data',$_POST);
			$this->session->set_userdata('cash_payment_code',$api_bt_data['ResponseMessage']);
			$arr=[	
				'msg'=>$api_bt_data['ResponseMessage'],
				'url'=>base_url('payment/cash_payment_success'),
				'success'=>true
			];
			
			echo json_encode($arr);
			exit;         	
         } else {
			$arr=[	
				'msg'=>$api_bt_data['ResponseMessage'],
				'success'=>false
			];
			
			echo json_encode($arr);
			exit;
         }		
	}

	public function get_fees(){
		//var_dump($_SESSION);die;

	    $arr=[
	    	'method'=>'login_business',
	        'P01'=> SERVICE_ACCOUNT_USERNAME_DEV,
	        'P02'=> SERVICE_ACCOUNT_PASSWORD_DEV,
	    ];
	    //$arr_tosend['url']="login_business?".http_build_query($arr);

	    //dd($arr_tosend);
	    $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
	    $api_bt_data=json_decode($api_bt_result,true);
	    //dd($api_bt_data);
	    $data['login_data']='';
	    if($api_bt_data['ResponseCode']=='YES'){
	    $data['login_data']=$api_bt_data['ResponseMessage'];                     
	    }		

       //get transaction fees. 
       $client_id = isset($_POST['client_id']) ?  $_POST['client_id'] : $this->session->userdata('post_data')['merchant_client_id'] ;
       $arr=[
         'method'=>'get_convenience_fee',
            'P01'=> $data['login_data']['SessionID'],
            'P02'=> $client_id,
       ];
       // $arr_tosend['url']="login_business?".http_build_query($arr);

       //dd($arr);
       //print_r($arr);die;
       $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
       $api_bt_data=json_decode($api_bt_result,true);
       //var_dump($api_bt_result);die;
       $transaction_fee_data='';
       if($api_bt_data['Success']=='YES'){
       $api_bt_data['ResponseMessage']=isset($api_bt_data['ResponseMessage']) ? $api_bt_data['ResponseMessage'] : '';
       $transaction_fee_data=$api_bt_data['ResponseMessage'];                   
       } 
      //print_r($transaction_fee_data);die;
       //apply fee...
       //$fee=0.00;
       $amount = isset($_POST['amount']) ? $_POST['amount']  : $this->session->userdata('post_data')['Amount'];
       $total= 0.00;
       $pf=0.00;
       $tf=0.00;
       $upf=0.00;
       $utf=0.00;
       $isenable='';
       if(!empty($transaction_fee_data)){
       	   $isenable=$transaction_fee_data['is_enable_cc_fee'];
	       if($transaction_fee_data['is_enable_cc_fee']==1){
		       if($transaction_fee_data['cc_processing_fee_isfixed']==1){//fixed
		       		//$fee+= $transaction_fee_data['cc_processing_fee'];
		       		$pf=$transaction_fee_data['cc_processing_fee'];
		       		$upf=$transaction_fee_data['cc_processing_fee'];
		       		//$total += floatval($amount)+floatval($fee);
		       } else if($transaction_fee_data['cc_processing_fee_isfixed']==0){
		       		$percentage= $transaction_fee_data['cc_processing_fee']/100;
		       		$total_percent=$amount*$percentage;
		       		//echo $amount;
		       		//echo $fee;
		       		$pf=$total_percent;
		       		$upf=$total_percent;
		       }
		       //apply fee...
		       if($transaction_fee_data['transaction_fee_isfixed']==1){//fixed
		       		//$fee+= $transaction_fee_data['transaction_fee'];
		       		$tf=$transaction_fee_data['transaction_fee'];
		       		$utf=$transaction_fee_data['transaction_fee'];		       		
		       		//$total += floatval($amount)+floatval($fee);
		       } else if($transaction_fee_data['transaction_fee_isfixed']==0){//percent
		       		$percentage=$transaction_fee_data['transaction_fee']/100;
		       		$total_percent=$amount*$percentage;
		       		//echo $fee."<br>";
		       		$tf=$total_percent;
		       		$utf=$total_percent;		       		
		       }  
	       } else {
		       if($transaction_fee_data['cc_processing_fee_isfixed']==1){//fixed
		       		//$fee+= $transaction_fee_data['cc_processing_fee'];
		       		$upf=$transaction_fee_data['cc_processing_fee'];
		       		//$total += floatval($amount)+floatval($fee);
		       } else if($transaction_fee_data['cc_processing_fee_isfixed']==0){
		       		$percentage= $transaction_fee_data['cc_processing_fee']/100;
		       		$total_percent=$amount*$percentage;
		       		//echo $amount;
		       		//echo $fee;
		       		$upf=$total_percent;
		       }
		       //apply fee...
		       if($transaction_fee_data['transaction_fee_isfixed']==1){//fixed
		       		//$fee+= $transaction_fee_data['transaction_fee'];
		       		$utf=$transaction_fee_data['transaction_fee'];
		       		//$total += floatval($amount)+floatval($fee);
		       } else if($transaction_fee_data['transaction_fee_isfixed']==0){//percent
		       		$percentage=$transaction_fee_data['transaction_fee']/100;
		       		$total_percent=$amount*$percentage;
		       		//echo $fee."<br>";
		       		$utf=$total_percent;
		       }  	       	
	       }  


       }
       $_POST['discount'] = isset($_POST['discount']) ? $_POST['discount']  : 0.00;
       $_POST['promo_code'] = isset($_POST['promo_code']) ? $_POST['promo_code']  : '';
       	//echo "total".$fee;
  		//compute.
  		$total = floatval($amount)+floatval($tf)+floatval($pf);
  		$total_fee = floatval($tf)+floatval($pf);
  		// $iscard_fee_on_merch=0;
  		// if($isenable==0){
  		// $iscard_fee_on_merch=1;
  		
  		$iscard_fee_on_merch=0;
  		if($isenable==0){
  		$iscard_fee_on_merch=1;
  		}

  		//update fees and total via api af....
		$arr_update=[
			'method'=>'update_checkout_transaction_fee',
			'P01'=>$_POST['r'],//reference 
			'P02'=>number_format($upf,2,'.',''),//status 
			'P03'=>number_format($utf,2,'.',''),//status 
			'P04'=>$total,//status 
			'P05'=>number_format($_POST['discount'],2,'.',''),//status 
			'P06'=>$_POST['promo_code'],//status 
			'P07'=>$iscard_fee_on_merch,
			'return_mode'=>'json'	
		];
		//dd($arr_update);
		$update_status=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr_update);

  		//update session and fees.
  		$_SESSION['post_data']['Amount'] = $total;
  		$_SESSION['post_data']['pf'] = $pf;
  		$_SESSION['post_data']['tf'] = $tf;
  		$_SESSION['post_data']['fee'] = $total_fee;
  		//$this->session->userdata('post_data')['Amount']
  		//dd($this->session->userdata('post_data')['Amount']);
		$arr=[	
			'fee'=>number_format($total_fee,2,'.',''),
			'pf'=>number_format($pf,2,'.',''),
			'tf'=>number_format($tf,2,'.',''),
			'total'=>number_format($total,2,'.',''),
			//'session'=>$_SESSION['post_data'],
			'success'=>true
		];
		
		echo json_encode($arr);
		exit;   


	}//
	public function get_fees_payment(){//$suncashme payment
		//var_dump($_SESSION);die;

	    $arr=[
	    	'method'=>'login_business',
	        'P01'=> SERVICE_ACCOUNT_USERNAME_DEV,
	        'P02'=> SERVICE_ACCOUNT_PASSWORD_DEV,
	    ];
	    //$arr_tosend['url']="login_business?".http_build_query($arr);

	    //dd($arr_tosend);
	    $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
	    $api_bt_data=json_decode($api_bt_result,true);
	    //dd($api_bt_data);
	    $data['login_data']='';
	    if($api_bt_data['ResponseCode']=='YES'){
	    $data['login_data']=$api_bt_data['ResponseMessage'];                     
	    }		

       //get transaction fees. 
       $client_record_id = $_SESSION['client_record_id'];
       $arr=[
         'method'=>'get_convenience_fee',
            'P01'=> $data['login_data']['SessionID'],
            'P02'=> $client_record_id,
       ];
       // $arr_tosend['url']="login_business?".http_build_query($arr);

       //dd($arr);
       // print_r($arr);die;
       $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
       $api_bt_data=json_decode($api_bt_result,true);
       // var_dump($api_bt_result);die;
       $transaction_fee_data='';
       if($api_bt_data['Success']=='YES'){
       $api_bt_data['ResponseMessage']=isset($api_bt_data['ResponseMessage']) ? $api_bt_data['ResponseMessage'] : '';
       $transaction_fee_data=$api_bt_data['ResponseMessage'];                   
       } 
      // print_r($api_bt_result);die;
       //apply fee...
       //$fee=0.00;
       $amount = isset($_POST['amount']) ? $_POST['amount']  : $this->session->userdata('post_data')['Amount'];
       $total= 0.00;
       $pf=0.00;
       $tf=0.00;
       $upf=0.00;
       $utf=0.00;
       $isenable='';
       if(!empty($transaction_fee_data)){
       	   $isenable=$transaction_fee_data['is_enable_cc_fee'];
	       if($transaction_fee_data['is_enable_cc_fee']==1){
		       if($transaction_fee_data['cc_processing_fee_isfixed']==1){//fixed
		       		//$fee+= $transaction_fee_data['cc_processing_fee'];
		       		$pf=$transaction_fee_data['cc_processing_fee'];
		       		$upf=$transaction_fee_data['cc_processing_fee'];
		       		//$total += floatval($amount)+floatval($fee);
		       } else if($transaction_fee_data['cc_processing_fee_isfixed']==0){
		       		$percentage= $transaction_fee_data['cc_processing_fee']/100;
		       		$total_percent=$amount*$percentage;
		       		//echo $amount;
		       		//echo $fee;
		       		$pf=$total_percent;
		       		$upf=$total_percent;
		       }
		       //apply fee...
		       if($transaction_fee_data['transaction_fee_isfixed']==1){//fixed
		       		//$fee+= $transaction_fee_data['transaction_fee'];
		       		$tf=$transaction_fee_data['transaction_fee'];
		       		$utf=$transaction_fee_data['transaction_fee'];		       		
		       		//$total += floatval($amount)+floatval($fee);
		       } else if($transaction_fee_data['transaction_fee_isfixed']==0){//percent
		       		$percentage=$transaction_fee_data['transaction_fee']/100;
		       		$total_percent=$amount*$percentage;
		       		//echo $fee."<br>";
		       		$tf=$total_percent;
		       		$utf=$total_percent;		       		
		       }  
	       } else {
		       if($transaction_fee_data['cc_processing_fee_isfixed']==1){//fixed
		       		//$fee+= $transaction_fee_data['cc_processing_fee'];
		       		$upf=$transaction_fee_data['cc_processing_fee'];
		       		//$total += floatval($amount)+floatval($fee);
		       } else if($transaction_fee_data['cc_processing_fee_isfixed']==0){
		       		$percentage= $transaction_fee_data['cc_processing_fee']/100;
		       		$total_percent=$amount*$percentage;
		       		//echo $amount;
		       		//echo $fee;
		       		$upf=$total_percent;
		       }
		       //apply fee...
		       if($transaction_fee_data['transaction_fee_isfixed']==1){//fixed
		       		//$fee+= $transaction_fee_data['transaction_fee'];
		       		$utf=$transaction_fee_data['transaction_fee'];
		       		//$total += floatval($amount)+floatval($fee);
		       } else if($transaction_fee_data['transaction_fee_isfixed']==0){//percent
		       		$percentage=$transaction_fee_data['transaction_fee']/100;
		       		$total_percent=$amount*$percentage;
		       		//echo $fee."<br>";
		       		$utf=$total_percent;
		       }  	       	
	       }  
	   }
       	//echo "total".$fee;
  		//compute.
  		$total = floatval($amount)+floatval($tf)+floatval($pf);
  		$total_fee = floatval($tf)+floatval($pf);

  		$iscard_fee_on_merch=0;
  		if($isenable==0 && $_SESSION['tag']=='MERCHANT'){
  		$iscard_fee_on_merch=1;
  		}
  		
  		//update session and fees.
  		$_SESSION['post_data']['Amount'] = $total;
  		$_SESSION['post_data']['pf'] = $pf;
  		$_SESSION['post_data']['tf'] = $tf;
  		$_SESSION['post_data']['fee'] = $total_fee;
  		$_SESSION['post_data']['iscard_fee_on_merch'] = $iscard_fee_on_merch;
  		// dd($iscard_fee_on_merch);
  		//$this->session->userdata('post_data')['Amount']
  		//dd($this->session->userdata('post_data')['Amount']);
		$arr=[	
			'fee'=>number_format($total_fee,2,'.',''),
			'pf'=>number_format($pf,2,'.',''),
			'tf'=>number_format($tf,2,'.',''),
			'total'=>number_format($total,2,'.',''),
			'success'=>true
		];
		
		echo json_encode($arr);
		exit;   


	}

	public function restorefee(){
		$_SESSION['post_data']['Amount'] = $_POST['total'];	
  		$_SESSION['post_data']['pf'] = 0.00;
  		$_SESSION['post_data']['tf'] = 0.00;
  		$_SESSION['post_data']['fee'] = 0.00;

       $_POST['discount'] = isset($_POST['discount']) ? $_POST['discount']  : 0.00;
       $_POST['promo_code'] = isset($_POST['promo_code']) ? $_POST['promo_code']  : '';

		$arr_update=[
			'method'=>'update_checkout_transaction_fee',
			'P01'=>$_POST['r'],//reference 
			'P02'=>0.00,//status 
			'P03'=>0.00,//status 
			'P04'=>$_POST['total'],//status 
			'P05'=>$_POST['discount'],//status 
			'P06'=>$_POST['promo_code'],//status 
			'return_mode'=>'json'	
		];
		//dd($arr_update);
		$update_status=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr_update);

		$arr=[	
			'success'=>true
		];
		
		echo json_encode($arr);
		exit; 		
	}
	public function process_cash_checkout(){
		//get temp tokken...
		//dd($_POST);
		//recall api staging on saving..
		$arr=[
			'method'=>'get_client_checkout_details',
			'P01'=>$_POST['m'],
			'P02'=>$_POST['r'],
			'return_mode'=>'json'
		];
	    $api_fee_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
	    //dd($api_fee_result);
	    $api_fee_data=json_decode($api_fee_result,true);
	    //dd($api_fee_result);		
	    if($api_fee_data['Success']=='YES'){
	   	//check if pending...
/*	   	if($api_fee_data['ResponseMessage']['status']!='PENDING'){
	   		redirect('');
	   	}*/
	    //process order as individual array
	    $ItemName=[];
	    $ItemQty=[];
	    $ItemPrice=[];
	    if(!empty($api_fee_data['ResponseMessage']['details'])){
	    	for ($i=0; $i <count($api_fee_data['ResponseMessage']['details']) ; $i++) { 
	    		# code...
			    $ItemName[$i]=$api_fee_data['ResponseMessage']['details'][$i]['item_name'];
			    $ItemQty[$i]=$api_fee_data['ResponseMessage']['details'][$i]['qty'];
			    $ItemPrice[$i]=$api_fee_data['ResponseMessage']['details'][$i]['price'];		    		
	    	}
	    }
		$return_data=[
		'merchant_client_id'=>$api_fee_data['ResponseMessage']['merchant_client_id'],
		'MerchantKey'=>$api_fee_data['ResponseMessage']['merchant_key'],
		'MerchantName'=>$api_fee_data['ResponseMessage']['merchant_name'],
		'Amount'=>$api_fee_data['ResponseMessage']['amount'],
		'total_amount'=>$api_fee_data['ResponseMessage']['total_amount'],
		'OrderID'=>$api_fee_data['ResponseMessage']['order_id'],
		'CallbackURL'=>$api_fee_data['ResponseMessage']['callback_url'],
		'ItemName'=>$ItemName,
		'ItemQty'=>$ItemQty,
		'ItemPrice'=>$ItemPrice,
		'OrderReference'=>$_POST['r'],
		'profile_pic'=>$api_fee_data['ResponseMessage']['profile_pic'],
		'reference_id'=>$_POST['r'],
		'sunpass_fee'=>$api_fee_data['ResponseMessage']['sunpass_fee'],
		'facility_fee'=>$api_fee_data['ResponseMessage']['facility_fee'],
		'processing_fee'=>$api_fee_data['ResponseMessage']['processing_fee'],
		'transaction_fee'=>$api_fee_data['ResponseMessage']['transaction_fee'],
		'discount'=>$api_fee_data['ResponseMessage']['discount'],
		//'auth',				
		];		
		}

		$post_data =$return_data;
			
		//dd($post_data);
		//detect if ios af.
		//$this->load->library('Mobile_Detect');
		$pre_total = $post_data['Amount']-$post_data['discount'];
		if($pre_total<=0){
			$pre_total=0.00;
		}
		$grand_total =$pre_total+$post_data['sunpass_fee']+$post_data['facility_fee'];
		$grand_total = number_format($grand_total, 2, '.', '');



		$mobile = $this->clean($this->security->xss_clean($this->input->post('mobile_cash')));
		$auth_key=$this->assetHelper->create_tokken();
		//dd($this->session->userdata('post_data'));
	    $arr=[
	    	'method'=>'create_cash_business_billpay',
	    	'P01'=>$auth_key['temp_auth'],//$auth_key['temp_auth'],
            'P02'=> $post_data['merchant_client_id'],//merch id
            'P03'=> $post_data['MerchantName'],//suntag
            'P04'=> str_replace( ',', '',$grand_total),//amount+sunpass fee , facility fee
            'P05'=> $post_data['reference_id'],
            'P06'=> '',//$this->input->post('notes'),
            'P07'=>	$this->input->post('firstName'),
            'P08'=>	$this->input->post('lastName'),
            'P09'=>	$this->input->post('email'),
            'P10'=>	$mobile,
            'P11'=>	$this->input->post('is_email'),
            'P12'=>	$this->input->post('is_sms'),
            'P13'=>$post_data['sunpass_fee'],//sunpass fee
            'P14'=>$post_data['facility_fee'],//sunpass facility fee
            'P15'=>$post_data['discount'],//generic discount
            'return_mode'=>'json'
	    ];

	     // dd($arr);

	    $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr);
	  	//dd($api_bt_result);
	    $api_bt_data=json_decode($api_bt_result,true);
	    //dd($api_bt_data);
         if($api_bt_data['ResponseCode']=='0000'){
         	//email...
         	//transaction_id
			//qr_code
			//barcode
			//mobile
			//email
			//creation_date
			if($this->input->post('is_email')==1){
			$qr=Qr::GenerateQr($api_bt_data['ResponseMessage'],'',140);
			$barcode=Qr::GenerateBarcode128($api_bt_data['ResponseMessage'],'','');
			$qr= base64_encode(base64_decode($qr->generate()));
			$barcode=base64_encode(base64_decode($barcode));
			$total=	(float)$post_data['Amount'];//+(float)$this->input->post('hid_fee')+(float)$this->input->post('hid_vat');

			$arr=[
				"amount"=>$post_data['Amount'],
				"transaction_id"=>$api_bt_data['ResponseMessage'],
				"fee"=>'0.00',//$this->input->post('hid_fee'),
				"vat"=>'0.00',//$this->input->post('hid_vat'),
				"total_amount"=>$total,
				// "qr_code"=>'<img src="data:image/png;base64,'.$qr.'  class="qr">',
				// "barcode"=>'<img src="data:image/png;base64,'.$barcode.' class="barcode">',
				"creation_date"=>date('M d, Y'),
				"email"=>$this->input->post('email'),
				"mobile"=>$this->input->post('mobile_cash'),
				"view"=>'email_payment_code.blade.php',
				"title"=>'Payment Code',
				"profile_pic" => $post_data['profile_pic'],
				//<img src="data:'.$qr->getContentType().';base64,'.$qr->generate().'" />
			];

	        $image_attachment[0]['img_string']=base64_decode($qr);
	        $image_attachment[0]['img_name']='qr_code';
	        $image_attachment[1]['img_string']=base64_decode($barcode);
	        $image_attachment[1]['img_name']='barcode';

			$this->pending_email_cash($arr,$image_attachment);

			}

			
			$to_send['cash_post_data']=$post_data;
			$to_send['cash_posted_data']=$_POST;
			$to_send['cash_payment_code']=$api_bt_data['ResponseMessage'];
			$to_send['cash_payment_barcode']=$barcode;
			$to_send['cash_payment_qr']=$qr;

			$to_send = json_encode($to_send);
			$params = base64_encode($to_send);
			
			$arr=[	
				'msg'=>$api_bt_data['ResponseMessage'],
				'url'=>base_url('payment/checkout_cashpayment_code/'.$params),
				'success'=>true
			];
			
			echo json_encode($arr);
			exit;         	
         } else {
			$arr=[	
				'msg'=>$api_bt_data['ResponseMessage'],
				'success'=>false
			];
			
			echo json_encode($arr);
			exit;
         }		
	}
	
	public function checkout_cashpayment_code($datas){
	    $param = base64_decode($datas);
	    $param =json_decode($param,true);
		// $data['tokken'] = $this->assetHelper->create_tokken();
		$data['post_data']=$param['cash_post_data'];	
		$data['cash_posted_data']=$param['cash_posted_data'];
		$data['cash_payment_code']=$param['cash_payment_code'];		
		$data['cash_payment_qr']=$param['cash_payment_qr'];		
		$data['cash_payment_barcode']=$param['cash_payment_barcode'];	
		$data['cash_return_params'] = base64_encode($data['post_data']['reference_id']."||".$data['cash_posted_data']['r']."||success||cash");
		//	$data['qr_content_type']=$cash_posted_data['qr_content_type');	
			
	//	dd($data['cash_return_params']);
			//unset($_SESSION['post_data']);
			//unset($_SESSION['cash_posted_data']);
			//unset($_SESSION['cash_payment_code']);
		$this->blade->view('checkout_cashpayment_code',$data);
	}
	public function get_promo(){
		//dd($_POST);
		//dd($_SESSION);
			$arr=[
				'method'=>'get_client_checkout_order_details',
				'P01'=>$_POST['m'],//merchantkey
				'P02'=>$_POST['o'],//$order id,
				'return_mode'=>'json'
			];
			//dd($arr); get_client_checkout_order_details
		    $api_fee_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
		    //dd($api_fee_result);
		    $api_fee_data=json_decode($api_fee_result,true);
/*		    $pf=0.00;
		    $tf=0.00;	*/
		    $subtotal = 0.00;
		    $sf=0.00;
		    $ff=0.00;
		    if($api_fee_data['Success']=='YES'){
		    	$subtotal=$api_fee_data['ResponseMessage']['amount'];
		    	$sf=$api_fee_data['ResponseMessage']['sunpass_fee'];
		    	$ff=$api_fee_data['ResponseMessage']['facility_fee'];
		    	//get need to pass.
		    	$other_data=json_decode($api_fee_data['ResponseMessage']['other_data'],true);

		    	//dd($other_data);
		    	$ticket_id='';
		    	$ticket_qty='';
		    	$ticket_price='';
				if(!empty($other_data['data']['ticket_id'])){
					//dd('af');
					$len = count($other_data['data']['ticket_id']);
					for ($i=0; $i <count($other_data['data']['ticket_id']) ; $i++) { 
						# code...
						if($len - 1==$i){
							$ticket_id.=$other_data['data']['ticket_id'][$i].",";	
							$ticket_qty.=$other_data['data']['ticket_qty'][$i].",";
							$ticket_price.=$other_data['data']['ticket_price'][$i].",";
						} else {
							$ticket_id.=$other_data['data']['ticket_id'][$i].",";	
							$ticket_qty.=$other_data['data']['ticket_qty'][$i].",";
							$ticket_price.=$other_data['data']['ticket_price'][$i].",";	
						}
						
					}
				}

			    $arr=[
			    	'method'=>'login_mobile',
		            'P01'=> md5(SERVICE_ACCOUNT_CUSTOMER_PASSWORD_DEV),
		            'P02'=> SERVICE_ACCOUNT_CUSTOMER_USERNAME_DEV,
		            'return_mode' => 'json'
			    ];
			    // $arr_tosend['url']="login_mobile?".http_build_query($arr);

			    // dd($arr);
			    $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr);
			    $api_bt_data=json_decode($api_bt_result,true);
			    //dd($api_bt_data);
				//dd($ticket_id."x".$ticket_qty."x".$ticket_price);
		    	//get promo if they have.
		    	$promo_arr=[
		    		'method'=>'get_discount_promocode',
		    		'P01'=>$api_bt_data['ResponseMessage']['SessionID'],
		    		'P02'=>$other_data['data']['event_id'],
		    		'P03'=>$_POST['promo_code'],
		    		'P04'=>$ticket_id,
		    		'P05'=>$ticket_qty,
		    		'P06'=>$ticket_price,
		    		'return_mode' => 'json'
		    	];
		    	//dd($promo_arr);
				$promo_result=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$promo_arr);	
				$promo_data=json_decode($promo_result,true);
				//dd($promo_result);
				if($promo_data['ResponseCode']=='0000'){
					//compute session amount 
					$pretotal = $subtotal-$promo_data['ResponseMessage']['discount'];
					if($pretotal<=0){
						$pretotal =0.00;
					}
					$total=$pretotal+$sf+$ff;
/*					$_SESSION['post_data']['Amount']=$pretotal+$_SESSION['post_data']['sunpass_fee']+$_SESSION['post_data']['facility_fee'];
	  				$_SESSION['post_data']['promo_code'] = $_POST['promo_code'];
	  				$_SESSION['post_data']['discount'] =$promo_data['ResponseMessage']['discount'];*/

					$arr_update=[
						'method'=>'update_checkout_transaction_fee',
						'P01'=>$_POST['r'],//reference 
						'P02'=>$_POST['p'],//status 
						'P03'=>$_POST['t'],//status 
						'P04'=>$total,//status 
						'P05'=>number_format($promo_data['ResponseMessage']['discount'],2,'.',''),//status 
						'P06'=>$_POST['promo_code'],//status 
						'return_mode'=>'json'	
					];
					//dd($arr_update);
					$update_status=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr_update);
					//dd($update_status);

					$arr=[	
						//'msg'=>$api_bt_data['ResponseMessage'],
						'discount'=>number_format($promo_data['ResponseMessage']['discount'],2,'.',''),
						'success'=>true
					];
					
					echo json_encode($arr);
					exit;    
				} else if ($promo_data['ResponseCode']=='1000'){
					$arr=[	
						//'msg'=>$api_bt_data['ResponseMessage'],
						'msg'=>$promo_data['ResponseMessage'],
						'success'=>false
					];
					
					echo json_encode($arr);
					exit;  					
				}
		    }


			
	}

	public function check_whitelist(){
		//dd($_POST);

	    $arr=[
	    	'method'=>'login_business',
	        'P01'=> SERVICE_ACCOUNT_USERNAME_DEV,
	        'P02'=> SERVICE_ACCOUNT_PASSWORD_DEV,
	    ];
	    //$arr_tosend['url']="login_business?".http_build_query($arr);

	    //dd($arr_tosend);
	    $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
	    $api_bt_data=json_decode($api_bt_result,true);
	    //dd($api_bt_data);
	    $data['login_data']='';
	    if($api_bt_data['ResponseCode']=='YES'){
	    $data['login_data']=$api_bt_data['ResponseMessage'];                     
	    }	
	    //check first if amount is on limit.
	    $_POST['ProtectedCardNumber']=ltrim($_POST['ProtectedCardNumber'], '*'); 
		//get payment settings. 
       	$arr=[
         	'method'=>'check_card_limit',
            'P01'=> $data['login_data']['SessionID'],
            'P02'=> $_POST['ProtectedCardNumber'],
            'P03'=> $_POST['CardType'],
            'P04'=> $_POST['NameonCard'],
            'P05'=> $_POST['merchant_key'],
            'P06'=> $_POST['amount'],
       	];
       	$api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
       	//dd($api_bt_result);
      	$api_bt_data=json_decode($api_bt_result,true);
	    if($api_bt_data['Success']=='YES'){
			if($api_bt_data['ResponseMessage']['status']=='approved'){
				$arr=[
					'success'=>true
				];
				echo json_encode($arr);
				exit;
			} else {

				$arr=[
					'data'=>$api_bt_data['ResponseMessage'],
					'status'=>$api_bt_data['ResponseMessage']['status'],
					'success'=>false
				];

				echo json_encode($arr);
				exit;

			}
	    }

		//check if orderid is unique//may bug pa din pag nag palit value sa card page
   //     $arr=[
   //       'method'=>'validate_payment_reference_id',
   //          'P01'=> $data['login_data']['SessionID'],
   //          'P02'=> $_POST['reference_num'],
   //     ];
   //     // $arr_tosend['url']="login_business?".http_build_query($arr);

   //     // dd($arr);
   //     //print_r($arr);die;
   //     $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
   //     $api_bt_data=json_decode($api_bt_result,true);
   //     	dd($api_bt_result);
   //     if($api_bt_data['Success']=='NO'){
			// $arr=[	
			// 	'msg'=>'Reference Id already exist.',
			// 	'success'=>false
			// ];
			
			// echo json_encode($arr);
			// exit;	       	
   //     }

	    //trim 
	    $_POST['ProtectedCardNumber']=ltrim($_POST['ProtectedCardNumber'], '*'); 
		//get payment settings. 
       	$arr=[
         	'method'=>'check_card_validity',
            'P01'=> $data['login_data']['SessionID'],
            'P02'=> $_POST['ProtectedCardNumber'],
            'P03'=> $_POST['CardType'],
            'P04'=> $_POST['NameonCard'],
            'P05'=> $_POST['merchant_key'],
            'P06'=> $_POST['amount'],
       	];
       	//dd($arr);
       	// $arr_tosend['url']="login_business?".http_build_query($arr);

       	// dd($arr_tosend);
       	//dd($arr);
       	$api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
       	//dd($api_bt_result);
      	$api_bt_data=json_decode($api_bt_result,true);
	    if($api_bt_data['Success']=='YES'){
	    	if(!empty($api_bt_data['ResponseMessage'])){
	    		//show info to customer to upload necessary 
	    		//dd($api_bt_data['ResponseMessage']);
				if($api_bt_data['ResponseMessage']['status']=='approved'){
					$arr=[	
						'success'=>true
					];
					
					echo json_encode($arr);
					exit; 
				} else {

					$arr=[	
						'data'=>$api_bt_data['ResponseMessage'],
						'status'=>$api_bt_data['ResponseMessage']['status'],
						'success'=>false
					];
					
					echo json_encode($arr);
					exit;

				}	    		


	    	} else {
	    		//create card info.
	    		$_POST['merchant_key']  = isset($_POST['merchant_key']) ? $_POST['merchant_key'] : '';
		       	$arr=[
		         	'method'=>'create_card_whitelist',
		            'P01'=> $data['login_data']['SessionID'],
		            'P02'=> $_POST['ProtectedCardNumber'],
		            'P03'=> $_POST['CardType'],
		            'P04'=> $_POST['NameonCard'],
		            'P05'=> $_POST['merchant_key'],
		            'P06'=> $_POST['source'],
		       	];
		       	//dd($arr);
		       	// $arr_tosend['url']="login_business?".http_build_query($arr);

		       	// dd($arr_tosend);
		       	//dd($arr);
		       	$api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
		       	//dd($api_bt_result);
		      	$api_bt_data=json_decode($api_bt_result,true);

	      		if($api_bt_data['Success']=='YES'){
				$arr=[	
					//'msg'=>$api_bt_data['ResponseMessage'],
					'msg'=>$api_bt_data['ResponseMessage'],
					'success'=>true
				];
				
				echo json_encode($arr);
				exit;  	
				}
	    		
	    	}
	    //$data['login_data']=;                     
	    }

	}

	public function update_whitelist(){
			//dd($_POST);
			if(empty($_POST)){
				$arr=[	
					'msg'=>'safari not working af',					
					'success'=>false
				];
				
				echo json_encode($arr);
				exit;
			}

			//required post
			$required_input=[
				'last4digits',
				'card_type_w',
				'card_name',
				'card_id',
				'card_mobile_number',
				'card_email',
				'card_id_upload',
				'credit_card_upload',
				'cc_with_card_upload',

				//'wid',
			];
			$label=[
				'last4digits'=>'Last 4 digits',
				'card_type_w'=>'Card Type',
				'card_name'=>'Card Name',
				'card_id'=>'Card ID',
				'card_mobile_number'=>'Mobile',
				'card_email'=>'Email',
				'card_id_upload'=>'Upload ID Picture',
				'credit_card_upload'=>'Upload Credit Card Picture.',
				'cc_with_card_upload'=>'Upload Credit Card with ID Picture.',				
				//'wid'=>'',
			];					
		
			$validated_fields=$this->assetHelper->checkpostfields($required_input,$_POST,$label);
			//dd($validated_fields);
			if(!empty($validated_fields)){
				$arr=[	
					'msg'=>'field required '.$validated_fields,
					'success'=>false
				];
				
				echo json_encode($arr);
				exit;				
			}
/*			$required_input=[
				'card_id_upload',
				'credit_card_upload',
				'cc_with_card_upload',

				//'wid',
			];
			$label=[
				'card_id_upload'=>'Upload ID Picture',
				'credit_card_upload'=>'Upload Credit Card Picture.',
				'cc_with_card_upload'=>'Upload Credit Card with ID Picture.',
				//'wid'=>'',
			];					
		
			$validated_fields=$this->assetHelper->checkpostfields($required_input,$_FILES,$label);
			//dd($validated_fields);
			if(!empty($validated_fields)){
				$arr=[	
					'msg'=>'field required '.$validated_fields,
					'success'=>false
				];
				
				echo json_encode($arr);
				exit;				
			}*/


		$card_id_url = '';
		if(!empty($_POST['card_id_upload'])){
			//die($key);	
			 //ci upload.
			$filename="".date("Y")."_".time()."_".rand().".png";
			$decodedData = base64_decode($_POST['card_id_upload']);
			// APPPATH will give you application folder path
			

			if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
			    $root_path = dirname(dirname(dirname(__FILE__)));
			    $tosend=$root_path.'\assets\img\whitelist_picture\ids\\'.$filename;
			} else {
			    $root_path = dirname(dirname(dirname(dirname(__FILE__))));
			    $tosend=$root_path.'/html/assets/img/whitelist_picture/ids/'.$filename;
			}

			$is_ok1=file_put_contents($tosend,$decodedData);
			if (!$is_ok1) {
				//$this->upload->display_errors();
				$arr=[	
					'msg'=>'Upload failed2',					
					'success'=>false
				];
				
				echo json_encode($arr);
				exit;
			}

/*			 $config['image_library'] = 'gd2';
			 
			 $config['upload_path'] = './assets/img/whitelist_picture/ids';
			 $config['allowed_types'] = '*';	//gif|jpg|png|PNG|jpeg|JPG|JPEG
			 $config['file_name'] = $filename;
			 $config['encrypt_name']= FALSE;
			

			if(!is_dir($config['upload_path'])) {
			 	mkdir($config['upload_path'], 0777, TRUE);
			}

			 //load ci upload						
			 $this->upload->initialize($config);
			 //$this->upload->do_upload($key);
			if ( ! $this->upload->do_upload("card_id_upload")) {
				$this->upload->display_errors();
				$arr=[	
					'msg'=>$this->upload->display_errors(),
					'success'=>false
				];
				
				echo json_encode($arr);
				exit;
			} 	*/

			$card_id_url= base_url()."assets/img/whitelist_picture/ids/".$filename;
		} 	

		$credit_card_url = '';
		if(!empty($_POST['credit_card_upload'])){
			//die($key);	
			 //ci upload.
			 //$config = array();

			$filename="".date("Y")."_".time()."_".rand().".png";
			$decodedData = base64_decode($_POST['credit_card_upload']);

			if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
			    $root_path = dirname(dirname(dirname(__FILE__)));
			    $tosend=$root_path.'\assets\img\whitelist_picture\cc\\'.$filename;
			} else {
			    $root_path = dirname(dirname(dirname(dirname(__FILE__))));
			    $tosend=$root_path . '/html/assets/img/whitelist_picture/cc/'.$filename;
			}

			// APPPATH will give you application folder path
			$is_ok2=file_put_contents($tosend,$decodedData);
			if (!$is_ok2) {
				//$this->upload->display_errors();
				$arr=[	
					'msg'=>'Upload failed2',
					'success'=>false
				];
				
				echo json_encode($arr);
				exit;
			}

/*			 $config['image_library'] = 'gd2';
			 $filename="".date("Y")."_".time()."_".rand().".png";
			 $config['upload_path'] = './assets/img/whitelist_picture/cc';
			 $config['allowed_types'] = '*';	//gif|jpg|png|PNG|jpeg|JPG|JPEG
			 $config['file_name'] = $filename;
			 $config['encrypt_name']= FALSE;


			if(!is_dir($config['upload_path'])) {
			 	mkdir($config['upload_path'], 0777, TRUE);
			}

			 //load ci upload						
			 $this->upload->initialize($config);
			 //$this->upload->do_upload($key);
			if ( ! $this->upload->do_upload("credit_card_upload")) {
				//$this->upload->display_errors();
				$arr=[	
					'msg'=>'Upload Failed.',
					'success'=>false
				];
				
				echo json_encode($arr);
				exit;
			} 	
*/
			$credit_card_url=base_url()."assets/img/whitelist_picture/cc/".$filename;
		} 

		$ccwithid_card_url='';
		if(!empty($_POST['cc_with_card_upload'])){


			$filename="".date("Y")."_".time()."_".rand().".png";
			$decodedData = base64_decode($_POST['cc_with_card_upload']);

			if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
			    $root_path = dirname(dirname(dirname(__FILE__)));
			    $tosend=$root_path.'\assets\img\whitelist_picture\ccwithid\\'.$filename;
			} else {
			    $root_path = dirname(dirname(dirname(dirname(__FILE__))));
			    $tosend=$root_path . '/html/assets/img/whitelist_picture/ccwithid/'.$filename;
			}
			// APPPATH will give you application folder path
			$is_ok3=file_put_contents($tosend,$decodedData);
			if (!$is_ok3) {
				//$this->upload->display_errors();
				$arr=[	
					'msg'=>'Upload failed3',
					'success'=>false
				];
				
				echo json_encode($arr);
				exit;
			}

			//die($key);	
			 //ci upload.
			 //$config = array();
/*			 $config['image_library'] = 'gd2';
			 $filename="".date("Y")."_".time()."_".rand().".png";
			 $config['upload_path'] = './assets/img/whitelist_picture/ccwithid';
			 $config['allowed_types'] = '*';	//gif|jpg|png|PNG|jpeg|JPG|JPEG
			 $config['file_name'] = $filename;
			 $config['encrypt_name']= FALSE;

			if(!is_dir($config['upload_path'])) {
			 	mkdir($config['upload_path'], 0777, TRUE);
			}
			 
			 //load ci upload						
			 $this->upload->initialize($config);
			 //$this->upload->do_upload($key);
			if ( ! $this->upload->do_upload("cc_with_card_upload")) {
				//$this->upload->display_errors();
				$arr=[	
					'msg'=>'Upload Failed.',
					'success'=>false
				];
				
				echo json_encode($arr);
				exit;
			} 	*/

			$ccwithid_card_url=base_url()."assets/img/whitelist_picture/ccwithid/".$filename;
		} 


		//after upload post it on suncash....
	    $arr=[
	    	'method'=>'login_business',
	        'P01'=> SERVICE_ACCOUNT_USERNAME_DEV,
	        'P02'=> SERVICE_ACCOUNT_PASSWORD_DEV,
	    ];
	    //$arr_tosend['url']="login_business?".http_build_query($arr);

	    //dd($arr_tosend);
	    $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
	    $api_bt_data=json_decode($api_bt_result,true);
	    //dd($api_bt_data);
	    $data['login_data']='';
	    if($api_bt_data['ResponseCode']=='YES'){
	    $data['login_data']=$api_bt_data['ResponseMessage'];                     
	    }	

	    //trim 
	    $_POST['last4digits']=ltrim($_POST['last4digits'], '*'); 
			//get payment settings. 
			//
		$card_mobile = $this->clean($this->security->xss_clean($this->input->post('card_mobile_number')));
				
       	$arr=[
         	'method'=>'create_card_whitelist_request',
            'P01'=> $data['login_data']['SessionID'],
            'P02'=> $_POST['last4digits'],
            'P03'=> $_POST['card_type_w'],
            'P04'=> $_POST['card_name'],
            'P05'=> $_POST['card_id'],
            'P06'=> $card_id_url,
            'P07'=> $credit_card_url,
            'P08'=> $ccwithid_card_url,
            'P09'=> $_POST['merckey'],
            'P10'=> $_POST['wid'],
            'P11'=> $card_mobile,
            'P12'=> $_POST['card_email'],
       	];
       	//dd($arr);
       	// $arr_tosend['url']="login_business?".http_build_query($arr);

       	// dd($arr_tosend);
       	//dd($arr);
       	$api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
       	//dd($api_bt_result);
      	$api_bt_data=json_decode($api_bt_result,true);
	    if($api_bt_data['Success']=='YES'){
				$arr=[	
					//'msg'=>$api_bt_data['ResponseMessage'],
					'msg'=>'Request Successfully submitted.',
					'success'=>true
				];
				
				echo json_encode($arr);
				exit;
	    } else {
				$arr=[	
					'msg'=>$api_bt_data['ResponseMessage'],
					//'msg'=>'Request Successfully submitted.',
					'success'=>false
				];
				
				echo json_encode($arr);
				exit;	    	
	    }
	}

	public function revalidate_whitelist($id,$extra){
		//$data=[];//get_card_whitelist
		//after upload post it on suncash....

		if($extra!='z1x2c3'){
			dd("Invalid data.");
		}
		if(!is_numeric($id)){
			dd("Invalid data.");
		}		
	    $arr=[
	    	'method'=>'login_business',
	        'P01'=> SERVICE_ACCOUNT_USERNAME_DEV,
	        'P02'=> SERVICE_ACCOUNT_PASSWORD_DEV,
	    ];
	    //$arr_tosend['url']="login_business?".http_build_query($arr);

	    //dd($arr_tosend);
	    $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
	    $api_bt_data=json_decode($api_bt_result,true);
	    //dd($api_bt_data);
	    $data['login_data']='';
	    if($api_bt_data['ResponseCode']=='YES'){
	    $data['login_data']=$api_bt_data['ResponseMessage'];
		}

	    //trim 
		//get payment settings.
		//
		$card_mobile = $this->clean($this->security->xss_clean($this->input->post('card_mobile_number')));

       	$arr=[
         	'method'=>'get_card_whitelist',
            'P01'=> $data['login_data']['SessionID'],
            'P02'=> $id,
       	];
       	//dd($arr);
       	// $arr_tosend['url']="login_business?".http_build_query($arr);

       	// dd($arr_tosend);
       	//dd($arr);
       	$api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
       	//dd($api_bt_result);
      	$api_bt_data=json_decode($api_bt_result,true);
      	//dd($api_bt_result);
      	$data['validation_data']='';
	    if($api_bt_data['Success']=='YES'){
	    $data['validation_data']=$api_bt_data['ResponseMessage'];
	    } 



		$this->blade->view('whitelist_form_update',$data);
	}

	public function process_card_credit(){

		//var_dump($_SESSION);die;
		if(empty($_POST['tokenid'])){

			$arr=[	
				'msg'=>'Please fill up all fields',
				'success'=>false
			];
			
			echo json_encode($arr);

		} 
		$post_data =$this->session->userdata('post_data');
		//dd($post_data);
		//dd($_POST);
		// $pre_total = $post_data['amount']

		// if($pre_total<=0){
		// 	$pre_total=0.00;
		// }
		//check if fee is enabled.
		$arr=[
	    	'method'=>'login_business',
	        'P01'=> SERVICE_ACCOUNT_USERNAME_DEV,
	        'P02'=> SERVICE_ACCOUNT_PASSWORD_DEV,
	    ];
	    //$arr_tosend['url']="login_business?".http_build_query($arr);

	    //dd($arr_tosend);
	    $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
	    $api_bt_data=json_decode($api_bt_result,true);
	    //dd($api_bt_data);
	    $data['login_data']='';
	    if($api_bt_data['ResponseCode']=='YES'){
	    $data['login_data']=$api_bt_data['ResponseMessage'];                     
	    }		

       //get transaction fees. .
       $client_record_id = $_SESSION['client_record_id'];
       $arr=[
         'method'=>'get_convenience_fee',
            'P01'=> $data['login_data']['SessionID'],
            'P02'=> $client_record_id,
       ];

       // $arr_tosend['url']="login_business?".http_build_query($arr);

       //dd($arr);
       //print_r($arr);die;
       $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
       $api_bt_data=json_decode($api_bt_result,true);
       //dd($api_bt_result);
       //var_dump($api_bt_result);die;
       $data['transaction_fee_data']='';
       if($api_bt_data['Success']=='YES'){
       $api_bt_data['ResponseMessage']=isset($api_bt_data['ResponseMessage']) ? $api_bt_data['ResponseMessage'] : '';
       $transaction_fee_data=$api_bt_data['ResponseMessage'];                   
       } 
       $amount =str_replace(',', '', $_POST['amount']); //? $_POST['amount']  : $this->session->userdata('post_data')['Amount'];
       //dd($amount);
       $total= 0.00;
       $pf=0.00;
       $tf=0.00;
       $upf=0.00;
       $utf=0.00;
       $isenable='';
	     if(!empty($transaction_fee_data)){
	       	   $isenable=$transaction_fee_data['is_enable_cc_fee'];
		       if($transaction_fee_data['is_enable_cc_fee']==1){

				   $gtot =number_format($amount,2,'.','')+number_format($_POST['hid_vat'],2,'.','')+number_format($_POST['hid_fee'],2,'.','');

			       if($transaction_fee_data['cc_processing_fee_isfixed']==1){//fixed
			       		//$fee+= $transaction_fee_data['cc_processing_fee'];
			       		$pf=$transaction_fee_data['cc_processing_fee'];
			       		$upf=$transaction_fee_data['cc_processing_fee'];
			       		//$total += floatval($amount)+floatval($fee);
			       } else if($transaction_fee_data['cc_processing_fee_isfixed']==0){
			       		$percentage= $transaction_fee_data['cc_processing_fee']/100;
			       		$total_percent=number_format($gtot,2,'.','')*number_format($percentage,2,'.','');
			       		//echo $amount;
			       		//echo $fee;
			       		$pf=$total_percent;
			       		$upf=$total_percent;
			       }
			       //apply fee...
			       if($transaction_fee_data['transaction_fee_isfixed']==1){//fixed
			       		//$fee+= $transaction_fee_data['transaction_fee'];
			       		$tf=$transaction_fee_data['transaction_fee'];
			       		$utf=$transaction_fee_data['transaction_fee'];		       		
			       		//$total += floatval($amount)+floatval($fee);
			       } else if($transaction_fee_data['transaction_fee_isfixed']==0){//percent
			       		$percentage=$transaction_fee_data['transaction_fee']/100;
			       		$total_percent=number_format($gtot,2,'.','')*number_format($percentage,2,'.','');
			       		//echo $fee."<br>";
			       		$tf=$total_percent;
			       		$utf=$total_percent;		       		
			       }  
		       } else {
		       		//
		       		$isenable=$transaction_fee_data['is_enable_cc_fee'];
		       		$gtot =$amount+$_POST['hid_vat']+$_POST['hid_fee'];

			       if($transaction_fee_data['cc_processing_fee_isfixed']==1){//fixed
			       		//$fee+= $transaction_fee_data['cc_processing_fee'];
			       		$upf=$transaction_fee_data['cc_processing_fee'];
			       		//$total += floatval($amount)+floatval($fee);
			       } else if($transaction_fee_data['cc_processing_fee_isfixed']==0){
			       		$percentage= $transaction_fee_data['cc_processing_fee']/100;
			       		$total_percent=number_format($gtot,2,'.','')*number_format($percentage,2,'.','');
			       		//echo $amount;
			       		//echo $fee;
			       		$upf=$total_percent;
			       }
			       //apply fee...
			       if($transaction_fee_data['transaction_fee_isfixed']==1){//fixed
			       		//$fee+= $transaction_fee_data['transaction_fee'];
			       		$utf=$transaction_fee_data['transaction_fee'];
			       		//$total += floatval($amount)+floatval($fee);
			       } else if($transaction_fee_data['transaction_fee_isfixed']==0){//percent
			       		$percentage=$transaction_fee_data['transaction_fee']/100;
			       		$total_percent=number_format($gtot,2,'.','')*number_format($percentage,2,'.','');
			       		//echo $fee."<br>";
			       		$utf=$total_percent;
			       }  

		       }  


	       }		




       	//dd($iscard_fee_on_merch);
		//check if fee is enabled  end

		$_POST['hid_tfee']=!empty($_POST['hid_tfee']) ? $_POST['hid_tfee'] : 0.00;//response
		$_POST['hid_pfee']=!empty($_POST['hid_pfee']) ? $_POST['hid_pfee'] : 0.00;//response		

		// is_enable_cc_fee ==isenable
		// $isenable=0 convenience fee disabled on settings else 1
		// iscard_fee_on_merch =0-customer | 1-merchant cc fee charge to 
		$iscard_fee_on_merch = 0;
		if($isenable==1 && $_SESSION['tag']=='MERCHANT'){
			$iscard_fee_on_merch=0;
			$grand_total =$amount+$_POST['hid_pfee']+$_POST['hid_tfee']+$_POST['hid_vat']+$_POST['hid_fee'];		
		} else {
			//fee charge on merchant  and customer merchant
			$iscard_fee_on_merch=1;//charge on merchant the fee
			$grand_total =$amount+$_POST['hid_vat']+$_POST['hid_fee'];
		}

		// dd($iscard_fee_on_merch);
		$grand_total = number_format($grand_total, 2, '.', '');
		//dd($grand_total);
		$auth_key=$this->assetHelper->create_tokken();
		//check money
		//$current_balance = $this->assetHelper->get_balance($_SESSION['SessionID']);
		//detect if merchant or customer.
		$user_type = $_SESSION['tag']=='CUSTOMER' ? $_SESSION['suntag_shortcode'] : $_SESSION['merchant_key'];
		//dd($grand_total);
		//$grand_total=50.01;
		if($grand_total <= 0){

			$arr=[	
				'msg'=>'Amount should be greather than 0',
				'success'=>false
			];
			
			echo json_encode($arr);
			exit;

		} 

		$arr=[
		'amount'=>$grand_total,
		'invoicenumber'=>$_POST['reference_num'],
		'type'=>'Sale',
		'email'=>'suncashme@gmail.com',
		'secretkey'=>CENPOST_MERCHANT_SECRETKEY,
		'merchant'=>CENPOST_MERCHANT_ID,
		'tokenid'=>$_POST['tokenid'],
		'type3d'=>"FunctionAuto",
		'cardinalReturn'=>"handle3DSecure",
		];
		//dd($arr);
/*		$verifying_post='';
		$verify_post=$this->assetHelper->api_requestv2(CENPOS_SITEVERIFY_URL,$arr);
		$verify_post=json_decode($verify_post,true);
		if($verify_post['Result']==0){
			$arr_post=[
				'method'=>'create_suncashme_checkout_log',
				'P01'=>$user_type,//MerchantKey
				'P02'=>$_POST['reference_num'],//order_id
				'P03'=>'',//reference_number
				'P04'=>'cenpos',//type
				'P05'=>'success',//response
				'P06'=>json_encode($arr),//response
				'P07'=>json_encode($verify_post),//response
				'return_mode'=>'json'	
			];
			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);


			$verifying_post=$verify_post['Data'];
		}else{///
			$arr_post=[
				'method'=>'create_suncashme_checkout_log',
				'P01'=>$user_type,//MerchantKey
				'P02'=>$_POST['reference_num'],//order_id
				'P03'=>'',//reference_number
				'P04'=>'cenpos',//type
				'P05'=>'failed',//response
				'P06'=>json_encode($arr),//response
				'P07'=>json_encode($verify_post),//response
				'return_mode'=>'json'	
			];
			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);

			$arr=[	
				'msg'=>$verify_post['Message'],
				'success'=>'false'
			];
			
			echo json_encode($arr);			
		}*/
		//record return of use tokken 
		$arr_post=[
			'method'=>'create_suncashme_checkout_log',
			'P01'=>$user_type,//MerchantKey
			'P02'=>$_POST['reference_num'],//order_id
			'P03'=>'',//reference_number
			'P04'=>'cenpos',//type
			'P05'=>'success',//response
			'P06'=>json_encode($arr),//response
			'P07'=>json_encode($_POST['cp_return']),//response
			'return_mode'=>'json'	
		];
		$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);


/*		$arr_post=[
			'verifyingpost'=>$verifying_post,
			'tokenid'=>$_POST['tokenid'],
			'type3d'=>"FunctionAuto",
			'cardinalReturn'=>"handle3DSecure",
		];
		$process_payment=$this->assetHelper->api_requestv2(CENPOS_PROCESS_URL,$arr_post);
		//var_dump($process_payment);die;

		$process_payment=json_decode($process_payment,true);*/
/*		if($process_payment['Result']==21){
			$arr=[	
				'msg'=>'3dsecure af',
				'data'=>$process_payment,
				'view'=>html_entity_decode($process_payment['View3D']),
				'success'=>'3d'
			];
			
			echo json_encode($arr);
			exit;				
		}*/
		// if($process_payment['Result']==0){
			//saving logs....
/*			$arr_post=[
				'method'=>'create_suncashme_checkout_log',
				'P01'=>$user_type,//MerchantKey
				'P02'=>$_POST['reference_num'],//order_id
				'P03'=>$process_payment['ReferenceNumber'],//reference_number
				'P04'=>'cenpos',//type
				'P05'=>'success',//response
				'P06'=>json_encode($arr_post),//response
				'P07'=>json_encode($process_payment),//response
				'return_mode'=>'json'	
			];
			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post)*/;

			//no convenience fee if is customer merchant temporary
			if($isenable==0 && $_SESSION['tag']=="CUSTOMER"){
				$utf=0.00;
				$upf=0.00;
				$iscard_fee_on_merch =0;
			}
			//dd($utf."".$upf);
			$arr_post=[
				'method'=>'create_suncashme_cenpos_transaction_detail',
				'P01'=>$user_type,//MerchantKey
				'P02'=>$_POST['reference_num'],//order_id
				'P03'=>$process_payment['ReferenceNumber'],//reference_number
				'P04'=>$_POST['card_number'],//type
				'P05'=>$_POST['name_on_card'],//response
				'P06'=>$_POST['card_type'],//response
				'P07'=>$amount,//response
				'P08'=>number_format($utf,2,'.',''), //transaction fee fee           
				'P09'=>number_format($upf,2,'.',''),//processing fee
				'P10'=>$_POST['total_fee'],//totall of all fee master fee tne fee
				'P11'=>0.00,//forsuncash
				'P12'=>0.00,//for ticketing only
				'P13'=>$_POST['hid_fee'],//billpay fee
				'P14'=>$_POST['hid_vat'],//vat
				'P15'=>'payment',//source checkout/payment/terminal/donation
				'P16'=>0.00,//discount
				'P17'=>$iscard_fee_on_merch,
				'P18'=>$_POST['notes'],
				'P19'=>'',//for donation
				'P20'=>'',//for donation				
				'return_mode'=>'json'	
			];
		//dd($arr_post);
			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);			
			//dd($save_logs);
			$suncash_credit=json_decode($save_logs,true);
			//saving logs....
			if($suncash_credit['ResponseMessage']=='0000'){
			$arr_post=[
				'method'=>'create_suncashme_checkout_log',
				'P01'=>$user_type,//MerchantKey
				'P02'=>$_POST['reference_num'],//order_id
				'P03'=>$process_payment['ReferenceNumber'],//reference_number
				'P04'=>'cenpos',//type
				'P05'=>'success',//response
				'P06'=>json_encode($arr_post),//response
				'P07'=>json_encode($save_logs),//response
				'P08'=>$process_payment['Message'],//response
				'return_mode'=>'json'	
			];
			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);
			//dd($save_logs);
			//
			//dd($_SESSION);
			unset($_SESSION);

			$url = base_url('payment/card_success');


			$arr=[	
				'msg'=>$process_payment['Message'],
				'reference'=>$process_payment['ReferenceNumber'],
				'card_success'=>$url ,
				'payment_method'=>'Card',
				'success'=>true
			];
			
			echo json_encode($arr);	
			exit;				
			} else {
				$arr_post=[
					'method'=>'create_suncashme_checkout_log',
					'P01'=>$user_type,//MerchantKey
					'P02'=>$_POST['reference_num'],//order_id
					'P03'=>'',//reference_number
					'P04'=>'cenpos',//type
					'P05'=>'failed',//response
					'P06'=>json_encode($arr_post),//response
					'P07'=>json_encode($suncash_credit),//response
					'return_mode'=>'json'	
				];
				$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);



				$arr=[	
					'msg'=>$suncash_credit['ResponseMessage'],
					'success'=>false
				];
				
				echo json_encode($arr);	
				exit;	

			}

		// } else {
		// 	//saving logs....
		// 	$arr_post=[
		// 		'method'=>'create_suncashme_checkout_log',
		// 		'P01'=>$user_type,//MerchantKey
		// 		'P02'=>$_POST['reference_num'],//order_id
		// 		'P03'=>'',//reference_number
		// 		'P04'=>'cenpos',//type
		// 		'P05'=>'failed',//response
		// 		'P06'=>json_encode($arr_post),//response
		// 		'P07'=>json_encode($process_payment),//response
		// 		'return_mode'=>'json'	
		// 	];
		// 	$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);
		// 	//save the card processing fee and transaction fee... via api ...
		// 	//if(in_array($_SESSION['merchant_key'],SUNPASS_MERCHANTS_KEYS)){
		// 	//$reference_number=$process_wallet['ResponseMessage']['TransactionId'];


		// 	//dd($update_status);

		// 	//}
		// 	$arr=[
		// 		'msg'=>$process_payment['Message'],
		// 		'success'=>'false'
		// 	];

		// 	echo json_encode($arr);
		// 	exit;
		// }
	}
	public function confirm_email($arr){
		// dd($arr);
		$to = $arr['email'];
		$subj = $arr['title'];
		$attachment_arr='';
		$template_html = $arr['view']; //views/templates/mail/
		//dd($template_html);
		//email receipt..
		$this->assetHelper->send_email($arr,$to,$subj,$template_html,$attachment_arr);
	}

	public function check_blocklist(){
	    $arr=[
	    	'method'=>'login_mobile',
			'P01'=> md5(SERVICE_ACCOUNT_CUSTOMER_PASSWORD_DEV),
			'P02'=> SERVICE_ACCOUNT_CUSTOMER_USERNAME_DEV,
			'return_mode' => 'json'
	    ];
	    //$arr_tosend['url']="login_business?".http_build_query($arr);

	    //dd($arr_tosend);
	    $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr);
	    $api_bt_data=json_decode($api_bt_result,true);
	    //dd($api_bt_data);
	    $data['login_data']='';
	    if($api_bt_data['ResponseCode']=='000'){
	    $data['login_data']=$api_bt_data['ResponseMessage'];
	    }	
	    //check first if amount is on limit.
	    //$_POST['ProtectedCardNumber']=ltrim($_POST['ProtectedCardNumber'], '*');
		//get payment settings. 
       	$arr=[
         	'method'=>'check_block_list',
            'P01'=> $data['login_data']['SessionID'],
            'P02'=> str_replace('*','',$_POST['ProtectedCardNumber']),
            'P03'=> $_POST['CardType'],
			'P04'=> $_POST['NameonCard'],
			'return_mode'=>'json'
            //'P05'=> $_POST['merchant_key'],
            //'P06'=> $_POST['amount'],
       	];
       	$api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr);
        //dd($api_bt_result);
      	$api_bt_data=json_decode($api_bt_result,true);
	    if($api_bt_data['ResponseCode']=='0000'){
			
			$arr=[	
				'msg'=>$api_bt_data['ResponseMessage']['msg'],
				'success'=>false
			];
			
			echo json_encode($arr);	
			exit;	

	    } else {
			$arr=[	
				'msg'=>$api_bt_data['ResponseMessage']['msg'],
				'success'=>true
			];
			
			echo json_encode($arr);	
			exit;			
		}
	}
	//checkout
	public function process_voucher_payment(){
		//recall api staging on saving..
		$arr=[
			'method'=>'get_client_checkout_details',
			'P01'=>$_POST['m'],
			'P02'=>$_POST['r'],
			'return_mode'=>'json'
		];
	    $api_fee_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
	    //dd($api_fee_result);
	    $api_fee_data=json_decode($api_fee_result,true);
	    //dd($api_fee_data);		
	    if($api_fee_data['Success']=='YES'){
	   	//check if pending...
       /*	   	if($api_fee_data['ResponseMessage']['status']!='PENDING'){
	   		redirect('');
	   	}*/
	    //process order as individual array
	    // $ItemName=[];
	    // $ItemQty=[];
	    // $ItemPrice=[];
	    // if(!empty($api_fee_data['ResponseMessage']['details'])){
	    // 	for ($i=0; $i <count($api_fee_data['ResponseMessage']['details']) ; $i++) {
	    // 		# code...
		// 	    $ItemName[$i]=$api_fee_data['ResponseMessage']['details'][$i]['item_name'];
		// 	    $ItemQty[$i]=$api_fee_data['ResponseMessage']['details'][$i]['qty'];
		// 	    $ItemPrice[$i]=$api_fee_data['ResponseMessage']['details'][$i]['price'];
	    // 	}
		// }
		$order_details = '' ; 
		if(!empty($api_fee_data['ResponseMessage']['details'])){
			$len = count($api_fee_data['ResponseMessage']['details']);
			for ($i=0; $i <count($api_fee_data['ResponseMessage']['details']) ; $i++) { 
				# code...
				if($len - 1==$i){
					$order_details.=$api_fee_data['ResponseMessage']['details'][$i]['item_name']."|".$api_fee_data['ResponseMessage']['details'][$i]['qty']."|".$api_fee_data['ResponseMessage']['details'][$i]['price']."";	
				} else {
					$order_details.=$api_fee_data['ResponseMessage']['details'][$i]['item_name']."|".$api_fee_data['ResponseMessage']['details'][$i]['qty']."|".$api_fee_data['ResponseMessage']['details'][$i]['price']."~";	
				}
				
			}
		}
		$total_amount = $api_fee_data['ResponseMessage']['amount']+$api_fee_data['ResponseMessage']['sunpass_fee']+$api_fee_data['ResponseMessage']['sunpass_fee']+$api_fee_data['ResponseMessage']['facility_fee']+$api_fee_data['ResponseMessage']['processing_fee']+$api_fee_data['ResponseMessage']['transaction_fee'];// prior to change kung may fee na need o wala sunpass fee sure mron ok 
		$return_data=[
		'merchant_client_id'=>$api_fee_data['ResponseMessage']['merchant_client_id'],
		'MerchantKey'=>$api_fee_data['ResponseMessage']['merchant_key'],
		'MerchantName'=>$api_fee_data['ResponseMessage']['merchant_name'],
		'Amount'=>$api_fee_data['ResponseMessage']['amount'],
		'total_amount'=>$api_fee_data['ResponseMessage']['total_amount'],
		'OrderID'=>$api_fee_data['ResponseMessage']['order_id'],
		'CallbackURL'=>$api_fee_data['ResponseMessage']['callback_url'],
		// 'ItemName'=>$ItemName,
		// 'ItemQty'=>$ItemQty,
		// 'ItemPrice'=>$ItemPrice,
		'OrderReference'=>$_POST['r'],
		'profile_pic'=>$api_fee_data['ResponseMessage']['profile_pic'],
		'reference_id'=>$_POST['r'],
		'sunpass_fee'=>$api_fee_data['ResponseMessage']['sunpass_fee'],
		'facility_fee'=>$api_fee_data['ResponseMessage']['facility_fee'],
		'processing_fee'=>$api_fee_data['ResponseMessage']['processing_fee'],
		'transaction_fee'=>$api_fee_data['ResponseMessage']['transaction_fee'],
		'discount'=>$api_fee_data['ResponseMessage']['discount'],
		'iscard_fee_on_merch'=>$api_fee_data['ResponseMessage']['iscard_fee_on_merch'],
		'order_details'=>$order_details,
		//'auth',
		];
		}

		$post_data =$return_data;
		//get voucher info here
		$arr_voucher=[
			'method'=>'voucherInquiry',
			'P01'=> $_POST['m'],//MerchantKey,
			'P02'=> $this->input->post('voucher_number'),
			'return_mode' => 'json'
		];
		$api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr_voucher);
		$api_bt_data=json_decode($api_bt_result,true);
		$voucher_amount=0.00;
		if($api_bt_data['Success']=='YES' ){//palitan mo depende sa api n ggwin ung return
			$voucher_amount=$api_bt_data['ResponseMessage']['amount'];
			//dd($voucher_amount);
			if($voucher_amount<$total_amount){
				$arr_post=[
					'method'=>'create_suncashme_checkout_log',
					'P01'=>$_POST['m'],//MerchantKey
					'P02'=>$api_fee_data['ResponseMessage']['order_id'],//order_id
					'P03'=>'',//reference_number
					'P04'=>'voucher',//type
					'P05'=>'failed',//response
					'P06'=>json_encode($arr_voucher),//response
					'P07'=>$api_bt_result,//response
					'P08'=>'Voucher value not enough',//response
					'return_mode'=>'json'
				];
				$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);
		
				$arr=[
					'msg'=>'Voucher value not enough',
					'success'=>false
				];
				
				echo json_encode($arr);
				exit;				
			}
		} else  {
			
			$arr_post=[
				'method'=>'create_suncashme_checkout_log',
				'P01'=>$_POST['m'],//MerchantKey
				'P02'=>$api_fee_data['ResponseMessage']['order_id'],//order_id
				'P03'=>'',//reference_number
				'P04'=>'voucher',//type
				'P05'=>'failed',//response
				'P06'=>json_encode($arr_voucher),//response
				'P07'=>$api_bt_result,//response
				'P08'=>$api_bt_data['ResponseMessage'],//response
				'return_mode'=>'json'
			];
			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);
			$arr=[
				'msg'=>$api_bt_data['ResponseMessage'],
				'success'=>false
			];
			echo json_encode($arr);
			exit;	
		}

		$arr=[
		   'method'=>'suncashCheckOutVoucherPayment',
		   'P01'=> $_POST['m'],//MerchantKey,
		   'P02'=> $post_data['OrderID'],
		   'P03'=> $_POST['voucher_mobile_number'],
		   'P04'=> $post_data['order_details'],
		   'P05'=> $total_amount,
		   'P06'=> $_POST['voucher_number'],
		   'P07'=> $_POST['voucher_pin'],
		   'P08'=> $voucher_amount,// to be fill by get voucher info.... need to check voucher status if valid then get amount
		   'return_mode'=>'json'
		];
	   $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
	   $api_bt_data=json_decode($api_bt_result,true);
	   if($api_bt_data['Success']=='YES'){
		//update payment detail status
		$arr_update=[
			'method'=>'update_checkout_transaction',
			'P01'=>$post_data['OrderReference'],//reference
			'P02'=>'PROCESSED',//status
			'return_mode'=>'json'
		];
		$update_status=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr_update);	
		$arr=[
			'method'=>'get_suntag_shortcode',
			'P01'=>$_POST['m'],//$this->session->userdata('SessionID'),
			'return_mode' => 'json'
		];
		$api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
		$api_bt_data=json_decode($api_bt_result,true);	
		// dd($api_bt_result);
		$data['suntag_data']='';
		if($api_bt_data['Success']=='YES'){
		$data['suntag_data']=$api_bt_data['ResponseMessage'];
		}else{
				$arr=[	
					'msg'=>'Something went wrong!',
					'success'=>false
				];
				echo json_encode($arr);
				exit;
		}  		
		$arr=[
			"customer_name"=>$_POST['voucher_mobile_number'],
			"amount"=>number_format($post_data['Amount'],2,'.',''),
			"transaction_id"=>$post_data['OrderID'],//cenpos
			"dba_name"=>$data['suntag_data']['dba_name'],
			"creation_date"=>date('M d, Y'),
			"reference_num"=>$reference_number,
			"notes"=>'',
			"balance"=>'',
			"processing_fee"=>0.00,
			"transaction_fee"=>0.00,
			"fee"=>0.00,
			"vat"=>0.00,
			"cfee"=>0.00,
			"tfee"=>0.00,
			"total"=>number_format($total_amount,2),  //amount +fee+vat
			"msg_above"=>"<h2>".$_POST['voucher_mobile_number']." paid <span class='amount'>".number_format($total_amount,2)." BSD</span>. </h2>",
			"msg_bottom"=>"",
			"email"=>$data['suntag_data']['business_email_address'],
			"view"=>'email_message_completed.blade.php',
			"title"=>'Completed Payment',
			"profile_pic" => $this->session->userdata("profile_pic"),
			"iscard_fee_on_merch"=>0
		];
		//dd($arr);
		$this->confirm_email($arr);

		// $arr_post=[
		// 	'method'=>'create_suncashme_checkout_log',
		// 	'P01'=>$_POST['m'],//MerchantKey
		// 	'P02'=>$post_data['OrderID'],//order_id
		// 	'P03'=>$api_bt_data['ResponseMessage']['TransactionId'],//reference_number
		// 	'P04'=>'voucher',//type
		// 	'P05'=>'success',//response
		// 	'P06'=>$arr,//response
		// 	'P07'=>$api_bt_result,//response
		// 	'P08'=>$api_bt_data['ResponseMessage'],//response
		// 	'return_mode'=>'json'
		// ];
		// $save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);
		// 	dd($save_logs);

		//final process
		unset($_SESSION['post_data']);
		$reference_num = $api_bt_data['ResponseMessage']['TransactionId']."||".$_POST['r'];
		$params=base64_encode($reference_num."||success||voucher");
		$arr=[
			'msg'=>'Successfully Purchased',
			'reference'=>$api_bt_data['ResponseMessage']['TransactionId'],
			'url'=>$post_data["CallbackURL"].$params,
			'payment_method'=>'Voucher',
			'success'=>true
		];

		echo json_encode($arr);

	   } else {

			$arr_post=[
				'method'=>'create_suncashme_checkout_log',
				'P01'=>$_POST['m'],//MerchantKey
				'P02'=>$post_data['OrderID'],//order_id
				'P03'=>'',//reference_number
				'P04'=>'voucher',//type
				'P05'=>'failed',//response
				'P06'=>json_encode($arr),//response
				'P07'=>$api_bt_result,//response
				'P08'=>$api_bt_data['ResponseMessage'],//response
				'return_mode'=>'json'
			];
			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);
		
		
		   $arr=[
			   'msg'=>$api_bt_data['ResponseMessage'],
			   'success'=>false
		   ];
		   echo json_encode($arr);
		   exit;
	   }
	}
	//suncashme voucher
	public function process_suncashme_voucher_payment(){

		$customer_name =$_POST['name'] == '' ? 'Anonymous Donor' : $_POST['name'];
		if(empty($_POST['amount']) || empty($_POST['reference_num']) || empty($_POST['voucher_pin']) || empty($_POST['voucher_number'])){
			$arr=[	
				'msg'=>'Please fill up all fields',
				'success'=>false
			];
			
			echo json_encode($arr);

		}
		$total_amount=$_POST['total_amount'];
		$amount=$_POST['amount'];
		$user_type = $_SESSION['tag']=='CUSTOMER' ? 'a15d411bdbdfffa46c50cb57958bd68fef35d5eacbfcaba0dc21a8caf03ac995' : $_SESSION['key'];
		$user_type_logs = $_SESSION['tag']=='CUSTOMER' ? $_SESSION['suntag_shortcode'] : $_SESSION['key'];
		
		//get voucher info here
		$arr_voucher=[
			'method'=>'voucherInquiry',
			'P01'=> $user_type,//MerchantKey,
			'P02'=> $this->input->post('voucher_number'),
			'return_mode' => 'json'
		];
		// dd($arr_voucher);
		$api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr_voucher);
		// dd($api_bt_result);
		$api_bt_data=json_decode($api_bt_result,true);
		$voucher_amount=0.00;
		if($api_bt_data['Success']=='YES' ){
			$voucher_amount=$api_bt_data['ResponseMessage']['amount'];
			if($voucher_amount<$total_amount){
				$arr_post=[
					'method'=>'create_suncashme_checkout_log',
					'P01'=>$user_type_logs,//MerchantKey
					'P02'=>$_POST['reference_num'],//order_id
					'P03'=>'',//reference_number
					'P04'=>'voucher',//type
					'P05'=>'failed',
					'P06'=>json_encode($arr_voucher),
					'P07'=>$api_bt_result,
					'P08'=>'Voucher value not enough',
					'return_mode'=>'json'
				];
						// dd($arr_post);
				$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);	
				$arr=[
					'msg'=>'Voucher value not enough',
					'success'=>false
				];
				echo json_encode($arr);
				exit;				
			}

			if($_SESSION['tag']=='MERCHANT'){	
				$type="BUSINESS_PAYMENT";

			}else if($_SESSION['tag']=='CUSTOMER'){
				//if tag is Customer
				$type="CUSTOMER_PAYMENT";
			}else{
				$arr=[
					'msg'=>'Tag Type not Valid',
					'success'=>false
				];
				echo json_encode($arr);
				exit;	

			}

			$arr_voucher=[
				'method'=>'suncashmeVoucherPayment',
				'P01'=> $this->session->userdata('client_record_id'),//merch id/custimerid
				'P02'=> $_POST['reference_num'],
				'P03'=> $this->clean($_POST['mobile']),//customer mobile
				'P04'=> $_POST['name'],//customer name
				'P05'=> $_POST['email'],//customer email
				'P06'=> $amount,//amount
				'P07'=> $total_amount,//total
				'P08'=> $_POST['voucher_number'],//voucher code
				'P09'=> $_POST['voucher_pin'],//pin
				'P10'=> $voucher_amount,//voucheraomunt
				'P11'=> $type,//type
				'P12'=> $_POST['notes'],//notes
				'P13'=> $_POST['hid_fee'],//fees
				'return_mode'=>'json'
			];

			// dd($arr_voucher);
			$api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr_voucher);
			// dd($api_bt_result);
			$api_bt_data=json_decode($api_bt_result,true);
			if($api_bt_data['Success']=='YES'){

				$arr_post=[
					'method'=>'create_suncashme_checkout_log',
					'P01'=>$user_type_logs,//MerchantKey
					'P02'=>$_POST['reference_num'],//order_id
					'P03'=>$api_bt_data['ResponseMessage']['ResponseData']['TransactionID'],//reference_number
					'P04'=>'voucher',//type
					'P05'=>'success',//response
					'P06'=>json_encode($arr_voucher),//response
					'P07'=>$api_bt_data,//response
					'return_mode'=>'json'
				];
				
			// dd($arr_post);
				$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);

				unset($_SESSION['post_data']);
				$arr=[
					'msg'=>$api_bt_data['ResponseMessage'],
					'reference'=>$api_bt_data['ResponseMessage']['ResponseData']['TransactionID'],//reference_number
					'success'=>true
				];
				echo json_encode($arr);

			} else {
				$arr_post=[
					'method'=>'create_suncashme_checkout_log',
					'P01'=>$user_type_logs,//MerchantKey
					'P02'=>$_POST['reference_num'],//order_id
					'P03'=>'',//reference_number
					'P04'=>'voucher',//type
					'P05'=>'failed',//response
					'P06'=>json_encode($arr_voucher),//response
					'P07'=>$api_bt_data,//response
					'return_mode'=>'json'
				];
				$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);
				
				
				$arr=[
					'msg'=>$api_bt_data['ResponseMessage'],
					'success'=>false
				];
				echo json_encode($arr);
				exit;
			}

		} else  {
			$arr=[
				'msg'=>$api_bt_data['ResponseMessage'],
				'success'=>false
			];
			echo json_encode($arr);
			exit;	
		}	

	}	
	//for cc page after login in checkout
	public function card_payment(){//method then routefind //get fee
		// if($_SESSION['tag']=='CUSTOMER'){
		// 	if($_SESSION['kyc_type']!='full'){
		// 		redirect('');
		// 	}

		// }

		//site verify
		$arr=[
			'merchant'=>CENPOST_MERCHANT_ID,
			'secretkey'=>CENPOST_MERCHANT_SECRETKEY,
			'email'=>'suncashme@gmail.com',
			'ip'=>'127.0.0.1',
		];
		$verify_post=$this->assetHelper->api_requestv2(CENPOS_SITEVERIFY_URL,$arr);
		$verify_post=json_decode($verify_post,true);	
		//dd($verify_post);p
		$data['verify_params']=$verify_post;
		$this->blade->view('card-payment',$data);

	}
	public function show_customerlogin(){
		$this->blade->view('showlogin');
	}
	public function lcc(){//login customer card
		$arr=[
			'method'=>'login_mobile',
			'P01'=> md5($_POST['p']),
			'P02'=> $this->clean($_POST['u']),
			'return_mode' => 'json'
		];
		// $arr_tosend['url']="login_mobile?".http_build_query($arr);
		// dd($arr);
		$api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr);
		$api_bt_data=json_decode($api_bt_result,true);
		//dd($api_bt_result);
		$data['login_data']='';
		$data_s='';
		$data_c='';
		if($api_bt_data['ResponseCode']=='0000'){
		$data['login_data']=$api_bt_data['ResponseMessage'];
			$data_s = $data['login_data']['SessionID'];
			$data_c = $data['login_data']['CustomerId'];
			$data_name = $data['login_data']['CustomerName'];
			$data_email = $data['login_data']['Email'];
		//session customer info
		$this->session->set_userdata('c_SessionID',$data_s);
		$this->session->set_userdata('c_CustomerId',$data_c);
		$this->session->set_userdata('c_mobile',$_POST['u']);
		$this->session->set_userdata('c_CustomerName',$data_name);
		$this->session->set_userdata('c_Email',$data_email);
		// get card info
			$arr=[
				'method'=>'get_customer_creditcard',
				'P01'=> $this->session->userdata('c_SessionID'),
				'P02'=> $this->session->userdata('c_CustomerId'),
				'P03'=> '',
				'return_mode' => 'json'
			];
			// dd($arr);
			$api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr);
			$api_bt_data=json_decode($api_bt_result,true);
			// dd($api_bt_data);
			$ccdata='';
			if($api_bt_data['ResponseCode']=='0000'){
				$ccdata=$api_bt_data['ResponseMessage'];
			}
			//prefer cards/
			$html='';
			if(!empty($ccdata)){
				foreach($ccdata as $ccdata_val){
					if($ccdata_val['is_verified']==1){
						$html.="<option value='{$ccdata_val['id']}'>".str_pad($ccdata_val['card_last_four_digits'], 12, "*", STR_PAD_LEFT)." (".$ccdata_val['card_type'].")</option>";
					}
				}
			}
		  	$arr=[
				'msg'=>$api_bt_data['ResponseMessage'],
				'data'=>$html,
				'success'=>true
			];
			echo json_encode($arr);
			exit;
		} else {
			$arr=[
				'msg'=>$api_bt_data['ResponseMessage'],
				'success'=>false
			];
			echo json_encode($arr);
			exit;
		}
	}
	public function process_savecard_payment(){
		if(empty($_POST['ccwu'])){

			$arr=[
				'msg'=>'Please fill up all fields',
				'success'=>false
			];
			echo json_encode($arr);
		}
		//recall api staging on saving..
		$arr=[
			'method'=>'get_client_checkout_details',
			'P01'=>$_POST['m'],
			'P02'=>$_POST['r'],
			'return_mode'=>'json'
		];
	    $api_fee_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
	    //dd($api_fee_result);
	    $api_fee_data=json_decode($api_fee_result,true);
	    //dd($api_fee_result);		
	    if($api_fee_data['Success']=='YES'){
	   	//check if pending...
 		if($api_fee_data['ResponseMessage']['status']!='PENDING'){
 	   		redirect('');
 	   	}
	    //process order as individual array
	    $ItemName=[];
	    $ItemQty=[];
	    $ItemPrice=[];
	    if(!empty($api_fee_data['ResponseMessage']['details'])){
	    	for ($i=0; $i <count($api_fee_data['ResponseMessage']['details']) ; $i++) {
	    		# code...
			    $ItemName[$i]=$api_fee_data['ResponseMessage']['details'][$i]['item_name'];
			    $ItemQty[$i]=$api_fee_data['ResponseMessage']['details'][$i]['qty'];
			    $ItemPrice[$i]=$api_fee_data['ResponseMessage']['details'][$i]['price'];
	    	}
	    }
		$return_data=[
		'merchant_client_id'=>$api_fee_data['ResponseMessage']['merchant_client_id'],
		'MerchantKey'=>$api_fee_data['ResponseMessage']['merchant_key'],
		'MerchantName'=>$api_fee_data['ResponseMessage']['merchant_name'],
		'Amount'=>$api_fee_data['ResponseMessage']['amount'],
		'total_amount'=>$api_fee_data['ResponseMessage']['total_amount'],
		'OrderID'=>$api_fee_data['ResponseMessage']['order_id'],
		'CallbackURL'=>$api_fee_data['ResponseMessage']['callback_url'],
		'ItemName'=>$ItemName,
		'ItemQty'=>$ItemQty,
		'ItemPrice'=>$ItemPrice,
		'OrderReference'=>$_POST['r'],
		'profile_pic'=>$api_fee_data['ResponseMessage']['profile_pic'],
		'reference_id'=>$_POST['r'],
		'sunpass_fee'=>$api_fee_data['ResponseMessage']['sunpass_fee'],
		'facility_fee'=>$api_fee_data['ResponseMessage']['facility_fee'],
		'processing_fee'=>$api_fee_data['ResponseMessage']['processing_fee'],
		'transaction_fee'=>$api_fee_data['ResponseMessage']['transaction_fee'],
		'discount'=>$api_fee_data['ResponseMessage']['discount'],
		'iscard_fee_on_merch'=>$api_fee_data['ResponseMessage']['iscard_fee_on_merch'],
		//'auth',
		];
		}

		$post_data =$return_data;
		
		//backdoor conveniene fee
		$arr=[
	    	'method'=>'login_business',
	        'P01'=> SERVICE_ACCOUNT_USERNAME_DEV,
	        'P02'=> SERVICE_ACCOUNT_PASSWORD_DEV,
	    ];
	    //$arr_tosend['url']="login_business?".http_build_query($arr);

	    //dd($arr_tosend);
	    $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
	    $api_bt_data=json_decode($api_bt_result,true);
	    //dd($api_bt_data);
	    $data['login_data']='';
	    if($api_bt_data['ResponseCode']=='YES'){
	    $data['login_data']=$api_bt_data['ResponseMessage'];
	    }

       //get transaction fees. 
       // $client_record_id = $_SESSION['client_record_id'];
       $arr=[
         'method'=>'get_convenience_fee',
            'P01'=> $data['login_data']['SessionID'],
            'P02'=> $api_fee_data['ResponseMessage']['merchant_client_id'],
       ];

       // $arr_tosend['url']="login_business?".http_build_query($arr);

       //dd($arr);
       //print_r($arr);die;
       $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
       $api_bt_data=json_decode($api_bt_result,true);
       //dd($api_bt_result);
       //var_dump($api_bt_result);die;
       $data['transaction_fee_data']='';
       if($api_bt_data['Success']=='YES'){
       $api_bt_data['ResponseMessage']=isset($api_bt_data['ResponseMessage']) ? $api_bt_data['ResponseMessage'] : '';
       $transaction_fee_data=$api_bt_data['ResponseMessage'];
       } 
       $amount =str_replace(',', '', $api_fee_data['ResponseMessage']['amount']); //? $_POST['amount']  : $this->session->userdata('post_data')['Amount'];
       //dd($amount);
       $total= 0.00;
       $pf=0.00;
       $tf=0.00;
       $upf=0.00;
       $utf=0.00;
       $isenable='';
	     if(!empty($transaction_fee_data)){
	       	   $isenable=$transaction_fee_data['is_enable_cc_fee'];
		       if($transaction_fee_data['is_enable_cc_fee']==1){
		       		$gtot =number_format($amount,2,'.','')+$api_fee_data['ResponseMessage']['sunpass_fee']+$api_fee_data['ResponseMessage']['facility_fee'];

			       if($transaction_fee_data['cc_processing_fee_isfixed']==1){//fixed
			       		//$fee+= $transaction_fee_data['cc_processing_fee'];
			       		$pf=$transaction_fee_data['cc_processing_fee'];
			       		$upf=$transaction_fee_data['cc_processing_fee'];
			       		//$total += floatval($amount)+floatval($fee);
			       } else if($transaction_fee_data['cc_processing_fee_isfixed']==0){
			       		$percentage= $transaction_fee_data['cc_processing_fee']/100;
			       		$total_percent=number_format($gtot,2,'.','')*number_format($percentage,2,'.','');
			       		//echo $amount;
			       		//echo $fee;
			       		$pf=$total_percent;
			       		$upf=$total_percent;
			       }
			       //apply fee...
			       if($transaction_fee_data['transaction_fee_isfixed']==1){//fixed
			       		//$fee+= $transaction_fee_data['transaction_fee'];
			       		$tf=$transaction_fee_data['transaction_fee'];
			       		$utf=$transaction_fee_data['transaction_fee'];
			       		//$total += floatval($amount)+floatval($fee);
			       } else if($transaction_fee_data['transaction_fee_isfixed']==0){//percent
			       		$percentage=$transaction_fee_data['transaction_fee']/100;
			       		$total_percent=number_format($gtot,2,'.','')*number_format($percentage,2,'.','');
			       		//echo $fee."<br>";
			       		$tf=$total_percent;
			       		$utf=$total_percent;
			       }
		       } else {
		       		//
		       		$isenable=$transaction_fee_data['is_enable_cc_fee'];
		       		$gtot =$amount+$api_fee_data['ResponseMessage']['sunpass_fee']+$api_fee_data['ResponseMessage']['facility_fee'];

			       if($transaction_fee_data['cc_processing_fee_isfixed']==1){//fixed
			       		//$fee+= $transaction_fee_data['cc_processing_fee'];
			       		$upf=$transaction_fee_data['cc_processing_fee'];
			       		//$total += floatval($amount)+floatval($fee);
			       } else if($transaction_fee_data['cc_processing_fee_isfixed']==0){
			       		$percentage= $transaction_fee_data['cc_processing_fee']/100;
			       		$total_percent=number_format($gtot,2,'.','')*number_format($percentage,2,'.','');
			       		//echo $amount;
			       		//echo $fee;
			       		$upf=$total_percent;
			       }
			       //apply fee...
			       if($transaction_fee_data['transaction_fee_isfixed']==1){//fixed
			       		//$fee+= $transaction_fee_data['transaction_fee'];
			       		$utf=$transaction_fee_data['transaction_fee'];
			       		//$total += floatval($amount)+floatval($fee);
			       } else if($transaction_fee_data['transaction_fee_isfixed']==0){//percent
			       		$percentage=$transaction_fee_data['transaction_fee']/100;
			       		$total_percent=number_format($gtot,2,'.','')*number_format($percentage,2,'.','');
			       		//echo $fee."<br>";
			       		$utf=$total_percent;
			       }
		       }
	       }

		//dd($post_data);
		//detect if ios af.
		$this->load->library('Mobile_Detect');
		$pre_total = $post_data['Amount']-$post_data['discount'];
		if($pre_total<=0){
			$pre_total=0.00;
		}
		if($post_data['iscard_fee_on_merch']==1){ //1 means disabled and convenience fee should be charged to  merchant/customer merchant account
			$grand_total =$pre_total+$post_data['sunpass_fee']+$post_data['facility_fee'];
		} else {
			$grand_total =$pre_total+$post_data['sunpass_fee']+$post_data['facility_fee']+$post_data['processing_fee']+$post_data['transaction_fee'];
		}
		
		$grand_total = number_format($grand_total, 2, '.', '');
		//dd($this->mobile_detect);
		$arr=[];
		//get card info
		$arr_get=[
			'method'=>'get_customercard_details',
			'P01'=>$this->session->userdata('c_SessionID'),
			'P02'=>$_POST['ccwu'],
			'return_mode'=>'json'
		];
		//dd($arr_get);
		$get_linkedcard_info=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_get);
		//dd($get_linkedcard_info);
		$get_linkedcard_info_data=json_decode($get_linkedcard_info,true);  
		// dd($get_linkedcard_info_data);
		//process verify post.
		if($grand_total <= 0){

			$arr=[	
				'msg'=>'Amount should be greather than 0',
				'success'=>false
			];
			
			echo json_encode($arr);
			exit;

		} 

		$arr=[
		'amount'=>$grand_total,
		'type'=>'Sale',
		'invoicenumber'=>$post_data['OrderID'],
		'secretkey'=>CENPOST_MERCHANT_SECRETKEY,
		'merchant'=>CENPOST_MERCHANT_ID,
		'customercode'=>$this->session->userdata('c_CustomerId'),
		'tokenid'=>$get_linkedcard_info_data['ResponseMessage']['token_id'],
		];
		$verifying_post='';
		$verify_post=$this->assetHelper->api_requestv2(CENPOS_SITEVERIFY_URL,$arr);
		//dd($verify_post2);
		$verify_post=json_decode($verify_post,true);
		//result verify
		if($verify_post['Result']==0){
			$arr_post=[
				'method'=>'create_suncashme_checkout_log',
				'P01'=>$post_data['MerchantKey'],//MerchantKey
				'P02'=>$post_data['OrderID'],//order_id
				'P03'=>'',//reference_number
				'P04'=>'cenpos',//type
				'P05'=>'success',//response
				'P06'=>json_encode($arr),//response
				'P07'=>json_encode($verify_post),//response
				'return_mode'=>'json'	
			];
			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);
			$verifying_post=$verify_post['Data'];
		} else{///
			$arr_post=[
				'method'=>'create_suncashme_checkout_log',
				'P01'=>$post_data['MerchantKey'],//MerchantKey
				'P02'=>$post_data['OrderID'],//order_id
				'P03'=>'',//reference_number
				'P04'=>'cenpos',//type
				'P05'=>'failed',//response
				'P06'=>json_encode($arr),//response
				'P07'=>json_encode($verify_post),//response
				'return_mode'=>'json'
			];
			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);

			$arr=[
				'msg'=>$verify_post['Message'],
				'data'=>$verify_post,
				'success'=>false
			];

			echo json_encode($arr);
		}
		  $arr_post2=[
			'verifyingpost'=>$verify_post['Data'],
			'tokenid'=>$get_linkedcard_info_data['ResponseMessage']['token_id'],
		  ];
		  //dd($arr_post2);
		  $process_payment=$this->assetHelper->api_requestv2(CENPOS_PROCESS_URL,$arr_post2);
		  $process_payment=json_decode($process_payment,true);
		  //log cenpos
		  $arr_post=[
			'method'=>'log_cenpos',
			'P01'=> $this->session->userdata('c_SessionID'),
			'P02'=> $process_payment,//data//request or response data
			'P03'=> 'RESPONSE',//status // "REQUEST" or "RESPONSE"
			'P04'=> CENPOS_PROCESS_URL,//cenpos_method
			'P05'=> $this->session->userdata('c_CustomerId'),
			'return_mode' => 'json'
		  ];
		  $save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);

		if($process_payment['Result']==0){//

			//update status ..
			$arr_update=[
				'method'=>'update_checkout_transaction',
				'P01'=>$post_data['OrderReference'],//reference
				'P02'=>'PROCESSED',//status
				'return_mode'=>'json'
			];
			$update_status=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr_update);
			//save transaction details....
			$c_mobile_number = $this->clean($this->security->xss_clean($this->session->userdata('c_mobile')));//session mobile from login
			$c_mobile_form = $this->clean($this->security->xss_clean($this->input->post('mobile_card')));//form field card
			$arr_post=[
				'method'=>'create_suncashme_cenpos_transaction_detail',
				'P01'=>$post_data['MerchantKey'],//MerchantKey
				'P02'=>$post_data['OrderID'],//order_id
				'P03'=>$process_payment['ReferenceNumber'],//reference_number
				'P04'=>str_pad($get_linkedcard_info_data['ResponseMessage']['card_last_four_digits'], 12, "*", STR_PAD_LEFT),//type
				'P05'=>$get_linkedcard_info_data['ResponseMessage']['cardholder_name'],//response
				'P06'=>$get_linkedcard_info_data['ResponseMessage']['card_type'],//response
				'P07'=>number_format($post_data['Amount'],2,'.',''),//response
				'P08'=>number_format($upf,2,'.',''),//response,//response
				'P09'=>number_format($utf,2,'.',''),//response,//response
				'P10'=>number_format($post_data['transaction_fee']+$post_data['processing_fee']+$post_data['sunpass_fee']+$post_data['facility_fee'],2,'.',''),//totall of all fee master fee tne fee
				'P11'=>$post_data['sunpass_fee'],//forsuncash
				'P12'=>$post_data['facility_fee'],//for ticketing only
				'P13'=>0.00,//billpay fee
				'P14'=>0.00,//vat
				'P15'=>'checkout',//source checkout
				'P16'=>number_format($post_data['discount'], 2, '.', ''),//source checkout
				'P17'=>$post_data['iscard_fee_on_merch'],
				'P18'=>'',//notes
				'P19'=>$this->session->userdata('C_CustomerName'),
				'P20'=>$this->session->userdata('c_Email'),
				'P21'=>$c_mobile_form,
				'return_mode'=>'json'
			];

			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);
			//dd($save_logs);

			//saving logs....
			$arr_post=[
				'method'=>'create_suncashme_checkout_log',
				'P01'=>$post_data['MerchantKey'],//MerchantKey
				'P02'=>$post_data['OrderID'],//order_id
				'P03'=>$process_payment['ReferenceNumber'],//reference_number
				'P04'=>'cenpos',//type
				'P05'=>'success',//response
				'P06'=>json_encode($arr_post),//response
				'P07'=>json_encode($process_payment),//response
				'P08'=>$process_payment['Message'],//response
				'return_mode'=>'json'
			];
			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);
            $cfee_email = $post_data['iscard_fee_on_merch'] ==1 ? $cfee_email=0.00 : $cfee_email=number_format($upf,2,'.','')+number_format($utf,2,'.','');
			//email merchant process payment
			$arr=[
				"customer_name"=>$get_linkedcard_info_data['ResponseMessage']['cardholder_name'],
				"amount"=>number_format($post_data['Amount'],2,'.',''),
				"transaction_id"=>$post_data['OrderID'],//cenpos
				"dba_name"=>$data['suntag_data']['dba_name'],
				"creation_date"=>date('M d, Y'),
				"reference_num"=>$process_payment['ReferenceNumber'],
				"notes"=>'',
				"balance"=>'',
				"processing_fee"=>number_format($upf,2,'.',''),
				"transaction_fee"=>number_format($utf,2,'.',''),
				"fee"=>0.00,
				"vat"=>0.00,
				"cfee"=>$cfee_email,
				"tfee"=>0.00,
				"total"=>number_format($grand_total,2),  //amount +fee+vat
				"msg_above"=>"<h2>".$get_linkedcard_info_data['ResponseMessage']['cardholder_name']." paid <span class='amount'>".number_format($grand_total,2)." BSD</span>. </h2>",
				"msg_bottom"=>"",
				"email"=>$this->session->userdata('email'),
				"view"=>'email_message_completed.blade.php',
				"title"=>'Completed Payment',
				"profile_pic" => $this->session->userdata("profile_pic"),
				"iscard_fee_on_merch"=>$post_data['iscard_fee_on_merch']
			];
			//dd($arr);
			$this->confirm_email($arr);

			unset($_SESSION['post_data']);
			$reference_num = $process_payment['ReferenceNumber']."||".$_POST['r'];
	

			$params=base64_encode($reference_num."||success||card");
			$arr=[
				'msg'=>$process_payment['Message'],
				'reference'=>$process_payment['ReferenceNumber'],
				'url'=>$post_data["CallbackURL"].$params,
				'payment_method'=>'Card',
				'success'=>true
			];

			echo json_encode($arr);
		} else {
			//saving logs....
			$arr_post=[
				'method'=>'create_suncashme_checkout_log',
				'P01'=>$post_data['MerchantKey'],//MerchantKey
				'P02'=>$post_data['OrderID'],//order_id
				'P03'=>'',//reference_number
				'P04'=>'cenpos',//type
				'P05'=>'failed',//response
				'P06'=>json_encode($arr_post),//response
				'P07'=>json_encode($process_payment),//response
				'return_mode'=>'json'
			];
			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);

			//unset($_SESSION['post_data']);

			$arr=[
				'msg'=>$process_payment['Message'],
				'success'=>false
			];
			echo json_encode($arr);
		}

	}
	public function process_sanddollar_checkout(){
		if(empty($_POST['mobile']) || empty($_POST['passcode'])){

			$arr=[	
				'msg'=>'Please fill up all fields',
				'success'=>false
			];
			
			echo json_encode($arr);

		} 

		$arr=[
			'method'=>'get_client_checkout_details',
			'P01'=>$_POST['m'],
			'P02'=>$_POST['r'],
			'return_mode'=>'json'
		];
	    $api_fee_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
	    $api_fee_data=json_decode($api_fee_result,true);	
	    if($api_fee_data['Success']=='YES'){

			$ItemName=[];
			$ItemQty=[];
			$ItemPrice=[];
			if(!empty($api_fee_data['ResponseMessage']['details'])){
				for ($i=0; $i <count($api_fee_data['ResponseMessage']['details']) ; $i++) { 
					# code...
					$ItemName[$i]=$api_fee_data['ResponseMessage']['details'][$i]['item_name'];
					$ItemQty[$i]=$api_fee_data['ResponseMessage']['details'][$i]['qty'];
					$ItemPrice[$i]=$api_fee_data['ResponseMessage']['details'][$i]['price'];		    		
				}
			}
			$return_data=[
				'merchant_client_id'=>$api_fee_data['ResponseMessage']['merchant_client_id'],
				'MerchantKey'=>$api_fee_data['ResponseMessage']['merchant_key'],
				'MerchantName'=>$api_fee_data['ResponseMessage']['merchant_name'],
				'Amount'=>$api_fee_data['ResponseMessage']['amount'],
				'total_amount'=>$api_fee_data['ResponseMessage']['total_amount'],
				'OrderID'=>$api_fee_data['ResponseMessage']['order_id'],
				'CallbackURL'=>$api_fee_data['ResponseMessage']['callback_url'],
				'ItemName'=>$ItemName,
				'ItemQty'=>$ItemQty,
				'ItemPrice'=>$ItemPrice,
				'OrderReference'=>$_POST['r'],
				'profile_pic'=>$api_fee_data['ResponseMessage']['profile_pic'],
				'reference_id'=>$_POST['r'],
				'sunpass_fee'=>$api_fee_data['ResponseMessage']['sunpass_fee'],
				'facility_fee'=>$api_fee_data['ResponseMessage']['facility_fee'],
				'processing_fee'=>$api_fee_data['ResponseMessage']['processing_fee'],
				'transaction_fee'=>$api_fee_data['ResponseMessage']['transaction_fee'],
				'discount'=>$api_fee_data['ResponseMessage']['discount'],
			//'auth',
			];
		}
		$post_data =$return_data;

		//process items...
		$order_details='';
		if(!empty($post_data['ItemName'])){
			$len = count($post_data['ItemName']);
			for ($i=0; $i <count($post_data['ItemName']) ; $i++) { 
				# code...
				if($len - 1==$i){
					$order_details.=$post_data['ItemName'][$i]."|".$post_data['ItemQty'][$i]."|".$post_data['ItemPrice'][$i]."";
				} else {
					$order_details.=$post_data['ItemName'][$i]."|".$post_data['ItemQty'][$i]."|".$post_data['ItemPrice'][$i]."~";
				}
			}
		}

		$sanddollar=[
			'method'=>'processPaymentViaSandDollar',	
			'P01' =>$api_fee_data['ResponseMessage']['merchant_client_id'], //client_id
			'P02' =>$_POST['fullname'], //customer_name
			'P03' =>$this->clean($_POST['mobile']), //customer_mobile 
			'P04' =>$post_data['total_amount'], //amount 
			'P05' =>'CHECKOUT',//source 
			'P06' =>'BUSINESS_PAYMENT', //transaction_type
			'P07' =>'',//notes //no notes in checkout
			'P08'=>$_POST['sand_qr'],//sanddollar_qr'
			'P09'=>$_POST['passcode'],//mobile
			'P10'=>$_POST['otp'],//sand cardotp
			'return_mode'=>'json'
		];
		// dd($sanddollar);
		$process_sand=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$sanddollar);
		$process_sanddollar=json_decode($process_sand,true);
		// dd($process_sanddollar);
		if($process_sanddollar['Success']=="YES"){
			$reference_number=$process_sanddollar['ResponseMessage']['data']['0']['paymentRequestId'];

			$arr_update=[
				'method'=>'update_checkout_transaction',
				'P01'=>$post_data['OrderReference'],//reference 
				'P02'=>'PROCESSED',//status 
				'return_mode'=>'json'	
			];
			$update_status=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr_update);

			//for logs need to convert to md5 (disallowd chars)
			$sanddollar=[
				'method'=>'processPaymentViaSandDollar',	
				'P01' =>$api_fee_data['ResponseMessage']['merchant_client_id'], //client_id
				'P02' =>$_POST['fullname'], //customer_name
				'P03' =>$this->clean($_POST['mobile']), //customer_mobile 
				'P04' =>$post_data['total_amount'], //amount 
				'P05' =>'CHECKOUT',//source 
				'P06' =>'BUSINESS_PAYMENT', //transaction_type
				'P07' =>'',//notes //no notes in checkout
				'P08'=>md5($_POST['sand_qr']),//sanddollar_qr'
				'P09'=>$_POST['passcode'],//mobile
				'P10'=>$_POST['otp'],//sand cardotp
				'return_mode'=>'json'
			];

			$arr_post=[
				'method'=>'create_suncashme_checkout_log',
				'P01'=>$post_data['MerchantKey'],//MerchantKey
				'P02'=>$post_data['OrderID'],//order_id
				'P03'=>$reference_number,//reference_number
				'P04'=>'sanddollar',//type
				'P05'=>'success',//response
				'P06'=>$sanddollar,//request
				'P07'=>$process_sand,//$process_sand,//response
				'P08'=>'Payment Successful',//response
				'return_mode'=>'json'	
			];
			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);
			//dd($save_logs);

			$arr=[
				'method'=>'get_suntag_shortcode',
				'P01'=>$_POST['m'],
				'return_mode' => 'json'
			];
			$api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
			$api_bt_data=json_decode($api_bt_result,true);	
			// dd($api_bt_result);
			$data['suntag_data']='';
			if($api_bt_data['Success']=='YES'){
			$data['suntag_data']=$api_bt_data['ResponseMessage'];
			}else{
					$arr=[	
						'msg'=>'Something went wrong!',
						'success'=>false
					];
					echo json_encode($arr);
					exit;
			} 		
			// email merchant process payment
			$arr=[
				"customer_name"=>$_POST['mobile'],
				"amount"=>number_format($post_data['total_amount'],2,'.',''),
				"transaction_id"=>$post_data['OrderID'],//cenpos
				"dba_name"=>$api_fee_data['ResponseMessage']['merchant_name'],
				"creation_date"=>date('M d, Y'),
				"reference_num"=>$reference_number,//referece sand
				"notes"=>'',
				"balance"=>'',
				"processing_fee"=>0.00,
				"transaction_fee"=>0.00,
				"fee"=>0.00,
				"vat"=>0.00,
				"cfee"=>0.00,
				"tfee"=>0.00,
				"total"=>number_format($post_data['total_amount'],2,'.',''),//amount +fee+vat
				"msg_above"=>"<h2>".$_POST['mobile']." paid <span class='amount'>".number_format($post_data['total_amount'],2,'.','')." BSD</span>. </h2>",
				"msg_bottom"=>"",
				"email"=>$data['suntag_data']['business_email_address'],
				"view"=>'email_message_completed.blade.php',
				"title"=>'Completed Payment',
				"profile_pic" => $this->session->userdata("profile_pic"),
				"iscard_fee_on_merch"=>'',
			];
			//dd($arr);
			$this->confirm_email($arr);			


			unset($_SESSION['post_data']);
			$params=base64_encode($reference_number."||".$_POST['r']."||success||sanddollar");

			// dd($post_data["CallbackURL"].$params);
			$arr=[	
				'msg'=>'Successfully paid',
				'url'=>$post_data["CallbackURL"].$params,
				'success'=>true
			];
			
			echo json_encode($arr);	
			exit;
				
		} else {

			//for logs need to convert to md5 (disallowd chars)
			$sanddollar=[
				'method'=>'processPaymentViaSandDollar',	
				'P01' =>$api_fee_data['ResponseMessage']['merchant_client_id'], //client_id
				'P02' =>$_POST['fullname'], //customer_name
				'P03' =>$this->clean($_POST['mobile']), //customer_mobile 
				'P04' =>$post_data['total_amount'], //amount 
				'P05' =>'CHECKOUT',//source 
				'P06' =>'BUSINESS_PAYMENT', //transaction_type
				'P07' =>'',//notes //no notes in checkout
				'P08'=>md5($_POST['sand_qr']),//sanddollar_qr'
				'P09'=>$_POST['passcode'],//mobile
				'P10'=>$_POST['otp'],//sand cardotp
				'return_mode'=>'json'
			];

			$arr_post=[
				'method'=>'create_suncashme_checkout_log',
				'P01'=>$post_data['MerchantKey'],//MerchantKey
				'P02'=>$post_data['OrderID'],//order_id
				'P03'=>'',//reference_number
				'P04'=>'sanddollar',//type
				'P05'=>'failed',//response
				'P06'=>$sanddollar,//response
				'P07'=>$process_sand,//response
				'P08'=>'Payment Failed',//response
				'return_mode'=>'json'	
			];
			
			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);
			// dd($save_logs);
			$arr=[	
				'msg'=>$process_sanddollar['ResponseMessage'],
				'success'=>false
			];
			
			echo json_encode($arr);		
			exit;
		}		
	}	
	public function process_sanddollar_payment(){
		// dd($_POST);

		$customer_name =$_POST['name'] == '' ? 'Anonymous Donor' : $_POST['name'];
		if(empty($_POST['mobile']) || empty($_POST['passcode']) || empty($_POST['pin'])){

			$arr=[	
				'msg'=>'Please fill up all fields',
				'success'=>false
			];
			
			echo json_encode($arr);

		}

		// $tamount = 	str_replace( ',', '', $_POST['total_due']);
		$amount = 	str_replace( ',', '', $_POST['amount']);
		if($_SESSION['tag']=='CUSTOMER'){
			$lbl="CUSTOMER_PAYMENT";
		}else{
			$lbl="BUSINESS_PAYMENT";
		}
		if($_POST['type'] =='custom_name'){
			$sanddollar=[
				'method'=>'processPaymentViaSandDollarAccountHolders',	
				'P01' =>$_SESSION['client_record_id'], //client_id
				'P02' =>$_POST['custom_name'], //customer_name
				'P03' =>$this->clean($_POST['mobile']), //customer_mobile 
				'P04' =>$amount, //amount 
				'P05' =>'SUNCASHME',//source 
				'P06' =>$lbl, //transaction_type
				'P07' =>$_POST['notes'],//notes //no notes in checkout
				'P08'=>$_POST['sand_qr'],//sanddollar_qr'
				'P09'=>$_POST['pin'],//mobile
				'P10'=>$_POST['passcode'],//sand cardotp
				'return_mode'=>'json'
			];			
		}else{
			$sanddollar=[
				'method'=>'processPaymentViaSandDollar',	
				'P01' =>$_SESSION['client_record_id'], //client_id
				'P02' =>$customer_name, //customer_name
				'P03' =>$this->clean($_POST['mobile']), //customer_mobile 
				'P04' =>$amount, //amount 
				'P05' =>'SUNCASHME',//source 
				'P06' =>$lbl, //transaction_type
				'P07' =>$_POST['notes'],//notes //no notes in checkout
				'P08'=>$_POST['sand_qr'],//sanddollar_qr'
				'P09'=>$_POST['pin'],//mobile
				'P10'=>$_POST['passcode'],//sand cardotp
				'return_mode'=>'json'
			];			
		}

		// dd($sanddollar);
		$process_sand=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$sanddollar);
		$process_sanddollar=json_decode($process_sand,true);
		// dd($process_sanddollar);
		if($process_sanddollar['Success']=="YES"){
			$reference_number=$process_sanddollar['ResponseMessage']['data']['0']['paymentRequestId'];

			$user_type = $_SESSION['tag']=='CUSTOMER' ? $_SESSION['suntag_shortcode'] : $_SESSION['merchant_key'];

			$arr_post=[
				'method'=>'create_suncashme_checkout_log',
				'P01'=>$user_type,//MerchantKey
				'P02'=>'',//order_id
				'P03'=>$reference_number,//reference_number
				'P04'=>'sanddollar',//type
				'P05'=>'success',//response
				'P06'=>$sanddollar,//request
				'P07'=>json_encode($process_sanddollar),//$process_sand,//response
				'P08'=>'Payment Successful',//response
				'return_mode'=>'json'	
			];
			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);
			//dd($save_logs);

			//for business epayments onlyu
			if($_SESSION['tag'] !='CUSTOMER'){
				$arr=[
					'method'=>'get_suntag_shortcode',
					'P01'=>$user_type,
					'return_mode' => 'json'
				];
				$api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
					// dd($api_bt_result);
				$api_bt_data=json_decode($api_bt_result,true);	
				$data['suntag_data']='';
				if($api_bt_data['Success']=='YES'){
				$data['suntag_data']=$api_bt_data['ResponseMessage'];
				}else{
						$arr=[	
							'msg'=>'Something went wrong!',
							'success'=>false
						];
						echo json_encode($arr);
						exit;
				} 		
				// email merchant process payment
				$arr=[
					"customer_name"=>$_POST['mobile'],
					"amount"=>number_format($amount,2,'.',''),
					"transaction_id"=>$reference_number,//cenpos
					"dba_name"=>$_SESSION['dba_name'],
					"creation_date"=>date('M d, Y'),
					"reference_num"=>$reference_number,//referece sand
					"notes"=>'',
					"balance"=>'',
					"processing_fee"=>0.00,
					"transaction_fee"=>0.00,
					"fee"=>0.00,
					"vat"=>0.00,
					"cfee"=>0.00,
					"tfee"=>0.00,
					"total"=>number_format($amount,2,'.',''),//amount +fee+vat
					"msg_above"=>"<h2>".$_POST['mobile']." paid <span class='amount'>".number_format($amount,2,'.','')." BSD</span>. </h2>",
					"msg_bottom"=>"",
					"email"=>$data['suntag_data']['business_email_address'],
					"view"=>'email_message_completed.blade.php',
					"title"=>'Completed Payment',
					"profile_pic" => $this->session->userdata("profile_pic"),
					"iscard_fee_on_merch"=>'',
				];

				$this->confirm_email($arr);	
			}		



			$arr=[	
				'msg'=>$process_sanddollar['ResponseMessage'],
				'success'=>true
			];
			
			echo json_encode($arr);	
			exit;
				
		} else {
			
			$user_type = $_SESSION['tag']=='CUSTOMER' ? $_SESSION['suntag_shortcode'] : $_SESSION['merchant_key'];



			$arr_post=[
				'method'=>'create_suncashme_checkout_log',
				'P01'=>$user_type,//MerchantKey
				'P02'=>'',//order_id
				'P03'=>'',//reference_number
				'P04'=>'sanddollar',//type
				'P05'=>'failed',//response
				'P06'=>$sanddollar,//json_encode($sanddollar),//response
				'P07'=>json_encode($process_sanddollar),//response
				'P08'=>'Payment Failed',//response
				'return_mode'=>'json'	
			];
			//dd($arr_post);
			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);
			// dd($save_logs);
			$arr=[	
				'msg'=>$process_sanddollar['ResponseMessage'],
				'success'=>false
			];
			
			echo json_encode($arr);		
			exit;
		}		
	}
	public function sanddollar_suncashme_process(){
		// dd($_POST);


		if($_SESSION['tag']=='CUSTOMER'){
			$lbl="CUSTOMER_PAYMENT";
		}else{
			$lbl="BUSINESS_PAYMENT";
		}
		if($_POST['type'] =='custom_name'){
			$qr=$_POST['custom_name'];
		}else{
		$val  =$_POST['sand_qr'];
		$qr =str_replace('nzia:qr/', '',$val);
		$qr =$qr ;
		}
		$sanddollar=[
			'method'=>'processPaymentViaSandDollarAccountHolders',	
			'P01' =>$this->session->userdata('sand_customer_info')['CustomerId'], //client_id
			'P02' =>$this->session->userdata('sand_customer_info')['CustomerName'], //customer_name
			'P03' =>$this->session->userdata('sand_customer_info')['MobileNumber'], //customer_mobile 
			'P04' =>$_POST['amount'], //amount 
			'P05' =>'SUNCASHME',//source 
			'P06' =>'CUSTOMER_PAYMENT', //transaction_type //fixed since session payor is wallet
			'P07' =>$_POST['notes'],//notes //no notes in checkout
			'P08'=>$qr,//sanddollar_qr'
			'return_mode'=>'json'
		];			
		

		//dd($sanddollar);
		$process_sand=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$sanddollar);
		$process_sanddollar=json_decode($process_sand,true);
		// dd($process_sanddollar);
		if($process_sanddollar['Success']=="YES"){
			$reference_number=$process_sanddollar['ResponseMessage']['data']['0']['paymentRequestId'];

			$user_type = $_SESSION['tag']=='CUSTOMER' ? $_SESSION['suntag_shortcode'] : $_SESSION['merchant_key'];

			$arr_post=[
				'method'=>'create_suncashme_checkout_log',
				'P01'=>$user_type,//MerchantKey
				'P02'=>'',//order_id
				'P03'=>$reference_number,//reference_number
				'P04'=>'sanddollar',//type
				'P05'=>'success',//response
				'P06'=>$sanddollar,//request
				'P07'=>json_encode($process_sanddollar),//$process_sand,//response
				'P08'=>'Payment Successful',//response
				'return_mode'=>'json'	
			];
			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);
			//dd($save_logs);

			//for business epayments onlyu
			if($_SESSION['tag'] !='CUSTOMER'){
				$arr=[
					'method'=>'get_suntag_shortcode',
					'P01'=>$user_type,
					'return_mode' => 'json'
				];
				$api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
					// dd($api_bt_result);
				$api_bt_data=json_decode($api_bt_result,true);	
				$data['suntag_data']='';
				if($api_bt_data['Success']=='YES'){
				$data['suntag_data']=$api_bt_data['ResponseMessage'];
				}else{
						$arr=[	
							'msg'=>'Something went wrong!',
							'success'=>false
						];
						echo json_encode($arr);
						exit;
				} 		
				// email merchant process payment
				$arr=[
					"customer_name"=>$_POST['mobile'],
					"amount"=>number_format($_POST['amount'],2,'.',''),
					"transaction_id"=>$reference_number,//cenpos
					"dba_name"=>$_SESSION['dba_name'],
					"creation_date"=>date('M d, Y'),
					"reference_num"=>$reference_number,//referece sand
					"notes"=>'',
					"balance"=>'',
					"processing_fee"=>0.00,
					"transaction_fee"=>0.00,
					"fee"=>0.00,
					"vat"=>0.00,
					"cfee"=>0.00,
					"tfee"=>0.00,
					"total"=>number_format($_POST['amount'],2,'.',''),//amount +fee+vat
					"msg_above"=>"<h2>".$_POST['mobile']." paid <span class='amount'>".number_format($_POST['amount'],2,'.','')." BSD</span>. </h2>",
					"msg_bottom"=>"",
					"email"=>$data['suntag_data']['business_email_address'],
					"view"=>'email_message_completed.blade.php',
					"title"=>'Completed Payment',
					"profile_pic" => $this->session->userdata("profile_pic"),
					"iscard_fee_on_merch"=>'',
				];

				$this->confirm_email($arr);	
			}		



			$arr=[	
				'msg'=>$process_sanddollar['ResponseMessage'],
				'success'=>true
			];
			
			echo json_encode($arr);	
			exit;
				
		} else {
			
			$user_type = $_SESSION['tag']=='CUSTOMER' ? $_SESSION['suntag_shortcode'] : $_SESSION['merchant_key'];



			$arr_post=[
				'method'=>'create_suncashme_checkout_log',
				'P01'=>$user_type,//MerchantKey
				'P02'=>'',//order_id
				'P03'=>'',//reference_number
				'P04'=>'sanddollar',//type
				'P05'=>'failed',//response
				'P06'=>$sanddollar,//json_encode($sanddollar),//response
				'P07'=>json_encode($process_sanddollar),//response
				'P08'=>'Payment Failed',//response
				'return_mode'=>'json'	
			];
			//dd($arr_post);
			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);
			// dd($save_logs);
			$arr=[	
				'msg'=>$process_sanddollar['ResponseMessage'],
				'success'=>false
			];
			
			echo json_encode($arr);		
			exit;
		}		
	}
	public function sand_generate_passcode(){
		$required_fields=[
		'MerchantKey',
		'sand_mobile',
		'sand_pin',				
		];
		$is_validated=$this->assetHelper->checkpostfields($required_fields,$_POST);	

		if(!empty($is_validated)){
		$arr=[	
			'msg'=>'Following parmeters are mandatory: '.$is_validated,
			'success'=>false
		];
		
		echo json_encode($arr);
		exit;				
		}

		$arr_post=[
			'method'=>'generate_passcode',
			'P01'=>$this->clean($_POST['sand_mobile']),//
			'P02'=>$_POST['sand_pin'],//
			'P03'=>$_POST['MerchantKey'],//
			'return_mode'=>'json'	
		];
		$get_passcode=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr_post);	
		//dd($get_passcode);
		$get_passcode = json_decode($get_passcode,true);
		if($get_passcode['Success']=='YES'){
			$arr=[	
				'msg'=>'Valid Account.',
				'data'=>$get_passcode,
				'success'=>true
			];
			
			echo json_encode($arr);
			exit;	
		} else {
			$arr=[	
				'msg'=>'Not a Valid Account.',
				'success'=>false
			];
			
			echo json_encode($arr);
			exit;	
		}


	}


	public function amazon_review_process(){

		//UPDATE SESSION response will redirect to defined checkoutReviewReturnUrl in payload create session

		$this->session->set_userdata('amz_mobile',$_POST['amazon_mobile']);
		//call fee here
	


		$checkoutSessionId = $this->session->userdata('amz_sess');
		$payload = array(
			'paymentDetails' => array(
				'paymentIntent' => 'AuthorizeWithCapture',
				'canHandlePendingAuthorization' => false,
				'chargeAmount' => array(
					'amount' => $_POST['total_due'],
					'currencyCode' => 'USD'
				),
			),
			'merchantMetadata' => array(
				'merchantReferenceId' => $_POST['dba_name'],//checkoutreferenceid
				'merchantStoreName' =>'SunCash Checkout',
				'noteToBuyer' => 'Thank you for your order!',
			)
		);

		try {
			$checkoutSessionId = $checkoutSessionId;
			$client = new Amazon\Pay\API\Client($this->amazonpay_config);
			$result = $client->updateCheckoutSession($checkoutSessionId, $payload);
			// dd($result);
			if ($result['status'] === 200) {
				$response = json_decode($result['response'], true);
				$amazonPayRedirectUrl = $response['webCheckoutDetails']['amazonPayRedirectUrl'];
				// echo "amazonPayRedirectUrl=$amazonPayRedirectUrl\n";
				$arr=[
					'msg'=>'success update.',
					'url'=>$amazonPayRedirectUrl,
					'success'=>true
				];
				echo json_encode($arr);
				exit;

			} else {
				$arr=[
					'msg'=>'status=' . $result['status'] . '; response=' . $result['response'] . "",
					'url'=>'',
					'success'=>false
				];
				echo json_encode($arr);
				// exit;
			}
		} catch (\Exception $e) {
			// handle the exception
			$arr=[
				'msg'=>$e,
				'url'=>'',
				'success'=>false
			];
			echo json_encode($arr);
			exit;
		}
	}

	public function process_amazon_checkoutv2(){
		if(empty($_POST['amazon_email']) || empty($_POST['amazon_fullname'])){

			$arr=[	
				'msg'=>'Please fill up all fields',
				'success'=>false
			];
			
			echo json_encode($arr);
			exit;
		} 
		$arr=[
			'method'=>'get_client_checkout_details',
			'P01'=>$_POST['m'],
			'P02'=>$_POST['r'],
			'return_mode'=>'json'
		];
		$api_fee_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
		$api_fee_data=json_decode($api_fee_result,true);	
		if($api_fee_data['Success']=='YES'){
			
			$ItemName=[];
			$ItemQty=[];
			$ItemPrice=[];
			if(!empty($api_fee_data['ResponseMessage']['details'])){
				for ($i=0; $i <count($api_fee_data['ResponseMessage']['details']) ; $i++) { 
					# code...
					$ItemName[$i]=$api_fee_data['ResponseMessage']['details'][$i]['item_name'];
					$ItemQty[$i]=$api_fee_data['ResponseMessage']['details'][$i]['qty'];
					$ItemPrice[$i]=$api_fee_data['ResponseMessage']['details'][$i]['price'];		    		
				}
			}
			$return_data=[
			'merchant_client_id'=>$api_fee_data['ResponseMessage']['merchant_client_id'],
			'MerchantKey'=>$api_fee_data['ResponseMessage']['merchant_key'],
			'MerchantName'=>$api_fee_data['ResponseMessage']['merchant_name'],
			'Amount'=>$api_fee_data['ResponseMessage']['amount'],
			'total_amount'=>$_POST['total_due'],//amount +cc fee
			'OrderID'=>$api_fee_data['ResponseMessage']['order_id'],
			'CallbackURL'=>$api_fee_data['ResponseMessage']['callback_url'],
			'ItemName'=>$ItemName,
			'ItemQty'=>$ItemQty,
			'ItemPrice'=>$ItemPrice,
			'OrderReference'=>$_POST['r'],
			'profile_pic'=>$api_fee_data['ResponseMessage']['profile_pic'],
			'reference_id'=>$_POST['r'],
			'sunpass_fee'=>$api_fee_data['ResponseMessage']['sunpass_fee'],
			'facility_fee'=>$api_fee_data['ResponseMessage']['facility_fee'],
			'processing_fee'=>$api_fee_data['ResponseMessage']['processing_fee'],
			'transaction_fee'=>$api_fee_data['ResponseMessage']['transaction_fee'],
			'discount'=>$api_fee_data['ResponseMessage']['discount'],
			];
		}
		$post_data =$return_data;

		//completeCheckoutSession
		$payload = array(
			'chargeAmount' => array(
				'amount' =>$_POST['total_due'],
				'currencyCode' => 'USD'
			),
		);

		try {
			$checkoutSessionId = $this->session->userdata('amz_sess');//chargeid respo form update
			$headers = array('x-amz-pay-Idempotency-Key' => uniqid());
			$client = new Amazon\Pay\API\Client($this->amazonpay_config);
			$result = $client->completeCheckoutSession($checkoutSessionId,$payload, $headers);
			// dd($result);
			if ($result['status'] === 200) {
				$response = json_decode($result['response'], true);
				// dd($response);
				$state = $response['statusDetails']['state'];
				$reasonCode = $response['statusDetails']['reasonCode'];
				$reasonDescription = $response['statusDetails']['reasonDescription'];
				// echo "state=$state; reasonCode=$reasonCode; reasonDescription=$reasonDescription\n";
			} else {
				// check the error
				// echo 'status=' . $result['status'] . '; response=' . $result['response'] . "\n";
				$arr=[
					'msg'=>'status=' . $result['status'] . '; response=' . $result['response'] . "",
					'url'=>'',
					'success'=>false
				];
				echo json_encode($arr);
				exit;

			}
		} catch (\Exception $e) {
			// handle the exception
			echo $e . "\n";
		}

		//process items...
		$order_details='';
		if(!empty($post_data['ItemName'])){
			$len = count($post_data['ItemName']);
			for ($i=0; $i <count($post_data['ItemName']) ; $i++) { 
				# code...
				if($len - 1==$i){
					$order_details.=$post_data['ItemName'][$i]."|".$post_data['ItemQty'][$i]."|".$post_data['ItemPrice'][$i]."";
				} else {
					$order_details.=$post_data['ItemName'][$i]."|".$post_data['ItemQty'][$i]."|".$post_data['ItemPrice'][$i]."~";
				}
			}
		}
		$arr=[
		   'method'=>'get_payment_fee',	
		   'P01' =>$post_data['MerchantKey'], //Merchant API Key
		   'P02' =>$api_fee_data['ResponseMessage']['amount'], //Mobile No
		   'P03' =>'AMAZON', //passcode 		   
		   'return_mode'=>'json'
		];
		$fees=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
		$fees_data=json_decode($fees,true);
		//dd($fees_data);
		$data['fees_data']='';
		if($fees_data['Success']=='YES'){
		$data['fees_data']=$fees_data['ResponseMessage'];
		}
		//dd($data['fees_data']);
		$arr_process=[
			'method'=>'process_amazon_checkout',	
			'P01' =>$post_data['MerchantKey'], //Merchant API Key
			'P02' =>$this->clean($_POST['amazon_mobile']), //Mobile No
			'P03' =>$_POST['amazon_fullname'], //passcode
			'P04' =>$_POST['amazon_email'], //email
			'P05' =>$api_fee_data['ResponseMessage']['amount'], //amount
			'P06' =>'AMAZON',//AMAZON
			'P07' =>$_POST['r'], //checkout refid
			'P08' =>urlencode($order_details),//order details "item_name|qty|price"  paano?
			'P09'=>$response['chargeId'],//amazon refrence id
			'P10' =>$data['fees_data']['processing_fee'], //pf
			'P11' =>$data['fees_data']['transaction_fee'],//tf  
			'P12' =>$data['fees_data']['iscard_fee_on_merch'],//is merch//1 or 0  		
			'P13' =>$_POST['total_due'],
			'return_mode'=>'json'
		];
				// dd($arr_process);
		$proces_amazon_x=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr_process);
		$proces_amazon=json_decode($proces_amazon_x,true);
		//dd($proces_amazon_x);
		if($proces_amazon['Success']=="YES"){
			$reference_number=$response['chargeId'];
			$arr_update=[
				'method'=>'update_checkout_transaction',
				'P01'=>$post_data['OrderReference'],//reference 
				'P02'=>'PROCESSED',//status 
				'return_mode'=>'json'	
			];
			$update_status=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr_update);

			//update fees and total via api af....
			$arr_update=[
				'method'=>'update_checkout_transaction_fee',
				'P01'=>$_POST['r'],//reference 
				'P02'=>$data['fees_data']['processing_fee'],//status 
				'P03'=>$data['fees_data']['transaction_fee'],//status 
				'P04'=>$_POST['total_due'],//status 
				'P05'=>0.00,//status 
				'P06'=>0.00,//status 
				'P07'=>$data['fees_data']['iscard_fee_on_merch'],
				'return_mode'=>'json'	
			];
			//dd($arr_update);
			$update_status=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr_update);
			// dd($update_status);

			$arr=[
				'method'=>'get_suntag_shortcode',
				'P01'=>$post_data['MerchantKey'],
				'return_mode' => 'json'
			];
			$api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
			$api_bt_data=json_decode($api_bt_result,true);	
			// dd($api_bt_result);
			$data['suntag_data']='';
			if($api_bt_data['Success']=='YES'){
				$data['suntag_data']=$api_bt_data['ResponseMessage'];
			}else{
				$arr=[	
					'msg'=>'Something went wrong!',
					'success'=>false
				];
				echo json_encode($arr);
				exit;
			}  
			//$cfee_email = $post_data['iscard_fee_on_merch'] ==1 ? $cfee_email=0.00 : $cfee_email=number_format($upf,2,'.','')+number_format($utf,2,'.','');
			//email merchant process payment
			$arr=[
				"customer_name"=>$_POST['amazon_mobile'],
				"amount"=>number_format($post_data['Amount'],2,'.',''),
				"transaction_id"=>$post_data['OrderID'],//cenpos
				"dba_name"=>$this->session->userdata('dba_name'),
				"creation_date"=>date('M d, Y'),
				"reference_num"=>$reference_number,//referece ng paypal
				"notes"=>'',
				"balance"=>'',
				"processing_fee"=>0.00,
				"transaction_fee"=>0.00,
				"fee"=>0.00,
				"vat"=>0.00,
				"cfee"=>0.00,
				"tfee"=>0.00,
				"total"=>number_format($post_data['total_amount'],2),  //amount +fee+vat
				"msg_above"=>"<h2>".$_POST['amazon_mobile']." paid <span class='amount'>".number_format($post_data['total_amount'],2)." BSD</span>. </h2>",
				"msg_bottom"=>"",
				"email"=>$data['suntag_data']['business_email_address'],
				"view"=>'email_message_completed.blade.php',
				"title"=>'Completed Payment',
				"profile_pic" => $this->session->userdata("profile_pic"),
				"iscard_fee_on_merch"=>'',
			];
			//dd($arr);
			$this->confirm_email($arr);			

			$arr_post=[
				'method'=>'create_suncashme_checkout_log',
				'P01'=>$post_data['MerchantKey'],//MerchantKey
				'P02'=>$post_data['OrderID'],//order_id
				'P03'=>$reference_number,//reference_number
				'P04'=>'amazon',//type
				'P05'=>'success',//response
				'P06'=>$arr_process,//request
				'P07'=>json_encode($proces_amazon),//response
				'P08'=>'Payment Successful',//response
				'return_mode'=>'json'	
			];
			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);
			//dd($save_logs);
			unset($_SESSION['post_data']);
			$params=base64_encode($reference_number."||".$_POST['r']."||success||amazon");
			$arr=[	
				'msg'=>'Successfully Paid',
				'url'=>$post_data["CallbackURL"].$params,
				'success'=>true
			];
			
			echo json_encode($arr);	
		
		} else {
			$arr_post=[
				'method'=>'create_suncashme_checkout_log',
				'P01'=>$post_data['MerchantKey'],//MerchantKey
				'P02'=>$post_data['OrderID'],//order_id
				'P03'=>'',//reference_number
				'P04'=>'amazon',//type
				'P05'=>'failed',//response
				'P06'=>$arr_process,//response
				'P07'=>json_encode($proces_amazon),//response
				'P08'=>'Payment Failed',//response
				'return_mode'=>'json'	
			];
			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);

			$arr=[	
				'msg'=>$proces_amazon['ResponseMessage'],
				'success'=>false
			];
			
			echo json_encode($arr);		
		}
	}

	public function process_amazon_suncashme(){// suncashme payment api

		
		$trans_type = $_SESSION['tag']=='CUSTOMER' ? 'CUSTOMER_PAYMENT' : 'BUSINESS_PAYMENT';
		$user_type = $_SESSION['tag']=='CUSTOMER' ? $_SESSION['suntag_shortcode'] : $_SESSION['merchant_key'];
		$tamount = 	str_replace( ',', '', $_POST['total_due']);
		$amount = 	str_replace( ',', '', $_POST['amount']);
		//completeCheckoutSession
		$payload = array(
			'chargeAmount' => array(
				'amount' =>$_POST['total_due'],
				'currencyCode' => 'USD'
			),
		);

		try {
			$checkoutSessionId = $this->session->userdata('amz_sess');//chargeid respo form update
			$headers = array('x-amz-pay-Idempotency-Key' => uniqid());
			$client = new Amazon\Pay\API\Client($this->amazonpay_config);
			$result = $client->completeCheckoutSession($checkoutSessionId,$payload, $headers);
			// dd($result);
			if ($result['status'] === 200) {
				$response = json_decode($result['response'], true);
				// dd($response);
				$state = $response['statusDetails']['state'];
				$reasonCode = $response['statusDetails']['reasonCode'];
				$reasonDescription = $response['statusDetails']['reasonDescription'];
				// echo "state=$state; reasonCode=$reasonCode; reasonDescription=$reasonDescription\n";
			} else {
				// check the error
				// echo 'status=' . $result['status'] . '; response=' . $result['response'] . "\n";
				$arr=[
					'msg'=>'status=' . $result['status'] . '; response=' . $result['response'] . "",
					'url'=>'',
					'success'=>false
				];
				echo json_encode($arr);
				exit;

			}
		} catch (\Exception $e) {
			// handle the exception
			echo $e . "\n";
		}



		//compute ccfeee for customer no setup
		if($user_type = $_SESSION['tag']=='CUSTOMER'){
			$fee_percentage = 6.9/100;
			$processing_fee = round($amount * $fee_percentage, 2);
			$processing_fee = number_format($processing_fee, "2");
			$transaction_fee = 0.30;
			$bfee = 0.00;
		}else{
			$processing_fee=$_POST['pf'];
			$transaction_fee=$_POST['tf'];
			$bfee = $_POST['bfee'];
		}


		$arr_process=[
			'method'=>'process_amazon_suncashme',
			'P01'=> $this->session->userdata('client_record_id'),//merch id/custimerid
			'P02'=>$_POST['r'],//order_id
			'P03' =>$this->clean($_POST['amazon_mobile']), //Mobile No
			'P04'=>$_POST['amazon_fullname'],
			'P05'=>$_POST['amazon_email'],
			'P06'=>$amount,
			'P07'=>$tamount,
			'P08'=>$response['chargeId'], //amz cahargeid
			'P09'=>$trans_type,//CUSTOMER_PAYMENT/MERCHANT_PAYMENT
			'P10'=>$_POST['notes'],//notes
			'P11'=>$bfee,//billpayfee
			'P12'=>'0.00',//vat
			'P13'=>$processing_fee,//processing fee ccfee
			'P14'=>$transaction_fee,//transaction feee
			'P15'=>$_POST['is_merch'],//source checkout/payment/terminal/donation	
			'return_mode'=>'json'	
		];
		// dd($arr_process);
		$proces_amazon_x=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr_process);
		$proces_amazon=json_decode($proces_amazon_x,true);
		// dd($proces_amazon);
		if($proces_amazon['Success']=="YES"){

			$arr_post=[
				'method'=>'create_suncashme_checkout_log',
				'P01'=>$user_type,//MerchantKey
				'P02'=>$_POST['r'],//order_id
				'P03'=>$response['chargeId'],//reference_number
				'P04'=>'amazon',//type
				'P05'=>'success',
				'P06'=>$arr_process,//response
				'P07'=>$proces_amazon_x,//response
				'P08'=>$proces_amazon['ResponseMessage'],//response
				'return_mode'=>'json'
			];
			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);
			$cfee_email = $_POST['is_merch'] ==1 ? $cfee_email=0.00 : $cfee_email=number_format($_POST['pf'],2,'.','')+number_format($_POST['tf'],2,'.','');
			//email merchant
			$arr=[
				"customer_name"=>$_POST['amazon_fullname'],
				"amount"=>$amount,
				"transaction_id"=>$_POST['r'],//cenpos
				"dba_name"=>$this->session->userdata('dba_name'),
				"creation_date"=>date('M d, Y'),
				"reference_num"=>$response['chargeId'],
				"notes"=>$_POST['notes'],
				"balance"=>'',
				"processing_fee"=>number_format($_POST['pf'],2,'.',''),
				"transaction_fee"=>number_format($_POST['tf'],2,'.',''),
				"fee"=>$_POST['bfee'],
				"vat"=>0.00,
				"cfee"=>$cfee_email,
				"tfee"=>number_format($_POST['bfee'],2,'.',''),
				"total"=>number_format($tamount,2),  //amount +fee+vat
				"msg_above"=>"<h2>".$_POST['amazon_fullname']." paid <span class='amount'>".number_format($tamount,2)." BSD</span>. </h2>",
				"msg_bottom"=>"",
				"email"=>$this->session->userdata('email'),
				"view"=>'email_message_completed.blade.php',
				"title"=>'Completed Payment',
				"profile_pic" => $this->session->userdata("profile_pic"),
				"iscard_fee_on_merch"=>$_POST['is_merch']
			];
			// dd($arr);
			// $this->confirm_email($arr);
			// unset($_SESSION['post_data']);
			// $params=base64_encode($reference_number."||".$_POST['r']."||success||wallet");


			$arr=[	
				'msg'=>$proces_amazon['ResponseMessage'],
				'reference'=>$response['chargeId'],
				'success'=>true
			];
			
			echo json_encode($arr);	
			exit;
		} else {
			//saving logs....
			$arr_post=[
				'method'=>'create_suncashme_checkout_log',
				'P01'=>$user_type,//MerchantKey
				'P02'=>$_POST['r'],//order_id
				'P03'=>'',//reference_number
				'P04'=>'amazon',//type
				'P05'=>'failed',//response
				'P06'=>$arr_process,//response
				'P07'=>$proces_amazon_x,//response
				'return_mode'=>'json'	
			];
			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);
			$arr=[
				'msg'=>$proces_amazon['ResponseMessage'],

				'success'=>false
			];

			echo json_encode($arr);
			exit;
		}
	}	
	public function process_paypal_suncashme(){// suncashme payment api
		// dd($_POST);
		
		$trans_type = $_SESSION['tag']=='CUSTOMER' ? 'CUSTOMER_PAYMENT' : 'BUSINESS_PAYMENT';
		$user_type_logs = $_SESSION['tag']=='CUSTOMER' ? $_SESSION['suntag_shortcode'] : $_SESSION['key'];
		$amount = 	str_replace( ',', '', $_POST['amount']);
		$tamount = 	str_replace( ',', '', $_POST['total_due']);
		if($_POST['status']!='COMPLETED'){
			$paypal_post1=[
				'method'=>'create_suncashme_checkout_log',
				'P01'=>$user_type_logs,//MerchantKey
				'P02'=>$_POST['r'],//order_id
				'P03'=>'',//reference_number
				'P04'=>'paypal-payment',//type
				'P05'=>'failed',//response
				'P06'=>$tamount,//response
				'P07'=>json_encode($_POST['response']),//response
				'P08'=>'PayPal Request Failed',//response
				'return_mode'=>'json'	
			];
			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$paypal_post1);
			
			echo json_encode($arr);	
			exit;	
			$arr=[	
				'msg'=>'Payment Failed. Please try again!',
				'success'=>false
			];
			
			echo json_encode($arr);
			exit;
		}

		//compute ccfeee for customer no setup
		if($user_type = $_SESSION['tag']=='CUSTOMER'){
			$fee_percentage = 6.9/100;
			$processing_fee = round($amount * $fee_percentage, 2);
			$processing_fee = number_format($processing_fee, "2");
			$transaction_fee = 0.30;
			$bfee = 0.00;
		}else{
			$processing_fee=$_POST['pf'];
			$transaction_fee=$_POST['tf'];
			$bfee = $_POST['bfee'];
		}

		//log paypal req
		$paypal_post=[
			'method'=>'create_suncashme_checkout_log',
			'P01'=>$user_type_logs,//MerchantKey
			'P02'=>$_POST['r'],//order_id
			'P03'=>$_POST['paypal_reference_number'],//reference_number
			'P04'=>'paypal-payment',//type
			'P05'=>'success',//response
			'P06'=>$tamount,//response
			'P07'=>json_encode($_POST['response']),//response
			'P08'=>'PayPal Request Success',//response
			'return_mode'=>'json'	
		];
		$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$paypal_post);

		$arr_process=[
			'method'=>'process_paypal_suncashme',
			'P01'=> $this->session->userdata('client_record_id'),//merch id/custimerid
			'P02'=>$_POST['r'],//order_id
			'P03' =>$this->clean($_POST['mobile']), //Mobile No
			'P04'=>$_POST['fullname'],
			'P05'=>$_POST['email'],
			'P06'=>$amount,
			'P07'=>$tamount,
			'P08'=>$_POST['paypal_reference_number'], //paypal refid
			'P09'=>$trans_type,//CUSTOMER_PAYMENT/MERCHANT_PAYMENT
			'P10'=>$_POST['notes'],//notes
			'P11'=>$bfee,//billpayfee
			'P12'=>'0.00',//vat
			'P13'=>$processing_fee,//processing fee ccfee
			'P14'=>$transaction_fee,//transaction feee
			'P15'=>$_POST['is_merch'],//source checkout/payment/terminal/donation	
			'return_mode'=>'json'	
		];
		// dd($arr_process);
		$proces_paypal_x=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr_process);
		$process_paypal=json_decode($proces_paypal_x,true);
		// dd($process_paypal);
		if($process_paypal['Success']=="YES"){

			$arr_post=[
				'method'=>'create_suncashme_checkout_log',
				'P01'=>$user_type_logs,//MerchantKey
				'P02'=>$_POST['r'],//order_id
				'P03'=>$_POST['paypal_reference_number'],//reference_number
				'P04'=>'paypal-payment',//type
				'P05'=>'success',
				'P06'=>$arr_process,//request
				'P07'=>json_encode($process_paypal),//response
				'P08'=>'Payment Successfull',//response
				'return_mode'=>'json'
			];
			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);
			// dd($arr_post);
			$cfee_email = $_POST['is_merch'] ==1 ? $cfee_email=0.00 : $cfee_email=number_format($_POST['pf'],2,'.','')+number_format($_POST['tf'],2,'.','');
			//email merchant
			$arr=[
				"customer_name"=>$_POST['fullname'],
				"amount"=>$amount,
				"transaction_id"=>$_POST['r'],//cenpos
				"dba_name"=>$this->session->userdata('dba_name'),
				"creation_date"=>date('M d, Y'),
				"reference_num"=>$_POST['paypal_reference_number'],
				"notes"=>$_POST['notes'],
				"balance"=>'',
				"processing_fee"=>number_format($_POST['pf'],2,'.',''),
				"transaction_fee"=>number_format($_POST['tf'],2,'.',''),
				"fee"=>$_POST['bfee'],
				"vat"=>0.00,
				"cfee"=>$cfee_email,
				"tfee"=>number_format($_POST['bfee'],2,'.',''),
				"total"=>$tamount,  //amount +fee+vat
				"msg_above"=>"<h2>".$_POST['fullname']." paid <span class='amount'>".$tamount." BSD</span>. </h2>",
				"msg_bottom"=>"",
				"email"=>$this->session->userdata('email'),
				"view"=>'email_message_completed.blade.php',
				"title"=>'Completed Payment',
				"profile_pic" => $this->session->userdata("profile_pic"),
				"iscard_fee_on_merch"=>$_POST['is_merch']
			];
			// dd($arr);
			// $this->confirm_email($arr);
			// unset($_SESSION['post_data']);
			// $params=base64_encode($reference_number."||".$_POST['r']."||success||wallet");


			$arr=[	
				'msg'=>$process_paypal['ResponseMessage'],
				'reference'=>$_POST['paypal_reference_number'],
				'success'=>true
			];
			
			echo json_encode($arr);	
			exit;
		} else {
			//saving logs....
			$arr_post=[
				'method'=>'create_suncashme_checkout_log',
				'P01'=>$user_type_logs,//MerchantKey
				'P02'=>$_POST['r'],//order_id
				'P03'=>'',//reference_number
				'P04'=>'paypal-payment',//type
				'P05'=>'failed',//response
				'P06'=>$arr_process,//request
				'P07'=>json_encode($process_paypal),//response
				'return_mode'=>'json'	
			];
			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);
			$arr=[
				'msg'=>$process_paypal['ResponseMessage'],

				'success'=>false
			];

			echo json_encode($arr);
			exit;
		}
	}
	public function clean($string) {
    	// $this->assetHelper->session_checker();
        $string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.
        return preg_replace('/[^A-Za-z0-9]/', '', $string); // Removes special chars.
	}
  public function link_card_form(){
		//site verify
		$arr=[
			'merchant'=>CENPOST_MERCHANT_ID,
			'secretkey'=>CENPOST_MERCHANT_SECRETKEY,
			'email'=>'suncashme@gmail.com',
			'ip'=>'127.0.0.1',
		];
		$verify_post=$this->assetHelper->api_requestv2(CENPOS_SITEVERIFY_URL,$arr);
		$verify_post=json_decode($verify_post,true);	
		//dd($verify_post);
    $data['verify_params']=$verify_post;
    $data['customer_id']=$this->session->userdata('CustomerId');//."_".date('YmdHis')
    $this->blade->view('customer.dashboard.add_card',$data); 
  }
  public function delete_linked_card(){
    $arr=[
      'method'=>'delete_customer_creditcard',
      'P01'=> $this->session->userdata('SessionID_cc'),
      'P02'=> $this->input->post('id'),
      'return_mode' => 'json'
    ];
    // dd($arr);
    $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr);
    $api_bt_data=json_decode($api_bt_result,true);
   // dd($api_bt_data);
    
    if($api_bt_data['ResponseCode']=='0000'){
      $arr=[  
        'msg'=>$api_bt_data['ResponseMessage'],
        'success'=>true
      ];
      
      echo json_encode($arr);
      exit;        
    }  else {
      $arr=[  
        'msg'=>$api_bt_data['ResponseMessage'],
        'success'=>false
      ];
      
      echo json_encode($arr);
      exit;  
    }
  }
  public function upload_requirements(){
    $data['id']=$_POST['other_data']['cid'];
    $data['v']=$_POST['other_data']['v'];
    $this->blade->view('requirement_uploads',$data); 
  }
  public function process_link_card(){
    $invoice = $_POST['customer_id'].''.strtotime(date('YmdHis'));
    $arr=[
			'amount'=>0.00,
			'invoicenumber'=>$_POST['customer_id'].''.strtotime(date('YmdHis')),
			'type'=>'Sale',
			'email'=>'suncashme@gmail.com',
			'secretkey'=>CENPOST_MERCHANT_SECRETKEY,
			'merchant'=>CENPOST_MERCHANT_ID,
			'tokenid'=>$_POST['tokenid'],
		];	
    $verifying_postp='';
    $vp='';
    $verifying_postp=$this->assetHelper->api_requestv2(CENPOS_SITEVERIFY_URL,$arr);
    //dd($verify_post2);
    $verifying_postp=json_decode($verifying_postp,true);     
    $vp=$verifying_postp['Data'];

		$arr_post=[
			'verifyingpost'=>$vp,
			'tokenid'=>$_POST['tokenid'],
		];
		$process_payment=$this->assetHelper->api_requestv2(CENPOS_PROCESS_URL,$arr_post);

    //save token
    $arr=[
      'secretkey'=>CENPOST_MERCHANT_SECRETKEY,
      'merchant'=>CENPOST_MERCHANT_ID,
      'customercode'=>$_POST['customer_id'],
      //'iscvv'=>true,
      //'isrecaptcha'=>false,
      'tokenid'=>$_POST['tokenid'],
    ];
    //dd($arr);
    $verifying_post2='';
    $verify_post2=$this->assetHelper->api_requestv2(CENPOS_SITEVERIFY_URL,$arr);
    //dd($verify_post2);
    $verify_post2=json_decode($verify_post2,true);     
    //dd($verify_post2);
    $arr_post2=[
      'verifyingpost'=>$verify_post2['Data'],
      'tokenid'=>$_POST['tokenid'],
    ];
    //dd($arr_post2);
    $process_payment2=$this->assetHelper->api_requestv2(CENPOS_CONVERTCRYPTO_URL,$arr_post2);
    $process_payment2=json_decode($process_payment2,true);
    $arr_post=[
      'method'=>'log_cenpos',
      'P01'=> $this->session->userdata('SessionID_cc'),
      'P02'=> $process_payment2,//data//request or response data
      'P03'=> 'RESPONSE',//status // "REQUEST" or "RESPONSE"
      'P04'=> CENPOS_SITEVERIFY_URL,//cenpos_method
      'P05'=> $this->session->userdata('customer_info_cc')['CustomerId'],
      'return_mode' => 'json'
    ];
    $save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);
    $_POST['card_number']=str_replace("*","",$_POST['card_number_link']);
    //dd($process_payment);
		if($process_payment2['Result']==0){
        $arr=[
          'method'=>'save_customer_creditcard',
          'P01'=> $this->session->userdata('SessionID_cc'),
          'P02'=> $this->input->post('customer_id'),
          'P03'=> $process_payment2['TokenId'],//TokenId
          'P04'=> $_POST['card_type_link'],//CardType
          'P05'=> str_replace('*','',$_POST['card_number_link']),//CardLastFourDigits
          'P06'=> substr($invoice, -6),//otp(invoice last 6 digits)
          'P07'=>'0',//fixed to zero 0
          'P08'=> $_POST['name_on_card_link'],//NameOnCard
          'return_mode' => 'json'
        ];
        $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr);
        //dd($api_bt_result);
        $api_bt_data=json_decode($api_bt_result,true);

        
        if($api_bt_data['ResponseCode']=='0000'){
          $arr=[  
            'msg'=>'Successfully linked.',//$api_bt_data['ResponseMessage'],
            'success'=>true
          ];
          
          echo json_encode($arr);
          exit;        
        }  else {
          $arr=[  
            'msg'=>$api_bt_data['ResponseMessage'],
            'success'=>false
          ];
          
          echo json_encode($arr);
          exit;  
        }
    }
  }
  public function process_verify_card(){
    $verifiction_code=hex2bin($_POST['v']);
    $post_verification_code=$_POST['verification'];
    //validate verification code.
    if($verifiction_code!=$post_verification_code){
      $arr=[  
        'msg'=>'Invalid Verification code.',
        'success'=>false
      ];
      
      echo json_encode($arr);
      exit;  
    }

    $arr=[
      'method'=>'verify_customer_creditcard',
      'P01'=> $this->session->userdata('SessionID_cc'),
      'P02'=> $this->input->post('c'),
      'return_mode' => 'json'
    ];
    $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr);
    //dd($api_bt_result);
    $api_bt_data=json_decode($api_bt_result,true);

    
    if($api_bt_data['ResponseCode']=='0000'){
      $arr=[  
        'msg'=>$api_bt_data['ResponseMessage'],
        'success'=>true
      ];
      
      echo json_encode($arr);
      exit;        
    }  else {
      $arr=[  
        'msg'=>$api_bt_data['ResponseMessage'],
        'success'=>false
      ];
      
      echo json_encode($arr);
      exit;  
    }
  }	
	public function process_customer_cc_payment(){
		//var_dump($_SESSION);die;
		if(empty($_POST['tokenid'])){

			$arr=[	
				'msg'=>'Please fill up all fields',
				'success'=>false
			];
			
			echo json_encode($arr);

		} 
		$post_data =$this->session->userdata('post_data');

		$arr_post=[
			'method'=>'processCreditCardPayment',
			'P01'=>$this->session->userdata('customer_info_cc')['SessionID_cc'],//customerid,
			'P02'=>$this->session->userdata('customer_info_cc')['CustomerId'],//order_id
			'P03'=>$_POST['amount'],//reference_number
			'P04'=>number_format($utf,2,'.',''),//transaction fee 4%
			'P05'=>number_format($upf,2,'.',''),//processing fee  .20
			'P06'=>$iscard_fee_on_merch,//if card fee is charged to merchant or customer
			'P07'=>$_POST['hid_fee'],//billpay fee
			'P08'=>$_POST['hid_vat'], //billpay vat
			'P09'=>$_POST['amount_val'],//total amount
			'P10'=>'Sale', //Sale or Auth
			'P11'=>CENPOST_MERCHANT_SECRETKEY,//forsuncash
			'P12'=>CENPOST_MERCHANT_ID,//for ticketing only
			'P13'=>base64_decode($_POST['select_card']),//billpay fee
			'P14'=>$_POST['reference_num'],//orderid or invoice number in form
			'P15'=>'payment',//source checkout/payment/terminal/donation	
			'return_mode'=>'json'	
		];		
			// dd($arr_post);
		$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);			
				//dd($save_logs);



		//email merchant
		$arr=[
			"customer_name"=>$_POST['name'],
			"amount"=>$amount,
			"transaction_id"=>$_POST['reference_num'],//cenpos
			"dba_name"=>$this->session->userdata('dba_name'),
			"creation_date"=>date('M d, Y'),
			"reference_num"=>$process_payment['ReferenceNumber'],
			"notes"=>$_POST['notes'],
			"balance"=>'',
			"processing_fee"=>number_format($upf,2,'.',''),
			"transaction_fee"=>number_format($utf,2,'.',''),
			"fee"=>$_POST['hid_fee'],
			"vat"=>$_POST['hid_vat'],
			"cfee"=>$cfee_email,
			"tfee"=>number_format($_POST['hid_fee'],2,'.','')+number_format($_POST['hid_vat'],2,'.',''),
			"total"=>number_format($grand_total,2),  //amount +fee+vat
			"msg_above"=>"<h2>".$_POST['name']." paid <span class='amount'>".number_format($grand_total,2)." BSD</span>. </h2>",
			"msg_bottom"=>"",
			"email"=>$this->session->userdata('email'),
			"view"=>'email_message_completed.blade.php',
			"title"=>'Completed Payment',
			"profile_pic" => $this->session->userdata("profile_pic"),
			"iscard_fee_on_merch"=>$iscard_fee_on_merch
		];
		//dd($arr);
		$this->confirm_email($arr);


		unset($_SESSION);

		$url = base_url('payment/card_success');


		$arr=[
			'msg'=>$process_payment['Message'],
			'reference'=>$process_payment['ReferenceNumber'],
			'card_success'=>$url ,
			'payment_method'=>'Card',
			'success'=>'success'
		];

		echo json_encode($arr);

	}

	public function saveBusinessSandDollarContact(){
		// dd($_POST['sand_card');
			$arr_post=[
				'method'=>'saveBusinessSandDollarContact',
				'P01'=>$_POST['m'],
				'P02'=>$_POST['merchant_customer_id'],
				'P03'=>$_POST['sand_fullname'],
				'P04'=>$_POST['sand_card'],
				'P05'=>$_POST['sand_mobile'],
				'P06'=>$_POST['sand_email'],			
				'return_mode'=>'json'	
			];
	// dd($arr_post);
			$api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr_post);
			$api_bt_data=json_decode($api_bt_result,true);	
			if($api_bt_data['Success']=='YES'){
      			$arr=[  
					'msg'=>$api_bt_data['ResponseMessage'],
					'success'=>true
				];
				
				echo json_encode($arr);
				exit;  
			} else {
				$arr=[  
					'msg'=>$api_bt_data['ResponseMessage'],
					'success'=>false
				];
				
				echo json_encode($arr);
				exit;  
			}

	}
// public function process_cc_wallet(){
// 	//get temp tokken...
// 	//dd($_POST);
// 	$auth_key=$this->assetHelper->create_tokken();
// 	//check money
// 	$current_balance = $this->assetHelper->get_balance($_SESSION['SessionID_cc']);
// 	$amt=	str_replace( ',', '', $this->input->post('amount_val'));// total amount with fees
// 	$amt1=	str_replace( ',', '', $this->input->post('amount'));// total amount with fees
// 	// $amt=$this->input->post('amount_val');
// 	if($this->input->post('amount')==0){
// 		$arr=[	
// 			'msg'=>'Amount should be greater than 0',
// 			'success'=>false,
// 		];
		
// 		echo json_encode($arr);
// 		exit;			
// 	}

// 	//pay via card 1st
// 		// $arr=[
// 		// 'amount'=>$_POST['amount_val'],
// 		// 'type'=>Ucfirst('Sale'), //SALE
// 		// 'invoicenumber'=>$_POST['reference_num'],
// 		// 'secretkey'=>CENPOST_MERCHANT_SECRETKEY,
// 		// 'merchant'=>CENPOST_MERCHANT_ID,
// 		// 'customercode'=>$this->session->userdata('customer_info_cc')['CustomerId'],//customerid
// 		// 'tokenid'=>base64_decode($_POST['select_card']),
// 		// ];
// 		// //dd($arr);
// 		// $verifying_post='';
// 		// $verify_post1=$this->assetHelper->api_requestv2(CENPOS_SITEVERIFY_URL,$arr);
// 		// //dd($verify_post1);
// 		// $verify_post=json_decode($verify_post1,true);
// 		// //result verify
// 		// if($verify_post['Result']==0){
// 		// $arr_post2=[	
// 		// 	'verifyingpost'=>$verify_post['Data'],
// 		// 	'tokenid'=>base64_decode($_POST['select_card']),
// 		// ];
// 		// //dd($arr_post2);
// 		// $process_payment1=$this->assetHelper->api_requestv2(CENPOS_PROCESS_URL,$arr_post2);
// 		// $process_payment=json_decode($process_payment1,true);
// 		// //dd($process_payment1);
// 		// if($process_payment['Result']==0){
		
// 		// 	$arr_post=[
// 		// 		'method'=>'create_suncashme_cenpos_transaction_detail',
// 		// 		'P01'=>$this->session->userdata('customer_info_cc')['CustomerId'],//customerid,
// 		// 		'P02'=>$_POST['reference_num'],//order_id
// 		// 		'P03'=>$process_payment['ReferenceNumber'],//reference_number
// 		// 		'P04'=>$_POST['card_number_link'],//type
// 		// 		'P05'=>$_POST['name_on_card_link'],//response
// 		// 		'P06'=>$_POST['card_type_link'],//response
// 		// 		'P07'=>$_POST['amount'],//response
// 		// 		'P08'=>number_format($utf,2,'.',''), //transaction fee fee
// 		// 		'P09'=>number_format($upf,2,'.',''),//processing fee
// 		// 		'P10'=>number_format($_POST['total_fee'],2,'.',''),//totall of all fee master fee tne fee
// 		// 		'P11'=>0.00,//forsuncash
// 		// 		'P12'=>0.00,//for ticketing only
// 		// 		'P13'=>$_POST['hid_fee'],//billpay fee
// 		// 		'P14'=>$_POST['hid_vat'],//vat
// 		// 		'P15'=>'payment',//source checkout/payment/terminal/donation
// 		// 		'P16'=>0.00,//discount
// 		// 		'P17'=>$iscard_fee_on_merch,
// 		// 		'P18'=>$_POST['notes'],
// 		// 		'P19'=>$_POST['name'],
// 		// 		'P20'=>$_POST['email'],	
// 		// 		'P21'=>$this->clean($_POST['mobile']),		
// 		// 		'return_mode'=>'json'	
// 		// 	];

// 			$arr_post=[
// 				'method'=>'processCreditCardPayment',
// 				'P01'=>$this->session->userdata('customer_info_cc')['SessionID_cc'],//customerid,
// 				'P02'=>$this->session->userdata('customer_info_cc')['CustomerId'],//order_id
// 				'P03'=>$_POST['amount'],//reference_number
// 				'P04'=>number_format($utf,2,'.',''),//transaction fee 4%
// 				'P05'=>number_format($upf,2,'.',''),//processing fee  .20
// 				'P06'=>$iscard_fee_on_merch,//if card fee is charged to merchant or customer
// 				'P07'=>$_POST['hid_fee'],//billpay fee
// 				'P08'=>$_POST['hid_vat'], //billpay vat
// 				'P09'=>$_POST['amount_val'],//total amount
// 				'P10'=>'Sale', //Sale or Auth
// 				'P11'=>CENPOST_MERCHANT_SECRETKEY,//forsuncash
// 				'P12'=>CENPOST_MERCHANT_ID,//for ticketing only
// 				'P13'=>base64_decode($_POST['select_card']),//billpay fee
// 				'P14'=>$_POST['reference_num'],//orderid or invoice number in form
// 				'P15'=>'payment',//source checkout/payment/terminal/donation	
// 				'return_mode'=>'json'	
// 			];		
// 		// dd($arr_post);
// 			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);			
// 			//dd($save_logs);

// 			//saving logs....
// 			$arr_post=[
// 				'method'=>'create_suncashme_checkout_log',
// 				'P01'=>$user_type,//MerchantKey
// 				'P02'=>$_POST['reference_num'],//order_id
// 				'P03'=>$process_payment['ReferenceNumber'],//reference_number
// 				'P04'=>'cenpos',//type
// 				'P05'=>'success',//response
// 				'P06'=>json_encode($arr_post),//response
// 				'P07'=>json_encode($save_logs),//response
// 				'P08'=>$process_payment['Message'],//response
// 				'return_mode'=>'json'
// 			];
// 			$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);



// 			//email customer.
				
// 		//$TransactionID = $_SESSION['tag']=='CUSTOMER' ? $api_bt_data['ResponseMessage']['TransactionID'] : $api_bt_data['ResponseMessage'];
// 		$arr=[
// 			"customer_name"=>$this->session->userdata('customer_info_cc')['CustomerName'],
// 			"amount"=>number_format($amt,2),
// 			"transaction_id"=>$process_payment['ReferenceNumber'],
// 			"dba_name"=>$this->session->userdata('dba_name'),
// 			"creation_date"=>date('M d, Y'),
// 			"reference_num"=>$this->input->post('reference_num'),
// 			"notes"=>$this->input->post('notes'),
// 			"balance"=>$this->input->post('balance'),
// 			"fee"=>number_format($this->input->post('hid_fee'),2),
// 			"vat"=>number_format($this->input->post('hid_vat'),2),
// 			"total"=>number_format($this->input->post('amount_val'),2),  //amount +fee+vat
// 			"msg_above"=>"<h2>You've sent <span class='amount'>".number_format($amt,2)." BSD</span> to ".$this->session->userdata('dba_name').".</h2>",
// 			"msg_bottom"=>"<p>Transaction is <span class='status'>PENDING</span> until ".$this->session->userdata('dba_name')." accepts payment!</p>",
// 			"email"=>$this->session->userdata('customer_info')['Email'],
// 			"view"=>'email_message.blade.php',
// 			"title"=>'Pending Payment',
// 			"profile_pic" => $this->session->userdata("ImageURL")
// 		];
// 		//dd($arr);	
// 		$this->pending_email($arr);
// 		//email merchant
// 		$arr=[
// 			"customer_name"=>$this->session->userdata('dba_name'),
// 			"amount"=>number_format($this->input->post('amount_val'),2),
// 			"transaction_id"=>$process_payment['ReferenceNumber'],
// 			"dba_name"=>$this->session->userdata('dba_name'),
// 			"creation_date"=>date('M d, Y'),
// 			"reference_num"=>$this->input->post('reference_num'),
// 			"notes"=>$this->input->post('notes'),
// 			"balance"=>$this->input->post('balance'),
// 			"fee"=>$this->input->post('hid_fee'),
// 			"vat"=>$this->input->post('hid_vat'),
// 			"total"=>number_format($this->input->post('amount_val'),2),  //amount +fee+vat
// 			"msg_above"=>"<h2>".$this->session->userdata('customer_info')['CustomerName']." sent you <span class='amount'>".number_format($this->input->post('amount_val'),2)." BSD</span></h2>",
// 			"msg_bottom"=>"<p>Transaction is <span class='status'>PENDING</span> until ".$this->session->userdata('dba_name')." accepts payment!</p>",
// 			"email"=>$this->session->userdata('email'),
// 			"view"=>"email_message_merchant.blade.php",
// 			"title"=>'Incoming Payment',
// 			"profile_pic" => $this->session->userdata("profile_pic")
// 		];

// 		$this->pending_email($arr);

// 		$user_type = $_SESSION['tag']=='CUSTOMER' ? $_SESSION['suntag_shortcode'] : $_SESSION['merchant_key'];

// 		$arr_post=[
// 			'method'=>'create_suncashme_checkout_log',
// 			'P01'=>$user_type,//MerchantKey
// 			'P02'=>$this->input->post('reference_num'),//order_id
// 			'P03'=>$process_payment['ReferenceNumber'],
// 			'P04'=>'cenpos',//type
// 			'P05'=>'success',//response
// 			'P06'=>$arr_r,//request
// 			'P07'=>$api_bt_data,//response
// 			'P08'=>'',//response
// 			'return_mode'=>'json'
// 		];
// 			// dd($arr_post);
// 		$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);
// 		//get t idvia tag
// 		$tid=$api_bt_data['ResponseMessage'];
// 		if($_SESSION['tag']=='CUSTOMER'){
// 			$tid=$process_payment['ReferenceNumber'];
// 		} 
// 		$arr=[
// 			'msg'=>$tid,
// 			'current_balance'=>'',
// 			'success'=>true
// 		];

// 		echo json_encode($arr);
// 		exit;
// 		} else {
// 		$user_type = $_SESSION['tag']=='CUSTOMER' ? $_SESSION['suntag_shortcode'] : $_SESSION['merchant_key'];

// 		$arr_post=[
// 			'method'=>'create_suncashme_checkout_log',
// 			'P01'=>$user_type,//MerchantKey
// 			'P02'=>$this->input->post('reference_num'),//order_id
// 			'P03'=>'',
// 			'P04'=>'cenpos',//type
// 			'P05'=>'failed',//response
// 			'P06'=>$arr_r,//request
// 			'P07'=>$api_bt_data,//response
// 			'P08'=>'',//response
// 			'return_mode'=>'json'	
// 		];
// 		$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);



// 		$arr=[	
// 			'msg'=>'Failed',//$tid,
// 			'current_balance'=>'',
// 			'success'=>false,
// 		];
		
// 		echo json_encode($arr);
// 		exit;

// 		}	
// 	} else {

// 	}



		
// }

}