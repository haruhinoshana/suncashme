<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SuncashmeController extends CI_Controller {// put filename of controller on class name 

	function __construct(){//where can put dynamic functions and models....
        parent::__construct();
   		$this->session->flashdata('msg');
   		//load model.
      	$this->load->model('core/db');//for transactions and other eloquent db features
      	$this->load->model('core/qr');
   		//$this->load->model('user');
   		//$this->load->model('Value_sets_global','global');
   		$this->assetHelper = new AssetHelper\AssetHelper();
   		
	}
	public function index($suntag=null,$amount=null){//method then routefind
		if(isset($suntag)){
		
		//get temp tokken...
			$data['sts']="Approved";
			$auth_key=$this->assetHelper->create_tokken();
			//dd($auth_key);
			if(empty($auth_key)){
				return $this->blade->view('errors.html.error_timeout');
			}
			//dd($auth_key);
		    $arr=[
		    	'method'=>'get_profile_via_suntag',
	            'P01'=> $suntag,
	            'P02'=>$auth_key['temp_auth'],
	            'return_mode'=>'json'
	            //'P02'=>$this->security->xss_clean($this->input->post('payroll_id')),
		    ];
		    //$arr_tosend['url']="get_payroll_listing?".http_build_query($arr);

		    // dd($arr);
		    $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_API_URL,$arr);
		  		
		    $api_bt_data=json_decode($api_bt_result,true);
		    // dd($api_bt_result);
		    //d($api_bt_data);
		    $data['suntag_data'] ='';
	        if($api_bt_data['ResponseCode']=='0000'){
	         	$data['suntag_data']=$api_bt_data['ResponseMessage'];
	        } else{
	          	return $this->blade->view('suncash-me',$data);
			}
			// dd($data['suntag_data']);
			$this->session->set_userdata($data['suntag_data']);
			//dd($data['suntag_data']);
			//get payment method allowed.
			$data['login_data']='';

			if($data['suntag_data']['RegistrationType']=='Charity'){
				//echo 'Charity';
			    $arr=[
			    	'method'=>'login_charity',
		            'P01'=> SERVICE_ACCOUNT_CHARITY_USERNAME_DEV,
		            'P02'=> SERVICE_ACCOUNT_CHARITY_PASSWORD_DEV,
			    ];
			    //$arr_tosend['url']="login_business?".http_build_query($arr);

			    //dd($arr_tosend);
			    $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_CHARITY_LOGIN_API_URL,$arr);
			    $api_bt_data=json_decode($api_bt_result,true);
			    // dd($api_bt_result);
			    $data['login_data']='';
			    if($api_bt_data['ResponseCode']=='YES'){
			    $data['login_data']=$api_bt_data['ResponseMessage'];                     
			    } else {
						return $this->blade->view('errors.html.error_timeout');
				}
			} else if(!isset($data['suntag_data']['RegistrationType'])){

				//echo 'business';
			    $arr=[
			    	'method'=>'login_business',
		            'P01'=> SERVICE_ACCOUNT_USERNAME_DEV,
		            'P02'=> SERVICE_ACCOUNT_PASSWORD_DEV,
			    ];
			    //$arr_tosend['url']="login_business?".http_build_query($arr);

			    //dd($arr_tosend);
			    $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
			    // dd($api_bt_result);
			    $api_bt_data=json_decode($api_bt_result,true);
			    // dd($api_bt_result);
			    //$data['login_data']='';
			    if($api_bt_data['ResponseCode']=='YES'){
			    $data['login_data']=$api_bt_data['ResponseMessage'];                     
			    }else {
					return $this->blade->view('errors.html.error_timeout');
				}

			} else {
				//echo 'business';
			    $arr=[
			    	'method'=>'login_business',
		            'P01'=> SERVICE_ACCOUNT_USERNAME_DEV,
		            'P02'=> SERVICE_ACCOUNT_PASSWORD_DEV,
			    ];
			    //$arr_tosend['url']="login_business?".http_build_query($arr);

			    //dd($arr_tosend);
			    $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);

			    // dd($api_bt_result);
			    $api_bt_data=json_decode($api_bt_result,true);
			    // dd($api_bt_result);
			    //$data['login_data']='';
			    if($api_bt_data['ResponseCode']=='YES'){
			    $data['login_data']=$api_bt_data['ResponseMessage'];                     
			    } else {
					return $this->blade->view('errors.html.error_timeout');
				}
			    //dd($api_bt_result);
			}

			//get payment settings. 
		       $arr=[
		         'method'=>'get_checkout_methods_byid',
		            'P01'=> $data['login_data']['SessionID'],
		            'P02'=> $data['suntag_data']['client_record_id'],
		       ];
		       // $arr_tosend['url']="login_business?".http_build_query($arr);

		       // dd($arr_tosend);
		       // dd($arr);
		       $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
		       $api_bt_data=json_decode($api_bt_result,true);
		    //    dd($api_bt_result);
		       $data['selected_settings_data']='';
		       if($api_bt_data['Success']=='YES'){
		       $api_bt_data['ResponseMessage'][0] =isset($api_bt_data['ResponseMessage'][0]) ? $api_bt_data['ResponseMessage'][0] : '';
		       $data['selected_settings_data']=$api_bt_data['ResponseMessage'][0];
		       } 
		       	
				if($data['suntag_data']['tag']=="MERCHANT"){
					$this->session->set_userdata('client_record_id' , $data['suntag_data']['client_record_id']);
					$this->session->set_userdata('client_id' , $data['suntag_data']['client_id']);
					$this->session->set_userdata('tag' , $data['suntag_data']['tag']);
					$this->session->set_userdata('key' , $data['suntag_data']['merchant_key']);
					$this->session->set_userdata('dba_name' , $data['suntag_data']['dba_name']);
					$this->session->set_userdata('email' , $data['suntag_data']['email']);
					$this->session->set_userdata('registration_type' , $data['suntag_data']['RegistrationType']);


					if($data['suntag_data']['client_status_id']=="0"){//pending...				
						if($data['suntag_data']['registration_status']=="Pending"){//pending...
							$data['sts']='Pending';
						} else if($data['suntag_data']['registration_status']=="Rejected"){//rejected
							$data['sts']='Rejected';
						} else if($data['suntag_data']['registration_status']=="For Verification"){//pending for verification
							//null or not yet finished in registration //F
							$data['sts']='For Verification';
						}else if($data['suntag_data']['registration_status']=="Inactive"){//inactive
							$data['sts']='Inactive';						
						}
					}elseif($data['suntag_data']['client_status_id']=="2"){
							$data['sts']='Store Deactivated';
					}elseif($data['suntag_data']['client_status_id']=="-1"){
						$data['sts']='Inactive';	
					}else{
						$data['sts']='Inactive';	
					}


				} else {
					$this->session->set_userdata('client_record_id' , $data['suntag_data']['client_record_id']);
					//$this->session->set_userdata('client_id' , $data['suntag_data']['client_id']);
					$this->session->set_userdata('tag' , $data['suntag_data']['tag']);	
					$this->session->set_userdata('kyc_type' , $data['suntag_data']['type']);	
					$this->session->set_userdata('mobile_suntag' , $data['suntag_data']['customer_mobile']);
					$this->session->set_userdata('dba_name' , $data['suntag_data']['dba_name']);
					$this->session->set_userdata('email' , $data['suntag_data']['email']);
					$this->session->set_userdata('registration_type' , $data['suntag_data']['RegistrationType']);					
					if($data['suntag_data']['status']=="Inactive"){//inactive...
						$data['sts']='Inactive';
					} 

				}
				$data['amount_to_process']=$amount;
			// dd($data);
			if($data['suntag_data']['RegistrationType']=='Charity'){
				return $this->blade->view('suncash-me-charity',$data);
			} else if(!isset($data['suntag_data']['RegistrationType'])){
				return $this->blade->view('suncash-me',$data);
			}else if($data['suntag_data']['RegistrationType']=='CUSTOMER'){
				return $this->blade->view('suncash-me',$data);
			// 	return $this->blade->view('errors.html.error_timeout');
			}else {
				return $this->blade->view('suncash-me',$data);
				// return $this->blade->view('errors.html.error_timeout');
			}
			
		} else {
			//echo "not set";


			return $this->blade->view('main');
		}


		//	//for blade view calling
	}
	public function indexqr($suntag=null,$amount=null){//method then routefind
		if(isset($suntag)){
		// dd($suntag);
		//get temp tokken...
			$data['sts']="Approved";
			$auth_key=$this->assetHelper->create_tokken();
			if(empty($auth_key)){
				return $this->blade->view('errors.html.error_timeout');
			}
		    $arr=[
		    	'method'=>'get_profile_via_suntag',
	            'P01'=> $suntag,
	            'P02'=>$auth_key['temp_auth'],
	            'return_mode'=>'json'
		    ];
		    $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_API_URL,$arr);	  		
		    $api_bt_data=json_decode($api_bt_result,true);
		    $data['suntag_data'] ='';
	        if($api_bt_data['ResponseCode']=='0000'){
	         	$data['suntag_data']=$api_bt_data['ResponseMessage'];
	        } else{
	          	return $this->blade->view('suncash-me',$data);
			}
			$this->session->set_userdata($data['suntag_data']);
			$data['login_data']='';

			if($data['suntag_data']['RegistrationType']=='Charity'){
				//echo 'Charity';
			    $arr=[
			    	'method'=>'login_charity',
		            'P01'=> SERVICE_ACCOUNT_CHARITY_USERNAME_DEV,
		            'P02'=> SERVICE_ACCOUNT_CHARITY_PASSWORD_DEV,
			    ];
			    $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_CHARITY_LOGIN_API_URL,$arr);
			    $api_bt_data=json_decode($api_bt_result,true);
			    if($api_bt_data['ResponseCode']=='YES'){
			    $data['login_data']=$api_bt_data['ResponseMessage'];                     
			    } else {
						return $this->blade->view('errors.html.error_timeout');
				}
			} else if(!isset($data['suntag_data']['RegistrationType'])){

				//echo 'business';
			    $arr=[
			    	'method'=>'login_business',
		            'P01'=> SERVICE_ACCOUNT_USERNAME_DEV,
		            'P02'=> SERVICE_ACCOUNT_PASSWORD_DEV,
			    ];
			    $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
			    $api_bt_data=json_decode($api_bt_result,true);
			    if($api_bt_data['ResponseCode']=='YES'){
			    $data['login_data']=$api_bt_data['ResponseMessage'];                     
			    }else {
					return $this->blade->view('errors.html.error_timeout');
				}

			} else {
				//echo 'business';
			    $arr=[
			    	'method'=>'login_business',
		            'P01'=> SERVICE_ACCOUNT_USERNAME_DEV,
		            'P02'=> SERVICE_ACCOUNT_PASSWORD_DEV,
			    ];
			    $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
			    $api_bt_data=json_decode($api_bt_result,true);
			    if($api_bt_data['ResponseCode']=='YES'){
			    $data['login_data']=$api_bt_data['ResponseMessage'];                     
			    } else {
					return $this->blade->view('errors.html.error_timeout');
				}
			}

			//get payment settings. 
		       $arr=[
		         'method'=>'get_checkout_methods_byid',
		            'P01'=> $data['login_data']['SessionID'],
		            'P02'=> $data['suntag_data']['client_record_id'],
		       ];
		       $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
		       $api_bt_data=json_decode($api_bt_result,true);
		       $data['selected_settings_data']='';
		       if($api_bt_data['Success']=='YES'){
		       $api_bt_data['ResponseMessage'][0] =isset($api_bt_data['ResponseMessage'][0]) ? $api_bt_data['ResponseMessage'][0] : '';
		       $data['selected_settings_data']=$api_bt_data['ResponseMessage'][0];
		       } 
		       	
				if($data['suntag_data']['tag']=="MERCHANT"){
					$this->session->set_userdata('client_record_id' , $data['suntag_data']['client_record_id']);
					$this->session->set_userdata('client_id' , $data['suntag_data']['client_id']);
					$this->session->set_userdata('tag' , $data['suntag_data']['tag']);
					$this->session->set_userdata('key' , $data['suntag_data']['merchant_key']);
					$this->session->set_userdata('dba_name' , $data['suntag_data']['dba_name']);
					if(isset($amount)){
						$this->session->set_userdata('amount' , $amount);
					}
					if($data['suntag_data']['registration_status']=="Pending"){//pending...
						$data['sts']='Pending';
					} else if($data['suntag_data']['registration_status']=="Rejected"){//rejected
						$data['sts']='Rejected';
					} else if($data['suntag_data']['registration_status']=="For Verification"){//pending for verification
						//null or not yet finished in registration //F
						$data['sts']='For Verification';
					}else if($data['suntag_data']['registration_status']=="Inactive"){//inactive
						$data['sts']='Inactive';						
					}


				} else {
					$this->session->set_userdata('client_record_id' , $data['suntag_data']['client_record_id']);
					//$this->session->set_userdata('client_id' , $data['suntag_data']['client_id']);
					$this->session->set_userdata('tag' , $data['suntag_data']['tag']);	
					$this->session->set_userdata('kyc_type' , $data['suntag_data']['type']);	
					$this->session->set_userdata('mobile_suntag' , $data['suntag_data']['customer_mobile']);
					$this->session->set_userdata('dba_name' , $data['suntag_data']['dba_name']);
					if(isset($amount)){
						$this->session->set_userdata('amount' , $amount);
					}

					if($data['suntag_data']['status']=="Inactive"){//inactive...
						$data['sts']='Inactive';
					} 

				}

			//dd($data);
			// if($data['suntag_data']['RegistrationType']=='Charity'){
			// 	return $this->blade->view('suncash-me-charity',$data);
			// } else if(!isset($data['suntag_data']['RegistrationType'])){
			// 	return $this->blade->view('suncash-me',$data);
			// }else if($data['suntag_data']['RegistrationType']=='CUSTOMER'){
			// 	return $this->blade->view('suncash-me',$data);
			// // 	return $this->blade->view('errors.html.error_timeout');
			// }else {
			// 	return $this->blade->view('suncash-me',$data);
			// 	// return $this->blade->view('errors.html.error_timeout');
			// }	
			
			return $this->blade->view('suncashme-amount-qr',$data);
		} else {

			return $this->blade->view('main');
		}
	}
	public function referral($referral_user=null){
			if(empty($referral_user)){
				dd("No valid referral.");
			}

			$data['sts']="Approved";
			$auth_key=$this->assetHelper->create_tokken();
			//dd($auth_key);
			if(empty($auth_key)){
				return $this->blade->view('errors.html.error_timeout');
			}
			//dd($auth_key);
		    $arr=[
		    	'method'=>'get_profile_via_suntag',
	            'P01'=> $referral_user,
	            'P02'=>$auth_key['temp_auth'],
	            'return_mode'=>'json'
	            //'P02'=>$this->security->xss_clean($this->input->post('payroll_id')),
		    ];
		    //$arr_tosend['url']="get_payroll_listing?".http_build_query($arr);

		    // dd($arr);
		    $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_API_URL,$arr);
		  		
		    $api_bt_data=json_decode($api_bt_result,true);
		    // dd($api_bt_result);
		    //dd($api_bt_data);
		    $data['suntag_data'] ='';
			$data['referer']='';
	        if($api_bt_data['ResponseCode']=='0000'){
	         	$data['suntag_data']=$api_bt_data['ResponseMessage'];
				$data['referer']=$referral_user;
	        }
		    $arr=[
				'method'=>'get_all_active_countries',
				'return_mode'=>'json'
			];
		    $a=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr);
			$ax=json_decode($a,true);
	        if($ax['ResponseCode']=='0000'){
	         	$data['country']=$ax['ResponseMessage'];
	        }
		return $this->blade->view('suncash-me-referral',$data);
	}
	public function referral_process(){
		$required_input=[
			"mobile",
			"referer",
			"hc",
		];
		$required_input_label=[
			"mobile"=>"Mobile",
			"rereferer"=>"Referral",
			"hc"=>"Country",
		];
		$validated_fields=$this->assetHelper->checkpostfields($required_input,$_POST,$required_input_label);
			$arr=[   
				 'link'=>SUNCASH_CUSTOMER_PORTAL_URL."?mobile=".$_POST['mobile']."&referer=".$_POST['referer']."&country=".$_POST['hc'],
				 'success'=>true
			];
			  
			echo json_encode($arr);
			exit;  



	}

	public function faq(){//method then routefind
		$this->blade->view('faq');
	}
	public function terms(){//method then routefind
		$this->blade->view('terms');
	}	
	public function contacts(){//method then routefind
		$this->blade->view('contact');
	}	
	public function scan_qr_form(){//method then routefind

		$data['amount_to_process'] = $_POST['other_data']['amount_to_process'];
		$this->blade->view('scan_qr_form',$data);
	}		
}