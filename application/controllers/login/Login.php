<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {// put filename of controller on class name 

	function __construct(){//where can put dynamic functions and models....
      parent::__construct();
   		$this->session->flashdata('msg');
   		//load model.
      	$this->load->model('core/db');//for transactions and other eloquent db features
   		//$this->load->model('user');
   		//$this->load->model('Value_sets_global','global');
   		$this->assetHelper = new AssetHelper\AssetHelper();
	}
	public function index(){//method then routefind
		if(isset($_SESSION['post_data'])){
		if($_POST){
			//dd($_POST);
			$mobile = $this->clean(strtolower($this->security->xss_clean($this->input->post('mobile'))));
			$pin = $this->security->xss_clean($this->input->post('pin'));	

				
			if($pin=='' || $mobile==''){
				    $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Incorrect Username/Password. </div>');					
		            redirect('wallet/login');					
			}	



		    $arr=[
		    	'method'=>'login_mobile_portal',
	            'P01'=> md5($pin),
	            'P02'=>  $mobile,
	            'return_mode'=>'json'
		    ];
		    //$arr_tosend['url']="login_business?".http_build_query($arr);

		    //dd($arr_tosend);
		    $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr);
		    //dd($api_bt_result);
		    $api_bt_data=json_decode($api_bt_result,true);
		    //dd($api_bt_data);
		    $data['login_data']='';
		    if($api_bt_data['ResponseCode']=='0000'){
		    $data['login_data']=$api_bt_data['ResponseMessage'];                     
		    }


            //dd($data['login_data']);
			//check if exist.
			if(empty($data['login_data'])){
				    $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Incorrect Username/Password. </div>');					
		            redirect('wallet/login');				
			} else {
				


					//initialize session			
					$this->session->set_userdata('SessionID' , $data['login_data']['SessionID']);
					//$this->session->set_userdata('fee_data' , $data['fee_data']);
					$this->session->set_userdata('customer_info' , $data['login_data']);
					//dd($_SESSION);
					//dd($this->session);
					/*array:12 [
					  "CustomerName" => "maner puyawan"
					  "MobileNumber" => "19177949577"
					  "SessionID" => "00fd48466338ed7f60f18338ca673b7e"
					  "CustomerId" => "4"
					  "PinoyWalletCardId" => "3"
					  "CardBalance" => "2811.91"
					  "ImageURL" => "http://dev.mysuncash.com/desktop/cardholders/images/customer/prod/sc_1536051971218.jpg"
					  "Type" => "pending"
					  "Transaction_limit" => "500.00"
					  "Sender_id" => "151"
					  "kyc_status" => "CARD_EXPIRED"
					  "RequireUpdatePin" => "YES"
					]*/
					//dd($_SESSION);
					//check if same suntag and customer
					if($this->session->userdata('suntag_shortcode')==$this->session->userdata('customer_info')['CustomerTag']){
						$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Cant transact with same suntag. </div>');
						redirect('wallet/login');	
					}
					
					redirect('payment/wallet');		
					
			
			}	
		}		
		$this->blade->view('login.login');//for blade view calling
		} else {
			redirect('wallet/login');
		}
	}

	public function index_charity(){//method then routefind
		if(isset($_SESSION['post_data'])){
		if($_POST){
			//dd($_POST);
			$mobile = $this->clean(strtolower($this->security->xss_clean($this->input->post('mobile'))));
			$pin = $this->security->xss_clean($this->input->post('pin'));	

				
			if($pin=='' || $mobile==''){
				    $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Incorrect Username/Password. </div>');					
		            redirect('wallet/login-charity');					
			}	



		    $arr=[
		    	'method'=>'login_mobile_portal',
	            'P01'=> md5($pin),
	            'P02'=>  $mobile,
	            'return_mode'=>'json'
		    ];
		    //$arr_tosend['url']="login_business?".http_build_query($arr);

		    //dd($arr_tosend);
		    $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr);
		    //dd($api_bt_result);
		    $api_bt_data=json_decode($api_bt_result,true);
		    //dd($api_bt_data);
		    $data['login_data']='';
		    if($api_bt_data['ResponseCode']=='0000'){
		    $data['login_data']=$api_bt_data['ResponseMessage'];                     
		    }


            //dd($data['login_data']);
			//check if exist.
			if(empty($data['login_data'])){
				    $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Incorrect Username/Password. </div>');					
		            redirect('wallet/login-charity');				
			} else {
				



					//initialize session			
					$this->session->set_userdata('SessionID' , $data['login_data']['SessionID']);
					//$this->session->set_userdata('fee_data' , $data['fee_data']);
					$this->session->set_userdata('customer_info' , $data['login_data']);
					//dd($this->session);
					/*array:12 [
					  "CustomerName" => "maner puyawan"
					  "MobileNumber" => "19177949577"
					  "SessionID" => "00fd48466338ed7f60f18338ca673b7e"
					  "CustomerId" => "4"
					  "PinoyWalletCardId" => "3"
					  "CardBalance" => "2811.91"
					  "ImageURL" => "http://dev.mysuncash.com/desktop/cardholders/images/customer/prod/sc_1536051971218.jpg"
					  "Type" => "pending"
					  "Transaction_limit" => "500.00"
					  "Sender_id" => "151"
					  "kyc_status" => "CARD_EXPIRED"
					  "RequireUpdatePin" => "YES"
					]*/

					redirect('paymentcharity/wallet');		
					
			
	}	}		
		$this->blade->view('login.login-charity');//for blade view calling
		} else {
			redirect('wallet/login-charity');
		}
	}
    protected function generate_sessionid() {
        do {
            $session_id = md5(uniqid(time(), true));
            $check = DB::select('select id from sessions where session_id="' . $session_id .'"');
        } while (isset($check['id']));
        return $session_id;
    }
    
    protected function create_session($user_id) {
        $login_date = date('Y-m-d H:i:s');
        $session_id = $this->generate_sessionid();
        $insert_data = array(
            'session_id'    => $session_id,
            'login_date'    => $login_date,
            'last_seen'     => $login_date,
            'user_id'       => $user_id,
        );
        //$this->db->insert('sessions', $insert_data);
		DB::table('sessions')->insert($insert_data);
        return $session_id;        
    }
    public function clean($string) {
    	// $this->assetHelper->session_checker();
        $string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.
        return preg_replace('/[^A-Za-z0-9]/', '', $string); // Removes special chars.
	}
	//cc_login
	public function customer_login(){//method then routefind
		$arr=[
			'method'=>'get_all_active_countries',
			'return_mode'=>'json'
		];
		$a=$this->assetHelper->api_requestv2(SUNCASH_CUSTOMER_API_URL,$arr);
		$ax=json_decode($a,true);
		if($ax['ResponseCode']=='0000'){
			$data_country['country']=$ax['ResponseMessage'];
		}		

		$this->blade->view('cc_login',$data_country);
	}		
	public function cc_login_process(){//method then routefind
		if(isset($_SESSION['post_data'])){
		if($_POST){
			// dd($_POST);
			$mobile = $this->clean(strtolower($this->security->xss_clean($this->input->post('s_mobile_number'))));
			$pin = $this->security->xss_clean($this->input->post('pin'));	
	
			if($pin=='' || $mobile==''){
				    $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Incorrect Username/Password. </div>');					
		            redirect('payment/cc_login_page');					
			}	

		    $arr=[
		    	'method'=>'login_mobile_portal',
	            'P01'=> md5($pin),
	            'P02'=>  $mobile,
	            'return_mode'=>'json'
		    ];
		    //$arr_tosend['url']="login_business?".http_build_query($arr);

		    //dd($arr_tosend);
		    $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr);
		    // dd($api_bt_result);
		    $api_bt_data=json_decode($api_bt_result,true);
		    //dd($api_bt_data);
		    $data['login_data']='';
		    if($api_bt_data['ResponseCode']=='0000'){
		    $data['login_data']=$api_bt_data['ResponseMessage'];                     
		    }


            //dd($data['login_data']);
			//check if exist.
			if(empty($data['login_data'])){
				    $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Incorrect Username/Password. </div>');					
		            redirect('payment/cc_login_page');				
			} else {
				
				//initialize session			
				$this->session->set_userdata('SessionID_cc' , $data['login_data']['SessionID']);
				// $this->session->set_userdata('CustomerId_cc' ,$data['login_data']['CustomerId']);
				$this->session->set_userdata('customer_info_cc' , $data['login_data']);
				//dd($data['login_data']);
				$arr=[
					'method'=>'get_customer_creditcard',
					'P01'=> $this->session->userdata('SessionID_cc'),
					'P02'=> $data['login_data']['CustomerId'],
					'P03'=> '',
					'return_mode' => 'json'
				];
				// dd($arr);
				$api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr);
				$api_bt_data=json_decode($api_bt_result,true);
				 //dd($api_bt_data);
				$data['cc_data']='';
				if($api_bt_data['ResponseCode']=='0000'){
					$data['cc_data']=$api_bt_data['ResponseMessage'];
				}
				//prefer cards/
				$html='';
				// if(!empty($ccdata)){
				// 	foreach($ccdata as $ccdata_val){
				// 		if($ccdata_val['is_verified']==1){
				// 			//$html.="<option value='{$ccdata_val['id']}'>".str_pad($ccdata_val['card_last_four_digits'], 12, "*", STR_PAD_LEFT)." (".$ccdata_val['card_type'].")</option>";
				// 		}
				// 	}
				// }
				if($this->session->userdata('suntag_shortcode')==$this->session->userdata('customer_info_cc')['CustomerTag']){
					$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Cant transact with same suntag. </div>');
					redirect('payment/cc_login_page');	
				}
				
				redirect('payment/cc_wallet');												
			}	
		}	
		$this->blade->view('cc_login');//for blade view calling
		} else {
			redirect('payment/cc_login_page');
		}
	}
	public function index_sanddollar(){//method then routefind
		if(isset($_SESSION['post_data'])){
		if($_POST){
			//dd($_POST);
			$mobile = $this->clean(strtolower($this->security->xss_clean($this->input->post('mobile'))));
			$pin = $this->security->xss_clean($this->input->post('pin'));	

				
			if($pin=='' || $mobile==''){
				    $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Incorrect Username/Password. </div>');					
		     		redirect('payment/wallet_sanddollar');						
			}	

		    $arr=[
		    	'method'=>'login_mobile_portal',
	            'P01'=> md5($pin),
	            'P02'=>  $mobile,
	            'return_mode'=>'json'
		    ];
		    //$arr_tosend['url']="login_business?".http_build_query($arr);

		    //dd($arr_tosend);
		    $api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr);
		    // dd($api_bt_result);
		    $api_bt_data=json_decode($api_bt_result,true);
		    //dd($api_bt_data);
		    $data['login_data_s']='';
		    if($api_bt_data['ResponseCode']=='0000'){
		    $data['login_data_s']=$api_bt_data['ResponseMessage'];                     
		    }

			//check if exist.
			if(empty($data['login_data_s'])){
				    $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Incorrect Username/Password. </div>');					
		            redirect('sanddollar/login');				
			} else {
				



					//initialize session			
					$this->session->set_userdata('Sand_SessionID' , $data['login_data_s']['SessionID']);
					//$this->session->set_userdata('fee_data' , $data['fee_data']);
					$this->session->set_userdata('sand_customer_info' , $data['login_data_s']);
					//dd($this->session);
					/*array:12 [
					  "CustomerName" => "maner puyawan"
					  "MobileNumber" => "19177949577"
					  "SessionID" => "00fd48466338ed7f60f18338ca673b7e"
					  "CustomerId" => "4"
					  "PinoyWalletCardId" => "3"
					  "CardBalance" => "2811.91"
					  "ImageURL" => "http://dev.mysuncash.com/desktop/cardholders/images/customer/prod/sc_1536051971218.jpg"
					  "Type" => "pending"
					  "Transaction_limit" => "500.00"
					  "Sender_id" => "151"
					  "kyc_status" => "CARD_EXPIRED"
					  "RequireUpdatePin" => "YES"
					]*/

					redirect('payment/wallet_sanddollar');		
					
			
				}	
			}		
		$this->blade->view('login.login-sanddollar');//for blade view calling
		} else {
			redirect('sanddollar/login');
		}
	}

}