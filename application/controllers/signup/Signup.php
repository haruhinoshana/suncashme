<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Signup extends CI_Controller {// put filename of controller on class name 

	function __construct(){//where can put dynamic functions and models....
      parent::__construct();
   	   $this->session->flashdata('msg');
   		//load model.
       $this->load->model('core/db');//for transactions and other eloquent db features
   		//$this->load->model('user');
   		//$this->load->model('Value_sets_global','global');
       $this->assetHelper = new AssetHelper\AssetHelper();
	}
	public function index(){//method then routefind
    //get menus via api....
    /*business type*/
    $arr=[
        'method'=>'get_business_type',
        'return_mode'=>'json'
    ];
    $arr_tosend['url']="?".http_build_query($arr);
    $api_bt_result=$this->assetHelper->api_get(SUNCASH_API_URL,$arr_tosend);
    $api_bt_data=json_decode($api_bt_result,true);
    $data['business_type']='';
    if($api_bt_data['ResponseCode']=='0000'){
    $data['business_type']=$api_bt_data['ResponseMessage'];                     
    } 

    /*get_merchant_type_list*/
    $arr=[
        'method'=>'get_merchant_type_list',
        'return_mode'=>'json'
    ];
    $arr_tosend['url']="?".http_build_query($arr);
    $api_mtl_result=$this->assetHelper->api_get(SUNCASH_API_URL,$arr_tosend);
    $api_mtl_data=json_decode($api_mtl_result,true);
    $data['merchant_type_list']='';
    if($api_mtl_data['ResponseCode']=='0000'){
    $data['merchant_type_list']=$api_mtl_data['ResponseMessage'];                     
    }

    /*get_island*/
    $arr=[
        'method'=>'get_island',
        'return_mode'=>'json'
    ];
    $arr_tosend['url']="?".http_build_query($arr);
    $api_gi_result=$this->assetHelper->api_get(SUNCASH_API_URL,$arr_tosend);
    $api_gi_data=json_decode($api_gi_result,true);
    $data['island_list']='';
    if($api_gi_data['ResponseCode']=='0000'){
    $data['island_list']=$api_gi_data['ResponseMessage'];                     
    } 

    /*get_countries*/
    $arr=[
        'method'=>'get_countries',
        'return_mode'=>'json'
    ];
    $arr_tosend['url']="?".http_build_query($arr);
    $api_gc_result=$this->assetHelper->api_get(SUNCASH_API_URL,$arr_tosend);
    $api_gc_data=json_decode($api_gc_result,true);
    $data['country_list']='';
    if($api_gc_data['ResponseCode']=='0000'){
    $data['country_list']=$api_gc_data['ResponseMessage'];                     
    } 

    /*get_service_category_list*/
    $arr=[
        'method'=>'get_service_category_list',
        'return_mode'=>'json'
    ];
    $arr_tosend['url']="?".http_build_query($arr);
    $api_gscl_result=$this->assetHelper->api_get(SUNCASH_API_URL,$arr_tosend);
    $api_gscl_data=json_decode($api_gscl_result,true);
    $data['service_category_list']='';
    if($api_gscl_data['ResponseCode']=='0000'){
    $data['service_category_list']=$api_gscl_data['ResponseMessage'];                     
    } 
    //dd($data);

		$this->blade->view('merchant.merchant-signup',$data);//for blade view calling
	}

  public function process(){
    $user_name=$this->security->xss_clean($this->input->post('user_name'));
    $password=$this->security->xss_clean($this->input->post('password'));
    $cpassword=$this->security->xss_clean($this->input->post('cpassword'));
    $dba_name=$this->security->xss_clean($this->input->post('dba_name'));
    $business_type=$this->security->xss_clean($this->input->post('business_type'));
    $suntag_shortcode=$this->security->xss_clean($this->input->post('suntag_shortcode'));
    $service_categories=$this->security->xss_clean($this->input->post('service_categories'));
    $name_of_parent_company=$this->security->xss_clean($this->input->post('name_of_parent_company'));
    $business_license_no=$this->clean($this->security->xss_clean($this->input->post('business_license_no')));
    $business_shortcode=$this->security->xss_clean($this->input->post('business_shortcode'));
    $company_address=$this->security->xss_clean($this->input->post('company_address'));
    $island=$this->clean($this->security->xss_clean($this->input->post('island')));
    $country=$this->clean($this->security->xss_clean($this->input->post('country')));
    $head_office_telephone_no1=$this->clean($this->security->xss_clean($this->input->post('head_office_telephone_no1')));
    $business_email_address=$this->security->xss_clean($this->input->post('business_email_address'));
    $primary_contact=$this->clean($this->security->xss_clean($this->input->post('primary_contact')));
    $p_telephone_no=$this->clean($this->security->xss_clean($this->input->post('p_telephone_no')));
    $p_email_address=$this->security->xss_clean($this->input->post('p_email_address'));
    $legal_name=$this->security->xss_clean($this->input->post('legal_name'));
    $head_office_telephone_no2=$this->clean($this->security->xss_clean($this->input->post('head_office_telephone_no2')));
    $merchant_type=$this->security->xss_clean($this->input->post('merchant_type'));
    $business_website=$this->security->xss_clean($this->input->post('business_website'));
    $secondary_contact=$this->clean($this->security->xss_clean($this->input->post('secondary_contact')));
    $s_telephone_no=$this->clean($this->security->xss_clean($this->input->post('s_telephone_no')));
    $s_email_address=$this->security->xss_clean($this->input->post('s_email_address'));
    $name_of_primary_guarantor=$this->security->xss_clean($this->input->post('name_of_primary_guarantor'));
    $name_of_secondary_guarantor=$this->security->xss_clean($this->input->post('name_of_secondary_guarantor'));


    $arr=[
        'method'=>'suntag_shortcode_availability',
        'P01'=>$suntag_shortcode,
        'return_mode'=>'json'
    ];
    $arr_tosend['url']="?".http_build_query($arr);
    $api_ssa_result=$this->assetHelper->api_get(SUNCASH_API_URL,$arr_tosend);
    $api_ssa_data=json_decode($api_ssa_result,true);
    //$data['service_category_list']='';
    if($api_ssa_data['ResponseCode']=='1000'){
      $msg_arr=[
        'msg'=>$api_ssa_data['ResponseMessage'],
        'success'=>false
      ];   

      exit(json_encode($msg_arr));                 
    } 

    $arr=[
    "method"=>'register_merchant',  
    "P01"=>$user_name,  
    "P02"=>$password, 
    "P03"=>$dba_name, 
    "P04"=>$business_type, 
    "P05"=>$suntag_shortcode ,
    "P06"=>$service_categories,
    "P07"=>$name_of_parent_company,
    "P08"=>$business_license_no,
    "P09"=>$business_shortcode,
    "P10"=>$company_address,
    "P11"=>$island,
    "P12"=>$country,
    "P13"=>$head_office_telephone_no1,
    "P14"=>$business_email_address,
    "P15"=>$primary_contact,
    "P16"=>$p_telephone_no,
    "P17"=>$p_email_address,
    "P18"=>$legal_name,
    "P19"=>$head_office_telephone_no2,
    "P20"=>$merchant_type,
    "P21"=>$business_website,
    "P22"=>$secondary_contact,
    "P23"=>$s_telephone_no,
    "P24"=>$s_email_address,
    "P25"=>$name_of_primary_guarantor,
    "P26"=>$name_of_secondary_guarantor,
    'return_mode'=>'json'
    ];

    $arr_tosend['url']="?".http_build_query($arr);
    //dd($arr_tosend['url']);
    $api_rm_result=$this->assetHelper->api_get(SUNCASH_API_URL,$arr_tosend);
    $api_rm_data=json_decode($api_rm_result,true);
    //dd($api_rm_data);
    //$data['service_category_list']='';
    if($api_rm_data['ResponseCode']=='1000'){
      $msg_arr=[
        'msg'=>$api_rm_data['ResponseMessage'],
        'success'=>false
      ];   

      exit(json_encode($msg_arr));                 
    } else {
      $this->session->set_userdata('email_registered' , $business_email_address);
      $msg_arr=[
        'msg'=>$api_rm_data['ResponseMessage'],
        'success'=>true
      ];   

      exit(json_encode($msg_arr));      
    }

  }
  public function success(){
    $this->blade->view('merchant.merchant-signup-success'); 
  }
  public function verify(){
    $this->blade->view('merchant.merchant-signup-verification');    
  }
  public function process_verify(){
    $email=$this->security->xss_clean($this->input->post('email'));
    $verification_code=$this->clean($this->security->xss_clean($this->input->post('verification_code')));

    $arr=[
      "method"=>"verify_registration",
      "P01"=>$email,
      "P02"=>$verification_code,
      'return_mode'=>'json'
    ];


    $arr_tosend['url']="?".http_build_query($arr);
    //dd($arr_tosend['url']);
    $api_rm_result=$this->assetHelper->api_get(SUNCASH_API_URL,$arr_tosend);
    $api_rm_data=json_decode($api_rm_result,true);
    //dd($api_rm_data);
    //$data['service_category_list']='';
    if($api_rm_data['ResponseCode']=='1000'){
      $msg_arr=[
        'msg'=>$api_rm_data['ResponseMessage'],
        'success'=>false
      ];   

      exit(json_encode($msg_arr));                 
    } else {
      $msg_arr=[
        'msg'=>$api_rm_data['ResponseMessage'],
        'success'=>true
      ];   

      exit(json_encode($msg_arr));      
    }

  }

  public function verifysuntag(){
    $suntag_shortcode=$this->security->xss_clean($this->input->post('suntag'));  
 
    $arr=[
        'method'=>'suntag_shortcode_availability',
        'P01'=>$suntag_shortcode,
        'return_mode'=>'json'
    ];
    $arr_tosend['url']="?".http_build_query($arr);
    $api_ssa_result=$this->assetHelper->api_get(SUNCASH_API_URL,$arr_tosend);
    $api_ssa_data=json_decode($api_ssa_result,true);
    //$data['service_category_list']='';
    if($api_ssa_data['ResponseCode']=='1000'){
      $msg_arr=[
        'msg'=>$api_ssa_data['ResponseMessage'],
        'success'=>false
      ];   

      exit(json_encode($msg_arr));                 
    } else if($api_ssa_data['ResponseCode']=='0000'){
      $msg_arr=[
        'msg'=>$api_ssa_data['ResponseMessage'],
        'success'=>true
      ];   

      exit(json_encode($msg_arr));               
    }


  }

  public function clean($string) {
      $string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.
      return preg_replace('/[^A-Za-z0-9]/', '', $string); // Removes special chars.
  }  

}