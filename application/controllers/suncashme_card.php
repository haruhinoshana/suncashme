<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//namespace AmazonPay;
class Suncashme_card extends CI_Controller {// put filename of controller on class name 

	function __construct(){//where can put dynamic functions and models....
		parent::__construct();
		include 'vendor/autoload.php';
   		$this->session->flashdata('msg');
   		//load model.
      	$this->load->model('core/db');//for transactions and other eloquent db features
      	$this->load->model('core/Qr');
 		$this->assetHelper = new AssetHelper\AssetHelper();
		//session checker customize
		$this->assetHelper->session_checker();
	}

	public function cc_wallet(){//method then routefind //get fee
		if($this->session->userdata('suntag_shortcode')==$this->session->userdata('customer_info_cc')['CustomerTag']){
			$this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">Cant transact with same suntag. </div>');
			redirect('wallet/login');	
		}
		$data['payment_data'] = $this->session->userdata('post_data');
		if(isset($_SESSION['SessionID_cc'])){
		$amount = 	str_replace( ',', '', $this->session->userdata('post_data')['amount']);
	    $arr=[
	    	'method'=>'get_business_fee',
            'P01'=> $amount,
            'P02'=>  'SUNCASHME_MONEYTRANSFER',
            'P03'=>	'',//$this->session->userdata('suntag_shortcode'),
            'return_mode'=>'json'
	    ];
	    //$arr_tosend['url']="login_business?".http_build_query($arr);
	    //dd($_SESSION);
	    //dd($arr);
	    $api_fee_result=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr);
	    //dd($api_fee_result);
	    $api_fee_data=json_decode($api_fee_result,true);
	    //dd($api_bt_data);
	    $data['fee_data']='';
	    if($api_fee_data['ResponseCode']=='0000'){
	    $data['fee_data']=$api_fee_data['ResponseMessage'];
	    }	

		// cc
		$trans_type = $_SESSION['tag']=='CUSTOMER' ? 'CUSTOMER' : 'BUSINESS';
		$user_type = $_SESSION['tag']=='CUSTOMER' ? $_SESSION['suntag_shortcode'] : $_SESSION['merchant_key'];
		$arr=[
			'method'=>'get_payment_fee',
			'P01'=> $user_type,//mercahntkey
			'P02'=>	$amount,
			'P03'=> 'SUNCASHMECARD',
			'P04'=> $trans_type,
			'return_mode'=>'json'
		];
		$api_fee_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
		// dd($api_fee_result);
		$api_fee_data=json_decode($api_fee_result,true);
		$data['convenience_data']='';
		if($api_fee_data['Success']=='YES'){
		$data['convenience_data']=$api_fee_data['ResponseMessage'];                 
		}
		$is_merch=$data['convenience_data']['iscard_fee_on_merch'];
		$ccfee =$data['convenience_data']['total_convenience_fee'];
		if($is_merch>0){
			$ccfee ="0.00";
		}
		$data['ccfee']=$ccfee ; //show value in display


	    $data['Balance'] = $this->assetHelper->get_balance($_SESSION['SessionID_cc']);
		//get cc data if have card loaded.
		    $arr=[
			'method'=>'get_customer_creditcard',
			'P01'=> $this->session->userdata('SessionID_cc'),
			'P02'=> $this->session->userdata('customer_info_cc')['CustomerId'],
			'P03'=> '',
			'return_mode' => 'json'
			];
			// dd($arr);
			$api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr);
			$api_bt_data=json_decode($api_bt_result,true);
		// dd($api_bt_data);
			$data['cc_data']='';
			if($api_bt_data['ResponseCode']=='0000'){
			$data['cc_data']=$api_bt_data['ResponseMessage'];
			}


		//site verify
		$arr=[
			'merchant'=>CENPOST_MERCHANT_ID,
			'secretkey'=>CENPOST_MERCHANT_SECRETKEY,
			'email'=>'suncashme@gmail.com',
			'ip'=>'127.0.0.1',
		];
		$verify_post=$this->assetHelper->api_requestv2(CENPOS_SITEVERIFY_URL,$arr);
		$verify_post=json_decode($verify_post,true);	
		//dd($verify_post);
		$data['verify_params']=$verify_post;
		$data['customer_id']=$this->session->userdata('customer_info_cc')['CustomerId'];//."_".date('YmdHis')


			// dd($data);
	    // dd($data['fee_data']);
		$this->blade->view('customer-card-page',$data);
		} else {
		redirect('');
		}
	}


	public function process_fee_data(){
		
			$amount = 	str_replace( ',', '', $this->session->userdata('post_data')['amount']);
			$trans_type = $_SESSION['tag']=='CUSTOMER' ? 'CUSTOMER' : 'BUSINESS';
			$user_type = $_SESSION['tag']=='CUSTOMER' ? $_SESSION['suntag_shortcode'] : $_SESSION['merchant_key'];
			$arr=[
				'method'=>'get_payment_fee',
				'P01'=> $user_type,//mercahntkey
				'P02'=>	$amount,
				'P03'=> 'CREDITCARD',
				'P04'=> $trans_type,
				'return_mode'=>'json'
			];
			$api_fee_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
			// dd($api_fee_result);
			$api_fee_data=json_decode($api_fee_result,true);
			$data['convenience_data']='';
			if($api_fee_data['Success']=='YES'){
			$data['convenience_data']=$api_fee_data['ResponseMessage'];                 
			}
			$is_merch=$data['convenience_data']['iscard_fee_on_merch'];
			$tf=$data['convenience_data']['transaction_fee'];
			$pf=$data['convenience_data']['processing_fee'];
			$ccfee=$data['convenience_data']['total_convenience_fee'];
			$total=$data['convenience_data']['total_due'];
			if($is_merch>0){
				$ccfee =0.00;
			}


		    $arr=[
		    	'method'=>'get_business_fee',
	            'P01'=> $this->input->post('amount'),
	            'P02'=>  'SUNCASHME_MONEYTRANSFER',
	            'P03'=>	'',//$this->session->userdata('suntag_shortcode'),
	            'return_mode'=>'json'
		    ];
			$api_fee_result2=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr);
						// dd($api_fee_result2);
		    $api_fee_data=json_decode($api_fee_result2,true);
		    $data['fee_data']='';
		    if($api_fee_data['ResponseCode']=='0000'){
		    	$data['fee_data']=$api_fee_data['ResponseMessage'];
			}
			//change session for amount
			$this->session->set_userdata('amz_amount',$this->input->post('amount'));
			$arr=[
				// 'convenience_data'=>$data['convenience_data'],
				'pf'=>$pf,
				'tf'=>$tf,
				'ccfee'=>$ccfee,
				'total'=>$total,
				'fee_data'=>$data['fee_data'],
				'success'=>true
			];

			echo json_encode($arr);
			exit;
	}
	public function get_fees_customer(){
		$arr=[
			'method'=>'get_fees_customer',
			'P01'=> $this->input->post('amount'),
			'P02'=>  'LOAD',
			'P03'=>	'',//$this->session->userdata('suntag_shortcode'),
			'return_mode'=>'json'
		];
		//$arr_tosend['url']="login_business?".http_build_query($arr);

		//dd($arr);
		$api_fee_result=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr);
		//dd($api_fee_result);
		$api_fee_data=json_decode($api_fee_result,true);
		//dd($api_bt_data);
		$data['fee_data']='';
		if($api_fee_data['ResponseCode']=='0000'){
		$data['fee_data']=$api_fee_data['ResponseMessage'];
		}
		$arr=[
			'fee_data'=>$data['fee_data'],
			'success'=>true
		];

		echo json_encode($arr);
		exit;
	}


	public function pending_email($arr){
		$to = $arr['email'];
		$subj = $arr['title'];
		$attachment_arr='';
        $template_html = $arr['view']; //views/templates/mail/
		//email receipt..
		$this->assetHelper->send_email($arr,$to,$subj,$template_html,$attachment_arr);		
	}

	public function pending_email_cash($arr,$image_attachment){
		$to = $arr['email'];
		$subj = $arr['title'];
		$attachment_arr='';
        $template_html = $arr['view']; //views/templates/mail/

		//email receipt..
		$this->assetHelper->send_email($arr,$to,$subj,$template_html,$attachment_arr,$image_attachment);		
	}






	public function confirm_email($arr){
		// dd($arr);
		$to = $arr['email'];
		$subj = $arr['title'];
		$attachment_arr='';
		$template_html = $arr['view']; //views/templates/mail/
		//dd($template_html);
		//email receipt..
		$this->assetHelper->send_email($arr,$to,$subj,$template_html,$attachment_arr);
	}


	
	public function clean($string) {
    	// $this->assetHelper->session_checker();
        $string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.
        return preg_replace('/[^A-Za-z0-9]/', '', $string); // Removes special chars.
	}
	public function link_card_form(){
			//site verify
			$arr=[
				'merchant'=>CENPOST_MERCHANT_ID,
				'secretkey'=>CENPOST_MERCHANT_SECRETKEY,
				'email'=>'suncashme@gmail.com',
				'ip'=>'127.0.0.1',
			];
			$verify_post=$this->assetHelper->api_requestv2(CENPOS_SITEVERIFY_URL,$arr);
			$verify_post=json_decode($verify_post,true);	
			//dd($verify_post);
		$data['verify_params']=$verify_post;
		$data['customer_id']=$this->session->userdata('CustomerId');//."_".date('YmdHis')
		$this->blade->view('customer.dashboard.add_card',$data); 
	}
	public function delete_linked_card(){
		$arr=[
		'method'=>'delete_customer_creditcard',
		'P01'=> $this->session->userdata('SessionID_cc'),
		'P02'=> $this->input->post('id'),
		'return_mode' => 'json'
		];
		// dd($arr);
		$api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr);
		$api_bt_data=json_decode($api_bt_result,true);
	// dd($api_bt_data);
		
		if($api_bt_data['ResponseCode']=='0000'){
		$arr=[  
			'msg'=>$api_bt_data['ResponseMessage'],
			'success'=>true
		];
		
		echo json_encode($arr);
		exit;        
		}  else {
		$arr=[  
			'msg'=>$api_bt_data['ResponseMessage'],
			'success'=>false
		];
		
		echo json_encode($arr);
		exit;  
		}
	}
	public function upload_requirements(){
		$data['id']=$_POST['other_data']['cid'];
		$data['v']=$_POST['other_data']['v'];
		$this->blade->view('requirement_uploads',$data); 
	}
	public function process_link_card(){
		$invoice = $_POST['customer_id'].''.strtotime(date('YmdHis'));
		$arr=[
				'amount'=>0.00,
				'invoicenumber'=>$_POST['customer_id'].''.strtotime(date('YmdHis')),
				'type'=>'Sale',
				'email'=>'suncashme@gmail.com',
				'secretkey'=>CENPOST_MERCHANT_SECRETKEY,
				'merchant'=>CENPOST_MERCHANT_ID,
				'tokenid'=>$_POST['tokenid'],
			];	
		$verifying_postp='';
		$vp='';
		$verifying_postp=$this->assetHelper->api_requestv2(CENPOS_SITEVERIFY_URL,$arr);
		//dd($verify_post2);
		$verifying_postp=json_decode($verifying_postp,true);     
		$vp=$verifying_postp['Data'];

			$arr_post=[
				'verifyingpost'=>$vp,
				'tokenid'=>$_POST['tokenid'],
			];
			$process_payment=$this->assetHelper->api_requestv2(CENPOS_PROCESS_URL,$arr_post);

		//save token
		$arr=[
		'secretkey'=>CENPOST_MERCHANT_SECRETKEY,
		'merchant'=>CENPOST_MERCHANT_ID,
		'customercode'=>$_POST['customer_id'],
		//'iscvv'=>true,
		//'isrecaptcha'=>false,
		'tokenid'=>$_POST['tokenid'],
		];
		//dd($arr);
		$verifying_post2='';
		$verify_post2=$this->assetHelper->api_requestv2(CENPOS_SITEVERIFY_URL,$arr);
		//dd($verify_post2);
		$verify_post2=json_decode($verify_post2,true);     
		//dd($verify_post2);
		$arr_post2=[
		'verifyingpost'=>$verify_post2['Data'],
		'tokenid'=>$_POST['tokenid'],
		];
		//dd($arr_post2);
		$process_payment2=$this->assetHelper->api_requestv2(CENPOS_CONVERTCRYPTO_URL,$arr_post2);
		$process_payment2=json_decode($process_payment2,true);
		$arr_post=[
		'method'=>'log_cenpos',
		'P01'=> $this->session->userdata('SessionID_cc'),
		'P02'=> $process_payment2,//data//request or response data
		'P03'=> 'RESPONSE',//status // "REQUEST" or "RESPONSE"
		'P04'=> CENPOS_SITEVERIFY_URL,//cenpos_method
		'P05'=> $this->session->userdata('customer_info_cc')['CustomerId'],
		'return_mode' => 'json'
		];
		$save_logs=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr_post);
		$_POST['card_number']=str_replace("*","",$_POST['card_number_link']);
		//dd($process_payment);
			if($process_payment2['Result']==0){
			$arr=[
			'method'=>'save_customer_creditcard',
			'P01'=> $this->session->userdata('SessionID_cc'),
			'P02'=> $this->input->post('customer_id'),
			'P03'=> $process_payment2['TokenId'],//TokenId
			'P04'=> $_POST['card_type_link'],//CardType
			'P05'=> str_replace('*','',$_POST['card_number_link']),//CardLastFourDigits
			'P06'=> substr($invoice, -6),//otp(invoice last 6 digits)
			'P07'=>'0',//fixed to zero 0
			'P08'=> $_POST['name_on_card_link'],//NameOnCard
			'return_mode' => 'json'
			];
			$api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr);
			//dd($api_bt_result);
			$api_bt_data=json_decode($api_bt_result,true);

			
			if($api_bt_data['ResponseCode']=='0000'){
			$arr=[  
				'msg'=>'Successfully linked.',//$api_bt_data['ResponseMessage'],
				'success'=>true
			];
			
			echo json_encode($arr);
			exit;        
			}  else {
			$arr=[  
				'msg'=>$api_bt_data['ResponseMessage'],
				'success'=>false
			];
			
			echo json_encode($arr);
			exit;  
			}
		}
	}
	public function process_verify_card(){
		$verifiction_code=hex2bin($_POST['v']);
		$post_verification_code=$_POST['verification'];
		//validate verification code.
		if($verifiction_code!=$post_verification_code){
		$arr=[  
			'msg'=>'Invalid Verification code.',
			'success'=>false
		];
		
		echo json_encode($arr);
		exit;  
		}

		$arr=[
		'method'=>'verify_customer_creditcard',
		'P01'=> $this->session->userdata('SessionID_cc'),
		'P02'=> $this->input->post('c'),
		'return_mode' => 'json'
		];
		$api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr);
		//dd($api_bt_result);
		$api_bt_data=json_decode($api_bt_result,true);

		
		if($api_bt_data['ResponseCode']=='0000'){
		$arr=[  
			'msg'=>$api_bt_data['ResponseMessage'],
			'success'=>true
		];
		
		echo json_encode($arr);
		exit;        
		}  else {
		$arr=[  
			'msg'=>$api_bt_data['ResponseMessage'],
			'success'=>false
		];
		
		echo json_encode($arr);
		exit;  
		}
	}	
	public function process_customer_cc_payment(){
		$amount = 	str_replace( ',', '', $_POST['amount']);
		$total_due = 	str_replace( ',', '', $_POST['total_due']);
		if(empty($_POST['select_card'])){

			$arr=[	
				'msg'=>'Please fill up all fields',
				'success'=>false
			];
			
			echo json_encode($arr);

		} 
		$post_data =$this->session->userdata('post_data');


		$arr=[
			'method'=>'validate_payment_reference_id',
			'P01'=> $_SESSION['customer_info_cc']['SessionID'],
			'P02'=> $_POST['reference_num'],
		];
		$api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr);
		//dd($api_bt_result);
		$api_bt_data=json_decode($api_bt_result,true);
		if($api_bt_data['Success']=='NO'){
			$arr=[	
				'msg'=>'Reference Id already Exist.',
				'success'=>false
			];
			
			echo json_encode($arr);
			exit;	       	
		}


		$trans_type = $_SESSION['tag']=='CUSTOMER' ? 'CUSTOMER_PAYMENT' : 'BUSINESS_PAYMENT';
		$user_type = $_SESSION['tag']=='CUSTOMER' ? $_SESSION['suntag_shortcode'] : $_SESSION['merchant_key'];


		$_POST['tf']=!empty($_POST['tf']) ? $_POST['tf'] : 0.00;//response
		$_POST['pf']=!empty($_POST['pf']) ? $_POST['pf'] : 0.00;//response		

		// is_enable_cc_fee ==isenable
		$arr_post=[
			'method'=>'processCreditCardPayment',
			'P01'=>$this->session->userdata('SessionID_cc'),//customerid,
			'P02'=>$_SESSION['client_record_id'],//$this->session->userdata('customer_info_cc')['CustomerId'],//customer id ng babayaran
			'P03'=>$amount,//reference_number/invoice 
			'P04'=>number_format($_POST['tf'],2,'.',''),//transaction fee 4%
			'P05'=>number_format($_POST['pf'],2,'.',''),//processing fee  .20
			'P06'=>$_POST['is_merch'],//if card fee is charged to merchant or customer
			'P07'=>$_POST['billpayfee'],//billpay fee
			'P08'=>0.00, //billpay vat
			'P09'=>$total_due,//total amount
			'P10'=>$trans_type, //CUSTOMER_PAYMENT or BUSINESS_PAYMENT
			'P11'=>'', //CENPOST_MERCHANT_SECRETKEY
			'P12'=>'',//CENPOST_MERCHANT_ID
			'P13'=>base64_decode($_POST['select_card']),//token
			'P14'=>$_POST['reference_num'],//orderid or invoice number in from cenpos
			'P15'=>'Sale',//Sale or Auth
			'P16'=>'suncashme-creditcard',//source checkout/payment/terminal/donation
			'P17'=>$user_type, //merchant key or customertag
			'P18'=>$_POST['notes'], 
			'P19'=>$_POST['card_num'], //card num
			'P20'=>$_POST['card_name'], //card name
			'P21'=>$_POST['card_type'], //card type
			'P22'=>$this->session->userdata('customer_info_cc')['CustomerId'], //payor id]
			'return_mode'=>'json'	
		];	
		// dd($arr_post);
		$card_process=$this->assetHelper->api_requestv2(SUNCASH_BUSINESS_LOGIN_API_URL,$arr_post);	

		$card_processx=json_decode($card_process,true);
		// dd($card_process);
		if($card_processx['ResponseCode']=="YES"){		
			$cenpos_ref_id =$card_processx['ResponseMessage']['ResponseData']['TransactionID'];

            $cfee_email = $_POST['is_merch'] ==1 ? $cfee_email=0.00 : $cfee_email=number_format($_POST['tf'],2,'.','')+number_format($_POST['pf'],2,'.','');
			//email merchant
			$arr_mail=[
				"customer_name"=>$_POST['card_name'],
				"amount"=>$amount,
				"transaction_id"=>$_POST['reference_num'],//cenpos
				"dba_name"=>$this->session->userdata('dba_name'),
				"creation_date"=>date('M d, Y'),
				"reference_num"=>$cenpos_ref_id,
				"notes"=>$_POST['notes'],
				"balance"=>'',
				"processing_fee"=>number_format($_POST['pf'],2,'.',''),
				"transaction_fee"=>number_format($_POST['tf'],2,'.',''),
				"fee"=>$_POST['billpayfee'],
				"vat"=>"0.00",
				"cfee"=>$cfee_email,
				"tfee"=>number_format($_POST['billpayfee'],2,'.',''),
				"total"=>number_format($total_due,2),  //amount +fee+vat
				"msg_above"=>"<h2>".$_POST['name']." paid <span class='amount'>".number_format($total_due,2)." BSD</span>. </h2>",
				"msg_bottom"=>"",
				"email"=>$this->session->userdata('email'),
				"view"=>'email_message_completed.blade.php',
				"title"=>'Completed Payment',
				"profile_pic" => $this->session->userdata("profile_pic"),
				"iscard_fee_on_merch"=>$_POST['is_merch']
			];
			// dd($arr_mail);
			$this->confirm_email($arr_mail);	

			$arr=[	
				'msg'=>$card_processx['ResponseMessage'],
				'reference'=>$cenpos_ref_id,
				'success'=>true
			];
	
			echo json_encode($arr);	
			exit;

		} else {

			$arr=[
				'msg'=>$card_processx['ResponseMessage'],
				'success'=>false
			];

			echo json_encode($arr);
			exit;
		}
	}
	public function process_verify_upload_requirements(){
		//dd($_FILES);
		$responses = [];
		if(!empty($_FILES)){
			if(!empty($_FILES['card_id_upload'])){
				$file_tmp= $_FILES['card_id_upload']['tmp_name'];
				$data = file_get_contents($file_tmp);
				$base64 = base64_encode($data);

				$arr=[
					'method'=>'customer_other_files',
					"P01"=> $this->session->userdata('SessionID_cc'),
					"P02"=> $this->session->userdata('customer_info_cc')['CustomerId'],
					"P03"=> "",
					"P04"=> "suncash-me",
					"P05"=> "card",
					"P06"=> "0",
					"P07"=> "",
					"P08"=> $base64,//image
					"P09"=> "",
					"P10"=> $_POST['c'],
					'return_mode' => 'json'
				];
				$responses[]=$this->process_uploads($arr);	
			}
			if(!empty($_FILES['credit_card_upload'])){
				$file_tmp= $_FILES['card_id_upload']['tmp_name'];
				$data = file_get_contents($file_tmp);
				$base64 = base64_encode($data);

				$arr=[
					'method'=>'customer_other_files',
					"P01"=> $this->session->userdata('SessionID_cc'),
					"P02"=> $this->session->userdata('customer_info_cc')['CustomerId'],
					"P03"=> "",
					"P04"=> "suncash-me",
					"P05"=> "selfie",
					"P06"=> "0",
					"P07"=> "",
					"P08"=> $base64,//image
					"P09"=> "",
					"P10"=> $_POST['c'],
					'return_mode' => 'json'
				];
				$responses[]=$this->process_uploads($arr);
			}
		$arr=[  
			'msg'=>json_encode($responses),
			'success'=>true
		];
		
		echo json_encode($arr);
		exit;  

		} else {

		}
	
	}

	private function process_uploads($arr){

		// dd($arr);
		$api_bt_result=$this->assetHelper->api_requestv2(SUNCASH_API_CHP_URL,$arr);
		$api_bt_data=json_decode($api_bt_result,true);
	// dd($api_bt_data);
		
		if($api_bt_data['ResponseCode']=='0000'){
			$arr=[  
				'msg'=>$api_bt_data['ResponseMessage'],
				'success'=>true
			];
			
			return json_encode($arr);        
		}  else {
		$arr=[  
			'msg'=>$api_bt_data['ResponseMessage'],
			'success'=>false
		];
		
			return json_encode($arr); 
		}	
	}

}