<?php
/**
 * Asset Helper
 *
 * A super-simple way to generate tags for including css and js files in CodeIgniter
 *
 * @author  Christian Leo-Pernold mazedlx@gmail.com
 * @link    https://github.com/mazedlx/asset_helper
 * @license http://www.opensource.org/licenses/mit-license
 * @package AssetHelper
 * @version 1.0.1
 */

namespace AssetHelper;

class AssetHelper
{
    public function __construct()
    {

    }

    /**
     * link_css()
     *
     * Creates the link tag for a local CSS file from the assets/css directory
     *
     * @access  public
     * @param   string $filename
     * @param   string $media
     * @return  string
     */
    public function link_css($filename, $media = null)
    {
        if (isset($media)) {
            $media = 'media="'.$media.'"';
        }
        $string = '<link rel="stylesheet" type="text/css" '.$media.' href="'.base_url().'/assets/css/'.$filename.'">';
        return $string;
    }

    /**
     * link_js
     *
     * Creates the link tag for a local JavaScript File from the assets/js directory
     *
     * @access  public
     * @param   string $filename
     * @param   string $additional
     * @return  string
     */
    public function link_js($filename, $additional = null)
    {
        $string = '<script type="text/javascript" src="'.base_url().'/assets/js/'.$filename.'" '.$additional.'></script>';
        return $string;
    }

    /**
     * link_plugins
     *
     * Creates the link tag for a local JavaScript File from the assets/js directory
     *
     * @access  public
     * @param   string $filename
     * @param   string $additional
     * @return  string
     */
    public function link_plugins($type='css', $filename, $additional = null)
    {   
        if($type=='css'){
            $string = '<link rel="stylesheet" type="text/css" href="'.base_url().'assets/plugins/'.$filename.'" '.$additional.' >';
        } else if($type=='js'){
            $string = '<script type="text/javascript" src="'.base_url().'assets/plugins/'.$filename.'" '.$additional.'></script>';
        } else if($type=='img'){
            $string = '<img src="'.base_url().'assets/img/'.$filename.'" '.$additional.'>';
        }
        return $string;
    }
 

    public function generate_receipt_html($order_arr,$filename,$isbaby){
        ob_start();
        $html=$this->html_loader($order_arr,$isbaby);
        echo $html;
        $content = ob_get_contents();
        ob_end_clean();
        file_put_contents("./assets/receipts/".$filename.".html",$content);        
    }

    public function send_sms($mobile_number, $response_msg) {
        //override, for testing
        if (substr($mobile_number, 0,4) == "1999" || substr($mobile_number, 0,4) == "1888") {
            $mobile_number = "639956336711";
        } else if (substr($mobile_number, 0,4) == "1111" || substr($mobile_number, 0,4) == "9999") {
            $mobile_number = "639178435554";
        } elseif ((substr($mobile_number, 0, 4) == '1639') && strlen ($mobile_number) == 13) {
            $mobile_number = substr($mobile_number, 1, 12);
        }
        $msgs = str_split($response_msg, 160);
        foreach($msgs as $d) {
            $params = array(
                'user'      => 'suncashapi',
                'password'  => '$unnyD@y2014!',
                'sender'    => 'SunPass Tickets',
                'SMSText'   => $d,
                'GSM'       => $mobile_number,
            );
            $send_url = 'http://api2.infobip.com/api/v3/sendsms/plain?' . http_build_query($params);
            //die($send_url);
            $send_response = file_get_contents($send_url);
        }
        //var_dump($send_response);die;
        if (!($send_response != '')) {
            return array(
                'success' => FALSE,
                'message' => 'No Response',
            );
        } else {
            if (strstr($send_response, '<status>0</status>') === false) {
                return array(
                    'success' => FALSE,
                    'message' => 'Failed:' . $send_response,
                );
            } else {
                return array(
                    'success' => true,
                    'message' => 'Success:' . $send_response,
                );
            }
        }
        return $send_response;
    }

    public function send_email($arr,$to,$subj,$template_html,$attachment_arr=null,$img_attachment=null){
        $ci = &get_instance();
        $ci->load->library('mail');
        //$mail = new Mail();
        $ci->mail->ClearAddresses();// each AddAddress add to list
        $ci->mail->ClearCCs();
        $ci->mail->ClearBCCs();
        $ci->mail->setMailBody($arr, $template_html);
        $ci->mail->smtpConnect([
            'ssl' => [
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            ]
        ]);
        //$ci->mail->IsHTML(true);
        //$ci->mail->AddEmbeddedImage('assets/img/media/suncash-icon.png', 'logoimg', 'logo.jpg'); 
        if(!empty($attachment_arr)){
            foreach($attachment_arr as $pdf_val){
            //dd($pdf_val['path']);
            //dd($ci->mail->addAttachment($pdf_val['path'],$pdf_val['filename']));
            $ci->mail->addAttachment($pdf_val['path'],$pdf_val['filename']);    
            }
        } 
        if(!empty($img_attachment)){
            foreach($img_attachment as $img_val){
            //dd($pdf_val['path']);
            //dd($ci->mail->addAttachment($pdf_val['path'],$pdf_val['filename']));
            $ci->mail->addStringEmbeddedImage($img_val['img_string'], $img_val['img_name'], $img_val['img_name'], "base64");  
            }
        }
        //$ci->mail->sendMail($subj, $to);
        if(!$ci->mail->sendMail($subj, $to))
        {
           //echo "Error sending: " . $ci->mail->ErrorInfo;
           //dd($ci->mail->ErrorInfo);
        }
        else
        {
           //echo "E-mail sent";
        }
        $ci->mail->clearAddresses();
        $ci->mail->clearAttachments();

    }



    public function get_ip(){
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        //return $ipaddress;
       //}
       $ipaddress = $ipaddress=="::1" ? $ipaddress="locahost" : $ipaddress;
       if($ipaddress!="locahost"){
          $PublicIP = $ipaddress;//get_client_ip(); 
          $json  = @file_get_contents("http://api.ipstack.com/$PublicIP?access_key=ea1610803cd1891ae355fb0db27d30eb&output=json&legacy=1");
          if($json === FALSE) { // handle error here... 
            return $ipaddress;
          }
          $json  =  json_decode($json ,true);
          $country =  $json['country_name'];
          $region= $json['region_name'];
          $city = $json['city'];


          return $country. " ".$region." ".$city;
       } 
      
          return $ipaddress;        
    }
    public function get_user_agent(){
        $ci = &get_instance();
        $ci->load->library('user_agent');  
        if ($ci->agent->is_browser())
        {
                $agent = $ci->agent->browser().' '.$ci->agent->version();
        }
        elseif ($ci->agent->is_robot())
        {
                $agent = $ci->agent->robot();
        }
        elseif ($ci->agent->is_mobile())
        {
                $agent = $ci->agent->mobile();
        }
        else
        {
                $agent = 'Unidentified User Agent';
        }

        return $agent;              
    }
    public function get_os(){
        $ci = &get_instance();
        $ci->load->library('user_agent'); 

        return $ci->agent->platform();
    }

    public function api_request($url,$data,$method,$extra =null){
        //dd($url.$data['url']);
        $curl = curl_init();
        if (!isset($data['data']) && empty($data['data'])) {
            $data['data'] = array();
        } 
        if (!isset($data['url']) && empty($data['url'])) {
            $data['url'] = '';
        }
        //var_dump(json_encode($data['data']));die;
        $options = array(
            CURLOPT_URL => $url . $data['url'],
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_POSTFIELDS => http_build_query($data['data']),
            CURLOPT_HTTPHEADER => array(
                "content-type: application/json",
                "accept: application/json",
                "cache-control: no-cache",
                $extra
            ),
        );
        if (ENV == 'DEV') {
            $options[CURLOPT_SSL_VERIFYHOST] = false;
            $options[CURLOPT_SSL_VERIFYPEER] = false;
        }
        curl_setopt_array($curl, $options);
        $response = curl_exec($curl);
        curl_close($curl);
        

        return $response;        
      }

    public function api_requestv2($url,$data){
        //dd($data);
/*        var_dump($data);
        dd($url);*/
        $req = curl_init($url);
        curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($req, CURLOPT_POST, true );
        curl_setopt($req, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($req, CURLOPT_CONNECTTIMEOUT ,0);
        curl_setopt($req, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($req, CURLOPT_SSL_VERIFYPEER, 0);
        $respCode = curl_getinfo($req, CURLINFO_HTTP_CODE);
        $response = curl_exec($req);  
        //dd($response);
        curl_close($req);  

        return $response;    
    } 

    public function api_post($url=null,$data) {
            $data['data']=$data;
            $api_url = empty($url) ? SUNCASH_API_URL : $url;
            return $this->api_request($api_url,$data, 'POST');
    }


    public function api_get($url=null,$data) {
            $api_url = empty($url) ? SUNCASH_API_URL : $url;
            return $this->api_request($api_url,$data, 'GET');
    } 

    public function create_tokken(){//for temporary tokken
        $arr=[
            'method'=>'get_temp_tokken',
            'P01'=> '$unC@sh',
            'return_mode'=>'json'
            //'P02'=>$this->security->xss_clean($this->input->post('payroll_id')),
        ];
        //$arr_tosend['url']="get_payroll_listing?".http_build_query($arr);

        //dd($arr);
        $api_bt_result=$this->api_requestv2(SUNCASH_API_URL,$arr);

        //dd($api_bt_result);
        $api_bt_data=json_decode($api_bt_result,true);
        //dd($api_bt_result);
         $data['auth_key'] ='';
         if($api_bt_data['ResponseCode']=='0000'){
            $data['auth_key']=$api_bt_data['ResponseMessage'];
         }

        return  $data['auth_key'];    
    }
    public function get_balance($session_id){
        $arr=[
            'method'=>'get_balance',
            'P01'=> $session_id,
            'return_mode'=>'json'
        ];
        //$arr_tosend['url']="get_payroll_listing?".http_build_query($arr);

        //dd($arr_tosend);
        $api_bt_result=$this->api_requestv2(SUNCASH_API_CHP_URL,$arr);
        $api_bt_data=json_decode($api_bt_result,true);
        //dd($api_bt_data);
        $data['balance']=0.00;
        if($api_bt_data['ResponseCode']=='0000'){
        $data['balance']=$api_bt_data['ResponseMessage']['Balance'];                     
        }


        return $data['balance'];
    }
    public function _get_balance_customer($card_id){
        $arr=[
            'method'=>'pinoywallet_get_balance_by_card_id',
            'P01'=> $card_id,
            'return_mode'=>'json'
        ];
                        // dd($arr);
        $api_bt_result=$this->api_requestv2(SUNCASH_API_CHP_URL,$arr);
                // dd($api_bt_result);
        $api_bt_data=json_decode($api_bt_result,true);

        $data['balance']=0.00;
        if($api_bt_data['ResponseCode']=='0000'){
        $data['balance']=$api_bt_data['ResponseMessage']['Balance'];                     
        }


        return $data['balance'];
    }
    // Function  to check whether the required params is exists in the array or not.
    public function checkpostfields($requiredFields, $requestArr,$label=null) {

        $missigFields = [];
        // Loop over the required fields and check whether the value is exist or not in the request params.
        foreach ($requiredFields as $field) {//`enter code here`
            //echo($field);
            if (empty($requestArr[$field]) || !isset($requestArr[$field])) {
                if(!empty($label)){
                    array_push($missigFields, $label[$field]);
                } else {
                    array_push($missigFields, $field);
                }
                
            }
        }
        $missigFields = implode(', ', $missigFields);
        return $missigFields;
    }

        //change if check session via api or in db
    public function session_checker(){
        $ci = &get_instance();

            //dd($ci->session->userdata('SessionID_lpbmanager'));
           if(empty($ci->session->userdata('SessionID_cc'))){
               if ($ci->input->is_ajax_request()) { 
                   http_response_code(401);
                   echo  "es||".base_url('customer/logout_cc');
                   exit;
               }  else {
                   return  redirect(base_url('customer/logout_cc'));
               }
           }            
        
    }


}
 

