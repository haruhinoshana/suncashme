<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');


// log-in in REVAPAY PORTAL USING API 
if(!function_exists('cenpos_get_tokken')){
    function cenpos_get_tokken($arr){
        require_once APPPATH.'third_party/cenpos/CenposConnector.php';

        $cenpos_connector = new CenposConnector();
        $cenpos_connector::Init(); 
        $Cenpos = new CenposConnector(); 
        $Request = new AddCryptoTokenRequest(); 
        $Request->UserId = CENPOS_USERNAME;
        $Request->Password = CENPOS_PASSWORD;
        $Request->MerchantId = CENPOS_MERCHANT_ID;
        $Request->CardNumber = $arr['card_number'];
        $Request->NameOnCard = $arr['card_name'];
        $Request->CVV = $arr['cccvv'];
        $Request->CardExpirationDate = $arr['ccedatemm'].$arr['ccedateyy'];
        $Response = $Cenpos->AddCryptoToken($Request);  

        $Response->GenerateCryptoTokenResult->Message=="Approved" ? $_SESSION['cc'] = $Response->GenerateCryptoTokenResult->CryptoToken : $_SESSION['cc']=''; 

        return $Response;
            
    }

}


if(!function_exists('cenpos_process_payment')){
    function cenpos_process_payment($arr){
        require_once APPPATH.'third_party/cenpos/CenposConnector.php';

        $cenpos_connector = new CenposConnector();
        $cenpos_connector::Init(); 
		$Cenpos = new CenposConnector(); 
		$Request = new UseCryptoTokenRequest(); 
        $Request->UserId = CENPOS_USERNAME;
        $Request->Password = CENPOS_PASSWORD;
        $Request->MerchantId = CENPOS_MERCHANT_ID;
		$Request->CardNumber = $arr['card_number_tokken'];
		$Request->Amount = $arr['amount'];
		$Request->InvoiceNumber = $arr['invoice_number'];
		$Response = $Cenpos->UseCryptoToken($Request);

        return $Response;
            
    }

}
