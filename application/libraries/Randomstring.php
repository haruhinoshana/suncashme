<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class RandomString
{
  private static $characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
  private static $string;
  private static $length = 8; //default random string length

  public static function generate($length = null,$type= null,$is_random = null,$count = null)
  {
    //die($length." - ".$type." - ".$is_random);
    $string='';
    if($type=='anum'){
      if($length){
        self::$length = $length;
      }

      $characters_length = strlen(self::$characters) - 1;
      //die($characters_length);
      $string='';
      for ($i = 0; $i < self::$length; $i++) {
        $string .= self::$characters[mt_rand(0, $characters_length)];
      }
    } else if($type=='num'){
      
      if($length){
        self::$length = $length;
      }

      if($is_random=="rnd"){
        self::$characters='0123456789';
        //die(self::$characters);
        $characters_length = strlen(self::$characters) - 1;
        //die($characters_length);
        $string='';
        for ($i = 0; $i < self::$length; $i++) {
          $string .= self::$characters[mt_rand(0, $characters_length)];
        }       
      } else if ($is_random=="asc"){
        //die($count+1);
        $string = sprintf("%0".$length."d", $count+1);
      }


    }


    return $string;

  }

}