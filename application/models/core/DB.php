<?php
use \Illuminate\Database\Eloquent\Model;
class DB {
    public static function __callStatic($method, $args)
    {
        return call_user_func_array([Model::getConnectionResolver()->connection(), $method], $args);
    }
}