<?php
use \Illuminate\Database\Eloquent\Model as Eloquent;
use CodeItNow\BarcodeBundle\Utils\QrCode;
use CodeItNow\BarcodeBundle\Utils\BarcodeGenerator;

class Qr extends Eloquent{
    //protected $table = 'users';
	 	public static function GenerateQr($text,$label,$size){
			$qrCode = new QrCode();
			$qrCode
			    ->setText($text)
			    ->setSize($size)
			    ->setPadding(5)
			    ->setErrorCorrection('high')
			    ->setForegroundColor(array('r' => 0, 'g' => 0, 'b' => 0, 'a' => 0))
			    ->setBackgroundColor(array('r' => 255, 'g' => 255, 'b' => 255, 'a' => 0))
			    ->setLabel($label)
			    ->setLabelFontSize(10)
			    ->setImageType(QrCode::IMAGE_TYPE_PNG);	

			return $qrCode;	 		
	 	}
	 	public static function GenerateBarcode128($text,$label,$size){

			$barcode = new BarcodeGenerator();
			$barcode->setText($text);
			$barcode->setType(BarcodeGenerator::Code128);
			$barcode->setScale(2);
			$barcode->setThickness(25);
			$barcode->setFontSize(10);
			$code = $barcode->generate();

			return $code;	 		
	 	}

}