<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$assetHelper = new AssetHelper\AssetHelper();
$ci = &get_instance();
?>
@extends('layout.navbar')

@section('title', 'SunCash Link Debit/Credit Card')

<!--for css to be use on page it can be include or css path -->
@section('custom_css')
<style type="text/css" >
		.ïframecenpos{
			border:1px !important;
		}
		.layaoutPanelTokenCardForm .canvasbox {
		    margin-left: 10% !important;
		    margin-right:10% !important;
		    width: 100% !important;	

		}
		.Modern .canvasbox .row > span {
			/*webkit-font-smoothing: antialiased !important;*/
			webkit-font-smoothing: antialiased !important;
		    font-family: "Roboto", sans-serif !important;
		    font-weight: 400 !important;
		    font-size: 15px !important;
		    color: #292929 !important;
		    line-height: 1.5em !important;
		    margin-bottom: .5rem !important;
		}

		.Modern .canvasbox .row > input {
			/*webkit-font-smoothing: antialiased !important;*/
			webkit-font-smoothing: antialiased !important;
		    font-family: "Roboto", sans-serif !important;
		    font-weight: 400 !important;
		    font-size: 16px !important;
		    color: #292929 !important;
		    line-height: 1.5em !important;
		    margin-bottom: .5rem !important;
		}
/*		#cenposPayIFrameId {
			    width: 85% !important;	

		}*/
	</style>
@endsection

@section('sub_content')

	<div class="page-title-container">
		<div class="container">
			<div class="row align-items-center">
				<div class="col">
					<h2 class="page-title">Link Debit/Credit Card</h2>
				</div>
				<div class="col text-right btn-col">
					<a href="{{base_url("customer/link_card_list")}}"><button class="btn btn-light">Back</button></a>

					<button class="btn btn-primary btn-icon"><i class="fas fa-search"></i></button>
				</div>
			</div>
		</div>
	</div>

<div class="main-menu">
		<div class="container">
			<div class="row mb-6">
				<div class="col">
					<div class="main-form">
						<div class="form-body">
						
							<div id="card_info" > 
								<div style="border-top: 1px solid #F0F0F0;"><br>
									<h3>Enter your card details</h3>
									<div>
										
										<form action="" class="text-left">
										<div id="NewCenposPlugin">
										</div>
										</form>
									</div>
								</div>

								<div class="form-footer">
									<div class="row">
										<div class="col">
											<button class="btn btn-primary btn-block " id="show_card" >Add/link Debit/Credit Card</button>
											<input type="hidden" class="form-control" id="amount" name="amount" value="0.00" >
										</div>
									</div>
								</div>
							</div>
							<div id="success_section" style="display:none;">
								<hr>
								<div class="label text-center">Card Details</div>

								<div class="item">
								<div class="row">
									<div class="col text-left text-details">Card Number: </div>
									<div class="col text-right"><span id="card_number"></span></div>
								</div>
								</div>
								<div class="item">
								<div class="row">
									<div class="col text-left text-details">Name on Card: </div>
									<div class="col text-right"><span id="name_on_card"></span></div>
								</div>
								</div>
								<div class="item total">
								<div class="row">
									<div class="col text-left text-details">Card Type: </div>
									<div class="col text-right amount"><span id="card_type"></span> </div>
								</div>
								</div>

								<input type="hidden" name="tid" id ="tid" value="">
								<div class="payment_btn_section">
									<div class="text-center"><button type="button" class="btn btn-primary btn-block" id="process_card_link">Add/link Debit/Credit Card</button></div>
								</div>
								<div class="cancel_section" style="display:none;">
									<div class="row">
										<!-- <div class="col text-center"><button type="button" class="btn btn-primary full" id="cancel_payment">Cancel</button></div>	 -->
										<a href="{{base_url("customer/link_card_list")}}"><button class="btn btn-light">Use Another Card</button></a>						
									</div>	
								</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
@endsection
@section('custom_js')

<script src="https://www.cenpos.com/Plugins/porthole.min.js"></script>
<script src="https://www.cenpos.com/Plugins/jquery.simplewebpay.js"></script> 
<script type="text/javascript" charset="utf-8">

	function CallbackSuccess(responseData) {
	alert(JSON.stringify(responseData));
	}

	function CallbackCancel(responseData) {
	alert(JSON.stringify(responseData));
	}

	$("#show_card").click(function(){
		$("#NewCenposPlugin").submitAction();
	});
	var merchant='{{CENPOST_MERCHANT_ID}}';
	var verify_params ='{{$verify_params["Data"]}}';
	var customer_id = '{{$customer_id}}';
	$("#NewCenposPlugin").createWebpay({
	url: 'https://www.cenpos.net/simplewebpay/cards',
	params : "customerCode="+customer_id+"&verifyingpost="+verify_params+"&iscvv=true",
	sessionToken:true,
	height:'400px',
	//isCvv :true&SecretKey=a0c70a0d5aa451bfc02f17e9199e41e6&verifyingpost="+vp
	//sessionToken:false,
	    beforeSend:function(xhr, textStatus) {
	        //called when complete
	        $("#submit").prop('disabled', true);
	      },
	    complete: function(xhr, textStatus) {
	        //called when complete
	         $("#submit").prop('disabled', false);
	      },
        success: function(data){
        	if(data.Result==0){
        	$("#card_number").text(data.ProtectedCardNumber);
        	$("#name_on_card").text(data.NameonCard);
        	$("#card_type").text(data.CardType);
        	$("#tid").val(data.RecurringSaleTokenId);
        	$("#success_section").show();
        	$("#card_info").hide();
        	} else {
        	$("#tid").val('');
        	swal(data.Message);	

        	}
        },
        cancel: function(response){
        	$("#tid").val('');
        	swal(response.Message);	
        	$("#success_section").hide();
        	$("#card_info").show();

        }	
	});
    $("#process_card_link").click(function(e){
    	var form_data ={
    	//merchant:merchant,	
    	tokenid:$("#tid").val(),
    	name_customer_card:$("#name_card").val(),
    	//email_card:$("#email_card").val(),
    	//mobile_card:$("#mobile_card").val(),
    	card_type:$("#card_type").text(),
    	card_number:$("#card_number").text(),
    	name_on_card:$("#name_on_card").text(),
		amount:$("#amount").val(),
		customer_id:customer_id,
        }   
		$.ajax({
		url: '{{base_url("customer/process_link_card")}}',
		type: 'POST',
		dataType: 'json',
		data: form_data,
		beforeSend:function(){
			$("#process_card_link").prop("disabled",true);
			$("#process_card_link").text("Adding Card... Please wait!");
			//loader.showPleaseWait();
		},
		complete: function(xhr, textStatus) {
			$("#process_card_link").prop("disabled",false);
			$("#process_card_link").text("Adding Card. Please Wait! ");
			//loader.hidePleaseWait();
		},
		success: function(data, textStatus, xhr) {
			//console.log();
			if(data.success){
				swal(data.msg);
				setTimeout(function(){ window.location.reload(); }, 2000);
			} else {
				swal(data.msg);
				$(".cancel_section").show();
				$(".payment_btn_section").hide();
				// $("#success_section_transaction").hide();
				$("#process_card_link").prop("disabled",false);
				$("#process_card_link").text("Adding Card.. Please Wait! ");
				
			}
		},
		error: function(xhr, textStatus, errorThrown) {
		}
		});
	});

</script>
@endsection