<?php
//loading assets helper
defined('BASEPATH') OR exit('No direct script access allowed');
$assetHelper = new AssetHelper\AssetHelper();
$ci=&get_instance();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>SunCash.Me</title>
	<link rel="stylesheet" href="{{base_url('assets_main/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/main.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/main_checkout.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/responsive.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/all.min.css')}}">
	<link rel="stylesheet" href="{{base_url('assets/plugins/global/global2.css')}}">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>assets/plugins/icon.ico">

</head>
<body>
	<nav class="navbar navbar-expand-lg sticky-top bg-white shadow-sm p-0">
	  <div class="container p-0" role="navigation">
	  	<a class="navbar-brand" href="<?php echo base_url();?>"><img src="{{base_url('assets_main/imgs/header-logo@2x.png')}}" alt="Suncash Logo" class="d-inline-block align-top" width="160"></a>
	  	<a href="#" class="menu-button"></a>
	  	<div class="justify-content-end main-menu">
				<ul class="navbar-nav d-flex align-items-center">


		    </ul>
		  </div>
	  </div>
	</nav>
	<section class="" id="amazon_section">
		<div class="container  text-center">
			<div class="row d-flex justify-content-center align-items-center mb-5">
				<div class="user-card transaction-details" style="margin-top:1rem;margin-bottom:-34px !important;">
					<div class="header w-content" style="height:6rem !important;"></div>
					<div class="body">
						<form id="amazon_form" method="POST">
				        <div class="user-image ">
								@if(!empty($_SESSION['profile_pic']))
								<img src="{{$_SESSION['profile_pic']}}">
								@else
								<img src="{{base_url('assets_main/imgs/suncash-icon.png')}}">
								@endif
						</div>
						<div class="details" style="margin-bottom: -40px;">

							<div class="message">You're about to pay <?=ucfirst($_SESSION['suntag_shortcode']) ?></div>
							<select name="payment_method" id="payment_method" >
								<option value="">BSD</option>
							</select>
							<input class="input-amount" type="text" id="amount" name="amount" value="{{$payment_data['amount']}}" style="height: 100% !important">
						</div>
						<div class="text-left">
							<div class="item">
								<!-- <input type="hidden" class="form-control" id="reference_num" name="reference_num"  value="{{$payment_data['reference_num']}}" placeholder="Reference # (Order, Quote, Invoice or Account)">
								<div class="form-group">
						          <input type="hidden" class="form-control" id="name" name="name" placeholder="Name" autocomplete="off" style="resize:none" value="{{$payment_data['name']}}" >
								</div>		
								<div class="form-group">
						          <input type="hidden" class="form-control" id="email" name="email" placeholder="Email" autocomplete="off" style="resize:none" value="{{$payment_data['email']}}" >
								</div>		
								<div class="form-group">
						          <input type="hidden" class="form-control" id="mobile" name="mobile" placeholder="mobile" autocomplete="off" style="resize:none" value="{{$payment_data['mobile']}}" >
								</div>																 -->
							  	<input type="hidden"  class="form-control" id="notes" name="notes" placeholder="Note to Business" value="{{$payment_data['notes']}}">
							</div>
							 
							<div class="item primary-border" >
								<?php 
									if($_SESSION['tag']=='MERCHANT' && $_SESSION['registration_type']!='Charity'){
										$tfees = number_format($fee_data['fee'], 2, '.', '')+$fee_data['vat_charge'];
									}else{
										$tfees = "0.00";
									}
								?>

								<div class="label text-center">Transaction Details:</div>
									<div class="item" style="margin-bottom: 0px !important;margin-top: 0px !important;">
										<div class="row text-details">
											<div class="col">Principal</div>
											<div class="col text-right amount ">$ <span class="amount_val">{{$payment_data['amount']}}</span></div>
										</div> 
									</div> 
									@if($_SESSION['tag']=='MERCHANT' && $_SESSION['registration_type']!='Charity')
									<div class="item" style="margin-bottom: 0px !important;margin-top: 0px !important;">
										<div class="row text-details">
										<div class="col">Transaction Fee</div>
										<div class="col text-right">$ <span class="bfee">{{number_format($tfees,2)}}</div>
										</div>
									</div>		
									@endif
									<div class="item" style="margin-bottom: 0px !important;margin-top: 0px !important;">
										<div class="row text-details">
										<div class="col">Convenience Fee</div>
											<div class="col text-right">$ <span class="conveniencefee">{{number_format($ccfee,2)}}</span></div>
										</div>
									</div>										

								</div>						

								<div class="item total">
									<div class="row">
									<?php 
										if($_SESSION['tag']=='MERCHANT' && $_SESSION['registration_type']!='Charity'){
											$total = str_replace( ',', '', $payment_data['amount'])+number_format($ccfee,2)+$tfees;
										}else{
											$total = str_replace( ',', '', $payment_data['amount'])+number_format($ccfee,2); //no bfee
										}

									?>
									<div class="col label">Total Due</div>
										<div class="col text-right"><span class="total" id="amz_total_x">{{number_format($total,2)}}</span> BSD</div>
									</div>
								</div>

								<div class="amazon_section">
									<form id="amazon_form" class="text-left">
										<div id="show_order_review"  style=" display:none ;">	
										<div class="label text-center">Verify Buyer Details</div>									
											<div class="form-group">
												<label for="name_card">Full Name</label>
												<div class="form-div">
												<input type="text" class="form-control" id="amazon_fullname" name="amazon_fullname" placeholder="Enter your full name" autocomplete="off" required readonly>
												</div>
											</div>							
											<div class="form-group">
												<label for="mobile">Mobile Number</label>
												<div class="form-div">
													<input type="text" class="form-control required " id="amazon_mobile" name="amazon_mobile" placeholder="Enter your mobile number" autocomplete="off" readonly >
													<!-- data-format="1 (ddd) ddd-dddd" value ="1 242 bfh-phone"  -->
												</div>
											</div>
											<div class="form-group">
												<label for="amazon_email">Email Address</label>
												<div class="form-div">
													<input type="email" class="form-control" id="amazon_email" name="amazon_email" placeholder="Email Address" value="{{$payment_data['mobile']}}" autocomplete="off" readonly required>	
												</div>
											</div>
										</div>	
										<div class="amz_section">		
											<div class="text-center">
												<div style="text-align: center; border: 1px solid #bbb;border-radius: 3px;padding:5px;margin:5px;">
													<center><div id="AmazonPayButton"></div></center>
													
												</div>
											</div>
										</div>

										<!-- campture process -->
										<div class="amz_process_section" style=" display:none ;">

                                            @if($_SESSION['tag']=='MERCHANT' && $_SESSION['registration_type']!='Charity')
                                            <div class="text-center"><button type="submit" id="amz_payment"  class="btn btn-orange full">Process Payment</button></div>
                                            @else
                                            <div class="text-center"><button type="submit" id="amz_payment"  class="btn btn-orange full">Donate</button></div>
                                            @endif
						
										</div>
									</form>
								</div>
						
							
							</div>									
						</div>
						</form>
					</div>
				</div>				
			</div>
		</div>
		<footer class="py-3 footer">
			<div class="container">
				<div class="row">
					<div class="col-12 col-lg-6">
						<span class="txt-shit">© {{date('Y')}} SunCash.Me. All Rights Reserved.</span>
					</div>
					<div class="col-12 col-lg-6 ">
						<img class="img-x" src="{{base_url('assets_main/imgs/footer-logo.png')}}">
					</div>
				</div>
			</div>
		</footer>		
	</section>


	<section class="full" id="success_section_transaction" style=" display:none ;">
		<div class="container  text-center">
			<div class="row d-flex justify-content-center align-items-center mb-5">
				<div class="user-card success-card" style="margin-top:1rem;margin-bottom:60px !important;">
					<div class="header w-content" style="margin-top:1rem;margin-bottom:-34px !important;"></div>
					<div class="body">
						<div class="user-image">
							<img src="{{base_url('assets_main/imgs/success-icon.png')}}">
						</div>
						<div class="details" id = "transaction-details">
							<div class="message" style="font-size: 2rem;line-height: 2.1rem;width: 90%;margin: 0 auto;">You Paid <span class="amount_val" style="color: #FF8400;">${{$payment_data['amount']}}</span> to <?=ucfirst($_SESSION['suntag_shortcode'])?></div>
						</div>
						<div class="text-left">

					  	<div class="label text-center">Transaction Details</div>
						  <div class="item">
							  <div class="row">
				  				<div class="col">TransactionID : <span id="transaction_code"></span></div>
							  	<div class="col text-right">{{date("d M Y")}}</div>
							  </div>
							</div>
							<div class="item">
							  <div class="row">
							  	<div class="col">Payment Method</div>
							  	<div class="col text-right">Amazon Payment</div>
							  </div>
							</div>

							<div class="item">
							  <div class="row">
							  	<div class="col">Principal</div>
							  	<div class="col text-right">$ <span class="amount_val">{{$payment_data['amount']}}</span></div>
							  </div>
							</div>	
									
							@if($_SESSION['tag']=='MERCHANT' && $_SESSION['registration_type']!='Charity')					
							<div class="item">
							  <div class="row">
							  	<div class="col">Transaction Fee</div>
							  	<div class="col text-right">$ <span class="bfee">{{number_format($tfees,2)}}</span></div>
							  </div>
							</div>	
							@endif
							<div class="item">
							  <div class="row">
							  	<div class="col">Convenience Fee</div>
								<div class="col text-right">$ <span class="conveniencefee">{{number_format($ccfee,2)}}</span></div>
							  </div>
							</div>							

						  <div class="item total">
							  <div class="row">
							  	<div class="col label">Total Due</div>
							  	<div class="col text-right amount"><span class="amount_total">{{number_format($total,2)}}</span> BSD</div>
							  	<input type="hidden" id="amount_total" name="amount_total" value="{{$total}}" />
							  	<input type="hidden" id="pf" name="pf" value="{{$convenience_data['processing_fee']}}" />
							  	<input type="hidden" id="tf" name="tf" value="{{$convenience_data['transaction_fee']}}" />
							  	<input type="hidden" id="bfee" name="bfee" value="{{$tfees}}" />							  	
							  </div>
						  </div>
						  	<!-- <div class="text-center"><button type="button" id="back" class="btn btn-orange full">Back to Main</button></div> -->
						</div>
						<!-- </form> -->

					</div>
				</div>
			</div>
		</div>
	</section>
	</div><!-- end -->


	<script src="{{base_url('assets_main/js/jquery-3.2.1.min.js')}}"></script>
	<script src="{{base_url('assets_main/js/bootstrap.min.js')}}"></script>

	{!!$assetHelper->link_plugins('js','sidebar-nav/dist/sidebar-nav.min.js')!!}
	{!!$assetHelper->link_plugins('js','override/jquery.slimscroll.js')!!}
	{!!$assetHelper->link_plugins('js','override/waves.js')!!}
	{!!$assetHelper->link_plugins('js','override/custom.min.js')!!}
	{!!$assetHelper->link_plugins('js','sweetalert2-master/dist/sweetalert2.all.min.js')!!}
    {!!$assetHelper->link_plugins('js','Inputmask-5.x/dist/jquery.inputmask.min.js')!!}
    {!!$assetHelper->link_plugins('js','bootstrap-formhelpers-phone/bootstrap-formhelpers-phone.js') !!}
    {!!$assetHelper->link_plugins('js','bootstrap-formhelpers-phone/bootstrap-formhelpers-phone.format.js') !!}
	{!!$assetHelper->link_plugins('js','global/global.js')!!}	
	{!!$assetHelper->link_plugins('js','html5-qrcode.min.js')!!}
	<script src="https://static-na.payments-amazon.com/checkout.js"></script>	
	<script src="{{base_url('assets_main/js/main.js')}}"></script>
	<script  type="text/javascript" charset="utf-8">

	var AMAZON_IS_SANDBOX = '{{AMAZON_IS_SANDBOX}}';
	var issandbox= false;
	if(AMAZON_IS_SANDBOX=='true'){
		issandbox=true;
	}

	var payload = '{!!json_encode($payload)!!}';
	        amazon.Pay.renderButton('#AmazonPayButton', {
            // set checkout environment
            merchantId: 'AVJGZSMCRS438',
            ledgerCurrency: 'USD',
            sandbox: issandbox,       //ture orfalse        
            // customize the buyer experience
            checkoutLanguage: 'en_US',
            productType: 'PayOnly',
            placement: 'Checkout',
            buttonColor: 'Gold',
            // configure Create Checkout Session request
			
            createCheckoutSessionConfig: {                     
                payloadJSON:payload , // string generated in step 2
                signature: '{{$signature}}', // signature generated in step 3
                publicKeyId: 'AGWGBAZVOO5IESX4GE6WM6UG' 
            }   
        });
	
	@if(!empty($amz_order_deatils))
		$("#amazon_fullname").val("{{$ci->session->userdata('amz_sess_buyer')['buyername']}}");
		$("#amazon_email").val("{{$ci->session->userdata('amz_sess_buyer')['buyeremail']}}");
		$("#amazon_mobile").val("{{$payment_data['mobile']}}");
		$("#show_order_review").show();
		$(".amz_section").hide();
		$(".amz_process_section").show();
		@if($amz_status=='review')
			// alert('review!');
			$("#amz_payment").text('Confirm Details').val('{{$amz_status}}');
			$("#amount").prop('readonly',true);
		@elseif($amz_status=='pay')
			$("#amz_payment").text('Pay').val('{{$amz_status}}');
		@endif
	@endif

	$("#amazon_form").submit(function(){
		var form_data =$(this).serializeArray();
		var r ='{{$payment_data["reference_num"]}}';
		var total_due ='{{$total}}';
		var status=$("#amz_payment").val();
		var dba_name='{{$ci->session->userdata("suntag_shortcode")}}';	
		var tf=$("#tf").val();
		var pf=$("#pf").val();
		var bfee=$("#bfee").val();		
		var is_merch ='{{$convenience_data["iscard_fee_on_merch"]}}';
		form_data.push({name: "r", value: r});
		form_data.push({name: "total_due", value: total_due});
		form_data.push({name: "status", value: status});
		form_data.push({name: "dba_name", value: dba_name});
		
		form_data.push({name: "bfee", value: bfee});

		form_data.push({name: "tf", value: tf});
		form_data.push({name: "pf", value: pf});
		form_data.push({name: "is_merch", value: is_merch});
		var url_to_process = status=='review' ? "{{base_url('payment/amazon_review_process')}}" : "{{base_url('payment/process_amazon_suncashme')}}";
		$.ajax({
			type: "POST",
			url: url_to_process,
			data: form_data,
			dataType: "json",
			beforeSend: function (response) {
				loader2.showPleaseWait();
				$("amz_payment").prop('disabled',true);  
			},
			complete: function (response) {
				$("#amz_payment").prop('disabled',false);
				loader2.hidePleaseWait();  
			},
			success: function (response) {
				// console.table(response);
				$("#amz_payment").prop('disabled',false);
				loader2.hidePleaseWait(); 
            	if(response.success){
					@if($amz_status=='review')
						window.location.href=response.url;
					@elseif($amz_status=='pay')
						$("#transaction_code").text(response.reference);
						$("#amazon_section").hide();	
						$("#success_section_transaction").show();
					@endif
				} else {
				$("#amz_payment").prop('disabled',false);
				loader2.hidePleaseWait();
				swal(response.msg);
				}
			},
			error: function (response) {
				loader2.hidePleaseWait();
				//swal(response.msg);
			},
   		});
		return false;
	});

	$("#amount").change(function(){

		if($("#amount").val()<=0.00){
		  	swal("Amount is required.");

		  	return false;
		}

		$.ajax({
		  url: '<?=site_url("payment/process_fee_data")?>',
		  type: 'POST',
		  dataType: 'json',
		  // data: {amount: $("#amount").val()},
		  data: {amount: parseFloat($("#amount").val().replace(/,/g, ''))},
	   	  beforeSend:function(xhr, textStatus) {
	        //called when complete
		    loader.showPleaseWait();
	        $("#submit").prop('disabled', true);
	      },		  
		  complete: function(xhr, textStatus) {
		    //called when complete
		  loader.hidePleaseWait();
		  },
		  success: function(data, textStatus, xhr) {
		    if(data.success){

				$(".amount_val").text($("#amount").val());
				$(".conveniencefee").text(data.ccfee);
		    	$(".amount_val").text($("#amount").val());
				$(".bfee").text(data.fee_data.fee);
				$("#pf").text(data.pf);
				$("#tf").text(data.tf);
				//$("#amz_total_x").text(data.total);
				@if($_SESSION['tag']=='MERCHANT' && $_SESSION['registration_type']!='Charity')
				var total_due= parseFloat(data.total)+parseFloat(data.fee_data.fee);				
					// amount_total
					$(".amount_total").text(total_due.toFixed(2));
					$("#amz_total_x").text(total_due.toFixed(2));
				@else
				var total_due= parseFloat(data.total);				
					$(".amount_total").text(total_due.toFixed(2));
					$("#amz_total_x").text(total_due.toFixed(2)); //no bfee
				@endif		
				$("#submit").prop('disabled', false);
		    } else {
		    	swal(data.msg);
		    	$("#submit").prop('disabled', false);
		    }
		  },
		  error: function(xhr, textStatus, errorThrown) {
		    //called when there is an error
		   	swal(xhr.msg);
		    $("#submit").prop('disabled', false);

		  }
		});

	});

	// function getcfee(amount){
	//     $.ajax({
	//       url: '{{base_url("merchant_checkout/get_fees_payment")}}',
	//       type: 'POST',
	//       dataType: 'json',
	//       data:{

	// 		  total_due:'{{$payment_data["amount"]}}',
	// 		  MerchantKey:'a15d411bdbdfffa46c50cb57958bd68fef35d5eacbfcaba0dc21a8caf03ac995',

	// 		},
	//    	  beforeSend:function(xhr, textStatus) {
	//         $("#submit").prop('disabled', true);
	//       },		  
	// 	  complete: function(xhr, textStatus) {
	// 	  },
	//       success: function(data, textStatus, xhr) {
	//         //called when successful
	//         //paste values
	//         if(data.success){
	//         	$(".convenience_fee").text(data.ccfee);
	// 			$(".total").text(data.total);
	// 			// $("#hid_pfee").val(data.pf);
	// 			// $("#hid_tfee").val(data.tf);
	// 			// $("#hid_totalfee").val(data.fee);

	// 	    	// $("#submit").prop('disabled', false);
	//         }  else {
	//         	swal(data.msg);
	// 	    	// $("#submit").prop('disabled', false);
	//         }

	//       },
	//       error: function(xhr, textStatus, errorThrown) {
	//         //called when there is an error
    //     	swal(xhr.msg);
	//     	// $("#submit").prop('disabled', false);
	//       }
	//     });
	// }


	</script>

</body>
</html>