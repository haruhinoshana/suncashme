<?php
//loading assets helper
defined('BASEPATH') OR exit('No direct script access allowed');
$assetHelper = new AssetHelper\AssetHelper();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="{{base_url('assets_main/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/main_checkout.css')}}">
		<link rel="stylesheet" href="{{base_url('assets_main/css/main.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/responsive.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/all.min.css')}}">
</head>
<body>
	<nav class="navbar navbar-light bg-light">
	  <a class="navbar-brand" href="#">
	    <img src="{{base_url('assets_main/imgs/suncash-logo.png')}}">
	  </a>
	</nav>

	<section class="full" id="success_section">
		<div class="container  text-center">
			<div class="row d-flex justify-content-center align-items-center mb-5">
				<div class="user-card message-card reload-card">
					<div class="header w-content"></div>
					<div class="body">
						<div class="user-image">
							<img src="{{base_url('assets_main/imgs/error-icon.png')}}">
						</div>
						<div class="details">
							<div class="message error_msg">Order has been cancelled!</div>
						</div>
						<div class="text-center border-top msg_area">
							Redirecting after <span id="countdown"  style="color: #FF8400;">5</span> seconds..

						</div>
						 <!-- <a href="{{base_url('payment/wallet')}}" class="btn btn-orange full">Back</a> -->
					</div>
				</div>
			</div>
		</div>
	</section>

	<script src="{{base_url('assets_main/js/jquery-3.2.1.min.js')}}"></script>
	<script src="{{base_url('assets_main/js/bootstrap.min.js')}}"></script>
	<script src="{{base_url('assets_main/js/main.js')}}"></script>
    <script src="{{base_url('assets_main/js/select.min.js')}}"></script>
	<script  type="text/javascript" charset="utf-8">
    <?php $params = base64_encode($payment_reference."||".$reference_id."||".$status."||".$payment_method);//dd($params);?>
    // Total seconds to wait
    var seconds = 5;
    var el = '{{$callbackurl}}';
	el=el.replace("&amp;", "&");
	//alert(el+'{{$params}}');
	//return false;
    function countdown() {
        seconds = seconds - 1;
        if (seconds < 0) {
            // Chanage your redirection link here
            //alert('{{$callbackurl}}{{$params}}');
            window.location.href=el+'{{$params}}';
        } else {
            // Update remaining seconds
            document.getElementById("countdown").innerHTML = seconds;
            // Count down using javascript
            window.setTimeout("countdown()", 1000);
        }
    }
    
    // Run countdown function
    countdown();
    //alert('{{$callbackurl}}');
</script>
</body>
</html>