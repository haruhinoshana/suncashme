<?php
//loading assets helper
defined('BASEPATH') OR exit('No direct script access allowed');
$assetHelper = new AssetHelper\AssetHelper();
$ci=&get_instance();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Link Debi/Credit Card</title>
	<link rel="stylesheet" href="{{base_url('assets_main/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/main.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/main_checkout.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/responsive.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/all.min.css')}}">
	<link rel="stylesheet" href="{{base_url('assets/plugins/global/global2.css')}}">
	<link href="https://unpkg.com/filepond/dist/filepond.css" rel="stylesheet">
	<link href="https://unpkg.com/filepond/dist/filepond.min.css" rel="stylesheet">
	<link href="https://unpkg.com/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.min.css" rel="stylesheet">	
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>assets/plugins/icon.ico">
	<style type="text/css" >
		.ïframecenpos{
			border:1px !important;
		}
		.layaoutPanelTokenCardForm .canvasbox {
		    margin-left: 10% !important;
		    margin-right:10% !important;
		    width: 100% !important;	

		}
		.Modern .canvasbox .row > span {
			/*webkit-font-smoothing: antialiased !important;*/
			webkit-font-smoothing: antialiased !important;
		    font-family: "Roboto", sans-serif !important;
		    font-weight: 400 !important;
		    font-size: 15px !important;
		    color: #292929 !important;
		    line-height: 1.5em !important;
		    margin-bottom: .5rem !important;
		}

		.Modern .canvasbox .row > input {
			/*webkit-font-smoothing: antialiased !important;*/
			webkit-font-smoothing: antialiased !important;
		    font-family: "Roboto", sans-serif !important;
		    font-weight: 400 !important;
		    font-size: 16px !important;
		    color: #292929 !important;
		    line-height: 1.5em !important;
		    margin-bottom: .5rem !important;
		}
	</style>

</head>
<body>
	<section class="" id="card_section">
		<div class="container  text-center">
			<div class="row d-flex justify-content-center align-items-center mb-5">
				<div class="user-card transaction-details">
					<!-- <div class="header w-content" style="height:6rem !important;"></div> -->
					<div class="body">
						<form id="cashpayment_form" method="POST">
						<br>					
			        	<div id="card_info" style="border-top: 1px solid #F0F0F0; display:block;"><br>
						    <h3>Enter your card details</h3>
						    <!-- <span>We dont share your financial details with merchant.</span> -->
							<div>
							    <form action="" class="text-left">
								<div id="NewCenposPlugin">
								</div>
								<div class="text-center"><button type="button" class="btn btn-orange full" id="submit">Next</button></div><br>
								</form>
							</div>
						</div>
						<div id="success_section" style="display:none;">
							<hr>
					  		<div class="label text-center">Card Information</div>
							<div class="item">
							  <div class="row">
							  	<div class="col text-left text-details">Name on Card: </div>
							  	<div class="col text-right"><span id="name_on_card"></span></div>
							  </div>
							</div>
						  	<div class="item">
							  <div class="row">
							  	<div class="col text-left text-details">Card Number: </div>
							  	<div class="col text-right"><span id="card_number"></span></div>
							  </div>
							</div>
						  	<div class="item">
							  <div class="row">
							  	<div class="col text-left text-details">Card Type: </div>
							  	<div class="col text-right amount"><span id="card_type"></span> </div>
							  </div>
							</div>
						  	<div class="item">
								<div class="row">
									<div class="col text-left text-details">Card Expiration: </div>
									<div class="col text-right amount"><span id="expiration"></span> </div>
								</div>
							</div>
							
							  <input type="hidden" name="tid" id ="tid" value="">
							<hr>
							<div id="picture_section">
								<div class="form-group mb-5" style="display:none;">
									<label>Upload a Scan of your ID</label>
									<div class="info"><i class="fas fa-exclamation-circle"></i> Please upload a Government issued ID such as  Passport, current Driver’s License, or National ID Card.</div>

									<div class="row">
										<div class="col-12 col-lg-6">
												<input type="file" class="my-pond" name="filepond1" id="card_id_upload" accept="image/jpeg, image/png" />
										</div>
										<div class="col-12 col-lg-6">
												<div class="illustration-image text-center">
													<div>
														<img src="{{base_url()}}assets_checkout/imgs/card-id.png" alt="">
														<span class="d-block">Example</span>
													</div>
												</div>
										</div>
									</div>
								</div>
								<div class="form-group mb-5"  style="display:none;">
									<label>Upload a Scan of your Debit/Credit Card</label>
									<div class="info"><i class="fas fa-exclamation-circle"></i> Please upload the debit/credit card you used with a clear view of your last 4 digit card number, name and expiry date</div>
									<div class="row">
										<div class="col-12 col-lg-6">
												<input type="file" class="my-pond" name="filepond2" id="credit_card_upload" accept="image/jpeg, image/png"/>
										</div>
										<div class="col-12 col-lg-6">
												<div class="illustration-image text-center">
													<div>
														<img src="{{base_url()}}assets_checkout/imgs/debit-card.png" alt="">
														<span class="d-block">Example</span>
													</div>
												</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label>Upload a Selfie with Card</label>
									<div class="info"><i class="fas fa-exclamation-circle"></i> Please upload a clear photo of you holding your credit/debit card, covering the card number but showing the last 4 digits.</div>

									<div class="row">
										<div class="col-12 col-lg-12">
												<div class="illustration-image text-center">
													<div>
														<span class="d-block">Example</span>
														<!-- <img src="{{base_url()}}assets_checkout/imgs/user-hold-cards.png" alt=""> -->
														<img src="http://dev.suncash.me/customer/assets/imgs/card_verify_pic.png" width='90%'>
														
													</div>
												</div>
										</div>
										<div class="col-12 col-lg-12">
												<input type="file" class="my-pond" name="filepond3" id="cc_with_card_upload" accept="image/jpeg, image/png"/>
										</div>
									</div>
								</div>
								<div class="payment_btn_section">
									<div class="text-center"><button type="button" class="btn btn-orange full" id="link_card">Save/Link Card</button></div>
								</div>
							</div>  
						  	<div class="cancel_section" style="display:none;">
						  		<div class="row">
						  			<div class="col text-center"><button type="button" class="btn btn-orange full" id="another_card">Use Another Card</button></div>						
						  		</div>	
						  	</div>
						</div>
						</form>
						<!-- <div id="validate_section" style="display:block;">
							<div class="label text-center">Card Information</div>
						  	<div class="item">
							  <div class="row">
							  	<div class="col text-left text-details">Card Number: </div>
							  	<div class="col text-right"><span id="cn"></span></div>
							  </div>
							</div>
						  	<!-- <div class="item">
							  <div class="row">
							  	<div class="col text-left text-details">Transaction Time: </div>
							  	<div class="col text-right tt"><span id="tt"></span> </div>
							  </div>
							</div> 
							<div class="item">
								<div class="row">
									<div class="col text-left text-details">Amount: </div>
									<div class="col text-right a"><span id="a">Between 0.00 to 2.00</span> </div>
								</div>
							</div>
						  	<div class="item">
								<div class="row">
									<div class="col text-left text-details">Currency: </div>
									<div class="col text-right currency"><span id="currency">USD</span> </div>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label">Amount (number of attempts: )</label>
								<div class="form-group">
								  <div class="input-group mb-3">
									<div class="input-group-prepend">
									  <span class="input-group-text">$</span>
									</div>
									<input type="text" class="form-control" aria-label="Amount (to the nearest dollar)">
								  </div>
								</div>
							  </div>
							  <div class="payment_btn_section">
								<div class="text-center"><button type="button" class="btn btn-orange full" id="verify_card_btn">Verify Card</button></div>
							</div>
						</div> -->
						<!-- <div id="verify_section" style="display:none;">
							<div class="">
								<div class="card px-0 pt-4 pb-0 mt-3 mb-3">
									<h2 id="heading">Linked your card.</h2>
									<p>Fill all form field to go to next step</p>
									<form id="msform">
										
										<ul id="progressbar">
											<li class="active" id="account"><strong>Card Information</strong></li>
											<li id="personal"><strong>Basic Information</strong></li>
											<li id="payment"><strong>Uploads</strong></li>
											<li id="confirm"><strong>Finish</strong></li>
										</ul>
										<div class="progress">
											<div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div> 
										</div> <br> 

										<fieldset class="fs">
											<div class="form-card">
												<div class="row">
													<div class="col-7">
														<h2 class="fs-title">Basic Information:</h2>
													</div>
													<div class="col-5">
														<h2 class="steps">Step 1 - 4</h2>
													</div>

												</div> <label class="fieldlabels">Last 4 digits</label> 
												<input type="email" id="last4digits" name="last4digits"  readonly placeholder="Last 4 Digits" /> 

												<label class="fieldlabels">Type: *</label> 
												<input type="text" id="card_type_w" name="card_type_w"  readonly placeholder="Card Type" /> 
												
												<label class="fieldlabels">Name on card *</label> 
												<input type="text" id="name_on_card_w" name="name_on_card_w"  readonly placeholder="Name on Card" /> 
												
												<input type="hidden" id ="expiration" name="expiration" />
											</div> 
											<input type="button" name="next" class="next action-button" v='1' value="Next" />
										</fieldset>
										<fieldset class="fs">
											<div class="form-card">
												<div class="row">
													<div class="col-7">
														<h2 class="fs-title">Personal Information:</h2>
													</div>
													<div class="col-5">
														<h2 class="steps">Step 2 - 4</h2>
													</div>
												</div> 
												<label class="fieldlabels">First Name: *</label> 
												<input type="text" class="required_v2" id="first_name" name="first_name" placeholder="First Name" />
												<label class="fieldlabels">Last Name: *</label> 
												<input type="text" class="required_v2" id="last_name" name="last_name" placeholder="Last Name" />
												<label class="fieldlabels">Middle Name: *</label> 
												<input type="text" class="required_v2" id="middle_name" name="middle_name" placeholder="Middle Name" />

												<label class="fieldlabels">Mobile: *</label> 
												<input type="text" class="required_v2" id="card_mobile_number" name="card_mobile_number"  placeholder="Mobile" />
												<label class="fieldlabels">Email: *</label> 
												<input type="text" class="required_v2" id="card_email" name="card_email" placeholder="Email" />	

												<label class="fieldlabels">Gender: *</label> 
												<input type="text" class="required_v2" id="gender" name="gender" placeholder="Gender" />

												<label class="fieldlabels">Date of Birth *</label> 
												<input type="text" class="required_v2" id="dob" name="dob" placeholder="Date of birth" />

												<label class="fieldlabels">Address: *</label> 
												<input type="text" class="required_v2" id="address" name="address"" placeholder="Address" /> 

												<label class="fieldlabels">City: *</label> 
												<input type="text" class="required_v2" id="city" name="city"" placeholder="City" />

												<label class="fieldlabels">Island: *</label> 
												<input type="text" class="required_v2" id="island" name="island"" placeholder="Island" /> 

												<label class="fieldlabels">Country: *</label> 
												<input type="text" class="required_v2" id="country" name="country" placeholder="Country" />

												<label class="fieldlabels">Card Name: *</label> 
												<input type="card_name" name="card_name" placeholder="Password" /> 

												<label class="fieldlabels">ID: *</label> 
												<input type="text" id="card_id" name="card_id" placeholder="Confirm Password" /> 

												<label class="fieldlabels">Email: *</label> 
												<input type="text" id="card_email" name="card_email" placeholder="Confirm Password" />												
											</div> <input type="button" name="next" class="next action-button" v='2' value="Next" /> <input type="button" name="previous" class="previous action-button-previous" value="Previous" />
										</fieldset>
										<fieldset class="fs">
											<div class="form-card">
												<div class="row">
													<div class="col-7">
														<h2 class="fs-title">Personal Information:</h2>
													</div>
													<div class="col-5">
														<h2 class="steps">Step 3 - 4</h2>
													</div>
												</div> 
											</div> 
											<div class="form-group mb-5">
												<label>Upload a Scan of your ID</label>
												<div class="info"><i class="fas fa-exclamation-circle"></i> Please upload a Government issued ID such as  Passport, current Driver’s License, or National ID Card.</div>

												<div class="row">
													<div class="col-12 col-lg-6">
															<input type="file" class="my-pond" name="filepond1" id="card_id_upload" accept="image/jpeg, image/png" />
													</div>
													<div class="col-12 col-lg-6">
															<div class="illustration-image text-center">
																<div>
																	<img src="{{base_url()}}assets_checkout/imgs/card-id.png" alt="">
																	<span class="d-block">Example</span>
																</div>
															</div>
													</div>
												</div>
											</div>
											<div class="form-group mb-5">
												<label>Upload a Scan of your Debit/Credit Card</label>
												<div class="info"><i class="fas fa-exclamation-circle"></i> Please upload the debit/credit card you used with a clear view of your last 4 digit card number, name and expiry date</div>
												<div class="row">
													<div class="col-12 col-lg-6">
															<input type="file" class="my-pond" name="filepond2" id="credit_card_upload"/>
													</div>
													<div class="col-12 col-lg-6">
															<div class="illustration-image text-center">
																<div>
																	<img src="{{base_url()}}assets_checkout/imgs/debit-card.png" alt="">
																	<span class="d-block">Example</span>
																</div>
															</div>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label>Upload a Credit Card with Card ID</label>
												<div class="info"><i class="fas fa-exclamation-circle"></i> Please upload a clear photo of you holding your credit/debit card.</div>

												<div class="row">
													<div class="col-12 col-lg-6">
															<input type="file" class="my-pond" name="filepond3" id="cc_with_card_upload" accept="image/jpeg, image/png"/>
													</div>
													<div class="col-12 col-lg-6">
															<div class="illustration-image text-center">
																<div>
																	<img src="{{base_url()}}assets_checkout/imgs/user-hold-cards.png" alt="">
																	<span class="d-block">Example</span>
																</div>
															</div>
													</div>
												</div>
											</div>
											<input type="button" name="next" class="next action-button" v='3' value="Process" /> 
											<input type="button" name="previous" class="previous action-button-previous" value="Previous" />
										</fieldset>
										<fieldset class="fs">
											<div class="form-card">
												<div class="row">
													<div class="col-7">
														<h2 class="fs-title">Finish:</h2>
													</div>
													<div class="col-5">
														<h2 class="steps">Step 4 - 4</h2>
													</div>
												</div> <br><br>
												<h2 class="purple-text text-center"><strong>SUCCESS !</strong></h2> <br>
												<div class="row justify-content-center">
												 <div class="col-3"> <i class="far fa-thumbs-up fit-image"></i> </div> 
												</div> <br><br>
												<div class="row justify-content-center">
													<div class="col-7 text-center">
														<h5 class="purple-text text-center">You Have Successfully Verified</h5>
													</div>
												</div>
											</div>
										</fieldset>
									</form>
								</div>
							</div>
		
						</div>  -->
					</div>
				</div>
			</div>
		</div>
	</section>



	<script src="{{base_url('assets_main/js/jquery-3.2.1.min.js')}}"></script>
	<script src="{{base_url('assets_main/js/bootstrap.min.js')}}"></script>
	{!!$assetHelper->link_plugins('js','bootstrap-3.3.7/js/bootstrap.min.js')!!}
	{!!$assetHelper->link_plugins('js','sidebar-nav/dist/sidebar-nav.min.js')!!}
	{!!$assetHelper->link_plugins('js','override/jquery.slimscroll.js')!!}
	{!!$assetHelper->link_plugins('js','override/waves.js')!!}
	{!!$assetHelper->link_plugins('js','override/custom.min.js')!!}
	{!!$assetHelper->link_plugins('js','sweetalert2-master/dist/sweetalert2.all.min.js')!!}
	{!!$assetHelper->link_plugins('js','bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js')!!}
	{!!$assetHelper->link_plugins('js','curioussolutions-datetimepicker/dist/DateTimePicker.min.js')!!}
    {!!$assetHelper->link_plugins('js','Inputmask-5.x/dist/jquery.inputmask.min.js')!!}
    {!!$assetHelper->link_plugins('js','bootstrap-formhelpers-phone/bootstrap-formhelpers-phone.js') !!}
    {!!$assetHelper->link_plugins('js','bootstrap-formhelpers-phone/bootstrap-formhelpers-phone.format.js') !!}
	{!!$assetHelper->link_plugins('js','dropzone-master/dist/dropzone.js')!!}
	{!!$assetHelper->link_plugins('js','global/global.js')!!}	
	<script src="https://www.cenpos.com/Plugins/porthole.min.js"></script>
	<script src="https://www.cenpos.com/Plugins/jquery.simplewebpay.js"></script> 
	<script src="https://unpkg.com/filepond/dist/filepond.min.js"></script>
	<script src="https://unpkg.com/jquery-filepond/filepond.jquery.js"></script>
	
	<script src="https://unpkg.com/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.min.js"></script>
	<script src="https://unpkg.com/filepond-plugin-image-exif-orientation/dist/filepond-plugin-image-exif-orientation.min.js"></script>
	<script src="https://unpkg.com/filepond-plugin-file-validate-type/dist/filepond-plugin-file-validate-type.js"></script>
	<script src="https://unpkg.com/filepond-plugin-file-encode/dist/filepond-plugin-file-encode.js"></script>	
	<script src="{{base_url('assets_main/js/main.js')}}"></script>
	<script  type="text/javascript" charset="utf-8">


	function CallbackSuccess(responseData) {
	alert(JSON.stringify(responseData));
	}
	function CallbackCancel(responseData) {
	alert(JSON.stringify(responseData));
	}

	$("#submit").click(function(){
		$("#NewCenposPlugin").submitAction();
	});

	$("#another_card").click(function(){
    	window.location.reload();        	
    });

	var merchant='{{CENPOST_MERCHANT_ID}}';
	var verify_params ='{{$verify_params["Data"]}}';
	$("#NewCenposPlugin").createWebpay({
	url: 'https://www.cenpos.net/simplewebpay/cards',
	params : "email=suncashme@gmail.com&verifyingpost="+verify_params+"&iscvv=true",
	height:'400px',
	// sessionToken:false,
	sessionToken:true,//for converycrypto
	isSameSite: 'Lax',

    beforeSend:function(xhr, textStatus) {
        //called when complete
	    loader2.showPleaseWait();
        $("#submit").prop('disabled', true);
      },
    complete: function(xhr, textStatus) {
        //called when complete
        $("#submit").prop('disabled', true);
        $("#submit").text("please wait..");
        loader2.hidePleaseWait();
      },
        success: function(data){
			console.table(data);
			 loader2.hidePleaseWait();
        	if(data.Result==0){
				$("#card_number").text(data.ProtectedCardNumber);
				$("#name_on_card").text(data.NameonCard);
				$("#card_type").text(data.CardType);
				$("#tid").val(data.RecurringSaleTokenId);
				$("#expiration").text(data.CardExpirationDate);
				$("#success_section").show();
				$("#card_info").hide();
        	} else {
				$("#tid").val('');
				swal(data.Message);	
        	}
        },
        cancel: function(response){
        	$("#tid").val('');
        	$("#submit").prop('disabled', false);
			 loader2.hidePleaseWait();
        	swal(response.Message);
				
        	if(response.Message!="Error validation Captcha"){
        		swal(response.Message);	
        		$("#success_section").hide();
        		$("#card_info").show();
        	} 
        }	
	});

	$("#link_card").click(function(e){
		// var file1 = JSON.parse($("#card_id_upload").find('[name=filepond]').val());
		// //form_data.append('card_id_upload', file1.data);
		// var file2 = JSON.parse($("#credit_card_upload").find('[name=filepond]').val());
		//form_data.append('credit_card_upload', file2.data);
		// var file3 = JSON.parse($("#cc_with_card_upload").find('[name=filepond]').val());
		//form_data.append('cc_with_card_upload', file3.data);
		//form_data.push({name: 'card_id_upload', value: file1.data});
		//form_data.push({name: 'credit_card_upload', value: file2.data});
		//form_data.push({name: 'cc_with_card_upload', value: file3.data});
		if($("#cc_with_card_upload .filepond--data").has('[name="filepond"]').length>0){

			var img = $("#cc_with_card_upload").find('[name=filepond]').val();
			var file3 = JSON.parse(img);
		}else{
			alert("Upload a Selfie with Card is required");
			return false;
		}

		var form_data ={
		//merchant:merchant,	
		tokenid:$("#tid").val(),
		merchant_key:'{{$merchant_key}}',
		merchant_customer_id:'{{$merchant_customer_id}}',
		card_type:$("#card_type").text(),
		card_number:$("#card_number").text(),
		name_on_card:$("#name_on_card").text(),
		expiration:$("#expiration").text(),
		card_id_upload:'',
		credit_card_upload:'',
		cc_with_card_upload:file3.data,
		} 
		$.ajax({
		url: '{{base_url("checkout_card/link_card_process")}}',
		type: 'POST',
		dataType: 'json',
		data: form_data,
		beforeSend:function(){
			// loader2.showPleaseWait();
			$("#link_card").prop("disabled",true);
			$("#link_card").text("Processing... Please wait");
			loader2.showPleaseWait();
		},
		complete: function(xhr, textStatus) {
			$("#link_card").prop("disabled",false);
			$("#link_card").text("");
			loader2.hidePleaseWait();
		},
		success: function(data, textStatus, xhr) {
			//console.log();
			if(data.success){
				swal(data.msg);
				$("#success_section").show();
				//$("#validate_section").show();
				$("#last4digits").val($("#card_number").text());
				$("#card_type_w").val($("#name_on_card").text());
				$("#name_on_card_w").val($("#card_type").text());
				$("#cn").text($("#card_number").text());
				//$("#tt").text();
				window.location.reload();
			// swal(response.Message);	
			} else {
				$("#link_card").prop("disabled",false);
				$("#link_card").text("Save/Link Card");
				loader2.hidePleaseWait();
				swal(data.msg);
				$(".cancel_section").show();
				$(".payment_btn_section").hide();
				$("#tid").val('');
			}
		},
		error: function(xhr, textStatus, errorThrown) {
				$("#link_card").prop("disabled",false);
				$("#link_card").text("Save/Link Card");
				loader2.hidePleaseWait();
				swal(data.msg);
				$(".cancel_section").show();
				$(".payment_btn_section").hide();
				$("#tid").val('');
			}
		});


});
//for modal validation of card
// $('.btn-process').on('click', function() {
// //alert("safari af");
// $(this).html('Submit');
// $progressContentDone = $('.progress-content.active').data('content');
// //alert($progressContentDone);
// if($progressContentDone==1){
//     if($("#card_id").val()=='' || $("#card_email").val()=='' || $("#card_mobile_number").val()=='' ){
//         swal('Fill up all required field/s.');
//         return false;
//     }
// }
// if(!isEmail($("#card_email").val())){
//         swal('Invalid Email format.');
//         return false;		
// }


// if($progressContentDone==2){
//     console.log('if');
//     $('.btn-gray').css({"opacity": "1"});    	
//     $('.hb').remove();
//     $('.required').removeClass('has-error-border').removeClass('has-success');

//     //check validation
//     var err_count = 0;
//     var to_req=[];
//     //jquery blank validation..
//     $("#whitelist_form .required").each(function(){
//         var field_id = $(this).attr("id");
//         var data=[];
//         if($(this).val()==""){
//           data['id']=field_id;      
//           to_req.push(data);
//           err_count++;
//         }
//     });

//     if(err_count>0){
//       swal(
//       'Oops...',
//       "Please do check required fields.",
//       'error'
//       );  
//       $.each(to_req, function( index, value ) {
//         $("#"+value.id).addClass('has-error-border');//.after('<div class="hb"><div class="help-block" style="color:#f38989; font-size:7pt;margin-bottom:0px !important;">Required filed.</div></div>')
//       });        
//       return false;
//     }    
//     //
//     //var form = new FormData();

//     if($("#card_id_upload").find('[name=filepond]').val()=='' || $("#credit_card_upload").find('[name=filepond]').val()=='' || $("#cc_with_card_upload").find('[name=filepond]').val()==''){
//       swal(
//       'Opps...',
//       "All photo are required",
//       'error'
//       ); 
//       return false;
//     }



//     var url = '{{base_url("payment/update_whitelist")}}';
//     //var formData = new FormData($("#whitelist_form")[0]);
//     //var formData = $("#whitelist_form").serializeArray();
//     var file1 = JSON.parse($("#card_id_upload").find('[name=filepond]').val());
//     //formData.append('card_id_upload', file1.data);
//     var file2 = JSON.parse($("#credit_card_upload").find('[name=filepond]').val());
//     //formData.append('credit_card_upload', file2.data);
//     var file3 = JSON.parse($("#cc_with_card_upload").find('[name=filepond]').val());
//     //formData.append('cc_with_card_upload', file3.data);	

// /*		formData.push({name: 'card_id_upload', value: file1.data});
//     formData.push({name: 'credit_card_upload', value: file2.data});
//     formData.push({name: 'cc_with_card_upload', value: file3.data});*/

//     //for (var i = 0; i < $('.my-pond input').length; i++) {

//     //}
//     var formData = {
//         last4digits:$("#last4digits").val(),
//         card_type_w:$("#card_type_w").val(),
//         card_name:$("#card_name").val(),
//         card_id:$("#card_id").val(),
//         card_email:$("#card_email").val(),
//         card_mobile_number:$("#card_email").val(),
//         card_id_upload:file1.data,
//         credit_card_upload:file2.data,
//         cc_with_card_upload:file3.data,
//         wid:$("#wid").val(),
//         merckey:$("#wid").val(),
//     };



// /*		formData.append('card_id_upload', $('#card_id_upload')[0].files[0]); 
//     formData.append('credit_card_upload', $('#credit_card_upload')[0].files[0]); 
//     formData.append('cc_with_card_upload', $('#cc_with_card_upload')[0].files[0]); */
//     var button = $("#submit_btn");

//     $.ajax({
//       url: url,
//       type: 'POST',
//       dataType: 'json',
//       data: formData,
//       //contentType: false,       
//       //cache: false,             
//       //processData:false,       
//       beforeSend: function(xhr, textStatus) {
//         //called when complete
//         //clearValidationArray();
//         loader.showPleaseWait();
//         $(button).prop('disabled',true);        
//       },          
//       complete: function(xhr, textStatus) {
//         //called when complete
//         loader.hidePleaseWait();
//         $(button).prop('disabled',false);        
//       },
//       success: function(data) {
//         //called when successful
//         if(data.success){
//             // swal(
//             // '',
//             // data.msg,
//             // 'success'
//             // );  

//         $('.progress-step[data-step="' + $progressContentDone + '"]').addClass('done').removeClass('active');
//         $('.progress-step.done .number').html('<i class="fas fa-check"></i>');
//         $('.progress-step[data-step="' + $progressContentDone + '"]').next().addClass('active');

//         $('.progress-content[data-content="'+$progressContentDone+'"]').addClass('done').removeClass('active');
//         $('.progress-content[data-content="'+$progressContentDone+'"]').next().addClass('active');

//             //hideModal("large");
//           $('#cardValidation .btn-container').remove();
//            $('.progress-content[data-content="3"]').addClass('done').removeClass('active');
//            $('.progress-step[data-step="3"]').addClass('done').removeClass('active');
//         $('.progress-step.done .number').html('<i class="fas fa-check"></i>');
//         $('.progress-content[data-content="3"]').addClass('done');

//         } else {
//             swal(
//             'Opps...',
//             data.msg,
//             'error'
//             );  
//         }     
//       },
//       error: function(xhr, textStatus, errorThrown) {
//         swal('something went wrong.');
//       }
//     });

// }
// //return false;
// if($progressContentDone!=2){
// $('.progress-step[data-step="' + $progressContentDone + '"]').addClass('done').removeClass('active');
// $('.progress-step.done .number').html('<i class="fas fa-check"></i>');
// $('.progress-step[data-step="' + $progressContentDone + '"]').next().addClass('active');

// $('.progress-content[data-content="'+$progressContentDone+'"]').addClass('done').removeClass('active');
// $('.progress-content[data-content="'+$progressContentDone+'"]').next().addClass('active');
// }

// });


$('.my-pond').filepond();
$.fn.filepond.registerPlugin(FilePondPluginImagePreview);
$.fn.filepond.registerPlugin(FilePondPluginFileValidateType);
$.fn.filepond.registerPlugin(FilePondPluginFileEncode);
// Turn input element into a pond with configuration options
$('.my-pond').filepond({
  allowMultiple: true,
  // labelIdle: '<button type="button" class="btn btn-upload"><i class="fas fa-upload"></i> Upload</button>',
  acceptedFileTypes: [
    'image/jpg',
    'image/jpeg',
    'image/png',
    ]
});

// Set allowMultiple property to true
$('.my-pond').filepond('allowMultiple', false);

// Listen for addfile event
$('.my-pond').on('FilePond:addfile', function(e) {
  console.log('file added event', e);
});

$(document).ready(function(){

var current_fs, next_fs, previous_fs; //fieldsets
var opacity;
var current = 1;
var steps = $(".fs").length;
//alert(steps);
setProgressBar(current);

$(".next").click(function(){//tbd function
if($(this).attr('v')==1){
		current_fs = $(this).parent();
		next_fs = $(this).parent().next();
		//console.table(next_fs);
		//Add Class Active
		$("#progressbar li").eq($(".fs").index(next_fs)).addClass("active");

		//show the next fieldset
		next_fs.show();
		//hide the current fieldset with style
		current_fs.animate({opacity: 0}, {
		step: function(now) {
		// for making fielset appear animation
		opacity = 1 - now;

		current_fs.css({
		'display': 'none',
		'position': 'relative'
		});
		next_fs.css({'opacity': opacity});
		},
		duration: 500
		});
		setProgressBar(++current);
}else if($(this).attr('v')==2){
    $('.hb').remove();
    $('.required_v2').removeClass('has-error-border').removeClass('has-success');

    //check validation
    var err_count = 0;
    var to_req=[];
    //jquery blank validation..
    $(".required_v2").each(function(){
        var field_id = $(this).attr("id");
        var data=[];
        if($(this).val()==""){
          data['id']=field_id;      
          to_req.push(data);
          err_count++;
        }
    });

    if(err_count>0){
      swal(
      'Oops...',
      "Please do check required fields.",
      'error'
      );  
      $.each(to_req, function( index, value ) {
        $("#"+value.id).addClass('has-error-border');//.after('<div class="hb"><div class="help-block" style="color:#f38989; font-size:7pt;margin-bottom:0px !important;">Required filed.</div></div>')
      });        
      return false;
    }    
	current_fs = $(this).parent();
		next_fs = $(this).parent().next();
		//console.table(next_fs);
		//Add Class Active
		$("#progressbar li").eq($(".fs").index(next_fs)).addClass("active");

		//show the next fieldset
		next_fs.show();
		//hide the current fieldset with style
		current_fs.animate({opacity: 0}, {
		step: function(now) {
		// for making fielset appear animation
		opacity = 1 - now;

		current_fs.css({
		'display': 'none',
		'position': 'relative'
		});
		next_fs.css({'opacity': opacity});
		},
		duration: 500
		});
		setProgressBar(++current);
}else if($(this).attr('v')==3){
	var formData =$('#msform').serializeArray();
    var file1 = JSON.parse($("#card_id_upload").find('[name=filepond]').val());
    //formData.append('card_id_upload', file1.data);
    var file2 = JSON.parse($("#credit_card_upload").find('[name=filepond]').val());
    //formData.append('credit_card_upload', file2.data);
    var file3 = JSON.parse($("#cc_with_card_upload").find('[name=filepond]').val());
    //formData.append('cc_with_card_upload', file3.data);
	formData.push({name: 'card_id_upload', value: file1.data});
    formData.push({name: 'credit_card_upload', value: file2.data});
    formData.push({name: 'cc_with_card_upload', value: file3.data});
	formData.push({name: 'merchant_key', value: '{{$merchant_key}}'});
	formData.push({name: 'merchant_customer_id', value: '{{$merchant_customer_id}}'});

	console.table(formData);
	$.ajax({
		type: "POST",
		url: "{{base_url('checkout/verify_card_process')}}",
		data: formData,
		dataType: "json",
		success: function (response) {
			if(response.success){
				//go the success
				current_fs = $(this).parent();
				next_fs = $(this).parent().next();
				//console.table(next_fs);
				//Add Class Active
				$("#progressbar li").eq($(".fs").index(next_fs)).addClass("active");
				//show the next fieldset
				next_fs.show();
				//hide the current fieldset with style
				current_fs.animate({opacity: 0}, {
				step: function(now) {
				// for making fielset appear animation
				opacity = 1 - now;

				current_fs.css({
				'display': 'none',
				'position': 'relative'
				});
				next_fs.css({'opacity': opacity});
				},
				duration: 500
				});
				setProgressBar(++current);
			} else {
				swal(data.msg);
			}
		}
	});
}
});

$(".previous").click(function(){//tdb function

current_fs = $(this).parent();
previous_fs = $(this).parent().prev();

//Remove class active
$("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

//show the previous fieldset
previous_fs.show();

//hide the current fieldset with style
current_fs.animate({opacity: 0}, {
step: function(now) {
// for making fielset appear animation
opacity = 1 - now;

current_fs.css({
'display': 'none',
'position': 'relative'
});
previous_fs.css({'opacity': opacity});
},
duration: 500
});
setProgressBar(--current);
});

function setProgressBar(curStep){
var percent = parseFloat(100 / steps) * curStep;
percent = percent.toFixed();
$(".progress-bar")
.css("width",percent+"%")
}

$(".submit").click(function(){
return false;
});

});

</script>

</body>
</html>