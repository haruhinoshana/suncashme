<?php
//loading assets helper
defined('BASEPATH') OR exit('No direct script access allowed');
$assetHelper = new AssetHelper\AssetHelper();
$ci=&get_instance();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>SunCash.Me</title>
	<link rel="stylesheet" href="{{base_url('assets_main/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/main.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/main_checkout.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/responsive.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/all.min.css')}}">
	<link rel="stylesheet" href="{{base_url('assets/plugins/global/global2.css')}}">
	<link href="https://unpkg.com/filepond/dist/filepond.css" rel="stylesheet">
	<link href="https://unpkg.com/filepond/dist/filepond.min.css" rel="stylesheet">
	<link href="https://unpkg.com/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.min.css" rel="stylesheet">	
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>assets/plugins/icon.ico">
	<style type="text/css" >
		.ïframecenpos{
			border:1px !important;
		}
		.layaoutPanelTokenCardForm .canvasbox {
		    margin-left: 10% !important;
		    margin-right:10% !important;
		    width: 100% !important;	

		}
		.Modern .canvasbox .row > span {
			/*webkit-font-smoothing: antialiased !important;*/
			webkit-font-smoothing: antialiased !important;
		    font-family: "Roboto", sans-serif !important;
		    font-weight: 400 !important;
		    font-size: 15px !important;
		    color: #292929 !important;
		    line-height: 1.5em !important;
		    margin-bottom: .5rem !important;
		}

		.Modern .canvasbox .row > input {
			/*webkit-font-smoothing: antialiased !important;*/
			webkit-font-smoothing: antialiased !important;
		    font-family: "Roboto", sans-serif !important;
		    font-weight: 400 !important;
		    font-size: 16px !important;
		    color: #292929 !important;
		    line-height: 1.5em !important;
		    margin-bottom: .5rem !important;
		}
	</style>

</head>
<body>
	<nav class="navbar navbar-expand-lg sticky-top bg-white shadow-sm p-0">
	  <div class="container p-0" role="navigation">
	  	<a class="navbar-brand" href="<?php echo base_url();?>"><img src="{{base_url('assets_main/imgs/header-logo@2x.png')}}" alt="Suncash Logo" class="d-inline-block align-top" width="160"></a>
	  	<a href="#" class="menu-button"></a>
	  	<div class="justify-content-end main-menu">
			<ul class="navbar-nav d-flex align-items-center">
		    </ul>
		  </div>
	  </div>
	</nav>
	<section class="" id="card_section">
		<div class="container  text-center">
			<div class="row d-flex justify-content-center align-items-center mb-5">
				<div class="user-card transaction-details" style="margin-top:1rem;margin-bottom:-34px !important;">
					<div class="header w-content" style="height:6rem !important;"></div>
					<div class="body">
					<div class="card-info">
						<form name="verify_form" id="verify_form" class="form-horizontal" method="POST" enctype="enctype/form-data">
							<div class="container-fluid">
								<br>
								<br>
								<div class="card-body">
								<p>
								This step will verify your card and confirm that the card is valid and that you are the card owner.  The name on card must match the SunCash account holder’s name.</p>

								<p>Send in a selfie (picture of yourself, showing your entire face) holding your card, covering the card number but showing the last 4 digits, the name and expiry date to kyc@mysuncash.com.   If you cannot make out the information on the card, please also send another photo of the card itself. Make sure the photo is clear.</p>

								<p>We will send you the credit card verification number once approved.</p>

								<img src="http://dev.suncash.me/customer/assets/imgs/card_verify_pic.png" width='90%'>
								<div class="col-12">
									<div class="message"><b>Verification Code</b></div>
									<input type="text" class="form-control" id="verification" name="verification" placeholder="Enter Code" required/>
								</div><!-- /.col -->

								</div>
							</div><!-- /.container-fluid -->

							<div class="modal-footer">
								<input type="hidden" id="c" name="c" value='{{$id}}'/>
								<input type="hidden" id="v" name="v" value='{{$v}}'/>
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
								<button type="submit" name="submit" class="btn btn-primary submit">Process Transaction</button>
							</div>
							</form>
			
					</div>
				</div>				
			</div>
		</div>
		<footer class="py-3 footer">
			<div class="container">
				<div class="row">
					<div class="col-12 col-lg-6">
						<span class="txt-shit">© {{date('Y')}} SunCash.Me. All Rights Reserved.</span>
					</div>
					<div class="col-12 col-lg-6 ">
						<img class="img-x" src="{{base_url('assets_main/imgs/footer-logo.png')}}">
					</div>
				</div>
			</div>
		</footer>		
	</section>



	<script src="{{base_url('assets_main/js/jquery-3.2.1.min.js')}}"></script>
	<script src="{{base_url('assets_main/js/bootstrap.min.js')}}"></script>
	{!!$assetHelper->link_plugins('js','bootstrap-3.3.7/js/bootstrap.min.js')!!}
	{!!$assetHelper->link_plugins('js','sidebar-nav/dist/sidebar-nav.min.js')!!}
	{!!$assetHelper->link_plugins('js','override/jquery.slimscroll.js')!!}
	{!!$assetHelper->link_plugins('js','override/waves.js')!!}
	{!!$assetHelper->link_plugins('js','override/custom.min.js')!!}
	{!!$assetHelper->link_plugins('js','sweetalert2-master/dist/sweetalert2.all.min.js')!!}
	{!!$assetHelper->link_plugins('js','bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js')!!}
	{!!$assetHelper->link_plugins('js','curioussolutions-datetimepicker/dist/DateTimePicker.min.js')!!}
    {!!$assetHelper->link_plugins('js','Inputmask-5.x/dist/jquery.inputmask.min.js')!!}
    {!!$assetHelper->link_plugins('js','bootstrap-formhelpers-phone/bootstrap-formhelpers-phone.js') !!}
    {!!$assetHelper->link_plugins('js','bootstrap-formhelpers-phone/bootstrap-formhelpers-phone.format.js') !!}
	{!!$assetHelper->link_plugins('js','dropzone-master/dist/dropzone.js')!!}
	{!!$assetHelper->link_plugins('js','global/global.js')!!}	
	<script src="https://www.cenpos.com/Plugins/porthole.min.js"></script>
	<script src="https://www.cenpos.com/Plugins/jquery.simplewebpay.js"></script> 
	<script src="https://unpkg.com/filepond/dist/filepond.min.js"></script>
	<script src="https://unpkg.com/jquery-filepond/filepond.jquery.js"></script>
	
	<script src="https://unpkg.com/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.min.js"></script>
	<script src="https://unpkg.com/filepond-plugin-image-exif-orientation/dist/filepond-plugin-image-exif-orientation.min.js"></script>
	<script src="https://unpkg.com/filepond-plugin-file-validate-type/dist/filepond-plugin-file-validate-type.js"></script>
	<script src="https://unpkg.com/filepond-plugin-file-encode/dist/filepond-plugin-file-encode.js"></script>	
	<script src="{{base_url('assets_main/js/main.js')}}"></script>
	<script  type="text/javascript" charset="utf-8">


	function CallbackSuccess(responseData) {
	alert(JSON.stringify(responseData));
	}
	function CallbackCancel(responseData) {
	alert(JSON.stringify(responseData));
	}

	$("#submit").click(function(){
		$("#NewCenposPlugin").submitAction();
	});

	$("#another_card").click(function(){
    	window.location.reload();        	
    });

	var merchant='{{CENPOST_MERCHANT_ID}}';
	var verify_params ='{{$verify_params["Data"]}}';
	$("#NewCenposPlugin").createWebpay({
	url: 'https://www.cenpos.net/simplewebpay/cards',
	params : "email=suncashme@gmail.com&verifyingpost="+verify_params+"&iscvv=true",
	height:'400px',
	sessionToken:false,
	isSameSite: 'Lax',
	// type3d:'FunctionAuto',
	// cardinalReturn:"handle3DSecure",

    beforeSend:function(xhr, textStatus) {
        //called when complete
	    loader.showPleaseWait();
        $("#submit").prop('disabled', true);
      },
    complete: function(xhr, textStatus) {
        //called when complete
        $("#submit").prop('disabled', true);
        $("#submit").text("please wait..");
        loader.hidePleaseWait();
      },
        success: function(data){
   
        	if(data.Result==0){


        	$("#card_number").text(data.ProtectedCardNumber);
        	$("#name_on_card").text(data.NameonCard);
        	$("#card_type").text(data.CardType);
        	$("#tid").val(data.RecurringSaleTokenId);
        	$("#success_section").show();
        	$("#card_info").hide();
        	} else {
        	$("#tid").val('');
        	swal(data.Message);	

        	}
        },
        cancel: function(response){
        	$("#tid").val('');
        	$("#submit").prop('disabled', false);
        	swal(response.Message);	
        	if(response.Message!="Error validation Captcha"){
        		swal(response.Message);	
        		$("#success_section").hide();
        		$("#card_info").show();
        	} 
        }	
	});



	</script>

</body>
</html>