<?php
//loading assets helper
defined('BASEPATH') OR exit('No direct script access allowed');
$assetHelper = new AssetHelper\AssetHelper();
$ci=&get_instance();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Save/Link Card</title>
	<link rel="stylesheet" href="{{base_url('assets_main/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/main.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/main_checkout.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/responsive.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/all.min.css')}}">
	<link rel="stylesheet" href="{{base_url('assets/plugins/global/global2.css')}}">	
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>assets/plugins/icon.ico">
	<style type="text/css" >
		.ïframecenpos{
			border:1px !important;
		}
		.layaoutPanelTokenCardForm .canvasbox {
		    margin-left: 10% !important;
		    margin-right:10% !important;
		    width: 100% !important;	

		}
		.Modern .canvasbox .row > span {
			webkit-font-smoothing: antialiased !important;
		    font-family: "Roboto", sans-serif !important;
		    font-weight: 400 !important;
		    font-size: 15px !important;
		    color: #292929 !important;
		    line-height: 1.5em !important;
		    margin-bottom: .5rem !important;
		}

		.Modern .canvasbox .row > input {
			webkit-font-smoothing: antialiased !important;
		    font-family: "Roboto", sans-serif !important;
		    font-weight: 400 !important;
		    font-size: 16px !important;
		    color: #292929 !important;
		    line-height: 1.5em !important;
		    margin-bottom: .5rem !important;
		}

		.user-card .body:before { 
            all: unset; 
        }
	</style>

</head>
<body>
	<section class="" id="card_section">
		<div class="container  text-center">
			<div class="row d-flex justify-content-center align-items-center">
				<div class="user-card transaction-details" style='margin-top: 2rem;'>
					<div class="body" style="background-color: #e9ecef;">
						<div id="validate_section" style="display:block;">
						<br>
							<h4> Credit and Debit Card Verification</h4>
							<p>We will charge your card a very small amount between $0.01 to $2.00 from SunCash. Check your online banking portal or contact your bank and enter the exact amount charged below. Once confirmed, your card will be verified and the transaction amount will be voided. </p>
							The following charge has been made on your card:
							<b>
								<div class="item">
								<div class="row">
									<div class="col text-left text-details">Card Last 4 digit number: </div>
								<div class="col text-right"><span id="cn">{{$cdata['card_last_four_digits']}}</span></div>
								</div>
								</div>
								<div class="item">
									<div class="row">
										<div class="col text-left text-details">Amount: </div>
										<div class="col text-right a"><span id="a">Between 0 to 2.00</span> </div>
									</div>
								</div>
								<div class="item">
									<div class="row">
										<div class="col text-left text-details">Currency: </div>
										<div class="col text-right currency"><span id="currency">USD</span> </div>
									</div>
								</div>
							</b>
							<H4>Enter Amount</H4>
							  <input class="input-amount" type="text" id="amount" name="amount" value="0.00" style="height: 25% !important; margin-bottom:0px !important;margin-top: 0px !important;border:1px solid !important;">
							  *Transaction amounts voided automatically after 7 days, 3 failed attempts or successful verification..
							  <div class="payment_btn_section">
								<div class="text-center"><button type="button" class="btn btn-orange full" id="verify_card_btn">Verify Card</button>
							  </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>



	<script src="{{base_url('assets_main/js/jquery-3.2.1.min.js')}}"></script>
	<script src="{{base_url('assets_main/js/bootstrap.min.js')}}"></script>
	{!!$assetHelper->link_plugins('js','bootstrap-3.3.7/js/bootstrap.min.js')!!}
	{!!$assetHelper->link_plugins('js','sidebar-nav/dist/sidebar-nav.min.js')!!}
	{!!$assetHelper->link_plugins('js','override/jquery.slimscroll.js')!!}
	{!!$assetHelper->link_plugins('js','override/waves.js')!!}
	{!!$assetHelper->link_plugins('js','override/custom.min.js')!!}
	{!!$assetHelper->link_plugins('js','sweetalert2-master/dist/sweetalert2.all.min.js')!!}
	{!!$assetHelper->link_plugins('js','bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js')!!}
	{!!$assetHelper->link_plugins('js','curioussolutions-datetimepicker/dist/DateTimePicker.min.js')!!}
    {!!$assetHelper->link_plugins('js','Inputmask-5.x/dist/jquery.inputmask.min.js')!!}
    {!!$assetHelper->link_plugins('js','bootstrap-formhelpers-phone/bootstrap-formhelpers-phone.js') !!}
    {!!$assetHelper->link_plugins('js','bootstrap-formhelpers-phone/bootstrap-formhelpers-phone.format.js') !!}
	{!!$assetHelper->link_plugins('js','dropzone-master/dist/dropzone.js')!!}
	{!!$assetHelper->link_plugins('js','global/global.js')!!}	
	<script src="https://www.cenpos.com/Plugins/porthole.min.js"></script>
	<script src="https://www.cenpos.com/Plugins/jquery.simplewebpay.js"></script> 
	<script src="{{base_url('assets_main/js/main.js')}}"></script>
	<script  type="text/javascript" charset="utf-8">

	$("#amount").inputmask({ 'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true, 'digits': 2, 'digitsOptional': false, 'placeholder': '0.00', rightAlign : false,clearMaskOnLostFocus: !1,   });

	$("#verify_card_btn").click(function(){
			<?php $d =base64_encode(serialize($cdata)); ?>
          	<?php  $url_x = base_url("merchant_checkout/checkoutv2/").base64_encode($merchant_key.'||'.$reference_id.'||'.$merchant_customer_id);?>
		var form_data ={
		//merchant:merchant,	
		t:'{{$d}}',
		amount:$("#amount").val(),
        ru: '{{$url_x}}' 
          
		} 
		$.ajax({
		url: '{{base_url("checkout_card/process_verify")}}',
		type: 'POST',
		dataType: 'json',
		data: form_data,
		beforeSend:function(){
			// loader2.showPleaseWait();
			$("#link_card").prop("disabled",true);
			$("#link_card").text("Processing... Please wait");
			loader.showPleaseWait();
		},
		complete: function(xhr, textStatus) {
			$("#link_card").prop("disabled",false);
			$("#link_card").text("");
			loader.hidePleaseWait();
		},
		success: function(data, textStatus, xhr) {
			//console.log();
			if(data.success){
					swal(data.msg);
					$("#success_section").hide();
					//$("#validate_section").show();
					$("#last4digits").val($("#card_number").text());
					$("#card_type_w").val($("#name_on_card").text());
					$("#name_on_card_w").val($("#card_type").text());
					$("#cn").text($("#card_number").text());
						// window.location.reload();
					//$("#tt").text();
					  window.location.href='{{$url_x}}';
	
			} else {
				$("#link_card").prop("disabled",false);
				$("#link_card").text("Save/Link Card");
				loader.hidePleaseWait();
				swal(data.msg);
				$(".cancel_section").show();
				$(".payment_btn_section").hide();
				$("#tid").val('');
				window.location.reload();

			}
		},
		error: function(xhr, textStatus, errorThrown) {
				$("#link_card").prop("disabled",false);
				$("#link_card").text("Save/Link Card");
				swal(data.msg);
				$(".cancel_section").show();
				$(".payment_btn_section").hide();
				$("#tid").val('');
		}
		});
	});
	</script>

</body>
</html>