<?php
//loading assets helper
defined('BASEPATH') OR exit('No direct script access allowed');
$assetHelper = new AssetHelper\AssetHelper();
$ci=&get_instance();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>SunCash.Me</title>
	<link rel="stylesheet" href="{{base_url('assets_main/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/main.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/main_checkout.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/responsive.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/all.min.css')}}">
	<link rel="stylesheet" href="{{base_url('assets/plugins/global/global2.css')}}">
	<link href="https://unpkg.com/filepond/dist/filepond.css" rel="stylesheet">
	<link href="https://unpkg.com/filepond/dist/filepond.min.css" rel="stylesheet">
	<link href="https://unpkg.com/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.min.css" rel="stylesheet">	
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>assets/plugins/icon.ico">
	<style type="text/css" >
		.ïframecenpos{
			border:1px !important;
		}
		.layaoutPanelTokenCardForm .canvasbox {
		    margin-left: 10% !important;
		    margin-right:10% !important;
		    width: 100% !important;	

		}
		.Modern .canvasbox .row > span {
			/*webkit-font-smoothing: antialiased !important;*/
			webkit-font-smoothing: antialiased !important;
		    font-family: "Roboto", sans-serif !important;
		    font-weight: 400 !important;
		    font-size: 15px !important;
		    color: #292929 !important;
		    line-height: 1.5em !important;
		    margin-bottom: .5rem !important;
		}

		.Modern .canvasbox .row > input {
			/*webkit-font-smoothing: antialiased !important;*/
			webkit-font-smoothing: antialiased !important;
		    font-family: "Roboto", sans-serif !important;
		    font-weight: 400 !important;
		    font-size: 16px !important;
		    color: #292929 !important;
		    line-height: 1.5em !important;
		    margin-bottom: .5rem !important;
		}

		@media only screen and (max-width: 768px)  {
				.img-x {
				  display: block;
				  margin-left: auto;
				  margin-right: auto;
				  //width: 100%;
				  min-width:128px;
				  min-height:43px;
				}
				.txt-shit{
					text-align:center !important;
				}
		}		
		
/*		#cenposPayIFrameId {
			    width: 85% !important;	

		}*/
	</style>

</head>
<body>
	<nav class="navbar navbar-expand-lg sticky-top bg-white shadow-sm p-0">
	  <div class="container p-0" role="navigation">
	  	<a class="navbar-brand" href="index.html"><img src="{{base_url('assets_main/imgs/header-logo@2x.png')}}" alt="Suncash Logo" class="d-inline-block align-top" width="160"></a>
	  	<a href="#" class="menu-button"></a>
	  	<div class="justify-content-end main-menu">
				<ul class="navbar-nav d-flex align-items-center">


		    </ul>
		  </div>
	  </div>
	</nav>
	<section class="" id="card_section">
		<div class="container  text-center">
			<div class="row d-flex justify-content-center align-items-center mb-5">
				<div class="user-card transaction-details" style="margin-top:1rem;margin-bottom:-34px !important;">
					<div class="header w-content" style="height:6rem !important;"></div>
					<div class="body">
						<form id="cashpayment_form" method="POST">
				        <div class="user-image ">
								@if(!empty($_SESSION['profile_pic']))
								<img src="{{$_SESSION['profile_pic']}}">
								@else
								<img src="{{base_url('assets_main/imgs/suncash-icon.png')}}">
								@endif
						</div>
						<div class="details" style="margin-bottom: -40px;">

							<div class="message">You're about to give <?=ucfirst($_SESSION['suntag_shortcode']) ?></div>
							<select name="payment_method" id="payment_method" >
								<option value="">BSD</option>
							</select>
							<input class="input-amount" type="text" id="amount" name="amount" value="{{$payment_data['amount']}}" style="height: 100% !important">

						</div>

						<div class="text-left">
							<div class="item" style="padding-bottom: 0px; margin-bottom:0px; margin-top:0px !important">
								<input type="hidden" class="form-control" id="reference_num" name="reference_num"  value="{{$payment_data['reference_num']}}" placeholder="Reference # (Order, Quote, Invoice or Account)" >
						
								<div class="form-group">
						          <input type="hidden" class="form-control" id="name" name="name" placeholder="Donor Name" autocomplete="off" style="resize:none" value="{{$payment_data['name']}}" >
								</div>	
								<div class="form-group">
						          <input type="hidden" class="form-control" id="mobile" name="mobile" placeholder="Email" autocomplete="off" style="resize:none" value="{{$payment_data['mobile']}}" >
								</div>										
								<div class="form-group">
						          <input type="hidden" class="form-control" id="email" name="email" placeholder="Email" autocomplete="off" style="resize:none" value="{{$payment_data['email']}}" >
								</div>	
								<div class="form-group">
						          <input type="hidden" class="form-control" id="notes" name="notes" placeholder="Notes" autocomplete="off" style="resize:none" value="{{$payment_data['notes']}}" >
								</div>

							</div>
						<!-- </div>
						<div class="text-left"> -->
						<div style="display: none">
							<div class="item primary-border" >
							  	<div class="label" style="margin-bottom: :0px !important">Transaction Details:</div>
								  	<div class="item" style="padding-bottom: 0px; margin-bottom:0px; margin-top:0px !important">
									  <div class="row text-details">
										  	<div class="col">Principal</div>
										  	<div class="col text-right ">$ <span class="amount_val">{{$payment_data['amount']}}</span></div>
									  </div> 
									</div> 							
							</div>
						  <div class="item total">
							  <div class="row">
								  	<?php 
								  		$total = str_replace( ',', '', $payment_data['amount']);
								  	?>
								  	<div class="col text-left label">Total Due</div>
								  	<div class="col text-right amount"><span class="amount_total">{{$total}}</span> BSD</div>
							  </div>
						  </div>
						</div>

			        	<div id="card_info" style="border-top: 1px solid #F0F0F0;"><br>
						    <h3>Enter your card details</h3>
						    <span>We dont share your financial details with merchant.</span>
							<div>
							    
							    <form action="" class="text-left">
								<div id="NewCenposPlugin">
								</div>
								<div class="text-center"><button type="button" class="btn btn-orange full" id="submit">Submit</button></div><br>
								<!-- <div class="text-center"><button class="btn btn-primary">Next</button></div> -->
								</form>
							</div>
						</div>
						<div id="success_section" style="display:none;">
							<hr>
					  		<div class="label text-center">Card Details</div>
						  		<div class="text-left">
								  	<div class="item">
									  <div class="row text-details">
									  	<div class="col text-left">Card Number: </div>
									  	<div class="col text-right"><span id="card_number"></span></div>
									  </div>
									</div>
									<div class="item">
									  <div class="row text-details">
									  	<div class="col text-left">Name on Card: </div>
									  	<div class="col text-right"><span id="name_on_card"></span></div>
									  </div>
									</div>
								  	<div class="item total">
									  <div class="row text-details">
									  	<div class="col text-left">Card Type: </div>
									  	<div class="col text-right amount"><span id="card_type"></span> </div>
									  </div>
								  	</div>
								  </div>

						  		<input type="hidden" name="tid" id ="tid" value="">
						  	<div class="payment_btn_section">
						  		<div class="text-center"><button type="button" class="btn btn-orange full" id="payment">Process Donation</button></div>
						  	</div>
						  	<div class="cancel_section" style="display:none;">
						  			<div class="col text-center"><button type="button" class="btn btn-orange full" id="another_card">Use Another Card</button></div></div>	
						</div>
					</div>


						</form>
					</div>
				</div>
			</div>
		</div>
		<footer class="py-3 footer">
			<div class="container">
				<div class="row">
					<div class="col-12 col-lg-6">
						<span class="txt-shit">© {{date('Y')}} SunCash.Me. All Rights Reserved.</span>
					</div>
					<div class="col-12 col-lg-6 ">
						<img class="img-x" src="{{base_url('assets_main/imgs/footer-logo.png')}}">
					</div>
				</div>
			</div>
		</footer>		
	</section>

	<section class="full" id="success_section_transaction" style=" display:none ;">
		<div class="container  text-center">
			<div class="row d-flex justify-content-center align-items-center mb-5">
				<div class="user-card success-card" style="margin-bottom:60px !important;margin-top:1rem;">
					<div class="header w-content" style="margin-top:1rem;margin-bottom:-34px !important;"></div>
					<div class="body">
						<div class="user-image">
							<img src="{{base_url('assets_main/imgs/success-icon.png')}}">
						</div>
						<div class="details" id = "transaction-details"style="margin-top:1rem;margin-bottom:-34px !important;">
							<div class="message" style="font-size: 2rem;line-height: 2.1rem;width: 90%;margin: 0 auto;">You Donated <span class="amount_val" style="color: #FF8400;">$ {{$payment_data['amount']}}</span> to <?=ucfirst($_SESSION['suntag_shortcode'])?></div>
						</div>
						<!-- <form action="" class="text-left"> -->
						<!-- <div class="label text-center">Transaction Details</div>	 -->
						<div class="text-left">
							  	<div class="item">
								  <div class="row">
								  	<div class="col">Transaction ID: <span id="transaction_code"></span></div>
								  	<div class="col text-right">{{date("d M Y")}}</div>
								  </div>
								</div>
								<div class="item">
								  <div class="row">
								  	<div class="col">Payment Method</div>
								  	<div class="col text-right">Debit/Credit Card Payment</div>
								  </div>
								</div>
								<div class="item">
								  <div class="row">
								  	<div class="col">Principal</div>
								  	<div class="col text-right">$ <span class="amount_val">{{$payment_data['amount']}}</span></div>
								  </div>
								</div>								
	<!-- 							<div class="item">
								  <div class="row">
								  	<div class="col">Transaction Fee</div>
								  	<div class="col text-right">$ <span class="fee_val">0.00</span></div>
								  </div>
								</div>	 -->						
	<!-- 							<div class="item">
								  <div class="row">
								  	<div class="col">Vat</div>
								  	<div class="col text-right">$ <span class="vat_val">{{$fee_data['vat_charge']}}</div>
								  </div>
								</div> -->
	<!-- 							<div class="item">
								  <div class="row">
								  	<div class="col">Convenience Fee</div>
								  	<div class="col text-right">$ <span class="conviniecefee_val">0.00</span></div>
								  </div>
								</div>	 -->							
							  <div class="item total">
								  <div class="row">
								  	<div class="col label">Total Due</div>
								  	<div class="col text-right amount"><span class="amount_val">{{$payment_data['amount']}}</span> BSD</div>
								  	<input type="hidden" id="amount_total" name="amount_total" value="{{$total}}" />
								  	<input type="hidden" id="hid_fee" name="hid_fee" value="0.00" />
								  	<input type="hidden" id="hid_vat" name="hid_vat" value="0.00" />
								  	<input type="hidden" id="hid_pfee" name="hid_pfee" value="0.00" />
								  	<input type="hidden" id="hid_tfee" name="hid_tfee" value="0.00" />
								  	<input type="hidden" id="hid_totalfee" name="hid_totalfee" value="0.00" />							  	
								  </div>
							  </div>						  
						</div>
						<!-- </form> -->

					</div>
				</div>
			</div>
		</div>
	</section>
	<div class="modal fade" id="cardValidation" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Credit Card Validation</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      	<form name="whitelist_form" id="whitelist_form" class="form-horizontal" method="POST" enctype="enctype/form-data">
		        <div class="progress">
							<div class="progress-inner">
								<div class="progress-step active" data-step="1">
									<div class="number">1</div>
									<div class="label">Basic Information</div>
								</div>

								<div class="progress-step" data-step="2">
									<div class="number">2</div>
									<div class="label">Upload Documents <br>and Selfie</div>
								</div>

								<div class="progress-step" data-step="3">
									<div class="number">3</div>
									<div class="label">Confirmation</div>
								</div>
							</div>
						</div>

						<div class="progress-container">
							<div class="progress-content active" data-content="1">
								<div class="container px-4">
									<div class="row card-section-title align-items-center">
										<div class="col-12 col-lg-6 p-0">
											<span >Basic Information</span>
										</div>
									</div>

								 <div class="form-group">
								    <label for="cardNumber">Last 4 Digit Card Number:</label>
								    <input type="text" class="form-control input-sm required" id="last4digits" name="last4digits"  readonly="" />
								  </div>
								  <div class="form-group two-input">
								    <label for="type">Type:</label>
								    <input type="text" class="form-control input-sm required" id="card_type_w" name="card_type_w" readonly=""   />
								  </div>
								  <div class="form-group">
								    <label for="cardName">Name on Card:</label>
								    <input type="text" class="form-control input-sm required" id="card_name" name="card_name" readonly=""  />
								  </div>
								  <div class="form-group">
								    <label for="cardID">* ID Number:</label>
								    <input type="text" class="form-control input-sm required" id="card_id" name="card_id"   />
								  </div>
								  <div class="form-group">
								    <label for="cardEmail">* Email:</label>
								    <input type="email" class="form-control input-sm required" id="card_email" name="card_email"  />
								  </div>
								  <div class="form-group">
								    <label for="cardMNumber">* Mobile Number:</label>
									<input type="text" class="form-control bfh-phone" id="card_mobile_number" name="card_mobile_number" data-format="1 (ddd) ddd-dddd" value ="1 242" >
								  </div>
								</div>
							</div>
							<div class="progress-content" data-content="2">
								<div class="container px-4">
									<div class="row card-section-title align-items-center">
										<div class="col-12 col-lg-6 p-0">
											<span >Upload ID and Selfies</span>
										</div>
									</div>
								
									<div class="upload-container">
									 	<div class="form-group mb-5">
									    <label>Upload a Scan of your ID</label>
									    <div class="info"><i class="fas fa-exclamation-circle"></i> Please upload a Government issued ID such as  Passport, current Driver’s License, or National ID Card.</div>

									    <div class="row">
									    	<div class="col-12 col-lg-6">
													<input type="file" class="my-pond" name="filepond1" id="card_id_upload" accept="image/jpeg, image/png" />
									    	</div>
									    	<div class="col-12 col-lg-6">
													<div class="illustration-image text-center">
														<div>
															<img src="{{base_url()}}assets_checkout/imgs/card-id.png" alt="">
															<span class="d-block">Example</span>
														</div>
													</div>
									    	</div>
									    </div>
									  </div>
									  <div class="form-group mb-5">
									    <label>Upload a Scan of your Debit/Credit Card</label>
									    <div class="info"><i class="fas fa-exclamation-circle"></i> Please upload the debit/credit card you used with a clear view of your last 4 digit card number, name and expiry date</div>
									    <div class="row">
									    	<div class="col-12 col-lg-6">
													<input type="file" class="my-pond" name="filepond2" id="credit_card_upload"/>
									    	</div>
									    	<div class="col-12 col-lg-6">
													<div class="illustration-image text-center">
														<div>
															<img src="{{base_url()}}assets_checkout/imgs/debit-card.png" alt="">
															<span class="d-block">Example</span>
														</div>
													</div>
									    	</div>
									    </div>
									  </div>
									  <div class="form-group">
									    <label>Upload a Credit Card with Card ID</label>
									    <div class="info"><i class="fas fa-exclamation-circle"></i> Please upload a clear photo of you with the debit/credit card and your Card ID.</div>

									    <div class="row">
									    	<div class="col-12 col-lg-6">
													<input type="file" class="my-pond" name="filepond3" id="cc_with_card_upload" accept="image/jpeg, image/png"/>
									    	</div>
									    	<div class="col-12 col-lg-6">
													<div class="illustration-image text-center">
														<div>
															<img src="{{base_url()}}assets_checkout/imgs/user-hold-cards.png" alt="">
															<span class="d-block">Example</span>
														</div>
													</div>
									    	</div>
									    </div>
									  </div>
									</div>
								</div>
							</div>
							<div class="progress-content success-screen" data-content="3">
								<div class="container px-4">
									<div class="row">
										<div class="col mt-2">
											<i class="far fa-thumbs-up"></i>
											<div class="validation-message">Validation submitted!</div>

											<center><button class="btn btn-primary" id="okay_btn"  type="button">OK</button></center>

										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="container btn-container  py-5">
							<div class="row align-items-center">
								<div class="col-6 mb-1">
									<button type='button' class="btn btn-close" data-dismiss="modal">Close</button>
								</div>
								<div class="col-6 text-right">
									<input type="hidden" name="wid" id="wid" />
	      							<input type="hidden" name="merckey" id="merckey" value="{{$_SESSION['merchant_key']}}"/>
									<button class="btn btn-primary btn-process" id="submit_btn"  type="button">Next</button>
								</div>
							</div>
						</div>
					</form>
	      </div>    
	  </div>
	</div>



	<script src="{{base_url('assets_main/js/jquery-3.2.1.min.js')}}"></script>
	<script src="{{base_url('assets_main/js/bootstrap.min.js')}}"></script>
<!-- 	 -->
    <!-- <script src="{{base_url('assets_main/js/select.min.js')}}"></script> -->
<!-- 	{!!$assetHelper->link_plugins('js','jquery-1.11.3.min.js')!!}
	{!!$assetHelper->link_plugins('js','bootstrap-3.3.7/js/bootstrap.min.js')!!} -->
	{!!$assetHelper->link_plugins('js','sidebar-nav/dist/sidebar-nav.min.js')!!}
	{!!$assetHelper->link_plugins('js','override/jquery.slimscroll.js')!!}
	{!!$assetHelper->link_plugins('js','override/waves.js')!!}
	{!!$assetHelper->link_plugins('js','override/custom.min.js')!!}
	{!!$assetHelper->link_plugins('js','sweetalert2-master/dist/sweetalert2.all.min.js')!!}
	{!!$assetHelper->link_plugins('js','bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js')!!}
	{!!$assetHelper->link_plugins('js','curioussolutions-datetimepicker/dist/DateTimePicker.min.js')!!}
    {!!$assetHelper->link_plugins('js','Inputmask-5.x/dist/jquery.inputmask.min.js')!!}
    {!!$assetHelper->link_plugins('js','bootstrap-formhelpers-phone/bootstrap-formhelpers-phone.js') !!}
    {!!$assetHelper->link_plugins('js','bootstrap-formhelpers-phone/bootstrap-formhelpers-phone.format.js') !!}
	{!!$assetHelper->link_plugins('js','dropzone-master/dist/dropzone.js')!!}
	{!!$assetHelper->link_plugins('js','global/global.js')!!}	
	<script src="https://www.cenpos.com/Plugins/porthole.min.js"></script>
	<script src="https://www.cenpos.com/Plugins/jquery.simplewebpay.js"></script> 
	<script src="https://unpkg.com/filepond/dist/filepond.min.js"></script>
	<script src="https://unpkg.com/jquery-filepond/filepond.jquery.js"></script>
	
	<script src="https://unpkg.com/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.min.js"></script>
	<script src="https://unpkg.com/filepond-plugin-image-exif-orientation/dist/filepond-plugin-image-exif-orientation.min.js"></script>
	<script src="https://unpkg.com/filepond-plugin-file-validate-type/dist/filepond-plugin-file-validate-type.js"></script>
	<script src="https://unpkg.com/filepond-plugin-file-encode/dist/filepond-plugin-file-encode.js"></script>	
	<script src="{{base_url('assets_main/js/main.js')}}"></script>
	<script src="{{base_url('assets_main/js/main.js')}}"></script>


	<script  type="text/javascript" charset="utf-8">
	function isEmail(email) {
	  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	  return regex.test(email);
	}

	function check_ccwhitelist(card_info){
		// alert("x");
		//
		var mkey = "{{$_SESSION['suntag_shortcode']}}";
		@if($_SESSION['tag']=='MERCHANT')
		mkey = "{{$_SESSION['merchant_key']}}";
		@endif
		var formdata={
			ProtectedCardNumber:card_info.ProtectedCardNumber,
			NameonCard:card_info.NameonCard,
			CardType:card_info.CardType,
			merchant_key:mkey,
			source:'payment',
			amount:$("#amount_total").val(),
		};

		$.ajax({
		  url: '{{base_url("payment/check_whitelist")}}',
		  type: 'POST',
		  dataType: 'json',
		  data: formdata,
		  beforeSend: function(xhr, textStatus) {
		    //called when complete
		    //$("#card_info").hide();
		    loader.showPleaseWait();
		  },		  
		  complete: function(xhr, textStatus) {
		    //called when complete
		    loader.hidePleaseWait();
		  },
		  success: function(data, textStatus, xhr) {
		    //called when successful
		    if(!data.success){
		    	//alert("repeat data");
		    	if(data.status=='active'){
			    swal({
			      title: 'We need more information to process this order.',
			      // text: "We need more information to verify your card ending with "+card_info.ProtectedCardNumber+" to continue this order.",
			      type: 'warning',
			      showCancelButton: true,
			      confirmButtonColor: '#FF8400',
			      cancelButtonColor: '#d33',
			      confirmButtonText: 'Proceed'
			    }).then((result) => {
			      if (result.value) {
			      	//alert("validate");

					$("#last4digits").val(card_info.ProtectedCardNumber);
					$("#card_type_w").val(card_info.CardType);
					$("#card_name").val(card_info.NameonCard);	
					$("#wid").val(data.data.id);			      	

			      	$("#cardValidation").modal('show');

			      } /*else{
			      	alert("cancelled");
			      }*/	
			    });
				} else if (data.status=='rejected'){
					swal("Unfortunately, your card was rejected and not authorized to continue this transaction.");
				} else if (data.status=='for_approval'){
					swal("Card not validated yet");
				}

		    	$("#card_info").show();
		    } else {
        	$("#card_number").text(card_info.ProtectedCardNumber);
        	$("#name_on_card").text(card_info.NameonCard);
        	$("#card_type").text(card_info.CardType);
        	$("#tid").val(card_info.RecurringSaleTokenId);
        	$("#success_section").show();
        	$("#card_info").hide();
		    }
		  },
		  error: function(xhr, textStatus, errorThrown) {
		    //called when there is an error
		  }
		});
		
	}

	function getcfee(amount){
		//e.preventDefault();
		//e.stopImmediatePropagation();				
	    // accordion is open
	    var total_billpay_fees = parseFloat($("#amount").val().replace(/,/g, ''))+parseFloat($("#hid_fee").val())+parseFloat($("#hid_vat").val())
	    $.ajax({
	      url: '{{base_url("paymentcharity/get_fees_payment")}}',
	      type: 'POST',
	      dataType: 'json',
	      data:{amount:total_billpay_fees,client_id:'{{$_SESSION["client_record_id"]}}'},
	   	  beforeSend:function(xhr, textStatus) {
	        //called when complete
		    loader.showPleaseWait();
	        $("#submit").prop('disabled', true);
	      },		  
		  complete: function(xhr, textStatus) {
		    //called when complete
		  	loader.hidePleaseWait();
		  },
	      success: function(data, textStatus, xhr) {
	        //called when successful
	        //paste values
	        if(data.success){
	        	$(".conviniecefee_val").text(data.fee);
	        	//$("#amount").text(data.total);
				var total= total_billpay_fees+parseFloat(data.fee);
				$(".amount_total").text(total.toFixed(2));
				$("#amount_total").val(total.toFixed(2));
				$("#hid_pfee").val(data.pf);
				$("#hid_tfee").val(data.tf);
				$("#hid_totalfee").val(data.fee);

		    	$("#submit").prop('disabled', false);				
	        }  else {
	        	swal(data.msg);
		    	$("#submit").prop('disabled', false);
	        }
	        
	      },
	      error: function(xhr, textStatus, errorThrown) {
	        //called when there is an error
        	swal(data.msg);
	    	$("#submit").prop('disabled', false);	        
	      }
	    });
	}
	@if($_SESSION['tag']=='MERCHANT')
	getcfee('{{$payment_data["amount"]}}');
	@endif
	function CallbackSuccess(responseData) {
	alert(JSON.stringify(responseData));
	}
	function CallbackCancel(responseData) {
	alert(JSON.stringify(responseData));
	}

	$("#submit").click(function(){
		$("#NewCenposPlugin").submitAction();
	});

	var merchant='{{CENPOST_MERCHANT_ID}}';
	var verify_params ='{{$verify_params["Data"]}}';
	var verify_params ='{{$verify_params["Data"]}}';
	$("#NewCenposPlugin").createWebpay({
	url: 'https://www.cenpos.net/simplewebpay/cards',
	params : "email=suncashme@gmail.com&verifyingpost="+verify_params+"&iscvv=false",
	height:'400px',
	//isCvv :true&SecretKey=a0c70a0d5aa451bfc02f17e9199e41e6&verifyingpost="+vp
	sessionToken:false,
	    beforeSend:function(xhr, textStatus) {
	        //called when complete
		    loader.showPleaseWait();
	        $("#submit").prop('disabled', true);
	      },
	    complete: function(xhr, textStatus) {
	        //called when complete
	        $("#submit").prop('disabled', true);
	        $("#submit").text("please wait..");
	        loader.hidePleaseWait();
	      },
        success: function(data){
        	//console.log(data);
        	//console.log(data.ProtectedCardNumber);
        	//if(data.success){
        	// $("#card_number").text(data.ProtectedCardNumber);
        	// $("#name_on_card").text(data.NameonCard);
        	// $("#card_type").text(data.CardType);
        	// $("#tid").val(data.RecurringSaleTokenId);
        	// $("#success_section").show();
        	// $("#card_info").hide();

        	if(data.Result==0){
        		// alert("AF");
        	check_ccwhitelist(data);

        	// $("#card_number").text(data.ProtectedCardNumber);
        	// $("#name_on_card").text(data.NameonCard);
        	// $("#card_type").text(data.CardType);
        	// $("#tid").val(data.RecurringSaleTokenId);
        	// $("#success_section").show();
        	// $("#card_info").hide();
        	} else {
        	$("#tid").val('');
        	swal(data.Message);	

        	}
        },
        cancel: function(response){
        	$("#tid").val('');
        	$("#submit").prop('disabled', false);
        	swal(response.Message);	
        	if(response.Message!="Error validation Captcha"){
        		swal(response.Message);	
        		$("#success_section").hide();
        		$("#card_info").show();
        	} 

/*        	$("#success_section").hide();
        	$("#card_info").show();*/
/*            if (isDefined(CallbackCancel) && CallbackCancel) window[CallbackCancel]("Error");
            else{

            }*/
        }	
	});


	/*@if($_SESSION['tag']=='MERCHANT')*/
	$("#amount").change(function(){

/*		    var err_count = 0;
			var to_req=[];
			//jquery blank validation..
			$(".required").each(function(){
			    var field_id = $(this).attr("id");
			    var data=[];
			    if($(this).val()==""){
			      data['id']=field_id;      
			      to_req.push(data);
			      err_count++;
			    }
			});

			if(err_count>0){
			  swal(
			  'Oops...',
			  "Please do check required fields.",
			  'error'
			  );  
			  $.each(to_req, function( index, value ) {
			    $("#"+value.id).parent().after('<div class="hb"><div class="help-block" style="color:#f38989; font-size:7pt;margin-bottom:0px !important;"></div></div>').addClass('has-error');
			  });        
			  return false;
			}*/

		if($("#amount").val()<=0.00){
		  	swal("Amount is required.");

		  	return false;
		}


		$.ajax({
		  url: '<?=site_url("paymentcharity/process_fee")?>',
		  type: 'POST',
		  dataType: 'json',
		  // data: {amount: $("#amount").val()},
		  data: {amount: parseFloat($("#amount").val().replace(/,/g, ''))},
	   	  beforeSend:function(xhr, textStatus) {
	        //called when complete
		    loader.showPleaseWait();
	        $("#submit").prop('disabled', true);
	      },		  
		  complete: function(xhr, textStatus) {
		    //called when complete
		  loader.hidePleaseWait();
		  },
		  success: function(data, textStatus, xhr) {
		    if(data.success){
		    	//console.log(data.fee_data.fee);
		    	$(".amount_val").text($("#amount").val());
				//$(".fee_val").text(data.fee_data.fee);
				//$(".vat_val").text(data.fee_data.vat_charge);
				// var total = parseFloat($("#amount").val())+parseFloat(data.fee_data.fee)+parseFloat(data.fee_data.vat_charge);
				var total = parseFloat($("#amount").val().replace(/,/g, ''))+parseFloat($(".conviniecefee_val").text());
				$(".amount_total").text(total.toFixed(2));
				$("#amount_total").val(total.toFixed(2));
				//$("#hid_fee").val(data.fee_data.fee);
				//$("#hid_vat").val(data.fee_data.vat_charge);
				getcfee($("#amount").val());
				$("#submit").prop('disabled', false);
		    } else {
		    	swal(data.msg);
		    	$("#submit").prop('disabled', false);
		    }
		  },
		  error: function(xhr, textStatus, errorThrown) {
		    //called when there is an error
		   	swal(data.msg);
		    $("#submit").prop('disabled', false);

		  }
		});

	});
/*	@endif*/

    $("#another_card").click(function(){
    	window.location.reload(); 

    	// $("#success_section").hide();
    	// $("#card_info").show();
    	// $(".cancel_section").hide();
    	// $(".payment_btn_section").show();        	
    });

    $("#whitelist_form").submit(function(e) {
    	/* Act on the event */
		e.preventDefault();

		//reset validation
		$('.hb').remove();
		$('.required').removeClass('has-error-border').removeClass('has-success');

		//check validation
		var err_count = 0;
		var to_req=[];
		//jquery blank validation..
		$("#whitelist_form .required").each(function(){
		    var field_id = $(this).attr("id");
		    var data=[];
		    if($(this).val()==""){
		      data['id']=field_id;      
		      to_req.push(data);
		      err_count++;
		    }
		});

		if(err_count>0){
		  swal(
		  'Oops...',
		  "Please do check required fields.",
		  'error'
		  );  
		  $.each(to_req, function( index, value ) {
		    $("#"+value.id).addClass('has-error-border');//.after('<div class="hb"><div class="help-block" style="color:#f38989; font-size:7pt;margin-bottom:0px !important;">Required filed.</div></div>')
		  });        
		  return false;
		}    
		//
		var url = '{{base_url("payment/update_whitelist")}}';
		var formData = new FormData($("#whitelist_form")[0]);
		formData.append('card_id_upload', $('#card_id_upload')[0].files[0]); 
		formData.append('credit_card_upload', $('#credit_card_upload')[0].files[0]); 
		formData.append('cc_with_card_upload', $('#cc_with_card_upload')[0].files[0]); 
		var button = $("#submit_btn");

		//@global .js
		//P1 form obj P2 url form process to.
		ProcessForm(formData,url,button);
		$("#modal_validate_card").modal('hide');



    	return false;
    });
    	//for cenpos process payment
    $("#payment").click(function(e){

    	//get total fees tf pf bfee vat
    	var total_fees = parseFloat($("#hid_fee").val())+parseFloat($("#hid_vat").val())+parseFloat($("#hid_pfee").val())+parseFloat($("#hid_tfee").val());
    	var form_data ={
    	//merchant:merchant,
    	notes:$("#notes").val(),
		reference_num:$("#reference_num").val(),	
    	tokenid:$("#tid").val(),
    	card_type:$("#card_type").text(),
    	card_number:$("#card_number").text(),
    	name_on_card:$("#name_on_card").text(),
    	// notes:$("#notes").val(),
    	// reference_num:$("#notes").val(),
		amount:$("#amount").val(),
		amount_val:$("#amount_total").val(),
		hid_fee:$("#hid_fee").val(),
		hid_vat:$("#hid_vat").val(),
    	hid_pfee:$("#hid_pfee").val(),
    	hid_tfee:$("#hid_tfee").val(),
    	// notes:$("#notes").val(),
    	mobile:$("#mobile").val(),
    	name:$("#name").val(),
    	email:$("#email").val(),
    	total_fee:total_fees,
        }  
        console.log(form_data); 

		if($("#amount").val()<=0.00){
		  	swal("Amount is required.");

		  	return false;
		}
        
		$.ajax({
          url: '{{base_url("paymentcharity/process_card")}}',
          type: 'POST',
          dataType: 'json',
          data: form_data,
          beforeSend:function(){
          	loader.showPleaseWait();
            $("#payment").prop("disabled",true);
            $("#payment").text("processing... please wait");

          },
          complete: function(xhr, textStatus) {
          	loader.hidePleaseWait();
            $("#payment").prop("disabled",false);
            $("#payment").text("Process Donation");

          },
          success: function(data, textStatus, xhr) {
            //console.log();
            if(data.success){
            	$("#transaction_code").text(data.reference);
                $("#success_section_transaction").show();
				$("#card_section").hide();
            } else {
                swal(data.msg);
              	$(".cancel_section").show();
		    	$(".payment_btn_section").hide();
		    	$("#success_section_transaction").hide();
		    	$("#payment").prop("disabled",false);
            	$("#payment").text("Process Donation");
		    	
            }
          },
          error: function(xhr, textStatus, errorThrown) {
       //          swal(data.msg);
       //        	$(".cancel_section").show();
		    	// $(".payment_btn_section").hide();
		    	// $("#success_section_transaction").hide();
		    	// $("#payment").prop("disabled",false);
       //      	$("#payment").text("Process Payment");
          }
        });


    });


	$('.btn-process').on('click', function() {
    $(this).html('Submit');
    $progressContentDone = $('.progress-content.active').data('content');
    //alert($progressContentDone);
    if($progressContentDone==1){
    	if($("#card_id").val()=='' || $("#card_email").val()=='' || $("#card_mobile_number").val()=='' ){
    		swal('Fill up all required field/s.');
    		return false;
    	}
    }
	if(!isEmail($("#card_email").val())){
    		swal('Invalid Email format.');
    		return false;		
	}


    if($progressContentDone==2){
	    console.log('if');
        $('.btn-gray').css({"opacity": "1"});    	
		$('.hb').remove();
		$('.required').removeClass('has-error-border').removeClass('has-success');

		//check validation
		var err_count = 0;
		var to_req=[];
		//jquery blank validation..
		$("#whitelist_form .required").each(function(){
		    var field_id = $(this).attr("id");
		    var data=[];
		    if($(this).val()==""){
		      data['id']=field_id;      
		      to_req.push(data);
		      err_count++;
		    }
		});

		if(err_count>0){
		  swal(
		  'Oops...',
		  "Please do check required fields.",
		  'error'
		  );  
		  $.each(to_req, function( index, value ) {
		    $("#"+value.id).addClass('has-error-border');//.after('<div class="hb"><div class="help-block" style="color:#f38989; font-size:7pt;margin-bottom:0px !important;">Required filed.</div></div>')
		  });        
		  return false;
		}    
		//
		var form = new FormData();

		if($("#card_id_upload").find('[name=filepond]').val()=='' || $("#credit_card_upload").find('[name=filepond]').val()=='' || $("#cc_with_card_upload").find('[name=filepond]').val()==''){
		  swal(
		  'Opps...',
		  "All photo is requires",
		  'error'
		  ); 
		  return false;
		}



		var url = '{{base_url("payment/update_whitelist")}}';
		//var formData = new FormData($("#whitelist_form")[0]);
		var formData = $("#whitelist_form").serializeArray();
		var file1 = JSON.parse($("#card_id_upload").find('[name=filepond]').val());
		//formData.append('card_id_upload', file1.data);
		var file2 = JSON.parse($("#credit_card_upload").find('[name=filepond]').val());
		//formData.append('credit_card_upload', file2.data);
		var file3 = JSON.parse($("#cc_with_card_upload").find('[name=filepond]').val());
		//formData.append('cc_with_card_upload', file3.data);	

		formData.push({name: 'card_id_upload', value: file1.data});
		formData.push({name: 'credit_card_upload', value: file2.data});
		formData.push({name: 'cc_with_card_upload', value: file3.data});

		//for (var i = 0; i < $('.my-pond input').length; i++) {
	
		//}

/*		formData.append('card_id_upload', $('#card_id_upload')[0].files[0]); 
		formData.append('credit_card_upload', $('#credit_card_upload')[0].files[0]); 
		formData.append('cc_with_card_upload', $('#cc_with_card_upload')[0].files[0]); */
		var button = $("#submit_btn");

	    $.ajax({
	      url: url,
	      type: 'POST',
	      dataType: 'json',
	      data: formData,
	      //contentType: false,       
	      //cache: false,             
	      //processData:false,       
	      beforeSend: function(xhr, textStatus) {
	        //called when complete
	        //clearValidationArray();
	        loader.showPleaseWait();
	        $(button).prop('disabled',true);        
	      },          
	      complete: function(xhr, textStatus) {
	        //called when complete
	        loader.hidePleaseWait();
	        $(button).prop('disabled',false);        
	      },
	      success: function(data) {
	        //called when successful
	        if(data.success){
	            // swal(
	            // '',
	            // data.msg,
	            // 'success'
	            // );  

		    $('.progress-step[data-step="' + $progressContentDone + '"]').addClass('done').removeClass('active');
		    $('.progress-step.done .number').html('<i class="fas fa-check"></i>');
		    $('.progress-step[data-step="' + $progressContentDone + '"]').next().addClass('active');

		    $('.progress-content[data-content="'+$progressContentDone+'"]').addClass('done').removeClass('active');
		    $('.progress-content[data-content="'+$progressContentDone+'"]').next().addClass('active');

	            //hideModal("large");
      		$('#cardValidation .btn-container').remove();
	   		$('.progress-content[data-content="3"]').addClass('done').removeClass('active');
	   		$('.progress-step[data-step="3"]').addClass('done').removeClass('active');
	    	$('.progress-step.done .number').html('<i class="fas fa-check"></i>');
	    	$('.progress-content[data-content="3"]').addClass('done');

	        } else {
	            swal(
	            'Opps...',
	            data.msg,
	            'error'
	            );  
	        }     
	      },
	      error: function(xhr, textStatus, errorThrown) {
	        swal('something went wrong.');
	      }
	    });

    }
    //return false;
    if($progressContentDone!=2){
    $('.progress-step[data-step="' + $progressContentDone + '"]').addClass('done').removeClass('active');
    $('.progress-step.done .number').html('<i class="fas fa-check"></i>');
    $('.progress-step[data-step="' + $progressContentDone + '"]').next().addClass('active');

    $('.progress-content[data-content="'+$progressContentDone+'"]').addClass('done').removeClass('active');
    $('.progress-content[data-content="'+$progressContentDone+'"]').next().addClass('active');
    }

  });

  // $(window).resize(function(){
  //   progressHeight();
  // });

  // setTimeout(function() {
	 //  progressHeight();
  // }, 2000);

  // function progressHeight() {
  //   $progressContent = $('.progress-content.active').height();
  //   $('.progress-container').css({
  //     "height": $progressContent
  //   });
  // }

  $('.my-pond').filepond();
  $.fn.filepond.registerPlugin(FilePondPluginImagePreview);
  $.fn.filepond.registerPlugin(FilePondPluginFileValidateType);
  $.fn.filepond.registerPlugin(FilePondPluginFileEncode);
  // Turn input element into a pond with configuration options
  $('.my-pond').filepond({
      allowMultiple: true,
      // labelIdle: '<button type="button" class="btn btn-upload"><i class="fas fa-upload"></i> Upload</button>',
      acceptedFileTypes: [
        'image/jpg',
        'image/jpeg',
        'image/png',
    	]
  });

  // Set allowMultiple property to true
  $('.my-pond').filepond('allowMultiple', false);

  // Listen for addfile event
  $('.my-pond').on('FilePond:addfile', function(e) {
      console.log('file added event', e);
  });

 $("#okay_btn").click(function(){
 	window.location.reload();
 });
	</script>
</body>
</html>