<?php
//loading assets helper
defined('BASEPATH') OR exit('No direct script access allowed');
$assetHelper = new AssetHelper\AssetHelper();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>SunCash.Me</title>
	<link rel="stylesheet" href="{{base_url('assets_main/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/main.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/responsive.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/all.min.css')}}">
</head>
<body>
	<nav class="navbar navbar-expand-lg sticky-top bg-white shadow-sm p-0">
	  <div class="container p-0" role="navigation">
	  	<a class="navbar-brand" href="index.html"><img src="{{base_url('assets_main/imgs/header-logo@2x.png')}}" alt="Suncash Logo" class="d-inline-block align-top" width="160"></a>
	  	<a href="#" class="menu-button"></a>
	  	<div class="justify-content-end main-menu">
				<ul class="navbar-nav d-flex align-items-center">


		    </ul>
		  </div>
	  </div>
	</nav>

	<section class="" id="cash_section"  >
		<div class="container  text-center">
			<div class="row d-flex justify-content-center align-items-center mb-5">
				<div class="user-card transaction-details" style="margin-bottom:60px !important;">
					<div class="header w-content"></div>
					<div class="body">
						<form id="cashpayment_form" method="POST">
				        <div class="user-image ">
								@if(!empty($_SESSION['profile_pic']))
								<img src="{{$_SESSION['profile_pic']}}">
								@else
								<img src="{{base_url('assets_main/imgs/suncash-icon.png')}}">
								@endif
							</div>
						<div class="details">

							<div class="message">You're about to pay <?=ucfirst($_SESSION['suntag_shortcode']) ?></div>
							<select name="payment_method" id="payment_method" >
								<option value="">BSD</option>
							</select>
							<input class="input-amount" type="text" id="amount" name="amount" value="0.00" style="height: 100% !important">	
						</div>

						<div class="text-left">
							<div class="item">
								<input type="text" class="form-control" id="reference_num" name="reference_num"  value="{{$payment_data['reference_num']}}" placeholder="Reference # (Order, Quote, Invoice or Account)">
							  	<textarea  class="form-control" id="notes" name="notes" placeholder="Note to business" rows="3">{{$payment_data['notes']}}</textarea>
							</div>
							<div class="item">
							  <div class="label">Please enter your name:</div>
							  <div class="row">
							  	
								  <div class="col-12 col-lg-6">
									 <input type="text" id="fname" name="fname" class="form-control" placeholder="First Name" required>
								  </div>
								  <div class="col-12 col-lg-6">
									 <input type="text" id="lname" name="lname" class="form-control" placeholder="Last Name" required>
								  </div>
							  </div>
							</div>
							<div class="item">
							  <div class="label">You are paying with:</div>
							  <div class="my-select p-0 no-border">
								<select class="form-control" id="facilities" placeholder="">
						      <!-- <option selected disabled hidden>Select Payment Method</option> -->
						      <!-- <option>Suncash Account</option> -->
						      <option selected>Cash Payment</option>
						    </select>
						    <!-- <i class="fa fa-chevron-down"></i> -->
						  </div>
							</div>
						  <div class="item primary-border">
						  	<div class="label">Transaction Details:</div>
							  <div class="row">
							  	<div class="col">Principal</div>
							  	<div class="col text-right "><span class="amount_val">{{$payment_data['amount']}}</span></div>
							  </div>
							  <div class="row">
							  	<div class="col">Fee</div>
							  	<div class="col text-right "><span class="fee_val">{{$fee_data['fee']}}</span></div>
							  </div>
							  <div class="row">
							  	<div class="col">VAT</div>
							  	<div class="col text-right "><span class="vat_val">{{$fee_data['vat_charge']}}</span></div>
							  </div>
						  </div>
						  <div class="item total">
							  <div class="row">
							  	<?php 
							  		$total = str_replace( ',', '', $payment_data['amount'])+$fee_data['fee']+$fee_data['vat_charge'];
							  	?>
							  	<div class="col label">Total Due</div>

							  	<div class="col text-right "><span class="amount_total">{{$total}}</span> BSD</div>

							  </div>
						  </div>
						  <div class="item">
						  	<div class="label">How would you like your code received?</div>
						  	<label class="checkbox-container">Send via SMS
								  <input type="checkbox" id="chkSMS" name="chkSMS" value="0"  >
								  <span class="checkmark"></span>
								</label>
								<input type="text" class="form-control bfh-phone" placeholder="" id="mobile" name="mobile" data-format="1 (ddd) ddd-dddd" value ="1 242" readonly >
								<label class="checkbox-container">Send via email
								  <input type="checkbox" id="chkEmail" name="chkEmail" value="0">
								  <span class="checkmark"></span>
								</label>
								<input type="email" id="email" name="email"  class="form-control" placeholder="Enter your email" readonly >

						  </div>						  
						</div>
							  	<input type="hidden" name="amount_total" id="amount_total" value="{{$payment_data['amount']}}">
							  	<!-- <input type="hidden" name="balance" id="balance" value="{{number_format($Balance,2)}}"> -->
							  	<input type="hidden" name="hid_fee" id="hid_fee" value="{{$fee_data['fee']}}">
							  	<input type="hidden" name="hid_vat" id="hid_vat" value="{{$fee_data['vat_charge']}}">

						<button type="submit" id="process_payment"  class="btn btn-orange full btn-process" data-loading-text="Loading...">Generate Payment Code</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="full" id="success_section" style=" display:none ;">
		<div class="container  text-center">
			<div class="row d-flex justify-content-center align-items-center mb-5">
				<div class="user-card success-card" style="margin-bottom:60px !important;">
					<div class="header w-content"></div>
					<div class="body">
						<div class="user-image">
							<img src="{{base_url('assets_main/imgs/success-icon.png')}}">
						</div>
						<div class="details" id = "transaction-details">
							<div class="message" style="font-size: 2rem;line-height: 2.1rem;width: 90%;margin: 0 auto;">You will Pay <span class="amount_val" style="color: #FF8400;">${{$payment_data['amount']}}</span> in Store to <?=ucfirst($_SESSION['suntag_shortcode'])?></div>
						</div>
						<!-- <form action="" class="text-left"> -->
						<div class="text-left">

					  	<div class="label text-center">Transaction Details</div>
						  <div class="item">
							  <div class="row">
							  	<div class="col">Transaction ID: <span id="transaction_code"></span></div>
							  	<div class="col text-right">{{date("d M Y")}}</div>
							  </div>
							</div>
							<div class="item">
							  <div class="row">
							  	<div class="col">Payment Method</div>
							  	<div class="col text-right">Cash Payment</div>
							  </div>
							</div>
							<div class="item">
							  <div class="row">
							  	<div class="col">Principal</div>
							  	<div class="col text-right"><span class="amount_val">${{$payment_data['amount']}}</span></div>
							  </div>
							</div>								
							<div class="item">
							  <div class="row">
							  	<div class="col">Fee</div>
							  	<div class="col text-right"><span class="fee_val">${{$fee_data['fee']}}</span></div>
							  </div>
							</div>
							<div class="item">
							  <div class="row">
							  	<div class="col">Vat</div>
							  	<div class="col text-right"><span class="vat_val">${{$fee_data['vat_charge']}}</div>
							  </div>
							</div>	
						  <div class="item total">
							  <div class="row">
							  	<div class="col label">Total Due</div>
							  	<div class="col text-right amount"><span class="amount_total">{{$total}}</span> BSD</div>
							  </div>
						  </div>
						</div>
						<!-- </form> -->

					</div>
				</div>
			</div>
		</div>
	</section>
	<footer class="py-3" style="position: fixed;
    left: 0;
    bottom: 0;
    width: 100%;">
		<div class="container">
			<div class="row">
				<div class="col-12 col-lg-6 d-flex align-self-center text-left">
					<span>© {{date('Y')}} SunCash.Me. All Rights Reserved.</span>
				</div>
				<div class="col-12 col-lg-6 text-right">
					<img src="{{base_url('assets_main/imgs/footer-logo.png')}}">
				</div>
			</div>
		</div>
	</footer>

	<script src="{{base_url('assets_main/js/jquery-3.2.1.min.js')}}"></script>
	<script src="{{base_url('assets_main/js/bootstrap.min.js')}}"></script>
	<script src="{{base_url('assets_main/js/main.js')}}"></script>
    <script src="{{base_url('assets_main/js/select.min.js')}}"></script>
	{!!$assetHelper->link_plugins('js','jquery-1.11.3.min.js')!!}
	{!!$assetHelper->link_plugins('js','bootstrap-3.3.7/js/bootstrap.min.js')!!}
	{!!$assetHelper->link_plugins('js','sidebar-nav/dist/sidebar-nav.min.js')!!}
	{!!$assetHelper->link_plugins('js','override/jquery.slimscroll.js')!!}
	{!!$assetHelper->link_plugins('js','override/waves.js')!!}
	{!!$assetHelper->link_plugins('js','override/custom.min.js')!!}
	{!!$assetHelper->link_plugins('js','sweetalert2-master/dist/sweetalert2.all.min.js')!!}
	{!!$assetHelper->link_plugins('js','bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js')!!}
	{!!$assetHelper->link_plugins('js','curioussolutions-datetimepicker/dist/DateTimePicker.min.js')!!}
    {!!$assetHelper->link_plugins('js','Inputmask-5.x/dist/jquery.inputmask.min.js')!!}
    {!!$assetHelper->link_plugins('js','bootstrap-formhelpers-phone/bootstrap-formhelpers-phone.js') !!}
    {!!$assetHelper->link_plugins('js','bootstrap-formhelpers-phone/bootstrap-formhelpers-phone.format.js') !!}
	{!!$assetHelper->link_plugins('js','dropzone-master/dist/dropzone.js')!!}	
	<script  type="text/javascript" charset="utf-8">
	$("#amount").inputmask({ 'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true, 'digits': 2, 'digitsOptional': false, 'placeholder': '0.00', rightAlign : false,clearMaskOnLostFocus: !1 });

	$("#chkEmail").click(function(){
	  if($(this).is(":checked")){
	    $("#email").addClass('required');
	    $("#email").prop('readonly',false);
	  } else {
	    $(this).val("0");
	    $("#email").removeClass('required');
	    $("#email").prop('readonly',true);
	  }
	});
	$("#chkSMS").click(function(){
	  if($(this).is(":checked")){
	    $("#mobile").addClass('required');
	    $("#mobile").prop('readonly',false);
	  } else {
	    $(this).val("0");
	    $("#mobile").removeClass('required');
	    $("#mobile").prop('readonly',true);
	  }
	});


	$("#cashpayment_form").on('submit',function(e){


    swal({
      title: 'Please confirm payment details.',
      // text: "",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes!'
    }).then((result) => {
      if (result.value) {
    	e.preventDefault();
		  $('.hb').remove();
		  $('.form-div').removeClass('has-error').removeClass('has-success');

		  //check validation
		  var err_count = 0;
		  var to_req=[];
		  //jquery blank validation..
		  $(".required").each(function(){
		      var field_id = $(this).attr("id");
		      var data=[];
		      if($(this).val()==""){
		        data['id']=field_id;      
		        to_req.push(data);
		        err_count++;
		      }
		  });

		  /*if($("#password").val()!=$("#cpassword").val()){
		    swal("Youre password and confirmation password do not match.");
		    return false;
		  }*/


		  if(err_count>0){
		    swal(
		    'Opps...',
		    "Please do check required fields.",
		    'error'
		    );  
/*		    $.each(to_req, function( index, value ) {
		      $("#"+value.id).parent().after('<div class="hb"><div class="help-block" style="color:#f38989; font-size:7pt;margin-bottom:0px !important;">Required filed.</div></div>').addClass('has-error');
		    });   */     
		    return false;
		  }
      		var is_email =0;
      		var is_sms=0;

      		if($("#chkSMS").is(":checked")){
      			 //alert("y");
      			is_sms=1;

      		}
      		if($("#chkEmail").is(":checked")){
      			  //alert("y");
      			is_email=1;
     			
      		}
	        var form_data = {
	        	notes:$("#notes").val(),
	        	reference_num:$("#reference_num").val(),
				amount:$("#amount").val(),
				email:$("#email").val(),
				mobile:$("#mobile").val(),
				fname:$("#fname").val(),
				lname:$("#lname").val(),
				is_email:is_email,
				is_sms:is_sms,
				amount_val:$("#amount_total").val(),
				hid_fee:$("#hid_fee").val(),
				hid_vat:$("#hid_vat").val(),
				// balance:$("#balance").val()	
	        };
	        $.ajax({
	          url: "<?=site_url("payment/process_cash")?>",
	          type: 'POST',
	          dataType: 'json',
	          data: form_data,
	          beforeSend:function(xhr, textStatus) {
	            //called when complete
	            $("#process_payment").prop('disabled', true);
	          },
	          complete: function(xhr, textStatus) {
	            //called when complete
	             $("#process_payment").prop('disabled', false);
	          },
	          success: function(data) {
	            if(data.success){
	                //alert(data.msg);
	                var str="<div class='alert alert-dismissible alert-success'><strong>Thank you for your payment ! Please do copy the payment code to present to the nearest suncash store. </strong>.</div>";
	                $("#msg").html(str);
	                $("#success_section").show();
	                $("#cash_section").hide();

	               	$("#transaction_code").text(data.msg);
	               	$("#notes_value").text($("#notes").val());
	               	$("#ref_value").text($("#reference_num").val());
	                $("#amount_value").text($("#amount").val());
	                $("#email").text($("#email").val());
	               	$("#mobile").text($("#mobile").val());
	                $("#fname").text($("#fname").val());
	                $("#lname").text($("#lname").val());
	               	// $("#transaction_section").show();
	               	$(".transaction_details").show();
	               	
	                //otable.ajax.reload();
	                //$("#transfer_modal").modal('hide');
	             	$("#process_payment").prop('disabled', false);
	            } else {
	                var str="<div class='alert alert-dismissible alert-danger'><strong> "+data.msg+" </strong>.</div>";
	                $("#msg").html(str);
	                $("#cash_section").hide();
	                $("#success_section").show();
					$("#transaction_details").hide();

	               	// $("#transaction_code").val('');
	               	// $("#transaction_section").hide();
	   
	               	
	             	$("#process_payment").prop('disabled', false);		
	            }
	          },
	          error: function(data, textStatus, errorThrown) {
	            //called when there is an error
	            //alert(data.msg);
	             $("#process_payment").prop('disabled', false);
	          }
	        });
      }
    });
    return false;




		});
	$("#back_to_form").click(function(){
		$("#cash_section").show();
		$("#success_section").hide();
	});
	$("#amount").change(function(){
		$.ajax({
		  url: '<?=site_url("payment/process_fee")?>',
		  type: 'POST',
		  dataType: 'json',
		  // data: {amount: $("#amount").val()},
		  data: {amount: parseFloat($("#amount").val().replace(/,/g, ''))},
		  complete: function(xhr, textStatus) {
		    //called when complete
		  },
		  success: function(data, textStatus, xhr) {
		    if(data.success){
		    	//console.log(data.fee_data.fee);
		    	$(".amount_val").text($("#amount").val());
				$(".fee_val").text(data.fee_data.fee);
				$(".vat_val").text(data.fee_data.vat_charge);
				// var total = parseFloat($("#amount").val())+parseFloat(data.fee_data.fee)+parseFloat(data.fee_data.vat_charge);
				var total = parseFloat($("#amount").val().replace(/,/g, ''))+parseFloat(data.fee_data.fee)+parseFloat(data.fee_data.vat_charge);
				$(".amount_total").text(total);
				$("#amount_total").val(total);
				$("#hid_fee").val(data.fee_data.fee);
				$("#hid_vat").val(data.fee_data.vat_charge);
				
		    }
		  },
		  error: function(xhr, textStatus, errorThrown) {
		    //called when there is an error
		  }
		});
		    var err_count = 0;
			var to_req=[];
			//jquery blank validation..
			$(".required").each(function(){
			    var field_id = $(this).attr("id");
			    var data=[];
			    if($(this).val()==""){
			      data['id']=field_id;      
			      to_req.push(data);
			      err_count++;
			    }
			});

			if(err_count>0){
			  swal(
			  'Oops...',
			  "Please do check required fields.",
			  'error'
			  );  
			  $.each(to_req, function( index, value ) {
			    $("#"+value.id).parent().after('<div class="hb"><div class="help-block" style="color:#f38989; font-size:7pt;margin-bottom:0px !important;"></div></div>').addClass('has-error');
			  });        
			  return false;
			}

	});

	</script>
</body>
</html>