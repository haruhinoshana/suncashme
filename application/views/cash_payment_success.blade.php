<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Payment Code</title>
	<link rel="stylesheet" href="{{base_url('assets_main/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/main_checkout.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/all.min.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/responsive_checkout.css')}}">
</head>
<body class="checkout-payment-code">
	<div class="shop-logo-header">
				@if(!empty($post_data))
					<img src="{{$post_data['profile_pic']}}">
				@else
					<img src="{{base_url('assets_main/imgs/shop-logo.png')}}">
				@endif	
	<main class="">
		<div class="container">
			<div class="row main">
				<div class="col-lg-4">
					<div class="order-summary">
						<div class="header">
							<div class="total-payment">
								You made a total payment of <span class="amount">$ {{$post_data['Amount']}}</span>
							</div>
						</div>
						<div class="table-responsive mb-4">
							<table class="table table-borderless">
								<thead>
							    <tr>
							      <th scope="col">Description</th>
							      <th scope="col" class="text-right">Amount</th>
							    </tr>
							  </thead>
							  <tbody>
							    <tr>
							      <td>
							      	<span class="item-name">{{$post_data['MerchantName']}}</span>
							      	<span class="item-number">Transaction ID: {{$cash_payment_code}}</span>
							      </td>
							      <td class="text-right amount">$ {{$post_data['Amount']}}</td>
							    </tr>
							    <tr class="item-total">
						  		@if(!empty($post_data))
							  		@if(!empty($post_data['ItemName']))
							  			<?php 
							  			$count = count($post_data['ItemName']); 
							  			$qty_total = 0 ;
							  			?>
							  			@for($i=0;$i<$count; $i++)
										    <?php $qty_total+=$post_data['ItemQty'][$i]++; ?>						  			
							  			@endfor
							    	@else

							    	@endif
							  	@else

							  	@endif							    	
							      <td class="f-medium">Item Total</td>
							      <td class="text-right amount">{{$qty_total}}</td>
							    </tr>
							  </tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="col-lg-8">
					<div class="card">
						<div class="card-header title-header">
					    Payment Code
					    <img src="{{base_url('assets_main/imgs/suncash-logo.png')}}">
					  </div>
					  <div class="card-body">
					  	<div class="receipt-container text-center">
					  			<?php $qr = \Qr::GenerateQr($cash_payment_code,'',100); ?>
								<img class="qr-img" src="data:<?php echo $qr->getContentType(); ?>;base64,<?php echo $qr->generate();?>" class="qr" />
								<div class="code">
									<span>Your Payment Code</span>
									<span>{{$cash_payment_code}}</span>
								</div>
								<?php $barcode = \Qr::GenerateBarcode128($cash_payment_code,'',100); ?>
								<img  src="data:image/png;base64,<?php echo $barcode;?>" class="barcode"/>
								<!-- <button class="btn" data-toggle="modal" data-target="#storeLocator">Store Locator</button> -->

								<div class="message">

								@if($cash_posted_data['is_sms']==1&& $cash_posted_data['is_email']==1)
									<div><p>We also sent this code to your mobile number (<span class="mobile-number">{{$cash_posted_data['mobile']}}</span>) and your email (<span class="email">{{$cash_posted_data['email']}}</span>).</p></div>
								
								@elseif($cash_posted_data['is_sms']==1)
									<div>We also sent this code to your mobile number (<span class="mobile-number">{{$cash_posted_data['mobile']}}</span>)</div>
								
								@elseif ($cash_posted_data['is_email']==1)
									<div>We also sent this code to your email (<span class="email">{{$cash_posted_data['email']}}</span>).</p></div>
								
								@endif

								</div>

								<div class="transaction-details">
									<p class="table-title">Transaction Details</p>
									<table>
										<tbody>
											<tr>
												<td>Transaction ID:</td>
												<td>{{$cash_payment_code}}</td>
											</tr>
											<tr>
												<td>Payment Method:</td>
												<td>Cash</td>
											</tr>
											<tr class="total">
												<td>Total Payment:</td>
												<td>{{$post_data['Amount']}} BSD</td>
											</tr>
										</tbody>

									</table>
								</div>
								<!-- <div class="footer"><button class="btn btn-primary">Return</button> <span>to <span class="">{{$post_data['MerchantName']}}</span></span></div> -->
							</div>
					  </div>
					</div>					  
				</div>
			</div>
		</div>
	</main>

	<footer>
		<div class="container">
			<div class="row d-flex align-items-center">
				<div class="col-12 col-lg-6">
					<div class="copyright">© Copyright Suncash 2018. All Rights Reserved.</div>
					<div class="link">
						<a href="#">Privacy Policy</a><span>|</span><a href="#">Terms & Conditions</a>
					</div>
				</div>
				<div class="col-12 col-lg-6 logo">
				  <img src="{{base_url('assets_main/imgs/suncash-logo.png')}}">
				</div>
			</div>
		</div>
	</footer>



	<div class="modal fade" id="storeLocator" tabindex="-1" role="dialog">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="row">
	      	<div class="col-3 store-location">
	      		<div class="store-count">
	      			<div><span class="text-primary">7 Stores</span> near you</div>
	      		</div>
	      		<div class="row store">
      				<div class="col-1">
      					<i class="fas fa-map-marker-alt"></i>
      				</div>
      				<div class="col">
      					<div class="store-name">7/11 Store</div>
      					<div class="store-address">#4 Carmichael & East Ave, Nassau</div>
      					<div class="store-travel-time">32 mins walk away</div>
      				</div>
      			</div>
      			<div class="row store">
      				<div class="col-1">
      					<i class="fas fa-map-marker-alt"></i>
      				</div>
      				<div class="col">
      					<div class="store-name">Ministop</div>
      					<div class="store-address">3634  Poplar Chase Lane, Nassau</div>
      					<div class="store-travel-time">32 mins walk away</div>
      				</div>
      			</div>
      			<div class="row store">
      				<div class="col-1">
      					<i class="fas fa-map-marker-alt"></i>
      				</div>
      				<div class="col">
      					<div class="store-name">7/11 Store</div>
      					<div class="store-address">#4 Carmichael & East Ave, Nassau</div>
      					<div class="store-travel-time">32 mins walk away</div>
      				</div>
      			</div>
      			<div class="row store">
      				<div class="col-1">
      					<i class="fas fa-map-marker-alt"></i>
      				</div>
      				<div class="col">
      					<div class="store-name">Ministop</div>
      					<div class="store-address">3634  Poplar Chase Lane, Nassau</div>
      					<div class="store-travel-time">32 mins walk away</div>
      				</div>
      			</div>
      			<div class="row store">
      				<div class="col-1">
      					<i class="fas fa-map-marker-alt"></i>
      				</div>
      				<div class="col">
      					<div class="store-name">7/11 Store</div>
      					<div class="store-address">#4 Carmichael & East Ave, Nassau</div>
      					<div class="store-travel-time">32 mins walk away</div>
      				</div>
      			</div>
      			<div class="row store">
      				<div class="col-1">
      					<i class="fas fa-map-marker-alt"></i>
      				</div>
      				<div class="col">
      					<div class="store-name">Ministop</div>
      					<div class="store-address">3634  Poplar Chase Lane, Nassau</div>
      					<div class="store-travel-time">32 mins walk away</div>
      				</div>
      			</div>
      			<div class="row store">
      				<div class="col-1">
      					<i class="fas fa-map-marker-alt"></i>
      				</div>
      				<div class="col">
      					<div class="store-name">7/11 Store</div>
      					<div class="store-address">#4 Carmichael & East Ave, Nassau</div>
      					<div class="store-travel-time">32 mins walk away</div>
      				</div>
      			</div>

	      	</div>
	      	<div class="col store-map">
	      		<div class="modal-header">
			        <div class="input-group search-store">
							  <input type="text" class="form-control" placeholder="Search a store" aria-label="Recipient's username" aria-describedby="basic-addon2">
							  <div class="input-group-append">
							    <span class="input-group-text" id="basic-addon2"><i class="fas fa-search"></i></span>
							  </div>
							</div>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<img src="{{base_url('assets_main/imgs/close-icon.png')}}" class="close-icon">
			        </button>
			      </div>
			      <div class="modal-body"><div id="map"></div></div>
	      	</div>
	      </div>
	    </div>
	  </div>
	</div>


	<script src="assets/js/jquery-3.2.1.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/main.js"></script>
</body>
</html>