<?php
//loading assets helper
defined('BASEPATH') OR exit('No direct script access allowed');
$assetHelper = new AssetHelper\AssetHelper();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Chekout</title>
	<link rel="stylesheet" href="{{base_url('assets_main/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/main_checkout.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/all.min.css')}}">
	<style type="text/css" >
		.ïframecenpos{
			border:0px !important;
		}
		.layaoutPanelTokenCardForm .canvasbox {
		    margin-left: 10% !important;
		    margin-right:10% !important;
		    width: 80% !important;			
		}
	</style>
</head>
<body>


	<section class="" id="cash_section"  >
		<div class="container  text-center">
			<div class="row d-flex justify-content-center align-items-center mb-5">


				<div class="user-card transaction-details" style="margin-bottom:60px !important;">
					<div id="show_cart_details">
						<div id="NewCenposPlugin">
							
						</div>
						<button type="button" class="btn btn-orange full" id="submit">Submit</button>
                	</div>
				</div>
			</div>
		</div>
	</section>


	<section class="full" id="success_section" style=" display:none ;">
		<div class="container  text-center">
			<div class="row d-flex justify-content-center align-items-center mb-5">
				<div class="user-card success-card" style="margin-bottom:60px !important; max-width:959px !important; min-width:959px !important;">
					<div class=""></div>
					<div class="body">
						<div class="" style=" height: 5rem;">
						</div>
						<div class="details" id = "transaction-details">

						</div>
						  <div class="row">
						  	
						    <div class="col">
							<form action="" class="text-left"> 	

								<div class="text-left">

							  	<div class="label text-center">Card Details</div>

								  	<div class="item">
									  <div class="row">
									  	<div class="col">Card Number: </div>
									  	<div class="col text-right"><span id="card_number"></span></div>
									  </div>
									</div>
									<div class="item">
									  <div class="row">
									  	<div class="col">Name on Card: </div>
									  	<div class="col text-right"><span id="name_on_card"></span></div>
									  </div>
									</div>
								  	<div class="item total">
									  <div class="row">
									  	<div class="col label">Card Type: </div>
									  	<div class="col text-right amount"><span id="card_type"></span> </div>
									  </div>
								  	</div>

								</div>
								
						    </div>
						    <div class="col">

								<div class="text-left">

							  	<div class="label text-center">Order Details</div>

								  	<div class="item">
									  <div class="row">
									  	<div class="col">Invoice Numberr: </div>
									  	<div class="col text-right"><span id="invoice_number">000000001111</span></div>
									  </div>
									</div>
									<div class="item">
									  <div class="row">
									  	<div class="col">Amount: </div>
									  	<div class="col text-right"><span id="amount">2.00</span></div>
									  </div>
									</div>

								</div>
								<input type="hidden" name="ihid" id="ihid" value="000000001111">
								<input type="hidden" name="ahid" id="ahid" value="2.00">
								<input type="hidden" name="tid" id="tid" value="">
								<button type="button" class="btn btn-orange full" id="payment">Process Payment</button>
						     </form>
						    </div>
						     
						  </div>

					</div>
				</div>
			</div>
		</div>
	</section>


	<script src="{{base_url('assets_main/js/jquery-3.2.1.min.js')}}"></script>
	<script src="{{base_url('assets_main/js/bootstrap.min.js')}}"></script>
	<script src="{{base_url('assets_main/js/main.js')}}"></script>
    <script src="{{base_url('assets_main/js/select.min.js')}}"></script>
	{!!$assetHelper->link_plugins('js','jquery-1.11.3.min.js')!!}
	{!!$assetHelper->link_plugins('js','bootstrap-3.3.7/js/bootstrap.min.js')!!}
	{!!$assetHelper->link_plugins('js','sidebar-nav/dist/sidebar-nav.min.js')!!}
	{!!$assetHelper->link_plugins('js','override/jquery.slimscroll.js')!!}
	{!!$assetHelper->link_plugins('js','override/waves.js')!!}
	{!!$assetHelper->link_plugins('js','override/custom.min.js')!!}
	{!!$assetHelper->link_plugins('js','sweetalert2-master/dist/sweetalert2.all.min.js')!!}
	{!!$assetHelper->link_plugins('js','bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js')!!}
	{!!$assetHelper->link_plugins('js','curioussolutions-datetimepicker/dist/DateTimePicker.min.js')!!}
    {!!$assetHelper->link_plugins('js','Inputmask-5.x/dist/jquery.inputmask.min.js')!!}
    {!!$assetHelper->link_plugins('js','bootstrap-formhelpers-phone/bootstrap-formhelpers-phone.js') !!}
    {!!$assetHelper->link_plugins('js','bootstrap-formhelpers-phone/bootstrap-formhelpers-phone.format.js') !!}
	{!!$assetHelper->link_plugins('js','dropzone-master/dist/dropzone.js')!!}	
	<script src="https://www.cenpos.com/Plugins/porthole.min.js"></script>
	<script src="https://www.cenpos.com/Plugins/jquery.simplewebpay.js"></script> 
	<script  type="text/javascript" charset="utf-8">
	function CallbackSuccess(responseData) {
	alert(JSON.stringify(responseData));
	}
	function CallbackCancel(responseData) {
	alert(JSON.stringify(responseData));
	}

	$("#submit").click(function(){
		$("#NewCenposPlugin").submitAction();
	});

	$("#NewCenposPlugin").createWebpay({
	url: 'https://www.cenpos.net/simplewebpay/cards',
	params : "merchantid=8115iItI79q9AKXGLA0deWiDl8x+tPd3vq5uLvcPraI=&iscvv=true",
	height:'300px',
	//isCvv :true&SecretKey=a0c70a0d5aa451bfc02f17e9199e41e6&verifyingpost="+vp
	sessionToken:false,
	    beforeSend:function(xhr, textStatus) {
	        //called when complete
	        $("#submit").prop('disabled', true);
	      },
	    complete: function(xhr, textStatus) {
	        //called when complete
	         $("#submit").prop('disabled', false);
	      },
        success: function(data){
        	//console.log(data);
        	//console.log(data.ProtectedCardNumber);

        	$("#card_number").text(data.ProtectedCardNumber);
        	$("#name_on_card").text(data.NameonCard);
        	$("#card_type").text(data.CardType);
        	$("#tid").val(data.RecurringSaleTokenId);
        	$("#success_section").show();
        	$(".transaction-details").hide();

        	//insert card info....
/*            if (isDefined(CallbackSuccess) && CallbackSuccess) window[CallbackSuccess](msg);
            else{

            }*/
        },
        cancel: function(response){
        	$("#success_section").hide();
        	$(".transaction-details").show();
/*            if (isDefined(CallbackCancel) && CallbackCancel) window[CallbackCancel]("Error");
            else{

            }*/
        }	
	});




    $("#payment").click(function(e){
/*    	var form_data ={
		amount:$("#ahid").val(),
		invoicenumber:$("#ihid").val(),
		type:'Sale',
		email:'zelbuado@gmail.com',
		secretkey:'a0c70a0d5aa451bfc02f17e9199e41e6',
		merchant:'8115iItI79q9AKXGLA0deWiDl8x+tPd3vq5uLvcPraI=',
		tokenid:$("#tid").val(),
		//verifyingpost:verify_token,
        }
		var vp = '';
		$.ajax({
		  url: 'https://www.cenpos.net/simplewebpay/cards/?app=genericcontroller&action=siteVerify',
		  type: 'POST',
		  dataType: 'json',
		  data:form_data,
		  async: false,
		  complete: function(xhr, textStatus) {
		    //called when complete
		  },
		  success: function(data, textStatus, xhr) {
		  	console.log(data);
		  	//console.log(data.Data);
		  	 vp =data.Data;
		    //called when successful
		  },
		  error: function(xhr, textStatus, errorThrown) {
		    //called when there is an error
		  }
		});*/


		//var vt = verifypost();
/*        var form_data ={
			verifyingpost:vp,
			tokenid:$("#tid").val()
		//verifyingpost:verify_token,
        }*/
/*        $.ajax({
          url: 'https://www.cenpos.net/simplewebpay/cards/api/UseToken/',
          type: 'POST',
          dataType: 'json',
          data: form_data,
          beforeSend:function(){
            $("#payment").prop("disabled",true);
            $("#payment").text("...");
          },
          complete: function(xhr, textStatus) {
            $("#payment").prop("disabled",false);
            $("#payment").text("Process Payment");
          },
          success: function(data, textStatus, xhr) {
            //console.log();
            if(data.success){
            $("#show_cart_details").hide();
            $('.modal-title').html('Process Payment');
            $(".show_cart_payment").show();
            $("#task").text('asda');
            $("#amount").text('0.00');
            $("#submit_payment").attr("auth",data.GenerateCryptoTokenResult.CryptoToken);
            } else {
                alert(data.msg);
            }
          },
          error: function(xhr, textStatus, errorThrown) {
/*            //called when there is an error
            $("#payment").prop("disabled",false);
            $("#payment").text("Submit");
          }
        });*/
    	var form_data ={
		amount:$("#ahid").val(),
		invoicenumber:$("#ihid").val(),
		type:'Sale',
		email:'zelbuado@gmail.com',
		secretkey:'a0c70a0d5aa451bfc02f17e9199e41e6',
		merchant:'8115iItI79q9AKXGLA0deWiDl8x+tPd3vq5uLvcPraI=',
		tokenid:$("#tid").val(),//https://www.cenpos.net/simplewebpay/cards/api/UseToken/
		//verifyingpost:verify_token,
        }   
		$.ajax({
          url: '{{base_url("payment/process_payment")}}',
          type: 'POST',
          dataType: 'json',
          data: form_data,
          beforeSend:function(){
            $("#payment").prop("disabled",true);
            $("#payment").text("...");
            //loader.showPleaseWait();
          },
          complete: function(xhr, textStatus) {
            $("#payment").prop("disabled",false);
            $("#payment").text("Process Payment");
            //loader.hidePleaseWait();
          },
          success: function(data, textStatus, xhr) {
            //console.log();
            if(data.success){
            	alert(data.msg);
/*            $("#show_cart_details").hide();
            $('.modal-title').html('Process Payment');
            $(".show_cart_payment").show();
            $("#task").text('asda');
            $("#amount").text('0.00');
            $("#submit_payment").attr("auth",data.GenerateCryptoTokenResult.CryptoToken);*/
            } else {
                alert(data.msg);
            }
          },
          error: function(xhr, textStatus, errorThrown) {
/*            //called when there is an error
            $("#payment").prop("disabled",false);
            $("#payment").text("Submit");*/
          }
        });


    });

	</script>
</body>
</html>