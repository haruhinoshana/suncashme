<?php
//loading assets helper
defined('BASEPATH') OR exit('No direct script access allowed');
$assetHelper = new AssetHelper\AssetHelper();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Chekout</title>
	<link rel="stylesheet" href="{{base_url('assets_main/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/main_checkout.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/all.min.css')}}">
	<style type="text/css" >
/*		.ïframecenpos{
			border:0px !important;
		}
		.layaoutPanelTokenCardForm .canvasbox {
		    margin-left: 10% !important;
		    margin-right:10% !important;
		    width: 80% !important;			
		}*/
	</style>
</head>
<body>
	<nav class="navbar navbar-light bg-light">
	  <a class="navbar-brand" href="#">
	    <img src="{{base_url('assets_main/imgs/suncash-logo.png')}}">
	  </a>
	</nav>
	<div class="container">
    <form id="frmPaymentPage" name="frmPaymentPage" action='{{base_url("payment/checkout")}}' method="post">
          <input type="text" class="form-control" name="MerchantKey" id="MerchantKey" value="c2bc674db209d81615f68e22fb8b9d475448328e853e37d45f80eb29678c7e71" /><br><!--value="V6NO7tWjo14j05sg0KqZ5k5hhAO01J+3Xghri7ITBMmAv+30E+CViVXjJaS3JnSU"><br />-->
          <input type="text" class="form-control" name="MerchantName" id="MerchantName" value="Sky Vantage"> <br />
          <input type="text" class="form-control" name="Amount" id="Amount" placeholder ="total amount"  value="2.00"> <br />
          <input type="text" class="form-control" name="OrderID" id="OrderID" placeholder ="Order ID" value=""><br />
          <input type="text" class="form-control" name="CallbackURL" id="CallbackURL" value="http://localhost/suncashme/payment/sample_success/"><br />
          <h3>item 1</h3>
          <input type="text" class="form-control" name="ItemName[]" id="ItemName" value="Headset"><br />
          <input type="text" class="form-control" name="ItemQty[]" id="ItemQty" value="1"><br />
          <input type="text" class="form-control" name="ItemPrice[]" id="ItemPrice" value="200.00"><br />
          <h3>item 2</h3>
          <input type="text" class="form-control" name="ItemName[]" id="ItemName" value="Celphone Charger"><br />
          <input type="text" class="form-control" name="ItemQty[]" id="ItemQty" value="2"><br />
          <input type="text" class="form-control" name="ItemPrice[]" id="ItemPrice" value="300.00"><br />
          <!-- <input type="hidden" name="auth" value="{{$tokken['temp_auth']}}"> -->
          <input type="submit" value="submit" />
    </form>
	</div>



	<script src="{{base_url('assets_main/js/jquery-3.2.1.min.js')}}"></script>
	<script src="{{base_url('assets_main/js/bootstrap.min.js')}}"></script>
	<script src="{{base_url('assets_main/js/main.js')}}"></script>
    <script src="{{base_url('assets_main/js/select.min.js')}}"></script>
	{!!$assetHelper->link_plugins('js','jquery-1.11.3.min.js')!!}
	{!!$assetHelper->link_plugins('js','bootstrap-3.3.7/js/bootstrap.min.js')!!}
	{!!$assetHelper->link_plugins('js','sidebar-nav/dist/sidebar-nav.min.js')!!}
	{!!$assetHelper->link_plugins('js','override/jquery.slimscroll.js')!!}
	{!!$assetHelper->link_plugins('js','override/waves.js')!!}
	{!!$assetHelper->link_plugins('js','override/custom.min.js')!!}
	{!!$assetHelper->link_plugins('js','sweetalert2-master/dist/sweetalert2.all.min.js')!!}
	{!!$assetHelper->link_plugins('js','bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js')!!}
	{!!$assetHelper->link_plugins('js','curioussolutions-datetimepicker/dist/DateTimePicker.min.js')!!}
    {!!$assetHelper->link_plugins('js','Inputmask-5.x/dist/jquery.inputmask.min.js')!!}
    {!!$assetHelper->link_plugins('js','bootstrap-formhelpers-phone/bootstrap-formhelpers-phone.js') !!}
    {!!$assetHelper->link_plugins('js','bootstrap-formhelpers-phone/bootstrap-formhelpers-phone.format.js') !!}
	{!!$assetHelper->link_plugins('js','dropzone-master/dist/dropzone.js')!!}	
	<script src="https://www.cenpos.com/Plugins/porthole.min.js"></script>
	<script src="https://www.cenpos.com/Plugins/jquery.simplewebpay.js"></script> 
	<script  type="text/javascript" charset="utf-8">
		$("#frmPaymentPage").submit(function(){
			var form_data = $(this).serializeArray();
			$.ajax({
			  url: '{{base_url("payment/payment_process_sample")}}',
			  type: 'POST',
			  dataType: 'json',
			  data: form_data,
			  complete: function(xhr, textStatus) {
			    //called when complete
			  },
			  success: function(data, textStatus, xhr) {
			    //called when successful
			    if(data.success){
			    	window.location.href=data.url;
			    } else {
			    	swal(data.msg);
			    }
			  },
			  error: function(data, textStatus, errorThrown) {
			    //called when there is an error
			    swal(data.msg);
			  }
			});
			
			return false;
		});
	</script>
</body>
</html>