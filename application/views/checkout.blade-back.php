<?php
//loading assets helper
defined('BASEPATH') OR exit('No direct script access allowed');
$assetHelper = new AssetHelper\AssetHelper();

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Chekout</title>
	<link rel="stylesheet" href="{{base_url('assets_main/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/main_checkout.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/all.min.css')}}">
	<style type="text/css" >
		.ïframecenpos{
			border:0px !important;
		}
		.layaoutPanelTokenCardForm .canvasbox {
		    margin-left: 10% !important;
		    margin-right:10% !important;
		    width: 100% !important;			
		}
		.Modern .canvasbox .row > span {
			/*webkit-font-smoothing: antialiased !important;*/
			webkit-font-smoothing: antialiased !important;
		    font-family: "Roboto", sans-serif !important;
		    font-weight: 400 !important;
		    font-size: 15px !important;
		    color: #292929 !important;
		    line-height: 1.5em !important;
		    margin-bottom: .5rem !important;
		}

		.Modern .canvasbox .row > input {
			/*webkit-font-smoothing: antialiased !important;*/
			webkit-font-smoothing: antialiased !important;
		    font-family: "Roboto", sans-serif !important;
		    font-weight: 400 !important;
		    font-size: 15px !important;
		    color: #292929 !important;
		    line-height: 1.5em !important;
		    margin-bottom: .5rem !important;
		}
	
	</style>
</head>
<body>
	<nav class="navbar navbar-light bg-light">
	  <a class="navbar-brand" href="#">
	    <img src="{{base_url('assets_main/imgs/suncash-logo.png')}}">
	  </a>
	</nav>

	<main class="v4">
		<div class="container">
			<div class="row main">
				<div class="col-lg-4">
					<div class="order-summary">
						<div class="header">
							<div class="shop-logo">
								@if(!empty($post_data))
									<img src="{{$post_data['profile_pic']}}">
								@else
									<img src="{{base_url('assets_main/imgs/shop-logo.png')}}">
								@endif
							</div>
							<div class="shop-name  align-self-center">
								<div>
									<h2>{{$post_data['MerchantName']}}</h2>
									<span>Order Summary</span>
								</div>
							</div>
						</div>
						<div class="table-responsive mb-4">
							<table class="table table-borderless">
								<thead>
							    <tr>
							      <th scope="col">Description</th>
							      <th scope="col" class="text-right">Amount</th>
							    </tr>
							  </thead>
							  <tbody>
							  	@if(!empty($post_data))
							  		@if(!empty($post_data['ItemName']))
							  			<?php 
							  			$count = count($post_data['ItemName']); 
							  			$qty_total = 0 ;
							  			?>
							  			@for($i=0;$i<$count; $i++)
										    <tr>
										      <td>
										      	<span class="item-name">{{$post_data['ItemName'][$i]}}</span>
										      	<span class="item-number"></span>
										      	<span class="quantity">Quantity: {{$post_data['ItemQty'][$i]}}</span>
										      </td>
										      <td class="text-right amount">$  {{$post_data['ItemPrice'][$i]}}</td>
										    </tr>	
										    <?php $qty_total+=$post_data['ItemQty'][$i]++; ?>						  			
							  			@endfor
							    	@else

							    	@endif
							  	@else

							  	@endif
							    <tr class="item-total">
							      <td class="f-medium">Item Total</td><!-- -->
							      <td class="text-right amount">{{!empty($post_data) ? $qty_total : 0.00 }}</td>
							    </tr>
							    <tr class="total">
							      <td class="f-medium text-right">Total</td>
							      <td class="text-right amount">$ {{!empty($post_data) ? $post_data['Amount'] : 0.00 }}</td>
							    </tr>
							  </tbody>
							</table>
						</div>
						<div class="text-center"><button class="btn btn-primary" id="cancel">Cancel</button></div>
						<div class="details row align-items-center">
							<div class="col">
								<img src="{{base_url('assets_main/imgs/suncash-logo.png')}}">
								<span>Protection</span>
							</div>
							<div class="col-3">
								<a href="">Details</a>
							</div>
						</div>
						<p class="text-center">Shop Around the World with Confidence!</p>
					</div>
				</div>
				<div class="col-lg-8">
					<h2 class="title">Choose your Payment Method</h2>
					<div class="accordion" id="accordionExample">
					  <div class="card">
					    <div class="card-header collapsed" id="headingOne"  data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
				      	<i class="fas fa-caret-right"></i>
				      	<div class="title">
			          	<span>Suncash Account</span>
				          <span>Pay using your SunCash account</span>
			          </div>
			          <img src="{{base_url('assets_main/imgs/suncash-icon-light.png')}}" class="colored">
			          <img src="{{base_url('assets_main/imgs/suncash-icon.png')}}" class="light">
					    </div>

					    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
					      <div class="card-body v3">
					      	<?php
					      	//implement checkout settings.
					      	
					      	?>
					        <div class="card">
					        	<div class="card-body">
					        		<div class="suncash_login_section">
								    <h2>Enter your card details below.</h2>
								    <span>We dont share your financial details with merchant.</span>
								    <form id="suncash_form" class="text-left">
											 <div class="form-group">
											    <label for="mobile">Mobile Number</label>
											    <div class="form-div">
											    <input type="text" class="form-control required bfh-phone" id="mobile" name="mobile" placeholder="Enter your mobile number" autocomplete="off" data-format="1 (ddd) ddd-dddd" value ="1 242" >
												</div>
											  </div>
											  <div class="form-group">
											    <label for="pin">Pin</label>
											    <div class="form-div">
											    <input type="password" class="form-control required" id="pin" name="pin" placeholder="Enter your pin" autocomplete="off" maxlength="4">
												</div>
											  </div>
											<input type="hidden" name="MerchantKey" id="MerchantKey" value="{{$post_data['MerchantKey']}}">  
											<div class="text-center"><button type="submit" id="card_next" class="btn btn-primary">Next</button></div>
									</form>
									</div>
									<div class="suncash_verify_section" style="display:none;">
								    <form id="suncash_process_form" class="text-left">
											  <div class="form-group">
											    <label for="passcode">OTP (One Time Password)</label>
											    <div class="form-div">
											    <input type="number" class="form-control required" id="passcode" name="passcode" placeholder="Enter your OTP">
												</div>
											  </div>
											
											<div class="text-center"><button type="submit" id="card_submit" class="btn btn-primary">Process Payment</button></div>
									</form>										
									</div>	
								</div>
							</div>
					      </div>
					    </div>
					  </div>

					  <div class="card">
					    <div class="card-header collapsed" id="headingOne"  data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
				      	<i class="fas fa-caret-right"></i>
				      	<div class="title">
			          	<span>Debit/Credit Cards</span>
				          <span>Pay using your credit/debit cards</span>
			          </div>
			          <img src="{{base_url('assets_main/imgs/card-icon.png')}}" class="colored">
			          <img src="{{base_url('assets_main/imgs/card-icon-light.png')}}" class="light">
					    </div>

					    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
					      <div class="card-body v3">
					        <div class="card">
					        	<div class="card-body">
	

					        	<div id="card_info">
								    <h2>Enter your card details below.</h2>
								    <span>We dont share your financial details with merchant.</span>
								    <form action="" class="text-left">
									<div id="NewCenposPlugin">
										
									</div>
									<div class="text-center"><button type="button" class="btn btn-primary full" id="submit">Submit</button></div>
									<!-- <div class="text-center"><button class="btn btn-primary">Next</button></div> -->
									</form>

									</div>
										<div id="success_section" style="display:none;">
									  		<div class="label text-center">Card Details</div>

										  	<div class="item">
											  <div class="row">
											  	<div class="col">Card Number: </div>
											  	<div class="col text-right"><span id="card_number"></span></div>
											  </div>
											</div>
											<div class="item">
											  <div class="row">
											  	<div class="col">Name on Card: </div>
											  	<div class="col text-right"><span id="name_on_card"></span></div>
											  </div>
											</div>
										  	<div class="item total">
											  <div class="row">
											  	<div class="col label">Card Type: </div>
											  	<div class="col text-right amount"><span id="card_type"></span> </div>
											  </div>
										  	</div>
										  	<input type="hidden" name="tid" id ="tid" value="">
										  	<div class="payment_btn_section">
										  	<div class="text-center"><button type="button" class="btn btn-primary full" id="payment">Process Payment</button></div>
										  	</div>
										  	<div class="cancel_section" style="display:none;">
										  	<div class="text-center"><button type="button" class="btn btn-primary full" id="cancel_payment">Cancel</button></div>	
										  	<div class="text-center"><button type="button" class="btn btn-primary full" id="another_card">Use another card</button></div>										  			
										  	</div>
										</div>
									</div>

								</div>
							<!-- </div> -->
					      </div>
					    </div>
					  </div>

					  <div class="card">
					    <div class="card-header collapsed" id="headingOne"  data-toggle="collapse" data-target="#cashPayment" aria-expanded="true" aria-controls="cashPayment">
				      	<i class="fas fa-caret-right"></i>
				      	<div class="title">
			          	<span>Cash Payment</span>
				          <span>Pay with Cash at SunCash Stores and Kiosks</span>
			          </div>
			          <img src="{{base_url('assets_main/imgs/cash-icon-light.png')}}" class="colored">
			          <img src="{{base_url('assets_main/imgs/cash-icon.png')}}" class="light">
					    </div>

					    <div id="cashPayment" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
					      <div class="card-body v3">
					        <div class="card">
					        	<div class="card-body">
									    <h2>Please enter your name</h2>
									    <!-- <span>We dont share your financial details with merchant.</span> -->
									  <!--   <form id="cashpayment_form" class="text-left">
												<div class="form-group row border-bottom">
												  <input type="text" class="form-control col-12 col-lg-6 required_cash_checkout" id="firstName" placeholder="First Name">
												  <input type="text" class="form-control col-12 col-lg-6 required_cash_checkout" id="lastName" placeholder="Last Name">
												</div> --><!-- 
												<div class="border-bottom"> -->
<!-- 													<p class="table-title">Transaction Details:</p>
													<table class="table table-borderless">
													  <tbody>
													    <tr>
													      <td>Amount</span></td>
													      <td class="text-right">250.00</td>
													    </tr>
													    <tr>
													      <td>Fee</span></td>
													      <td class="text-right">0.00</td>
													    </tr>
													    <tr>
													      <td>VAT</span></td>
													      <td class="text-right">0.00</td>
													    </tr>
													  </tbody>
													  <tfoot>
													  	<tr class="total">
													      <td class="f-medium">Total Payment</span></td>
													      <td class="text-right amount">252.00 BSD</td>
													    </tr>
													  </tfoot>
													</table> -->
<!-- 												</div> -->
<!-- 												<div class="mt-3">
													<p class="table-title">How would you like your code received?</p>
													<div class="custom-radio">
											    	<label class="label-container">
														  <input type="checkbox" id="chkSMS" name="chkSMS" value="0">
														  <span class="checkmark"></span>
														  <span class="label">Send via SMS</span>
														</label>
														<input type="text" placeholder="Enter your mobile number" class="form-control bfh-phone" id="mobile_cash" name="mobile_cash" data-format="1 (ddd) ddd-dddd" value ="1 242" readonly>
														<label class="label-container">
														  <input type="checkbox" id="chkEmail" name="chkEmail" value="0">
														  <span class="checkmark"></span>
														  <span class="label">Send via email</span>
														</label>
														<input  type="email" id="email" name="email" placeholder="Enter your email" class="form-control" readonly>
											    </div>
												</div> -->
												<!-- <div class=""><button type="submit" id="process_payment_cash" class="btn btn-primary btn-process" data-loading-text="Processing...">Generate Payment Code</button></div> -->
											</form>
									  </div>
									</div>
					      </div>
					    </div>
					  </div>
					  
					</div>
				</div>
			</div>
		</div>
	</main>



	<script src="{{base_url('assets_main/js/jquery-3.2.1.min.js')}}"></script>
	<script src="{{base_url('assets_main/js/bootstrap.min.js')}}"></script>
	<script src="{{base_url('assets_main/js/main.js')}}"></script>
    <script src="{{base_url('assets_main/js/select.min.js')}}"></script>
	{!!$assetHelper->link_plugins('js','jquery-1.11.3.min.js')!!}
	{!!$assetHelper->link_plugins('js','bootstrap-3.3.7/js/bootstrap.min.js')!!}
	{!!$assetHelper->link_plugins('js','sidebar-nav/dist/sidebar-nav.min.js')!!}
	{!!$assetHelper->link_plugins('js','override/jquery.slimscroll.js')!!}
	{!!$assetHelper->link_plugins('js','override/waves.js')!!}
	{!!$assetHelper->link_plugins('js','override/custom.min.js')!!}
	{!!$assetHelper->link_plugins('js','sweetalert2-master/dist/sweetalert2.all.min.js')!!}
	{!!$assetHelper->link_plugins('js','bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js')!!}
	{!!$assetHelper->link_plugins('js','curioussolutions-datetimepicker/dist/DateTimePicker.min.js')!!}
    {!!$assetHelper->link_plugins('js','Inputmask-5.x/dist/jquery.inputmask.min.js')!!}
    {!!$assetHelper->link_plugins('js','bootstrap-formhelpers-phone/bootstrap-formhelpers-phone.js') !!}
    {!!$assetHelper->link_plugins('js','bootstrap-formhelpers-phone/bootstrap-formhelpers-phone.format.js') !!}
	{!!$assetHelper->link_plugins('js','dropzone-master/dist/dropzone.js')!!}	
	<script src="https://www.cenpos.com/Plugins/porthole.min.js"></script>
	<script src="https://www.cenpos.com/Plugins/jquery.simplewebpay.js"></script> 
	<script  type="text/javascript" charset="utf-8">
	function CallbackSuccess(responseData) {
	alert(JSON.stringify(responseData));
	}
	function CallbackCancel(responseData) {
	alert(JSON.stringify(responseData));
	}

	$("#submit").click(function(){
		$("#NewCenposPlugin").submitAction();
	});

	var merchant='{{CENPOST_MERCHANT_ID}}';
	$("#NewCenposPlugin").createWebpay({
	url: 'https://www.cenpos.net/simplewebpay/cards',
	params : "merchantid="+merchant+"&iscvv=true",
	height:'350px',
	//isCvv :true&SecretKey=a0c70a0d5aa451bfc02f17e9199e41e6&verifyingpost="+vp
	sessionToken:false,
	    beforeSend:function(xhr, textStatus) {
	        //called when complete
	        $("#submit").prop('disabled', true);
	      },
	    complete: function(xhr, textStatus) {
	        //called when complete
	         $("#submit").prop('disabled', false);
	      },
        success: function(data){
        	//console.log(data);
        	//console.log(data.ProtectedCardNumber);

        	$("#card_number").text(data.ProtectedCardNumber);
        	$("#name_on_card").text(data.NameonCard);
        	$("#card_type").text(data.CardType);
        	$("#tid").val(data.RecurringSaleTokenId);
        	$("#success_section").show();
        	$("#card_info").hide();

        	//insert card info....
/*            if (isDefined(CallbackSuccess) && CallbackSuccess) window[CallbackSuccess](msg);
            else{

            }*/
        },
        cancel: function(response){
        	$("#success_section").hide();
        	$("#card_info").show();
/*            if (isDefined(CallbackCancel) && CallbackCancel) window[CallbackCancel]("Error");
            else{

            }*/
        }	
	});

	$("#suncash_form").submit(function(){
		$('.hb').remove();
		$('.form-div').removeClass('has-error').removeClass('has-success');

		//check validation
		var err_count = 0;
		var to_req=[];
		//jquery blank validation..
		$("#suncash_form .required").each(function(){
		    var field_id = $(this).attr("id");
		    var data=[];
		    if($(this).val()==""){
		      data['id']=field_id;      
		      to_req.push(data);
		      err_count++;
		    }
		});

		if(err_count>0){
		  swal(
		  'Oops...',
		  "Please do check required fields.",
		  'error'
		  );  
		  $.each(to_req, function( index, value ) {
		    $("#"+value.id).parent().after('<div class="hb"><div class="help-block" style="color:#f38989; font-size:7pt;margin-bottom:0px !important;">Required filed.</div></div>').addClass('has-error');
		  });        
		  return false;
		}
		var form_data = $(this).serializeArray();
		$.ajax({
		  url: '{{base_url("payment/generate_passcode")}}',
		  type: 'POST',
		  dataType: 'json',
		  data: form_data,
	    	beforeSend:function(xhr, textStatus) {
	        //called when complete
	        $("#card_next").prop('disabled', true);
	      },
	    	complete: function(xhr, textStatus) {
	        //called when complete
	         $("#card_next").prop('disabled', false);
	      },
		  success: function(data, textStatus, xhr) {
		    //called when successful
		    if(data.success){
		    	console.log(data.data);
				$(".suncash_verify_section").show();
				$(".suncash_login_section").hide();		    	
		    }
		  },
		  error: function(data, textStatus, errorThrown) {
		    //called when there is an error
		    
		  }
		});
		



		return false;
	});
	$("#suncash_process_form").submit(function(){
		$('.hb').remove();
		$('.form-div').removeClass('has-error').removeClass('has-success');

		//check validation
		var err_count = 0;
		var to_req=[];
		//jquery blank validation..
		$("#suncash_process_form .required").each(function(){
		    var field_id = $(this).attr("id");
		    var data=[];
		    if($(this).val()==""){
		      data['id']=field_id;      
		      to_req.push(data);
		      err_count++;
		    }
		});

		if(err_count>0){
		  swal(
		  'Oops...',
		  "Please do check required fields.",
		  'error'
		  );  
		  $.each(to_req, function( index, value ) {
		    $("#"+value.id).parent().after('<div class="hb"><div class="help-block" style="color:#f38989; font-size:7pt;margin-bottom:0px !important;">Required filed.</div></div>').addClass('has-error');
		  });        
		  return false;
		}
		var form_data = {
			passcode:$("#passcode").val(),
			mobile:$("#mobile").val(),
		};
		$.ajax({
		  url: '{{base_url("payment/process_suncash_payment")}}',
		  type: 'POST',
		  dataType: 'json',
		  data: form_data,
	    	beforeSend:function(xhr, textStatus) {
	        //called when complete
	        $("#card_submit").prop('disabled', true);
	        $("#card_submit").text("processing... please wait");
	      },
	    	complete: function(xhr, textStatus) {
	        //called when complete
	         $("#card_submit").prop('disabled', false);
	         $("#card_submit").text("Process Payment");
	      },
		  success: function(data, textStatus, xhr) {
		    //called when successful
		    if(data.success){
		    	//$(".suncash_verify_section").show();
		    	//$(".suncash_login_section").hide();
            	console.log('{{base_url("payment/successpage_redirect")}}/'+data.reference+'/success/wallet/{{urlencode(base64_encode($post_data["CallbackURL"]))}}');
            	//alert('{{base64_encode($post_data["CallbackURL"])}}');
            	// window.location.href='{{$post_data["CallbackURL"]}}/reference='+data.reference;
            	// window.location.assign("https://www.example.com");
            	window.location.href='{{base_url("payment/successpage_redirect")}}/'+data.reference+'/success/wallet/{{urlencode(base64_encode($post_data["CallbackURL"]))}}';
		    	
		    } else {
		    	swal(data.msg);
		    }
		  },
		  error: function(data, textStatus, errorThrown) {
		    //called when there is an error
		    swal(data.msg);
		  }
		});
		



		return false;
	});


	//for cenpos process payment
    $("#payment").click(function(e){

    	var form_data ={
    	//merchant:merchant,	
    	tokenid:$("#tid").val(),
    	card_type:$("#card_type").text(),
    	card_number:$("#card_number").text(),
    	name_on_card:$("#name_on_card").text(),
        }   
		$.ajax({
          url: '{{base_url("payment/process_payment")}}',
          type: 'POST',
          dataType: 'json',
          data: form_data,
          beforeSend:function(){
            $("#payment").prop("disabled",true);
            $("#payment").text("processing... please wait");
            //loader.showPleaseWait();
          },
          complete: function(xhr, textStatus) {
            $("#payment").prop("disabled",false);
            $("#payment").text("Process Payment");
            //loader.hidePleaseWait();
          },
          success: function(data, textStatus, xhr) {
            //console.log();
            if(data.success){
            	//alert(data.msg);
            	//go to callbackurlshit  urlencode(base64_encode('http://stackoverflow.com/questions/9585034'))
            	console.log('{{base_url("payment/successpage_redirect")}}/'+data.reference+'/success/card/{{urlencode(base64_encode($post_data["CallbackURL"]))}}');
            	//alert('{{base64_encode($post_data["CallbackURL"])}}');
            	// window.location.href='{{$post_data["CallbackURL"]}}/reference='+data.reference;
            	// window.location.assign("https://www.example.com");
            	window.location.href='{{base_url("payment/successpage_redirect")}}/'+data.reference+'/success/card/{{urlencode(base64_encode($post_data["CallbackURL"]))}}';
            } else {
                swal(data.msg);
              	$(".cancel_section").show();
		    	$(".payment_btn_section").hide();
            }
          },
          error: function(xhr, textStatus, errorThrown) {
/*            //called when there is an error
            $("#payment").prop("disabled",false);
            $("#payment").text("Submit");*/
          }
        });


    });
    $("#cancel").click(function(){
    	$.ajax({
    	  url: '{{base_url("payment/cancel_order")}}',
    	  type: 'POST',
    	  dataType: 'json',
    	  data: {reference_id: '{{$post_data["OrderReference"]}}'},
    	  complete: function(xhr, textStatus) {
    	    //called when complete
    	  },
    	  success: function(data, textStatus, xhr) {
    	   	if(data.success){
    	   		//history.back();
    	   		//console.log(data.url);return false;
    	   		swal("Order has been cancelled.");
    	   		window.location.href=data.url+data.params+"/"+'{{urlencode(base64_encode($post_data["CallbackURL"]))}}';
    	   	} else {
    	   		swal(data.msg);
    	   	}
    	  },
    	  error: function(data, textStatus, errorThrown) {
    	    //called when there is an error
    	    swal("Something went wrong.");
        	$(".cancel_section").show();
        	$(".payment_btn_section").hide()    	    
    	  }
    	});
    	
    });


    $("#cancel_payment").click(function(){
    	$.ajax({
    	  url: '{{base_url("payment/cancel_order")}}',
    	  type: 'POST',
    	  dataType: 'json',
    	  data: {reference_id: '{{$post_data["OrderReference"]}}'},
    	  complete: function(xhr, textStatus) {
    	    //called when complete
    	  },
    	  success: function(data, textStatus, xhr) {
    	   	if(data.success){
    	   		//history.back();data.msg
    	   		swal("Order has been cancelled.");
    	   		window.location.href=data.url+data.params+"/"+'{{urlencode(base64_encode($post_data["CallbackURL"]))}}';
    	   	} else {
    	   		swal(data.msg);
    	   	}
    	  },
    	  error: function(data, textStatus, errorThrown) {
    	    //called when there is an error
    	    swal("Something went wrong.");
    	    
    	  }
    	});
    	
    });    

    $("#another_card").click(function(){
    	$("#success_section").hide();
    	$("#card_info").show();
    	$(".cancel_section").hide();
    	$(".payment_btn_section").show();        	
    });

	$("#chkEmail").click(function(){
	  if($(this).is(":checked")){
	    $("#email").addClass('required_cash_checkout');
	    $("#email").prop('readonly',false);
	  } else {
	    $(this).val("0");
	    $("#email").removeClass('required_cash_checkout');
	    $("#email").prop('readonly',true);
	  }
	});
	$("#chkSMS").click(function(){
	  if($(this).is(":checked")){
	    $("#mobile_cash").addClass('required_cash_checkout');
	    $("#mobile_cash").prop('readonly',false);
	  } else {
	    $(this).val("0");
	    $("#mobile_cash").removeClass('required_cash_checkout');
	    $("#mobile_cash").prop('readonly',true);
	  }
	});


	$("#cashpayment_form").on('submit',function(e){


	    swal({
	      title: 'Please confirm payment details.',
	      // text: "",
	      type: 'warning',
	      showCancelButton: true,
	      confirmButtonColor: '#3085d6',
	      cancelButtonColor: '#d33',
	      confirmButtonText: 'Yes!'
	    }).then((result) => {
	      if (result.value) {
	    	e.preventDefault();
			  $('.hb').remove();
			  $('.form-div').removeClass('has-error').removeClass('has-success');

			  //check validation
			  var err_count = 0;
			  var to_req=[];
			  //jquery blank validation..
			  $(".required_cash_checkout").each(function(){
			      var field_id = $(this).attr("id");
			      var data=[];
			      if($(this).val()==""){
			        data['id']=field_id;      
			        to_req.push(data);
			        err_count++;
			      }
			  });

			  /*if($("#password").val()!=$("#cpassword").val()){
			    swal("Youre password and confirmation password do not match.");
			    return false;
			  }*/


			  if(err_count>0){
			    swal(
			    'Opps...',
			    "Please do check required fields.",
			    'error'
			    );  
	/*		    $.each(to_req, function( index, value ) {
			      $("#"+value.id).parent().after('<div class="hb"><div class="help-block" style="color:#f38989; font-size:7pt;margin-bottom:0px !important;">Required filed.</div></div>').addClass('has-error');
			    });   */     
			    return false;
			  }
	      		var is_email =0;
	      		var is_sms=0;

	      		if($("#chkSMS").is(":checked")){
	      			 //alert("y");
	      			is_sms=1;

	      		}
	      		if($("#chkEmail").is(":checked")){
	      			  //alert("y");
	      			is_email=1;
	     			
	      		}
		        var form_data = {
		        	//notes:$("#notes").val(),
		        	//reference_num:$("#reference_num").val(),
					//amount:$("#amount").val(),
					email:$("#email").val(),
					mobile:$("#mobile").val(),
					firstName:$("#firstName").val(),
					lastName:$("#lastName").val(),
					is_email:is_email,
					is_sms:is_sms,
					//amount_val:$("#amount_total").val(),
					//hid_fee:$("#hid_fee").val(),
					//hid_vat:$("#hid_vat").val(),
					//balance:$("#balance").val()	
		        };
		        $.ajax({
		          url: "<?=site_url("payment/process_cash_checkout")?>",
		          type: 'POST',
		          dataType: 'json',
		          data: form_data,
		          beforeSend:function(xhr, textStatus) {
		            //called when complete
		            $("#process_payment_cash").prop('disabled', true);
		          },
		          complete: function(xhr, textStatus) {
		            //called when complete
		             $("#process_payment_cash").prop('disabled', false);
		          },
		          success: function(data) {
		            if(data.success){
		                //alert(data.msg);
		                window.location.href=data.url;
/*		                var str="<div class='alert alert-dismissible alert-success'><strong>Thank you for your payment ! Please do copy the payment code to present to the nearest suncash store. </strong>.</div>";
		                $("#msg").html(str);
		                $("#success_section").show();
		                $("#cash_section").hide();

		               	$("#transaction_code").text(data.msg);
		               	$("#notes_value").text($("#notes").val());
		               	$("#ref_value").text($("#reference_num").val());
		                $("#amount_value").text($("#amount").val());
		                $("#email").text($("#email").val());
		               	$("#mobile").text($("#mobile").val());
		                $("#fname").text($("#fname").val());
		                $("#lname").text($("#lname").val());
		               	// $("#transaction_section").show();
		               	$(".transaction_details").show();*/
		               	
		                //otable.ajax.reload();
		                //$("#transfer_modal").modal('hide');
		             	$("#process_payment_cash").prop('disabled', false);
		            } else {
		            	swal(data.msg);
/*		                var str="<div class='alert alert-dismissible alert-danger'><strong> "+data.msg+" </strong>.</div>";
		                $("#msg").html(str);
		                $("#cash_section").hide();
		                $("#success_section").show();
						$("#transaction_details").hide();*/

		               	// $("#transaction_code").val('');
		               	// $("#transaction_section").hide();
		   
		               	
		             	$("#process_payment_cash").prop('disabled', false);		
		            }
		          },
		          error: function(data, textStatus, errorThrown) {
		            //called when there is an error
		           		alert(data.msg);
		             $("#process_payment_cash").prop('disabled', false);
		          }
		        });
	      }
	    });
	    return false;




		});


	</script>
</body>
</html>