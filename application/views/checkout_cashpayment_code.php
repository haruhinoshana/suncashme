<?php
//loading assets helper
defined('BASEPATH') OR exit('No direct script access allowed');
$assetHelper = new AssetHelper\AssetHelper();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Document</title>
	<link rel="stylesheet" href="{{base_url('assets_checkout/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_checkout/css/main.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_checkout/css/responsive.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_checkout/css/all.min.css')}}">
</head>
<body class="checkout-payment-code">
        <div class="shop-logo-header">
            @if(!empty($post_data))
                <img src="{{$post_data['profile_pic']}}">
            @else
                <img src="{{base_url('assets_main/imgs/shop-logo.png')}}">
            @endif
        </div>

	<main class="">
		<div class="container">
			<div class="row main">
				<div class="col-lg-4">
					<div class="order-summary">
						<div class="header">
							<div class="total-payment">
								You made a total payment of <span class="amount">$ {{!empty($post_data) ? $post_data['Amount'] : 0.00 }}</span>
							</div>
						</div>
						<div class="table-responsive mb-4">
							<table class="table table-borderless">
								<thead>
							    <tr>
							      <th scope="col">Description</th>
							      <th scope="col" class="text-right">Amount</th>
							    </tr>
							  </thead>
							  <tbody>
							    <tr>
							      <td>
							      	<span class="item-name">{{$post_data['MerchantName']}}</span>
							      	<span class="item-number">Transaction ID: {{$post_data['OrderID']}}</span>
							      </td>
							      <td class="text-right amount">$ {{!empty($post_data) ? $post_data['Amount'] : 0.00 }}</td>
							    </tr>
							    <!-- <tr class="item-total">
							      <td class="f-medium">Item Total</td>
							      <td class="text-right amount">$ 180.00</td>
							    </tr> -->
							  </tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="col-lg-8">
					<div class="card">
						<div class="card-header title-header">
					    Payment Code
					    <img src="{{base_url('assets_main/imgs/suncash-logo.png')}}">
					  </div>
					  <div class="card-body">
					  	<div class="receipt-container text-center">
                                <img src="data:image/png;base64,{{$cash_payment_qr}}" class="qr">
                                <!-- <img src="data:'.$qr->getContentType().';base64,'.$qr->generate().'" /> -->
								<div class="code">
									<span>Your Payment Code</span>
                                    <span>{{$cash_payment_code}}</span>
                                    <!-- <span>{{$cash_payment_barcode}}</span> -->
                                    
								</div>
                                <!-- <img src="{{base_url('./assets_checkout/imgs/barcode@3x.png')}}" class="barcode"> -->
                                <img src="data:image/png;base64,{{$cash_payment_barcode}}"   class="barcode">
								<!-- <button class="btn" data-toggle="modal" data-target="#storeLocator">Store Locator</button> -->

                                @if(!empty($cash_posted_data))
                                    @if($cash_posted_data['is_email']=='1' && $cash_posted_data['is_sms']=='1')
                                        <div class="message">
                                            <p>We also sent this code to your mobile number (<span class="mobile-number">{{$cash_posted_data['mobile']}}</span>) and your email (<span class="email">{{$cash_posted_data['email']}}</span>).</p>
                                        </div>
                                    @elseif($cash_posted_data['is_email']=='1')
                                        <div class="message">
                                            <p>We also sent this code to your email (<span class="email">{{$cash_posted_data['email']}}</span>).</p>
                                        </div>
                                    @elseif($cash_posted_data['is_sms']=='1')    
                                        <div class="message">
                                        <p>We also sent this code to your mobile number (<span class="mobile-number">{{$cash_posted_data['mobile']}}</span>).</p>
                                        </div>
                                    @endif
                                @endif
                                
								

								<div class="transaction-details">
									<p class="table-title">Transaction Details</p>
									<table>
										<tbody>
											<tr>
												<td>Transaction ID:</td>
												<td>{{$post_data['reference_id']}}</td>
											</tr>
											<tr>
												<td>Payment Method:</td>
												<td>Cash</td>
											</tr>
											<tr class="total">
												<td>Total Payment:</td>
												<td>{{!empty($post_data) ? $post_data['Amount'] : 0.00 }} BSD</td>
											</tr>
										</tbody>

									</table>
								</div>
								<div class="footer"><button onclick="window.location.href='{{$post_data['CallbackURL']}}/{{$cash_return_params}}'" class="btn btn-primary">Return</button> <span>to <span class="">{{$post_data['MerchantName']}}</span></span></div>
							</div>
					  </div>
					</div>					  
				</div>
			</div>
		</div>
	</main>

	<footer>
		<div class="container">
			<div class="row d-flex align-items-center">
				<div class="col-12 col-lg-6">
					<div class="copyright">© Copyright Suncash 2018. All Rights Reserved.</div>
					<div class="link">
						<a href="#">Privacy Policy</a><span>|</span><a href="#">Terms & Conditions</a>
					</div>
				</div>
				<div class="col-12 col-lg-6 logo">
                <img src="{{base_url('assets_main/imgs/suncash-logo.png')}}">
				</div>
			</div>
		</div>
	</footer>



	<div class="modal fade" id="storeLocator" tabindex="-1" role="dialog">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="row">
	      	<div class="col-3 store-location" style="overflow-y: auto; height:595px; ">
	      		<div class="store-count">
	      			<div><span class="text-primary">7 Stores</span> near you</div>
	      		</div>
	      		<div class="row store">
      				<div class="col-1">
      					<i class="fas fa-map-marker-alt"></i>
      				</div>
      				<div class="col">
      					<div class="store-name">7/11 Store</div>
      					<div class="store-address">#4 Carmichael & East Ave, Nassau</div>
      					<div class="store-travel-time">32 mins walk away</div>
      				</div>
      			</div>
      			<div class="row store">
      				<div class="col-1">
      					<i class="fas fa-map-marker-alt"></i>
      				</div>
      				<div class="col">
      					<div class="store-name">Ministop</div>
      					<div class="store-address">3634  Poplar Chase Lane, Nassau</div>
      					<div class="store-travel-time">32 mins walk away</div>
      				</div>
      			</div>
      			<div class="row store">
      				<div class="col-1">
      					<i class="fas fa-map-marker-alt"></i>
      				</div>
      				<div class="col">
      					<div class="store-name">7/11 Store</div>
      					<div class="store-address">#4 Carmichael & East Ave, Nassau</div>
      					<div class="store-travel-time">32 mins walk away</div>
      				</div>
      			</div>
      			<div class="row store">
      				<div class="col-1">
      					<i class="fas fa-map-marker-alt"></i>
      				</div>
      				<div class="col">
      					<div class="store-name">Ministop</div>
      					<div class="store-address">3634  Poplar Chase Lane, Nassau</div>
      					<div class="store-travel-time">32 mins walk away</div>
      				</div>
      			</div>
      			<div class="row store">
      				<div class="col-1">
      					<i class="fas fa-map-marker-alt"></i>
      				</div>
      				<div class="col">
      					<div class="store-name">7/11 Store</div>
      					<div class="store-address">#4 Carmichael & East Ave, Nassau</div>
      					<div class="store-travel-time">32 mins walk away</div>
      				</div>
      			</div>
      			<div class="row store">
      				<div class="col-1">
      					<i class="fas fa-map-marker-alt"></i>
      				</div>
      				<div class="col">
      					<div class="store-name">Ministop</div>
      					<div class="store-address">3634  Poplar Chase Lane, Nassau</div>
      					<div class="store-travel-time">32 mins walk away</div>
      				</div>
      			</div>
      			<div class="row store">
      				<div class="col-1">
      					<i class="fas fa-map-marker-alt"></i>
      				</div>
      				<div class="col">
      					<div class="store-name">7/11 Store</div>
      					<div class="store-address">#4 Carmichael & East Ave, Nassau</div>
      					<div class="store-travel-time">32 mins walk away</div>
      				</div>
      			</div>

	      	</div>
	      	<div class="col store-map">
	      		<div class="modal-header">
			        <div class="input-group search-store">
							  <input type="text" class="form-control" placeholder="Search a store" aria-label="Recipient's username" aria-describedby="basic-addon2">
							  <div class="input-group-append">
							    <span class="input-group-text" id="basic-addon2"><i class="fas fa-search"></i></span>
							  </div>
							</div>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<img src="{{base_url('./assets_checkout/imgs/close-icon.png')}}" class="close-icon">
			        </button>
			      </div>
			      <div class="modal-body"><div id="map" style="height:520px; "></div></div>
	      	</div>
	      </div>
	    </div>
	  </div>
	</div>


    <script src="{{base_url('assets_checkout/js/jquery-3.2.1.min.js')}}"></script>
	<script src="{{base_url('assets_checkout/js/bootstrap.min.js')}}"></script>
    <script src="{{base_url('assets_checkout/js/main.js')}}"></script>
</body>
</html>