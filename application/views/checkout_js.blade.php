<script>
function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}
function check_ccwhitelist(card_info){

    var formdata={
        ProtectedCardNumber:card_info.ProtectedCardNumber,
        NameonCard:card_info.NameonCard,
        CardType:card_info.CardType,
        merchant_key:"{{$post_data['MerchantKey']}}",
        source:"checkout",
        amount:parseFloat($("#amount").text()),
    };

    $.ajax({
      url: '{{base_url("payment/check_whitelist")}}',
      type: 'POST',
      dataType: 'json',
      data: formdata,
      beforeSend: function(xhr, textStatus) {
        //called when complete
        //$("#card_info").hide();
        loader.showPleaseWait();
      },		  
      complete: function(xhr, textStatus) {
        //called when complete
        loader.hidePleaseWait();
      },
      success: function(data, textStatus, xhr) {
        //called when successful
        if(!data.success){
            //alert("repeat data");
            if(data.status=='active'){
            swal({
              title: 'We need more information to process this order.',
              // text: "We need more information to verify your card ending with "+card_info.ProtectedCardNumber+" to continue this order.",
              // text: "",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#FF8400',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Proceed'
            }).then((result) => {
              if (result.value) {
                  //alert("validate");

                $("#last4digits").val(card_info.ProtectedCardNumber);
                $("#card_type_w").val(card_info.CardType);
                $("#card_name").val(card_info.NameonCard);	
                $("#wid").val(data.data.id);			      	

                  //$("#modal_validate_card").modal('show');
                  $("#cardValidation").modal('show');
              } /*else{
                  alert("cancelled");
              }*/	
            });
            } else if (data.status=='rejected'){
                swal("Unfortunately, your card was rejected and not authorized to continue this transaction.");
            } else if (data.status=='for_approval'){
                swal("Card not validated yet.");
            }

            $("#card_info").show();
        } else {
        $("#card_number").text(card_info.ProtectedCardNumber);
        $("#name_on_card").text(card_info.NameonCard);
        $("#card_type").text(card_info.CardType);
        $("#tid").val(card_info.RecurringSaleTokenId);
        $("#success_section").show();
        $("#card_info").hide();
        }
      },
      error: function(xhr, textStatus, errorThrown) {
        //called when there is an error
      }
    });
    
}
function check_ccblocklist(card_info){

var formdata={
    ProtectedCardNumber:card_info.ProtectedCardNumber,
    NameonCard:card_info.NameonCard,
    CardType:card_info.CardType,
    merchant_key:"{{$post_data['MerchantKey']}}",
    source:"checkout",
    amount:parseFloat($("#amount").text()),
};

$.ajax({
url: '{{base_url("payment/check_blocklist")}}',
type: 'POST',
dataType: 'json',
data: formdata,
beforeSend: function(xhr, textStatus) {
    //called when complete
    //$("#card_info").hide();
    loader.showPleaseWait();
},		  
complete: function(xhr, textStatus) {
    //called when complete
    loader.hidePleaseWait();
},
success: function(data, textStatus, xhr) {
    //called when successful
    if(!data.success){
        swal(data.msg);
        
        $("#card_info").show();
    } else {
    $("#card_number").text(card_info.ProtectedCardNumber);
    $("#name_on_card").text(card_info.NameonCard);
    $("#card_type").text(card_info.CardType);
    $("#tid").val(card_info.RecurringSaleTokenId);
    $("#success_section").show();
    $("#card_info").hide();
    }
},
error: function(xhr, textStatus, errorThrown) {
    //called when there is an error
}
});

}



function reinitialize_session(){
/*		alert("reinitialize session");
    var to_send ={
        'sunpass_fee':'{{$post_data["sunpass_fee"]}}',
    };
    $.ajax({
      url: '{{base_url("payment/reinitialize_session")}}',
      type: 'POST',
      dataType: 'json',
      data: to_send,
        beforeSend:function(xhr, textStatus) {
        //called when complete
        //loader.showPleaseWait();
        //$("#card_next").prop('disabled', true);
      },
        complete: function(xhr, textStatus) {
        //called when complete
        //loader.hidePleaseWait();
        //$("#card_next").prop('disabled', false);
      },
      success: function(data, textStatus, xhr) {
        //called when successful
        if(data.success){
            alert(data.session);
            //console.log(data.data);
            //$(".suncash_verify_section").show();
            //$(".suncash_login_section").hide();		    	
        }
      },
      error: function(data, textStatus, errorThrown) {
        //called when there is an error
        
      }
    });	*/	
}
//reinitialize_session();

function CallbackSuccess(responseData) {
alert(JSON.stringify(responseData));
}
function CallbackCancel(responseData) {
alert(JSON.stringify(responseData));
}

$("#submit").click(function(){
    // loader.showPleaseWait();
    $("#NewCenposPlugin").submitAction();
});
//create site verify
var merchant='{{CENPOST_MERCHANT_ID}}';
var verify_params ='{{$verify_params["Data"]}}';
$("#NewCenposPlugin").createWebpay({
url: 'https://www.cenpos.net/simplewebpay/cards',
params : "email=suncashme@gmail.com&verifyingpost="+verify_params+"&iscvv=true",
height:'350px',
sessionToken:false,
    beforeSend:function(xhr, textStatus) {
        //called when complete
        $("#submit").prop('disabled', true);
      },
    complete: function(xhr, textStatus) {
        //called when complete
         $("#submit").prop('disabled', false);
      },
    success: function(data){
        //console.log(data);
        //console.log(data.ProtectedCardNumber);
        if(data.Result==0){
        // loader.hidePleaseWait();	

        //check_ccwhitelist(data);
        @if(in_array($post_data['MerchantKey'],SUNPASS_MERCHANTS_KEYS))
        check_ccblocklist(data);
        @else
        $("#card_number").text(data.ProtectedCardNumber);
        $("#name_on_card").text(data.NameonCard);
        $("#card_type").text(data.CardType);
        $("#tid").val(data.RecurringSaleTokenId);
        $("#success_section").show();
        $("#card_info").hide();
        @endif


        //return false;

        } else {
        $("#tid").val('');
        // swal(data.Message);	
        // loader.hidePleaseWait();
        }

        //insert card info....
/*            if (isDefined(CallbackSuccess) && CallbackSuccess) window[CallbackSuccess](msg);
        else{

        }*/
    },
    cancel: function(response){
        // loader.hidePleaseWait();
        $("#tid").val('');
        // swal(response.Message);	
        swal(response.Message);	
        if(response.Message!="Error validation Captcha"){
            swal(response.Message);	
            $("#success_section").hide();
            $("#card_info").show();
        }       	

/*            if (isDefined(CallbackCancel) && CallbackCancel) window[CallbackCancel]("Error");
        else{

        }*/
    }	
});

$("#suncash_form").submit(function(e){
    $('.hb').remove();
    $('.form-div').removeClass('has-error').removeClass('has-success');

    //check validation
    var err_count = 0;
    var to_req=[];
    //jquery blank validation..
    $("#suncash_form .required").each(function(){
        var field_id = $(this).attr("id");
        var data=[];
        if($(this).val()==""){
          data['id']=field_id;      
          to_req.push(data);
          err_count++;
        }
    });

    if(err_count>0){
      swal(
      'Oops...',
      "Please do check required fields.",
      'error'
      );  
      $.each(to_req, function( index, value ) {
        $("#"+value.id).parent().after('<div class="hb"><div class="help-block" style="color:#f38989; font-size:7pt;margin-bottom:0px !important;">Required filed.</div></div>').addClass('has-error');
      });        
      return false;
    }
    var form_data = $(this).serializeArray();
    $.ajax({
      url: '{{base_url("payment/generate_passcode")}}',
      type: 'POST',
      dataType: 'json',
      data: form_data,
        beforeSend:function(xhr, textStatus) {
        //called when complete
        // loader.showPleaseWait();
        $("#card_next").prop('disabled', true);
      },
        complete: function(xhr, textStatus) {
        //called when complete
        // loader.hidePleaseWait();
        $("#card_next").prop('disabled', false);
      },
      success: function(data, textStatus, xhr) {
        //called when successful
        if(data.success){
            console.log(data.data);
            $(".suncash_verify_section").show();
            $(".suncash_login_section").hide();		    	
        } else {
            swal(data.msg);
        }
      },
      error: function(data, textStatus, errorThrown) {
        //called when there is an error
        
      }
    });
    



    return false;
});
$("#suncash_process_form").submit(function(e){
    $('.hb').remove();
    $('.form-div').removeClass('has-error').removeClass('has-success');

    //check validationhas-danger
    var err_count = 0;
    var to_req=[];
    //jquery blank validation..
    $("#suncash_process_form .required").each(function(){
        var field_id = $(this).attr("id");
        var data=[];
        if($(this).val()==""){
          data['id']=field_id;      
          to_req.push(data);
          err_count++;
        }
    });

    if(err_count>0){
      swal(
      'Oops...',
      "Please do check required fields.",
      'error'
      );  
      $.each(to_req, function( index, value ) {
        $("#"+value.id).parent().after('<div class="hb"><div class="help-block" style="color:#f38989; font-size:7pt;margin-bottom:0px !important;">Required filed.</div></div>').addClass('has-error');
      });        
      return false;
    }
    var form_data = {
        passcode:$("#passcode").val(),
        mobile:$("#mobile").val(),
        m:'{{$post_data["MerchantKey"]}}',
        r:'{{$post_data["reference_id"]}}',			
    };
    $.ajax({
      url: '{{base_url("payment/process_suncash_payment")}}',
      type: 'POST',
      dataType: 'json',
      data: form_data,
        beforeSend:function(xhr, textStatus) {
        //called when complete
        // loader.showPleaseWait();
        $("#card_submit").prop('disabled', true);
        $("#card_submit").text("processing... please wait");
      },
        complete: function(xhr, textStatus) {
        //called when complete
/*	         $("#card_submit").prop('disabled', false);
         $("#card_submit").text("Process Payment");*/
      },
      success: function(data, textStatus, xhr) {
        //called when successful
        if(data.success){
            //$(".suncash_verify_section").show();
            //$(".suncash_login_section").hide();
/*            	console.log('{{base_url("payment/successpage_redirect")}}/'+data.reference+'/success/wallet/{{urlencode(base64_encode($post_data["CallbackURL"]))}}');
            //alert('{{base64_encode($post_data["CallbackURL"])}}');
            // window.location.href='{{$post_data["CallbackURL"]}}/reference='+data.reference;
            // window.location.assign("https://www.example.com");
            window.location.href='{{base_url("payment/successpage_redirect")}}/'+data.reference+'/success/wallet/{{urlencode(base64_encode($post_data["CallbackURL"]))}}';*/
            window.location.href=data.url;
            
        } else {
            // loader.hidePleaseWait();
            $(".suncash_login_section").show();
            $(".suncash_verify_section").hide();
             $("#card_submit").prop('disabled', false);
             $("#card_submit").text("Process Payment");		    	
            swal(data.msg);
        }
      },
      error: function(data, textStatus, errorThrown) {
        //called when there is an error
        // loader.hidePleaseWait();
        $("#suncash_login_section").show();
        $(".suncash_verify_section").hide();
         $("#card_submit").prop('disabled', false);
         $("#card_submit").text("Process Payment");			    
        swal(data.msg);
      }
    });
    



    return false;
});


//for cenpos process payment
$("#payment").click(function(e){

    var form_data ={
    //merchant:merchant,	
    tokenid:$("#tid").val(),
    name_customer_card:$("#name_card").val(),
    email_card:$("#email_card").val(),
    mobile_card:$("#mobile_card").val(),
    card_type:$("#card_type").text(),
    card_number:$("#card_number").text(),
    name_on_card:$("#name_on_card").text(),
    m:'{{$post_data["MerchantKey"]}}',
    r:'{{$post_data["reference_id"]}}',
    }   
    $.ajax({
      url: '{{base_url("payment/process_payment")}}',
      type: 'POST',
      dataType: 'json',
      data: form_data,
      beforeSend:function(){
          loader2.showPleaseWait();
        $("#payment").prop("disabled",true);
        $("#payment").text("Processing... Please wait");
        //loader.showPleaseWait();
      },
      complete: function(xhr, textStatus) {
        $("#payment").prop("disabled",false);
        $("#payment").text("Process Payment");
        //loader.hidePleaseWait();
      },
      success: function(data, textStatus, xhr) {
        //console.log();
        if(data.success){
            //alert(data.msg);
            //go to callbackurlshit  urlencode(base64_encode('http://stackoverflow.com/questions/9585034'))
            //console.log('{{base_url("payment/successpage_redirect")}}/'+data.reference+'/success/card/{{urlencode(base64_encode($post_data["CallbackURL"]))}}');
            //alert('{{base64_encode($post_data["CallbackURL"])}}');
            // window.location.href='{{$post_data["CallbackURL"]}}/reference='+data.reference;
            // window.location.assign("https://www.example.com");
            //window.location.href='{{base_url("payment/successpage_redirect")}}/'+data.reference+'/success/card/{{urlencode(base64_encode($post_data["CallbackURL"]))}}';
            //window.location.href='{{$post_data["CallbackURL"]}}'+data.reference+'/success/card';
            // window.location.href=data.url;
            window.location.replace(data.url);
        } else {
            $("#payment").prop("disabled",false);
            $("#payment").text("Process Payment");
            loader2.hidePleaseWait();
            swal(data.msg);
              $(".cancel_section").show();
            $(".payment_btn_section").hide();
            $("#tid").val('');
        }
      },
      error: function(xhr, textStatus, errorThrown) {
            $("#payment").prop("disabled",false);
            $("#payment").text("Process Payment");
            swal(data.msg);
              $(".cancel_section").show();
            $(".payment_btn_section").hide();
            $("#tid").val('');
      }
    });


});
$("#cancel").click(function(e){
    $.ajax({
      url: '{{base_url("payment/cancel_order")}}',
      type: 'POST',
      dataType: 'json',
      data: {reference_id: '{{$post_data["OrderReference"]}}'},
      complete: function(xhr, textStatus) {
        //called when complete
      },
      success: function(data, textStatus, xhr) {
           if(data.success){
               //history.back();
               //console.log(data.url);return false;
               console.log(data);
               swal("Order has been cancelled.");
               //alert(data.url+""+data.params+"/"+'{{urlencode(base64_encode($post_data["CallbackURL"]))}}');
               	window.location.href=data.url+""+data.params+"/"+'{{base64_encode(urlencode($post_data["CallbackURL"]))}}';
           } else {
               swal(data.msg);
           }
      },
      error: function(data, textStatus, errorThrown) {
        //called when there is an error
        swal("Something went wrong.");
        $(".cancel_section").show();
        $(".payment_btn_section").hide()    	    
      }
    });
    
});



$("#cancel_payment").click(function(e){
    $.ajax({
      url: '{{base_url("payment/cancel_order")}}',
      type: 'POST',
      dataType: 'json',
      data: {reference_id: '{{$post_data["OrderReference"]}}'},
      complete: function(xhr, textStatus) {
        //called when complete
      },
      success: function(data, textStatus, xhr) {
           if(data.success){
               //history.back();data.msg
               swal("Order has been cancelled.");
              window.location.href=data.url+""+data.params+"/"+'{{base64_encode(urlencode($post_data["CallbackURL"]))}}';
           } else {
               swal(data.msg);
           }
      },
      error: function(data, textStatus, errorThrown) {
        //called when there is an error
        swal("Something went wrong.");
        
      }
    });
    
});    

$("#another_card").click(function(){
    $("#success_section").hide();
    $("#card_info").show();
    $(".cancel_section").hide();
    $(".payment_btn_section").show();        	
});

$("#chkEmail").click(function(){
  if($(this).is(":checked")){
    $("#email").addClass('required_cash_checkout');
    $("#email").prop('readonly',false);
  } else {
    $(this).val("0");
    $("#email").removeClass('required_cash_checkout');
    $("#email").prop('readonly',true);
  }
});
$("#chkSMS").click(function(){
  if($(this).is(":checked")){
    $("#mobile_cash").addClass('required_cash_checkout');
    $("#mobile_cash").prop('readonly',false);
  } else {
    $(this).val("0");
    $("#mobile_cash").removeClass('required_cash_checkout');
    $("#mobile_cash").prop('readonly',true);
  }
});


$("#cashpayment_form").on('submit',function(e){


    // swal({
    //   title: 'Please confirm payment details.',
    //   // text: "",
    //   type: 'warning',
    //   showCancelButton: true,
    //   confirmButtonColor: '#3085d6',
    //   cancelButtonColor: '#d33',
    //   confirmButtonText: 'Yes!'
    // }).then((result) => {
    //   if (result.value) {
        
    //   }
    // });

        e.preventDefault();
          $('.hb').remove();
          $('.form-div').removeClass('has-error').removeClass('has-success');

          //check validation
          var err_count = 0;
          var to_req=[];
          //jquery blank validation..
          $(".required_cash_checkout").each(function(){
              var field_id = $(this).attr("id");
              var data=[];
              if($(this).val()==""){
                data['id']=field_id;      
                to_req.push(data);
                err_count++;
              }
          });

          /*if($("#password").val()!=$("#cpassword").val()){
            swal("Youre password and confirmation password do not match.");
            return false;
          }*/


          if(err_count>0){
            swal(
            'Opps...',
            "Please do check required fields.",
            'error'
            );  
/*		    $.each(to_req, function( index, value ) {
              $("#"+value.id).parent().after('<div class="hb"><div class="help-block" style="color:#f38989; font-size:7pt;margin-bottom:0px !important;">Required filed.</div></div>').addClass('has-error');
            });   */     
            return false;
          }
              var is_email =0;
              var is_sms=0;

              if($("#chkSMS").is(":checked")){
                   //alert("y");
                  is_sms=1;

              }
              if($("#chkEmail").is(":checked")){
                    //alert("y");
                  is_email=1;
                 
              }
            var form_data = {
                //notes:$("#notes").val(),
                //reference_num:$("#reference_num").val(),
                //amount:$("#amount").val(),
                email:$("#email").val(),
                mobile:$("#mobile_cash").val(),
                firstName:$("#firstName").val(),
                lastName:$("#lastName").val(),
                is_email:is_email,
                is_sms:is_sms,
                m:'{{$post_data["MerchantKey"]}}',
                r:'{{$post_data["reference_id"]}}',
                //amount_val:$("#amount_total").val(),
                //hid_fee:$("#hid_fee").val(),
                //hid_vat:$("#hid_vat").val(),
                //balance:$("#balance").val()	
            };
            $.ajax({
              url: "<?=site_url("payment/process_cash_checkout")?>",
              type: 'POST',
              dataType: 'json',
              data: form_data,
              beforeSend:function(xhr, textStatus) {
                //called when complete
                $("#process_payment_cash").prop('disabled', true);
              },
              complete: function(xhr, textStatus) {
                //called when complete
                 $("#process_payment_cash").prop('disabled', false);
              },
              success: function(data) {
                if(data.success){
                    //alert(data.msg);
                    // window.location.href=data.url;
                      window.location.replace(data.url);
/*		                var str="<div class='alert alert-dismissible alert-success'><strong>Thank you for your payment ! Please do copy the payment code to present to the nearest suncash store. </strong>.</div>";
                    $("#msg").html(str);
                    $("#success_section").show();
                    $("#cash_section").hide();

                       $("#transaction_code").text(data.msg);
                       $("#notes_value").text($("#notes").val());
                       $("#ref_value").text($("#reference_num").val());
                    $("#amount_value").text($("#amount").val());
                    $("#email").text($("#email").val());
                       $("#mobile").text($("#mobile").val());
                    $("#fname").text($("#fname").val());
                    $("#lname").text($("#lname").val());
                       // $("#transaction_section").show();
                       $(".transaction_details").show();*/
                       
                    //otable.ajax.reload();
                    //$("#transfer_modal").modal('hide');
                     $("#process_payment_cash").prop('disabled', false);
                } else {
                    swal(data.msg);
/*		                var str="<div class='alert alert-dismissible alert-danger'><strong> "+data.msg+" </strong>.</div>";
                    $("#msg").html(str);
                    $("#cash_section").hide();
                    $("#success_section").show();
                    $("#transaction_details").hide();*/

                       // $("#transaction_code").val('');
                       // $("#transaction_section").hide();
       
                       
                     $("#process_payment_cash").prop('disabled', false);		
                }
              },
              error: function(data, textStatus, errorThrown) {
                //called when there is an error
                       alert(data.msg);
                 $("#process_payment_cash").prop('disabled', false);
              }
            });

    return false;




    });
//show creditcard section and fee
$(document).on('shown.bs.collapse','#collapseTwo', function (e) {

    $.ajax({
      url: '{{base_url("payment/get_fees")}}',
      type: 'POST',
      dataType: 'json',
      data:{
          client_id:'{{$post_data["merchant_client_id"]}}',
          amount:$("#aval").val(),
          promo_code:$("#promo_code").val(),
          discount:$("#dval").val(),
          r:'{{$post_data["reference_id"]}}',
          },
        beforeSend: function(xhr, textStatus) {
            //called when complete
            //loader.showPleaseWait();    
        },          
        complete: function(xhr, textStatus) {
            //called when complete
            //loader.hidePleaseWait();      
        },
          success: function(data, textStatus, xhr) {
        //called when successful
        //paste values
        if(data.success){
            //alert(data.session);
            $("#fee_amount").text(data.fee);
            $("#amount").text(data.total);
            $("#aval").val(data.total);
            $("#pf").val(data.pf);
            $("#tf").val(data.tf);
            //loader.hidePleaseWait(); 
        } else {
            //loader.hidePleaseWait(); 
        }
        
      },
      error: function(xhr, textStatus, errorThrown) {
        //called when there is an errordiscount_amount
        //loader.hidePleaseWait(); 
      }
    });
            
});
//hide creditcard section and fee
$(document).on('hidden.bs.collapse','#collapseTwo', function (e) {

  var old_total = parseFloat($("#aval").val())-parseFloat($("#fee_amount").text());
  old_total = old_total.toFixed(2);
  $.ajax({
    url: '{{base_url("payment/restorefee")}}',
    type: 'POST',
    dataType: 'json',
    data: {
        total: old_total,
      promo_code:$("#promo_code").val(),
        discount:$("#dval").val(),
        r:'{{$post_data["reference_id"]}}',	  	
    },
      beforeSend: function(xhr, textStatus) {
          //called when complete
          //loader.showPleaseWait();    
      },          
      complete: function(xhr, textStatus) {
          //called when complete
          //loader.hidePleaseWait();      
      },
    success: function(data, textStatus, xhr) {
          $("#fee_amount").text(0.00);
          $("#amount").text(old_total);
          $("#aval").val(old_total);
    },
    error: function(xhr, textStatus, errorThrown) {
      //called when there is an error
    }
  });
            
});

//show suncash card section and fee
$(document).on('shown.bs.collapse','#collapseOne', function (e) {

var old_total = parseFloat($("#aval").val())-parseFloat($("#fee_amount").text());
old_total = old_total.toFixed(2);
$.ajax({
  url: '{{base_url("payment/restorefee")}}',
  type: 'POST',
  dataType: 'json',
  data: {
      total: old_total,
    promo_code:$("#promo_code").val(),
      discount:$("#dval").val(),
      r:'{{$post_data["reference_id"]}}',	  	
  },
    beforeSend: function(xhr, textStatus) {
        //called when complete
        //loader.showPleaseWait();    
    },          
    complete: function(xhr, textStatus) {
        //called when complete
        //loader.hidePleaseWait();      
    },
  success: function(data, textStatus, xhr) {
        $("#fee_amount").text(0.00);
        $("#amount").text(old_total);
        $("#aval").val(old_total);
  },
  error: function(xhr, textStatus, errorThrown) {
    //called when there is an error
  }
});
            
});
//hide suncash card section and fee
$(document).on('hidden.bs.collapse','#collapseOne', function (e) {

var old_total = parseFloat($("#aval").val())-parseFloat($("#fee_amount").text());
old_total = old_total.toFixed(2);
$.ajax({
  url: '{{base_url("payment/restorefee")}}',
  type: 'POST',
  dataType: 'json',
  data: {
      total: old_total,
    promo_code:$("#promo_code").val(),
      discount:$("#dval").val(),
      r:'{{$post_data["reference_id"]}}',	  	
  },
    beforeSend: function(xhr, textStatus) {
        //called when complete
        //loader.showPleaseWait();    
    },          
    complete: function(xhr, textStatus) {
        //called when complete
        //loader.hidePleaseWait();      
    },
  success: function(data, textStatus, xhr) {
        $("#fee_amount").text(0.00);
        $("#amount").text(old_total);
        $("#aval").val(old_total);
  },
  error: function(xhr, textStatus, errorThrown) {
    //called when there is an error
  }
});
            
});
//show cash card section and total amount
$(document).on('shown.bs.collapse','#collapseThree', function (e) {
$(".amount_cash").text($("#aval").val());
$(".amount_total_cash").text($("#aval").val());
/*    var old_total = parseFloat($("#aval").val())-parseFloat($("#fee_amount").text());
old_total = old_total.toFixed(2);
$.ajax({
  url: '{{base_url("payment/restorefee")}}',
  type: 'POST',
  dataType: 'json',
  data: {
      total: old_total,
    promo_code:$("#promo_code").val(),
      discount:$("#dval").val(),
      r:'{{$post_data["reference_id"]}}',	  	
  },
    beforeSend: function(xhr, textStatus) {
        //called when complete
        //loader.showPleaseWait();    
    },          
    complete: function(xhr, textStatus) {
        //called when complete
        //loader.hidePleaseWait();      
    },
  success: function(data, textStatus, xhr) {
        $("#fee_amount").text(0.00);
        $("#amount").text(old_total);
        $("#aval").val(old_total);
  },
  error: function(xhr, textStatus, errorThrown) {
    //called when there is an error
  }
});*/
            
});

/*		$("#cc").click(function(e){

        //e.stopPropagation();
        //alert("x"); 
        var hasopen=$('#cc').hasClass('collapsed');
        //alert(hasopen);
        if(hasopen) {
            e.preventDefault();
            //e.stopImmediatePropagation();				
            // accordion is open
            $.ajax({
              url: '{{base_url("payment/get_fees")}}',
              type: 'POST',
              dataType: 'json',
              data:{client_id:'{{$post_data["merchant_client_id"]}}'},
                beforeSend: function(xhr, textStatus) {
                    //called when complete
                    //loader.showPleaseWait();    
                },          
                complete: function(xhr, textStatus) {
                    //called when complete
                    //loader.hidePleaseWait();      
                },
              success: function(data, textStatus, xhr) {
                //called when successful
                //paste values
                if(data.success){
                    $("#fee_amount").text(data.fee);
                    $("#amount").text(data.total);
                } 
                
              },
              error: function(xhr, textStatus, errorThrown) {
                //called when there is an errordiscount_amount
              }
            });
            
            
        } else {
            e.preventDefault();
            // accordion is closed
            var old_total = parseFloat($("#amount").text())-parseFloat($("#fee_amount").text());
            old_total = old_total.toFixed(2);
            $.ajax({
              url: '{{base_url("payment/restorefee")}}',
              type: 'POST',
              dataType: 'json',
              data: {total: old_total},
                beforeSend: function(xhr, textStatus) {
                    //called when complete
                    //loader.showPleaseWait();    
                },          
                complete: function(xhr, textStatus) {
                    //called when complete
                   // loader.hidePleaseWait();      
                },
              success: function(data, textStatus, xhr) {
                    $("#fee_amount").text(0.00);
                    $("#amount").text(old_total);
              },
              error: function(xhr, textStatus, errorThrown) {
                //called when there is an error
              }
            });
                            
            
        }
    });

    $("#suncash_card").click(function(e){
        //e.preventDefault();
        // accordion is closed
            e.preventDefault();
            // accordion is closed
            var old_total = parseFloat($("#amount").text())-parseFloat($("#fee_amount").text());
            old_total = old_total.toFixed(2);
            $.ajax({
              url: '{{base_url("payment/restorefee")}}',
              type: 'POST',
              dataType: 'json',
              data: {total: old_total},
                beforeSend: function(xhr, textStatus) {
                    //called when complete
                    //loader.showPleaseWait();    
                },          
                complete: function(xhr, textStatus) {
                    //called when complete
                    //loader.hidePleaseWait();      
                },
              success: function(data, textStatus, xhr) {
                    $("#fee_amount").text(0.00);
                    $("#amount").text(old_total);
              },
              error: function(xhr, textStatus, errorThrown) {
                //called when there is an error
              }
            });
    });*/

    document.addEventListener( 'dblclick', function(event) {  
        //alert("Double-click disabled!");  
        event.preventDefault();  
        event.stopPropagation(); 
      },  true //capturing phase!!
    );
// promotion
$("#get_promo").click(function(e){
/*       var promo_code = $("#promo_code").val();
   var event_id = '{{$events_data->id}}';
   var ticket_ids = '';
    $(".ticket_items").each(function(){
        formData.append('price_set['+ctr+']', $(this).val());
        ctr++;
        if($(this).is(":checked")){
            formData.append('ticket_'+$(this).val(), $(this).val());
        }
    });*/
   var promo_code = $("#promo_code").val();
   if(promo_code==''){
     swal("Please put a promo code.");
     return false;
   }

   $.ajax({
    url: '{{base_url("payment/get_promo")}}',
    type: 'POST',
    dataType: 'json',
    data: {
         promo_code:$("#promo_code").val(),
         r:'{{$post_data["reference_id"]}}',
         o:'{{$post_data["OrderID"]}}',
         m:'{{$post_data["MerchantKey"]}}',
         p:$("#pf").val(),
         t:$("#tf").val(),
    },
    beforeSend: function(xhr, textStatus) {
        //called when complete
        //loader.showPleaseWait();    
    },          
    complete: function(xhr, textStatus) {
        //called when complete
        //loader.hidePleaseWait();      
    },
    success: function(data, textStatus, xhr) {

        

       //called when successful
       if(data.success){
        //process promotion.       
        //compute..
        var gt=parseFloat($("#subtotal").text())-parseFloat(data.discount);
        if(gt<=0){
          gt =0.00;
        }
        //alert(gt);
        gt=gt+parseFloat($("#sunpass_fee").text())+parseFloat($("#facility_fee").text());//+parseFloat($("#fee_amount").text());
        gt=gt.toFixed(2);
        $("#promo_code").prop('readonly',true);
        //$("#hid_total").val(gt);
        $("#total").text(gt);
        $("#aval").val(gt);
        
        $("#get_promo").prop('disabled',true).hide();
       // $("#discount_lbl").text(data.discount);
        $("#discount_amount").text(data.discount);  
        $("#dval").val(data.discount); 
         $("#amount").text(gt);

         if($("#pf").val()>0 && $("#tf").val()>0){
         //var x=parseFloat($("#subtotal").text())-parseFloat(data.discount);
        $.ajax({
          url: '{{base_url("payment/get_fees")}}',
          type: 'POST',
          dataType: 'json',
          data:{
              client_id:'{{$post_data["merchant_client_id"]}}',
              amount:gt,
              promo_code:$("#promo_code").val(),
              discount:$("#dval").val(),
              r:'{{$post_data["reference_id"]}}',
              },
            beforeSend: function(xhr, textStatus) {
                //called when complete
                //loader.showPleaseWait();    
            },          
            complete: function(xhr, textStatus) {
                //called when complete
                //loader.hidePleaseWait();      
            },
              success: function(data, textStatus, xhr) {
            //called when successful
            //paste values
            if(data.success){
                //alert(data.session);
                $("#fee_amount").text(data.fee);
                $("#amount").text(data.total);
                $("#aval").val(data.total);
                $("#pf").val(data.pf);
                $("#tf").val(data.tf);
                //loader.hidePleaseWait(); 
            } else {
                //loader.hidePleaseWait(); 
            }
            
          },
          error: function(xhr, textStatus, errorThrown) {
            //called when there is an errordiscount_amount
            // swal(data.msg);
            //loader.hidePleaseWait(); 
          }
        });
        }


       } else {
        swal(
        'Oops...',
        data.msg,
        'error'
        );             
       }
       
    },
    error: function(xhr, textStatus, errorThrown) {
       //called when there is an error
       swal("Something went wrong1.");
    }
   });
   

});

$("#refresh").click(function(){
    location.reload(true);
});

//   $("#whitelist_form").submit(function(e) {
//   	/* Act on the event */
    // e.preventDefault();

    // //reset validation
    // $('.hb').remove();
    // $('.required').removeClass('has-error-border').removeClass('has-success');

    // //check validation
    // var err_count = 0;
    // var to_req=[];
    // //jquery blank validation..
    // $("#whitelist_form .required").each(function(){
    //     var field_id = $(this).attr("id");
    //     var data=[];
    //     if($(this).val()==""){
    //       data['id']=field_id;      
    //       to_req.push(data);
    //       err_count++;
    //     }
    // });

    // if(err_count>0){
    //   swal(
    //   'Oops...',
    //   "Please do check required fields.",
    //   'error'
    //   );  
    //   $.each(to_req, function( index, value ) {
    //     $("#"+value.id).addClass('has-error-border');//.after('<div class="hb"><div class="help-block" style="color:#f38989; font-size:7pt;margin-bottom:0px !important;">Required filed.</div></div>')
    //   });        
    //   return false;
    // }    
    // //
    // var url = '{{base_url("payment/update_whitelist")}}';
    // var formData = new FormData($("#whitelist_form")[0]);
    // formData.append('card_id_upload', $('#card_id_upload')[0].files[0]); 
    // formData.append('credit_card_upload', $('#credit_card_upload')[0].files[0]); 
    // formData.append('cc_with_card_upload', $('#cc_with_card_upload')[0].files[0]); 
    // var button = $("#submit_btn");

    // //@global .js
    // //P1 form obj P2 url form process to.
    // ProcessForm(formData,url,button);
    // $("#modal_validate_card").modal('hide');



//   	return false;
//   });

//for modal validation of card
$('.btn-process').on('click', function() {
//alert("safari af");
$(this).html('Submit');
$progressContentDone = $('.progress-content.active').data('content');
//alert($progressContentDone);
if($progressContentDone==1){
    if($("#card_id").val()=='' || $("#card_email").val()=='' || $("#card_mobile_number").val()=='' ){
        swal('Fill up all required field/s.');
        return false;
    }
}
if(!isEmail($("#card_email").val())){
        swal('Invalid Email format.');
        return false;		
}


if($progressContentDone==2){
    console.log('if');
    $('.btn-gray').css({"opacity": "1"});    	
    $('.hb').remove();
    $('.required').removeClass('has-error-border').removeClass('has-success');

    //check validation
    var err_count = 0;
    var to_req=[];
    //jquery blank validation..
    $("#whitelist_form .required").each(function(){
        var field_id = $(this).attr("id");
        var data=[];
        if($(this).val()==""){
          data['id']=field_id;      
          to_req.push(data);
          err_count++;
        }
    });

    if(err_count>0){
      swal(
      'Oops...',
      "Please do check required fields.",
      'error'
      );  
      $.each(to_req, function( index, value ) {
        $("#"+value.id).addClass('has-error-border');//.after('<div class="hb"><div class="help-block" style="color:#f38989; font-size:7pt;margin-bottom:0px !important;">Required filed.</div></div>')
      });        
      return false;
    }    
    //
    //var form = new FormData();

    if($("#card_id_upload").find('[name=filepond]').val()=='' || $("#credit_card_upload").find('[name=filepond]').val()=='' || $("#cc_with_card_upload").find('[name=filepond]').val()==''){
      swal(
      'Opps...',
      "All photo are required",
      'error'
      ); 
      return false;
    }



    var url = '{{base_url("payment/update_whitelist")}}';
    //var formData = new FormData($("#whitelist_form")[0]);
    //var formData = $("#whitelist_form").serializeArray();
    var file1 = JSON.parse($("#card_id_upload").find('[name=filepond]').val());
    //formData.append('card_id_upload', file1.data);
    var file2 = JSON.parse($("#credit_card_upload").find('[name=filepond]').val());
    //formData.append('credit_card_upload', file2.data);
    var file3 = JSON.parse($("#cc_with_card_upload").find('[name=filepond]').val());
    //formData.append('cc_with_card_upload', file3.data);	

/*		formData.push({name: 'card_id_upload', value: file1.data});
    formData.push({name: 'credit_card_upload', value: file2.data});
    formData.push({name: 'cc_with_card_upload', value: file3.data});*/

    //for (var i = 0; i < $('.my-pond input').length; i++) {

    //}
    var formData = {
        last4digits:$("#last4digits").val(),
        card_type_w:$("#card_type_w").val(),
        card_name:$("#card_name").val(),
        card_id:$("#card_id").val(),
        card_email:$("#card_email").val(),
        card_mobile_number:$("#card_email").val(),
        card_id_upload:file1.data,
        credit_card_upload:file2.data,
        cc_with_card_upload:file3.data,
        wid:$("#wid").val(),
        merckey:$("#wid").val(),
    };



/*		formData.append('card_id_upload', $('#card_id_upload')[0].files[0]); 
    formData.append('credit_card_upload', $('#credit_card_upload')[0].files[0]); 
    formData.append('cc_with_card_upload', $('#cc_with_card_upload')[0].files[0]); */
    var button = $("#submit_btn");

    $.ajax({
      url: url,
      type: 'POST',
      dataType: 'json',
      data: formData,
      //contentType: false,       
      //cache: false,             
      //processData:false,       
      beforeSend: function(xhr, textStatus) {
        //called when complete
        //clearValidationArray();
        loader.showPleaseWait();
        $(button).prop('disabled',true);        
      },          
      complete: function(xhr, textStatus) {
        //called when complete
        loader.hidePleaseWait();
        $(button).prop('disabled',false);        
      },
      success: function(data) {
        //called when successful
        if(data.success){
            // swal(
            // '',
            // data.msg,
            // 'success'
            // );  

        $('.progress-step[data-step="' + $progressContentDone + '"]').addClass('done').removeClass('active');
        $('.progress-step.done .number').html('<i class="fas fa-check"></i>');
        $('.progress-step[data-step="' + $progressContentDone + '"]').next().addClass('active');

        $('.progress-content[data-content="'+$progressContentDone+'"]').addClass('done').removeClass('active');
        $('.progress-content[data-content="'+$progressContentDone+'"]').next().addClass('active');

            //hideModal("large");
          $('#cardValidation .btn-container').remove();
           $('.progress-content[data-content="3"]').addClass('done').removeClass('active');
           $('.progress-step[data-step="3"]').addClass('done').removeClass('active');
        $('.progress-step.done .number').html('<i class="fas fa-check"></i>');
        $('.progress-content[data-content="3"]').addClass('done');

        } else {
            swal(
            'Opps...',
            data.msg,
            'error'
            );  
        }     
      },
      error: function(xhr, textStatus, errorThrown) {
        swal('something went wrong.');
      }
    });

}
//return false;
if($progressContentDone!=2){
$('.progress-step[data-step="' + $progressContentDone + '"]').addClass('done').removeClass('active');
$('.progress-step.done .number').html('<i class="fas fa-check"></i>');
$('.progress-step[data-step="' + $progressContentDone + '"]').next().addClass('active');

$('.progress-content[data-content="'+$progressContentDone+'"]').addClass('done').removeClass('active');
$('.progress-content[data-content="'+$progressContentDone+'"]').next().addClass('active');
}

});


$('.my-pond').filepond();
$.fn.filepond.registerPlugin(FilePondPluginImagePreview);
$.fn.filepond.registerPlugin(FilePondPluginFileValidateType);
$.fn.filepond.registerPlugin(FilePondPluginFileEncode);
// Turn input element into a pond with configuration options
$('.my-pond').filepond({
  allowMultiple: true,
  // labelIdle: '<button type="button" class="btn btn-upload"><i class="fas fa-upload"></i> Upload</button>',
  acceptedFileTypes: [
    'image/jpg',
    'image/jpeg',
    'image/png',
    ]
});

// Set allowMultiple property to true
$('.my-pond').filepond('allowMultiple', false);

// Listen for addfile event
$('.my-pond').on('FilePond:addfile', function(e) {
  console.log('file added event', e);
});


$("#okay_btn").click(function(){
 window.location.reload();
});
$("#suncash_voucher_form").submit(function(){
//  alert("x");
var form_data ={
    voucher_mobile_number:$("#voucher_mobile_number").val(),
    voucher_number:$("#voucher_number").val(),
    voucher_pin:$("#voucher_pin").val(),
    m:'{{$post_data["MerchantKey"]}}',
    r:'{{$post_data["reference_id"]}}',
    }   
    $.ajax({
      url: '{{base_url("payment/process_voucher_payment")}}',
      type: 'POST',
      dataType: 'json',
      data: form_data,
      beforeSend:function(){
          loader2.showPleaseWait();
        $("#voucher_card_next").prop("disabled",true);
        $("#voucher_card_next").text("Processing... Please wait");
        //loader.showPleaseWait();
      },
      complete: function(xhr, textStatus) {
        $("#voucher_card_next").prop("disabled",false);
        $("#voucher_card_next").text("Process Payment");
        //loader.hidePleaseWait();
      },
      success: function(data, textStatus, xhr) {
        //console.log();
        if(data.success){
            window.location.href=data.url;
        } else {
            loader2.hidePleaseWait();
            $("#voucher_card_next").prop("disabled",false);
            $("#voucher_card_next").text("Process Payment");

            swal(data.msg);
        }
      },
      error: function(xhr, textStatus, errorThrown) {
           $("#voucher_card_next").prop("disabled",false);	   
           $("#voucher_card_next").text("Process Payment");

            loader2.hidePleaseWait();
            swal(data.msg);
      }
    });
return false;
});
$(document).on('click','#used_card',function(){
    $("#show_login_section").show();
    $("#card_info").hide();
});
$(document).on('click','#back_customer',function(){
    $("#show_login_section").hide();
    $("#card_info").show();
});
$(document).on('click','#back_to_customer',function(){
    $("#show_cards_section").hide();
    $("#card_info").show();
    $("#ccwu").html("");
});

$("#cl_form").submit(function(){
    $.ajax({
        type: "POST",
        url: '{{base_url("payment/lcc")}}',
        data: {
            'u':$("#customer_login_user").val(),
            'p':$("#customer_login_pass").val(),
        },
        dataType: "json",
        beforeSend: function (response) {
            loader.showPleaseWait();
            $("#login_customer").prop('disabled',true);  
        },
        complete: function (response) {
            $("#login_customer").prop('disabled',false);
            loader.hidePleaseWait();  
        },
        success: function (response) {
            $("#login_customer").prop('disabled',false);
            if(response.success){
                  $("#show_login_section").hide();
                  $("#show_cards_section").show();
                if(response.data!=''){
                  $("#ccwu").html(response.data);
                  $("#process_linked_card").prop('disabled',false);
                } else {
                  $("#ccwu").html('<option value="">No Data.</option>');
                  $("#process_linked_card").prop('disabled',true);
                }
            } else {
            $("#login_customer").prop('disabled',false);
            $("#show_login_section").show();
            $("#show_cards_section").hide();
            $("#ccwu").html("");
            loader.hidePleaseWait();
            swal(response.msg);
            }
        },
        error: function (response) {
            $("#login_customer").prop('disabled',false);
            $("#show_login_section").show();
            $("#show_cards_section").hide();
            $("#ccwu").html("");
            loader.hidePleaseWait();
            swal(response.msg);
        },
    });

    return false;
});
$("#process_linked_card_form").submit(function(){

    var form_data ={
    ccwu:$("#ccwu").val(),
    m:'{{$post_data["MerchantKey"]}}',
    r:'{{$post_data["reference_id"]}}',
    }   
    $.ajax({
        type: "POST",
        url: '{{base_url("payment/process_savecard_payment")}}',
        data: form_data,
        dataType: "json",
        beforeSend: function (response) {
            loader2.showPleaseWait();
            $("#process_linked_card").prop('disabled',true);  
        },
        complete: function (response) {
            $("#process_linked_card").prop('disabled',false);
            loader2.hidePleaseWait();  
        },
        success: function (response) {
            $("#process_linked_card").prop('disabled',false);
            if(response.success){
                window.location.href=response.url;
            } else {
            $("#process_linked_card").prop('disabled',false);
            // $("#show_login_section").show();
            // $("#show_cards_section").hide();
            // $("#ccwu").html("");
            loader2.hidePleaseWait();
            swal(response.msg);
            }
        },
        error: function (response) {
            // $("#process_linked_card").prop('disabled',false);
            // $("#show_login_section").show();
            // $("#show_cards_section").hide();
            // $("#ccwu").html("");
            loader2.hidePleaseWait();
            swal(response.msg);
        },
    });

    return false;
});
$("#savelink_card").click(function(){
    $("#show_login_link_section").show();
    $("#card_info").hide();
});
//  $(document).on('click','#link_card',function(){
// 	var attr_id =$('#link_card').attr('form_id');
// 	var other_data ={
// 	tokenid:$("#tid").val(),
// 	name_customer_card:$("#name_card").val(),
// 	email_card:$("#email_card").val(),
// 	mobile_card:$("#mobile_card").val(),
// 	card_type:$("#card_type").text(),
// 	card_number:$("#card_number").text(),
// 	name_on_card:$("#name_on_card").text(),
// 	m:'{{$post_data["MerchantKey"]}}',
// 	r:'{{$post_data["reference_id"]}}',
// 	}
// 	var url_path = '{{base_url("payment/login_page")}}';
// 	hideModal('large');
// 	/*p1 button , id , url , size: small,meduim,large,xlarge*/
// 	LoadModal($(this),attr_id,url_path,'medium',other_data);
// });

$("#sand_form").submit(function(e){
    $('.hb').remove();
    $('.form-div').removeClass('has-error').removeClass('has-success');

    //check validation
    var err_count = 0;
    var to_req=[];
    //jquery blank validation..
    $("#sand_form .required").each(function(){
        var field_id = $(this).attr("id");
        var data=[];
        if($(this).val()==""){
          data['id']=field_id;      
          to_req.push(data);
          err_count++;
        }
    });

    if(err_count>0){
      swal(
      'Oops...',
      "Please do check required fields.",
      'error'
      );  
      $.each(to_req, function( index, value ) {
        $("#"+value.id).parent().after('<div class="hb"><div class="help-block" style="color:#f38989; font-size:7pt;margin-bottom:0px !important;">Required filed.</div></div>').addClass('has-error');
      });        
      return false;
    }
    var form_data = $(this).serializeArray();
    $.ajax({
      url: '{{base_url("payment/sand_generate_passcode")}}',
      type: 'POST',
      dataType: 'json',
      data: form_data,
        beforeSend:function(xhr, textStatus) {
        //called when complete
        // loader.showPleaseWait();
        $("#sand_next").prop('disabled', true);
        $("#voucher_card_next").text("Processing... Please wait");
      },
        complete: function(xhr, textStatus) {
        //called when complete
        // loader.hidePleaseWait();
        $("#sand_next").prop('disabled', false);
        $("#voucher_card_next").text("Next");
      },
      success: function(data, textStatus, xhr) {
        //called when successful
        if(data.success){
            console.log(data.data);
            $(".sand_verify_section").show();
            $(".sand_login_section").hide();		    	
        } else {
            swal(data.msg);
            $("#sand_submit").text("Next");	
        }
      },
      error: function(data, textStatus, errorThrown) {
        //called when there is an error
        
      }
    });
        return false;
});
$("#sand_process_form").submit(function(e){
    $('.hb').remove();
    $('.form-div').removeClass('has-error').removeClass('has-success');

    //check validationhas-danger
    var err_count = 0;
    var to_req=[];
    //jquery blank validation..
    $("#sand_process_form .required").each(function(){
        var field_id = $(this).attr("id");
        var data=[];
        if($(this).val()==""){
          data['id']=field_id;      
          to_req.push(data);
          err_count++;
        }
    });

    if(err_count>0){
      swal(
      'Oops...',
      "Please do check required fields.",
      'error'
      );  
      $.each(to_req, function( index, value ) {
        $("#"+value.id).parent().after('<div class="hb"><div class="help-block" style="color:#f38989; font-size:7pt;margin-bottom:0px !important;">Required filed.</div></div>').addClass('has-error');
      });        
      return false;
    }
    var form_data = {
        otp:$("#sand_otp").val(),
        passcode:$("#sand_pin").val(),
        mobile:$("#sand_mobile").val(),
        fullname:$("#sand_fullname").val(),
        sand_qr:$("#sand_qr").val(),
        email:$("#sand_email").val(),
        m:'{{$post_data["MerchantKey"]}}',
        r:'{{$post_data["reference_id"]}}',			
    };
    $.ajax({
      url: '{{base_url("payment/process_sanddollar_checkout")}}',
      type: 'POST',
      dataType: 'json',
      data: form_data,
        beforeSend:function(xhr, textStatus) {
        //called when complete
        // loader.showPleaseWait();
        $("#sand_process_payment").prop('disabled', true);
        $("#sand_process_payment").text("processing... please wait");
      },
        complete: function(xhr, textStatus) {
        //called when complete
       $("#sand_process_payment").prop('disabled', false);
       $("#sand_process_payment").text("Process Payment");
      },
      success: function(data, textStatus, xhr) {
        //called when successful
        if(data.success){

            window.location.href=data.url;
            
        } else {
            // loader.hidePleaseWait();
            $(".sand_login_section").show();
            // $(".sand_verify_section").hide();
            $("#sand_process_payment").prop('disabled', false);
            $("#sand_process_payment").text("Process Payment");		    	
           	swal(data.msg.type);
        }
      },
      error: function(data, textStatus, errorThrown) {
        //called when there is an error
        // loader.hidePleaseWait();
        $("#sand_login_section").show();
        // $(".sand_verify_section").hide();
         $("#sand_process_payment").prop('disabled', false);
         $("#sand_process_payment").text("Process Payment");			    
        swal(data.msg);
      }
    });
    



    return false;
});


</script>