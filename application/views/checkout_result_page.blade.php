<?php
//loading assets helper
defined('BASEPATH') OR exit('No direct script access allowed');
$assetHelper = new AssetHelper\AssetHelper();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="{{base_url('assets_main/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/main_checkout.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/main.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/responsive.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/all.min.css')}}">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>assets/plugins/icon.ico">
</head>
<body>
	<nav class="navbar navbar-light bg-light">
	  <a class="navbar-brand" href="#">
	    <img src="{{base_url('assets_main/imgs/suncash-logo.png')}}">
	  </a>
	</nav>

	<section class="full" id="success_section">
		<div class="container  text-center">
			<div class="row d-flex justify-content-center align-items-center mb-5">
				<div class="user-card message-card reload-card">
					<div class=" w-content"></div>
					<div class="">

						<input class="input-amount" type="text" id="amount" name="amount" value="{{$post_data['Amount']}}" style="height: 100% !important" readonly>
						<div class="container">
							<div class="item primary-border ">
								<div class="item" style="margin-bottom: 0px !important;margin-top: 0px !important;">
									<div class="row text-details">
										<div class="col text-left">Date:</div>
										<div class="col text-right ">{{$post_data['created_date']}}</div>
									</div> 
								</div> 
								<div class="item" style="margin-bottom: 0px !important;margin-top: 0px !important;">
									<div class="row text-details">
										<div class="col text-left">Status:</div>
										<div class="col text-right ">{{$post_data['status']}}</div>
									</div> 
								</div> 
								<div class="item" style="margin-bottom: 0px !important;margin-top: 0px !important;">
									<div class="row text-details">
										<div class="col text-left">Category:</div>
										<div class="col text-right ">{{$post_data['payment_method']}}</div>
									</div> 
								</div> 
								<!-- <div class="item" style="margin-bottom: 0px !important;margin-top: 0px !important;">
									<div class="row text-details">
										<div class="col text-left">Total:</div>
										<div class="col text-right ">{{$post_data['Total']}}</div>
									</div> 
								</div>  -->
								<div class="item" style="margin-bottom: 0px !important;margin-top: 0px !important;">
									<div class="row text-details">
										<div class="col text-left">Name:</div>
										<div class="col text-right ">{{$post_data['customer_name']}}</div>
									</div> 
								</div>								
								<div class="item" style="margin-bottom: 0px !important;margin-top: 0px !important;">
									<div class="row text-details">
										<div class="col text-left">Mobile Number:</div>
										<div class="col text-right ">{{$post_data['customer_mobile']}}</div>
									</div> 
								</div> 
								<div class="item" style="margin-bottom: 0px !important;margin-top: 0px !important;">
									<div class="row text-details">
										<div class="col text-left">Email Address:</div>
										<div class="col text-right ">{{$post_data['customer_email']}}</div>
									</div> 
								</div>								
								<div class="item" style="margin-bottom: 0px !important;margin-top: 0px !important;">
									<div class="row text-details">
										<div class="col text-left">Transaction ID:</div>
										<div class="col text-right "><b>{{$post_data['reference_id']}}</b></div>
									</div> 
								</div> 
							</div>
						</div>		
					</div>
				</div>
			</div>
		</div>
	</section>

	<script src="{{base_url('assets_main/js/jquery-3.2.1.min.js')}}"></script>
	<script src="{{base_url('assets_main/js/bootstrap.min.js')}}"></script>
	<script src="{{base_url('assets_main/js/main.js')}}"></script>
    <script src="{{base_url('assets_main/js/select.min.js')}}"></script>
	<script  type="text/javascript" charset="utf-8">

</script>
</body>
</html>