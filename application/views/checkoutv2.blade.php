<?php
//loading assets helper
defined('BASEPATH') OR exit('No direct script access allowed');
$assetHelper = new AssetHelper\AssetHelper();
$ci= &get_instance();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Checkout</title>
	<link rel="stylesheet" href="{{base_url('assets_main/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/main_checkout.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/all.min.css')}}">
	<link rel="stylesheet" href="{{base_url('assets/plugins/global/global2.css')}}">
	<link href="https://unpkg.com/filepond/dist/filepond.css" rel="stylesheet">
	<link href="https://unpkg.com/filepond/dist/filepond.min.css" rel="stylesheet">
	<link href="https://unpkg.com/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.min.css" rel="stylesheet">	
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>assets/plugins/icon.ico">

	<style type="text/css" >
		.ïframecenpos{
			border:0px !important;
		}
		.layaoutPanelTokenCardForm .canvasbox {
		    margin-left: 10% !important;
		    margin-right:10% !important;
		    width: 100% !important;			
		}
		.Modern .canvasbox .row > span {
			/*webkit-font-smoothing: antialiased !important;*/
			webkit-font-smoothing: antialiased !important;
		    font-family: "Roboto", sans-serif !important;
		    font-weight: 400 !important;
		    font-size: 15px !important;
		    color: #292929 !important;
		    line-height: 1.5em !important;
		    margin-bottom: .5rem !important;
		}

		.Modern .canvasbox .row > input {
			/*webkit-font-smoothing: antialiased !important;*/
			webkit-font-smoothing: antialiased !important;
		    font-family: "Roboto", sans-serif !important;
		    font-weight: 400 !important;
		    font-size: 15px !important;
		    color: #292929 !important;
		    line-height: 1.5em !important;
		    margin-bottom: .5rem !important;
		}
		.has-error-border{
			border-color:red !important ;
		}
	
	</style>
</head>
<body>
	<!-- <nav class="navbar navbar-light bg-light">
	  <a class="navbar-brand" href="#">
	    <img src="{{base_url('assets_main/imgs/suncash-logo.png')}}">
	  </a>
	</nav>  -->
	<main class="v4">
		<div class="container">
			<div class="row">
				<div class="col-lg-4" style="display:none;">
					<div class="order-summary">
						<div class="header">
							<div class="shop-logo">
								@if(!empty($post_data))
									<img src="{{$post_data['profile_pic']}}">
								@else
									<img src="{{base_url('assets_main/imgs/shop-logo.png')}}">
								@endif
							</div>
							<div class="shop-name  align-self-center">
								<div>
									<h2>{{$post_data['MerchantName']}}</h2>
									<span>Order Summary</span>
								</div>
							</div>
						</div>
						<div class="table-responsive mb-4">
							<table class="table table-borderless">
								<thead>
							    <tr>
							      <th scope="col">Description</th>
							      <th scope="col" class="text-right">Amount</th>
							    </tr>
							  </thead>
							  <tbody>
							  	@if(!empty($post_data))
							  		@if(!empty($post_data['ItemName']))
							  			<?php 
							  			$count = count($post_data['ItemName']); 
							  			$qty_total = 0 ;
							  			?>
							  			@for($i=0;$i<$count; $i++)
										    <tr>
										      <td>
										      	<span class="item-name">{{$post_data['ItemName'][$i]}}</span>
										      	<span class="item-number"></span>
										      	<span class="quantity">Quantity: {{$post_data['ItemQty'][$i]}}</span>
										      </td>
										      <td class="text-right amount">$  {{$post_data['ItemPrice'][$i]}}</td>
										    </tr>	
										    <?php $qty_total+=$post_data['ItemQty'][$i]++; ?>						  			
							  			@endfor
							    	@else

							    	@endif
							  	@else

							  	@endif
							    <tr class="item-total">
							      <td class="f-medium">Item Total</td><!-- -->
							      <td class="text-right t_qty">{{!empty($post_data) ? $qty_total : 0.00 }}</td>
							    </tr>
							    <tr class="item-total">
							      <td class="f-medium">Subtotal</td><!-- -->
							      <td class="text-right ">$ <b id="subtotal">{{!empty($post_data) ? number_format($subtotal,2,'.','') : 0.00 }}</b></td>
							    </tr>							    
							    @if(in_array($post_data['MerchantKey'],SUNPASS_MERCHANTS_KEYS))
							  	<tr class="item-total">
							    	<td class="f-medium">HAVE PROMO CODE?</td>
							    	<td class="text-right">
								    	<div class="input-group mb-2">
										  <input type="text" class="form-control" aria-describedby="basic-addon2" id="promo_code" name="promo_code" style="border-color: orange;">
										  <div class="input-group-append">
										    <span class="input-group-text" style="padding: 0px !important">
										    	<button class="btn btn-primary" id="get_promo"><i class="fa fa-paper-plane"></i></button>
										    </span>
										  </div>
										</div>	
							    	</td>
								</tr>
							    <tr class="item-total">
							      <td class="f-medium">Discount</td>
							      <td class="text-right ">$ 
							      	<b id="discount_amount">0.00</b>
							      	
							      </td>
							    </tr>	

							    <tr class="item-total">
							      <td class="f-medium">Sunpass Fee</td><!-- -->
							      <td class="text-right ">$ <b id="sunpass_fee">{{number_format(str_replace(',', '', $sunpass_fee),2,'.','')}}</b></td>
							    </tr>
							    <tr class="item-total">
							      <td class="f-medium">Facility Fee</td><!-- -->
							      <td class="text-right ">$ <b id="facility_fee">{{number_format($facility_fee,2,'.','')}}</b></td>
							    </tr>
							
							    @endif	
							    						    
							    <tr class="item-total">
							      <td class="f-medium">Convenience Fee</td><!-- -->
							      <td class="text-right ">$ <b id="fee_amount">0.00</b></td>
							    </tr>
							    
							    <tr class="total">
							      <?php
							      //catch
							      $recompute_default=$post_data['Amount']+$sunpass_fee+$facility_fee;
							      if($recompute_default!=$post_data['total_amount']){
							      	$post_data['total_amount']=$recompute_default;
							      }
							      ?>
							      <td class="f-medium text-right">Total</td>
							      <td class="text-right ">$ <b id="amount" style="font-size:13pt;">{{!empty($post_data) ? number_format($post_data['total_amount'],'2','.','') : 0.00 }}</b>
							      </td>
							    </tr>
							  </tbody>
							</table>
						</div>
						<div class="text-center">
							<input type="hidden" id="dval" value="0.00" />
							<input type="hidden" id="aval" value="{{!empty($post_data) ? number_format($post_data['total_amount'],2,'.','')  : 0.00 }}" />
							<input type="hidden" id="pf" value="0.00" />
							<input type="hidden" id="tf" value="0.00" />
							<button class="btn btn-primary" id="cancel"><i class="fa fa-times"> Cancel Order</i></button>
							<button class="btn btn-primary" id="refresh"><i class="fa fa-refresh" aria-hidden="true">	Refresh</i></button>
						</div>
						<div class="details row align-items-center">
							<div class="col">
								<img src="{{base_url('assets_main/imgs/suncash-logo.png')}}">
								<span>Protection</span>
							</div>
							<div class="col-3">
								<a href="">Details</a>
							</div>
						</div>
						<p class="text-center">Shop Around the World with Confidence!</p>
					</div>
                </div>

			@if(!empty($selected_settings_data['system_services_ids']))
				<?php
					$_1='none';//suncash account
					$_2='none';//credit card/debit card
					$_3='none';//cash payment
					$_4='none';//voucher payment
					$_5='none';//sanddollar payment
					$_6='none';//amazon payment
					$_7='none';//paypal payment
					//dd($post_data['payment_method']);
					$set_option=explode(',',$selected_settings_data['system_services_ids']);
					foreach($set_option as $value) {
						if(!empty($post_data['payment_method'])){
							//dd("x");
							if($post_data['payment_method']=='1'){//&& in_array($post_data['payment_method'],$set_option)
								$_1="block";
							}
							if($post_data['payment_method']=='2'){//&& in_array($post_data['payment_method'],$set_option)
								$_2="block";
							}
							if($post_data['payment_method']=='3'){//&& in_array($post_data['payment_method'],$set_option)
								$_3="block";
							}	
							if($post_data['payment_method']=='4'){//&& in_array($post_data['payment_method'],$set_option)
								$_4="block";
							}
							if($post_data['payment_method']=='5'){//&& in_array($post_data['payment_method'],$set_option)
								$_5="block";
							}	
							if($post_data['payment_method']=='6'){//&& in_array($post_data['payment_method'],$set_option)
								$_6="block";
							}	
							if($post_data['payment_method']=='7'){//&& in_array($post_data['payment_method'],$set_option)
								$_7="block";
							}								
						} else {
							if($value=='1'){
								$_1="block";
							}
							if($value=='2'){
								$_2="block";
							}
							if($value=='3'){
								$_3="none";
							}	
							if($value=='4'){
								$_4="block";
							}
							if($value=='5'){
								$_5="block";
							}	
							if($value=='6'){
								$_6="block";
							}	
							if($value=='7'){
								$_7="block";
							}	
						}
						
					}
					//dd($_4);
				?>
				<div class="col-lg-12">
					<h2 class="title"></h2>
					@if($post_data['payment_method'] ==0)
					<h2 class="title">Choose Your Payment Method</h2>
					@endif	
					<div class="accordion" id="accordionExample">
						<div class="card" style="display:{{$_1}};"><!-- section 1 card holder -->

							<div class="card-header collapsed" id="suncash_card"  data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" style="cursor:pointer">
								<i class="fas fa-caret-right"></i>
								<div class="title">
									<span>SunCash Account</span>
									<span>Pay using your SunCash account</span>
						
									<img src="{{base_url('assets_main/imgs/suncash-icon-light.png')}}" class="colored">
									<img src="{{base_url('assets_main/imgs/suncash-icon.png')}}" class="light">
								</div>
							</div>

							<div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
								<div class="card-body v3">
									<div class="card"><!-- card main div -->

										<div class="card-body">
											<div class="suncash_login_section">
											<h2>Enter your card details below.</h2>
											<span>We dont share your financial details with merchant.</span>
											<form id="suncash_form" class="text-left">
													<div class="form-group">
														<label for="mobile">Mobile Number</label>
														<div class="form-div">
														<input type="text" class="form-control required " id="mobile" name="mobile" placeholder="Enter your mobile number" autocomplete="off" >
														</div>
													</div>
													<div class="form-group">
														<label for="pin">Pin</label>
														<div class="form-div">
														<input type="password" class="form-control required" id="pin" name="pin" placeholder="Enter your pin" autocomplete="off" maxlength="4">
														</div>
													</div>
													<input type="hidden" name="MerchantKey" id="MerchantKey" value="{{$post_data['MerchantKey']}}">  
													<div class="text-center"><button type="submit" id="card_next" class="btn btn-primary">Next</button></div>
											</form>
											</div>
											<div class="suncash_verify_section" style="display:none;">
											<form id="suncash_process_form" class="text-left">
													<div class="form-group">
														<label for="passcode">OTP (One Time Password)</label>
														<div class="form-div">
														<input type="number" class="form-control required" id="passcode" name="passcode" placeholder="Enter your OTP">
														</div>
													</div>
													<div class="text-center"><button type="submit" id="card_submit" class="btn btn-primary">Process Payment</button></div>
											</form>										
											</div>	
										</div>

									</div><!-- card main div end -->
								</div>
							</div>

						</div> <!-- end of section 1 card holder -->

					  	<div class="card" id="card_section" style="display:{{$_2}};"><!-- card 2nd div -->
							<div id="cc"  class="card-header collapsed"  data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo" style="cursor:pointer"><!--collapsed -->
								<i class="fas fa-caret-right"></i>
								<div class="title">
									<span>Debit/Credit Cards</span>
									<span>Pay using your credit/debit cards</span>
								</div>
								<img src="{{base_url('assets_main/imgs/card-icon.png')}}" class="colored">
								<img src="{{base_url('assets_main/imgs/card-icon-light.png')}}" class="light">
							</div>

							<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
								<div class="card-body v3">
									<div class="card">
										<div class="card-body">
											<div id="card_info">
												<div id="show_cenpos_section">
												
													<h2>Enter your card details below.</h2>	
													<div class="col-12" id="NewCenposPlugin">	
													</div>
													<center>
													<div class="btn-group col-12" role="group">
														<div class='col 2-6-2'>
															<button type="button" class="btn btn-primary full" id="submit">Pay as Guest</button> 
														</div>
														<div class='col 2-6-2'>
															<button type="button" class="btn btn-primary full" id="used_card">Use Linked Card</button>
														</div>
													</div>
													</center>
												</div>
												<div id="show_cards_section">
													<div class="label text-center"></div><br>
														<form id="process_linked_card_form">
														@if(!empty($customer_merchant_cards))
														<div class="form-group">
															<H2>Choose saved/linked Card</H2>
															<div class="form-div">
																<div class="table-responsive">
																	<table class="table table-sm" style="width:100%">
																		<thead>
																		<tr>
																			<th scope="col">Select</th>
																			<th scope="col">Card Type</th>
																			<th scope="col">Last 4 digit</th>
																			<th scope="col">Status</th>
																			<th scope="col">Action</th>
																		</tr>
																		</thead>
																		<tbody>
																		@if(!empty($customer_merchant_cards))
																			@foreach($customer_merchant_cards as $ccdata_val)
																			<!-- dont show if no card is active/pending -->

																				<tr>
																					@if($ccdata_val['is_pending'] ==0 &&  $ccdata_val['status']==0 && $ccdata_val['is_rejected']==0)
																						<td scope="row"><input type="checkbox" class="select_card" id="{{base64_encode($ccdata_val['id'])}}_c" value='{{base64_encode($ccdata_val['id'])}}'></td>
																					@else
																						<td></td>
																					@endif
																					<td>
																						<?php
																							$card_type='';
																							if($ccdata_val['card_type']=='DISCOVER'){
																								$card_type='discover.png';
																							} else if ($ccdata_val['card_type']=='VISA'){
																								$card_type='visa.png';
																							} else if ($ccdata_val['card_type']=='MASTERCARD'){
																								$card_type='master.png';
																							} else if ($ccdata_val['card_type']=='AMEX'){
																								$card_type='amex.png';
																							}
																						?>
																						<img src="{{base_url('assets/img/cards')."/".$card_type}}" width="77" height="50" alt=""> 
																					</td>

																					<td>{{str_pad($ccdata_val['card_last_four_digits'], 16, "*", STR_PAD_LEFT)}}</td>
																				
																						<?php
																							$status='';
																							if($ccdata_val['is_rejected'] ==1 ){
																								$status='REJECTED';
																							} else if ($ccdata_val['status']==0 && $ccdata_val['is_verified']==1){
																								$status='VERIFIED';
																							} else if ($ccdata_val['status']==1){
																								$status='UNLINKED';
																							} else if ($ccdata_val['status']==0 && $ccdata_val['is_pending']==1 && $ccdata_val['is_verified'] !=1){
																								$status='Not Verified';
																							} else if ($ccdata_val['status']=='AMEX'){
																								$status='amex.png';	
																							}	else{
																								$status='test';
																							}															
																						?>
																					<td>{{$status}}</td>
																					<td>
																						@if($ccdata_val['status']==0 && $ccdata_val['is_verified']==0 && $ccdata_val['is_pending'] ==1)
																						<button type="button" class="btn btn-warning verify_card" id="{{base64_encode($ccdata_val['id'])}}_r"  _c="{{base64_encode($ccdata_val['id'])}}" style="margin-top: 0;height: 10px;width: 75px;">Verify</button>	
			
																						@endif															
																						<!-- <button type="button" class="btn btn-danger remove_card" id="{{base64_encode($ccdata_val['id'])}}_r"  _c="{{base64_encode($ccdata_val['id'])}}" style="margin-top: 0;height: 10px;width: 75px;">Unlink</button> -->
																					
																					</td>
																					
																				</tr>
																				
																			
																			@endforeach
																		@endif
																						<input type="hidden" name="refid" id="refid" value="{{$post_data['reference_id']}}"> 	
																						<input type="hidden" name="refid" id="merchant_customer_id" value="{{$post_data['merchant_customer_id']}}"> 
																		</tbody>
																	</table>
																</div>
															</div>
														</div>

													<center>									
														<div class="btn-group col-8" role="group">
															<input type="hidden" id="ccwu" name="ccwu" />
															@if($is_cc_guest=='1')
															<div class='col 2-6-2'>
																
																<button type="button" class="btn btn-primary full" id="back_to_customer">Back <<</button>
																
															</div>
															@endif
															<div class='col 2-6-2'>
																<button type="submit" class="btn btn-primary full" id="process_linked_card">Pay</button>
															</div>
														</div>
														</form>
														@else
														<p style="color:red;font-size:18px;">Must save/link your card first.</p>
														@endif

													</center>	
												</div>
												<div id="success_section" style="display:none;" >
													<div class="label text-center">Card Details</div>

													<div class="item">
														<div class="row">
														<div class="col">Card Number: </div>
														<div class="col text-right"><span id="card_number"></span></div>
														</div>
													</div>
													<div class="item">
														<div class="row">
														<div class="col">Name on Card: </div>
														<div class="col text-right"><span id="name_on_card"></span></div>
														</div>
													</div>
													<div class="item total">
														<div class="row">
														<div class="col label">Card Type: </div>
														<div class="col text-right amount"><span id="card_type"></span> </div>
														</div>
													</div>
													<input type="hidden" name="tid" id ="tid" value="">
													<div class="payment_btn_section">
														<div class="text-center">
															<button type="button" class="btn btn-primary full" id="payment_auth">Process Payment</button>
														</div>
													</div>
													<div class="cancel_section" style="display:none;">
													<!-- <div class="text-center"><button type="button" class="btn btn-primary full" id="cancel_payment"><i class="fa fa-times"> Cancel Order</i></button></div>	 -->
														<div class="text-center">
														<button type="button" class="btn btn-primary full" id="another_card">Use Another Card</button>
														</div>										  			
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div><!-- end of section 2 card holder -->					  

						<div class="card" style="display:{{$_3}};"><!-- card 3rd div -->
								<div class="card-header collapsed noncardtab" id="headingOne"  data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree" style="cursor:pointer">
									<i class="fas fa-caret-right"></i>
									<div class="title">
										<span>Cash Payment</span>
										<span>Pay with Cash at SunCash Stores and Kiosks</span>
									</div>
									<img src="{{base_url('assets_main/imgs/cash-icon-light.png')}}" class="colored">
									<img src="{{base_url('assets_main/imgs/cash-icon.png')}}" class="light">
								</div>

								<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
									<div class="card-body v3">
										<div class="card">
											<div class="card-body">
												<h2>Please enter your name</h2>
												<span>We dont share your financial details with merchant.</span>
													<form id="cashpayment_form" class="text-left">
														<div class="form-group row border-bottom">
														<input type="text" class="form-control col-12 col-lg-6 required_cash_checkout" id="firstName" placeholder="First Name">
														<input type="text" class="form-control col-12 col-lg-6 required_cash_checkout" id="lastName" placeholder="Last Name">
														</div>
														<div class="border-bottom" style="display:none;">
															<p class="table-title">Transaction Details:</p>
															<table class="table table-borderless">
															<tbody>
																<tr>
																<td>Amount</span></td>
																<td class="text-right">$ <span class="amount_cash">{{!empty($post_data) ? number_format($post_data['total_amount'],2,'.','') : 0.00 }}</span></td>
																</tr>
															</tbody>
															<tfoot>
																<tr class="total">
																<td class="f-medium">Total Payment</span></td>
																<td class="text-right amount "><span class="amount_total_cash">{{!empty($post_data) ? number_format($post_data['total_amount'],2,'.','') : 0.00 }} BSD</span></td>
																</tr>
															</tfoot>
															</table>
														</div>
														<div class="mt-3">
															<p class="table-title">How would you like your code received?</p>
															<div class="custom-radio">
																<label class="label-container">
																	<input type="checkbox" id="chkSMS" name="chkSMS" value="0">
																	<span class="checkmark"></span>
																	<span class="label">Send via SMS</span>
																	</label>
																	<input type="text" placeholder="Enter your mobile number" class="form-control bfh-phone" id="mobile_cash" name="mobile_cash" data-format="1 (ddd) ddd-dddd" value ="1 242" readonly>
																	<label class="label-container">
																	<input type="checkbox" id="chkEmail" name="chkEmail" value="0">
																	<span class="checkmark"></span>
																	<span class="label">Send via email</span>
																</label>
																<input  type="email" id="email" name="email" placeholder="Enter your email" class="form-control" readonly>
															</div>
														</div>
														<div class=""><button type="submit" id="process_payment_cash" class="btn btn-primary" data-loading-text="Processing...">Generate Payment Code</button></div>
													</form>
											</div>
										</div>
									</div>
								</div>
						</div><!-- end of section 3 card holder -->
						<div class="card" style="display:{{$_4}}"><!-- section4 card holder -->
							<div class="card-header collapsed" id="suncash_voucher"  data-toggle="collapse" data-target="#collapseFour" aria-expanded="true" aria-controls="collapseFour" style="cursor:pointer">
								<i class="fas fa-caret-right"></i>
								<div class="title">
									<span>SunCash Voucher</span>
									<span>Pay using your SunCash Voucher</span>
								</div>
								<img src="{{base_url('assets_main/imgs/suncash-icon-light.png')}}" class="colored">
								<img src="{{base_url('assets_main/imgs/suncash-icon.png')}}" class="light">
							</div>
						
							<div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
								<div class="card-body v3">
									<div class="card"><!-- card main div -->

										<div class="card-body">
											<div class="suncash_login_section">
												<h2>Enter your voucher details below.</h2>
												<!-- <span>We dont share your financial details with merchant.</span> -->
												<form id="suncash_voucher_form" class="text-left">
														<div class="form-group" style="display:none">
															<label for="mobile">Mobile Number</label>
															<div class="form-div">
																<input type="text" class="form-control bfh-phone" id="voucher_mobile_number" name="voucher_mobile_number" data-format="1 (ddd) ddd-dddd" value ="1 242" required >
															</div>
														</div>
														<div class="form-group">
															<label for="voucher_number">Voucher Number</label>
															<div class="form-div">
															<input type="text" class="form-control required" id="voucher_number" name="voucher_number" placeholder="Enter your voucher number" autocomplete="off" required>
															</div>
														</div>
														<div class="form-group">
															<label for="pin_voucher">PIN</label>
															<div class="form-div">
															<input type="password" class="form-control required" id="voucher_pin" name="voucher_pin" placeholder="Enter your pin" autocomplete="off" maxlength="5" required>
															</div>
														</div>
														<div class="text-center"><button type="submit" id="voucher_card_next" class="btn btn-primary">Process Payment</button></div>
												</form>
			
											</div>	
											<br>
											Note: This system does not give change for excess voucher amount.
										</div>

									</div><!-- card main div end -->
								</div>
							</div>
						</div> <!-- end of section 4 card holder -->

					<div class="card" style="display:{{$_5}}"><!-- section4 sand holder -->
					<div class="card-header collapsed" id="sanddollar_payment"  data-toggle="collapse" data-target="#collapseFive" aria-expanded="true" aria-controls="collapseFive" style="cursor:pointer">
						<i class="fas fa-caret-right"></i>
						<div class="title">
							<span>SandDollar</span>
							<span>Pay using your Sand Dollars</span>
						</div>
						<img src="{{base_url('assets_main/imgs/sand-logo.png')}}" class="colored">
						<img src="{{base_url('assets_main/imgs/sand-logo.png')}}" class="light">
					</div>
					
					<div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
						<div class="card-body v3">
							<div class="card">
								<div class="card-body">
									<form id="sand_process_form" class="text-left">
										How would you like to pay?
										<select class="form-control" name="type" id="type"  placeholder="">
											<option value="">-Select- </option>
											<option value="qr">Scan with SandDollar Enabled App </option>
											<option value="manual">Scan Receivers QRCode </option>
											<option value="custom_name">Custom Name</option>
										</select>
										<div class="qr" style="display:block">
											<br>
												<center>Show QR Code for the Sender to Scan  in SandDollar App<br>

												<?php
													$custom_val='';

														$qrcode = \qr::GenerateQr('nzia:qr/sinag+'.''.$post_data['OrderReference'].'@bs.nzia?amount='.$post_data['subtotal'],'',300);
														$custom_val1 ="nzia:qr/sinag+".$post_data['OrderReference']."@bs.nzia?amount=".$post_data['subtotal'];
														// $custom_name ="sinag+".$post_data['OrderReference']."@bs.nzia?amount=".$post_data['subtotal'];
														$custom_val ="suncash://suncash.me/".$post_data['OrderReference']."/".$post_data['subtotal'];
														$custom_name ="sinag+".$post_data['OrderReference'];
														$custom_val2 ="nzia:qr/sinag+".$post_data['OrderReference']."@bs.nzia?amount=".$post_data['subtotal'];
												
														$qrimage =' <img src="data:'.$qrcode->getContentType().';base64,'.$qrcode->generate().'" id="qrcode" />';
														echo $qrimage;
												
												?>
												<br>
												<br>
												@if($location =='app')


													<div class="text-center"><button type="button" id="pay_custom"  name="pay_custom" class="btn btn-orange full">Pay</button></div><br>
													<center>  OR</center><br>
													<div class="input-group ">

														<input type="text" class="form-control  text_to_copy" id="custom_url" name="custom_url" placeholder="Enter your pin" autocomplete="off" value ="{{$custom_val}}" readonly>
														<div class="input-group-append form-div">
															<button type="button" class=" btn-sm btn-primary" id="copy_text1" style="margin-top: 0px;width:50px">
																<span class="fa fa-clipboard" id="copy" ></span>
															</button>			
																			
														</div>
													</div>	
													<br>Copy and paste custom name to sand Dollar Enabled App and manually enter the amount to pay


												@else
													<center>  OR</center><br>
													<div class="input-group ">
														<input type="text" class="form-control  text_to_copy" id="custom_url" name="custom_url" placeholder="Enter your pin" autocomplete="off" value ="{{$custom_name}}" readonly>
														<div class="input-group-append form-div">
															<button type="button" class=" btn-sm btn-primary" id="copy_text" style="margin-top: 0px;width:50px">
																<span class="fa fa-clipboard" id="copy" ></span>
															</button>							
														</div>
													</div>	
													<br>Copy and paste custom name to sand Dollar Enabled App and manually enter the amount to pay
												@endif
												</center>	
												<br>	
										</div>
										<div class="manual" style="display:none">				

										<div class="embed-responsive embed-responsive-4by3"> 
											<video  id="preview" width="400" height="240" playsinline></video>
										</div> 
									
											<div id="show_sdcards_section">
												<div class="label text-center"></div>
													<form id="process_linked_card_form">
													@if(!empty($customer_merchant_sdcards))
													<div class="form-group">
														<H2>Choose Saved SandDollar Account</H2>
														<div class="form-div">

															<div class="table-responsive">
																<table class="table table-sm" style="width:100%">
																	<thead>
																	<tr>
																		<th scope="col">Nickname</th>
																		<th scope="col">Full Name</th>
																		<!-- <th scope="col">Email</th> -->
																		@if($sd_settings['allow_sanduser_delete']==1)	
																		<th scope="col">Action</th>
																		@endif
																	</tr>
																	</thead>
																	<tbody>
																	@if(!empty($customer_merchant_sdcards))
																		@foreach($customer_merchant_sdcards as $sd)
																			<tr class="sdc" sdn="{{ $sd['contact_name']}}" sde="{{ $sd['contact_email']}}"   sdnm="{{ $sd['contact_nickname']}}" sdm="{{ $sd['contact_number']}}" sdn="{{ $sd['contact_nickname']}}" sdcd="{!! $sd['contact_card'] !!}" raw="{{$sd['contact_card']}}">
																				<td>{{$sd['contact_nickname']}}</td>	
																				<td>{{$sd['contact_name']}}</td>
																				<td>
																					@if($sd_settings['allow_sanduser_delete']==1)
																					<button type="button" class="btn btn-danger remove_sdcard" id="{{base64_encode($sd['id'])}}_r"  _c="{{base64_encode($sd['id'])}}" style="margin-top: 0;height: 10px;width: 75px;">X</button>			
																					@endif
																				</td>
																			</tr>
																		@endforeach
																	@endif
																	</tbody>
																</table>
															</div>
														</div>
													</div>
													@endif
											</div>

											<label for="sand_qr">Scan SandDollar Card QRCode</label>
											<div class="input-group ">													
												<input type="text" class="form-control required" id="sand_qr" name="sand_qr" placeholder="" autocomplete="off"  required readonly>
												<div class="input-group-append form-div">
													<button type="button" class=" btn-sm btn-primary" id="clear_text1" onclick="clearFields()" style="margin-top: 0px;width:50px">
														<span class="fa fa-trash" id="copy" ></span>
													</button>							
												</div>
											</div>				
											<br>	
				
											<div class="sand_details" style="display:none">
												<div class="sand_details_nickname" style="display:none">
													<div class="form-group">
														<label for="name_card">Nick Name</label>
														<div class="form-div">
														<input type="text" class="form-control" id="sand_nickname" name="sand_nickname" placeholder="Enter your nickname" autocomplete="off" >
														</div>
													</div>	
												</div>	
												<div class="sand_details_fullname" style="display:none">
													<div class="form-group">
														<label for="mobile">Full Name</label>
														<div class="form-div">
															<input type="text" class="form-control required " id="sand_fullname" name="sand_fullname" placeholder="Enter your full namer" autocomplete="off">
														</div>
													</div>	
												</div>		
												<div class="sand_details_mobile" style="display:none">					
													<div class="form-group">
														<label for="mobile">Mobile Number</label>
														<div class="form-div">
															<input type="text" class="form-control required " id="sand_mobile" name="sand_mobile" placeholder="Enter your mobile number" autocomplete="off">
														</div>
													</div>
												</div>		
												<div class="sand_details_email" style="display:none">
													<div class="form-group">
														<label for="sand_email">Email Address</label></label>
														<div class="form-div">
														<input type="text" class="form-control" id="sand_email" name="sand_email" placeholder="Email Address" autocomplete="off">
														</div>
													</div>																				
												</div>																														
												<div class="form-group">
													<label for="pin">OTP</label>
													<div class="form-div"> 
														<input type="password" class="form-control required" id="sand_otp" name="sand_otp" placeholder="Enter your pin" autocomplete="off" maxlength="6">
													</div>
												</div>	
												<div class="form-group">
													<label for="pin">Pin</label>
													<div class="form-div">
														<input type="password" class="form-control required" id="sand_pin" name="sand_pin" placeholder="Enter your pin" autocomplete="off" maxlength="6">
													</div>
												</div>		
											</div>																														
											<div class="text-center">
												<button type="submit" id="sand_process_payment" class="btn btn-primary">Process Payment</button>
											</div> 
										</div>

										<div class="custom_name" style="display:none">		
											<div class="form-group">
												<div class="">
													<br>
														<center>								
															@if($location =='app')
															<div class="text-center"><button type="button" id="pay_custom1"  name="pay_custom1" class="btn btn-orange full">Pay</button></div><br>
															<center>  OR</center><br>
															<div class="input-group ">

																<input type="text" class="form-control  text_to_copy" id="custom_url1" name="custom_url1" placeholder="Enter your pin" autocomplete="off" value ="{{$custom_val}}" readonly>
																<div class="input-group-append form-div">
																	<button type="button" class=" btn-sm btn-primary" id="copy_text1" style="margin-top: 0px;width:50px">
																		<span class="fa fa-clipboard" id="copy" ></span>
																	</button>		
																			
																</div>
															</div>	
															<br>Copy and paste custom name to sand Dollar Enabled App and manually enter the amount to pay

															@else
															<div class="input-group ">

																<input type="text" class="form-control  text_to_copy" id="custom_url1" name="custom_url1" placeholder="Enter your pin" autocomplete="off" value ="{{$custom_name}}" readonly>
																<div class="input-group-append form-div">
																	<button type="button" class=" btn-sm btn-primary" id="copy_text1" style="margin-top: 0px;width:50px">
																		<span class="fa fa-clipboard" id="copy" ></span>
																	</button>							
																</div>
																
															</div>	
															<br>Copy and paste custom name to sand Dollar Enabled App and manually enter the amount to pay
															@endif
														</center>

												</div>
			
											</div>

										</div>	

									</form>
								</div>
							</div><!-- card main div end -->
						</div>
					</div>
					</div> <!-- end of section 5 card holder -->			 

					<div class="card" style="display:{{$_6}};"><!-- section4 sand holder{{$_6}} -->

					<div class="card-header collapsed" id="amazon_payment"  data-toggle="collapse" data-target="#collapseSix" aria-expanded="true" aria-controls="collapseSix" style="cursor:pointer">
						<i class="fas fa-caret-right"></i>
						<div class="title">
							<span>Amazon Pay</span>
							<span>Pay using your Amazon Account</span>
						</div>
						<img src="{{base_url('assets_main/imgs/amazon.png')}}" class="colored">
						<img src="{{base_url('assets_main/imgs/amazon.png')}}" class="light">
					</div>
					
					<div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordionExample">
						<div class="card-body v3">
						<div class="card"><!-- card main div -->

							<div class="card-body">
					
								<div class="amazon_section">
									<!-- charge -->
									<form id="amazon_form" class="text-left">
										<div id="show_order_review"  style=" display:none ;">	

											<div class="item primary-border" >
												<div class="label text-center">Transaction Details:</div>
												<!-- <div class="item" style="margin-bottom: 0px !important;margin-top: 0px !important;">
													<div class="row text-details">
														<div class="col">Principal</div>
														<div class="col text-right "> {{$ci->session->userdata('amz_amount')}}</div>
													</div> 
												</div> 
												<div class="item" style="margin-bottom: 0px !important;margin-top: 0px !important;">
													<div class="row text-details">
														<div class="col">Convenience Fee</div>
													<div class="col text-right">$ <span class="convenience_fee">0.00</span></div>
													</div>
												</div>		 -->
												<hr>
												<div class="item" style="margin-bottom: 0px !important;margin-top: 0px !important;">
													<div class="row text-details">
													<div class="col">Total Due</div>
														<div class="col text-right total" ></div>
													</div>
												</div>										
																				
											</div>	
											<br>	
											<!-- <label for="amz_amount">Amount</label> -->
											<input type="hidden" id="amz_amount" name="amz_amount" autocomplete="off" readonly required>
				
											<!-- <label for="amazon_email">Convenience Fee</label>
											<input type="email" class="form-control" id="amz_cc_fee" name="amz_fee"  autocomplete="off" readonly required> -->

		
											<!-- <div class="form-group">
												<label for="amazon_email">Total Due</label>
												<div class="form-div"> -->
												<input type="hidden" class="form-control" id="amz_total" name="amz_total"  autocomplete="off" readonly required>
												<!-- </div>
											</div> -->

											<div class="form-group">
												<label for="name_card">Full Name</label>
												<div class="form-div">
												<input type="text" class="form-control" id="amazon_fullname" name="amazon_fullname" placeholder="Enter your full name" autocomplete="off" required readonly>
												</div>
											</div>							
											<div class="form-group">
												<label for="amazon_email">Email Address</label>
												<div class="form-div">
												<input type="email" class="form-control" id="amazon_email" name="amazon_email" placeholder="Email Address" autocomplete="off" readonly required>
												</div>
											</div>

											<div class="form-group">
												<label for="mobile">Mobile Number</label>
												<div class="form-div">
													<input type="text" class="form-control required " id="amazon_mobile" name="amazon_mobile" placeholder="Enter your mobile number" autocomplete="off" required >
													<!-- data-format="1 (ddd) ddd-dddd" value ="1 242 bfh-phone"  -->
												</div>
											</div>												
											<!-- <button type="button" name="button" id="Logout">Logout</button> -->
										</div>	
										<div class="amz_section">	

											<div class="text-center">
												<div>
													<center>
													<div id="AmazonPayButton"></div>
													</center>
												</div>
											</div>
										</div>
										<input type="hidden" class="form-control required " id="amz_or" name="amz_or" autocomplete="off"  >
										<input type="hidden" class="form-control required " id="amz_auth" name="amz_auth" autocomplete="off"  >


										<!-- campture process -->
										<div class="amz_process_section" style=" display:none ;">
											<div class="text-center"><button type="submit" id="amz_payment" class="btn btn-primary">Process Payment</button></div>
										</div>
									</form>
								</div>
					
							</div>	

						</div><!-- card main div end -->
						</div>
					</div>

					</div> <!-- end of section 6 card holder -->

					<div class="card" style="display:{{$_7}}"><!-- section4 sand holder -->

					<div class="card-header collapsed" id="paypal_payment"  data-toggle="collapse" data-target="#collapseSeven" aria-expanded="true" aria-controls="collapseSeven" style="cursor:pointer">
						<i class="fas fa-caret-right"></i>
						<div class="title">
							<span>Paypal</span>
							<span>Pay using your PayPal Account</span>
						</div>
						<img src="{{base_url('assets_main/imgs/paypal.png')}}" class="colored">
						<img src="{{base_url('assets_main/imgs/paypal.png')}}" class="light">
					</div>
					
					<div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordionExample">
						<div class="card-body v3">
						<div class="card">

							<div class="card-body">
								<div class="paypal_section">
									
									<form id="paypal_process_form" class="text-left">
																													
										<div class="form-group" style="display:none">
											<label for="mobile">Mobile Number</label>
											<div class="form-div">
												<input type="text" class="form-control required " id="paypal_mobile" name="paypal_mobile" placeholder="Enter your mobile number" autocomplete="off"  > 
											</div>
										</div>
											<br>
										<center>																				
											<div id="paypal-button-container"></div>
										</center>

									</form>
								</div>
							</div>	

						</div><!-- card main div end -->
						</div>
					</div>

					</div> <!-- end of section 7 card holder -->

					</div>  <!--end of accordion -->


				</div><!-- end of accordion -->	
				</div>
			@else
			<div class="col-lg-8">
				<center><b>Please setup payment options on Business Portal.</b></center>
			</div>	
			@endif
			</div>
		</div>
	</main>

	@include('layout.modal')

	<script src="{{PAYPAL_CLIENTID}}"></script>	
	<script src="{{base_url('assets_main/js/jquery-3.2.1.min.js')}}"></script>
	<script src="{{base_url('assets_main/js/bootstrap.min.js')}}"></script>
    <!-- <script src="{{base_url('assets_main/js/select.min.js')}}"></script> -->
	{!!$assetHelper->link_plugins('js','sidebar-nav/dist/sidebar-nav.min.js')!!}
	{!!$assetHelper->link_plugins('js','override/jquery.slimscroll.js')!!}
	{!!$assetHelper->link_plugins('js','override/waves.js')!!}
	{!!$assetHelper->link_plugins('js','override/custom.min.js')!!}
	{!!$assetHelper->link_plugins('js','sweetalert2-master/dist/sweetalert2.all.min.js')!!}
	{!!$assetHelper->link_plugins('js','bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js')!!}
	{!!$assetHelper->link_plugins('js','curioussolutions-datetimepicker/dist/DateTimePicker.min.js')!!}
    {!!$assetHelper->link_plugins('js','Inputmask-5.x/dist/jquery.inputmask.min.js')!!}
    {!!$assetHelper->link_plugins('js','bootstrap-formhelpers-phone/bootstrap-formhelpers-phone.js') !!}
    {!!$assetHelper->link_plugins('js','bootstrap-formhelpers-phone/bootstrap-formhelpers-phone.format.js') !!}
	{!!$assetHelper->link_plugins('js','dropzone-master/dist/dropzone.js')!!}	
	{!!$assetHelper->link_plugins('js','global/global.js')!!}	
	<script src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>
	<script src="https://www.cenpos.com/Plugins/porthole.min.js"></script>
	<script src="https://www.cenpos.com/Plugins/jquery.simplewebpay.js"></script> 
	<script src="https://unpkg.com/filepond/dist/filepond.min.js"></script>
	<script src="https://unpkg.com/jquery-filepond/filepond.jquery.js"></script>
	<script src="https://unpkg.com/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.min.js"></script>
	<script src="https://unpkg.com/filepond-plugin-image-exif-orientation/dist/filepond-plugin-image-exif-orientation.min.js"></script>
	<script src="https://unpkg.com/filepond-plugin-file-validate-type/dist/filepond-plugin-file-validate-type.js"></script>
	<script src="https://unpkg.com/filepond-plugin-file-encode/dist/filepond-plugin-file-encode.js"></script>
	<script src="{{base_url('assets_main/js/main.js')}}"></script>
	<!-- <script src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>	 -->
	{!!$assetHelper->link_plugins('js','global/global.js')!!}		
	{!!$assetHelper->link_plugins('js','instascan.min.jss')!!}
	@include('checkoutv2_js')
   <script src="https://static-na.payments-amazon.com/checkout.js"></script>	
	<script type='text/javascript'>
	var sdf_asl = '{{$sd_settings["allow_multiple_sandaccount"]}}';
	var allow_del = '{{$sd_settings["allow_sanduser_delete"]}}';
	  let scanner = new Instascan.Scanner({ video: document.getElementById('preview') });

		// var tempVideo = document.getElementsByTagName("video")[0];
		// tempVideo.width=scanner;
		// tempVideo.height='100%';
	  scanner.addListener('scan', function (content) {

		$("#sand_qr").val(content);
		
		$(".sand_details").show();



		var fullname = '{{$post_data["merchant_customer_fullname"]}}';
		var mobile = '{{$post_data["merchant_customer_mobile"]}}';   
		var email = '{{$post_data["merchant_customer_email"]}}';   
 
		if(fullname==""){
			$(".sand_details_fullname").show();
			$("sand_fullname").addClass("required");
		}else{
			$(".sand_details_fullname").hide();
			$("sand_fullname").removeClass("required");
		}
		if(mobile==""){
			$(".sand_details_mobile").show();
			$("sand_mobile").addClass("required");
		}else{
			$(".sand_details_mobile").hide();
			$("sand_mobile").removeClass("required");
		}
		if(email==""){
			$(".sand_details_email").show();
		}else{
			$(".sand_details_email").hide();
			$("sand_email").removeClass("required");
		}                  

		$(".sand_details_nickname").show();
 

	  });	

	$("#type").change(function(){
		if($(this).val()=='qr'){
			 scanner.stop();
			$(".qr").show();
			$(".qr_section").hide()
			$(".manual").hide();
			$(".custom_name").hide();
		}else if($(this).val()=='manual'){
		  Instascan.Camera.getCameras().then(function (cameras) {
			if (cameras.length > 0) {
			 scanner.start(cameras[0]);
			} else {
			 console.error('No cameras found.');
			}
		  }).catch(function (e) {
			console.error(e);
		  });

			$(".qr").hide();
			$(".qr_section").show()			
			$(".manual").show();
			$(".custom_name").hide();
			$(".custom_name_txt").hide();
		}else if($(this).val()=='custom_name'){
			 scanner.stop();
			$(".qr").hide();
			$(".qr_section").hide()			
			$(".manual").hide();
			$(".custom_name_txt").hide();
			$(".custom_name").show();			
		}else{
		scanner.stop();
		$(".qr").show();
		$(".qr_section").hide()
		$(".manual").hide();
		$(".custom_name").hide();		
		}
		
	});	

	if(typeof(EventSource)!=="undefined"){
		//alert('s');
			var url = "{{base_url('payment/sd/check_status_cfid/').$sand_data['referenceid']}}";
			var source=new EventSource(url);
			source.onmessage=function(event){
				var dt = $.parseJSON(event.data);
				console.log(dt.status);
				if(dt.status=='processed'){
					var url ="{{$post_data['CallbackURL']}}";
					window.location.href=url+dt.params;
					// $("#success_section_transaction").show();
				//	$("#sand_section").hide();
				}
				//document.getElementById("result").innerHTML += event.data + "<br>";
			};

	} else {
			document.getElementById("result").innerHTML="Sorry, your browser does not support server-sent events...";
	}


	function copyToClipboard($input) {
			$input.focus();
			$input.select();
			try {  
				var successful = document.execCommand('copy');
		alert("text copied");  
			} catch(err) {  
				console.error('Unable to copy'); 
		alert("cant copied");
			}	

	}
	$(document).ready(function(){
		var location = "{{$location}}";
		if(location=='web'){
			$(".qr").show();
			$("#type").val("qr").trigger("change");		
		}else{			
			$(".custom_name").show();	
			$("#type").val("custom_name").trigger("change");					
		}

    
		$("#copy_text").click(function(){
			copyToClipboard($(".text_to_copy"));
		});

		    
		$("#copy_text1").click(function(){
			copyToClipboard($(".text_to_copy"));
		});
	});	

	var AMAZON_IS_SANDBOX = '{{AMAZON_IS_SANDBOX}}';
	var issandbox= false;
	if(AMAZON_IS_SANDBOX=='true'){
		issandbox=true;
	}

	var payload = '{!!json_encode($payload)!!}';
	
	        amazon.Pay.renderButton('#AmazonPayButton', {
            // set checkout environment
            merchantId: 'AVJGZSMCRS438',
            ledgerCurrency: 'USD',
            sandbox: issandbox,                 
            // customize the buyer experience
            checkoutLanguage: 'en_US',
            productType: 'PayOnly',
            placement: 'Checkout',
            buttonColor: 'Gold',
	
            createCheckoutSessionConfig: {                     
                payloadJSON:payload ,
                signature: '{{$signature}}', 
                publicKeyId: 'AGWGBAZVOO5IESX4GE6WM6UG' 
            }   
        });

	@if(!empty($amz_order_deatils))
		$("#amazon_fullname").val("{{$ci->session->userdata('amz_sess_buyer')['buyername']}}");
		$("#amazon_email").val("{{$ci->session->userdata('amz_sess_buyer')['buyeremail']}}");
		$("#amazon_mobile").val();
		$("#show_order_review").show();
		$(".amz_section").hide();
		$(".amz_process_section").show();
		@if($amz_status=='review')
			$("#amz_payment").text('Place Order').val('{{$amz_status}}');
			getcfee('{{$post_data["total_amount"]}}');
			$(".total").text("{{$ci->session->userdata('amz_total')}}");
			$('#amazon_payment').trigger('click');			
		@elseif($amz_status=='pay')

			$("#amz_amount").val("{{$ci->session->userdata('amz_amount')}}");
			$("#amz_total").val("{{$ci->session->userdata('amz_total')}}");//total due in get fee api
			$("#amz_cc_fee").val("{{$ci->session->userdata('amz_cc_fee')}}");
			$("#amz_cc_ismerch").val("{{$ci->session->userdata('amz_cc_ismerch')}}");
			$(".total").text("{{$ci->session->userdata('amz_total')}}");
			$('#amazon_payment').trigger('click');			
			@if(!empty($ci->session->userdata('amz_mobile')))
			var amzmobile = "{{$ci->session->userdata('amz_mobile')}}";
			$("#amazon_mobile").val("{{$ci->session->userdata('amz_mobile')}}");
			@endif
			$("#amz_payment").text('Process Order').val('{{$amz_status}}');

		@endif
	@endif
 
	$("#amazon_form").submit(function(){
		var form_data =$(this).serializeArray();
		var m ='{{$post_data["MerchantKey"]}}';
		var r ='{{$post_data["reference_id"]}}';
		var total_due ='{{$amazon_fees["total_due"]}}';
		var dba_name ='{{$post_data["MerchantName"]}}';
		var status=$("#amz_payment").val();
		form_data.push({name: "m", value: m});
		form_data.push({name: "r", value: r});
		form_data.push({name: "total_due", value: total_due});
		form_data.push({name: "status", value: status});
		form_data.push({name: "dba_name", value: dba_name});
		// form_data.push({name: "amz_auth", value: amz_auth});
	
		var url_to_process = status=='review' ? "{{base_url('merchant_checkout/amazon_review_process')}}" : "{{base_url('merchant_checkout/process_amazon_checkoutv2')}}";
		$.ajax({
			type: "POST",
			url: url_to_process,
			data: form_data,
			dataType: "json",
			beforeSend: function (response) {
				loader2.showPleaseWait();
				$("amz_payment").prop('disabled',true);  
			},
			complete: function (response) {
				$("#amz_payment").prop('disabled',false);
				loader2.hidePleaseWait();  
			},
			success: function (response) {
				console.table(response);
				$("#amz_payment").prop('disabled',false);
				if(response.success){
					// window.location.href=response.url;
					window.location.replace(response.url);
				} else {
				$("#amz_payment").prop('disabled',false);
				loader2.hidePleaseWait();
				// swal(response.msg);
				}
			},
			error: function (response) {
				loader2.hidePleaseWait();
				// swal(xhr.msg);
			},
   		});
		return false;
	});
	function getcfee(amount){
	    $.ajax({
	      url: '{{base_url("merchant_checkout/get_fees_payment")}}',
	      type: 'POST',
	      dataType: 'json',
	      data:{

			  total_due:'{{$post_data["total_amount"]}}',
			  MerchantKey:'{{$post_data["MerchantKey"]}}',

			},
	   	  beforeSend:function(xhr, textStatus) {
	        $("#submit").prop('disabled', true);
	      },		  
		  complete: function(xhr, textStatus) {
		  },
	      success: function(data, textStatus, xhr) {
	        //called when successful
	        //paste values
	        if(data.success){
	        	$(".convenience_fee").text(data.fee);
				$(".total").text(data.total);
				$("#hid_pfee").val(data.pf);
				// $("#hid_tfee").val(data.tf);
				// $("#hid_totalfee").val(data.fee);

		    	// $("#submit").prop('disabled', false);
	        }  else {
	        	swal(data.msg);
		    	// $("#submit").prop('disabled', false);
	        }

	      },
	      error: function(xhr, textStatus, errorThrown) {
	        //called when there is an error
        	swal(xhr.msg);
	    	// $("#submit").prop('disabled', false);
	      }
	    });
	}
	paypal.Buttons({
		style:{
			size: 'responsive',
			label: 'checkout',
		},
		createOrder:function(data,actions){
		return actions.order.create({
			"purchase_units": [
			{
				"amount": {
				"currency_code": "USD",
				"value":$("#aval").val()
				}
			}]

		});
		},

		onApprove: function(data, actions) {
		loader2.showPleaseWait();
		// This function captures the funds from the transaction.
		return actions.order.capture().then(function(details) {
		loader2.hidePleaseWait();
			console.log(details);
			// This function shows a transaction success message to your buyer.
			//alert('Transaction completed by ' + details.payer.name.given_name);
			// alert($("#paypal_mobile").val()+" "+$("#paypal_mobile").val());
			var form_data={
			'fullname':details.purchase_units[0].shipping.name.full_name,
			'mobile' :$("#paypal_mobile").val(),
			// 'email':$("#paypal_email").val(),
			'email':details.purchase_units[0].payee.email_address,
			'm':'{{$post_data["MerchantKey"]}}',
			'r':'{{$post_data["reference_id"]}}',
			'paypal_reference_number':details.purchase_units[0].payments.captures[0].id,
			'status':details.status,
			'response':details,
			'total_due':details.purchase_units[0].payments.captures[0].amount.value,
			};
			//console.log(form_data);
			$.ajax({
			url: '{{base_url("merchant_checkout/process_paypal_checkout")}}',
			type: 'POST',
			dataType: 'json',
			data: form_data,
				beforeSend:function(xhr, textStatus) {
				//called when complete
				loader2.showPleaseWait();
				$("#card_next").prop('disabled', true);
			},
				complete: function(xhr, textStatus) {
				//called when complete
				loader2.hidePleaseWait();
				$("#card_next").prop('disabled', false);
			},
			success: function(data, textStatus, xhr) {
				//called when successful
				if(data.success){
					// console.log(data.data);
					// loader2.hidePleaseWait();//remove to refrain customer to refresh page
					window.location.href=data.url; 	
				} else {
					loader2.hidePleaseWait();
					swal(data.msg);
				}
			},
			error: function(data, textStatus, errorThrown) {
				//called when there is an error
				loader2.hidePleaseWait();
				swal(data.msg);
			}
			});

		});
		}
	}).render('#paypal-button-container');
	$("#pay_custom").click(function(){
		setTimeout(function() {
			window.location.replace("{{$custom_val2}}");
				// This is a fallback if the app is not installed.
				// It could direct to an app store or a website
				// telling user how to get the app
		}, 25);
		// window.location = "custom-uri://AppShouldListenForThis";
		window.location.replace("{{$custom_val}}");

	});
	$("#pay_custom1").click(function(){
		setTimeout(function() {
			window.location.replace("{{$custom_val2}}");
				// This is a fallback if the app is not installed.
				// It could direct to an app store or a website
				// telling user how to get the app
		}, 25);
		// window.location = "custom-uri://AppShouldListenForThis";
		window.location.replace("{{$custom_val}}");
	});
	function clearFields() {
		document.getElementById("sand_qr").value=""
		$(".sand_details").hide();
		document.getElementById("sand_nickname").value=""
		document.getElementById("sand_fullname").value=""
		document.getElementById("sand_mobile").value=""
		document.getElementById("sand_otp").value=""	
		document.getElementById("sand_pin").value=""		
	}	
	</script>

</body>
</html>