<script>
    function isEmail(email) {
      var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      return regex.test(email);
    }
    //check 

    function CallbackSuccess(responseData) {
    alert(JSON.stringify(responseData));
    }
    function CallbackCancel(responseData) {
    alert(JSON.stringify(responseData));
    }
    // initate 
    @if($is_cc_guest=='1')
        $("#show_cards_section").hide();
        $("#show_cenpos_section").show();
    @else
    $("#show_cards_section").show();
    $("#show_cenpos_section").hide();
    $("#process_linked_card").prop('disabled',false); 
    @endif
    
    $(document).on('click','#used_card',function(){
        $("#show_cards_section").show();
        $("#show_cenpos_section").hide();
        $("#process_linked_card").prop('disabled',false); 
    });
    $(document).on('click','#back_to_customer',function(){
        $("#show_cards_section").hide();
        $("#show_cenpos_section").show();
        $("#ccwu").html("");
    });
 
	$("#another_card").click(function(){
    	window.location.reload();        	
    });

    $("#submit").click(function(){
        // loader.showPleaseWait();
        $("#NewCenposPlugin").submitAction();
    });
    //create site verify
    var merchant='{{CENPOST_MERCHANT_ID}}';
    var verify_params ='{{$verify_params["Data"]}}';

    $("#NewCenposPlugin").createWebpay({
    url: 'https://www.cenpos.net/simplewebpay/cards',
    params : "customerCode=1&verifyingpost="+verify_params+"&iscvv=true",
    height:'350px',
    sessionToken:false,
        beforeSend:function(xhr, textStatus) {
            //called when complete
            $("#submit").prop('disabled', true);
          },
        complete: function(xhr, textStatus) {
            //called when complete
             $("#submit").prop('disabled', false);
          },
        success: function(data){
            //console.log(data);
            //console.log(data.ProtectedCardNumber);
            if(data.Result==0){
            // loader.hidePleaseWait();	
    
            //check_ccwhitelist(data);
            @if(in_array($post_data['MerchantKey'],SUNPASS_MERCHANTS_KEYS))
            check_ccblocklist(data);
            @else
            $("#card_number").text(data.ProtectedCardNumber);
            $("#name_on_card").text(data.NameonCard);
            $("#card_type").text(data.CardType);
            $("#tid").val(data.RecurringSaleTokenId);
            $("#success_section").show();
            $("#show_cenpos_section").hide();
            $("#show_cards_section").hide();
            
           // $("#show_cards_section").hide();
            @endif
            } else {
            $("#tid").val('');
            }
        },
        cancel: function(response){
            // loader.hidePleaseWait();
            $("#tid").val('');
            // swal(response.Message);	
            swal(response.Message);	
            if(response.Message!="Error validation Captcha"){
                swal(response.Message);	
                $("#success_section").hide();
                // $("#card_info").show();
            }
        }
    });
    $("#process_linked_card_form").submit(function(){
       var confim = confirm("Are you sure you want to make this payment?");

       if(confim){
        var form_data ={
        ccwu:$("#ccwu").val(),
        m:'{{$post_data["MerchantKey"]}}',
        r:'{{$post_data["reference_id"]}}',
        il:'l',
        // card_type:$("#c_name").val(),
        // card_number:$("#c_number").val(),
        // name_on_card:$("c_name").val(),
        }   
        $.ajax({
            type: "POST",
            url: '{{base_url("merchant_checkout/process_tokenization")}}',
            data: form_data,
            dataType: "json",
            beforeSend: function (response) {
                loader2.showPleaseWait();
                $("#process_linked_card").prop('disabled',true); 
                $("#process_linked_card").text("Processing... Please wait"); 
            },
            complete: function (response) {
                $("#process_linked_card").prop('disabled',false);
                $("#process_linked_card").text("Process Payment");
                loader2.hidePleaseWait();  
            },
            success: function (response) {
                // $("#process_linked_card").prop('disabled',false);
                if(response.success){
                   window.location.href=response.url;
                } else {
                $("#process_linked_card").prop("disabled",false);
                $("#process_linked_card").text("Process Payment");
                //$("#show_cards_section").hide();
                $("#ccwu").html("");
                loader2.hidePleaseWait();
                swal(response.msg);
                }
            },
            error: function (response) {
                $("#process_linked_card").prop("disabled",false);
                $("#process_linked_card").text("Process Payment");
                //$("#show_cards_section").hide();
                $("#ccwu").html("");
                loader2.hidePleaseWait();
                swal(response.msg);
            },
        });
       }


        return false;
    });
    //for cenpos process payment
    $("#payment_auth").click(function(e){

        var form_data ={
        //merchant:merchant,	
        tokenid:$("#tid").val(),
        card_type:$("#card_type").text(),
        card_number:$("#card_number").text(),
        name_on_card:$("#name_on_card").text(),
        m:'{{$post_data["MerchantKey"]}}',
        r:'{{$post_data["reference_id"]}}',
        il:'nl',
        }   
        $.ajax({
        url: '{{base_url("merchant_checkout/process_nontokenization")}}',
        type: 'POST',
        dataType: 'json',
        data: form_data,
        beforeSend:function(){
            loader2.showPleaseWait();
            $("#payment").prop("disabled",true);
            $("#payment").text("Processing... Please wait");
            //loader.showPleaseWait();
        },
        complete: function(xhr, textStatus) {
            $("#payment").prop("disabled",false);
            $("#payment").text("Process Payment");
            //loader.hidePleaseWait();
        },
        success: function(data, textStatus, xhr) {
            //console.log();
            if(data.success){
                //alert(data.msg);
                //go to callbackurlshit  urlencode(base64_encode('http://stackoverflow.com/questions/9585034'))
                //console.log('{{base_url("payment/successpage_redirect")}}/'+data.reference+'/success/card/{{urlencode(base64_encode($post_data["CallbackURL"]))}}');
                //alert('{{base64_encode($post_data["CallbackURL"])}}');
                // window.location.href='{{$post_data["CallbackURL"]}}/reference='+data.reference;
                // window.location.assign("https://www.example.com");
                //window.location.href='{{base_url("payment/successpage_redirect")}}/'+data.reference+'/success/card/{{urlencode(base64_encode($post_data["CallbackURL"]))}}';
                //window.location.href='{{$post_data["CallbackURL"]}}'+data.reference+'/success/card';
                window.location.href=data.url;
                // $("#card_section").hide();
                // $(".success_section_transaction").show();
                loader2.hidePleaseWait();
                // swal(data.msg);
            } else {
                $("#payment").prop("disabled",false);
                $("#payment").text("Process Payment");
                loader2.hidePleaseWait();
                swal(data.msg);
                $(".cancel_section").show();
                $(".payment_btn_section").hide();
                $("#tid").val('');
            }
        },
        error: function(xhr, textStatus, errorThrown) {
                $("#payment").prop("disabled",false);
                $("#payment").text("Process Payment");
                swal(data.msg);
                $(".cancel_section").show();
                $(".payment_btn_section").hide();
                $("#tid").val('');
        }
        });


    });
    $(document).on('click','.select_card',function(){
        $('.select_card').not(this).prop('checked', false);  
        $("#ccwu").val($(this).val());
    });
    $(document).on('click','.remove_card',function(){
        var confirm = window.confirm('Do you want to remove this card?');
        if(confirm){
            $.ajax({
                type: "POST",
                url: '{{base_url("merchant_checkout/remove_card")}}',
                data: {
                    m:'{{$post_data["MerchantKey"]}}',
                    _c:$(this).attr('_c'),
                },
                dataType: "json",
                success: function (response) {
                    if(response.success){
                        window.location.reload();
                    }
                }
            });
        }
    });   
     $(document).on('click','.remove_sdcard',function(){
        var confirm = window.confirm('Do you want to remove this?');
        if(confirm){
            $.ajax({
                type: "POST",
                url: '{{base_url("merchant_checkout/remove_sdcard")}}',
                data: {
                    m:'{{$post_data["MerchantKey"]}}',
                    _c:$(this).attr('_c'),
                },
                dataType: "json",
                success: function (response) {
                    if(response.success){
                        window.location.reload();
                    }
                }
            });
        }
    });     
     $(document).on('click','.verify_card',function(){
            var idx = atob($(this).attr('_c'));
            var params = "{{$post_data['MerchantKey']}}"+"||"+"{{$post_data['reference_id']}}"+"||"+"{{$post_data['merchant_customer_id']}}"+"||"+idx;
            //alert(params);
            var url = "{{base_url('checkout_card/verify_form/')}}"+btoa(params);
            console.table(url);
            window.location.href=url;
    });     
    $("#suncash_voucher_form").submit(function(){
    //  alert("x");
    var form_data ={
    voucher_mobile_number:$("#voucher_mobile_number").val(),
    voucher_number:$("#voucher_number").val(),
    voucher_pin:$("#voucher_pin").val(),
    m:'{{$post_data["MerchantKey"]}}',
    r:'{{$post_data["reference_id"]}}',
    }   
    $.ajax({
      url: '{{base_url("payment/process_voucher_payment")}}',
      type: 'POST',
      dataType: 'json',
      data: form_data,
      beforeSend:function(){
          loader2.showPleaseWait();
        $("#voucher_card_next").prop("disabled",true);
        $("#voucher_card_next").text("Processing... Please wait");
        //loader.showPleaseWait();
      },
      complete: function(xhr, textStatus) {
        $("#voucher_card_next").prop("disabled",false);
        $("#voucher_card_next").text("Process Payment");
        //loader.hidePleaseWait();
      },
      success: function(data, textStatus, xhr) {
        //console.log();
        if(data.success){
            window.location.href=data.url;
        } else {
            loader2.hidePleaseWait();
            $("#voucher_card_next").prop("disabled",false);
            $("#voucher_card_next").text("Process Payment");

            swal(data.msg);
        }
      },
      error: function(xhr, textStatus, errorThrown) {
           $("#voucher_card_next").prop("disabled",false);	   
           $("#voucher_card_next").text("Process Payment");

            loader2.hidePleaseWait();
            swal(data.msg);
      }
    });
  return false;
  });


$("#suncash_form").submit(function(e){
    $('.hb').remove();
    $('.form-div').removeClass('has-error').removeClass('has-success');

    //check validation
    var err_count = 0;
    var to_req=[];
    //jquery blank validation..
    $("#suncash_form .required").each(function(){
        var field_id = $(this).attr("id");
        var data=[];
        if($(this).val()==""){
          data['id']=field_id;      
          to_req.push(data);
          err_count++;
        }
    });

    if(err_count>0){
      swal(
      'Oops...',
      "Please do check required fields.",
      'error'
      );  
      $.each(to_req, function( index, value ) {
        $("#"+value.id).parent().after('<div class="hb"><div class="help-block" style="color:#f38989; font-size:7pt;margin-bottom:0px !important;">Required filed.</div></div>').addClass('has-error');
      });        
      return false;
    }
    var form_data = $(this).serializeArray();
    $.ajax({
      url: '{{base_url("payment/generate_passcode")}}',
      type: 'POST',
      dataType: 'json',
      data: form_data,
        beforeSend:function(xhr, textStatus) {
        //called when complete
        // loader.showPleaseWait();
        $("#card_next").prop('disabled', true);
      },
        complete: function(xhr, textStatus) {
        //called when complete
        // loader.hidePleaseWait();
        $("#card_next").prop('disabled', false);
      },
      success: function(data, textStatus, xhr) {
        //called when successful
        if(data.success){
            console.log(data.data);
            $(".suncash_verify_section").show();
            $(".suncash_login_section").hide();		    	
        } else {
            swal(data.msg);
        }
      },
      error: function(data, textStatus, errorThrown) {
        //called when there is an error
        
      }
    });
    



    return false;
});
$("#suncash_process_form").submit(function(e){
    $('.hb').remove();
    $('.form-div').removeClass('has-error').removeClass('has-success');

    //check validationhas-danger
    var err_count = 0;
    var to_req=[];
    //jquery blank validation..
    $("#suncash_process_form .required").each(function(){
        var field_id = $(this).attr("id");
        var data=[];
        if($(this).val()==""){
          data['id']=field_id;      
          to_req.push(data);
          err_count++;
        }
    });

    if(err_count>0){
      swal(
      'Oops...',
      "Please do check required fields.",
      'error'
      );  
      $.each(to_req, function( index, value ) {
        $("#"+value.id).parent().after('<div class="hb"><div class="help-block" style="color:#f38989; font-size:7pt;margin-bottom:0px !important;">Required filed.</div></div>').addClass('has-error');
      });        
      return false;
    }
    var form_data = {
        passcode:$("#passcode").val(),
        mobile:$("#mobile").val(),
        m:'{{$post_data["MerchantKey"]}}',
        r:'{{$post_data["reference_id"]}}',			
    };
    $.ajax({
      url: '{{base_url("payment/process_suncash_payment")}}',
      type: 'POST',
      dataType: 'json',
      data: form_data,
        beforeSend:function(xhr, textStatus) {
        //called when complete
        // loader.showPleaseWait();
        $("#card_submit").prop('disabled', true);
        $("#card_submit").text("processing... please wait");
      },
        complete: function(xhr, textStatus) {
        //called when complete
/*	         $("#card_submit").prop('disabled', false);
         $("#card_submit").text("Process Payment");*/
      },
      success: function(data, textStatus, xhr) {
        //called when successful
        if(data.success){
            //$(".suncash_verify_section").show();
            //$(".suncash_login_section").hide();
/*            	console.log('{{base_url("payment/successpage_redirect")}}/'+data.reference+'/success/wallet/{{urlencode(base64_encode($post_data["CallbackURL"]))}}');
            //alert('{{base64_encode($post_data["CallbackURL"])}}');
            // window.location.href='{{$post_data["CallbackURL"]}}/reference='+data.reference;
            // window.location.assign("https://www.example.com");
            window.location.href='{{base_url("payment/successpage_redirect")}}/'+data.reference+'/success/wallet/{{urlencode(base64_encode($post_data["CallbackURL"]))}}';*/
            window.location.href=data.url;
            
        } else {
            // loader.hidePleaseWait();
            $(".suncash_login_section").show();
            $(".suncash_verify_section").hide();
             $("#card_submit").prop('disabled', false);
             $("#card_submit").text("Process Payment");		    	
            swal(data.msg);
        }
      },
      error: function(data, textStatus, errorThrown) {
        //called when there is an error
        // loader.hidePleaseWait();
        $("#suncash_login_section").show();
        $(".suncash_verify_section").hide();
         $("#card_submit").prop('disabled', false);
         $("#card_submit").text("Process Payment");			    
        swal(data.msg);
      }
    });
    



    return false;
});

$("#sand_form").submit(function(e){
    $('.hb').remove();
    $('.form-div').removeClass('has-error').removeClass('has-success');

    //check validation
    var err_count = 0;
    var to_req=[];
    //jquery blank validation..
    $("#sand_form .required").each(function(){
        var field_id = $(this).attr("id");
        var data=[];
        if($(this).val()==""){
          data['id']=field_id;      
          to_req.push(data);
          err_count++;
        }
    });

    if(err_count>0){
      swal(
      'Oops...',
      "Please do check required fields.",
      'error'
      );  
      $.each(to_req, function( index, value ) {
        $("#"+value.id).parent().after('<div class="hb"><div class="help-block" style="color:#f38989; font-size:7pt;margin-bottom:0px !important;">Required filed.</div></div>').addClass('has-error');
      });        
      return false;
    }
    var form_data = $(this).serializeArray();
    $.ajax({
      url: '{{base_url("payment/sand_generate_passcode")}}',
      type: 'POST',
      dataType: 'json',
      data: form_data,
        beforeSend:function(xhr, textStatus) {
        //called when complete
        // loader.showPleaseWait();
        $("#sand_next").prop('disabled', true);
        $("#voucher_card_next").text("Processing... Please wait");
      },
        complete: function(xhr, textStatus) {
        //called when complete
        // loader.hidePleaseWait();
        $("#sand_next").prop('disabled', false);
        $("#voucher_card_next").text("Next");
      },
      success: function(data, textStatus, xhr) {
        //called when successful
        if(data.success){
            console.log(data.data);
            $(".sand_verify_section").show();
            $(".sand_login_section").hide();		    	
        } else {
            swal(data.msg);
            $("#sand_submit").text("Next");	
        }
      },
      error: function(data, textStatus, errorThrown) {
        //called when there is an error
        
      }
    });
        return false;
});
$("#sand_process_form").submit(function(e){
  // alert($("#sand_qr").val());
    // $('.hb').remove();
    // $('.form-div').removeClass('has-error').removeClass('has-success');

    // //check validationhas-danger
    // var err_count = 0;
    // var to_req=[];
    // //jquery blank validation..
    // $("#sand_process_form .required").each(function(){
    //     var field_id = $(this).attr("id");
    //     var data=[];
    //     if($(this).val()==""){
    //       data['id']=field_id;      
    //       to_req.push(data);
    //       err_count++;
    //     }
    // });

    // if(err_count>0){
    //   swal(
    //   'Oops...',
    //   "Please do check required fields.",
    //   'error'
    //   );  
    //   $.each(to_req, function( index, value ) {
    //     $("#"+value.id).parent().after('<div class="hb"><div class="help-block" style="color:#f38989; font-size:7pt;margin-bottom:0px !important;">Required filed.</div></div>').addClass('has-error');
    //   });        
    //   return false;
    // }


    var fullname;
    var fullname_sand = '{{$post_data["merchant_customer_fullname"]}}';
    var mobile;
    var mobile_sand = '{{$post_data["merchant_customer_mobile"]}}';   
    var email;
    var email_sand = '{{$post_data["merchant_customer_mobile"]}}';       
    if(fullname_sand=""){
      fullname =$post_data["merchant_customer_fullname"];
    }else{
      fullname =$("#sand_fullname").val();
    }
    if(mobile_sand=""){
      mobile =$post_data["merchant_customer_mobile"];
    }else{
      mobile =$("#sand_mobile").val();
    }   
    if(email_sand=""){
      email =$post_data["merchant_customer_email"];
    }else{
      email =$("#sand_email").val();
    }       
    var form_data = {
        otp:$("#sand_otp").val(),
        passcode:$("#sand_pin").val(),
        // mobile:$("#sand_mobile").val(),
        mobile:mobile,
        email:email,        
        fullname:fullname,
        sand_qr:$("#sand_qr").val(),
        // email:$("#sand_email").val(),
        m:'{{$post_data["MerchantKey"]}}',
        r:'{{$post_data["reference_id"]}}',			
    };
    $.ajax({
      url: '{{base_url("payment/process_sanddollar_checkout")}}',
      type: 'POST',
      dataType: 'json',
      data: form_data,
        beforeSend:function(xhr, textStatus) {
        //called when complete
        // loader.showPleaseWait();
        $("#sand_process_payment").prop('disabled', true);
        $("#sand_process_payment").text("processing... please wait");
      },
        complete: function(xhr, textStatus) {
        //called when complete
       $("#sand_process_payment").prop('disabled', false);
       $("#sand_process_payment").text("Process Payment");
      },
      success: function(data, textStatus, xhr) {
        //called when successful
        if(data.success){ 
            var sdf_as = '{{$sd_settings["allow_multiple_sandaccount"]}}';
            var sdf_msd = '{{$sd_settings["allow_autosaving_sanddaccount"]}}';
            var IsSdcardempty = '{{$IsSdcardempty}}';
            if(sdf_msd==1){//autosave
              if(IsSdcardempty==0 && sdf_as ==0){ //wit date and not multiple
                 window.location.href=data.url;
              }else{
                  var form_data = {
                    m:'{{$post_data["MerchantKey"]}}',
                    sand_fullname:fullname,
                    sand_mobile:mobile,
                    sand_email:email,
                    sand_card:$("#sand_qr").val(),
                    nickname:$("#sand_nickname").val(),       
                    sdf_as:sdf_as,
                    merchant_customer_id:'{{$post_data["merchant_customer_id"]}}'
                  };
                  $.ajax({
                    url: '{{base_url("payment/saveBusinessSandDollarContact")}}',
                    type: 'POST',
                    dataType: 'json',
                    data: form_data,
                    beforeSend:function(xhr, textStatus) {
                      $("#sand_process_payment").prop('disabled', true);
                      $("#sand_process_payment").text("processing... please wait");
                    },
                    complete: function(xhr, textStatus) {
                      $("#sand_process_payment").prop('disabled', false);
                      $("#sand_process_payment").text("Process Payment");                  
                    },
                    success: function(data2, textStatus, xhr) {
                      //console.log(data.url);
                      if(data2.success){
                          swal(data2.msg);
            
                      } else {		    	
                          swal(data2.msg);
                    
                      }
                      window.location.href=data.url;
                    },
                    error: function(data2, textStatus, errorThrown) {	  
                      $("#sand_process_payment").prop('disabled', false);
                      $("#sand_process_payment").text("Process Payment");		                  
                      swal(data2.msg);
                    }
                  });
              }
            } else{
              if(IsSdcardempty==0 && sdf_as ==0){ //wit date and not multiple
                window.location.href=data.url;
              }else{
                  var r = confirm("Do you want to save the SandDollar QRCode?");
                  if(r==true){
                    var form_data = {
                      m:'{{$post_data["MerchantKey"]}}',
                      sand_fullname:fullname,
                      sand_mobile:mobile,
                      sand_email:email,
                      sand_card:$("#sand_qr").val(),
                      nickname:$("#sand_nickname").val(),   
                      merchant_customer_id:'{{$post_data["merchant_customer_id"]}}'
                    };
                    $.ajax({
                      url: '{{base_url("payment/saveBusinessSandDollarContact")}}',
                      type: 'POST',
                      dataType: 'json',
                      data: form_data,
                      beforeSend:function(xhr, textStatus) {
                        $("#sand_process_payment").prop('disabled', true);
                        $("#sand_process_payment").text("processing... please wait");
                      },
                      complete: function(xhr, textStatus) {
                        $("#sand_process_payment").prop('disabled', false);
                        $("#sand_process_payment").text("Process Payment");                  
                      },
                      success: function(data2, textStatus, xhr) {
                        //console.log(data.url);
                        if(data2.success){
                            swal(data2.msg);
              
                        } else {		    	
                            swal(data2.msg);
                        }
                        window.location.href=data.url;
                      },
                      error: function(data2, textStatus, errorThrown) {	  
                        $("#sand_process_payment").prop('disabled', false);
                        $("#sand_process_payment").text("Process Payment");		                  
                        swal(data2.msg);
                      }
                    });
                  } else {
                    window.location.href=data.url;
                  }
              }
                
                  
            }
            
        } else {
            // loader.hidePleaseWait();
            $(".sand_login_section").show();
            // $(".sand_verify_section").hide();
            $("#sand_process_payment").prop('disabled', false);
            $("#sand_process_payment").text("Process Payment");		    	
           	swal(data.msg.type);
        }
      },
      error: function(data, textStatus, errorThrown) {
        //called when there is an error
        // loader.hidePleaseWait();
        $("#sand_login_section").show();
        // $(".sand_verify_section").hide();
         $("#sand_process_payment").prop('disabled', false);
         $("#sand_process_payment").text("Process Payment");			    
        swal(data.msg);
      }
    });
    



    return false;
});



	paypal.Buttons({
		style:{
			size: 'responsive',
			label: 'checkout',
		},
		createOrder:function(data,actions){
		return actions.order.create({
			"purchase_units": [
			{
				"amount": {
				"currency_code": "USD",
				"value":$("#aval").val()
				}
			}]

		});
		},

		onApprove: function(data, actions) {
		loader2.showPleaseWait();
		// This function captures the funds from the transaction.
		return actions.order.capture().then(function(details) {
		loader2.hidePleaseWait();
			console.log(details);
			// This function shows a transaction success message to your buyer.
			//alert('Transaction completed by ' + details.payer.name.given_name);
			// alert($("#paypal_mobile").val()+" "+$("#paypal_mobile").val());
			var form_data={
			'fullname':details.purchase_units[0].shipping.name.full_name,
			'mobile' :$("#paypal_mobile").val(),
			// 'email':$("#paypal_email").val(),
			'email':details.purchase_units[0].payee.email_address,
			'm':'{{$post_data["MerchantKey"]}}',
			'r':'{{$post_data["reference_id"]}}',
			'paypal_reference_number':details.purchase_units[0].payments.captures[0].id,
			'status':details.status,
			'response':details,
			'total_due':details.purchase_units[0].payments.captures[0].amount.value,
			};
			//console.log(form_data);
			$.ajax({
			url: '{{base_url("merchant_checkout/process_paypal_checkout")}}',
			type: 'POST',
			dataType: 'json',
			data: form_data,
				beforeSend:function(xhr, textStatus) {
				//called when complete
				loader2.showPleaseWait();
				$("#card_next").prop('disabled', true);
			},
				complete: function(xhr, textStatus) {
				//called when complete
				loader2.hidePleaseWait();
				$("#card_next").prop('disabled', false);
			},
			success: function(data, textStatus, xhr) {
				//called when successful
				if(data.success){
					// console.log(data.data);
					// loader2.hidePleaseWait();//remove to refrain customer to refresh page
					window.location.href=data.url; 	
				} else {
					loader2.hidePleaseWait();
					swal(data.msg);
				}
			},
			error: function(data, textStatus, errorThrown) {
				//called when there is an error
				loader2.hidePleaseWait();
				swal(data.msg);
			}
			});

		});
		}
	}).render('#paypal-button-container');

  $(document).on('click','.sdc',function(){
    $("#sand_qr").val($(this).attr('sdcd'));
    $("#sand_fullname").val($(this).attr('sdn'));
    $("#sand_nickname").val($(this).attr('sdnm'));
    $("#sand_mobile").val($(this).attr('sdm'));
    $("#sand_email").val($(this).attr('sde'));
    $(".QRCode").val($(this).attr('sdcd'));

    var fullname = $(this).attr("sdn");
    var mobile = $(this).attr("sdm");
    var email = $(this).attr("sde"); 
    var nickname = $(this).attr("sdnm"); 
    $(".sand_details").show();       
		if(fullname==""){
			$(".sand_details_fullname").show();
			$("sand_fullname").addClass("required");
		}else{
			$(".sand_details_fullname").hide();
			$("sand_fullname").removeClass("required");
		}
		if(mobile==""){
			$(".sand_details_mobile").show();
			$("sand_mobile").addClass("required");
		}else{
			$(".sand_details_mobile").hide();
			$("sand_mobile").removeClass("required");
		}
		if(email==""){
			$(".sand_details_email").show();
		}else{
			$(".sand_details_email").hide();
			$("sand_email").removeClass("required");
		}                                 
		if(nickname==""){
			$(".sand_details_nickname").show();
		}else{
			$(".sand_details_nickname").hide();
		}        
  });
  
 

  </script>

