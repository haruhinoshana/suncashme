<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>SunCash.Me</title>
	<link rel="stylesheet" href="{{base_url('assets_main/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/main.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/responsive.css')}}">
</head>
<body>
	<nav class="navbar navbar-expand-lg sticky-top bg-white shadow-sm p-0">
	  <div class="container p-0" role="navigation">
	  	<a class="navbar-brand" href="{{base_url('')}}"><img src="{{base_url('assets_main/imgs/header-logo@2x.png')}}" alt="Suncash Logo" class="d-inline-block align-top" width="200"></a>
	  	<a href="#" class="menu-button"><span></span></a>
	  	<div class="justify-content-end main-menu">
				<ul class="navbar-nav d-flex">
		      <li class="nav-item">
		        <a class="nav-link" href="{{base_url('')}}">About</a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link " href="{{base_url('info/faq')}}">FAQ </a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="{{base_url('info/terms')}}">Terms</a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link active" href="{{base_url('info/contacts')}}">Contact Us</a>
		      </li>
		      <li class="nav-item button-item">
		        <a class="nav-link btn-link btn-primary" href="http://dev.suncash.me/business/">For Business</a>
		      </li>
		      <li class="nav-item button-item">
		        <a class="nav-link btn-link btn-secondary" href="http://dev.suncash.me/customer/">For Customers</a>
		      </li>
		  </div>
	  </div>
	</nav>

	<section>
		<div class="container">
			<div class="row">
				<div class="page-title">
					<h2>Contact</h2>
					<hr>
				</div>
			</div>
		</div>
	</section>

	<section class="content-block">
		<div class="container p-0">
			<div class="row">
				<div class="col large-text">
					<p>Got a question?  We’d love to hear from you.</p>
					<p>Send us a message and we’ll respond as soon as possible!</p>
				</div>
			</div>

			<form action="" class="suncash-form mb-5">
				<div class="form-row">
					<div class="col-12 col-lg-6 form-group">
						<input class="form-control" id="first-name" name="first-name" placeholder="First Name" type="text" required="" autofocus="">
					</div>
					<div class="col-12 col-lg-6 form-group">
						<input class="form-control" id="last-name" name="last-name" placeholder="Last Name" type="text" required="" autofocus="">
					</div>
				</div>
				<div class="form-row">
					<div class="col-12 col-lg-6 form-group">
						<input class="form-control" id="company-name" name="company-name" placeholder="Company Name" type="text" required="" autofocus="">
					</div>
					<div class="col-12 col-lg-6 form-group">
						<input class="form-control" id="work-email" name="work-email" placeholder="Work Email" type="email" required="" autofocus="">
					</div>
				</div>
				<div class="form-row">
					<div class="col-12 form-group">
						<textarea class="form-control" rows="5" placeholder="Message or Comment"></textarea>
					</div>
				</div>
				<div class="form-row">
					<div class="col-12 col-lg-5 offset-lg-7">
						<a href="#" class="btn btn-primary full">send message</a>
					</div>
				</div>
			</form>

			<div class="row">
				<div class="col large-text">Our Contact Details:</div>
			</div>

			<div class="row mb-5">
				<div class="col-12 col-lg-4 mb-3">
					<div class="label">Address</div>
					<div class="data">#4 Patton Street, Palmdale PO Box N10620, Nassau, The Bahamas</div>
				</div>

				<div class="col-12 col-lg-4 mb-3">
					<div class="label">Email/Support</div>
					<div class="data">info@mysuncash.com</div>
				</div>

				<div class="col-12 col-lg-4 mb-3">
					<div class="label">Telephone Number</div>
					<div class="data">(242) 393-4778 or</div>
					<div class="data">Toll Free (242) 300-4SUN</div>
				</div>
			</div>
		</div>

		
	</section>

	<section class="cta-banner">
		<div class="container text-center">
			<div class="row">
				<div class="col-12">
					<div class="banner-text-container">
						<h1 class="banner-header">Start using SunCash.Me today.</h1>
						<p>SunCash.Me is the fastest, safest and simple way for your family, friends or customers to pay.</p>
						<a href="#" class="btn btn-secondary">Create Your SunCash.Me Link</a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<footer class="py-3">
		<div class="container">
			<div class="row">
				<div class="col-12 col-lg-6 d-flex align-self-center text-left">
					<span>© 2016 SunCash.Me. All Rights Reserved.</span>
				</div>
				<div class="col-12 col-lg-6 text-right">
					<img src="{{base_url('assets_main/imgs/footer-logo.png')}}">
				</div>
			</div>
		</div>
	</footer>

	<script src="{{base_url('assets_main/js/jquery-3.2.1.min.js')}}"></script>
	<script src="{{base_url('assets_main/js/bootstrap.min.js')}}"></script>
	<script src="{{base_url('assets_main/js/main.js')}}"></script>
</body>
</html>