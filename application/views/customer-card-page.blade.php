<?php
//loading assets helper
defined('BASEPATH') OR exit('No direct script access allowed');
$assetHelper = new AssetHelper\AssetHelper();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>SunCash.Me Card Test</title>
	<link rel="stylesheet" href="{{base_url('assets_main/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/main.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/responsive.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/all.min.css')}}">
	<link rel="stylesheet" href="{{base_url('assets/plugins/global/global2.css')}}">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>assets/plugins/icon.ico">
	<style type="text/css">
		@media only screen and (max-width: 768px)  {
				.img-x {
				  display: block;
				  margin-left: auto;
				  margin-right: auto;
				  //width: 100%;
				  min-width:128px;
				  min-height:43px;
				}
				.txt-shit{
					text-align:center !important;
				}
		}		
	</style>	
</head>
<body>
	<nav class="navbar navbar-expand-lg sticky-top bg-white shadow-sm p-0">
	  <div class="container p-0" role="navigation">
	  	<a class="navbar-brand" href="{{base_url('payment/wallet')}}"><img src="{{base_url('assets_main/imgs/header-logo@2x.png')}}" alt="Suncash Logo" class="d-inline-block align-top" width="160"></a>
	  	<a href="#" class="menu-button"></a>
	  	<div class="justify-content-end main-menu">
				<ul class="navbar-nav d-flex align-items-center">

					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle main-nav" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<span><?=ucwords(($_SESSION['customer_info_cc']['CustomerName'])) ?></span>
								@if(!empty($suntag_data['profile_pic']))
								<img src="<?=($_SESSION['customer_info_cc']['ImageURL']) ?>" alt="profile image" class="profile-image">
								
								@else
								<img src="{{base_url('assets/img/media/td.png')}}"" alt="profile image" class="profile-image">
								<!-- <img src="{{$suntag_data['profile_pic']}}" alt="profile image" class="profile-image" > -->
								@endif
						</a>
					</li>

		      <li class="nav-item btn-logout">
		        <a class="nav-link" href="{{base_url('wallet/logout_cc')}}">Logout</a>
		      </li>
		    </ul>
		  </div>
	  </div>
	</nav>

	<section class="" id="wallet_section" >
		<div class="container  text-center">
			<div class="row d-flex justify-content-center align-items-center mb-5">
				<div class="user-card transaction-details" style="margin-top:1rem;margin-bottom: -34px !important;">
					<div class="header w-content" style="height:6rem !important;"></div>
						<div class="body">
							<form id="cc_walletaccount" method="POST">
							<div class="user-image ">
								@if(!empty($_SESSION['profile_pic']))
							
								<img src="{{$_SESSION['profile_pic']}}">
								@else
								<img src="{{base_url('assets_main/imgs/suncash-icon.png')}}">
								@endif
							</div>

							<div class="details" style="margin-bottom: -40px;">
								<div class="message">You're about to pay <?=ucfirst($_SESSION['suntag_shortcode']) ?></div>
								<select name="payment_method" id="payment_method" >
									<option value="">BSD</option>
								</select>
								<input type="text" id="amount" name="amount" value="{{$payment_data['amount']}}" >
							</div>

							<div class="card_section">
								@if(!empty($cc_data))
									<hr>
									<div class="form-group">
										<H4>Choose linked Card</H4>
										<div class="form-div">
											<button  type="button" class="btn btn-sm btn-orange link_card" id="link_card" style="padding: 0rem 0rem !important;min-width: 0rem !important; margin-top:3px !important">Link Card</button>
											<div class="table-responsive">
												<table class="table table-bordered table-hover table-sm" id="trans_tbl" width='100%'>
													<thead>
													<tr>
														<th>Select</th>
														<th>Name</th>
														<th>Type</th>
														<th>last 4 digit</th>
														<th>Verified</th>
														<th>Action</th>
													</tr>
													</thead>
													<tbody id="trans_tr">
													@if(!empty($cc_data))
														@foreach($cc_data as $cc_val)
															<tr>
																@if($cc_val['is_pending'] ==0 &&  $cc_val['status']==0 && $cc_val['is_rejected']==0 && $cc_val['is_verified']==1)
																	<td scope="row"><input type="radio" class="select_card required" name="select_card" id="select_card" value='{{base64_encode($cc_val['token_id'])}}' n="{{$cc_val['cardholder_name']}}" ct="{{$cc_val['card_type']}}" cn="{{str_pad($cc_val['card_last_four_digits'], 12, "*", STR_PAD_LEFT) }}" ></td>
																@else
																	<td></td>
																@endif
																<td id="card_name">{{$cc_val['cardholder_name']}}</td>
																<td id="card_type">{{$cc_val['card_type']}}</td>
																<td id="card_num" >{{str_pad($cc_val['card_last_four_digits'], 12, "*", STR_PAD_LEFT) }}</td>
																<td>
																	@if($cc_val['is_verified']=='1')
																		<i class="fa fa-check-circle" aria-hidden="true"></i>
																	@else
																		<i class="fa fa-times-circle" aria-hidden="true"></i>
																	@endif
																</td>
																<td>
																<button  type="button" class="btn btn-sm btn-orange delete_card" id="delete_card" cid="{{$cc_val['id']}}" style="padding: 0rem 0rem !important;min-width: 0rem !important; margin-top:3px !important">Unlink</button>
																@if($cc_val['is_verified']=='0')
																<button type="button" class="btn btn-sm btn-orange upload_req" id="upload_req" cid="{{$cc_val['id']}}" v="{{bin2hex($cc_val['otp'])}}" style="padding: 0rem 0rem !important;min-width: 1rem !important; margin-top:3px !important">Verify</button>
																@endif
																</td>
															</tr>
														@endforeach
													@else

													@endif	
													</tbody>     
												</table>
											</div>
										</div>
									</div>
								@else
								<button  type="button" class="btn btn-sm btn-orange link_card" id="link_card" style="padding: 0rem 0rem !important;min-width: 0rem !important; margin-top:3px !important">Link Card</button>
									<p style="color:red;font-size:18px;">Must save/link your card first.</p>
								@endif
							</div>
							<div class="payment_section" style="display:block;">

								<div class="text-left">
									<div class="item">
										<input type="hidden" class="form-control" id="reference_num" name="reference_num"  value="{{$payment_data['reference_num']}}" placeholder="Reference # (Order, Quote, Invoice or Account)">
										<input type="hidden" class="form-control" id="notes" name="notes" placeholder="Note to Business" value="{{$payment_data['notes']}}">
										<input type="hidden" class="form-control" id="name" name="name" placeholder="Name" autocomplete="off" style="resize:none" value="{{$payment_data['name']}}" >
										<input type="hidden" class="form-control" id="email" name="email" placeholder="Email" autocomplete="off" style="resize:none" value="{{$payment_data['email']}}" >
										<input type="hidden" class="form-control" id="mobile" name="mobile" placeholder="mobile" autocomplete="off" style="resize:none" value="{{$payment_data['mobile']}}" >							  	
									</div>
								</div>


									<!-- No display of feees for customer suntag 	 -->
									
									<div class="item primary-border ">

										<div class="label">Transaction Details:</div>

										
										<div class="text-left">
											<div class="row">
												<div class="col">Principal</div>
												<div class="col text-right ">$ <span class="amount_val">{{$payment_data['amount']}}</span></div>
											</div>
											@if($_SESSION['tag']=='MERCHANT')		
											<div class="row">
												<div class="col">Transaction Fee</div>
												<div class="col text-right ">$ <span class="fee_val">{{number_format($fee_data['fee'], 2, '.', '')}}</span></div>
											</div>
											@endif
											<div class="row">
												<div class="col">Convenience Fee</div>
												<div class="col text-right">$ <span class="conveniencefee">{{number_format($ccfee, 2, '.', '')}}</span></div>
											</div>
										</div>
									</div>
									
									<div class="item total">
									  <div class="row">
									  	<?php 
									  		$total = str_replace( ',', '', $payment_data['amount'])+$fee_data['fee']+$fee_data['vat_charge'];
									  	?>
									  	<div class="col label">Total Due</div>
									  	<div class="col text-right amount"><span class="amount_total">{{$total}}</span> BSD</div>
									  </div>
									</div>
									




								</div>

								<!-- <input type="hidden" name="amount_total" id="amount_total" value="{{number_format($payment_data['amount'], 2, '.', '')}}"> -->
									
								<button type="submit" id="process_payment"  class="btn btn-orange full btn-process" data-loading-text="Loading...">Pay</button>

							</form>						
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<footer class="py-2 footer">
			<div class="container">
				<div class="row">
					<div class="col-12 col-lg-6">
						<span class="txt-shit">© {{date('Y')}} SunCash.Me. All Rights Reserved.</span>
					</div>
					<div class="col-12 col-lg-6 text-right">
						<img class="img-x" src="{{base_url('assets_main/imgs/footer-logo.png')}}">
					</div>
				</div>
			</div>
		</footer>		
	</section>
	<section class="full" id="success_section_transaction" style=" display:none ;">
		<div class="container  text-center">
			<div class="row d-flex justify-content-center align-items-center mb-5">
				<div class="user-card success-card" style="margin-top:1rem;margin-bottom:60px !important;">
					<div class="header w-content" style="margin-top:1rem;margin-bottom:-34px !important;"></div>
					<div class="body">
						<div class="user-image">
							<img src="{{base_url('assets_main/imgs/success-icon.png')}}">
						</div>
						<div class="details" id = "transaction-details">
							<div class="message" style="font-size: 2rem;line-height: 2.1rem;width: 90%;margin: 0 auto;">You Paid <span class="amount_val" style="color: #FF8400;">${{$payment_data['amount']}}</span> to <?=ucfirst($_SESSION['suntag_shortcode'])?></div>
						</div>
						<!-- <form action="" class="text-left"> -->
						<div class="text-left">

					  	<div class="label text-center">Transaction Details</div>
						  <div class="item">
							  <div class="row">
							  	<div class="col">Transaction ID: <span id="transaction_code"></span></div>
							  	<div class="col text-right">{{date("d M Y")}}</div>
							  </div>
							</div>
							<div class="item">
							  <div class="row">
							  	<div class="col">Payment Method</div>
							  	<div class="col text-right">Debit/Credit Card Payment</div>
							  </div>
							</div>
							<div class="item">
							  <div class="row">
							  	<div class="col">Principal</div>
							  	<div class="col text-right">$ <span class="amount_val">{{$payment_data['amount']}}</span></div>
							  </div>
							</div>	
							@if($_SESSION['tag']=='MERCHANT' && $_SESSION['registration_type']!='Charity')															
							<div class="item">
							  <div class="row">
							  	<div class="col">Transaction Fee</div>
							  	<div class="col text-right">$ <span class="bfee">{{number_format($fee_data['fee'], 2, '.', '')}}</span></div>
							  </div>
							</div>				
							@endif			
			<!-- 							<div class="item">
							  <div class="row">
							  	<div class="col">Vat</div>
							  	<div class="col text-right">$ <span class="vat_val">{{$fee_data['vat_charge']}}</div>
							  </div>
							</div> -->
							<div class="item">
							  <div class="row">
							  	<div class="col">Convenience Fee</div>
							  	<div class="col text-right">$ <span class="conveniencefee">0.00</span></div>
							  </div>
							</div>								
						  <div class="item total">
							  <div class="row">
							  	<div class="col label">Total Due</div>
							  	<div class="col text-right amount"><span class="amount_total">{{$total}}</span> BSD</div>
							  	<input type="hidden" id="amount_total" name="amount_total" value="{{$total}}" />
							  	<input type="hidden" id="hid_fee" name="hid_fee" value="{{number_format($fee_data['fee'], 2, '.', '')}}" />
							  	<input type="hidden" id="hid_vat" name="hid_vat" value="{{$fee_data['vat_charge']}}" />
							  	<input type="hidden" id="hid_pfee" name="hid_pfee" value="0.00" />
							  	<input type="hidden" id="hid_tfee" name="hid_tfee" value="0.00" />
							  	<input type="hidden" id="hid_totalfee" name="hid_totalfee" value="0.00" />						  	
							  </div>
						  </div>
						  
						</div>
						<!-- </form> -->

					</div>
				</div>
			</div>
		</div>
	</section>	
	<section id="link_section" style=" display:none ;">
		<div class="container  text-center">
			<div class="row d-flex justify-content-center align-items-center mb-5">
				<div class="user-card transaction-details" style="margin-top:1rem;margin-bottom: -34px !important;">
					<div class="header w-content" style="height:6rem !important;"></div>
						<div class="body">
							<div class="user-image ">
								@if(!empty($_SESSION['profile_pic']))
								<img src="{{$_SESSION['profile_pic']}}">
								@else
								<img src="{{base_url('assets_main/imgs/suncash-icon.png')}}">
								@endif
							</div>

							<div id="card_info" > 
								<div style="border-top: 1px solid #F0F0F0;"><br>
									<h3>Enter your card details</h3>
									<div>
										
										<form action="" class="text-left">
										<div id="NewCenposPlugin">
										</div>
										</form>
									</div>
								</div>

								<div class="form-footer">
									<div class="row">
										<div class="col">
											<button class="btn btn-orange full btn-block " id="show_card" >Link Debit/Credit Card</button>
											<input type="hidden" class="form-control" id="amount_link" name="amount_link" value="0.00" >
										</div>
									</div>
								</div>
							</div>
							<div id="success_link_section" style="display:none;">
								<hr>
								<div class="label text-center">Card Details</div>

								<div class="item">
								<div class="row">
									<div class="col text-left text-details">Card Number: </div>
									<div class="col text-right"><span id="card_number_link"></span></div>
								</div>
								</div>
								<div class="item">
								<div class="row">
									<div class="col text-left text-details">Name on Card: </div>
									<div class="col text-right"><span id="name_on_card_link"></span></div>
								</div>
								</div>
								<div class="item total">
								<div class="row">
									<div class="col text-left text-details">Card Type: </div>
									<div class="col text-right amount"><span id="card_type_link"></span> </div>
								</div>
								</div>

								<input type="hidden" name="tid" id ="tid" value="">
								<div class="payment_btn_section">
									<div class="text-center"><button type="button" class="btn btn-orange full btn-block" id="process_card_link">Link Debit/Credit Card</button></div>
								</div>
								<div class="cancel_link_section" style="display:none;">
									<div class="row">
										<!-- <div class="col text-center"><button type="button" class="btn btn-primary full" id="cancel_payment">Cancel</button></div>	 -->
										<!-- <a href="{{base_url("customer/link_card_list")}}"><button class="btn btn-light">Use Another Card</button></a>						 -->
									</div>	
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="verify_section" style=" display:none ;">
			<div class="container  text-center">
			<div class="row d-flex justify-content-center align-items-center mb-5">
				<div class="user-card transaction-details" style="margin-top:1rem;margin-bottom: -34px !important;">
					<div class="header w-content" style="height:6rem !important;"></div>
						<div class="body">
								<form name="verify_form" id="verify_form" class="form-horizontal" method="POST" enctype="enctype/form-data">
									<div class="user-image ">
										@if(!empty($_SESSION['profile_pic']))
									
										<img src="{{$_SESSION['profile_pic']}}">
										@else
										<img src="{{base_url('assets_main/imgs/suncash-icon.png')}}">
										@endif
									</div>
									<p>This step will verify your card and confirm that the card is valid and that you are the card owner.  The name on card must match the SunCash account holder’s name.</p>

									<p>Send in a selfie (picture of yourself, showing your entire face) holding your card, covering the card number but showing the last 4 digits, the name and expiry date to kyc@mysuncash.com.   If you cannot make out the information on the card, please also send another photo of the card itself. Make sure the photo is clear.</p>

									<p>We will send you the credit card verification number once approved.</p>

								
									<img src="http://dev.suncash.me/customer/assets/imgs/card_verify_pic.png" width='90%'>
									<div class="col-12">
										<div class="message"><b>Verification Code</b></div>
										<input type="text" class="form-control" id="verification" name="verification" placeholder="Enter Code" />
									</div><!-- /.col -->

				

							
									<input type="hidden" id="c" name="c" name="c" value=''/>
									<input type="hidden" id="v" name="v" name="v" value=''/>
									<button type="submit" name="submit" class="btn btn-orange full submit">Verify</button>
									<button type="button" name="manual_upload" class="btn btn-orange full manual_upload">Upload a Selfie & Id</button></button>
									<button type="button" name="back_main" class="btn btn-orange full submit back_main">Back</button>

								</form>


						</div>
					</div>
				</div>
			</div>
			</div>
		
	</section>
	<section id="upload_verify_section" style=" display:none ;">
			<div class="container  text-center">
			<div class="row d-flex justify-content-center align-items-center mb-5">
				<div class="user-card transaction-details" style="margin-top:1rem;margin-bottom: -34px !important;">
					<div class="header w-content" style="height:6rem !important;"></div>
						<div class="body">
								<form name="verify_upload_form" id="verify_upload_form" class="form-horizontal" method="POST" enctype="enctype/form-data">
									<div class="user-image ">
										@if(!empty($_SESSION['profile_pic']))
									
										<img src="{{$_SESSION['profile_pic']}}">
										@else
										<img src="{{base_url('assets_main/imgs/suncash-icon.png')}}">
										@endif
									</div>

									<legend>Last step: take a selfie holding card</legend>
									<p class='text-left'>
									For your safety, we must verify the card and confirm that you are the card owner.<br>

									<b>Please note:</b><br>

									 - Show only the last 4 digit of your card covering the other digits. <br>
									 - Cardholder name and expiry date must also be visible.<br>
									 - Your face should be entrirely visible.<br>
									 - The name on the card must match the name on your SunCash Account.<br>
									</p>
									<hr>
									<div class="upload-container">
									 	<div class="form-group mb-5">
									    <label>Upload a Scan of your ID</label>
									    <div class="info"><i class="fas fa-exclamation-circle"></i> Please upload a Government issued ID such as  Passport, current Driver’s License, or National ID Card.</div>

									    <div class="row">
									    	<div class="col-12 col-lg-6">
													<input type="file" class="my-pond" name="filepond1" id="card_id_upload" accept="image/jpeg, image/png" />
									    	</div>
									    	<div class="col-12 col-lg-6">
													<div class="illustration-image text-center">
														<div>
															<img src="{{base_url()}}assets_checkout/imgs/card-id.png" alt="">
															<span class="d-block">Example</span>
														</div>
													</div>
									    	</div>
									    </div>
									  </div>
									  <div class="form-group mb-5">
									    <label>Upload a Scan of your Debit/Credit Card</label>
									    <div class="info"><i class="fas fa-exclamation-circle"></i> Please upload the debit/credit card you used with a clear view of your last 4 digit card number, name and expiry date</div>
									    <div class="row">
									    	<div class="col-12 col-lg-6">
													<input type="file" class="my-pond" name="filepond2" id="credit_card_upload"/>
									    	</div>
									    	<div class="col-12 col-lg-6">
													<div class="illustration-image text-center">
														<div>
															<img src="{{base_url()}}assets_checkout/imgs/debit-card.png" alt="">
															<span class="d-block">Example</span>
														</div>
													</div>
									    	</div>
									    </div>
									  </div>
									</div>
				

							
									<!-- <input type="hidden" id="c" name="c" value=''/>
									<input type="hidden" id="v" name="v" value=''/> -->
									<button type="button" name="submit_upload" id="submit_upload" class="btn btn-orange full submit_upload">Upload</button>
									<button type="button" name="back_verify"  class="btn btn-orange full submit back_verify">Back</button>

								</form>


						</div>
					</div>
				</div>
			</div>
			</div>
		
	</section>

	<section class="full" id="reload_section" style=" display:none ;">
		<div class="container  text-center">
			<div class="row d-flex justify-content-center align-items-center mb-5">
				<div class="user-card message-card reload-card">
					<div class="header w-content"></div>
					<div class="body">
						<div class="user-image">
							<img src="{{base_url('assets_main/imgs/reload-icon.png')}}">
						</div>
						<div class="details">
							<div class="message error_msg"></div>
						</div>
						<div class="text-center border-top msg_area">


						</div>
						 <a href="{{base_url('payment/wallet')}}" class="btn btn-orange full">Back</a>
					</div>
				</div>
			</div>
		</div>

	</section>


	<script src="{{base_url('assets_main/js/jquery-3.2.1.min.js')}}"></script>
	<script src="{{base_url('assets_main/js/bootstrap.min.js')}}"></script>
	<script src="{{base_url('assets_main/js/main.js')}}"></script>
    <script src="{{base_url('assets_main/js/select.min.js')}}"></script>
	{!!$assetHelper->link_plugins('js','jquery-1.11.3.min.js')!!}
	{!!$assetHelper->link_plugins('js','bootstrap-3.3.7/js/bootstrap.min.js')!!}
	{!!$assetHelper->link_plugins('js','sidebar-nav/dist/sidebar-nav.min.js')!!}
	{!!$assetHelper->link_plugins('js','override/jquery.slimscroll.js')!!}
	{!!$assetHelper->link_plugins('js','override/waves.js')!!}
	{!!$assetHelper->link_plugins('js','override/custom.min.js')!!}
	{!!$assetHelper->link_plugins('js','sweetalert2-master/dist/sweetalert2.all.min.js')!!}
	{!!$assetHelper->link_plugins('js','bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js')!!}
	{!!$assetHelper->link_plugins('js','curioussolutions-datetimepicker/dist/DateTimePicker.min.js')!!}
	{!!$assetHelper->link_plugins('js','Inputmask-5.x/dist/jquery.inputmask.min.js')!!}
	{!!$assetHelper->link_plugins('js','global/global.js')!!}
	<script src="https://www.cenpos.com/Plugins/porthole.min.js"></script>
<script src="https://www.cenpos.com/Plugins/jquery.simplewebpay.js"></script> 
	<script  type="text/javascript" charset="utf-8">
	//validate amount when 1st load.

	$("#amount").inputmask({ 'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true, 'digits': 2, 'digitsOptional': false, 'placeholder': '0.00', rightAlign : false,clearMaskOnLostFocus: !1,min:0});

	$("#cc_walletaccount").on('submit',function(e){
		swal({
			title: 'Please confirm payment details.',
			text: "",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes!'
		}).then((result) => {
			if (result.value) {
			e.preventDefault();
					$('.hb').remove();
					$('.form-div').removeClass('has-error').removeClass('has-success');

					//check validation
					var err_count = 0;
					var to_req=[];
					//jquery blank validation..
					$(".required").each(function(){
						var field_id = $(this).attr("id");
						var data=[];
						if($(this).val()==""){
						data['id']=field_id;      
						to_req.push(data);
						err_count++;
						}
					});

					if(err_count>0){
					swal(
					'Opps...',
					"Please do check required fields.",
					'error'
					);     
					return false;
					}
					//check if card is selectged
					if(!$('.select_card').is(':checked')){
					swal(
					'Opps...',
					"Please do choose a card.",
					'error'
					);  
					return false;
					}
					var selected_card = $(".select_card:checked").val();
					var n = $(".select_card:checked").attr('n');
					var ct = $(".select_card:checked").attr('ct');
					var cn = $(".select_card:checked").attr('cn');
					//var function_ctrl = "process_check" ;
					var form_data = {
						notes:$("#notes").val(),
						email:$("#email").val(),
						mobile:$("#mobile").val(),
						name:$("#name").val(),
						reference_num:$("#reference_num").val(),
						amount:parseFloat($("#amount").val().replace(/,/g, '')),
						total_due:$("#amount_total").val(),
						tf:$("#hid_pfee").val(),
						pf:$("#hid_tfee").val(),
						billpayfee:$("#hid_fee").val(),
						is_merch:"{{$convenience_data['iscard_fee_on_merch']}}",
						balance:$("#balance").val(),
						select_card:selected_card,
						card_num:cn,
						card_type:ct,
						card_name:n,
					};
					$.ajax({
						url: "<?=site_url("suncashme_card/process_customer_cc_payment")?>",
						type: 'POST',
						dataType: 'json',
						data: form_data,
						beforeSend:function(xhr, textStatus) {
						//called when complete
						$("#process_payment").prop('disabled', true);
						$("#process_payment").text("processing... please wait");
						loader.showPleaseWait();				            
						},
						complete: function(xhr, textStatus) {
						//called when complete
						$("#process_payment").prop("disabled",false);
						$("#process_payment").text("Pay");
						loader.hidePleaseWait();
						},
						success: function(data) {
						__19e1919c83ck(data);
						if(data.success){
							//alert(data.msg);
							$("#transaction_code").text(data.reference);
							$("#success_section_transaction").show();
							
							//otable.ajax.reload();
							$("#wallet_section").hide();
							$("#process_payment").prop('disabled', false);
						} else {
							swal(data.msg);
							$("#process_payment").prop("disabled",false);
							$("#process_payment").text("Pay");
							loader.hidePleaseWait();	
						}
						},
						error: function(data, textStatus, errorThrown) {
						//called when there is an error
						alert(data.msg);
						$("#process_payment").prop("disabled",false);
						$("#process_payment").text("Pay");
						loader.hidePleaseWait();
						}
					});
			}
		});

		return false;

	});
	$("#back_to_form").click(function(){
		$("#cash_section").show();
		$("#success_section").hide();
	});
	$("#amount").change(function(){

		if($("#amount").val()<=0.00){
		  	swal("Amount is required.");

		  	return false;
		}

		$.ajax({
		  url: '<?=site_url("payment/process_fee")?>',
		  type: 'POST',
		  dataType: 'json',
		  // data: {amount: $("#amount").val()},
		  data: {amount: parseFloat($("#amount").val().replace(/,/g, ''))},
	   	  beforeSend:function(xhr, textStatus) {
	        //called when complete
		    loader.showPleaseWait();
	        $("#submit").prop('disabled', true);
	      },		  
		  complete: function(xhr, textStatus) {
		    //called when complete
		  loader.hidePleaseWait();
		  },
		  success: function(data, textStatus, xhr) {
			__19e1919c83ck(data);
		    if(data.success){
		    	//console.log(data.fee_data.fee);
		    	$(".amount_val").text($("#amount").val());
				$(".fee_val").text(data.fee_data.fee);
				$(".vat_val").text(data.fee_data.vat_charge);
				// var total = parseFloat($("#amount").val())+parseFloat(data.fee_data.fee)+parseFloat(data.fee_data.vat_charge);
				var total = parseFloat($("#amount").val().replace(/,/g, ''))+parseFloat(data.fee_data.fee)+parseFloat(data.fee_data.vat_charge)+parseFloat($(".conveniencefee").text());
				$(".amount_total").text(total.toFixed(2));
				$("#amount_total").val(total.toFixed(2));
				$("#hid_fee").val(data.fee_data.fee);
				$("#hid_vat").val(data.fee_data.vat_charge);
				// getcfee($("#amount").val());
				@if($_SESSION['tag']=='MERCHANT')
				getcfee($("#amount").val());
				@endif
				@if($_SESSION['tag']=='CUSTOMER')
				getcfee_customer($("#amount").val());
				@endif
				$("#submit").prop('disabled', false);
		    } else {
		    	swal(data.msg);
		    	$("#submit").prop('disabled', false);
		    }
		  },
		  error: function(xhr, textStatus, errorThrown) {
		    //called when there is an error
		   	swal(data.msg);
		    $("#submit").prop('disabled', false);

		  }
		});

	});
	function getcfee(amount){
		//e.preventDefault();
		//e.stopImmediatePropagation();				
	    // accordion is open
	    var total_billpay_fees = parseFloat($("#amount").val().replace(/,/g, ''))+parseFloat($("#hid_fee").val())+parseFloat($("#hid_vat").val());
	    //alert($("#amount").val()+"+"+$("#hid_fee").val()+"+"+$("#hid_vat").val());
	    $.ajax({
	      url: '{{base_url("payment/get_fees_payment")}}',
	      type: 'POST',
	      dataType: 'json',
	      data:{amount:total_billpay_fees,client_id:'{{$_SESSION["client_record_id"]}}'},
	   	  beforeSend:function(xhr, textStatus) {
	        //called when complete
		    //loader.showPleaseWait();
	        $("#submit").prop('disabled', true);
	      },		  
		  complete: function(xhr, textStatus) {
		    //called when complete
		  	//loader.hidePleaseWait();
		  },
	      success: function(data, textStatus, xhr) {
			__19e1919c83ck(data);
	        //called when successful
	        //paste values
	        if(data.success){
	        	$(".conveniencefee").text(data.fee);
	        	//$("#amount").text(data.total);
				var total= total_billpay_fees+parseFloat(data.fee);
				$(".amount_total").text(total.toFixed(2));
				$("#amount_total").val(total.toFixed(2));
				$("#hid_pfee").val(data.pf);
				$("#hid_tfee").val(data.tf);
				$("#hid_totalfee").val(data.fee);

		    	$("#submit").prop('disabled', false);
	        }  else {
	        	swal(data.msg);
		    	$("#submit").prop('disabled', false);
	        }

	      },
	      error: function(xhr, textStatus, errorThrown) {
	        //called when there is an error
        	swal(data.msg);
	    	$("#submit").prop('disabled', false);
	      }
	    });
	}
	function getcfee_customer(amount){
		//e.preventDefault();
		//e.stopImmediatePropagation();				
	    // accordion is open
	    var total_billpay_fees = parseFloat($("#amount").val().replace(/,/g, ''))+parseFloat($("#hid_fee").val())+parseFloat($("#hid_vat").val());
	    //alert($("#amount").val()+"+"+$("#hid_fee").val()+"+"+$("#hid_vat").val());
	    $.ajax({
	      url: '{{base_url("payment/get_fees_customer")}}',
	      type: 'POST',
	      dataType: 'json',
	      data:{amount:total_billpay_fees,client_id:'{{$_SESSION["client_record_id"]}}'},
	   	  beforeSend:function(xhr, textStatus) {
	        //called when complete
		    //loader.showPleaseWait();
	        $("#submit").prop('disabled', true);
	      },		  
		  complete: function(xhr, textStatus) {
		    //called when complete
		  	//loader.hidePleaseWait();
		  },
	      success: function(data, textStatus, xhr) {
			__19e1919c83ck(data);
	        //called when successful
	        //paste values
	        if(data.success){
	        	$(".conveniencefee").text(data.fee_data.fee);
	        	//$("#amount").text(data.total);
				var total= total_billpay_fees+parseFloat(data.fee_data.fee);
				$(".amount_total").text(total.toFixed(2));
				$("#amount_total").val(total.toFixed(2));
				$("#hid_pfee").val(data.fee_data.pf);
				$("#hid_tfee").val(data.fee_data.tf);
				$("#hid_totalfee").val(data.fee_data.fee);

		    	$("#submit").prop('disabled', false);
	        }  else {
	        	swal(data.msg);
		    	$("#submit").prop('disabled', false);
	        }

	      },
	      error: function(xhr, textStatus, errorThrown) {
	        //called when there is an error
        	swal(data.msg);
	    	$("#submit").prop('disabled', false);
	      }
	    });
	}

	@if($_SESSION['tag']=='MERCHANT')
	getcfee('{{$payment_data["amount"]}}');
	@endif
	@if($_SESSION['tag']=='CUSTOMER')
	getcfee_customer('{{$payment_data["amount"]}}');
	@endif



	$(document).on('click','.upload_req',function(){

			$("#c").val($(this).attr('cid'));
			$("#v").val($(this).attr('v'));
			$("#verify_section").show();
			$("#link_section").hide();
			$("#wallet_section").hide();
			$("#upload_verify_section").hide();
	});	
	$(document).on('click','.back_main',function(){

			$("#verify_section").hide();
			$("#link_section").hide();
			$("#wallet_section").show();
			$("#upload_verify_section").hide();
	});	
	$(document).on('click','.manual_upload',function(){


			$("#verify_section").hide();
			$("#link_section").hide();
			$("#wallet_section").hide();
			$("#upload_verify_section").show();

			
	});		
	$(document).on('click','.back_verify',function(){

			$("#verify_section").show();
			$("#link_section").hide();
			$("#wallet_section").hide();
			$("#upload_verify_section").hide();
	});	
	$(document).on('click','.link_card',function(){
		
			$("#link_section").show();
			$("#verify_section").hide();
			$("#wallet_section").hide();
			$("#upload_verify_section").hide();		
	});		

	
	$("#verify_form").submit(function(){
	$.ajax({
		type: "post",
		url: "{{base_url('suncashme_card/process_verify_card')}}",
		data: $(this).serializeArray(),
		dataType: "json",
		success: function (data) {
		__19e1919c83ck(data);
		if(data.success){
			swal(data.msg);
			window.location.reload();
		}else {
			swal(data.msg);
		}
		},
		error: function (data) {
		swal(data.msg);
		}
	});
	return false;
	});
	$(document).on('click','.delete_card',function(){
		var r = confirm("Are you sure you want to delete linked card?");
			if(r==true){
				$.ajax({
					type: "POST",
					url: "{{base_url('suncashme_card/delete_linked_card')}}",
					data: {id:$(this).attr('cid')},
					dataType: "json",
					success: function (response) {
						__19e1919c83ck(data);
						alert(response.msg);
						window.location.reload();
					}
				});
			}
	});
	function CallbackSuccess(responseData) {
	alert(JSON.stringify(responseData));
	}

	function CallbackCancel(responseData) {
	alert(JSON.stringify(responseData));
	}

	$("#show_card").click(function(){
		$("#NewCenposPlugin").submitAction();
	});
	var merchant='{{CENPOST_MERCHANT_ID}}';
	var verify_params ='{{$verify_params["Data"]}}';
	var customer_id = '{{$customer_id}}';
	$("#NewCenposPlugin").createWebpay({
	url: 'https://www.cenpos.net/simplewebpay/cards',
	params : "customerCode="+customer_id+"&verifyingpost="+verify_params+"&iscvv=true",
	sessionToken:true,
	height:'400px',
	//isCvv :true&SecretKey=a0c70a0d5aa451bfc02f17e9199e41e6&verifyingpost="+vp
	//sessionToken:false,
	    beforeSend:function(xhr, textStatus) {
	        //called when complete
	        $("#submit").prop('disabled', true);
	      },
	    complete: function(xhr, textStatus) {
	        //called when complete
	         $("#submit").prop('disabled', false);
	      },
        success: function(data){
			//__19e1919c83ck(data);
        	if(data.Result==0){
        	$("#card_number_link").text(data.ProtectedCardNumber);
        	$("#name_on_card_link").text(data.NameonCard);
        	$("#card_type_link").text(data.CardType);
        	$("#tid").val(data.RecurringSaleTokenId);
        	$("#success_link_section").show();
        	$("#card_info").hide();
        	} else {
        	$("#tid").val('');
        	swal(data.Message);	

        	}
        },
        cancel: function(response){
        	$("#tid").val('');
        	swal(response.Message);	
        	$("#success_section").hide();
        	$("#card_info").show();

        }	
	});
    $("#process_card_link").click(function(e){
    	var form_data ={
    	//merchant:merchant,	
    	tokenid:$("#tid").val(),
    	name_customer_card:$("#name_card_link").val(),
    	//email_card:$("#email_card").val(),
    	//mobile_card:$("#mobile_card").val(),
    	card_type_link:$("#card_type_link").text(),
    	card_number_link:$("#card_number_link").text(),
    	name_on_card_link:$("#name_on_card_link").text(),
		amount_link:$("#amount_link").val(),
		customer_id:customer_id,
        }   
		$.ajax({
		url: '{{base_url("suncashme_card/process_link_card")}}',
		type: 'POST',
		dataType: 'json',
		data: form_data,
		beforeSend:function(){
			$("#process_card_link").prop("disabled",true);
			$("#process_card_link").text("Adding Card... Please wait!");
			//loader.showPleaseWait();
		},
		complete: function(xhr, textStatus) {
			$("#process_card_link").prop("disabled",false);
			$("#process_card_link").text("Adding Card. Please Wait! ");
			//loader.hidePleaseWait();
		},
		success: function(data, textStatus, xhr) {
			 __19e1919c83ck(data);
			//console.log();
			if(data.success){
				swal(data.msg);
				setTimeout(function(){ window.location.reload(); }, 2000);
			} else {
				swal(data.msg);
				$(".cancel_section").show();
				$(".payment_btn_section").hide();
				// $("#success_section_transaction").hide();
				$("#process_card_link").prop("disabled",false);
				$("#process_card_link").text("Adding Card.. Please Wait! ");
				
			}
		},
		error: function(xhr, textStatus, errorThrown) {
		}
		});
	});
	$("#submit_upload").click(function(){
	alert("x");
		var url = '{{base_url("payment/process_verify_upload_requirements")}}';
		var formData = new FormData();
		
		formData.append('c', $('#c').val());
		formData.append('card_id_upload', $('#card_id_upload')[0].files[0]); 
		formData.append('credit_card_upload', $('#credit_card_upload')[0].files[0]); 
		var button = $("#submit_upload");
		ProcessForm(formData,url,button);
	});


      // $("wallet")[0].reset();
	</script>
</body>
</html>