<?php
//loading assets helper
defined('BASEPATH') OR exit('No direct script access allowed');
$assetHelper = new AssetHelper\AssetHelper();
//loading ci core
$ci = &get_instance();
?>
@extends('admin.partials.admin-home')


@section('title', 'Customer Login Dashboard')

<!--for css to be use on page it can be include or css path -->
@section('custom_css')
	
@endsection

@section('sub_content')
<!-- bread crumbs -->
<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Dashboard</h4> 
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="{{base_url('admin/dashboard')}}">Dashboard</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>

    <div class="col-sm-12 col-md-12 custom-well" id="content">
          <h1>Welcome {{ucfirst($ci->session->userdata('admin_first_name'))}}! </h1>
    </div>

    <div class="row">

    <a href="{{base_url('admin/user')}}"><div class="col-lg-4 col-sm-6 col-xs-12">
        <div class="white-box">
            <h3 class="box-title">ADMINISTRATORS</h3>
            <ul class="list-inline two-part">
                <li><i class="fa fa-user-secret text-purple"></i></li>
                <li class="text-right"><span class="counter">{{$admin_count}}</span></li>
            </ul>
        </div>
    </div></a>
    <a href="{{base_url('admin/organizer')}}"><div class="col-lg-4 col-sm-6 col-xs-12">
        <div class="white-box">
            <h3 class="box-title">ORGANIZERS</h3>
            <ul class="list-inline two-part">
                <li><i class="fa fa-users fa-fw text-info"></i></li>
                <li class="text-right"><span class="">{{$organizer_count}}</span></li>
            </ul>
        </div>
    </div></a>
    <a href="{{base_url('admin/agents')}}"><div class="col-lg-4 col-sm-6 col-xs-12">
        <div class="white-box">
            <h3 class="box-title">AGENTS</h3>
            <ul class="list-inline two-part">
                <li><i class="mdi mdi-message-processing fa-fw text-success"></i></li>
                <li class="text-right"><span class="counter">{{$agent_count}}</span></li>
            </ul>
        </div>
    </div></a>                  
</div>
<!-- /.row -->
@endsection

<!--for js to be use on page it can be include or js path -->
@section('custom_js')

@endsection

