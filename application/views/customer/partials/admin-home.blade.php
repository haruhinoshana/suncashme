<?php
//loading assets helper
defined('BASEPATH') OR exit('No direct script access allowed');
$assetHelper = new AssetHelper\AssetHelper();
//loading ci core
$ci = &get_instance();
?>
@extends('layout.main')

@section('content')
    @include('admin/partials/admin-nav')
    <div id="page-wrapper">
        <div class="container-fluid">
            <!-- Page Heading -->
            <div class="row" id="main">
            	@yield('sub_content')
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
@endsection
@section('footer')
@include('layout/footer') 
@endsection


