<nav class="navbar navbar-default navbar-static-top m-b-0">
    <div class="navbar-header">
        <div class="top-left-part">
            <!-- Logo -->
            <a class="logo" href="{{base_url('admin/dashboard')}}">
                <!-- Logo icon image, you can use font-icon also --><b>
                <!--This is dark logo icon-->
                {!!$assetHelper->link_plugins('img','logo.png')!!}
                <!--This is light logo icon-->
                <!-- <img src="../plugins/images/admin-logo-dark.png" alt="home" class="light-logo" /> -->
             </b>
                <!-- Logo text image you can use text also -->
                <!-- <span class="hidden-xs"> -->
                    <!-- Ticketing -->
                <!--This is dark logo text-->
                
                <!--This is light logo text-->
                <!-- <img src="../plugins/images/admin-text-dark.png" alt="home" class="light-logo" /> -->
             </span> 
            </a>
        </div>
        <!-- /Logo -->
        <!-- Search input and Toggle icon -->
        <ul class="nav navbar-top-links navbar-left">
            <li><a href="javascript:void(0)" class="open-close waves-effect waves-light"><i class="ti-menu"></i></a></li>
        </ul>
        <ul class="nav navbar-top-links navbar-right pull-right">
            <li class="dropdown">
                <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"> 
              <?php 

                    $lbl = $ci->session->userdata('admin_user_role') =='admin_user' ? 'Admin' : '' ; 

              ?>
                    <img src="<?php echo base_url();?>assets/img/user-logo.png" alt="user-img" width="36" class="img-circle">
                    <!-- <img src="../assets/img/user-logo.png" alt="user-img" width="36" class="img-circle">  -->
                    <b>{{ucfirst($lbl)}}:</b>

                    <b class="hidden-xs">{{ucfirst($ci->session->userdata('admin_full_name'))}}</b><span class="caret"></span>
                     </a>
                <ul class="dropdown-menu dropdown-user animated slideInDown">
                    <li>
                        <div class="dw-user-box">
                            <!-- <div class="u-img"><img src="../plugins/images/users/varun.jpg" alt="user" /></div> -->
                            <div class="u-text">
                                <h4>{{ucfirst($ci->session->userdata('admin_full_name'))}}</h4>
                                <p class="text-muted">{{$ci->session->userdata('admin_email')}}</p>
                                
                                <!-- <a href="{{base_url('profile')}}" class="btn btn-color1 btn-sm">View Profile</a></div> -->
                        </div>
                    </li>
                    <li role="separator" class="divider"></li>
                    <li><a href={{base_url('admin/changepassword')}}><i class="fa fa-key"></i > Change Password</a></li>
  <!--                   <li><a href="#"><i class="ti-wallet"></i> My Balance</a></li>
                    <li><a href="#"><i class="ti-email"></i> Inbox</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="#"><i class="ti-settings"></i> Account Setting</a></li> -->
                    <li role="separator" class="divider"></li>
                    <li><a href="{{base_url('admin/logout')}}"><i class="fa fa-power-off"></i> Logout</a></li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
        </ul>
    </div>
    <!-- /.navbar-header -->
    <!-- /.navbar-top-links -->
    <!-- /.navbar-static-side -->
</nav>
@include('admin/partials/admin-side-nav')
