<!-- ============================================================== -->
<!-- Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav slimscrollsidebar">
        <div class="sidebar-head">
            <h3><span class="fa-fw open-close"><i class="ti-close ti-menu"></i></span> <span class="hide-menu">Navigation</span></h3>
        </div>
        <div class="user-profile">
        </div>
        <ul class="nav" id="side-menu">
            <li> <a href="<?=site_url('admin/dashboard')?>" class="waves-effect"><i class="mdi mdi-av-timer fa-fw" data-icon="v"></i> <span class="hide-menu"> Dashboard </span></a>
            </li>
            <li> <a href="<?=site_url('admin/organizer')?>" class="waves-effect"><i class="fa fa-user fa-fw" data-icon="v"></i> <span class="hide-menu"> Organizers </span></a>
            </li>
            <li> <a href="<?=site_url('admin/user')?>" class="waves-effect"><i class="fa fa-user-secret fa-fw" data-icon="v"></i>     <span class="hide-menu">  Administrators</span></a>
            </li>
            <li> <a href="<?=site_url('admin/agents')?>" class="waves-effect"><i class="fa fa-users fa-fw" data-icon="v"></i> <span class="hide-menu"> Agents </span></a>
            </li>     
            <!-- <li> <a href="<?=site_url('admin/csr')?>" class="waves-effect"><i class="fa fa-user fa-fw" data-icon="v"></i> <span class="hide-menu"> Customer Service </span></a> -->
            </li>   
            <li> <a href="<?=site_url('#')?>" class="waves-effect"><i class="fa fa-file-text-o fa-fw" data-icon="v"></i> <span class="hide-menu"> Reports </span></a>
                <ul class="nav nav-second-level">
                <li> <a href="<?=site_url('admin/reports/ticket_sale')?>" class="waves-effect"><i class="fa-fw" data-icon="v">$</i> <span class="hide-menu">Agent Ticket Sales</span></a></li>                    
                   <!--  <li> <a href="<?=site_url('admin/reports/customer')?>" class="waves-effect"><i class="fa fa-male fa-fw"></i><span class="hide-menu">Customer Management</span></a> </li>  --> 
<!--                     <li> <a href="<?=site_url('admin/reports/topmerchant')?>" class="waves-effect"><i class="fa fa-user fa-fw"></i><span class="hide-menu">Top Organizer</span></a></li>   
                    <li> <a href="<?=site_url('admin/reports/topagent')?>" class="waves-effect"><i class="fa fa-users fa-fw"></i><span class="hide-menu">Top Agent</span></a> </li> -->
<!--                     <li> <a href="<?=site_url('admin/reports/sales')?>" class="waves-effect"><i class="fa-fw" data-icon="v">$</i> <span class="hide-menu"> Ticket Sales </span></a></li> -->
<!--                     <li> <a href="<?=site_url('admin/reports/distribution')?>" class="waves-effect"><i class="fa fa-qrcode fa-fw" data-icon="v"></i> <span class="hide-menu"> Distribution </span></a></li> -->
<!--                     <li> <a href="<?=site_url('admin/reports/eventattendance')?>" class="waves-effect"><i class="fa fa-list fa-fw" data-icon="v"></i> <span class="hide-menu"> Attendance </span></a></li>
                    <li> <a href="<?=site_url('admin/reports/coupons')?>" class="waves-effect"><i class="fa fa-gift fa-fw" data-icon="v"></i> <span class="hide-menu"> Coupons/Promo Codes </span></a></li>
                    <li> <a href="<?=site_url('admin/reports/complementary')?>" class="waves-effect"><i class="fa fa-ticket fa-fw" data-icon="v"></i> <span class="hide-menu"> Complementary Tickets </span></a></li>
                    <li> <a href="<?=site_url('admin/reports/agentsales')?>" class="waves-effect"><i class="fa fa-users fa-fw" data-icon="v"></i> <span class="hide-menu"> Outlets/Agents Sales </span></a></li> -->
<!--                      <ul class="nav nav-third-level">
                             <li> <a href="<?=site_url('admin/reports/sales')?>" class="waves-effect"><i class="fa fa-eye-slash fa-fw"></i><span class="hide-menu">Organizer</span></a> 
                             <li> <a href="<?=site_url('admin/reports/sales')?>" class="waves-effect"><i class="fa fa-eye-slash fa-fw"></i><span class="hide-menu">Event</span></a> 
                             <li> <a href="<?=site_url('admin/reports/sales')?>" class="waves-effect"><i class="fa fa-eye-slash fa-fw"></i><span class="hide-menu">Agent</span></a> 
                     </ul> -->
                    <!-- <li> <a href="<?=site_url('admin/reports/noshow')?>" class="waves-effect"><i class="fa fa-eye-slash fa-fw"></i><span class="hide-menu">No Show</span></a> </li> -->
                    <!-- <li> <a href="<?=site_url('admin/reports/arrivalattendees')?>" class="waves-effect"><i class="fa fa-male fa-fw"></i><span class="hide-menu">Arrival of Attendees</span></a> </li> -->

                    <!-- <li> <a href="<?=site_url('admin/reports/eventattendance')?>" class="waves-effect"><i class="fa fa-list fa-fw"></i><span class="hide-menu">Event Attendance</span></a> </li> -->
                    <!-- <li> <a href="<?=site_url('admin/reports/redemption')?>" class="waves-effect"><i class="fa fa-hand-rock-o fa-fw"></i><span class="hide-menu">Redemption</span></a></li>  -->
                </ul>    

            <li>   
<!--             <li> <a href="groups.html" class="waves-effect"><i class="mdi mdi-account-multiple-plus fa-fw" data-icon="v"></i> <span class="hide-menu"> Groups </span></a>
            </li>
            <li> <a href="templates.html" class="waves-effect"><i class="mdi mdi-newspaper fa-fw" data-icon="v"></i> <span class="hide-menu"> Templates </span></a>
            </li> -->
<!--             <li> <a href="settings.html" class="waves-effect"><i class="mdi mdi-settings fa-fw" data-icon="v"></i> <span class="hide-menu"> Settings </span></a>
            </li>
            <li> <a href="#" class="waves-effect"><i class="mdi mdi-email fa-fw" data-icon="v"></i> <span class="hide-menu"> SMS <span class="fa arrow"></span> </span></a>
                <ul class="nav nav-second-level">
                    <li> <a href="messages.html"><i class=" fa-fw">M</i><span class="hide-menu">Messages</span></a> </li>
                    <li> <a href="logs.html"><i class=" fa-fw">L</i><span class="hide-menu">Logs</span></a> </li>
                </ul>
            </li> -->
        </ul>
    </div>
</div>
