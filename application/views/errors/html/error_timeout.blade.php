<?php
//loading assets helper
defined('BASEPATH') OR exit('No direct script access allowed');
$assetHelper = new AssetHelper\AssetHelper();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>SunCash.Me</title>
	<link rel="stylesheet" href="{{base_url('assets_main/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/main.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/responsive.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/all.min.css')}}">
</head>
<body>
	<nav class="navbar navbar-expand-lg sticky-top bg-white shadow-sm p-0">
	  <div class="container p-0" role="navigation">
	  	<a class="navbar-brand" href="index.html"><img src="{{base_url('assets_main/imgs/header-logo@2x.png')}}" alt="Suncash Logo" class="d-inline-block align-top" width="160"></a>
	  	<a href="#" class="menu-button"></a>
	  	<div class="justify-content-end main-menu">

		  </div>
	  </div>
	</nav>

	<section class="full" id="reload_section" >
		<div class="container  text-center">
			<div class="row d-flex justify-content-center align-items-center mb-5">
				<div class="user-card message-card reload-card">
					<div class="header w-content"></div>
					<div class="body">
						<div class="user-image">
							<img src="{{base_url('assets_main/imgs/reload-icon.png')}}">
						</div>
						<div class="details">
							<div class="message error_msg">Page not available this time.</div>
						</div>
						<div class="text-center border-top msg_area">


						</div>
						 <a href="{{base_url('')}}" class="btn btn-orange full">Back</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<footer class="py-3" style="position: fixed;
    left: 0;
    bottom: 0;
    width: 100%;">
		<div class="container">
			<div class="row">
				<div class="col-12 col-lg-6 d-flex align-self-center text-left">
					<span>© {{date('Y')}} SunCash.Me. All Rights Reserved.</span>
				</div>
				<div class="col-12 col-lg-6 text-right">
					<img src="{{base_url('assets_main/imgs/footer-logo.png')}}">
				</div>
			</div>
		</div>
	</footer>

	<script src="{{base_url('assets_main/js/jquery-3.2.1.min.js')}}"></script>
	<script src="{{base_url('assets_main/js/bootstrap.min.js')}}"></script>
	<script src="{{base_url('assets_main/js/main.js')}}"></script>
    <script src="{{base_url('assets_main/js/select.min.js')}}"></script>
	{!!$assetHelper->link_plugins('js','jquery-1.11.3.min.js')!!}
	{!!$assetHelper->link_plugins('js','bootstrap-3.3.7/js/bootstrap.min.js')!!}
	{!!$assetHelper->link_plugins('js','sidebar-nav/dist/sidebar-nav.min.js')!!}
	{!!$assetHelper->link_plugins('js','override/jquery.slimscroll.js')!!}
	{!!$assetHelper->link_plugins('js','override/waves.js')!!}
	{!!$assetHelper->link_plugins('js','override/custom.min.js')!!}
	{!!$assetHelper->link_plugins('js','sweetalert2-master/dist/sweetalert2.all.min.js')!!}
	{!!$assetHelper->link_plugins('js','bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js')!!}
	{!!$assetHelper->link_plugins('js','curioussolutions-datetimepicker/dist/DateTimePicker.min.js')!!}
	{!!$assetHelper->link_plugins('js','Inputmask-5.x/dist/jquery.inputmask.min.js')!!}

	<!-- {!!$assetHelper->link_plugins('js','dropzone-master/dist/dropzone.js')!!}	 -->
	<script  type="text/javascript" charset="utf-8">

	</script>
</body>
</html>