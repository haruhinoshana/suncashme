<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>SunCash.Me</title>
	<link rel="stylesheet" href="{{base_url('assets_main/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/main.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/responsive.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/all.min.css')}}">
</head>
<body>
	<nav class="navbar navbar-expand-lg sticky-top bg-white shadow-sm p-0">
	  <div class="container p-0" role="navigation">
	  	<a class="navbar-brand" href="{{base_url('')}}"><img src="{{base_url('assets_main/imgs/header-logo@2x.png')}}" alt="Suncash Logo" class="d-inline-block align-top" width="200"></a>
	  	<a href="#" class="menu-button"><span></span></a>
  	<div class="justify-content-end main-menu">
				<ul class="navbar-nav d-flex">
		      <li class="nav-item">
		        <a class="nav-link" href="{{base_url('')}}">About</a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link active" href="{{base_url('info/faq')}}">FAQ </a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="{{base_url('info/terms')}}">Terms</a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="{{base_url('info/contacts')}}">Contact Us</a>
		      </li>
          <li class="nav-item button-item">
            <a class="nav-link btn-link btn-primary" href="http://dev.suncash.me/business/">For Business</a>
          </li>
          <li class="nav-item button-item">
            <a class="nav-link btn-link btn-secondary" href="http://dev.suncash.me/customer/">For Customers</a>
          </li>
		    </ul>
		  </div>
	  </div>
	</nav>

	<section class="content-block faq">
		<div class="container">
	  	<div id="accordion" class="accordion">
        <div class="card mb-0 no-border">
          <div class="card-header collapsed" data-toggle="collapse" href="#collapseOne">
              <a class="card-title">What is SunCash.Me?</a>
          </div>
          <div id="collapseOne" class="card-body collapse" data-parent="#accordion" >
              <p>SunCash.me is the fastest, safest and simplest way for your family, friends or customers to pay through SunCash. Just share your personalized SunCash.Me link (SunCash.Me/YourName) with others, and they can send you money.</p>
              <p>They just tap on your link to go to your SunCash.Me, enter the amount to be paid, and send the money without knowing your email address or mobile phone number, or even have the SunCash app.</p>
          </div>
          <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
              <a class="card-title">How do I create my own SunCash.Me link?</a>
          </div>
          <div id="collapseTwo" class="card-body collapse" data-parent="#accordion" >
              <p>Visit SunCash.Me to create your link and account. If you already have a SunCash account, log in and generate your link. If you don't, signup is easy, fast and free.  You’ll be required to create one when signing-up.</p>
          </div>
          <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
              <a class="card-title">The SunCash.Me link I want is taken. What can I do?</a>
          </div>
          <div id="collapseThree" class="card-body collapse" data-parent="#accordion" >
              <p>SunCash.Me links are available on a first-come, first-served basis and can’t be reserved. If your preferred link is taken, try other alternatives. Grab your SunCash.Me link before someone else does!</p>
          </div>
          <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
              <a class="card-title">How many links can I have for SunCash.Me?</a>
          </div>
          <div id="collapseFour" class="card-body collapse" data-parent="#accordion" >
              <p>Only one active SunCash.Me link per SunCash account is allowed. Once you create your SunCash.Me link, you won’t be able to edit it. So choose carefully!</p>
          </div>
          <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
              <a class="card-title">Are there any restrictions on what I can name my link?</a>
          </div>
          <div id="collapseFive" class="card-body collapse" data-parent="#accordion" >
              <p>Yes. Here are the guidelines for selecting a SunCash.Me link:
              	<ul class="list">
              		<li>It can only contain alphanumeric characters (letters A-Z, numbers 0-9) and can’t contain symbols, dashes, or spaces.</li>
              		<li>It’s not case sensitive, but casing is recommended to improve its readability for your friends and family.</li>
              		<li>Profane, offensive, defamatory or reserved trademarks are forbidden even if the link is available. Your account may be suspended if you violate this one. Let’s keep it clean.</li>
              		<li>Link names are limited to 20 characters max.</li>
              	</ul>
              </p>
          </div>
          <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
              <a class="card-title">How long can I keep my SunCash.Me link?</a>
          </div>
          <div id="collapseSix" class="card-body collapse" data-parent="#accordion" >
              <p>It will never expire. You can choose to turn it off and on as often as you like.</p>
          </div>
          <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">
              <a class="card-title">How do I get paid with SunCash.Me?</a>
          </div>
          <div id="collapseSeven" class="card-body collapse" data-parent="#accordion" >
              <p>Just share your SunCash.Me link. Text it. Tweet it. Place it on print media. Post it to your blog, website, Instagram or Facebook page. Email it to your dad. Your friends can then click, type, or tap on your link and pay you back. Getting paid is easy! </p>
          </div>
          <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseEight">
              <a class="card-title">Who can I request money from?</a>
          </div>
          <div id="collapseEight" class="card-body collapse" data-parent="#accordion" >
              <p>Anyone can send you money through your SunCash.Me link.</p>
          </div>
          <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseNine">
          		<a class="card-title">Does it cost me anything to receive money?</a>
          </div>
          <div id="collapseNine" class="card-body collapse" data-parent="#accordion" >
              <p>Receiving is Free!</p>
          </div>
          <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTen">
          	<a class="card-title">When I get paid using SunCash.Me, where does the money go?</a>
          </div>
          <div id="collapseTen" class="card-body collapse" data-parent="#accordion" >
              <p>As soon as you receive a payment, it goes into your SunCash balance. You can use the money to buy things online or in stores, pay your utility bills, top-up mobile phones locally or anywhere in the world, buy movie or events tickets or send money to friends. Soon, if you have your bank account linked to SunCash, you can transfer the funds to your bank.</p>
          </div>
          <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTen">
          	<a class="card-title">Does it cost anything to transfer money from SunCash to my bank?</a>
          </div>
          <div id="collapseTen" class="card-body collapse" data-parent="#accordion" >
              <p>Yes.  Fees will be posted once the service is started.</p>
          </div>
          <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTen">
          	<a class="card-title">Can I send more than one request at a time?</a>
          </div>
          <div id="collapseTen" class="card-body collapse" data-parent="#accordion" >
              <p>There’s no limit on how many requests you can send. Just share your link. That’s it. </p>
          </div>
          <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTen">
          	<a class="card-title">How do I request a specific amount?</a>
          </div>
          <div id="collapseTen" class="card-body collapse" data-parent="#accordion" >
              <p>Just add the amount you want to request to the end of your link. For example, use SunCash.Me/TinaKnowles/25 to request $25.00.</p>
          </div>
          <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTen">
          	<a class="card-title">Can I cancel a Suncash payment?</a>
          </div>
          <div id="collapseTen" class="card-body collapse" data-parent="#accordion" >
              <p>If the payment has a status of "Unclaimed”, you can cancel them.   If the payment is completed, you won’t be able to cancel it because the receiver has already received the money. You can reverse a completed payment by contacting your receiver and asking for a refund.</p>
          </div>
          <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTen">
          	<a class="card-title">Is it free to set up? </a>
          </div>
          <div id="collapseTen" class="card-body collapse" data-parent="#accordion" >
              <p>100% Free! SunCash doesn't charge a fee to open a SunCash account or set up a SunCash.Me link.</p>
          </div>
          <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTen">
          	<a class="card-title">What kind of SunCash account do I need? </a>
          </div>
          <div id="collapseTen" class="card-body collapse" data-parent="#accordion" >
              <p>SunCash.Me is open to all account types - Business, Personal, Charities, Churches, Renters etc.</p>
          </div>
          <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTen">
          	<a class="card-title">Is SunCash.Me available on my Island?</a>
          </div>
          <div id="collapseTen" class="card-body collapse" data-parent="#accordion" >
              <p>Yes, its available in every island in the Bahamas.</p>
          </div>
        </div>
        <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTen">
          	<a class="card-title">Can I use SunCash.Me on my mobile device?</a>
        </div>
        <div id="collapseTen" class="card-body collapse" data-parent="#accordion" >
            <p>Sure! Your SunCash.Me link is available on mobile (iPhone and Android), desktop, and tablet. </p>
            <p>Just tap the SunCash.Me link you’ve received or type it in your browser. Enter the amount you’d like to pay or send and choose how you’d like to pay. You can only send money from your SunCash balance at this time.  Coming soon you will be able to choose your bank account or your preferred debit or credit card. If you don’t have a SunCash account, we will help you create one. Signup is fast and free.</p>
        </div>
        <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTen">
          	<a class="card-title">What are the fees for sending money?</a>
        </div>
        <div id="collapseTen" class="card-body collapse" data-parent="#accordion" >
            <p>The sender pays a small fee to send money.  Government VAT at 12% is also levied on the transaction fee.  It’s totally free for the receiver.</p>
        </div>
        <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTen">
          	<a class="card-title">How do I know if my money request or invoice has been paid?</a>
        </div>
        <div id="collapseTen" class="card-body collapse" data-parent="#accordion" >
            <p>After your money request or invoice has been paid:
							<ul class="list">
								<li>We email you to let you know it has been paid.</li>
								<li>The <strong>Status</strong> of the invoice or money request changes to <strong>Paid</strong> in your transaction history.</li>
								<li>The money appears in your balance.</li>
							</ul>
            </p>
        </div>
        <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTen">
          	<a class="card-title">How do I report potential fraud, spoof or unauthorized transactions to SunCash?</a>
        </div>
        <div id="collapseTen" class="card-body collapse" data-parent="#accordion" >
            <p>It’s extremely important to report any suspected instances of fraud. If you think your account has been compromised change your password and update your security questions right away to protect your account (we may limit what you can do on your account until you do so).</p>
            <p>If you've received an email notification that something has been changed on your account, but you don't remember changing it, please change your password and security questions. Next, you can update any changed information, such as your email address, address, phone number, or other profile information.</p>
        </div>
        <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTen">
          	<a class="card-title">I forgot my SunCash.Me link, what should I do?</a>
        </div>
        <div id="collapseTen" class="card-body collapse" data-parent="#accordion" >
            <p>Your SunCash.Me link is included in the welcome email we sent when you first signed up. Also you can log into SunCash.Me and you’ll see your link.</p>
        </div>
        <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTen">
          	<a class="card-title">How can I cancel or delete my SunCash.Me link?</a>
        </div>
        <div id="collapseTen" class="card-body collapse" data-parent="#accordion" >
            <p>Once you’ve created your SunCash.Me link, you won’t be able to delete it. If you want to hide your SunCash.Me link from the everyone, log into SunCash.Me, tap or click “Turn Off My Link.” No one will be able to send you money. Your SunCash.Me link will still be linked to your SunCash account and won’t be available for others to take.</p>
        </div>
        <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTen">
          	<a class="card-title">What photo will show up on my link? Can I change it?</a>
        </div>
        <div id="collapseTen" class="card-body collapse" data-parent="#accordion" >
            <p>If you already have a Profile photo saved to your SunCash account, that’s what will show up. If you don’t have a photo or if you want to replace it, you can do that through your SunCash.Me link.</p>
        </div>
        <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTen">
          	<a class="card-title">How Secure is SunCash.Me</a>
        </div>
        <div id="collapseTen" class="card-body collapse" data-parent="#accordion" >
            <p>We automatically encrypt your confidential info in transit from your computer to ours using the Secure Sockets Layer protocol (SSL) with an encryption key length of 128-bits (the highest level commercially available). Before you even register or log in to the SunCash site, our server checks that you're using an approved browser one that uses SSL 3.0 or higher.</p>

            <h5>Your financial information</h5>
            <p>SunCash helps keep your transactions secure by not sharing your full financial information with sellers.</p>

            <h5>24/7 Monitoring</h5>
            <p>We monitor transactions 24/7. That should help you rest easy.</p>

            <h5>Secure technology</h5>
            <p>Our encryption help keeps your online transactions guarded from start to finish.</p>

            <h5>Fraud prevention</h5>
            <p>Us if anything seems suspicious so we can help you protect yourself from fraudulent charges against your account. We'll 	never ask for sensitive information in an email.</p>

            <h5>Dispute resolution</h5>
            <p>If there’s a problem with a transaction, we’ll put a hold on the funds until the issue is resolved. We investigate and stay involved every step of the way.</p>
        </div>
        <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTen">
          	<a class="card-title">Will any of my information be shared?</a>
        </div>
        <div id="collapseTen" class="card-body collapse" data-parent="#accordion" >
            <p>The only information available publicly through your SunCash.Me link will be your Profile photo (if you have one), the name associated with the account, a cover photo if you added one, the personal note, and your city/settlement and island/country according to your preferences</p>
        </div>
        <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTen">
          	<a class="card-title">Someone else is using a SunCash.Me link that infringes my intellectual property rights. What can I do about that?</a>
        </div>
        <div id="collapseTen" class="card-body collapse" data-parent="#accordion" >
            <p>Use of a SunCash.Me link that infringes the intellectual property rights of a third person is prohibited under the SunCash.Me <a href="#">Terms and Conditions</a>. It is our policy to deactivate any infringing links that are reported to us together with adequate supporting information. Send a report to <a href="#">customerservice@mysuncash.com</a> to start the investigation.</p>
        </div>
        <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTen">
          	<a class="card-title">What are SunCash Confirmed Charities and Churches?</a>
        </div>
        <div id="collapseTen" class="card-body collapse" data-parent="#accordion" >
            <p>SunCash Confirmed Charities or Churches is a process for confirming that every entity that registers with SunCash as a charity or church is properly registered and in good-standing according to local regulations, and that these charities own the bank accounts they provide to SunCash, ensuring funds you donate reach the charity you selected. Whenever you donate through SunCash.Me, we have confirmed that the charity or church is real.</p>
        </div>
        <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTen">
          	<a class="card-title">I forgot my password. How do I reset it? </a>
        </div>
        <div id="collapseTen" class="card-body collapse" data-parent="#accordion" >
            <p>From the login page, click the “Forgot Password” link and enter your username (SunCash.Me link).  You will receive an email to your registered email address on your account.  Follow the instructions to reset.</p>
        </div>
        <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTen">
          	<a class="card-title">How do I view or edit my account information? </a>
        </div>
        <div id="collapseTen" class="card-body collapse" data-parent="#accordion" >
            <p>Log into your SunCash.Me account, go to My Account to view or edit our account information.</p>
        </div>
      </div>
		</div>		
	</section>


	<section class="cta-banner">
		<div class="container text-center">
			<div class="row">
				<div class="col-12">
					<div class="banner-text-container">
						<h1 class="banner-header">Start using SunCash.Me today.</h1>
						<p>SunCash.Me is the fastest, safest and simple way for your family, friends or customers to pay.</p>
						<a href="#" class="btn btn-secondary">Create Your SunCash.Me Link</a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<footer class="py-3">
		<div class="container">
			<div class="row">
				<div class="col-12 col-lg-6 d-flex align-self-center text-left">
					<span>© 2016 SunCash.Me. All Rights Reserved.</span>
				</div>
				<div class="col-12 col-lg-6 text-right">
					<img src="{{base_url('assets_main/imgs/footer-logo.png')}}">
				</div>
			</div>
		</div>
	</footer>

	<script src="{{base_url('assets_main/js/jquery-3.2.1.min.js')}}"></script>
	<script src="{{base_url('assets_main/js/bootstrap.min.js')}}"></script>
	<script src="{{base_url('assets_main/js/main.js')}}"></script>
</body>
</html>