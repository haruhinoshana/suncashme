<?php
//loading assets helper
defined('BASEPATH') OR exit('No direct script access allowed');
$assetHelper = new AssetHelper\AssetHelper();
?>
{!!$assetHelper->link_plugins('css','modified/css/bootstrap.min.css')!!}
{!!$assetHelper->link_plugins('css','modified/css/main.css')!!}
{!!$assetHelper->link_plugins('css','modified/css/plugin.css')!!}
{!!$assetHelper->link_plugins('css','modified/css/all.min.css')!!}
{!!$assetHelper->link_plugins('css','modified/css/tab.css')!!}
{!!$assetHelper->link_plugins('css','modified/css/tempusdominus-bootstrap-4.min.css')!!}
{!!$assetHelper->link_plugins('css','modified/css/uppy.min.css')!!}


<!-- {!!$assetHelper->link_plugins('css','bootstrap-3.3.7/css/bootstrap.min.css')!!} -->
{!!$assetHelper->link_plugins('css','datatableBS4/datatables.min.css.min.css')!!} 
{!!$assetHelper->link_plugins('css','datatableBS4/Buttons-1.5.2/css/buttons.bootstrap4.min.css')!!} 
{!!$assetHelper->link_plugins('css','datatableBS4/Responsive-2.2.2/css/responsive.bootstrap4.min.css')!!} 


{!!$assetHelper->link_plugins('css','font-awesome/css/font-awesome.min.css')!!}
{!!$assetHelper->link_plugins('css','sweetalert2-master/dist/sweetalert2.min.css')!!}
{!!$assetHelper->link_plugins('css','bootstrap-datepicker-master/dist/css/bootstrap-datepicker.min.css')!!}
{!!$assetHelper->link_plugins('css','curioussolutions-datetimepicker/dist/DateTimePicker.css')!!}
{!!$assetHelper->link_plugins('css','dropzone-master/dist/dropzone.css')!!}
{!!$assetHelper->link_plugins('css','cropper/cropper.min.css')!!}
{!!$assetHelper->link_plugins('css','override/tab.css')!!} 
<!-- override -->
{!!$assetHelper->link_plugins('css','sidebar-nav/dist/sidebar-nav.min.css')!!}

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->