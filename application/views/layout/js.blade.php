<?php
//loading assets helper
defined('BASEPATH') OR exit('No direct script access allowed');
$assetHelper = new AssetHelper\AssetHelper();
?>
{!!$assetHelper->link_plugins('js','jquery-1.11.3.min.js')!!}
{!!$assetHelper->link_plugins('js','bootstrap-3.3.7/js/bootstrap.min.js')!!}
{!!$assetHelper->link_plugins('js','sidebar-nav/dist/sidebar-nav.min.js')!!}
{!!$assetHelper->link_plugins('js','override/jquery.slimscroll.js')!!}
{!!$assetHelper->link_plugins('js','override/waves.js')!!}
{!!$assetHelper->link_plugins('js','override/custom.min.js')!!}
{!!$assetHelper->link_plugins('js','sweetalert2-master/dist/sweetalert2.all.min.js')!!}
{!!$assetHelper->link_plugins('js','bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js')!!}
{!!$assetHelper->link_plugins('js','curioussolutions-datetimepicker/dist/DateTimePicker.min.js')!!}
{!!$assetHelper->link_plugins('js','dropzone-master/dist/dropzone.js')!!}
{!!$assetHelper->link_plugins('js','cropper/cropper.min.js')!!}
<!-- {!!$assetHelper->link_plugins('js','bootstrap-session-timeout-master/bootstrap-session-timeout.min.js')!!} -->

{!!$assetHelper->link_plugins('js','global/global.js')!!}
<script>
var base_url = '<?=base_url() ?>';
</script>


