<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$assetHelper = new AssetHelper\AssetHelper();
$ci = &get_instance();

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta charset="utf-8" />
		<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>assets/plugins/icon.ico">
	<meta name="description" content="overview &amp; stats" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
	<title>@yield('title')</title>
	@include('layout/css')
	@yield('custom_css')
</head>
<body class="fix-header">
<div id="wrapper">
		@yield('content')
</div>
</body>
	@include('layout/js')
	@yield('footer')
	@include('layout/modal')
	@yield('custom_js')
</html>