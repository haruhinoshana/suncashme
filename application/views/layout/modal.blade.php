<!-- global modal  -->
<div class="modal fade bs-example-modal-lg" id="modal-large"  data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
<div class="modal-dialog modal-lg" id="modal-content-large" role="document">
</div>
</div>
<div class="modal fade bs-example-modal-md" id="modal-medium"  data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
<div class="modal-dialog modal-md" id="modal-content-medium" role="document">
</div>
</div>
<div class="modal fade bs-example-modal-sm" id="modal-small"  data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
<div class="modal-dialog modal-sm" id="modal-content-small" role="document">
</div>
</div>