<?php
//loading assets helper
defined('BASEPATH') OR exit('No direct script access allowed');
$assetHelper = new AssetHelper\AssetHelper();
$ci = &get_instance();
?>
@extends('layout.main')

@section('title', 'SunCash Customer')

<!--for css to be use on page it can be include or css path -->
@section('custom_css')
{!!$assetHelper->link_plugins('css','modified/css/main.css')!!}
<!-- {!!$assetHelper->link_plugins('css','modified/css/fonts.css')!!} -->
@endsection
<style type="text/css">
  body{
    min-height: 100vh !important;
    background-color: #FF8400 !important;
  }
</style>

@section('content')
<div class="container">
    <div class="row d-flex justify-content-center" style="height: 90vh;">
      <div class="col-lg-6 col-md-10 d-flex align-items-center login-card">
        <div class="card d-flex align-items-center">
          <div class="card-body">
            <a href="<?php echo base_url();?>">{!!$assetHelper->link_plugins('img','/media/logo.png','alt="Suncash Logo" class="logo"')!!}</a>
          </div>
          <form action="{{base_url('wallet/login')}}" class="mb-5" method="POST">
            <div class="input-group mb-3">
              <input type="text" class="form-control bfh-phone" id="mobile" name="mobile" data-format="1 (ddd) ddd-dddd" value ="1 242" placeholder="">
              <div class="input-group-append">
                <span class="input-group-text">{!!$assetHelper->link_plugins('img','/media/user-icon.png','')!!}</span>
              </div>
            </div>

            <div class="input-group mb-3">
              <input type="password" class="form-control" id="pin" name="pin" placeholder="Account Pin" maxlength="4">
              <div class="input-group-append">
                <span class="input-group-text">{!!$assetHelper->link_plugins('img','/media/password-icon.png','')!!}</span>
              </div>
            </div>

            <div class="form-row">
              <div class="col d-flex align-items-center">
                <div class="text-center">
                  <label class="c-gray font-weight-medium link-text"><a href="" class=" btn-link">Forgot Password?</a></label>

                </div>
              </div>
              <div class="col text-right">
                <button type="submit" class="btn btn-primary btn-block">Sign In {!!$assetHelper->link_plugins('img','/media/arrow-left.png','')!!}</button>
              </div>
            </div>
            <hr>
            <div class="form-row">
              <div class="col d-flex align-items-center">
                <div class="text-center">
                  <label class="c-gray font-weight-medium link-text">Need An Account?</label><br>

                </div>
              </div>
              <div class="col text-right">
                <a href="" class="btn btn-border-primary btn-block">Register</a>
              </div>
            </div>
          </form>
        </div>
      </div>     
    </div> 
    <footer class="text-center">
      <p>© Copyright Suncash <?=date('Y')?>. All Rights Reserved.</p>
    </footer> 
</div>
@endsection

<!--for js to be use on page it can be include or js path -->
@section('custom_js')
   {!! $assetHelper->link_plugins('js','bootstrap-formhelpers-phone/bootstrap-formhelpers-phone.js') !!}
   {!! $assetHelper->link_plugins('js','bootstrap-formhelpers-phone/bootstrap-formhelpers-phone.format.js') !!}
<script>
var session = '{!!html_entity_decode($ci->session->flashdata("msg"))!!}';
//alert(session);
if(session!=''){
    swal(
    'Opps...',
    session,
    'error'
    );  
}

</script>
@endsection

