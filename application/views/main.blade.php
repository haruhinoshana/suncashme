<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>SunCash.Me</title>
	<link rel="stylesheet" href="{{base_url('assets_main/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/main.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/responsive.css')}}">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>assets/plugins/icon.ico">

</head>
<body>
	<nav class="navbar navbar-expand-lg sticky-top bg-white shadow-sm p-0">
	  <div class="container p-0" role="navigation">
	  	<a class="navbar-brand" href="{{base_url('')}}"><img src="{{base_url('assets_main/imgs/header-logo@2x.png')}}" alt="Suncash Logo" class="d-inline-block align-top" width="200"></a>
	  	<a href="#" class="menu-button"><span></span></a>
	  	<div class="justify-content-end main-menu">
				<ul class="navbar-nav d-flex">
		      <li class="nav-item">
		        <a class="nav-link active" href="{{base_url('')}}">About</a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link " href="{{base_url('info/faq')}}">FAQ </a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="{{base_url('info/terms')}}">Terms</a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link " href="{{base_url('info/contacts')}}">Contact Us</a>
		      </li>
		      <li class="nav-item button-item">
		        <a class="nav-link btn-link btn-primary" href="http://dev.suncash.me/business/">For Business</a>
		      </li>
		      <li class="nav-item button-item">
		        <a class="nav-link btn-link btn-secondary" href="http://dev.suncash.me/customer/">For Customers</a>
		      </li>
			 <li class="nav-item button-item">
		        <a class="nav-link btn-link btn-primary" href="http://dev.suncash.me/charity/">For Charity </a>
		     </li>
		    </ul>
		  </div>
	  </div>
	</nav>
	<section class="main-banner">
		<div class="container">
			<div class="row">
				<div class="col-12 col-lg-7">
					<div class="banner-text-container">
						<h1 class="banner-header">Your <strong>Personal Link</strong> to <strong>Receive Money</strong>!</h1>
						<p>Create your link, share it, and start receiving money. Repay friends or accept payments from customers. Everyone can use it.</p>
						<div class="dropdown">
						  <button class="dropbtn btn btn-primary">Create Your SunCash.Me Link</button>
						  <div class="dropdown-content">
						    <a href="http://dev.suncash.me/business/signup">Business</a>
						    <a href="http://dev.suncash.me/customer/signup">Customer</a>
							<a href="http://dev.suncash.me/charity/signup">Charity</a>
						  </div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</section>

	<section class="features">
		<div class="container">
			<div class="row">
				<div class="col-12 col-lg-8 p-5 order-lg-last">
					<div class="row mb-3">
						<div class="col-12 col-lg-2">
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="64px" height="64px">
								<image  x="0px" y="0px" width="64px" height="64px"  xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAAB3RJTUUH4goQBisxrtLe9gAACEBJREFUeNrtm31sVtUdxz+/c2/7FGgFW97aIh20vA5xCsPYKeuI+EZUXJDNLVnMtrhIAIdmY7osWfaHyWaii0Pd4qbGiM6WQsGYhchmNiA4V16sQAuUlQKlvLUdtqVPnz73nP1xb5+3tuDTp33uXPkmp89zzz333u/ve875/X7nPj3S2RWmH2QCy71yG1Dg1X2R0AGcBvYDW4CtQCixkfQjwEPAb4ASvy0YYtQD64HNsZUq5rsF/Npr8P9mPJ5NlZ6NVm+lHdPgWeCnfrNMA3ptXA/RKbACqPCbWZrxMLBJOrvCmcBxYIrfjNKMJmC68pQYacYDFAIrFW6oG6lYroCv+s3CRyyUzq5wEAj4zcQnhNQINh4gU6V+jy82rgngNwG/cU0Avwn4jREvgJ36LcDe+XPouoCe9yi6oNRvm5KCdHaFTUp3CHWQ8eatiNMJYjC5JYSXvITJne23bZ8LKY8AdbQKerowIgggrfXIuWoATE4hZOT4beMVkfIIsP+8DGmtBTEgBvE+EQPKQCDHFSK7EJNzA7roPvQkb5r0fIa68DGqpRrpbER6/oMJjENPKEUX3IsJjO+fdOgicrkBPS71ZUxKAkjzPuzKlVGDewVQJq4u9pzJnUP4xjVYJypRzX8DE4oXTQwIoAQ98Xb0lAdxCpZhsvJRbdXYDX/AairHjJlKcEkNbmOfBLA+WI+q2xzf44mjIKbomStRZ3chnaf6iDTQNYhroh47C9V+OCqQGLpLt6PHL05JgMGHweAl5Mj7YMTrBXG/G8GY6PdIXe481MkdSEdTXNsrXeMWMBjUZ3Ux1wEG7MY/pWR8SgKo2ipwetwD07eYxOO8+RBs7VPftwwsBIY4sa0zVUioJSUBBhkFDPLJu27PMfAsjJzLyEIuNXijBYyyMdPux9gBsDJAd2M1lGNMCPeOsZD4e/WeFwHdg3XyLcIlT6RXADn1L6T1hEsijly/3NGFZajG7ZH2etpDqOMVqN55btmE5z+Jfeh5T4REGd2bGRP7HLfebnyNcMlaBusMBzUF1Cfl/Qz5mOEaM0yNETcXiB3ewbb4tjqMffB3OHNWA9YA/iDW7Gi9tB9DXdw5KOMHJ8DlVuTojqiRkRLtqzhhxk6D07six7rgDlTTbs+5xbQNB7EO/x7ny+tcWgP6Aol5kPts+8Tr6RNAPq0CJ3wVgjHefdJCpOO826uBPFCZ4ITiR0hvCXdj1b6KM2d1P86yv4jhnrPObEVCbWkQwGjU/gqPwNXIeUUyMJMXoouWoid+BXXywyuHwFAH1rGNOPOecutjRliUR8I1TjfWqY2DEiCpREga9qDKf+Tx6Zu8mCkLYWwBxrKwDm+KnssMgBME0QMmPJKQTJms69HFK7HqXr5ykiRuL5jrZhK8c1/SAiQVBWRfuee4DEhCCByVC1ojh7citkLPfhCUAmWh6t+LxPC+EcNE/opEDpFgG+rEVpzZq7HqNgzg43sbg7QfRbXsRud9LSkBkpsCmWNAMiLzT5f9BDO9DIygZ9yFnNnvssnKxeRMQR3egjpUgS5acoUkKTrMTYKjk85m1MltOHOfGDBRivUHdkPyzjD5tUDHRdT+cjiyA/3oO2Bnoj7dgtr5AnS1ujn/4qdQe34L2l3oOKXrkIu1rvfvbvtci6a4qXVdEfpLy7EPPd930RR7jZVJ8L56TOb1wzQCALLHo+9Yhf7hZrADbs/lz4fONrd3C25BTuyCsBsp9Mz7Ucf/ijr2F/TUskiPGwaOHE7xSkxOcaSttDeiTm8nPPMxiAm9JmsyWKMA3Ps5IayTbydlzpC8EzTji9E3fwdTVIqZeQ/S+E93uGZkY3IK4GyNl7Sc4WqLJoyg6jejpy1HT1gYTXja6lAXPiY863FX2MlLCd29C+eGFTGLpuSnwZC8EwRB3/kMANaW1fS6OX3z91B7X/WODXr63UhgLIzOQ9W+4/VcP0ms0agDL6Dn/gAdGIdq+sB9SksNYo8mVLYJnV8GCE7x97Ea3nI55C/FKXksOeYpvxNMgKqpRO34FaZoEYQ6kLMH3Hk8617k3AGk/TTYmZgJs5DzB6JzOJCNyZuBnK+Of4dQ8k0QhfXvdyN1oRW1mKzo2yL7yAacwmWY7GnJd91QCwCAE0I6zmP98S63e0ePQ899ALX/dfet0MS5SNd5uHwuEvtFDCZ/AWbMJFTDewn5xdcxeTdiHXwRnV9Kz9LNKVOMdNiQGw9gZWJy8jFTFoE9GmfxetSBjZFFkxk/BzoukLhokua90NWCc9OaeJ9w+u8QDtL9yDF6llYOKdXh+2FEWTjfeoPw2moYN9VdPyCYGfcgR7a5bQzoWx6HjOxIfqCaPkLO7SNc9qLr4XsXTbrHe8Oc2jvARAzPFEiE0ag9L8PlC0jHGaTxH9FYnxHAWbQWq+Y16DoXiem68DacW5/G3v0zyBhF+BuvYEZPGnJq6REg9oEt9aj3f4y0HYuKYFk4tz+DOvgmcuk44iVIPd/9CDMmf1j5pP23QZNXgvNIBXruimge4DhYu55FL1iFyV+EUVk4pb8cduPBhxEQ9/C6bVgf/gIJX3a9/eSbCK+oBKNB0tM3vv46bGY/gPPtLZiJ88Eeg16wylMmfbR8HQH/Cxjx/x9wTQC/CfiNawL4TcBvXBMAaPebhI8IKaDZbxY+olkBNX6z8BHVCqjym4WPqBrpm6aKFe520if9ZuMD1gHdvWGwAnjOb0ZpxHOezXF5wNPABr+ZpQEvebaSKIADrMHdRVrvN8thQD3uHsnVnq1A/7vHwd0q/zDuTvIFuJsMM/y2IEn04Dq6vbjb5yvoZ/v8fwHYGgCE+ajEpgAAAABJRU5ErkJggg==" />
							</svg>
						</div>
						<div class="col feature-text">
							<h3>For your Business</h3>
							<p>Customers can secure their orders and book services by following your link. With your business account both you and your customers benefit as SunCash.Me is more secure for everyone.</p>
						</div>
					</div>
					<div class="row mb-3">
						<div class="col-12 col-lg-2">
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="64px" height="64px">
								<image  x="0px" y="0px" width="64px" height="64px"  xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAAB3RJTUUH4goQBjQYITpIBAAACC5JREFUeNrtm3twVcUdxz+759wHjxAID4EQgrzCowNRw7tQpGmdKsMQlTpqGZy2jE5rqWIVnbG2/mMVykyn08o4VaZjFcuAPK20gIBSlFYQSi1gDJRXAkbDIyS5yb3n7PaPvTn3JqQK5N5zEPzOnLn37Dm75/v7nt3f/nb3rKiPObSBMDAzeUwA+ibTvkyoA04Ae4DVwFog3vom0YYAZcBCYHDQFmQYFcACYFV6okz7bwHPJW+42ownadPrSRut5kQ77YZngMeCZukDmm1cAKkmcCewImhmPmMWsFLUx5wwcAjoFzQjn1EJDJRJJa414wHyge9KTFd3rWKmBMYEzSJAlIj6mNMIRIJmEhDiMkjjQ3seJ7qiK6H3ZgdFISzbX8Zlwm3EKn8BNFgn1iFilYHQsNtfxOVBuE2gRSpBudeWAABoT47AKATWBLRneHDGZ1QAcf444tx/L00ELUwz0JcmgkicRSTOXDkCiKNbsV+9Gfu1aVh7l1xcpnAuOmcIWgt0KA8d7XVR2axTbxDdOJDo3wqwTq1rN/eM+AD5yT5QCgTInc8BGrf4R1+YL1G6Glm5EdV7MljRLza+ag3hXXNAx0EYMdzeM9rHPRMCqGF3QCTPq87WzoVYuxZ/bh5x7mNEzb/QkR6IcxWIhs/vBq3jywn/czYoB6N0B5z+3283d1Efc3S7SwFEzUfYa+6F+GkQGiE0zpRnUMPvSd0Ur8U6sBSr4s+IuiMgNYi0o2Nv3AFluEPvR3dMjc/kJ28R2XE74Jr77ChNE15H9Zjabt4Zc4K6exHOzFe9mqC1QFa+m3rQgT8RXj4ea/cixPljQJoDbD4aTmEfeJ7IG2OwP3wWVAIAq2oDWiuTx+pI0/hVGTEeMlgDvALPVGBtegjiZ3GnLUb3KcHeMg95eJ15e1JDuDO6/zdRvUrQnfogEnWImt3Io6uR8c+SvaNG9ZpIfPIy5LkPCe24CyEl8fEvo3p+I3N8My1Aa1hbHsH6eKUx3LJxb3gQ92tzIZRj2rNTD3ZHkCFwm7DKXyL072fBrTMi9BxH/OZ1ePGCDGWUX1YFkAdWYL39GEJoiHYh8Z0/onvdiDy8Fmv/S4iavQidAClQvUpwh/8QVTgDUVtOeOudiNgJEBpn+DycUU9nhWP2BHAaCb0yFWLVIAXu9JdR+ZOwty9AfrTMc3yiuVkkz1XhdBKTX0DUHiKysRTcerBsmm77oIVjzNhLyorxgCxfD/WfggY1pAyVPwl5+C/IA6+ZOFiDKijFHVjmnaNBHl2PtX8JOrcIZ8R84xxdB6tiaXZ4ZksAcXiz591V8f0m7chbaaGvQA0sQxXe2jIk1gJ5fAMAztAfgIyYIXPVpi+XAPLkPtMd5hSg84aYxFAOzQMg3aE3qmAa8sQ2r9v0RBDJOZpQF9yeEwGBOLsfnIaM88zOcDgRQzecRgiBzhvmJbvjHkXn9EW4jahhdyEaqpHlK1sOhoTEHfGAd6pzh6FPbUOgEQ2V6C5DghdAvPcicvcycBtTjkyA7jEYd/pimge7GiDcKZUx3NlrDgD2hjngul4Pp/JG4I55EpU/NZUnlGtqCCDcRoifwX7/IWT9IeM4wfyGOuAMegC336zsCyDffh4vLPUOEJV7kAffRBXf7b1VUfdp24U4Dchj25LGC9wxj+OO/jEXzA/UnfAmTnS4K9ahV7COr2/1bPMSwmf3EbtEAS7LB+j+Jck/Iq39AjKE7lMMdhTdbZC5XrUXGs+18eQwRLoDAt19ZNvGawdZ9Y55Rrg7ulMBOmdQi14jnYPqdOlrupcXB7gJxOkj3hCYxlrDt/v10KmHse/d3yL/scT07cX3oKY+dWE5DdXImoOo626AcM4Fl639f8B+/+cgNe7gu3Em/MaUXfMBovZg0gKMElYE1ftb6FCuDwJcDGJnsJfeAvFaI8K0X6BGpY0M0YjKnYjYZ6h+kyCa1yK7PLaR0La5ZuwvJfEZ29C5mXWAkM05wQ7dcEt/SXO3J7c+jaz4q3fZ2vwo9tp7sTbNI7S8FHH+eIpU5XbsLXPRbgI0OKMeyYrx2RUA0EW3oibNTwU4m59E1FcjzlciD65JGwafQe5fZjI5Max3fmZ6By1wi+bgjn44axyzPiusxs5FFX/PGNpYh9j9YlKd1vMBpiXK8pWIupMmguxXijPhV2Rz5tiXaXE16adgmZBWVGxG5+SjimZ6ITHRPNQI4x/koTc9UdxxT2XVePBrYSTcGZ1fgji2A1FbBbHTuN9eiBpehojVoPp/HaLdQCtE9T7QoPOK0LmDsk7Nt4UR3femlC/4zypETQXkFqIKpyASDYjTFcjytRBvMFPleSN94eXb0pjuW5yM6ARy+2L4+6/Twuj0w1R51XeiL7z8qwGFE1Ajb78gims9FAaBGlCKGnrH1SUAgB5+G956YHP4TNoaafJEX38LSOvSH3AZ8H91uHno2+zc24xD/Vsw9VUAnX/j/10S1+ma9PTHAYLfy+N2BNV/fKr6tzGi090Go/OGXqUCALpgDLqFH2gphrquGIR/tPwXYOx96D6jjfen5aG7DkBNecJXPv5/IWJH0GPv80Jjrwlgo4tnQ6SLr3SyvjR2pSO4z+SuEHwlQNAEgsZXAgDngyYRIOISOBk0iwBxUgL7gmYRIHZJYE3QLALEmmt909QgidlOOj9oNgHgYaCpuRtcASwKmpGPWJS0uUUc8ATwu6CZ+YDfJ22ltQAu8BPMLtKKoFlmARWYPZIPJm0F2t49Dmar/CzMTvKbMJsMM/uFYvaRwDi63Zjt8ytoY/v8/wAwlzDFSBAKFAAAAABJRU5ErkJggg==" />
							</svg>
						</div>
						<div class="col feature-text">
							<h3>For your Side Hustles</h3>
							<p>Whether you bake pastries on the weekend or sell straw products online, invoices and awkward payment reminders are a thing of the past, simply share your link and get paid.</p>
						</div>
					</div>
					<div class="row mb-3">
						<div class="col-12 col-lg-2">
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="64px" height="64px">
								<image  x="0px" y="0px" width="64px" height="64px"  xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAAB3RJTUUH4goQBjUboSgo/wAACIxJREFUeNrtm2uMVGcZx3/Pe2ZnZnfZG1tYLsu15VbA0oKYUqsmprFpxICFfmhN7Bf90qpoG4k1xloviaIfTFQ+2ERN/VJppY1RY2yptikRBCqkQC906XJbusuy19mZM3PO+/jhPTM7wy71A4edQfgnJ7v7nve88zz/81zf2Vcy2YBJkAQ2RdedwJxo7FrCKHAGeAPYDbwI5C+dJJMQsBn4CXBLtTWIGSeA7cAfywdN2e8e8ONowv+b8kQ6PR/p6BUHE2UTfgR8s9pSTgGKOm6HcRfYAuyqtmRTjK3Ac5LJBkngPaCz2hJNMc4Ci03ExPWmPMBc4AGDS3XXKzYZ4KPVlqKKWCeZbJADUtWWpErIm+tYeYCkufI1rm3cIKDaAlQb1z0BiStf4kMQ5jC9+5EL+zGDx5HsacT/AMn1AD4YD5KNkJ6BNnRim5dh29Zi2z+O1k9NbSaZbKCxL9p3GO/403in/gI2i53xEbT9NrR1Kdp8M4gghSEQgyZbEb8XGTuJGTyI6X8VKfRjp68j7HyIYN4Xwau/NgiQkdN4e7+LOf13tKUTu+pL2EWb0PT0inmJt3eS+M8TIIq/qQtNlt1Xi7m4l0T303jn/gDJ6RSWfYdgwZcBqV0CpPsVEi89ioRjhOu+RrjmETB14/cH38J79/fIxYN4Q0fBjoIoiKKpNmiYibYsp7DqKbRhAQBm+Ah1hx7GGz1KOHsT/u3PgIl3YyoWAqTvKInntwAFwnt3YhfeU3Hfe/sZEvu2AyEYRVuXoB13oul2JBhCMt1I5h1ElNw9+0HKQlOYIfWvz+IN7CWc+4AjIUbEEgS913dAUMDe9oUJygOYY78FqyACCuGKRwgXba2wEAdlgpl7jeTv+B3pPbfinX0Ws+ir2Nb42pdY0qCc3gcq2CWfm/R+ePtjqNcIKqBCYu82UruWUbdnC4kjP8Cc+xsEGS7n41o/H9u2ARC8/tdiUx5isgC1FhGBcNIdZuzC+8jPuRtz5iXM2ZcxFw4go12Ynn/C+X+QEAvJRoKVjxEs3zb5h9gCqgJhpvYIQAUFzLHnCOeun3xOsgm7eDN28WYAxB9Azr9G4sB2xO+FQobE4e8Rzr8fbZhX8agZOY7p3+cMROPNBLERgEkgx17EzFqDXf3gxA96/Qlk8B20ZRE0tLtaYKQL8YdRFUTBzrwLTc+qeE78PpL7HgIMmHSsysdMQApSrXivPIlcOE644XFItbj7oQ+ZPhjrw/S/CcEIYqxLgW1L0PZVhPM3Yjs/A1Lascb0/JXkG9uQYBCtX4hku0DDWAmIJQ0mfrbS5XQj0NSOhFnQALvy89glG9GONRXzvf3fxzu6ExHFf7incrHCMN6ZP+O992vM4GGXKtVHgn4QxbaswP/Eq+A11BABP10VFTUWjEJdChqnQ8JD8sNgwM5eCzctR1vmQbIZEikojECYQ3IfIKPvIwOH8YaOog0djsxwxPUNooiJ1hclWPo4hVufqh0CvB2rkVSDU0jUkSDqzDzViE6bCZ5xYwQu+RoPKCCiQMEpLCFYHzPWXVqjtF4ijdiMc5v6meTuPRkLAfHEAEmAn4GmDvCHwGYBUCuIn0HyXZFC1ilsyqylSFSZsiouKBahqQ7EjpYygGR7YxEbYiqEdP7HXI4e6QMvDdPmODOPCh9UwLqfWj4WXWrNxDEVtGEBWj8PyfVDfrTy2ZgQUy9wAvObLUA4/obT0yCZRhMJxIBkzoPmSm9ZIn8umbqxUN8OqUag4NLkWHc0z1a4hIiSvX8kFgLiqQRn3AJNs2D4HCpOTnKj4A9HwUvR1nmQTIPmAYvWJRFCIA/GgPqIfwEZ7i4FO2ef4i7FdYKeh9r4qsH4doRyY5F5FlsaHRccYPD0BF9HbIkgSvfEBUQoW0vQxoVQGEAKA2j9TTVGQOCDP1qmcFlXVx7MrCDFqKO4ORb3pq2AiZ4s+rhXjzZ0oASYkaJlCLZ9Q20RIKcOuXbX80ADyt5/BQEVFhERoIB4aTTZAOlm0DxqLIJFCkPI8PsgFjXOEpzUTbVFAD1vReZvIN0GuYGSjnIJAaogdfWuPkgkXHwwiskPwGBXZUFVrC4jS3EkKEh8X2bF5AJ5R0AYQnbEBcQwB7kBp3AZAShoPocUsuMxwSgq6lpqA84XbBmB5e4kaPOyGiOgaMqKc4XhXkimoHUBSIgGY8hYH+UuUOHrURxQygsgzzU+lkuCohDO/nStEQCoQZMpJBhzfxdyMHDKsZJqdD2A8UACl/aMuBpB85FmPlJwfQNagGAYRCqJKlpAuqPGCEg3gyqYOrR+OpK9WHnfz0B+tKwM1pKvj6dB97eWGh8tvXnVKPxZHHlejcUAXbjWRejsiBOwqQMI3R5A+TwuExRLyhUVjiJ+ca6VKACCbb/DldsxIZ7vBjuWoKvvw/m4RYd7YXQAmudAyzxo6YRU6yW9ABV1/2S9QLF/KI1JHeG6J2NTHmKMAbrx2xD4yPGXHQ9qkcGeqJJVaGxFZywFQrdDVJeCMAueuLFgDKEA/gDY0MUIihZjwCQJPvlz7Mz1VyLmBMT83aAiB3cje34FucFS48Ikl1SUxOPlsExSKuvsdQR3/xBtiy/9XSUCIuTHkEO7kcN/gr53P5yEywXFuiR20aewKx9EO++KXcSrS0A5Bs4i3f9Gzr0JF08ig6eddYS58a2u+ia0sR3aOtGZK9DZa9DO9ZC4et8KTx0BNY7r/j9EbhBQbQGqjRsEVFuAauMGAUA8+8vXJvIG6LniZa5d9BjgSLWlqCIOGOCFaktRRbxwvR+autngjpN+o9rSVAFfB/xiGtwF7Ki2RFOIHZHOFXXAt4BfVFuyKcAvI125lIAQ+AruFOmJakt5FXACd0by0UhXYPLT4+COym/FnSRfiztkWPe/P6OmUMAFuoO44/O7mOT4/H8BQ+TLiQ28WGgAAAAASUVORK5CYII=" />
							</svg>
						</div>
						<div class="col feature-text">
							<h3>For your Personal Transactions</h3>
							<p>Share your SunCash.Me link with all your friends and family members. Dining out and splitting the cheque? Contributing for a gift? Reimbursing a friend? Donating to charity? Tithing to your church? SunCash.Me makes it all easier.</p>
						</div>
					</div>
				</div>
				<div class="col-12 col-lg-4 order-lg-first">
					<div class="img-container">
						<img src="{{base_url('assets_main/imgs/iphone@2x.png')}}">
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="how-it-works">
		<div class="colored">
			<div class="container">
				<div class="row">
					<div class="header-block">
						<h2>How SunCash.Me Works</h2>
					</div>
				</div>
				<div class="row">
					<div class="col-12 col-lg-6 order-lg-last">
						<div class="img-container">
							<img src="{{base_url('assets_main/imgs/laptop-screen@2x.png')}}">
						</div>
					</div>
					<div class="col-12 col-lg-6 how-item pt-5">
						<div class="number">01</div>
						<div class="description">
							<h3>Create your personal link.</h3>
							<p>You have complete freedom to customize your very own personal link to instantly start accepting payments. Signing up is fast and free even if you don&#39;t already have a SunCash account.</p>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="easy-share">
			<div class="container p-4">
				<div class="row">
					<div class="col-12 col-lg-5 offset-lg-6 how-item with-bg">
						<div class="number">02</div>
						<div class="description">
							<h3>Easily share your link</h3>
							<p>Share your link any way you want to – via email, SMS, in a chat, or even on your business webpage or card.</p>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="paid-instant">
			<div class="container">
				<div class="row d-flex align-items-center">
					<div class="col-12 col-lg-5 how-item">
						<div class="number">03</div>
						<div class="description">
							<h3>Get paid, instantly.</h3>
							<p>Family, friends and customers can follow the link, enter the amount to be paid and that’s it! The funds should appear in your SunCash account in seconds.</p>
						</div>
					</div>
					<div class="col-12 col-lg-5 offset-lg-2">
						<div class="img-container">
							<img src="{{base_url('assets_main/imgs/paid-instant@2x.jpg')}}">
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="why-suncash colored">
		<div class="container">
			<div class="row">
				<div class="header-block">
					<h2>Why use SunCash.Me?</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-12 col-lg-4">
					<div class="card">
					  <div class="card-img-top">
						  <img class="card-img-top" src="{{base_url('assets_main/imgs/placeholder-image001.jpg')}}">
						</div>
					  <div class="card-body">
					    <h5 class="card-title">Easy to use</h5>
					    <p class="card-text">Super simple like child’s play, just a click. Plus it’s an easy way to remind someone you’re waiting on their payment. Share your link and that’s it.</p>
					  </div>
					</div>
				</div>
				<div class="col-12 col-lg-4">
					<div class="card">
						<div class="card-img-top">
						  <img class="card-img-top" src="{{base_url('assets_main/imgs/placeholder-image002.jpg')}}">
						</div>
					  <div class="card-body">
					    <h5 class="card-title">Instant Payments</h5>
					    <p class="card-text">Eliminate running down customers, writing cheques and making tedious bank transfers from your life. With SunCash.Me, anyone can pay you instantly.</p>
					  </div>
					</div>
				</div>
				<div class="col-12 col-lg-4">
					<div class="card">
						<div class="card-img-top">
						  <img class="card-img-top" src="{{base_url('assets_main/imgs/placeholder-image003.jpg')}}">
						</div>
					  <div class="card-body">
					    <h5 class="card-title">Safer for you and your business</h5>
					    <p class="card-text">You and your customers benefit from our Protection Programs when using SunCash.Me for business or personal use. More security for everyone.</p>
					  </div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="cta-banner">
		<div class="container text-center">
			<div class="row">
				<div class="col-12">
					<div class="banner-text-container">
						<h1 class="banner-header">Start using SunCash.Me today.</h1>
						<p>SunCash.Me is the fastest, safest and simple way for your family, friends or customers to pay.</p>
						<!-- <a href="http://dev.suncash.me/customer" class="btn btn-secondary">Create Your SunCash.Me Link</a> -->
						<div class="dropdown">
						  <button class="dropbtn_secondary btn">Create Your SunCash.Me Link</button>
						  <div class="dropdown-content">
						    <a href="http://dev.suncash.me/business/signup">Business</a>
						    <a href="http://dev.suncash.me/customer/signup">Customer</a>
							<a href="http://dev.suncash.me/charity/signup">Charity</a>
						  </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<footer class="py-3">
		<div class="container">
			<div class="row">
				<div class="col-12 col-lg-6 d-flex align-self-center text-left">
					<span>© 2019 SunCash.Me. All Rights Reserved.</span>
				</div>
				<div class="col-12 col-lg-6 text-right">
					<img src="{{base_url('assets_main/imgs/footer-logo.png')}}">
				</div>
			</div>
		</div>
	</footer>
  	<div class="modal fade bs-example-modal-lg" id="announcement_modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
        <div class="modal-body" style="padding: 0px;">
                <img src="{{base_url('assets_main/imgs/Christmas Card.jpg')}}" style="width:100%; height:100%;">
        </div>                  
        </div>
    </div>
    </div>
	<script src="{{base_url('assets_main/js/jquery-3.2.1.min.js')}}"></script>
	<script src="{{base_url('assets_main/js/bootstrap.min.js')}}"></script>
	<script src="{{base_url('assets_main/js/main.js')}}"></script>
</body>
</html>
	
<script>
	$("#announcement_modal").modal('show');
</script>