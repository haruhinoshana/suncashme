<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Merchant_model extends CI_Model  {
    protected $response = array();
    protected $token_info = array();
    protected $session_timeout = .5; //hours
    protected $user_info = array();
    protected $user_permissions = array();
    protected $last_result = array();
    protected $user_types = array(1 => 'Merchant', 2 => 'Admin');

    public function __construct() {
        parent::__construct();
        $this->load->library('crypt');
        $this->load->model('cardholders_model');
        $this->setTimeZone('America/New_York', $this->db);

        $cmd = "ALTER TABLE clients ADD COLUMN IF NOT EXISTS registration_status varchar (5) DEFAULT 'F'";
        $this->db->query($cmd);
        $cmd = "ALTER TABLE clients ADD COLUMN IF NOT EXISTS verification_code varchar (100)";
        $this->db->query($cmd);
        $cmd = "ALTER TABLE clients ADD COLUMN IF NOT EXISTS verified_date varchar (50)";
        $this->db->query($cmd);
        $cmd = "ALTER TABLE clients ADD COLUMN IF NOT EXISTS suntag_shortcode varchar (32)";
        $this->db->query($cmd);
        $cmd = "ALTER TABLE clients ADD COLUMN IF NOT EXISTS merchant_type_id int";
        $this->db->query($cmd);

        $cmd = "CREATE TABLE IF NOT EXISTS `client_billpay_application` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `client_id` int(11) DEFAULT NULL,
            `sole_proprietorship` varchar(32) DEFAULT NULL,
            `name_of_parent_company` varchar(50) DEFAULT NULL,
            `business_license_no` varchar(50) DEFAULT NULL,
            `business_shortcode` varchar(32) DEFAULT NULL,
            `company_address` varchar(100) DEFAULT NULL,
            `island` varchar(32) DEFAULT NULL,
            `country` varchar(32) DEFAULT NULL,
            `head_office_telephone_no1` varchar(32) DEFAULT NULL,
            `head_office_telephone_no2` varchar(32) DEFAULT NULL,
            `business_email_address` varchar(32) DEFAULT NULL,
            `business_website` varchar(32) DEFAULT NULL,
            `primary_contact` varchar(32) DEFAULT NULL,
            `p_telephone_no` varchar(32) DEFAULT NULL,
            `p_email_address` varchar(32) DEFAULT NULL,
            `secondary_contact` varchar(32) DEFAULT NULL,
            `s_telephone_no` varchar(32) DEFAULT NULL,
            `s_email_address` varchar(32) DEFAULT NULL,
            `name_of_primary_guarantor` varchar(50) DEFAULT NULL,
            `name_of_secondary_guarantor` varchar(50) DEFAULT NULL,
            `service_categories` varchar(50) DEFAULT NULL,
            PRIMARY KEY (`id`)
            ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1";
        $this->db->query($cmd);


        $cmd = "CREATE TABLE IF NOT EXISTS `client_business_categories` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `name` varchar(32) DEFAULT NULL,
            `status` varchar(5) DEFAULT 'A',
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1";
        $this->db->query($cmd);

        $cmd = "CREATE TABLE IF NOT EXISTS `merchant_type` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `name` varchar(32) DEFAULT NULL,
            `status` varchar(5) DEFAULT 'A',
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;";
        $this->db->query($cmd);
        
        $cmd = "CREATE TABLE IF NOT EXISTS `merchant_transaction_history` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `client_record_id` int(11) DEFAULT NULL,
            `system_services_code` varchar(20) DEFAULT NULL,
            `type` varchar(15) DEFAULT NULL COMMENT 'DEBIT or CREDIT',
            `source` varchar(15) DEFAULT NULL,
            `destination` varchar(15) DEFAULT NULL,
            `description` varchar(50) DEFAULT NULL,
            `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
         $this->db->query($cmd); 

         $cmd = "ALTER TABLE merchant_transaction_history ADD COLUMN IF NOT EXISTS amount decimal (18,2)";
        $this->db->query($cmd);
        $cmd = "ALTER TABLE merchant_transaction_history ADD COLUMN IF NOT EXISTS reference varchar (100)";
        $this->db->query($cmd);
  
            

    }
    //======================usermanagement==========================
    //generic functions of user-management
    //todo: all functions creating record, return an auto-id in response>>ok
    //todo: update/modification of records, need to update the modified_timestamp and modified by fields>>ok
    //todo: later: update reset_counter, retry_counter, require_change_password; include require_change_password in login calls>>ok
    //todo: blocked account (bec of failed login attempts) can be reset;>>ok
    //check logon attempts before allowing to create session;>>ok
    //todo: send email>>ok, send sms
    //todo: create test script and run>>ok, but direct was preferred
    //todo: create documentation>>
    //todo: invalidate all session id upon login of a new one>>ok
    //todo: check if i need to create a special permission that when set/enabled, will allow a merchant user to modify other merchant user accounts;>>later
    //todo: implement change_password_required field by not allowing function calls if password is not changed first>>later
    //todo: lock account after 5 unsuccessful password change
    //TODO: ACCEPT EITHER ALIAS OR ID IN ALL API (GROUP, RESOURCE, ETC)


    protected function generate_um_sessionid() {
        do {
            $session_id = md5(uniqid(time(), true));
            $query = $this->db->where('session_id', $session_id)->get('um_sessions');
        } while ($query->num_rows() > 0);
        return $session_id;
    }

    protected function create_um_session($user_id, $ip_address) {
        $login_date = date('Y-m-d H:i:s');
        $session_id = $this->generate_um_sessionid();
        $insert_data = array(
            'session_id'        => $session_id,
            'login_timestamp'   => $login_date,
            'last_seen'         => $login_date,
            'user_id'           => $user_id,
            'ip_address'        => $ip_address,
        );
        $this->db->set($insert_data);
        $this->db->insert('um_sessions');
        return $session_id;
    }

    protected function send_email($email_address, $body, $subject) {
        if (IS_PRODUCTION) {
            $config = array('mailtype' => 'html');
            $this->load->library('email');
            $this->email->initialize($config);
            $this->email->from(settings::$email_recipients['from'], PROGRAM . ' Auto Mailer');
            $this->email->to($email_address);
            $this->email->subject($subject);
            $this->email->message("<html><body><div>{$body}</div></body></html>");
            $this->email->send();
        }
    }

    //params:username,password
    protected function create_user_session($params, $user_type) {
        $where_cond = array(
            'username' => $params['P01'],
            'user_type' => $user_type,
        );
        $ip_address = isset($params['_ip_'])? $params['_ip_']: null;
        $query = $this->db->where($where_cond)->get('um_users');
        if (!($query->num_rows() > 0)) {
            $message = 'Invalid username/password';
        } else {
            $result = $query->result_array();
            $user_info = $result[0];
            if (!($user_info['password'] == md5($params['P02']))) {
                $message = 'Invalid username/password';
                $new_retry_counter = $user_info['retry_counter'] + 1;
                $update_data = array('retry_counter' => $new_retry_counter);
                if ($new_retry_counter > 3)  {
                    $update_data['status'] = 2;//locked
                    $message .= '; Account is locked';
                }
                $this->db->where('id', $user_info['id'])->set($update_data)->update('um_users');
                //todo: error handling if failed?
            } elseif ($user_info['status'] == 0) {
                $message = 'Account is currently disabled';
            } elseif ($user_info['status'] == 2) {
                $message = 'The limit for wrong password attempts was exceeded';
            } else {
                $session_id = $this->create_um_session($user_info['id'], $ip_address);
                if (!$session_id) {
                    $message = 'Unable to create session';
                } else {
                    $update_data = array(
                        'reset_counter' => 0,
                        'retry_counter' => 0,
                    );
                    $this->db->where('id', $user_info['id'])->set($update_data)->update('um_users');
                    $this->db->where('user_id', $user_info['id'])->where('session_id !=', $session_id)->set(array('logout_timestamp' => date('Y-m-d H:i:s')))->update('um_sessions');
                    $response = array(
                        'session_id' => $session_id,
                        'user_id' => $user_info['id'],
                        'name' => $user_info['firstname'] . ' ' . $user_info['lastname'],
                        'email_address' => $user_info['email_address'],
                        'mobile_number' => $user_info['mobile_number'],
                        'user_type' => $this->user_types[$user_type],
                        'change_password_required' => $user_info['require_change_password'],
                        'merchant_reference' => $user_type == 1? $user_info['reference']: '',
                    );
                    return $this->success($response);
                }
            }
        }
        return $this->failed($message);
    }

    protected function get_user_details($user_id, $set_member = true) {
        if (!(is_numeric($user_id) && $user_id > 0)) {
            return $this->failed('Invalid user ID');
        } else {
            $query = $this->db->where('id', $user_id)->get('um_users');
            if (!($query->num_rows() > 0)) {
                return $this->failed('Invalid user ID');
            } else {
                $result = $query->result_array();
                if ($set_member) { $this->user_info = $result[0];}
                return $result[0];
            }
        }
    }

    protected function get_group_details($group_id) {
        if (!(is_numeric($group_id) && $group_id > 0)) {
            return $this->failed('Invalid group ID');
        } else {
            $query = $this->db->where('id', $group_id)->get('um_groups');
            if (!($query->num_rows() > 0)) {
                return $this->failed('Invalid group ID');
            } else {
                $result = $query->result_array();
                return $result[0];
            }
        }
    }

    protected function check_um_session($session_id, $user_type, $check_user_status = true, $extend_session=true) {//user_type is ignored since it won't be the basis of validating what resource the user is accessing should be limited by his/her user type in db (against the function's type)
        if (!(strlen($session_id) == 32)) {
            return $this->failed('Invalid session ID');
        } else {
            $query = $this->db->where('session_id', $session_id)->get('um_sessions');
            if (!($query->num_rows() > 0)) {
                return $this->failed('Invalid session ID');
            } else {
                $row = $query->row();
                $user_info = $this->get_user_details($row->user_id);
                if ($row->logout_timestamp != NULL) {
                    return $this->failed('Session expired');
                } elseif ($check_user_status && !(isset($user_info['status']) && $user_info['status'] == 1)) {
                    return $this->failed('Invalid user status');
//                } elseif (!(isset($user_info['user_type']) && $user_info['user_type'] == $user_type)) {
//                    return $this->failed('Invalid user access detected');
                } elseif (strtotime($row->last_seen) + ($this->session_timeout * 3600) < time()) {
                    $this->logout_um_session($session_id);
                    return $this->failed('Session expired');
                } else {
                    if ($extend_session) $this->db->where('id', $row->id)->set('last_seen', date('Y-m-d H:i:s'))->update('um_sessions');
                    return $this->success('Session is valid');
                }
            }
        }
    }

    protected function get_user_groups($user_id) {
        $query = $this->db->where('user_id', $user_id)->get('um_user_groups');
        if (!($query->num_rows() > 0)) {
            return array();
        } else {
            $result = $query->result_array();
            foreach($result as $row) {
                $groups[] = $row['group_id'];
            }
            return $groups;
        }
    }

    protected function check_permissions($user_id, $function) {
        $permissions = $this->_get_user_permissions($user_id);
        if (!(in_array($function, $permissions))) {
            return $this->failed('Function not allowed');
        } else {
            return $this->success('Allowed');
        }
    }

    protected function _get_all_groups($group_type, $include_desc = false, $status = null) {
        if ($status != null) $this->db->where('status',$status);
        $query = $this->db->where('group_type', $group_type)->get('um_groups');
        $groups = array();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $this->last_result = array();
            foreach ($result as $row) {
                $description = $include_desc? '|'.$row['description']: '';
                $groups[$row['id']] = $row['name'].$description;
                $this->last_result[$row['id']] = $row;
            }
        }
        return $groups;
    }

    protected function _get_all_resources($resource_type, $status = null) {
        if ($status != null) $this->db->where('status',$status);
        if ($resource_type == 1) $this->db->where('resource_type', $resource_type); //if user is merchant, only select merchant resources, else if user is admin, select all (merchant+admin) resources
        $query = $this->db->get('um_resources');
        $resources = array();
        $this->last_result = array();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            foreach ($result as $row) {
                $resources[$row['id']] = $row['name'];
                $this->last_result[$row['id']] = $row;
            }
        }
        return $resources;
    }

    protected function _get_group_permissions($group_id, $include_details = false) {
        $query = $this->db->query('select distinct res.id, res.name,res.description,res.status from um_permissions acl inner join um_resources res on acl.resource_id=res.id
                where (user_type=2 and user_id=' . $group_id . ' and res.status=1 and acl.status=1)');
        if (!($query->num_rows() > 0)) {
            return array();
        } else {
            $result = $query->result_array();
            $permission = array();
            $this->last_result = array();
            foreach ($result as $row) {
                $description = $include_details? '|'.$row['description']: '';
                $permission[$row['id']] = $row['name'] . $description;
                $this->last_result[$row['id']] = $row;
            }
            return $permission;
        }
    }

    protected function _get_user_permissions($user_id, $include_group_permissions = true, $include_details = false) {
        //return array: resource_id=>name
        $user_groups = $this->get_user_groups($user_id);
        $group_condition = '';
        if ($include_group_permissions && is_array($user_groups) && count($user_groups) > 0) {
            $group_condition = ' (user_type=2 and user_id in (' . implode($user_groups) . ') and res.status=1 and acl.status=1) OR ';
        }
        $query_string = 'select distinct res.id, res.name, res.description from um_permissions acl inner join um_resources res on acl.resource_id=res.id
                where '.$group_condition.'(user_type=1 and user_id=' . $user_id . ' and res.status=1 and acl.status=1)';
        $query = $this->db->query($query_string);
        if (!($query->num_rows() > 0)) {
            return array();
        } else {
            $result = $query->result_array();
            $permission = array();
            $this->last_result = array();
            foreach ($result as $row) {
                $description = $include_details? '|'.$row['description']: '';
                $permission[$row['id']] = $row['name'] . $description;
                $this->last_result[$row['id']] = $row;
            }
            $this->user_permissions = $permission;
            return $permission;
        }
    }

    protected function logout_um_session($session_id) {
        $values = array("logout_timestamp" => date('Y-m-d H:i:s a'));
        $this->db->where('session_id', $session_id);
        return (bool) ($this->db->update('um_sessions', $values));
    }
    //end==generic functions of user-management

    //params: session_id, alias, description
    public function add_resource($params, $user_type, $function) {
        $resources = $this->_get_all_resources($user_type);
        if (!$this->check_um_session($params['P01'], $user_type)) {
            return false;
        } elseif (!($this->check_permissions($this->user_info['id'], $function))) {
            return false;
        } elseif (!($params['P02'] != '' && strlen($params['P02']) > 0)) {
            return $this->failed('Resource name could not be empty');
        } elseif (in_array(strtolower($params['P02']), $resources)) {
            return $this->failed('Resource name already exist');
        } else {
            $insert_data = array(
                'name'          => strtolower($params['P02']),
                'description'   => $params['P03'],
                'status'        => 1,
                'resource_type' => $user_type,
                'created_by'    => $this->user_info['id'],
            );
            $this->db->set($insert_data)->insert('um_resources');
            if (!($this->db->affected_rows() > 0)) {
                return $this->failed('System error: Unable to add new resource');
            } else {
                $res_id = $this->db->insert_id();
                $response = array(
                    'resource_id'   => $res_id,
                    'message'       => 'Successfully added new resource',
                );
                return $this->success($response);
            }
        }
    }

    //params: session_id, name, description
    public function add_group($params, $user_type, $function) {
        $groups = $this->_get_all_groups($user_type);
        if (!$this->check_um_session($params['P01'], $user_type)) {
            return false;
        } elseif (!($this->check_permissions($this->user_info['id'], $function))) {
            return false;
        } elseif (!($params['P02'] != '' && strlen($params['P02']) > 0)) {
            return $this->failed('Group name could not be empty');
        } elseif (in_array(strtolower($params['P02']), $groups)) {
            return $this->failed('Group name already exist');
        } else {
            $insert_data = array(
                'name'          => strtolower($params['P02']),
                'description'   => $params['P03'],
                'status'        => 1,
                'group_type' => $user_type,
                'created_by'    => $this->user_info['id'],
            );
            $this->db->set($insert_data)->insert('um_groups');
            if (!($this->db->affected_rows() > 0)) {
                return $this->failed('System error: Unable to add new group');
            } else {
                $grp_id = $this->db->insert_id();
                $response = array(
                    'group_id'  => $grp_id,
                    'message'   => 'Successfully added new group',
                );
                return $this->success($response);
            }
        }
    }

    //params: session_id, user_id/group_id, resource_id[comma-separated if multiple values]
    //purpose: add permission/s to user or group
    public function add_permissions($params, $user_type, $function) {
        $cleaned_res = str_replace(',', '', $params['P03']);
        if (!(is_numeric($params['P02']) && $params['P02'] > 0)) {
            return $this->failed('Invalid user/group ID');
        } elseif (!((is_numeric($params['P03']) && strlen($params['P03']) > 0) || (strpos($params['P03'], ',') !== false && is_numeric($cleaned_res) && strlen($cleaned_res) > 0))) {
            return $this->failed('Invalid resource ID/s');
        } else {
            if ($user_type == 2) {//group
                $group_info = $this->get_group_details($params['P02']);
                $all_resources = $this->_get_all_resources($group_info['group_type']); //permission to add will be validated against this result
                $permissions = $this->_get_group_permissions($params['P02']);
            } else {//user
                $user_info = $this->get_user_details($params['P02'], false);
                $all_resources = $this->_get_all_resources($user_info['user_type']);//permission to add will be validated against this result
                $permissions = $this->_get_user_permissions($params['P02'], false); //false = don't include group-derived permissions
            }
            if (!$this->check_um_session($params['P01'], $user_type)) {
                return false;
            } elseif (!($this->check_permissions($this->user_info['id'], $function))) {
                return false;
            } else {
                $resources = explode(',', $params['P03']);
                $ctr_exist = 0;
                $ctr_invalid = 0;
                $ctr_failed = 0;
                $ctr_success = 0;
                foreach ($resources as $res_id) {
                    if (isset($permissions[$res_id])) {
                        $ctr_exist++;
                    } elseif (!isset($all_resources[$res_id])) {
                        $ctr_invalid++;
                    } else {
                        $insert_data = array(
                            'user_id'       => $params['P02'],
                            'user_type'     => $user_type,
                            'resource_id'   => $res_id,
                            'created_by'    => $this->user_info['id'],
                        );
                        $this->db->set($insert_data)->insert('um_permissions');
                        if (!($this->db->affected_rows() > 0)) {
                            $ctr_failed++;
                        } else {
                            $ctr_success++;
                        }
                    }
                }
                $response = array(
                    'added'         => $ctr_success,
                    'failed'        => $ctr_failed,
                    'already_exist' => $ctr_exist,
                    'invalid_resource' => $ctr_invalid,
                );
                return $this->success($response);
            }
        }
    }

    //params: session_id, resource_id
    public function remove_resource($params, $user_type, $function) {
        $resources = $this->_get_all_resources($user_type);
        if (!$this->check_um_session($params['P01'], $user_type)) {
            return false;
        } elseif (!($this->check_permissions($this->user_info['id'], $function))) {
            return false;
        } elseif (!(is_numeric($params['P02']) && $params['P02'] > 0)) {
            return $this->failed('Invalid resource ID');
        } elseif (!isset($resources[$params['P02']])) {
            return $this->failed('Invalid resource ID');
        } else {
            $this->db->where('id', $params['P02'])->delete('um_resources');
            if (!($this->db->affected_rows() > 0)) {
                return $this->failed('System error: Unable to delete resource');
            } else {
                return $this->success('Successfully deleted resource');
            }
        }
    }

    //params: session_id, group_id
    public function remove_group($params, $user_type, $function) {
        $groups = $this->_get_all_groups($user_type);
        if (!$this->check_um_session($params['P01'], $user_type)) {
            return false;
        } elseif (!($this->check_permissions($this->user_info['id'], $function))) {
            return false;
        } elseif (!(is_numeric($params['P02']) && $params['P02'] > 0)) {
            return $this->failed('Invalid group ID');
        } elseif (!isset($groups[$params['P02']])) {
            return $this->failed('Invalid group ID');
        } else {
            $this->db->where('id', $params['P02'])->delete('um_groups');
            if (!($this->db->affected_rows() > 0)) {
                return $this->failed('System error: Unable to delete group');
            } else {
                return $this->success('Successfully deleted group');
            }
        }
    }

    //params: session_id,user_id/group_id, resource_id[comma-separated if multiple values]
    public function remove_permission($params, $user_type, $function) {
        $cleaned_res = str_replace(',', '', $params['P03']);
        if (!(is_numeric($params['P02']) && $params['P02'] > 0)) {
            return $this->failed('Invalid user/group ID');
        } elseif (!((is_numeric($params['P03']) && strlen($params['P03']) > 0) || (strpos($params['P03'], ',') !== false && is_numeric($cleaned_res) && strlen($cleaned_res) > 0))) {
            return $this->failed('Invalid resource ID/s');
        } else {
            if ($user_type == 2) {//group
                $permissions = $this->_get_group_permissions($params['P02']);
            } else {//user
                $permissions = $this->_get_user_permissions($params['P02'], false); //false = don't include group-derived permissions
            }
            if (!$this->check_um_session($params['P01'], $user_type)) {
                return false;
            } elseif (!($this->check_permissions($this->user_info['id'], $function))) {
                return false;
            } else {
                $resources = explode(',', $params['P03']);
                $ctr_not_exist = 0;
                $ctr_failed = 0;
                $ctr_success = 0;
                foreach ($resources as $res_id) {
                    if (!(isset($permissions[$res_id]))) {
                        $ctr_not_exist++;
                    } else {
                        $where_cond = array(
                            'user_id' => $params['P02'],
                            'user_type' => $user_type,
                            'resource_id' => $res_id,
                        );
                        $this->db->where($where_cond)->delete('um_permissions');
                        if (!($this->db->affected_rows() > 0)) {
                            $ctr_failed++;
                        } else {
                            $ctr_success++;
                        }
                    }
                }
                $response = array(
                    'deleted'   => $ctr_success,
                    'failed'    => $ctr_failed,
                    'invalid'   => $ctr_not_exist,
                );
                return $this->success($response);
            }
        }
    }

    //params: session_id,user_id(merchant/admin), group_id[comma-separated if multiple values]
    public function assign_groups_to_user($params, $user_type, $function) {
        $cleaned_group = str_replace(',', '', $params['P03']);
        if (!(is_numeric($params['P02']) && $params['P02'] > 0)) {
            return $this->failed('Invalid user ID');
        } elseif (!((is_numeric($params['P03']) && strlen($params['P03']) > 0) || (strpos($params['P03'], ',') !== false && is_numeric($cleaned_group) && strlen($cleaned_group) > 0))) {
            return $this->failed('Invalid group ID/s');
        } else {
            $all_groups = $this->_get_all_groups($user_type);
            $user_groups = $this->get_user_groups($params['P02']);
            if (!$this->check_um_session($params['P01'], $user_type)) {
                return false;
            } elseif (!($this->check_permissions($this->user_info['id'], $function))) {
                return false;
            } else {
                $groups_to_add = explode(',', $params['P03']);
                $ctr_exist = 0;
                $ctr_invalid = 0;
                $ctr_failed = 0;
                $ctr_success = 0;
                foreach ($groups_to_add as $group_id) {
                    if (in_array($group_id, $user_groups)) {
                        $ctr_exist++;
                    } elseif (!isset($all_groups[$group_id])) {
                        $ctr_invalid++;
                    } else {
                        $insert_data = array(
                            'user_id'       => $params['P02'],
                            'group_id'      => $group_id,
                            'created_by'    => $this->user_info['id'],
                        );
                        $this->db->set($insert_data)->insert('um_user_groups');
                        if (!($this->db->affected_rows() > 0)) {
                            $ctr_failed++;
                        } else {
                            $ctr_success++;
                        }
                    }
                }
                $response = array(
                    'assigned_groups'   => $ctr_success,
                    'failed'            => $ctr_failed,
                    'already_exist'     => $ctr_exist,
                    'invalid_group'     => $ctr_invalid,
                );
                return $this->success($response);
            }
        }
    }

    //params: session_id,user_id, group_id
    public function remove_groups_from_user($params, $user_type, $function) {
        $cleaned_group = str_replace(',', '', $params['P03']);
        if (!(is_numeric($params['P02']) && $params['P02'] > 0)) {
            return $this->failed('Invalid user ID');
        } elseif (!((is_numeric($params['P03']) && strlen($params['P03']) > 0) || (strpos($params['P03'], ',') !== false && is_numeric($cleaned_group) && strlen($cleaned_group) > 0))) {
            return $this->failed('Invalid group ID/s');
        } else {
            $user_groups = $this->get_user_groups($params['P02']);
            if (!$this->check_um_session($params['P01'], $user_type)) {
                return false;
            } elseif (!($this->check_permissions($this->user_info['id'], $function))) {
                return false;
            } else {
                $groups_to_delete = explode(',', $params['P03']);
                $ctr_not_exist = 0;
                $ctr_failed = 0;
                $ctr_success = 0;
                foreach ($groups_to_delete as $group_id) {
                    if (!in_array($group_id, $user_groups)) {
                        $ctr_not_exist++;
                    } else {
                        $where_cond = array(
                            'user_id' => $params['P02'],
                            'group_id' => $group_id,
                        );
                        $this->db->where($where_cond)->delete('um_user_groups');
                        if (!($this->db->affected_rows() > 0)) {
                            $ctr_failed++;
                        } else {
                            $ctr_success++;
                        }
                    }
                }
                $response = array(
                    'removed_groups'    => $ctr_success,
                    'failed'            => $ctr_failed,
                    'not_exist'         => $ctr_not_exist,
                );
                return $this->success($response);
            }
        }
    }

    //params: session_id,user_id/group_id; user_type(1=merchant,2=admin); category(1=user,2=group); function: calling function name
    //returns: array(array('id'=> 1, 'name'=>'','description'=>''),);
    public function get_permissions($params, $user_type, $category, $function) {
        if (!$this->check_um_session($params['P01'], $user_type)) {
            return false;
        } elseif (!($this->check_permissions($this->user_info['id'], $function))) {
            return false;
        } elseif (!(is_numeric($params['P02']) && $params['P02'] > 0)) {
            return $this->failed('Invalid user/group ID');
        } else {
            $out = array();
            if ($category == 1) {//user
                $user_info = $this->get_user_details($params['P02'], false);
                if (!($user_info && $user_info['user_type'] == $user_type)) {return $this->failed('Invalid user ID');}
                $permissions = $this->_get_user_permissions($params['P02'], true, true);
            } else {//group
                $group_info = $this->get_group_details($params['P02']);
                if (!($group_info && $group_info['group_type'] == $user_type))  return $this->failed('Invalid group ID');
                $permissions = $this->_get_group_permissions($params['P02'], true);
            }
            foreach ($permissions as $res_id => $res) {
                $res = explode('|', $res . '|');
                $out[] = array(
                    'id' => $res_id,
                    'name' => $res[0],
                    'description' => $res[1],
                );
            }
            return $this->success($out);
        }
    }

    //params: session_id
    //returns: array(array('id'=> 1, 'name'=>'','description'),);
    public function get_all_groups($params, $user_type, $function) {
        if (!$this->check_um_session($params['P01'], $user_type)) {
            return false;
        } elseif (!($this->check_permissions($this->user_info['id'], $function))) {
            return false;
        } else {
            $out = array();
            $groups = $this->_get_all_groups($user_type, true);
            foreach ($groups as $grp_id => $grp) {
                $grp_details = explode('|', $grp . '|');
                $out[] = array(
                    'id'    => $grp_id,
                    'name'  => $grp_details[0],
                    'description' => $grp_details[1],
                );
            }
            return $this->success($out);
        }
    }

    //params: session_id,user_id
    public function enable_user($params, $user_type, $function) {
        if (!$this->check_um_session($params['P01'], $user_type)) {
            return false;
        } elseif (!($this->check_permissions($this->user_info['id'], $function))) {
            return false;
        } elseif (!(is_numeric($params['P02']) && $params['P02'] > 0)) {
            return $this->failed('Invalid user ID');
        } else {
            $user_info = $this->get_user_details($params['P02'], false);
            if (!(isset($user_info['user_type']) && $user_info['user_type'] == $user_type)) {
                return $this->failed('Invalid user ID');
            } else {
                if (!($user_info['user_type'] == $user_type)) { //the API being called is not the same as the user's type (merchant != admin)
                    return $this->failed('Invalid user/group ID');
                } elseif (!($user_info['status'] != 1)) {
                    return $this->failed('User was already enabled');
                } else {
                    $update_data = array(
                        'status' => 1,
                        'modified_by' => $this->user_info['id'],
                        'modified_timestamp' => date('Y-m-d H:i:s'),
                    );
                    $this->db->where('id', $params['P02'])->set($update_data)->update('um_users');
                    if (!($this->db->affected_rows() > 0)) {
                        return $this->failed('Unable to enable user');
                    } else {
                        return $this->Success('Successfully enabled user');
                    }
                }
            }
        }
    }

    //params: session_id,user_id
    public function disable_user($params, $user_type, $function) {
        if (!$this->check_um_session($params['P01'], $user_type)) {
            return false;
        } elseif (!($this->check_permissions($this->user_info['id'], $function))) {
            return false;
        } elseif (!(is_numeric($params['P02']) && $params['P02'] > 0)) {
            return $this->failed('Invalid user ID');
        } else {
            $user_info = $this->get_user_details($params['P02'], false);
            if (!(isset($user_info['user_type']) && $user_info['user_type'] == $user_type)) {
                return $this->failed('Invalid user ID');
            } else {
                if (!($user_info['user_type'] == $user_type)) { //the API being called is not the same as the user's type (merchant != admin)
                    return $this->failed('Invalid user/group ID');
                } elseif (!($user_info['status'] != 0)) {
                    return $this->failed('User was already disabled');
                } else {
                   $update_data = array(
                        'status' => 0,
                        'modified_by' => $this->user_info['id'],
                        'modified_timestamp' => date('Y-m-d H:i:s'),
                    );
                    $this->db->where('id', $params['P02'])->set($update_data)->update('um_users');
                    if (!($this->db->affected_rows() > 0)) {
                        return $this->failed('Unable to disable user');
                    } else {
                        return $this->Success('Successfully disabled user');
                    }
                }
            }
        }
    }

    //params: session_id,resource_id
    public function disable_resource($params, $user_type, $function) {
        if (!$this->check_um_session($params['P01'], $user_type)) {
            return false;
        } elseif (!($this->check_permissions($this->user_info['id'], $function))) {
            return false;
        } elseif (!(is_numeric($params['P02']) && $params['P02'] > 0)) {
            return $this->failed('Invalid resource ID');
        } elseif (!($resources = $this->_get_all_resources($user_type))) {
            return $this->failed('Unable to check resources');
        } elseif (!(isset($resources[$params['P02']]))) {
            return $this->failed('Invalid resource ID');
        } elseif (!($this->last_result[$params['P02']]['status'] == 1)) {
            return $this->failed('Resource status was already disabled');
        } else {
            $update_data = array(
                'status' => 0,
                'modified_by' => $this->user_info['id'],
                'modified_timestamp' => date('Y-m-d H:i:s'),
            );
            $this->db->where('id', $params['P02'])->set($update_data)->update('um_resources');
            if (!($this->db->affected_rows() > 0)) {
                return $this->failed('Unable to disable resource');
            } else {
                return $this->Success('Successfully disabled resource');
            }
        }
    }

    //params: session_id,group_id
    public function disable_group($params, $user_type, $function) {
        if (!$this->check_um_session($params['P01'], $user_type)) {
            return false;
        } elseif (!($this->check_permissions($this->user_info['id'], $function))) {
            return false;
        } elseif (!(is_numeric($params['P02']) && $params['P02'] > 0)) {
            return $this->failed('Invalid group ID');
        } elseif (!($groups = $this->_get_all_groups($user_type))) {
            return $this->failed('Invalid group ID');
        } elseif (!(isset($groups[$params['P02']]))) {
            return $this->failed('Invalid group ID');
        } elseif (!($this->last_result[$params['P02']]['status'] == 1)) {
            return $this->failed('Group status was already disabled');
        } else {
            $update_data = array(
                'status' => 0,
                'modified_by' => $this->user_info['id'],
                'modified_timestamp' => date('Y-m-d H:i:s'),
            );
            $this->db->where('id', $params['P02'])->set($update_data)->update('um_groups');
            if (!($this->db->affected_rows() > 0)) {
                return $this->failed('Unable to disable group');
            } else {
                return $this->Success('Successfully disabled group');
            }
        }
    }

    //params: session_id,resource_id
    public function enable_resource($params, $user_type, $function) {
        if (!$this->check_um_session($params['P01'], $user_type)) {
            return false;
        } elseif (!($this->check_permissions($this->user_info['id'], $function))) {
            return false;
        } elseif (!(is_numeric($params['P02']) && $params['P02'] > 0)) {
            return $this->failed('Invalid resource ID');
        } elseif (!($resources = $this->_get_all_resources($user_type))) {
            return $this->failed('Unable to check resources');
        } elseif (!(isset($resources[$params['P02']]))) {
            return $this->failed('Invalid resource ID');
        } elseif (!($this->last_result[$params['P02']]['status'] == 0)) {
            return $this->failed('Resource status was already enabled');
        } else {
            $update_data = array(
                'status' => 1,
                'modified_by' => $this->user_info['id'],
                'modified_timestamp' => date('Y-m-d H:i:s'),
            );
            $this->db->where('id', $params['P02'])->set($update_data)->update('um_resources');
            if (!($this->db->affected_rows() > 0)) {
                return $this->failed('Unable to enable resource');
            } else {
                return $this->Success('Successfully enabled resource');
            }
        }
    }

    //params: session_id,group_id
    public function enable_group($params, $user_type, $function) {
        if (!$this->check_um_session($params['P01'], $user_type)) {
            return false;
        } elseif (!($this->check_permissions($this->user_info['id'], $function))) {
            return false;
        } elseif (!(is_numeric($params['P02']) && $params['P02'] > 0)) {
            return $this->failed('Invalid group ID');
        } elseif (!($groups = $this->_get_all_groups($user_type))) {
            return $this->failed('Invalid group ID');
        } elseif (!(isset($groups[$params['P02']]))) {
            return $this->failed('Invalid group ID');
        } elseif (!($this->last_result[$params['P02']]['status'] == 0)) {
            return $this->failed('Group status was already enabled');
        } else {
            $update_data = array(
                'status' => 1,
                'modified_by' => $this->user_info['id'],
                'modified_timestamp' => date('Y-m-d H:i:s'),
            );
            $this->db->where('id', $params['P02'])->set($update_data)->update('um_groups');
            if (!($this->db->affected_rows() > 0)) {
                return $this->failed('Unable to enable group');
            } else {
                return $this->Success('Successfully enabled group');
            }
        }
    }

    //params: session_id,username,firstname,lastname,email_address,mobile_number,user_reference
    public function create_user($params, $user_type, $function) {
        if (!$this->check_um_session($params['P01'], $user_type)) {
            return false;
        } elseif (!($this->check_permissions($this->user_info['id'], $function))) {
            return false;
        } elseif ($user_type == 1 && !(isset($params['P07']) && is_numeric($params['P07']) && $params['P07'] > 0)) {
            return $this->failed('Invalid user reference');
        } else {
            $username = $params['P02'];
            $firstname = $params['P03'];
            $lastname = $params['P04'];
            $email_address = $params['P05'];
            $mobile_number = $params['P06'];
            $user_reference = $user_type == 1? $params['P07']: null;
            $check_username = $this->db->where('username', $username)->limit(1)->get('um_users');
            if (!($username != '' && $email_address != '' && $mobile_number != '')) {
                return $this->failed('Username, email address, and mobile number could not be empty');
            } elseif (strpos($email_address, '@')  === false) {
                return $this->failed('Invalid email address');
            } elseif ($check_username->num_rows() > 0) {
                return $this->failed('The username already exists');
            } else {
                $password = substr(md5(microtime(true)),5,8);
                $insert_data = array(
                    'username'      => trim($username),
                    'password'      => md5($password),
                    'firstname'     => trim($firstname),
                    'lastname'      => trim($lastname),
                    'email_address' => trim($email_address),
                    'mobile_number' => trim($mobile_number),
                    'user_type'     => $user_type,
                    'reference'     => trim($user_reference),
                    'created_by'    => $this->user_info['id'],
                    'require_change_password' => 1,
                );
                $this->db->set($insert_data)->insert('um_users');
                if (!($this->db->affected_rows() > 0)) {
                    return $this->failed('Unable to create user at this time');
                } else {
                    if (isset($insert_data['email_address']) && trim($insert_data['email_address']) != '') {
                        $email_message = 'Your '. PROGRAM .' username is:'. $username .', with password:' . $password;
                        $email_subject = PROGRAM . ': New user account';
                        $this->send_email($insert_data['email_address'], $email_message, $email_subject);
                    }
                    if (isset($insert_data['mobile_number']) && trim($insert_data['mobile_number']) != '') {
                        $sms_message = 'Your '. PROGRAM .' username is:'. $username .', with password:' . $password;
                        $this->send_sms($insert_data['mobile_number'], $sms_message);
                    }
                    $user_id = $this->db->insert_id();
                    $response = array(
                        'user_id'   => $user_id,
                        'message' => 'Successfully created user.',
                    );
                    if (!IS_PRODUCTION) $response['password'] = $password;
                    return $this->success($response);
                }
            }
        }
    }

    public function reset_own_password($params) {
        if (!($params['P01'] != '' && $params['P02'] != '')) {
            return $this->failed('Username and e-mail address are both required');
        } else {
            $query = $this->db->where('username', $params['P01'])->where('email_address', $params['P02'])->get('um_users');
            if (!($query->num_rows() > 0)) {
                return $this->failed('The username and/or e-mail address were not found');
            } else {
                $result = $query->result_array();
                $user_info = $result[0];
                return $this->reset_password_generic($user_info, '');
            }
        }
    }

    //params: session_id,user_id
    public function reset_password($params, $user_type, $function) {
        if (!$this->check_um_session($params['P01'], $user_type)) {
            return false;
        } elseif (!($this->check_permissions($this->user_info['id'], $function))) {
            return false;
        } elseif (!(is_numeric($params['P02']) && $params['P02'] > 0)) {
            return $this->failed('Invalid user/group ID');
        } else {
            $user_info = $this->get_user_details($params['P02'], false);
            if (!$user_info) {
                return $this->failed('Invalid user/group ID');
            } else {
                return $this->reset_password_generic($user_info, $this->user_info['id']);
            }
        }
    }

    protected function reset_password_generic($user_info, $modified_by = null) {
        if (!($user_info['status'] == 1 || $user_info['status'] == 2)) {
            return $this->failed('Invalid user account status');
        } elseif (!( trim($user_info['email_address']) != '' || trim($user_info['mobile_number']) != '')) {
            return $this->failed('The user has no email_address or mobile_number defined where the new password will be sent');
        } elseif ($user_info['reset_counter'] > 3) {
            return $this->failed('Error: the account password was reset more than 3 consecutive times already');
        } else {
            $new_password = substr(md5(microtime(true)),5,8);
            $update_data = array(
                'status' => 1,
                'reset_counter' => $user_info['reset_counter'] + 1,
                'password'  => md5($new_password),
                'modified_by' => $modified_by,
                'modified_timestamp' => date('Y-m-d H:i:s'),
            );
            $this->db->where('id', $user_info['id'])->set($update_data)->update('um_users');
            if (!($this->db->affected_rows() > 0)) {
                return $this->failed('Unable to reset user password');
            } else {
                if (isset($user_info['email_address']) && trim($user_info['email_address']) != '') {
                    $email_message = 'Your new password is:' . $new_password;
                    $email_subject = PROGRAM . ': Password reset request';
                    $this->send_email($user_info['email_address'], $email_message, $email_subject);
                }
                if (isset($user_info['mobile_number']) && trim($user_info['mobile_number']) != '') {
                    $sms_message = 'Your new '. PROGRAM .' password is:' . $new_password;
                    $this->send_sms($user_info['mobile_number'], $sms_message);
                }
                $tmp_password = IS_PRODUCTION? '': ':' . $new_password;
                $response = 'Successfully reset user password' . $tmp_password;
                return $this->success($response);
            }
        }
    }

    //params: session_id,user_id,firstname,lastname,email_address,mobile_number
    //note: if a parameter provided is empty, it won't change that field
    public function update_user($params, $user_type, $function) {
        if (!$this->check_um_session($params['P01'], $user_type)) {
            return false;
        } elseif (!($this->check_permissions($this->user_info['id'], $function))) {
            return false;
        } else {
            $user_info = $this->get_user_details($params['P02'], false);
            if (!(isset($user_info['user_type']) && $user_info['user_type'] == $user_type)) {
                return $this->failed('Invalid user ID');
            } else {
                $update_data = array();
                $flag = false;
                if (!empty($params['P03'])) $update_data['firstname'] = $params['P03'];
                if (!empty($params['P04'])) $update_data['lastname'] = $params['P04'];
                if (!empty($params['P05']) && strpos($params['P05'], '@') === false) {
                    return $this->failed('Invalid email address');
                } elseif (!empty($params['P05'])) {
                    $update_data['email_address'] = $params['P05'];
                }
                if (!empty($params['P06'])) $update_data['mobile_number'] = $params['P06'];
                foreach ($update_data as $key => $val) {
                    if ($user_info[$key] != $val) {
                        $flag = true;
                        break;
                    }
                }
                if (!$flag) {
                    return $this->failed('Nothing to update');
                } else {
                    $update_data['modified_by'] = $this->user_info['id'];
                    $update_data['modified_timestamp'] = date('Y-m-d H:i:s');
                    $this->db->where('id', $params['P02'])->set($update_data)->update('um_users');
                    if (!($this->db->affected_rows() > 0)) {
                        return $this->failed('Unable to update user at this time');
                    } else {
                        return $this->success('Successfully updated user info');
                    }
                }
            }
        }
    }


    protected function validate_password($password) {
        if (!($password != '' && strlen($password) >=6)) {
            return $this->failed('Password must be at least 6 characters');
        } elseif (!(strlen($password) <= 32)) {
            return $this->failed('Password cannot be more than 32 characters');
        } elseif(!preg_match('/^(?=.*\d)(?=.*[A-Za-z]).{6,32}$/', $password)) {
            return $this->failed('Password must have at least one letter and one number');
        } else {
            return $this->success('Valid');
        }
    }

    //params: session_id,old_pw,new_pw
    public function change_password($params) {
        if (!$this->check_um_session($params['P01'], 0)) { //note: 0 since not really used in the function check_um_session
            return false;
        } elseif (!($this->check_permissions($this->user_info['id'], __FUNCTION__))) {
            return false;
        } elseif (!($params['P02'] !='' && md5($params['P02']) == $this->user_info['password'])) {
            $message = '';
            $new_retry_counter = $this->user_info['retry_counter'] + 1;
            $update_data = array('retry_counter' => $new_retry_counter);
            if ($new_retry_counter > 4)  {
                $update_data['status'] = 2;//locked
                $message .= '; Account is locked';
            } elseif ($new_retry_counter == 5) {
                $message .= '; Account will be locked in the next invalid attempt';
            }
            $this->db->where('id', $this->user_info['id'])->set($update_data)->update('um_users');
            return $this->failed('Invalid current password'. $message);
        } elseif (!($params['P03'] !='' && $params['P02'] != $params['P03'])) {
            return $this->failed('New password must not be empty and must be different from current password');
        } elseif (!($this->validate_password($params['P03']))) {
            return false;
        } else {
            $update_data = array(
                'reset_counter' => 0,
                'retry_counter' => 0,
                'require_change_password' => 0,
                'password' => md5($params['P03']),
                'modified_by' => $this->user_info['id'],
                'modified_timestamp' => date('Y-m-d H:i:s'),
            );
            $this->db->where('id', $this->user_info['id'])->set($update_data)->update('um_users');
            if (!($this->db->affected_rows() > 0)) {
                return $this->failed('Unable to change user password');
            } else {
                $response = 'Successfully changed user password';
                return $this->success($response);
            }
        }
    }

    //legend: 1=merchant, 2=admin
    //legend: 1=user, 2=group
    public function add_merchant_resource($params) {return $this->add_resource($params,1,__FUNCTION__);}
    public function add_admin_resource($params) {return $this->add_resource($params,2,__FUNCTION__);}
    public function add_merchant_group($params) {return $this->add_group($params,1,__FUNCTION__);}
    public function add_admin_group($params) {return $this->add_group($params,2,__FUNCTION__);}
    public function add_user_permissions($params) {return $this->add_permissions($params,1,__FUNCTION__);}
    public function add_group_permssions($params) {return $this->add_permissions($params,2,__FUNCTION__);}
    public function assign_groups_to_merchant($params) { return $this->assign_groups_to_user($params,1,__FUNCTION__); }
    public function assign_groups_to_admin($params) { return $this->assign_groups_to_user($params,2,__FUNCTION__); }
    public function remove_merchant_resource($params) {return $this->remove_resource($params,1,__FUNCTION__);}
    public function remove_admin_resource($params) {return $this->remove_resource($params,2,__FUNCTION__);}
    public function remove_merchant_group($params) {return $this->remove_group($params,1,__FUNCTION__);}
    public function remove_admin_group($params) {return $this->remove_group($params,2,__FUNCTION__);}
    public function remove_user_permissions($params) {return $this->remove_permission($params,1,__FUNCTION__);}
    public function remove_group_permissions($params) {return $this->remove_permission($params,2,__FUNCTION__);}
    public function remove_groups_from_merchant($params) { return $this->remove_groups_from_user($params,1,__FUNCTION__); }
    public function remove_groups_from_admin($params) { return $this->remove_groups_from_user($params,2,__FUNCTION__); }
    public function get_merchant_user_permissions($params) {return $this->get_permissions($params,1,1,__FUNCTION__);}
    public function get_admin_user_permissions($params) {return $this->get_permissions($params,2,1,__FUNCTION__);}
    public function get_merchant_group_permissions($params) {return $this->get_permissions($params,1,2,__FUNCTION__);}
    public function get_admin_group_permissions($params) {return $this->get_permissions($params,2,2,__FUNCTION__);}
    public function get_all_merchant_groups($params) { return $this->get_all_groups($params,1,__FUNCTION__); }
    public function get_all_admin_groups($params) { return $this->get_all_groups($params,2,__FUNCTION__); }
    public function enable_merchant_user($params) { return $this->enable_user($params,1,__FUNCTION__); }
    public function enable_admin_user($params) { return $this->enable_user($params,2,__FUNCTION__); }
    public function disable_merchant_user($params) { return $this->disable_user($params,1,__FUNCTION__); }
    public function disable_admin_user($params) { return $this->disable_user($params,2,__FUNCTION__); }
    public function disable_merchant_resource($params) { return $this->disable_resource($params,1,__FUNCTION__); }
    public function disable_admin_resource($params) { return $this->disable_resource($params,2,__FUNCTION__); }
    public function disable_merchant_group($params) { return $this->disable_group($params,1,__FUNCTION__); }
    public function disable_admin_group($params) { return $this->disable_group($params,2,__FUNCTION__); }
    public function enable_merchant_resource($params) { return $this->enable_resource($params,1,__FUNCTION__); }
    public function enable_admin_resource($params) { return $this->enable_resource($params,2,__FUNCTION__); }
    public function enable_merchant_group($params) { return $this->enable_group($params,1,__FUNCTION__); }
    public function enable_admin_group($params) { return $this->enable_group($params,2,__FUNCTION__); }
    public function create_merchant_user($params) { return $this->create_user($params,1,__FUNCTION__); }
    public function create_admin_user($params) { return $this->create_user($params,2,__FUNCTION__); }
    public function reset_merchant_password($params) { return $this->reset_password($params,1,__FUNCTION__); }
    public function reset_admin_password($params) {  return $this->reset_password($params,2,__FUNCTION__);}
    public function update_merchant_user($params) { return $this->update_user($params,1,__FUNCTION__); }
    public function update_admin_user($params) {  return $this->update_user($params,2,__FUNCTION__);}

    public function merchant_login($params) {  return $this->create_user_session($params,1); }
    public function admin_login($params) { return $this->create_user_session($params,2); }

    //======================end-usermanagement==========================

    //======================ECOMMERCE==========================
    protected function validate_mobile_number($mobile) {
        $query = $this->db->where('mobile_number', $mobile)->get('ezkard_accounts');
        if (!($query->num_rows() > 0)) {
            return $this->failed('Mobile number not found');
        } else {
            $result = $query->result_array();
            $account_info = $result[0];
            $this->account_info = $account_info;
            if ($account_info['card_status_id'] != 0) {
                return $this->failed('Account status is not active');
            } else {
                return true;
            }
        }
    }

    protected function validate_token($token, $function, $params = '') {
        //$query = $this->db->select(array('merchant_tokens.*','clients.merchant_name'))->from('merchant_tokens')->join('clients','clients.id=merchant_tokens.merchant_record_id')->where('merchant_token', $token)->get();
        //^modified above query by bom(12/6/17) as below since we don't have merchant_tokens table
        $query = $this->db->where('merchant_key', $token)->get('clients');
        if (!($query->num_rows() > 0)) {
            return $this->failed('Invalid merchant token');
        } else {
            $result = $query->result_array();
            $token_info = $result[0];
            $token_info['merchant_record_id'] = $token_info['id']; //inserted by bom 12/6/17
            if (!$this->cardholders_model->get_currency_by_client_id($token_info['merchant_record_id'])) {
                return $this->failed('Unable to get merchant currency'); //this should not really happen, just in case;
            } else {
                $response = $this->cardholders_model->get_response();
                $currency = $response['ResponseMsg'];
                $token_info['currency'] = strtoupper($currency);
                $this->token_info = $token_info;
                $activity_info = array(
                    'token_used' => $token,
                    'merchant_record_id' => $token_info['merchant_record_id'],
                    'merchant_token_id' => $token_info['id'],
                    'action_description' => 'session validation:' . $function,
                    'other_reference' => 'params:' . $params,
                );
                $this->db->insert('ecommerce_merchant_activity', $activity_info);
                return true;
            }
        }
    }

    protected function check_pin($account_id, $pin) {
        return ($this->db->get_where('ezkard_pins', array('ezkard_id' => $account_id, 'pin' => $pin), 1, 0)->num_rows() > 0);
    }

    //parameters: static token,mobile number, pin(optional)
    public function generate_passcode($params) {

        $token = $params['P01'];
        $mobile_number = $params['P02'];
        $pin = isset($params['P03'])? $params['P03']: null;
        if (!$this->validate_token($token, __FUNCTION__, http_build_query($params))) {
            return false;
        } elseif (!($this->validate_mobile_number($mobile_number))) { //if response value is not false, $this->token_info should contain merchant info
            return false;
        } elseif ($pin != null && !($this->check_pin($this->account_info['id'], $pin))) {//validate pin
            return $this->failed('Invalid account PIN');
        } else {
            if (!$this->cardholders_model->actual_passcode_generator($this->account_info['id'])) {
                $response = $this->cardholders_model->get_response();
                return $this->failed($response['ResponseMsg']);
            } else {
                $response = $this->cardholders_model->get_response();
                $code = $response['ResponseMsg']['passcode'];
                $message = 'Your verification code is ' . $code;
                $mobile = ($mobile_number == '1639432788941')? '1639997699467': $mobile_number;
                settings::send_sms($mobile, $message); //$this->cardholders_model->send_sms_notification_bahamasgw($mobile_number, $message);

                $activity_info = array(
                    'token_used' => $token,
                    'merchant_record_id' => $this->token_info['merchant_record_id'],
                    'merchant_token_id' => $this->token_info['id'],
                    'action_description' => 'generate passcode',
                    'account_involved' => $mobile_number,
                    'other_reference' => $this->account_info['id'],
                );
                $this->db->insert('ecommerce_merchant_activity', $activity_info);
                $suffix = ENV == 'DEV'? ': '.$code: '';
                return $this->success('Passcode was successfully generated and sent to customer\'s mobile number'.$suffix);
            }
        }
    }

    //parameters: P01 static token + P02 mobile number + P03 passcode + P04 amount + P05 currency + P06 description (say 30 characters)
    public function process_transaction($params) {
        $token = $params['P01'];
        $mobile_number = $params['P02'];
        $passcode = $params['P03'];
        $amount = $params['P04'];
        $currency = $params['P05'];
        $description = $params['P06'];
        $description_with_prefix = 'New Online Purchase: ' . $description;
        $validate_passcode_params = array(
            'P01' => $mobile_number,
            'P02' => $passcode,
        );
        if (!$this->validate_token($token, __FUNCTION__, http_build_query($params))) {
            return false;
        } elseif (!($this->validate_mobile_number($mobile_number))) { //if response value is not false, $this->token_info should contain merchant info
            return false;
        } elseif(!(strtoupper($currency) == $this->token_info['currency'])) {
            return $this->failed('Invalid currency provided. Only ' . $this->token_info['currency'] . ' is being accepted at the moment.');
        } elseif(!(is_numeric($amount) && $amount > 0)) {
            return $this->failed('Transaction amount must be numeric and greater than 0');
        } elseif(!($description != '')) {
            return $this->failed('Description must not be empty.');
        } elseif(!$this->cardholders_model->validate_passcode($validate_passcode_params)) {
            $response = $this->cardholders_model->get_response();
            return $this->failed($response['ResponseMsg']);
        } elseif (!$this->cardholders_model->process_online_purchase($this->account_info['id'], $this->token_info['merchant_record_id'], $description_with_prefix, $amount, 31, 43, 44)) {
            $response = $this->cardholders_model->get_response();
            return $this->failed($response['ResponseMsg']);
        } else {
            $response = $this->cardholders_model->get_response();
            $this->validate_mobile_number($mobile_number); //just to update card_balance
            $sms_notification = 'Trans ID: ' . $response['ResponseMsg']['TransactionId']. ' You have successfully paid '.number_format($amount,2).' to Merchant '.$this->token_info['merchant_name'].'. Your current balance is ' . number_format($this->account_info['card_balance'],2) .' ' . $currency;
            settings::send_sms($mobile_number, $sms_notification); //$this->cardholders_model->send_sms_notification_bahamasgw($mobile_number, $message);

            $activity_info = array(
                'token_used' => $token,
                'merchant_record_id' => $this->token_info['merchant_record_id'],
                'merchant_token_id' => $this->token_info['id'],
                'action_description' => 'process transaction:' . $this->account_info['id'],
                'account_involved' => $mobile_number,
                'other_reference' => base64_encode(http_build_query($params)),
            );
            $this->db->insert('ecommerce_merchant_activity', $activity_info);
            return $this->success($response['ResponseMsg']);
        }
    }
    //======================ECOMMERCE==========================

    protected function logout_session($session_id) {
        $values = array("logout_date" => date('Y-m-d H:i:s a'));
        $this->db->where('session_id', $session_id);
        return (bool) ($this->db->update('ezkard_sessions', $values));
    }

    protected function extend_session($session_id) {
        $values = array("last_seen" => date('Y-m-d H:i:s a'));
        $this->db->where('session_id', $session_id);
        return (bool) ($this->db->update('ezkard_sessions', $values));
    }

    protected function getFunctionName() {
        $backtrace = debug_backtrace();
        return $backtrace[1]['function'];
    }

    protected function success($responsemsg) {
        $this->response = array(
            'ServiceType'   => $this->getFunctionName(),
            'ResponseId' => "0000",
            'ResponseMsg' => $responsemsg,
        );
        return TRUE;
    }

    protected function failed($responsemsg, $responseid = '0001') {
        $this->response = array(
            'ServiceType'   => $this->getFunctionName(),
            'ResponseId'    => $responseid,
            'ResponseMsg'   => $responsemsg,
        );
        return FALSE;
    }

    protected function adjust_client_balance($params) {
        $result = $this->cardholders_model->_adjust_client_balance($params);
        $this->response = $this->cardholders_model->get_response();
        return $result;
    }

    protected function adjust_card_account($params) {
        $result = $this->cardholders_model->_adjust_card_account($params);
        $this->response = $this->cardholders_model->get_response();
        return $result;
    }

    protected function get_currency_by_merchant_id($merchant_id) { //client_record_id
        $query = $this->db->select('programs.currency')
            ->from("clients")
            ->join("programs", "programs.id = clients.program_id", "inner")
            ->where('clients.id', $merchant_id)
            ->get();
        if (!($query->num_rows() > 0)) {
            return $this->failed('Merchant ID not found');
        } else {
            $row = $query->row();
            return $row->currency;
        }
    }

    //session id, amount, description, reference
    public function debit_merchant_prefund($params) {
        if (!isset($params['P01'], $params['P02'], $params['P03'], $params['P04'])) {
            return $this->failed('Missing required parameters');
        } elseif (!($params['P01'] != '' && $params['P02'] != '' && $params['P03'] != '')) {
            return $this->failed('Missing required parameters');
        } elseif (!(is_numeric($params['P02']) && $params['P02'] > 0)) {
            return $this->failed('Invalid amount');
        } elseif (!(is_numeric($params['P02']) && $params['P02'] > 0)) {
            return $this->failed('Invalid amount');
        } elseif ($params['P04'] != '' && !(is_numeric($params['P04']))) {
            return $this->failed('Reference value must be numeric');
        } elseif (!$this->validate_session($params['P01'])) {
            return false;
        } else {
            $this->extend_session($params['P01']);
            $response = $this->get_response();
            $merchant_user_id = $response['ResponseMsg']['merchant_user_id'];
            $user_details = $this->get_merchant_user_details($merchant_user_id);
            if (!(isset($user_details['user_reference']) && is_numeric($user_details['user_reference']) && $user_details['user_reference'] > 0)) {
                return $this->failed('Invalid merchant.');
            } else {
                $client_record_id = $user_details['user_reference'];
                $debit_merchant_params = array(
                    'function'          => 'less',
                    'ref_trans_id'      => is_numeric($params['P04'])? $params['P04']: time(),
                    'transfer_amount'   => $params['P02'],
                    'client_id'         => $client_record_id,
                    'trans_type'        => 5, //client prefund debit
                    'client_account_type' => 'prefund',
                    'description'       => $params['P03'],
                );
                if (!$this->adjust_client_balance($debit_merchant_params)) {
                    return $this->failed('Unable to perform prefund');
                } else {
                    $debit_amount_response = $this->get_response();
                    $currency = $this->get_currency_by_merchant_id($client_record_id);
                    $success_response = array(
                        'Reference' => $debit_amount_response['ResponseMsg']['TransactionID'],
                        'DebitedAmount' => $params['P02'],
                        'Currency' => $currency != false? $currency: '',
                    );
                    return $this->success($success_response);
                }
            }
        }
    }

    public function add_prefund_balance($params) {
        if (!isset($params['P01'], $params['P02'], $params['P03'])) {
            return $this->failed('Missing parameters');
        } elseif (!($params['P01'] != '' && $params['P02'] != '' && $params['P03'] != '')) {
            return $this->failed('Missing parameters');
        } else {
            $session_id = $params['P01'];
            $token = $params['P02'];
            $amount = $params['P03'];
            if ($token != md5($session_id . $amount . 'fuze')) {
                return $this->failed('Invalid token');
            } elseif (!$this->validate_session($session_id)) {
                return false;
            } else {
                $this->extend_session($session_id);
                $response = $this->get_response();
                $merchant_user_id = $response['ResponseMsg']['merchant_user_id'];
                $user_details = $this->get_merchant_user_details($merchant_user_id);
                if (!(isset($user_details['user_reference']) && is_numeric($user_details['user_reference']) && $user_details['user_reference'] > 0)) {
                    return $this->failed('Invalid merchant.');
                } else {
                    $client_record_id = $user_details['user_reference'];
                    $credit_merchant_params = array(
                        'function'          => 'add',
                        'ref_trans_id'      => time(),
                        'transfer_amount'   => $amount,
                        'client_id'         => $client_record_id,
                        'trans_type'        => 4, //prefund credit
                        'client_account_type' => 'prefund',
                        'description'       => 'Add prefund balance',
                    );
                    if (!$this->adjust_client_balance($credit_merchant_params)) {
                        return $this->failed('Unable to perform prefund');
                    } else {
                        $credit_amount_response = $this->get_response();
                        $currency = $this->get_currency_by_merchant_id($client_record_id);
                        $success_response = array(
                            'Reference' => $credit_amount_response['ResponseMsg']['TransactionID'],
                            'CreditedAmount' => $amount,
                            'Currency' => $currency != false? $currency: '',
                        );
                        return $this->success($success_response);
                    }
                }
            }
        }
    }

    public function check_prefund_balance($session_id) {
        if (!$this->validate_session($session_id)) {
            return FALSE;
        } else {
            $this->extend_session($session_id);
            $response = $this->get_response();
            $merchant_user_id = $response['ResponseMsg']['merchant_user_id'];
            $user_details = $this->get_merchant_user_details($merchant_user_id);
            if (!(isset($user_details['user_reference']) && is_numeric($user_details['user_reference']) && $user_details['user_reference'] > 0)) {
                return $this->failed('Invalid merchant.');
            } else {
                $client_record_id = $user_details['user_reference'];
                $merchant_details = $this->get_merchant_details($client_record_id);
                if (!(isset($merchant_details['client_prefund']))) {
                    return $this->failed('Invalid merchant.');
                } else {
                    $currency = $this->get_currency_by_merchant_id($client_record_id);
                    $success_response = array(
                        'Balance' => $merchant_details['client_prefund'],
                        'Currency' => $currency != false? $currency: '',
                    );
                    return $this->success($success_response);
                }
            }
        }
    }

    private function check_prefund_balance_for_billers($merchant_user_id) {
            $this->extend_session($session_id);
            $response = $this->get_response();
            //$merchant_user_id = $response['ResponseMsg']['merchant_user_id'];
            $user_details = $this->get_merchant_user_details($merchant_user_id);
            if (!(isset($user_details['user_reference']) && is_numeric($user_details['user_reference']) && $user_details['user_reference'] > 0)) {
                return $this->failed('Invalid merchant.');
            } else {
                $client_record_id = $user_details['user_reference'];
                $merchant_details = $this->get_merchant_details($client_record_id);
                if (!(isset($merchant_details['client_prefund']))) {
                    return $this->failed('Invalid merchant.');
                } else {
                    $currency = $this->get_currency_by_merchant_id($client_record_id);
                    $success_response = array(
                        'Balance' => $merchant_details['client_prefund'],
                        'Currency' => $currency != false? $currency: '',
                    );
                    return $this->success($success_response);
                }
            }
    }

    protected function _get_card_details_by_mobile($mobile_number) {
        $query = $this->db
                ->select('ea.*, clients.client_id as merchant_id, m.first_name, m.last_name')
                ->from('ezkard_accounts ea')
                ->join('clients', 'clients.id=ea.client_id')
                ->join('merchant_members m', 'm.giftcard_id=ea.id', 'left')
                ->where('ea.mobile_number', $mobile_number)
                ->or_where('ea.mobile_number', trim($mobile_number, ' +'))
                ->or_where('ea.mobile_number', '+' . $mobile_number)
                ->get();
        if ($query->num_rows() > 0) {
            $row = $query->result_array();
            return $row[0];
        } else {
            return false;
        }
    }

    public function load_cardholder($session_id, $loyalty_user_reference, $amount) {
        if (!(is_numeric($amount) && $amount > 0)) {
            return $this->failed('Invalid load amount.');
        } elseif (!$this->validate_session($session_id)) {
            return FALSE;
        } else {
            $this->extend_session($session_id);
            $response = $this->get_response();
            $merchant_user_id = $response['ResponseMsg']['merchant_user_id'];
            $user_details = $this->get_merchant_user_details($merchant_user_id);
            if (!(isset($user_details['user_reference']) && is_numeric($user_details['user_reference']) && $user_details['user_reference'] > 0)) {
                return $this->failed('Invalid merchant.');
            } else {
                $client_record_id = $user_details['user_reference'];
                $merchant_details = $this->get_merchant_details($client_record_id);
                if (!(isset($merchant_details['client_prefund']))) {
                    return $this->failed('Invalid merchant.');
                } elseif (!(is_numeric($merchant_details['client_prefund']) && $merchant_details['client_prefund'] >= $amount)) {
                    return $this->failed('Insufficient client prefund balance.');
                } else {
                    $card_details = $this->get_loyalty_user_details($loyalty_user_reference);
                    if (!(isset($card_details['card_balance']))) {
                        return $this->failed('Invalid loyalty reference code.');
                    } elseif (!($client_record_id == $card_details['client_id'])) {
                        return $this->failed('Unable to perform cross-merchant loading.');
                    } else {
                        $credit_card_params = array(
                            'function'          => 'add',
                            'transfer_amount'   => $amount,
                            'client_id'         => $client_record_id,
                            'card_id'           => $card_details['id'],
                            'trans_type_id'     => 0, //reload credit
                            'reference_number'  => $card_details['lla_id'],
                            'description'       => 'Card Loading via Loyalty',
                            'terminal_id'       => $client_record_id,
                        );
                        if (!$this->adjust_card_account($credit_card_params)) { //todo: fail-safe
                            return false;
                        } else {
                            $credit_amount_response = $this->get_response();
                            $debit_merchant_params = array(
                                'function'          => 'less',
                                'ref_trans_id'      => $credit_amount_response['ResponseMsg']['TransactionID'],
                                'transfer_amount'   => $amount,
                                'client_id'         => $client_record_id,
                                'trans_type'        => 9, //reload debit
                                'client_account_type' => 'prefund',
                                'description'       => 'Card Loading via Loyalty',
                            );
                            if (!$this->adjust_client_balance($debit_merchant_params)) {
                                return false;
                            } else {
                                //$debit_amount_response = $this->get_response();
                                $success_response = array(
                                    'Message' => 'Card loading successful',
                                    'TransactionID'     => $credit_amount_response['ResponseMsg']['TransactionID'],
                                    'ReloadAmount' => number_format($amount, '2'),
                                );
                                return $this->success($success_response);
                            }
                        }
                    }
                }
            }
        }
    }

    public function login($merchant_id, $username, $password, $ip_address) {
        $where = array(
            'ua.user_type_id'   => 1,
            'ua.user_name'      => $username,
            'ua.password'       => md5($password),
            'ua.user_status_id' => 0,
            'c.client_id'       => $merchant_id,
        );
        $query = $this->db
            ->select('ua.id as user_id')
            ->from('user_account ua')
            ->join('clients c', 'c.id=ua.user_reference')
            ->where($where)
            ->get();
        if (!($query->num_rows() > 0)) {
            $message = 'Invalid username/password';
        } else {
            $result = $query->result_array();
            $merchant_user_id = $result[0]['user_id'];
            $session_id = $this->create_session($merchant_user_id, $ip_address);
            if (!$session_id) {
                $message = 'Unable to create session';
            } else {
                return $this->success(array('SessionID' => $session_id));
            }
        }
        return $this->failed($message);
    }

    public function logout($session_id) {
        if (!$this->validate_session($session_id)) {
            return FALSE;
        } else {
            if ($this->logout_session($session_id)) {
                return $this->success('Successfully logged out.');
            } else {
                return $this->failed('Unable to logout session.');
            }
        }
    }

    protected function generate_sessionid() {
        do {
            $session_id = md5(uniqid(time(), true));
            $query = $this->db->get_where('ezkard_sessions', array('session_id' => $session_id), 1, 0);
        } while ($query->num_rows() > 0);
        return $session_id;
    }

    protected function get_merchant_user_details($merchant_user_id) {
        $query = $this->db->where('id', $merchant_user_id)->get('user_account');
        if (!($query->num_rows() > 0)) {
            return false;
        } else {
            $result = $query->result_array();
            return $result[0];
        }
    }

    protected function get_loyalty_user_details($loyalty_user_reference) {
        $query = $this->db->select('ea.*,lla.id as lla_id')->from('linked_loyalty_accounts lla')->join('ezkard_accounts ea', 'ea.id=lla.card_id')->where('lla.loyalty_reference', $loyalty_user_reference)->where('lla.status', 1)->get();
        if (!($query->num_rows() > 0)) {
            return false;
        } else {
            $result = $query->result_array();
            return $result[0];
        }
    }

    protected function get_merchant_details($merchant_record_id) {
        $query = $this->db->where('id', $merchant_record_id)->get('clients');
        if (!($query->num_rows() > 0)) {
            return false;
        } else {
            $result = $query->result_array();
            return $result[0];
        }
    }

    protected function get_merchant_details_by_merchant_id($merchant_id) {
        $query = $this->db->where('client_id', $merchant_id)->get('clients');
        if (!($query->num_rows() > 0)) {
            return false;
        } else {
            $result = $query->result_array();
            return $result[0];
        }
    }

    protected function create_session($merchant_user_id, $ip_address) {
        $login_date = date('Y-m-d H:i:s');
        $session_id = $this->generate_sessionid();
        $session_details = array(
            'merchant_user_id'=> $merchant_user_id,
            'ip_address'    => $ip_address,
        );
        $session_details = $this->crypt->encrypt($session_id, serialize($session_details));
        $insert_data = array(
            'session_id'    => $session_id,
            'login_date'    => $login_date,
            'last_seen'     => $login_date,
            'session_details' => $session_details,
        );
        $this->db->set($insert_data);
        $this->db->insert('ezkard_sessions');
        return $session_id;
    }

    protected function send_sms($mobile, $message) {
        $sms_endpoint = URL_SMS;
        $mobile = trim($mobile, ' +');
        $sms_params = array(
            'P00'   => 'opens1same',
            'P01'   => $mobile,
            'P02'   => $message,
        );
        $response = file_get_contents($sms_endpoint . 'method=send_sms&' . http_build_query($sms_params));
        return $response;
    }


    public function validate_session($session_id, $return_info = TRUE) {
        $query = $this->db->get_where('ezkard_sessions', array('session_id' => $session_id), 1, 0);
        if (!$query->num_rows() > 0) {
             if (!$this->validate_session_pos($session_id, '')) {
                return $this->failed('Invalid session ID');
             } else{
                return $this->success('Session is valid');
             }
            return $this->failed('Invalid session ID');
        } else {
            $row = $query->row();
            if ($row->logout_date != NULL) {
                return $this->failed('Session expired');
            } elseif (strtotime($row->last_seen) + (.5*3600) < time()) {
                $this->logout_session($session_id);
                return $this->failed('Session expired');
            } else {
                $session_data = unserialize($this->crypt->decrypt($session_id, $row->session_details));
                if (!(isset($session_data['merchant_user_id']))) {
                    return $this->failed('Invalid session id');
                } elseif ($return_info) {
                    return $this->success($session_data);
                } else {
                    return $this->success('Session is valid');
                }
            }
        }
    }

    public function setTimeZone($time_zone, $db = NULL) {
        date_default_timezone_set($time_zone);
        if ($db !== NULL) {
            $dateTimeZoneGmt = new DateTimeZone("GMT");
            $dateTimeZoneCustom = new DateTimeZone($time_zone);
            $dateTimeGmt = new DateTime(date('Y-m-d'), $dateTimeZoneGmt);
            $timeOffset = $dateTimeZoneCustom->getOffset($dateTimeGmt) / 3600;
            $plus = ((int) $timeOffset > 0) ? '+' : '';
            $tz = $plus . $timeOffset;
            $db->query("SET time_zone = '{$tz}:00'");
        }
    }

    protected function apply_fee($card_id, $trans_type_id, $fee_amount, $client_account_id_debit_fee_from, $trans_ref_id, $processor_id, $involved_cardholder_id, $process_revshare = true, $custom_descr = null, $credit_fee_to = null) {
        if (!$this->cardholders_model->applyFee($card_id, $trans_type_id, $fee_amount, $client_account_id_debit_fee_from, $trans_ref_id, $processor_id, $involved_cardholder_id, $process_revshare, $custom_descr, $credit_fee_to)) {
            $response = $this->cardholders_model->get_response();
            return $this->failed($response['ResponseMsg']);
        } else {
            $response = $this->cardholders_model->get_response();
            return $response['ResponseMsg']['TransactionID'];
        }
    }

    protected function get_merchant_fee($client_record_id, $trans_type_id) {
        $query = $this->db->where('client_record_id', $client_record_id)->where('main_transaction_type_id', $trans_type_id)->get('merchant_fees_and_commissions');
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $response = array(
                'fee' => $result[0]['transaction_fee'],
                'charge_to' => $result[0]['charge_to'],
            );
            return $this->success($response);
        } else {
            return $this->failed('No fees defined for the specified parameters');
        }
    }

    public function get_response() {
        return $this->response;
    }

    public function test_jeff()
    {
        $response = array(
            'name' => 'Mark Jeffrey',
            'surname' => 'Ramos',
        );
        return $this->success($response);
    }

    private function omni_billpay_get_response($url)
    {
       $respJson = file_get_contents($url);
       $resp = json_decode($respJson, true);
       return $resp;
    }

    private function omni_billpay_login()
    {
        $url = OMNIBILLPAY_URL."method=login&P01=".OMNIBILLPAY_USER_MERC."&P02=".OMNIBILLPAY_PASSWORD_MERC;
        $resp = $this->omni_billpay_get_response($url);
        if($resp['respcode'] != '0000'){
            return $this->failed("Unable to get session from OMNI BEC");
        }else{
            return $this->success($resp['respmsg']);
        }

    }


    //session, account no, amount, transaction fee
    public function omni_billpay_process_old($params)
    {
        $this->omni_billpay_login();
        $resp = $this->response;
        $session ="";
        if($resp['ResponseId'] != '0000'){
            return $this->failed("Unable to get session");
        }else{
            $session = $resp['ResponseMsg']['SessionID'];
        }
        //$session = $params['session'];
        $account_no = $params['P02'];
        $amount = $params['P03'];
        $transaction_fee =$params['P04'];
        $biller_code =$params['P07'];
        $total_amount = $transaction_fee + $amount;
        $user= OMNIBILLPAY_USER_MERC;
        $account_name=  str_ireplace(' ','%20',$params['P08']);
        /*
        $location=  str_ireplace(' ','%20',$params['location']);
        $clerk=  $params['clerk'];
        $payment_type= str_ireplace(' ','%20',  $params['payment_type']);
        $payment_remarks= str_ireplace(' ','%20', $params['payment_remarks']);
        $country_code= $params['country_code'];
        $currency= $params['currency'];
        */

        $location=  "";
        $clerk=  "";
        $payment_type= "";
        $payment_remarks= "";
        $country_code= "";
        $currency= "";

        $url = OMNIBILLPAY_URL."method=add_payment&P01={$session}&P02={$account_no}&P03={$amount}&P04={$transaction_fee}&P05={$total_amount}&P06={$user}&P07={$biller_code}&P08={$account_name}&P09={$location}&P10={$clerk}&P11={$payment_type}&P12={$payment_remarks}&P13={$country_code}&P14={$currency}";
        $resp = $this->omni_billpay_get_response($url);
        //print_r($resp);
        //die();
        if($resp['respcode'] != '0000'){
            return $this->failed($resp['respmsg']);
        }else{
            return $this->success($resp['respmsg']);
        }
    }

    public function omni_billpay_get_transaction_fee(){
        $this->omni_billpay_login();
        $resp = $this->response;
        $session ="";
        if($resp['ResponseId'] != '0000'){
            return $this->failed("Unable to get session");
        }else{
            $session = $resp['ResponseMsg']['SessionID'];
        }
        $url = OMNIBILLPAY_URL."method=get_transaction_fee&P01={$session}";
        $resp = $this->omni_billpay_get_response($url);
        if($resp['respcode'] != '0000'){
            return $this->failed("Unable to get transaction fee");
        }else{
            return $this->success($resp['respmsg']);
        }
    }

    private function omni_billpay_get_status($params)
    {
        $reference_no =$params['P01'];
        $this->omni_billpay_login();
        $resp = $this->response;
        $session ="";
        if($resp['ResponseId'] != '0000'){
            return $this->failed("Unable to get session");
        }else{
            $session = $resp['ResponseMsg']['SessionID'];
        }
        $url = OMNIBILLPAY_URL."method=getPaymentStatus&P01={$session}&P02={$reference_no}";
        $resp = $this->omni_billpay_get_response($url);
        if($resp['respcode'] != '0000'){
            return $this->failed("Unable to get payment status");
        }else{
            return $this->success($resp['respmsg']);
        }
    }

     //PP 2018/08/17
     public function billpay_validate_account_no($params)
     {
         $session_id = $params['R1'];
         $account_no =$params['R2'];
         $biller_code=$params['R3'];
         if (!$this->validate_session($session_id)) {
             return FALSE;
         } else {
            $this->omni_billpay_login($params);
            $resp = $this->response;
 
            $session ="";
            if($resp['ResponseId'] != '0000'){
                 return $this->failed("Unable to get session");
            }else{
             $session = $resp['ResponseMsg']['SessionID'];
            }
 
            $url = OMNIBILLPAY_URL."method=check_if_account_exists&P01={$session}&P02={$account_no}";
            $resp = $this->omni_billpay_get_response($url);
            if($resp['respcode'] != '0000'){
                 return $this->failed($resp['respmsg']);
            }else{
             if (strpos($resp['respmsg'], 'not') !== false) {
                 return $this->failed($resp['respmsg']);
             }else{
                 $params = array(
                      'P01' => '', //session
                      'P02' =>'', //first_name
                      'P03' => '', //last_name
                      'P04' =>$params['R2'], //account_no
                      'P05' => '', //phone_number
                      'P06' => $params['R3'], //biller_code
                  );
 
                 $this->omni_billpay_search_customer($params);
             }
                 
            }
         }
     }
 

    private function omni_validate_account_no($params)
    {
        $session_id = $params['P01'];
        $account_no =$params['P02'];
        if (!$this->validate_session($session_id)) {
            return FALSE;
        } else {
           $this->omni_billpay_login($params);
           $resp = $this->response;

           $session ="";
           if($resp['ResponseId'] != '0000'){
                return $this->failed("Unable to get session");
           }else{
            $session = $resp['ResponseMsg']['SessionID'];
           }

           $url = OMNIBILLPAY_URL."method=check_if_account_exists&P01={$session}&P02={$account_no}";
           $resp = $this->omni_billpay_get_response($url);
           if($resp['respcode'] != '0000'){
                return $this->failed($resp['respmsg']);
           }else{
                return $this->success($resp['respmsg']);
           }
        }
    }

    public function omni_billpay_search_customer($params)
    {
        $this->omni_billpay_login();
        $resp = $this->response;
        $session ="";
        if($resp['ResponseId'] != '0000'){
            return $this->failed("Unable to get session");
        }else{
            $session = $resp['ResponseMsg']['SessionID'];
        }
        //$session = $params['session'];
        $first_name = $params['P02'];
        $last_name = $params['P03'];
        $account_no = $params['P04'];
        $phone_number = $params['P05'];
        $biller_code = $params['P06'];
        $url = OMNIBILLPAY_URL."method=search_customer_ex&P01={$session}&P02={$first_name}&P03={$last_name}&P04={$account_no}&P05={$phone_number}&P06={$biller_code}";
        $resp = $this->omni_billpay_get_response($url);

        if($resp['respcode'] != '0000'){
            return $this->failed($resp['respmsg']);
        }else{
            return $this->success($resp['respmsg']);
        }
    }

    public function billpay_process_transaction($params)
    {
        $session_id = $params['P01'];
        $user_id= $params['P02'];
        $account_no = $params['P03'];
        $biller_tag = $params['P04'];
        $total_amount = $params['P05'];
        $fee = $params['P06'];
        $clerk = $params['P07'];
        $account_name = $params['P08'];
        $product = $params['P09'];
        $phone_number = $params['P10'];
        $payment_type = "";
        $payment_remarks = "";
        $country_code = "";
        $currency = "";

        if (isset($params['P11'])) { $payment_type = $params['P11']; }
        if (isset($params['P12'])) { $payment_remarks = $params['P12']; }

        if (isset($params['P13'])) { $country_code = $params['P13']; }
        if (isset($params['P14'])) { $currency = $params['P14']; }

        //get merchant id and location by user_id
        $info = $this->get_merchant_terminal_user_details_by_id($user_id);
        //return $this->success($params);
        if (!(isset($info['id']))){
            return $this->failed("Invalid user id.");
        }
        $location = $info['location'];
        $merchant_id = $info['merchant_id'];
        $branch_id = $info['branch_id'];


        if(!is_numeric($total_amount))
        {
            return $this->failed("Unable to process billpay transaction. Please enter valid amount.");
        }

        if (!$this->validate_session($session_id)) {
            return FALSE;
        } else {
            $this->extend_session($session_id);

            //get merchant_id first from merchant_terminal_users then use it as user_reference in user_account to get its id
            $merchant_user_id = $this->get_user_id_by_terminal_username($clerk);
            $this->check_prefund_balance_for_billers($merchant_user_id);
            $balance = $this->response;
            //$balance = $balance['Balance'];
            $balance = $balance['ResponseMsg']['Balance'];

            if($balance<$total_amount)
            {
                return $this->failed("Your Account has insufficient balance.");
                die();
            }

                $this->omni_billpay_login();
                $resp = $this->response;
                $session ="";


                if($resp['ResponseId'] != '0000')
                {
                    return $this->failed("Unable to login to OMNI Billspay. Please try again.");

                }

                //$this->omni_billpay_get_transaction_fee();
//                $fee_resp = $this->response;
//                $session ="";
//
                //override fee if found
//                if($fee_resp['ResponseId'] == '0000')
//                {
//                    $fee = $fee_resp['ResponseMsg'];
//                }

                //$paybills_session = $resp['ResponseMessage']['SessionID'];
                $paybills_session = $resp['ResponseMsg']['SessionID'];
                $param_data = array(
                    'P01' => $session_id,
                    'P02' => 'BILLPAY'
                );
                $this->_get_transaction_fees($param_data);
                $percentage = $this->response['ResponseMsg'];
                $percentage = $percentage[0]['percentage'];
                $amount_to_be_paid = $total_amount-$fee;
                $amount_to_be_paid = $amount_to_be_paid-($amount_to_be_paid * $percentage);

                $amount_to_be_paid = round($amount_to_be_paid, 2);


               //session, account no, amount, transaction fee
               $billpay_params = array(
                    'session' => $paybills_session,
                    'account_no' => $account_no,
                    'amount' => $total_amount - $fee,
                    'transaction_fee' => $fee,
                    'biller_code' => $biller_tag,
                    'account_name' => $account_name,
                    'location' => $location,
                    'clerk' => $clerk,
                    'payment_type' => $payment_type,
                    'payment_remarks' => $payment_remarks,
                    'country_code' => $country_code,
                    'currency' => $currency,
               );

               $this->omni_billpay_process($billpay_params);
               $re = $this->get_response();
               //print_r($re);
               //die();
               //return $this->failed($re);

               if($re['ResponseId']!="0000")
               {
                    if (isset($re['ResponseMsg'])){
                       return $this->failed($re['ResponseMsg']);
                    } else {
                        return $this->failed("Transaction cannot be processed. Please try again.");
                    }

                } else {

                    if (!$this->_debit_credit_client_account($merchant_id, SUPPLIER_ID, $total_amount-$fee, 'Bill Pay ' . $biller_tag . ':' . $account_no, 'vas', 24, 25)) {
                            return false;
                        } else {
                            $settlement_info = $this->get_response();
                            $settlement_info = $settlement_info['ResponseMsg'];
                            $trans_type_id = 13;//pay bill

                            $debit_trans_id = $settlement_info['DebitTransID'];
                            $credit_trans_id = $settlement_info['CreditTransID'];

                            $this->_debit_credit_client_account($merchant_id, SUPPLIER_ID, $fee, 'Bill Pay fee', 'fee', 26, 27);
                            $settlement_info = $this->get_response();
                            $settlement_info = $settlement_info['ResponseMsg'];
                            $fee_trans_id = $settlement_info['DebitTransID'];
                            $fee_credit_trans_id = $settlement_info['CreditTransID'];

                            $billpay_info = array(
                                    'merchant_id'           => $merchant_id,
                                    'biller_code'           => $biller_tag,
                                    'bill_account_no'       => $account_no,
                                    'bill_amount'           => $total_amount-$fee,
                                    'bill_fee_amount'            => $fee,
                                    'product'    => $product,
                                    'clerk'    => $clerk,
                                    'location' => $location,
                                    'bill_account_name' => $account_name,
                                    'bill_account_phone' => $phone_number,
                                    'transaction_id' => $re['ResponseMsg']['reference_no'],
                                    'user_id' => $user_id,
                                    'payment_type' => $payment_type,
                                    'payment_remarks' => $payment_remarks,
                                    //'mango_reference' => $mango_reference_no,
                                    'mobile_assist_reference' => $re['ResponseMsg']['mobile_assist_reference'],
                                    'branch_id' => $branch_id,
                            );
                            $log_id = $this->_log_transaction($billpay_info, 'omni_transactions');
                            $params['FeeTransID'] = $fee_trans_id;
                            $params['FeeCreditTransID'] = $fee_credit_trans_id;
                            $params['DebitTransID'] = $debit_trans_id;
                            $params['CreditTransID'] = $credit_trans_id;
                            $params['TransactionLogID'] = $log_id;
                    }

                    if($log_id>0)
                    {
                        $data = array(
                            'transaction_type' => 'BILLPAY',
                            'P01' => $session_id,
                            'P02' => $total_amount,
                            'P03' => 'OMNI Billspay',
                            'P04' => $re['ResponseMsg']['reference_no'],
                            'P05' => $merchant_id
                        );
                      $this->debit_merchant_prefund_branch($data);
                      //$debit = $this->get_response();
                      //return $this->success($debit);
                      $response = array(
                        'message' => 'Your bill pay transaction is being processed.',
                        'transaction_id' => $re['ResponseMsg']['reference_no'],
                      );
                      //return $this->success('Your bill pay transaction is being processed.');
                      return $this->success($response);
                    }else{
                        return $this->failed("Transaction cannot be processed. Please try again.");
                    }
            }
        }
    }



    private function _debit_credit_client_account($merchant_id, $supplier_id, $transfer_amount, $description, $client_account_type, $debit_trans_type, $credit_trans_type, $refNo = "")
    {
        //$card_number = $this->get_cardnumber_by_id($card_id);
        if (!($client_account_type == 'settlement' || $client_account_type == 'fee' || $client_account_type == 'vas' )) {
            return $this->failed('Invalid client account type');
        } else {
            $log_info_debit = array(
                'supplier_id'   => $supplier_id,
                'merchant_id'   => $merchant_id,
                'ezkard_id'     => -1,
                'terminal_id'   => '',
                'transaction_id' => $this->generate_transaction_id(),
                'amount'        => $transfer_amount,
                'trans_type_id' => $debit_trans_type,
                'description'   => $description,
                'reference_id'  => $refNo,
                'timestamp'     => date('Y-m-d H:i:s'),
                'trans_status_id' => 0,
                'is_merchant' => 1,
            );
            $log_id = $this->_log_transaction($log_info_debit, 'ezkard_transactions');
            if (!$log_id) {
                return false;
            } else {
                $log_info_credit = array(
                    "client_record_id"  => -1,
                    "user_type_id"      => 2,
                    "ref_trans_id"      => $log_id,
                    "trans_type_id"     => $credit_trans_type,
                    "amount"            => $transfer_amount,
                    "description"       => $description,
                    "timestamp"         => date('Y-m-d H:i:s'),
                    "is_merchant"       => 1,
                    "merchant_id"       => $merchant_id,
                );
                $client_trans_id = $this->_log_transaction($log_info_credit, 'client_transactions');
                if (!$client_trans_id) {
                    return false;
//                } elseif (!$this->client_account_add($client_id, $client_account_type, $transfer_amount)) {
//                    return false;
                    //todo: rollback
                } else {
                    $account_types = array('prefund' => 0, 'settlement' => 1, 'fee' => 2, 'vas' => 3);
                    $transaction_details = array(
                        'client_transaction_id' => $client_trans_id,
                        'client_account_type'   => $account_types[$client_account_type],
                        'client_record_id'      => $merchant_id,
                        'amount'                => $transfer_amount,
                    );
                    $details_id = $this->_log_transaction($transaction_details, 'client_transaction_details');
                    if (!$details_id) {
                        return false;
                    } else {
                        $response = array(
                           'TransactionID'      => $details_id,
                           'DebitTransID'      => $log_info_debit['transaction_id'],
                           'CreditTransID' =>  $client_trans_id,
                           'ResponseMessage'    => 'card transfer successful',
                        );
                        return $this->success($response);
                    }
                }
            }
        }
    }

     protected function _log_transaction($log_parameters, $log_table) {
        if (is_array($log_parameters) && count($log_parameters) > 0) {
            $this->db
                ->set($log_parameters)
                ->insert($log_table);
            if(! ($this->db->affected_rows() > 0)) {
                return $this->failed('Unable to save logs');
            } else {
                $auto_id = $this->db->insert_id();
                return $auto_id;
                //return $this->success('Logs successfully saved');
            }
        }
    }

    private function _get_transaction_fees($params)
    {
       $session_id = $params['P01'];
       $trans_type = $params['P02'];
       $trans_fee = $this->db->select('*')->where('transaction_type', $trans_type)->get('customer_transaction_fee');
       $trans_fee = $trans_fee->result_array();
       return  $this->success($trans_fee);
    }

    protected function generate_transaction_id() {
        do {
            $trans_id = time();
            $query = $this->db->where('transaction_id', $trans_id)->get('ezkard_transactions');
        } while ($query->num_rows() > 0);
        return $trans_id;
    }

     public function checkSupervisorPIN($params) {
         $session_id = $params['P01'];
         $user_name = $params['P02'];
         $pin = $params['P03'];
         if (!$this->validate_session($session_id)) {
            return FALSE;
        } else {
            $branch = $this->get_branch_id_by_user_name($user_name);
            //return $this->failed($branch);
            $branch_id = $branch != false? $branch: -1;
            $script = "select * from merchant_terminal_users
                    where user_status_id = 0 and branch_user_type_id = 1 and branch_id ='" . $branch_id . "' and pin_1='". $pin . "'";
            $query = $this->db->query($script);
            if (($query->num_rows() > 0)) {
                return $this->success("PIN is valid");
            } else {
                return $this->failed("Invalid PIN");
            }
        }

    }

    protected function get_branch_id_by_user_name($user_name) { //client_record_id
        $query = $this->db->select('branch_id')
            ->from("merchant_terminal_users")
            ->where('user_name', $user_name)
            ->get();
        if (!($query->num_rows() > 0)) {
            return $this->failed('Merchant ID not found');
        } else {
            $row = $query->row();
            return $row->branch_id;
        }
    }

    private function validate_session_pos($session_id, $device_id) {
        $this->load->model('ezsessions_model');
        $this->ezsessions_model->session_validation($session_id, $device_id);
        $response = $this->ezsessions_model->get_response();
        if($response['ResponseId'] == '0000') {
            $this->session_response = $response['ResponseData'];
            $this->client_record_id = $this->session_response['ClientDetails']['ClientRecordId'];
            $query = $this->db->where('id', $this->client_record_id)->get('clients');
            if (!($query->num_rows() > 0)) {
                return $this->failed('Invalid client id');
            } else {
                $row = $query->result_array();
                $this->client_details = $row[0];
                return $this->success($response['ResponseMsg']);
            }
        } else {
            return $this->failed($response['ResponseMsg']);
        }
    }

     public function debit_merchant_prefund_branch($params) {
        if (!isset($params['P01'], $params['P02'], $params['P03'], $params['P04'])) {
            return $this->failed('Missing required parameters');
        } elseif (!($params['P01'] != '' && $params['P02'] != '' && $params['P03'] != '')) {
            return $this->failed('Missing required parameters');
        } elseif (!(is_numeric($params['P02']) && $params['P02'] > 0)) {
            return $this->failed('Invalid amount');
        } elseif (!(is_numeric($params['P02']) && $params['P02'] > 0)) {
            return $this->failed('Invalid amount');
        } elseif ($params['P04'] != '' && !(is_numeric($params['P04']))) {
            return $this->failed('Reference value must be numeric');
        } elseif (!$this->validate_session($params['P01'])) {
            return false;
        } else {
            //$this->extend_session($params['P01']);
            //$response = $this->get_response();
            $merchant_user_id = $params['P05'];

            $user_details = $this->get_merchant_user_details_branch($merchant_user_id);
            if (!(isset($user_details['user_reference']) && is_numeric($user_details['user_reference']) && $user_details['user_reference'] > 0)) {
                return $this->failed('Invalid merchant.');
            } else {
                $client_record_id = $user_details['user_reference'];
                $debit_merchant_params = array(
                    'function'          => 'less',
                    'ref_trans_id'      => is_numeric($params['P04'])? $params['P04']: time(),
                    'transfer_amount'   => $params['P02'],
                    'client_id'         => $client_record_id,
                    'trans_type'        => 5, //client prefund debit
                    'client_account_type' => 'prefund',
                    'description'       => $params['P03'],
                );
                if (!$this->adjust_client_balance($debit_merchant_params)) {
                    return $this->failed('Unable to perform prefund');
                } else {
                    $debit_amount_response = $this->get_response();
                    $currency = $this->get_currency_by_merchant_id($client_record_id);
                    $success_response = array(
                        'Reference' => $debit_amount_response['ResponseMsg']['TransactionID'],
                        'DebitedAmount' => $params['P02'],
                        'Currency' => $currency != false? $currency: '',
                    );
                    return $this->success($success_response);
                }
            }
        }
    }

    protected function get_merchant_user_details_branch($merchant_user_id) {
        $query = $this->db->where('user_reference', $merchant_user_id)->get('user_account');
        if (!($query->num_rows() > 0)) {
            return false;
        } else {
            $result = $query->result_array();
            return $result[0];
        }
    }

    protected function get_merchant_terminal_user_details_by_id($merchant_terminal_user_id) {
        $query = $this->db->where('id', $merchant_terminal_user_id)->get('merchant_terminal_users');
        if (!($query->num_rows() > 0)) {
            return false;
        } else {
            $result = $query->result_array();
            return $result[0];
        }
    }

    public function get_available_country_for_billpay()
    {
        $countries[] = array(
            "country_code" => "BHS",
            "country_name" => "Bahamas"
        );

        $countries[] = array(
            "country_code" => "JAM",
            "country_name" => "Jamaica"
        );

        return $this->success($countries);
    }

    //P02 = country_code
    public function get_available_billers($params)
    {
        $country_code = $params["P02"];
        if($country_code == "JAM")
        {
            $billers = $this->paymaster_get_billers("");

            if($billers['success'] == 1)
            {
                return $this->success($billers["ResponseMsg"]);
            }else
            {
                return $this->failed("Unable to get billers.");
            }
        }else if ($country_code == "BHS"){
            $billers = $this->omni_get_billers();

            
            if($billers['success'] == 1)
            {
                return $this->success($billers["ResponseMsg"]);   
            }else{
                return $this->failed("Unable to get billers.");
            }
        }else{
            return $this->failed("Invalid country code.");
        }
    }


    //R1 = client_id, R2 = branch_id, R3 = terminal_id, R4 = biller_mode, R5 = biller_code, R6 = biller_name
    //R7 = account_name, R8 = account_number, R9 = country, R10 = mobile number,
    //R11 = amount, R12 = vat , R13 = transaction_fee
    public function process_billpay($params)
    {
        $client_record_id = $params["R1"];
        $branch_id = $params["R2"];
        $terminal_id = $params["R3"];

        $amount = $params['R11'];
        $amount = str_replace(',', '', $amount);
        $vat = $params['R12'];
        $vat = str_replace(',', '', $vat);
        $fee = $params['R13'];
        $fee = str_replace(',', '', $fee);

        $biller_mode = $params['R4'];
        $biller_code = $params['R5'];
        $biller_name = $params['R6'];
        $account_name = $params['R7'];
        $account_number = $params['R8'];
        $country = $params['R9'];
        $mobile_number = $params['R10'];
        

        
        $params = array(
            "mode"              => $biller_mode,
            "biller_code"       => $biller_code,
            "biller_name"       => $biller_name,
            "fee"               => $fee,
            "amount"            => $amount,
            "account_name"      => $account_name,
            "account_number"    => $account_number,
            "total_amount"      => $amount + $fee + $vat,
            "country"           => $country,
            "mobile_number"     => $mobile_number,
            'fee_transaction'   => $fee,
            'fee_vat'           => $vat,
            'customer_sender_id'           => -1,
            'customer_mobile'           => "",
            'customer_name'           => "",
            'customer_id'           => -1,
            'merchant_id' => $client_record_id,
            'terminal_id' => $terminal_id,
            'branch_id' => $branch_id
        );

        // print_r($params);
        // die();

        if($country == "BHS")
        {
            if(ENV == "PROD")
            {
                $billpay_response = $this->omni_billpay_process($params);
            }else{
                $billpay_response["success"] = true;
                $billpay_response["message"]["reference_no"] = time();
            }



            if($billpay_response['success']){
                $resp = $billpay_response['message'];
                $ref_no = $resp['reference_no'];
                $billpay_log_id = $this->billpay_log_details($params, $resp['reference_no']);
                $debit_merchant_params = array(
                    'function'          => 'less',
                    'ref_trans_id'      => is_numeric($resp['reference_no'])? $resp['reference_no']: time(),
                    'transfer_amount'   => $params['total_amount'],
                    'client_id'         => $client_record_id,
                    'trans_type'        => 5, //client prefund debit
                    'client_account_type' => 'prefund',
                    'description'       => 'Billpay transaction',
                );

                // $this->_adjust_client_balance($debit_merchant_params);
                $this->adjust_client_balance($debit_merchant_params);
                $resp = $this->response;
                $transaction_id = $resp['ResponseMsg']['TransactionID'];
                
                $debit_merchant_params = array(
                    'function'          => 'add',
                    'ref_trans_id'      => is_numeric($transaction_id)? $transaction_id: time(),
                    'transfer_amount'   => $params['total_amount'],
                    'client_id'         => SUPPLIER_ID,
                    'trans_type'        => 5, //client prefund debit
                    'client_account_type' => 'prefund',
                    'description'       => 'Billpay transaction',
                );

                $this->adjust_client_balance($debit_merchant_params);
                $resp = $this->response;
                if($resp['ResponseId'] == "0000")
                {
                    // $this->data['success'] = true;
                    // $this->data['ResponseMsg'] = $r;
                    // return $this->success($r);

                    $response_msg = array(
                            "Message" => "Billpay transaction has been processed.",
                            "TransactionId" => $ref_no,
                    );
                    return $this->success($response_msg);
                }
                else
                {
                    // $this->data['success'] = false;
                    // $this->data['ResponseMsg'] = "Unable to process request";
                    return $this->failed("Unable to process request.");
                }
            }else{
                // $this->data['success'] = false;
                // $this->data['ResponseMsg'] = $billpay_response['message'];
                return $this->failed($billpay_response['message']);
            }

        }
        else if($params['country'] == "JAM")
        {
            if(ENV == "PROD")
            {
                $billpay_response = $this->paymaster_pay_bill($params);
            }else{
                $billpay_response["success"] = true;
                $billpay_response["message"]['bill']["transaction_number"] = time();
            }
            
            if($billpay_response['success']){
                
                if(ENV == "PROD")
                {
                    $resp = json_decode($billpay_response['message'], true);
                }else{
                    $resp = $billpay_response;
                }

                $ref_no = $resp["bill"]["transaction_number"];

                $billpay_log_id = $this->billpay_log_details($params, $ref_no);
                $debit_merchant_params = array(
                    'function'          => 'less',
                    'ref_trans_id'      => is_numeric($ref_no)? $ref_no: time(),
                    'transfer_amount'   => $params['total_amount'],
                    'client_id'         => $client_record_id,
                    'trans_type'        => 5, //client prefund debit
                    'client_account_type' => 'prefund',
                    'description'       => 'Billpay transaction',
                );

                $this->adjust_client_balance($debit_merchant_params);
                $resp = $this->response;
                $transaction_id = $resp['ResponseMsg']['TransactionID'];
                
                $debit_merchant_params = array(
                    'function'          => 'add',
                    'ref_trans_id'      => is_numeric($transaction_id)? $transaction_id: time(),
                    'transfer_amount'   => $params['total_amount'],
                    'client_id'         => SUPPLIER_ID,
                    'trans_type'        => 5, //client prefund debit
                    'client_account_type' => 'prefund',
                    'description'       => 'Billpay transaction',
                );

                $this->adjust_client_balance($debit_merchant_params);
                $resp = $this->response;
                if($resp['ResponseId'] == "0000")
                {
                    // $this->data['success'] = true;
                    // $this->data['ResponseMsg'] = $r;
                    // return $this->success($r);

                    $response_msg = array(
                            "Message" => "Billpay transaction has been processed.",
                            "TransactionId" => $ref_no,
                    );
                    return $this->success($response_msg);
                }
                else
                {
                    // $this->data['success'] = false;
                    // $this->data['ResponseMsg'] = "Unable to process request";
                    return $this->failed("Unable to process request.");
                }

                
            }else{
                // $this->data['success'] = false;
                // $this->data['ResponseMsg'] = "Unable to process request";
                return $this->failed("Unable to process request.");
            }
        }
        else
        {
            return $this->failed("Unable to process request.");
        }
    }


    private function paymaster_get_biller($biller_code)
    {
        $billers_param = array(
            "method"        => "get_billers",
            "api_key"       => PAYMASTERBILLPAY_APIKEY
        );

        $paymaster_result = $this->paymaster_get_curl_result($billers_param);

        if($paymaster_result["respcode"] != '0000'){
            $data['success'] = false;
            $data['ResponseMsg'] = $paymaster_result['respmsg'];
        }else{

            $billers = json_decode($paymaster_result['respmsg']);
            $billers = $billers->billers;
            $biller = array();
            foreach($billers as $value)
            {
                if($biller_code == "")
                {
                    $biller[] = array(
                        'id'            => $value->id,
                        'biller_code'   => $value->id,
                        'biller_name'   => $value->name,
                        'fee_amount'    => 0,
                        'mode'          => '',
                        'trans_type'    => $value->transtype
                    );   
                }
                else if(strtoupper($value->id) == strtoupper($biller_code))
                {
                    $biller[] = array(
                        'id'            => $value->id,
                        'biller_code'   => $value->id,
                        'biller_name'   => $value->name,
                        'fee_amount'    => 0,
                        'mode'          => '',
                        'trans_type'    => $value->transtype
                    );
                }
            }
            $data['success'] = true;
            $data['ResponseMsg'] = $biller;
        }

        return $data;
    }

    private function paymaster_pay_bill($params)
    {
        $account_no = $params['account_number'];
        $biller_tag = $params['biller_code'];
        $total_amount = $params['total_amount'];
        $currency = "USD";

        $trans_type = array();
        $biller_details = $this->paymaster_get_biller($biller_tag, true);
        $trans_type = json_encode($biller_details->transtype);
        $trans_type = json_decode($trans_type, true);

        $ref_info = $trans_type[0]['ref_info'];
        $ctr = count($ref_info) - 1;
        for ($x = 0; $x <= $ctr; $x++) {
            $trans_type[0]['ref_info'][$x]['value'] = "";
        }

        if (isset($params['currency'])) { $currency = $params['currency']; }

        if(!is_numeric($total_amount))
        {
            $response = array(
                'success'   => false,
                'message'   => "Please enter valid amount."
            );
        }
        else
        {
            $check_account = $this->paymaster_search_by_account_no($params);

            if($check_account)
            {
                $billpay_params = array(
                    'method'            => 'pay_bill',
                    'biller_id'         => $biller_tag,
                    'account_number'    => $account_no,
                    'payment_amount'    => $total_amount,
                    'payment_currency'  => $currency,
                    'local_ref'         => time(),
                    'request_date'      => date('Y-m-d H:i:s'),
                    'trans_type'        => $trans_type,
                    'api_key'           => PAYMASTERBILLPAY_APIKEY
                );

                $paymaster_result = $this->paymaster_get_curl_result($billpay_params);

                if($paymaster_result["respcode"] != '0000'){
                    $response = array(
                        'success'   => false,
                        'message'   => $paymaster_result['respmsg']
                    );
                }else{

                    $response = array(
                        'success'   => true,
                        'message'   => $paymaster_result['respmsg']
                    );
                }
            }
            else
            {
                $response = array(
                    'success'   => false,
                    'message'   => "Invalid account number."
                );
            }
        }

        return $response;
    }

    private function billpay_log_details($params, $reference_no)
    {

        $this->db->query("ALTER TABLE omni_transactions ADD COLUMN IF NOT EXISTS
        terminal_id VARCHAR(250) DEFAULT NULL");

        $this->db->query("ALTER TABLE paymaster_transactions ADD COLUMN  IF NOT EXISTS
        terminal_id VARCHAR(250) DEFAULT NULL");

        $log_id = false;
        $billpay_info = array(
            'merchant_id'           => $params['merchant_id'],
            'biller_code'           => $params['biller_code'],
            'bill_account_no'       => $params['account_number'],
            'bill_amount'           => $params['amount'],
            // 'bill_fee_amount'       => $params['fee_transaction'],
            'bill_fee_amount'       => $params['fee'],
            'product'               => '',
            // 'clerk'                 => $this->session->FullName,
            // 'location'              => $this->session->Location,
            'clerk'                 => '',
            'location'              => '',
            'bill_account_name'     => $params['account_name'],
            'bill_account_phone'    => $params['mobile_number'],
            'transaction_id'        => $reference_no,
            'user_id'               => -1,
            'payment_type'          => isset($params['payment_type']) ? $params['payment_type'] : 'Cash',
            'payment_remarks'       => isset($params['payment_remarks']) ? $params['payment_remarks'] : '',
            'branch_id'             => $params['branch_id'],
            'terminal_id'           => $params['terminal_id']
        );

        if($params['country'] == "BHS")
        {
            $log_id = $this->log_billpay_transaction($billpay_info);
        }
        elseif($params['country'] == 'JAM')
        {
            $log_id = $this->log_paymaster_transaction($billpay_info);
        }
        else
        {

        }


        $billpay_info = array(
            'merchant_id'           => $params['merchant_id'],
            'biller_code'           => $params['biller_code'],
            'bill_account_no'       => $params['account_number'],
            'bill_amount'           => $params['amount'],
            'bill_fee_amount'       => $params['fee_transaction'],
            'bill_vat_amount'       => $params['fee_vat'],
            'clerk'                 => '',
            'location'              => '',
            'bill_account_name'     => $params['account_name'],
            'transaction_id'        => $reference_no,
            'user_id'               => -1,
            'payment_type'          => isset($params['payment_type']) ? $params['payment_type'] : 'Cash',
            'payment_remarks'       => isset($params['payment_remarks']) ? $params['payment_remarks'] : '',
            'branch_id'             => $params['branch_id'],
            'customer_sender_id'           => $params['customer_sender_id'],
            'customer_mobile'           => $params['customer_mobile'],
            'customer_name'           => $params['customer_name'],
            'customer_id'           => $params['customer_id'],
        );
        

        $loginfo = $this->log_billpay_web_transaction($billpay_info);

        $webpos_transaction = array(
            'transaction_id'        => $reference_no,
            'transaction_type'      => "BILLPAY",
            'merchant_id'           => $params['merchant_id'],
            'branch_id'             => $params['branch_id'],
            'terminal_user_id'      => -1,
            'terminal_id'           => $params['terminal_id'],
            'amount'                => $params['amount'],
            'fee_amount'            => $params['fee_transaction'],
            'vat_amount'            => $params['fee_vat'],
            'total_amount'          => number_format(($params['amount'] + $params['fee_transaction'] + $params['fee_vat']),2),
        );

        $loginfo = $this->webpos_transaction($webpos_transaction);

        return $log_id;
    }


    public function webpos_transaction($insert_data)
    {
        $this->db->set($insert_data)->insert('webpos_transaction');
        if(!($this->db->affected_rows() > 0)) {
            return false;
        } else {
            $auto_id = $this->db->insert_id();
            return $auto_id;
        }
    }
    
    public function log_billpay_transaction($insert_data)
    {
        $this->db->set($insert_data)->insert('omni_transactions');
        if(!($this->db->affected_rows() > 0)) {
            return false;
        } else {
            $auto_id = $this->db->insert_id();
            return $auto_id;
        }
    }

    public function log_paymaster_transaction($insert_data)
    {
        $this->db->set($insert_data)->insert('paymaster_transactions');
        if(!($this->db->affected_rows() > 0)) {
            return false;
        } else {
            $auto_id = $this->db->insert_id();
            return $auto_id;
        }
    }

    public function log_billpay_web_transaction($insert_data)
    {
        $this->db->set($insert_data)->insert('billpay_web_transactions');
        if(!($this->db->affected_rows() > 0)) {
            return false;
        } else {
            $auto_id = $this->db->insert_id();
            return $auto_id;
        }
    }

    private function omni_billpay_process($params)
    {
        $resp = $this->omni_billpay_login();
        $resp = $this->response;
        $response = array(
            'success'   => false,
            'message'   => 'Unable to get session'
        );
        if($resp['ResponseId'] == '0000')
        {
            $session = $resp['ResponseMsg']['SessionID'];

            $amount = $params['amount'];
            $transaction_fee = 0;
            $total_amount = $params['total_amount'];
            $location="";
            $clerk=  "";

            $payment_type= "cash";
            $payment_remarks= "";
            $country_code= "";
            $currency= "USD";

            $billpay_param = array(
                'method'    => 'add_payment',
                'P01'       => $session,
                'P02'       => $params['account_number'],
                'P03'       => $amount,
                'P04'       => $params['fee'],
                'P05'       => $total_amount,
                'P06'       => OMNIBILLPAY_USER_MERC,
                'P07'       => $params['biller_code'],
                'P08'       => $params['account_name'],
                'P09'       => $location,
                'P10'       => $clerk,
                'P11'       => $payment_type,
                'P12'       => $payment_remarks,
                'P13'       => $country_code,
                'P14'       => $currency,
            );

            $url = OMNIBILLPAY_URL . http_build_query($billpay_param);
            $resp = $this->omni_billpay_get_response($url);
            if($resp['respcode'] != '0000'){
                $response = array(
                    'success'   => false,
                    'message'   => $resp['respmsg']
                );
            }else{
                $response = array(
                    'success'   => true,
                    'message'   => $resp['respmsg']
                );
            }

            return $response;
        }
        else
        {
            return $response;
        }
    }


    public function omni_get_billers()
    {
        $resp = $this->omni_billpay_login();
        $session ="";
        $resp = $this->response;
        if($resp['ResponseId'] != '0000'){
            $data['success'] = false;
            $data['ResponseMsg'] = 'Unable to get session';
        }else{
            $session = $resp['ResponseMsg']['SessionID'];
        }

        $url = OMNIBILLPAY_URL."method=get_biller_list&P01={$session}&P02=true&P03=true";

        $resp = $this->omni_billpay_get_response($url);

        if($resp['respcode'] != '0000'){
            $data['success'] = false;
            $data['ResponseMsg'] = $resp['respmsg'];
        }else{
            $data['success'] = true;
            $data['ResponseMsg'] = $resp['respmsg'];
        }
        return $data;

    }

    public function omni_get_biller_by_code($params)
    {
        $this->omni_billpay_login();
        $resp = $this->response;
        $session ="";
        if($resp['ResponseId'] != '0000'){
            return $this->failed("Unable to get session");
        }else{
            $session = $resp['ResponseMsg']['SessionID'];
        }
        $biller_code = $params['P02'];
        $url = OMNIBILLPAY_URL."method=get_biller_list_by_code&P01={$session}&P02={$biller_code}";
        $resp = $this->omni_billpay_get_response($url);

        if($resp['respcode'] != '0000'){
            return $this->failed($resp['respmsg']);
        }else{
            return $this->success($resp['respmsg']);
        }
    }

     protected function get_user_id_by_terminal_username($username) { //client_record_id
        $query = $this->db->select('user_account.id')
            ->from("merchant_terminal_users")
            ->join("user_account", "merchant_terminal_users.merchant_id = user_account.user_reference", "inner")
            ->where('merchant_terminal_users.user_name', $username)
            ->where('merchant_terminal_users.user_status_id', "0")
            ->get();
        if (!($query->num_rows() > 0)) {
            return $this->failed('Merchant ID not found');
        } else {
            $row = $query->row();
            return $row->id;
        }
    }

    /*private function mobile_assist_authentication()
    {
        ini_set('max_execution_time', 0);
        $data = array(
          'username' => MOBILE_ASSIST_UNAME,
          'password' => MOBILE_ASSIST_PASS,
        );
        $req = curl_init(MOBILE_ASSIST_URL . "/authenticate");
        curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($req, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        curl_setopt($req, CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt($req, CURLOPT_POST, true );
        curl_setopt($req, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($req, CURLOPT_CONNECTTIMEOUT ,0);
        $respCode = curl_getinfo($req, CURLINFO_HTTP_CODE);
        $resp =curl_exec($req);
        $resp = json_decode($resp, true);
        curl_close($req);
        return $resp;
    }   */

    private function mobile_assist_authentication()
    {
        ini_set('max_execution_time', 0);
        $data = array(
          'username' => MOBILE_ASSIST_UNAME,
          'password' => MOBILE_ASSIST_PASS,
        );
        $postdata = http_build_query(
            array(
                'username' => 'manual',
                'password' => 'manual123!',
            )
        );

        $opts = array('http' =>
            array(
                'method'  => 'POST',
                'header'  => 'Content-type: application/x-www-form-urlencoded',
                'content' => $postdata
            )
        );

        $context  = stream_context_create($opts);

        $result = file_get_contents(MOBILE_ASSIST_URL . "/authenticate", false, $context);
        $result = json_decode($result, true);
        return $result;
    }

    public function mobile_assist_validate_mobile_number($params)
    {
        $session_id=$params['P01'];
        $mobileNum= $params['P02'];
        $countryCode=$params['P03'];

        if (strpos($mobileNum,'+') == false) {
            $mobileNum = "+".$mobileNum;
        }

        if (!$this->validate_session($session_id)) {
            return FALSE;
        } else {
            $this->omni_billpay_login();
            $resp = $this->response;
            $session ="";
            if($resp['ResponseId'] != '0000'){
                return $this->failed("Unable to get session");
            }else{
                $session = $resp['ResponseMsg']['SessionID'];
            }
            $biller_code = $params['P02'];
            $url = OMNIBILLPAY_URL."method=mobile_assist_validate_mobile_number&P01={$session}&P02={$mobileNum}&P03={$countryCode}";
            //print_r($url);
            //die();

            $resp = $this->omni_billpay_get_response($url);

            if($resp['respcode'] != '0000'){
                return $this->failed($resp['respmsg']);
            }else{
                return $this->success($resp['respmsg']);
            }
        }
    }

    public function mobile_assist_create_payment($params)
    {
        //return $this->success($params);
        $authentication = $this->mobile_assist_authentication();
        if (!isset($authentication['token']))
        {
            return $this->failed('Invalid token authentication. Please contact system administrator');
        }

        $session_id=$params['P01'];
        $mobileNum= $params['P02'];
        $countryCode=$params['P03'];
        $amount=$params['P04'];
        $currency=$params['P05'];
        $manualSystemTransnId=$params['P06'];

        if (strpos($mobileNum,'+') == false) {
            $mobileNum = "+".$mobileNum;
        }

        $url= MOBILE_ASSIST_URL . "/payments/manual";

        if (!$this->validate_session($session_id)) {
            return FALSE;
        } else {
           $data = array(
              'mobileNum' => $mobileNum,
              'countryCode' => $countryCode,
              'amount' => $amount,
              'currency' => $currency,
              'manualSystemTransnId' => $manualSystemTransnId,
              'manualSystemName' => 'Mango',
           );
           $opts = array('http' =>
                array(
                    'method'  => 'POST',
                    'header'  => "Content-type: application/json\r\n"
                               . "x-auth-token: {$authentication['token']}\r\n",
                    'content' => json_encode($data),
                    'ignore_errors' => '1',
                )
            );


            $context  = stream_context_create($opts);
            $result = file_get_contents($url, false, $context);
            $resp = json_decode($result, true);
            if(isset($resp['message'])){
                return $this->failed($resp['message']);
            }else{
                return $this->success($resp);
            }
        }
    }

    /*
    public function mobile_assist_create_payment($params)
    {
        //return $this->success($params);
        $authentication = $this->mobile_assist_authentication();
        if (!isset($authentication['token']))
        {
            return $this->failed('Invalid token authentication. Please contact system administrator');
        }

        $mobileNum= $params['P01'];
        $countryCode=$params['P02'];
        $amount=$params['P03'];
        $currency=$params['P04'];
        $manualSystemTransnId=$params['P05'];

        if (strpos($mobileNum,'+') == false) {
            $mobileNum = "+".$mobileNum;
        }

        $data = array(
          'mobileNum' => $mobileNum,
          'countryCode' => $countryCode,
          'amount' => $amount,
          'currency' => $currency,
          'manualSystemTransnId' => $manualSystemTransnId,
          'manualSystemName' => 'Mango',
        );
        $url= MOBILE_ASSIST_URL . "/payments/manual";
        $data_string = json_encode($data);
        ini_set('max_execution_time', 0);
        $req = curl_init($url);
        curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($req, CURLOPT_HTTPHEADER, array('Content-Type: application/json', "x-auth-token: " . $authentication['token']));
        curl_setopt($req, CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt($req, CURLOPT_POST, true );
        curl_setopt($req, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($req, CURLOPT_CONNECTTIMEOUT ,0);
        $respCode = curl_getinfo($req, CURLINFO_HTTP_CODE);
        $resp = curl_exec($req);
        $resp = json_decode($resp, true);
        curl_close($req);

        if(isset($resp['message'])){
            return $this->failed($resp['message']);
        }else{
            return $this->success($resp);
        }
    }
   */

    public function mobile_assist_delete_payment($params)
    {
        $authentication = $this->mobile_assist_authentication();
        if (!isset($authentication['token']))
        {
            return $this->failed('Invalid token authentication. Please contact system administrator');
        }

        $session_id=$params['P01'];
        $mobileAssistTransnId= $params['P02'];
        $manualSystemTransnId=$params['P03'];

        if (strpos($mobileNum,'+') == false) {
            $mobileNum = "+".$mobileNum;
        }

        $url= MOBILE_ASSIST_URL . "/payments/cancelManual";

        if (!$this->validate_session($session_id)) {
            return FALSE;
        } else {
           $data = array(
                'mobileAssistTransnId'=>$mobileAssistTransnId,
                'manualSystemTransnId'=>$manualSystemTransnId
           );
           $opts = array('http' =>
                array(
                    'method'  => 'PUT',
                    'header'  => "Content-type: application/json\r\n"
                               . "x-auth-token: {$authentication['token']}\r\n",
                    'content' => json_encode($data),
                    'ignore_errors' => '1',
                )
            );


            $context  = stream_context_create($opts);
            $result = file_get_contents($url, false, $context);
            $resp = json_decode($result, true);
            if(isset($resp['message'])){
                return $this->failed($resp['message']);
            }else{
                return $this->success($resp);
            }
        }
    }

   /*
    public function mobile_assist_delete_payment($params)
    {
        //return $this->success($params);
        $authentication = $this->mobile_assist_authentication();
        if (!isset($authentication['token']))
        {
            return $this->failed('Invalid token authentication. Please contact system administrator');
        }

        $mobileAssistTransnId=$params['P01'];
        $manualSystemTransnId=$params['P02'];

        $url= MOBILE_ASSIST_URL . "/payments/cancelManual";
        $data = array(
            'mobileAssistTransnId'=>$mobileAssistTransnId,
            'manualSystemTransnId'=>$manualSystemTransnId
        );
        $data_json = json_encode($data);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', "x-auth-token: " . $authentication['token']));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response  = curl_exec($ch);
        $resp = json_decode($response, true);
        curl_close($req);
        if(isset($resp['message'])){
            return $this->failed($resp['message']);
        }else{
            return $this->success($resp);
        }
    }

    */


    //MOBILE TOP UP

    function process_topup_transaction($params)
    {
        $session_id = $params['P01'];
        $customer_id = $params['P02'];//optional
        $carrier_id = $params['P03'];
        $carrier_name = $params['P04'];
        $country_id = $params['P05'];
        $payment_type = $params['P06'];//optional
        $sku_id = $params['P07'];
        $mobile_number = $params['P08'];
        $amount = $params['P09'];

        if(!is_numeric($carrier_id))
        {
            return $this->failed("Unable to process topup. Please enter a valid carrier id.");
        }
        if($carrier_name == "")
        {
            return $this->failed("Unable to process topup. Please enter a carrier name.");
        }
        if(!is_numeric($country_id))
        {
            return $this->failed("Unable to process topup. Please enter a valid country id.");
        }

        if(!is_numeric($sku_id))
        {
            return $this->failed("Unable to process topup. Please enter an valid sku id.");
        }
        if($mobile_number == "")
        {
            return $this->failed("Unable to process topup. Please enter a mobile number.");
        }
        if(!is_numeric($amount))
        {
            return $this->failed("Unable to process topup. Please enter a valid amount.");
        }

        if (!$this->validate_session($session_id)) {
            return FALSE;
        } else {
            $this->extend_session($session_id);
            $sess_resp = $this->get_response();
            $merchant_user_id = $sess_resp['ResponseMsg']['merchant_user_id'];
            $user_details = $this->get_merchant_user_details($merchant_user_id);

            $merchant_id = $user_details['user_reference'];

            if($customer_id != '' && !empty($customer_id))
            {
                $customer_info = $this->get_customer_info($customer_id);
                $card_id = $customer_info['ezkard_account_id'];
                $merchant_id = $customer_info['merchant_id'];
            }
            else
            {
                $card_id = "";
            }

            $this->check_prefund_balance($session_id);
            $balance = $this->get_response();
            $balance = $balance['ResponseMsg']['Balance'];

            if($balance<$amount)
            {
                return $this->failed("Your SunCash Account has insufficient balance.");
                die();
            }

            if (!$this->_debit_credit_client_account_topup($card_id, $merchant_id, $amount, 'Mobile topup :' . $mobile_number, 'settlement', 41, 42, "")) {
                return false;
            } else {
                $settlement_info = $this->get_response();
                $settlement_info = $settlement_info['ResponseMsg'];
                $trans_type_id = 13;//pay bill
                $debit_trans_id = $settlement_info['DebitTransID'];

                $billpay_info = array(
                    'ezkard_id'                 => $card_id,
                    'sku_id'                    => $sku_id,
                    'amount'                    => $amount,
                    'fee_amount'                => 0,
                    'fee_transaction_id'        => -1,
                    'settlement_transaction_id' => $debit_trans_id, //$settlement_info['TransactionID'],
                );

                $log_id = $this->_log_transaction($billpay_info, 'mobile_topup_transactions');

                $credit_trans_id = $settlement_info['CreditTransID'];
                $params['DebitTransID'] = $debit_trans_id;
                $params['CreditTransID'] = $credit_trans_id;
                $params['TransactionLogID'] = $log_id;
                $params['merchant_id'] = $merchant_id;

                $cron_trans = array(
                    'parameters'            => serialize($params),
                    'transaction_type'      => 'TOPUP'
                );

                $log_id = $this->_log_transaction($cron_trans, 'merchant_cron_transactions');
            }
            if($log_id>0)
            {
                $debit_merchant_params = array(
                    'function'              => 'less',
                    'ref_trans_id'          => time(),
                    'transfer_amount'       => $amount,
                    'client_id'             => $merchant_id,
                    'trans_type'            => 5, //client prefund debit
                    'client_account_type'   => 'prefund',
                    'description'           => 'Mobile topup :' . $mobile_number,
                );
                if (!$this->adjust_client_balance($debit_merchant_params)) {
                    return $this->failed('Unable to perform prefund');
                } else {

                    $email_settings = $this->get_system_settings('send_mail_template_for_processing_topup');
                    $sms_settings = $this->get_system_settings('send_sms_template_for_processing_topup');

                    $email_header = $this->get_system_settings('send_email_header');
                    $email_footer = $this->get_system_settings('send_email_footer');
                    $email_links = $this->get_system_settings('send_email_links');
                    $logo_url = WEB_PROTOCOL.PORTAL_SUBDOMAIN.DOMAIN."/";
                    $email_text_header = str_ireplace("{logo_url}",$logo_url,$email_header['set_value']);
                    $email_text_footer = str_ireplace("{logo_url}",$logo_url,$email_footer['set_value']);
                    $email_text_links = str_ireplace("{logo_url}",$logo_url,$email_links['set_value']);
                    // ******************************************************* //
                    if(IS_PRODUCTION)
                    {
                        //$this->email->message("<html><body><div>{$body}</div></body></html>");
//                    $this->email->send();
//                    @@settings::send_sms($customer_info['mobile'], "You topup transaction is being processed with BSD {$amount} to {$carrier_name} worth of phone credits to {$mobile_number}.");
                        if (strtolower($sms_settings['is_enable'])=="true")
                        {
                            $mobile = $mobile_number;
                            $sms_message = str_ireplace("{amount}",$amount,$sms_settings['set_value']);
                            $sms_message = str_ireplace("{carrier_name}",$carrier_name,$sms_message);
                            $sms_message = str_ireplace("{mobile_number}",$mobile_number,$sms_message);
                            @@settings::send_sms($mobile,strip_tags($sms_message));
                        }
                        if (strtolower($email_settings['is_enable'])=="true")
                        {
                            $date = date('m/d/Y h:i:s a', time());

                            $email_message = str_ireplace("{first_name}",$customer_info['first_name'],$email_settings['set_value']);
                            $email_message = str_ireplace("{mobile_network}",$carrier_name,$email_message);
                            $email_message = str_ireplace("{mobile_number}",$mobile_number,$email_message);
                            $email_message = str_ireplace("{trans_date}",$date,$email_message);
                            $email_message = str_ireplace("{amount}",$amount,$email_message);
                            $email_message = $email_text_header . $email_message . $email_text_footer;

                            $this->send_mail($email_message,$customer_info['email'],$email_settings['subject']);
                        }

                    }
                    return $this->success("Your mobile topup transaction is being processed.");
                }
            }else{
                return $this->failed("Transaction cannot be processed. Please try again.");
            }
        }
    }

    function get_carrier_list($params)
    {
        $session_id = $params['P01'];
        if (!$this->validate_session($session_id)) {
            return FALSE;
        } else {
            $this->extend_session($session_id);

            if($params['P02']!="BS")
            {
                $resp =  file_get_contents("api_files/".$params['P02'].$params['P03']."carrier_list.txt");
                return $this->success(unserialize($resp));
            }else{
                $resp =  array(
                    'carrierListResponse' => array(
                        'carriers' => array(
                            'carrier' => array (
                                'carrierId' => "00000",
                                'carrierName' => 'BTC',
                                'category' => 'Rtr',
                                'countryCode' => 'BS',
                                'currencyCode' => 'BSD',
                                'productId' => 'AlphaNumeric',
                                'operator' => 'BTC',
                                'mccCode' => '00000',
                                'mncCode' => '00000',
                                'denominationType' => 'Variable',
                            )
                        )
                    )
                );
                return $this->success($resp);
            }
        }
    }

    function get_sku_list_by_carrier($params)
    {
        $session_id = $params['P01'];
        if (!$this->validate_session($session_id)) {
            return FALSE;
        } else {
            $this->extend_session($session_id);

            if($params['P02']!="0000")
            {
                $resp =  file_get_contents("api_files/".$params['P02']."carrier_sku_list.txt");
                return $this->success(unserialize($resp));
            }else
            {
                $resp =  array(
                    'GetSkuListByCarrierResult' => array(
                        'skus' => array(
                            'sku' => array (
                                'skuId' => "00000",
                                'localPhoneNumberLength' => '0',
                                'internationalCodes' => '0',
                                'maxAmount' => '2000',
                                'minAmount' => '1',
                            )
                        )
                    )
                );
                return $this->success($resp);
            }
        }
    }

    function process_transactions()
    {
        $cron_transactions = $this->db->select('*')->where('status', 'PENDING')->order_by('transaction_type', 'desc')->get('merchant_cron_transactions');
        $cron_transactions = $cron_transactions->result_array();

        $values = array(
            'status' => "FORPROC",
        );
        $this->db->where('status', 'PENDING');
        $this->db->update('merchant_cron_transactions', $values);

        $transaction = array();

        foreach($cron_transactions as $row)
        {
            $transaction[] = $row['id'];
        }

        foreach($cron_transactions as $row)
        {

            if(in_array($row['id'],$transaction)){
                $params = unserialize($row['parameters']);
                $response ="";

                if($row['transaction_type']=="TOPUP")
                {
                    $this->process_topup_transaction_background($params);
                }

                $response = $this->response;
                if($response['ResponseId']=="0000")
                {
                    //success
                    $values = array(
                        'status' => "SUCCESS",
                        'execute_date' => date('Y-m-d H:i:s a'),
                        'response' => serialize($response),
                    );
                    $this->db->where('id', $row['id']);
                    $this->db->update('merchant_cron_transactions', $values);

                }else
                {
                    $this->_rollback_transaction_history($params, $row['transaction_type']);
                    //failed
                    $values = array(
                        'status' => "FAILED",
                        'execute_date' => date('Y-m-d H:i:s a'),
                        'response' => serialize($response),
                    );
                    $this->db->where('id', $row['id']);
                    $this->db->update('merchant_cron_transactions', $values);
                }
            }
        }

        print_r('Done Processing Merchant Transactions');
        die();
    }

    function delete_topup($params)
    {
        $session_id = $params['P01'];
        $customer_id = $params['P02'];
        $topup_id  = $params['P03'];
        if (!$this->validate_session($session_id)) {
            return FALSE;
        } else {
            $this->extend_session($session_id);
            $this->db->set('status', "D", TRUE);
            $this->db->where('id', $topup_id);
            if(isset($customer_id) && $customer_id != "")
            {
                $this->db->where('customer_id', $customer_id);
            }
            $this->db->update('customer_topup');

            if($this->db->affected_rows() > 0) {
                return $this->success('Topup Information has been deleted.');
            } else {
                return $this->failed('Topup Information cannot be deleted');
            }
        }
    }

    function register_topup($params)
    {
        $session_id = $params['P01'];
        $customer_id = $params['P02'];
        $carrier_id = $params['P03'];
        $carrier_name = $params['P04'];
        $country_id = $params['P05'];
        $recipient_name = $params['P06'];
        $mobile =$params['P07'];

        if($customer_id == "")
        {
           $customer_id = -1;
        }
        if (!$this->validate_session($session_id)) {
            return FALSE;
        } else {
            $this->extend_session($session_id);
            $values = array(
                "customer_id" => $customer_id,
                "recipient_name" => $recipient_name,
                "mobile_recipient" => $mobile,
                "carrier_id" => $carrier_id,
                "carrier_name" => $carrier_name,
                "country_id" =>  $country_id,
            );
            $this->db->set($values);
            if(!$this->db->insert('customer_topup'))
            {
                return $this->failed("Cannot register topup information.");
            }else
            {
                return  $this->success("Topup information has been registered.");
            }
        }
    }

    function get_topup_info($params)
    {
        $session_id = $params['P01'];
        $customer_id = $params['P02'];
        $customer_topup_id = $params['P03'];

        if($customer_id == "")
        {
            $customer_id = -1;
        }

        if (!$this->validate_session($session_id)) {
            return FALSE;
        } else {
            $this->extend_session($session_id);
            $query = $this->db
                ->select(array('c.id as customer_topup_id','mobile_recipient', 'recipient_name', 'carrier_name', 'carrier_id', 'ct.country_id', 'cts.code as country_code', 'cts.currency_code'))
                ->from('customer_topup c')
                ->join('country ct', 'c.country_id=ct.country_id', 'left')
                ->join('countries cts', 'ct.iso_code_2=cts.code', 'left')
                ->where(array('customer_id' => $customer_id, 'c.id' => $customer_topup_id ))
                ->get();
            $customer_topup = array();
            if ($query->num_rows() > 0) $customer_topup = $query->result_array();
            return $this->success($customer_topup);
        }
    }

    private function get_customer_info($customerid)
    {
        $customer = $this->db->select(array('c.*','e.card_prefix'))
            ->from('customers c')
            ->join('ezkard_accounts e', 'e.id=c.ezkard_account_id', 'left')
            ->where(array('c.id' => $customerid))
            ->get();
        $customer=$customer->result_array();
        return $customer[0];
    }

    private function _debit_credit_client_account_topup($card_id, $client_id, $transfer_amount, $description, $client_account_type, $debit_trans_type, $credit_trans_type, $refNo = "")
    {
        if (!($client_account_type == 'settlement' || $client_account_type == 'fee')) {
            return $this->failed('Invalid client account type');
        } else {
            $log_info_debit = array(
                'merchant_id'   => $client_id,
                'ezkard_id'     => $card_id,
                'terminal_id'   => '',
                'transaction_id' => $this->generate_transaction_id(),
                'amount'        => $transfer_amount,
                'trans_type_id' => $debit_trans_type,
                'description'   => $description,
                'reference_id'  => $refNo,
                'timestamp'     => date('Y-m-d H:i:s'),
                'trans_status_id' => 0,
            );
            $log_id = $this->_log_transaction($log_info_debit, 'ezkard_transactions');
            if (!$log_id) {
                return false;
            } else {
                $log_info_credit = array(
                    "client_record_id"  => $client_id,
                    "user_type_id"      => 2,
                    "ref_trans_id"      => $log_id,
                    "trans_type_id"     => $credit_trans_type,
                    "amount"            => $transfer_amount,
                    "description"       => $description,
                    "timestamp"         => date('Y-m-d H:i:s'),
                );
                $client_trans_id = $this->_log_transaction($log_info_credit, 'client_transactions');
                if (!$client_trans_id) {
                    return false;
                } else {
                    $account_types = array('prefund' => 0, 'settlement' => 1, 'fee' => 2);
                    $transaction_details = array(
                        'client_transaction_id' => $client_trans_id,
                        'client_account_type'   => $account_types[$client_account_type],
                        'client_record_id'      => $client_id,
                        'amount'                => $transfer_amount,
                    );
                    $details_id = $this->_log_transaction($transaction_details, 'client_transaction_details');
                    if (!$details_id) {
                        return false;
                    } else {
                        $response = array(
                            'TransactionID'      => $details_id,
                            'DebitTransID'      => $log_info_debit['transaction_id'],
                            'CreditTransID' =>  $client_trans_id,
                            'ResponseMessage'    => 'card transfer successful',
                        );
                        return $this->success($response);
                    }
                }
            }
        }
    }

    private function process_topup_transaction_background($params)
    {
        $session_id = $params['P01'];
        $customer_id = $params['P02'];
        $carrier_id = $params['P03'];
        $carrier_name = $params['P04'];
        $country_id = $params['P05'];
        $payment_type = $params['P06'];
        $sku_id = $params['P07'];
        $mobile_number = $params['P08'];
        $amount = $params['P09'];
        $merchant_id = $params['merchant_id'];

        $amount = str_replace(",", "", $amount);

        $customer_info = "";
        if($customer_id != '' && !empty($customer_id))
        {
            $customer_info = $this->get_customer_info($customer_id);
        }

        if($carrier_name!="BTC")
        {
            $sessionid = $this->get_session_from_topup(MOBILE_TOPUP_USER, MOBILE_TOPUP_PASSWORD);
            if($session_id=="")
            {
                $data = array(
                    'transaction_type' => 'TOPUP',
                    'P01' => $carrier_name,
                    'P02' => $mobile_number,
                    'P03' => date('Y-m-d H:i:s a'),
                    'P04' => $amount,
                    'P05' => $customer_info['first_name'],
                );

                //**** For sms/email notification via db configuration ****//
                $email_settings = $this->get_system_settings('send_mail_template_for_unsuccessful_topup');
                $sms_settings = $this->get_system_settings('send_sms_template_for_unsuccessful_topup');

                $email_header = $this->get_system_settings('send_email_header');
                $email_footer = $this->get_system_settings('send_email_footer');
                $email_links = $this->get_system_settings('send_email_links');
                $logo_url = WEB_PROTOCOL.PORTAL_SUBDOMAIN.DOMAIN."/";
                $email_text_header = str_ireplace("{logo_url}",$logo_url,$email_header['set_value']);
                $email_text_footer = str_ireplace("{logo_url}",$logo_url,$email_footer['set_value']);
                $email_text_links = str_ireplace("{logo_url}",$logo_url,$email_links['set_value']);
                // ******************************************************* //

                if(IS_PRODUCTION)
                {
                    if (strtolower($sms_settings['is_enable'])=="true")
                    {
                        $mobile = $customer_info['mobile'];
                        $sms_message = str_ireplace("{amount}",$amount,$sms_settings['set_value']);
                        $sms_message = str_ireplace("{carrier_name}",$carrier_name,$sms_message);
                        $sms_message = str_ireplace("{mobile_number}",$mobile_number,$sms_message);
                        @@settings::send_sms($mobile,strip_tags($sms_message));
                    }
                    if (strtolower($email_settings['is_enable'])=="true")
                    {
                        $date = date('m/d/Y h:i:s a', time());

                        $email_message = str_ireplace("{first_name}",$customer_info['first_name'],$email_settings['set_value']);
                        $email_message = str_ireplace("{mobile_network}",$carrier_name,$email_message);
                        $email_message = str_ireplace("{mobile_number}",$mobile_number,$email_message);
                        $email_message = str_ireplace("{trans_date}",$date,$email_message);
                        $email_message = str_ireplace("{amount}",$amount,$email_message);
                        $email_message = $email_text_header . $email_message . $email_text_footer;

                        $this->send_mail($email_message,$customer_info['email'],$email_settings['subject']);
                    }
                }
                //email and text customer for failed transaction

                $credit_merchant_params = array(
                    'function'              => 'add',
                    'transfer_amount'       => $amount,
                    'client_id'             => $merchant_id,
                    'trans_type'            => 4, //client prefund credit
                    'client_account_type'   => 'prefund',
                    'description'           => 'Mobile topup :' . $mobile_number,
                );
                $this->adjust_client_balance($credit_merchant_params);
                return $this->failed("Unable to get session from topup.");
                die();
            }

            $data = array(
                'method' => 'purchase_rtr',
                'P01' => $sessionid,
                'P02' => MOBILE_TOPUP_PROGRAMID,
                'P03' => MOBILE_TOPUP_PRODUCTID,
                'P04' => $amount, //amount
                'P05' => $mobile_number,  //mobile number
                'P06' => $sku_id,  //sku id
                'P07' => null, //correlation id  - optional
                'P08' => null, //sender mobile   - optional
                'P09' => null, //storeid - optional
                'P10' => -1, //merchant_id
                'P11' => -1, //customer_id
            );
            $resp = $this->get_curl_result($data);
            $this->end_session_from_topup($sessionid);

            if($resp['status']=="failed"){
                //email and text customer for failed transaction
                $data = array(
                    'transaction_type' => 'TOPUP',
                    'P01' => $carrier_name,
                    'P02' => $mobile_number,
                    'P03' => date('Y-m-d H:i:s a'),
                    'P04' => $amount,
                    'P05' => $customer_info['first_name'],
                );

                //**** For sms/email notification via db configuration ****//
                $email_settings = $this->get_system_settings('send_mail_template_for_unsuccessful_topup');
                $sms_settings = $this->get_system_settings('send_sms_template_for_unsuccessful_topup');

                $email_header = $this->get_system_settings('send_email_header');
                $email_footer = $this->get_system_settings('send_email_footer');
                $email_links = $this->get_system_settings('send_email_links');
                $logo_url = WEB_PROTOCOL.PORTAL_SUBDOMAIN.DOMAIN."/";
                $email_text_header = str_ireplace("{logo_url}",$logo_url,$email_header['set_value']);
                $email_text_footer = str_ireplace("{logo_url}",$logo_url,$email_footer['set_value']);
                $email_text_links = str_ireplace("{logo_url}",$logo_url,$email_links['set_value']);
                // ******************************************************* //

                if(IS_PRODUCTION)
                {
                    if (strtolower($sms_settings['is_enable'])=="true")
                    {
                        $mobile = $mobile_number;
                        $sms_message = str_ireplace("{amount}",$amount,$sms_settings['set_value']);
                        $sms_message = str_ireplace("{carrier_name}",$carrier_name,$sms_message);
                        $sms_message = str_ireplace("{mobile_number}",$mobile_number,$sms_message);
                        @@settings::send_sms($mobile,strip_tags($sms_message));
                    }
                    if (strtolower($email_settings['is_enable'])=="true")
                    {
                        $date = date('m/d/Y h:i:s a', time());

                        $email_message = str_ireplace("{first_name}",$customer_info['first_name'],$email_settings['set_value']);
                        $email_message = str_ireplace("{mobile_network}",$carrier_name,$email_message);
                        $email_message = str_ireplace("{mobile_number}",$mobile_number,$email_message);
                        $email_message = str_ireplace("{trans_date}",$date,$email_message);
                        $email_message = str_ireplace("{amount}",$amount,$email_message);
                        $email_message = $email_text_header . $email_message . $email_text_footer;

                        $this->send_mail($email_message,$customer_info['email'],$email_settings['subject']);
                    }
                }

                $credit_merchant_params = array(
                    'function'              => 'add',
                    'transfer_amount'       => $amount,
                    'client_id'             => $merchant_id,
                    'trans_type'            => 4, //client prefund credit
                    'client_account_type'   => 'prefund',
                    'description'           => 'Mobile topup :' . $mobile_number,
                );
                $this->adjust_client_balance($credit_merchant_params);
                return $this->failed($resp);
                die();
            }
            else{
                $this->check_prefund_balance($session_id);
                $balance = $this->get_response();
                $balance = $balance['ResponseMsg']['Balance'];

                $this->_update_reference_no($params, $resp['respmsg']['vaps_transaction_no']);
                //**** For sms/email notification via db configuration ****//
                $email_settings = $this->get_system_settings('send_mail_template_topup');
                $sms_settings = $this->get_system_settings('send_sms_template_topup');

                $email_header = $this->get_system_settings('send_email_header');
                $email_footer = $this->get_system_settings('send_email_footer');
                $email_links = $this->get_system_settings('send_email_links');
                $logo_url = WEB_PROTOCOL.PORTAL_SUBDOMAIN.DOMAIN."/";
                $email_text_header = str_ireplace("{logo_url}",$logo_url,$email_header['set_value']);
                $email_text_footer = str_ireplace("{logo_url}",$logo_url,$email_footer['set_value']);
                $email_text_links = str_ireplace("{logo_url}",$logo_url,$email_links['set_value']);
                // ******************************************************* //
                if(IS_PRODUCTION)
                {
                    if (strtolower($sms_settings['is_enable'])=="true")
                    {
                        $mobile = $mobile_number;
                        $sms_message = str_ireplace("{amount}",$amount,$sms_settings['set_value']);
                        $sms_message = str_ireplace("{trans_id}",$resp['respmsg']['vaps_transaction_no'],$sms_message);
                        $sms_message = str_ireplace("{mobile_number}",$mobile_number,$sms_message);
                        @@settings::send_sms($mobile,strip_tags($sms_message));
                    }
                    if (strtolower($email_settings['is_enable'])=="true")
                    {
                        $date = date('m/d/Y h:i:s a', time());

                        $email_message = str_ireplace("{first_name}",$customer_info['first_name'],$email_settings['set_value']);
                        $email_message = str_ireplace("{mobile_network}",$carrier_name,$email_message);
                        $email_message = str_ireplace("{mobile_number}",$mobile_number,$email_message);
                        $email_message = str_ireplace("{trans_date}",$date,$email_message);
                        $email_message = str_ireplace("{trans_id}",$resp['respmsg']['vaps_transaction_no'],$email_message);
                        $email_message = str_ireplace("{amount}",$amount,$email_message);
                        $email_message = str_ireplace("{balance}",$balance,$email_message);
                        $email_message = $email_text_header . $email_message . $email_text_footer;

                        $email_subject = str_ireplace("{amount}",$amount,$email_settings['subject']);
                        $email_subject = str_ireplace("{trans_id}",$resp['respmsg']['vaps_transaction_no'],$email_subject);
                        $this->send_mail($email_message,$customer_info['email'],$email_subject);
                    }
                }
                return $this->success($resp);

            }
        }
        else{ //For Emida transaction
            $emida_params = array(
                'method' => 'login',
                'P01' => EMIDA_USER,
                'P02' => EMIDA_PASSWORD,
            );
            $emida_session = $this->emida_get_curl_result($emida_params);

            if($emida_session['respcode']!='0000')
            {
                $data = array(
                    'transaction_type' => 'TOPUP',
                    'P01' => $carrier_name,
                    'P02' => $mobile_number,
                    'P03' => date('Y-m-d H:i:s a'),
                    'P04' => $amount,
                    'P05' => $customer_info['first_name'],
                );

                //**** For sms/email notification via db configuration ****//
                $email_settings = $this->get_system_settings('send_mail_template_for_unsuccessful_topup');
                $sms_settings = $this->get_system_settings('send_sms_template_for_unsuccessful_topup');

                $email_header = $this->get_system_settings('send_email_header');
                $email_footer = $this->get_system_settings('send_email_footer');
                $email_links = $this->get_system_settings('send_email_links');
                $logo_url = WEB_PROTOCOL.PORTAL_SUBDOMAIN.DOMAIN."/";
                $email_text_header = str_ireplace("{logo_url}",$logo_url,$email_header['set_value']);
                $email_text_footer = str_ireplace("{logo_url}",$logo_url,$email_footer['set_value']);
                $email_text_links = str_ireplace("{logo_url}",$logo_url,$email_links['set_value']);
                // ******************************************************* //
                if(IS_PRODUCTION)
                {
                    if (strtolower($sms_settings['is_enable'])=="true")
                    {
                        $mobile = $mobile_number;
                        $sms_message = str_ireplace("{amount}",$amount,$sms_settings['set_value']);
                        $sms_message = str_ireplace("{carrier_name}",$carrier_name,$sms_message);
                        $sms_message = str_ireplace("{mobile_number}",$mobile_number,$sms_message);
                        @@settings::send_sms($mobile,strip_tags($sms_message));
                    }
                    if (strtolower($email_settings['is_enable'])=="true")
                    {
                        $date = date('m/d/Y h:i:s a', time());

                        $email_message = str_ireplace("{first_name}",$customer_info['first_name'],$email_settings['set_value']);
                        $email_message = str_ireplace("{mobile_network}",$carrier_name,$email_message);
                        $email_message = str_ireplace("{mobile_number}",$mobile_number,$email_message);
                        $email_message = str_ireplace("{trans_date}",$date,$email_message);
                        $email_message = str_ireplace("{amount}",$amount,$email_message);
                        $email_message = $email_text_header . $email_message . $email_text_footer;

                        $this->send_mail($email_message,$customer_info['email'],$email_settings['subject']);
                    }
                }
                //email and text customer for failed transaction

                $credit_merchant_params = array(
                    'function'              => 'add',
                    'transfer_amount'       => $amount,
                    'client_id'             => $merchant_id,
                    'trans_type'            => 4, //client prefund credit
                    'client_account_type'   => 'prefund',
                    'description'           => 'Mobile topup :' . $mobile_number,
                );
                $this->adjust_client_balance($credit_merchant_params);
                return $this->failed($emida_session);
                die();
            }

            $session_emida_id = $emida_session['respmsg']['SessionID'];
            $emida_params = array(
                'method' => 'pin_dist_sale_emida',
                'P01' => $session_emida_id,
                'P02' => EMIDA_PROGRAM_ID,
                'P03' => EMIDA_MODULE_ID,
                'P04' => $amount,
                'P05' => $mobile_number,
                'P06' => EMIDA_PRODUCT_ID
            );
            $emida_response = $this->emida_get_curl_result($emida_params);
            if($emida_response['respcode']!='0000')
            {
                $data = array(
                    'transaction_type' => 'TOPUP',
                    'P01' => $carrier_name,
                    'P02' => $mobile_number,
                    'P03' => date('Y-m-d H:i:s a'),
                    'P04' => $amount,
                    'P05' => $customer_info['first_name'],
                );

                //**** For sms/email notification via db configuration ****//
                $email_settings = $this->get_system_settings('send_mail_template_for_unsuccessful_topup');
                $sms_settings = $this->get_system_settings('send_sms_template_for_unsuccessful_topup');

                $email_header = $this->get_system_settings('send_email_header');
                $email_footer = $this->get_system_settings('send_email_footer');
                $email_links = $this->get_system_settings('send_email_links');
                $logo_url = WEB_PROTOCOL.PORTAL_SUBDOMAIN.DOMAIN."/";
                $email_text_header = str_ireplace("{logo_url}",$logo_url,$email_header['set_value']);
                $email_text_footer = str_ireplace("{logo_url}",$logo_url,$email_footer['set_value']);
                $email_text_links = str_ireplace("{logo_url}",$logo_url,$email_links['set_value']);
                // ******************************************************* //

                if(IS_PRODUCTION)
                {
                    if (strtolower($sms_settings['is_enable'])=="true")
                    {
                        $mobile = $customer_info['mobile'];
                        $sms_message = str_ireplace("{amount}",$amount,$sms_settings['set_value']);
                        $sms_message = str_ireplace("{carrier_name}",$carrier_name,$sms_message);
                        $sms_message = str_ireplace("{mobile_number}",$mobile_number,$sms_message);
                        @@settings::send_sms($mobile,strip_tags($sms_message));
                    }
                    if (strtolower($email_settings['is_enable'])=="true")
                    {
                        $date = date('m/d/Y h:i:s a', time());

                        $email_message = str_ireplace("{first_name}",$customer_info['first_name'],$email_settings['set_value']);
                        $email_message = str_ireplace("{mobile_network}",$carrier_name,$email_message);
                        $email_message = str_ireplace("{mobile_number}",$mobile_number,$email_message);
                        $email_message = str_ireplace("{trans_date}",$date,$email_message);
                        $email_message = str_ireplace("{amount}",$amount,$email_message);
                        $email_message = $email_text_header . $email_message . $email_text_footer;

                        $this->send_mail($email_message,$customer_info['email'],$email_settings['subject']);
                    }
                }
                //email and text customer for failed transaction

                $credit_merchant_params = array(
                    'function'              => 'add',
                    'transfer_amount'       => $amount,
                    'client_id'             => $merchant_id,
                    'trans_type'            => 4, //client prefund credit
                    'client_account_type'   => 'prefund',
                    'description'           => 'Mobile topup :' . $mobile_number,
                );
                $this->adjust_client_balance($credit_merchant_params);
                return $this->failed($emida_response);
                die();
            }


            //proceed if succeed
            $this->_update_reference_no($params, $emida_response['respmsg']['invoice_no']);

            //**** For sms/email notification via db configuration ****//
            $this->check_prefund_balance($session_id);
            $balance = $this->get_response();
            $balance = $balance['ResponseMsg']['Balance'];

            $email_settings = $this->get_system_settings('send_mail_template_topup');
            $sms_settings = $this->get_system_settings('send_sms_template_topup');

            $email_header = $this->get_system_settings('send_email_header');
            $email_footer = $this->get_system_settings('send_email_footer');
            $email_links = $this->get_system_settings('send_email_links');
            $logo_url = WEB_PROTOCOL.PORTAL_SUBDOMAIN.DOMAIN."/";
            $email_text_header = str_ireplace("{logo_url}",$logo_url,$email_header['set_value']);
            $email_text_footer = str_ireplace("{logo_url}",$logo_url,$email_footer['set_value']);
            $email_text_links = str_ireplace("{logo_url}",$logo_url,$email_links['set_value']);
            // ******************************************************* //
            if(IS_PRODUCTION)
            {
                if (strtolower($sms_settings['is_enable'])=="true")
                {
                    $mobile = $mobile_number;
                    $sms_message = str_ireplace("{amount}",$amount,$sms_settings['set_value']);
                    $sms_message = str_ireplace("{trans_id}",$emida_response['respmsg']['invoice_no'],$sms_message);
                    $sms_message = str_ireplace("{balance}",$balance,$sms_message);
                    $sms_message = str_ireplace("{mobile_number}",$mobile_number,$sms_message);
                    @@settings::send_sms($mobile,strip_tags($sms_message));
                }
                if (strtolower($email_settings['is_enable'])=="true")
                {
                    $date = date('m/d/Y h:i:s a', time());

                    $email_message = str_ireplace("{first_name}",$customer_info['first_name'],$email_settings['set_value']);
                    $email_message = str_ireplace("{mobile_network}",$carrier_name,$email_message);
                    $email_message = str_ireplace("{mobile_number}",$mobile_number,$email_message);
                    $email_message = str_ireplace("{trans_date}",$date,$email_message);
                    $email_message = str_ireplace("{balance}",$balance,$email_message);
                    $email_message = str_ireplace("{trans_id}",$emida_response['respmsg']['invoice_no'],$email_message);
                    $email_message = str_ireplace("{amount}",$amount,$email_message);
                    $email_message = $email_text_header . $email_message . $email_text_footer;

                    $email_subject = str_ireplace("{amount}",$amount,$email_settings['subject']);
                    $email_subject = str_ireplace("{trans_id}",$emida_response['respmsg']['invoice_no'],$email_subject);
                    $this->send_mail($email_message,$customer_info['email'],$email_subject);
                }
            }
            return $this->success($emida_response);

        }
    }

    private function get_curl_result($object_array)
    {
        $req = curl_init(MOBILE_TOPUP_API);
        curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($req, CURLOPT_POST, true );
        curl_setopt($req, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($req, CURLOPT_POSTFIELDS, http_build_query($object_array));
        $respCode = curl_getinfo($req, CURLINFO_HTTP_CODE);
        $resp =curl_exec($req);
        $resp = json_decode($resp, true);
        curl_close($req);
        //return $resp['respmsg'];
        return $resp;
    }

    private function emida_get_curl_result($object_array)
    {
        $req = curl_init(EMIDA_URL);
        curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($req, CURLOPT_POST, true );
        curl_setopt($req, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($req, CURLOPT_POSTFIELDS, http_build_query($object_array));
        $respCode = curl_getinfo($req, CURLINFO_HTTP_CODE);
        $resp =curl_exec($req);
        $resp = json_decode($resp, true);
        curl_close($req);
        return $resp;
    }

    public function get_system_settings($set_code)
    {
        $query = $this->db->where('set_code', $set_code)->get('system_settings');
        if ($query->num_rows() > 0)
        {
            $result = $query->result_array();
            $result = $result[0];
        } else {
            $result = array();
        }
        return $result;

    }

    private function _update_reference_no($params, $reference_no)
    {
        $DebitTransID = $params['DebitTransID']; // transaction_id ezkard_transactions
        $values = array(
            'reference_id' => $reference_no
        );
        $this->db->where('transaction_id', $DebitTransID);
        $this->db->update('ezkard_transactions', $values);
    }

    private function _rollback_transaction_history($params, $transaction_type)
    {

        if(isset($params['DebitTransID']))
        {
            $DebitTransID = $params['DebitTransID']; // transaction_id ezkard_transactions
            $CreditTransID = $params['CreditTransID'];   // id client_transactions, client_transaction_id client_transaction_details
            $TransactionLogID = $params['TransactionLogID'];
            $this->db->where('transaction_id', $DebitTransID)->delete('ezkard_transactions'); //todo: what if failed;
            $this->db->where('id', $CreditTransID)->delete('client_transactions'); //todo: what if failed;
            $this->db->where('client_transaction_id', $CreditTransID)->delete('client_transaction_details'); //todo: what if failed;
            if($transaction_type =="TOPUP")
            {
                $this->db->where('id', $TransactionLogID)->delete('mobile_topup_transactions'); //todo: what if failed;
            }

        }
    }

    //PAYMASTER BILLPAY
    function paymaster_search_by_account_no_old($params)
    {
        $session_id = $params["P01"];
        $biller_id = $params["P02"];
        $account_no = $params["P03"];

        if (!$this->validate_session($session_id)) {
            return FALSE;
        } else {
            $this->extend_session($session_id);

            $search_param = array(
                "method"        => "search_by_account_no",
                "biller_id"     => $biller_id,
                "account_no"    => $account_no,
                "api_key"       => PAYMASTERBILLPAY_APIKEY
            );

            $paymaster_result = $this->paymaster_get_curl_result($search_param);

            if($paymaster_result["respcode"] == "0000")
            {
                return $this->success($paymaster_result["respmsg"]);
            }
            else
            {
                return $this->failed($paymaster_result["respmsg"]);
            }
        }
    }

    private function paymaster_search_by_account_no($params)
    {
        $biller_id = $params["biller_code"];
        $account_no = $params["account_number"];

        $search_param = array(
            "method"        => "search_by_account_no",
            "biller_id"     => $biller_id,
            "account_no"    => $account_no,
            "api_key"       => PAYMASTERBILLPAY_APIKEY
        );

        $paymaster_result = $this->paymaster_get_curl_result($search_param);

        if($paymaster_result["respcode"] != '0000'){
            return false;
        }else{

            $resp_msg = json_decode($paymaster_result["respmsg"]);
            if(isset($resp_msg->error))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }

    function paymaster_search_by_account_name($params)
    {
        $session_id = $params["P01"];
        $biller_id = $params["P02"];
        $account_name = $params["P03"];

        if (!$this->validate_session($session_id)) {
            return FALSE;
        } else {
            $this->extend_session($session_id);

            $search_param = array(
                "method"        => "search_by_account_name",
                "biller_id"     => $biller_id,
                "account_name"    => $account_name,
                "api_key"       => PAYMASTERBILLPAY_APIKEY
            );

            $paymaster_result = $this->paymaster_get_curl_result($search_param);

            if($paymaster_result["respcode"] == "0000")
            {
                return $this->success($paymaster_result["respmsg"]);
            }
            else
            {
                return $this->failed($paymaster_result["respmsg"]);
            }
        }
    }






    function paymaster_get_billers($params)
    {
        $billers_param = array(
            "method"        => "get_billers",
            "api_key"       => PAYMASTERBILLPAY_APIKEY
        );

        $paymaster_result = $this->paymaster_get_curl_result($billers_param);

        if($paymaster_result["respcode"] != '0000'){
            $data['success'] = false;
            $data['ResponseMsg'] = $paymaster_result['respmsg'];
        }else{

            $billers = json_decode($paymaster_result['respmsg']);
            $billers = $billers->billers;

            $biller = array();
            foreach($billers as $value)
            {
                $biller[] = array(
                    'id'            => $value->id,
                    'biller_code'   => $value->id,
                    // 'biller_name'   => str_replace("\\"," ",$value->name),
                    'biller_name'   => htmlspecialchars($value->name, ENT_XML1, 'UTF-8'),
                    'fee_amount'    => 0,
                    'mode'          => '0',
                    'trans_type'    => '' //$value->transtype
                );   
            }
            $data['success'] = true;
            $data['ResponseMsg'] = $biller;
        }
        return $data;
    }

    function paymaster_pay_bill_old($params)
    {
        $session_id = $params['P01'];
        $user_id= $params['P02'];
        $account_no = $params['P03'];
        $biller_tag = $params['P04'];
        $total_amount = $params['P05'];
        $fee = $params['P06'];
        $clerk = $params['P07'];
        $account_name = $params['P08'];
        $product = $params['P09'];
        $phone_number = $params['P10'];
        $payment_type = "";
        $payment_remarks = "";
        $country_code = "";
        $currency = "";
        $trans_type = "";

        if (isset($params['P11'])) { $payment_type = $params['P11']; }
        if (isset($params['P12'])) { $payment_remarks = $params['P12']; }

        if (isset($params['P13'])) { $country_code = $params['P13']; }
        if (isset($params['P14'])) { $currency = $params['P14']; }

        if (isset($params['P15']))  { $trans_type = $params['P15']; }

        //get merchant id and location by user_id
        $info = $this->get_merchant_terminal_user_details_by_id($user_id);
        //return $this->success($params);
        if (!(isset($info['id']))){
            return $this->failed("Invalid user id.");
        }
        $location = $info['location'];
        $merchant_id = $info['merchant_id'];
        $branch_id = $info['branch_id'];


        if(!is_numeric($total_amount))
        {
            return $this->failed("Unable to process billpay transaction. Please enter valid amount.");
        }

        if (!$this->validate_session($session_id)) {
            return FALSE;
        } else {
            $this->extend_session($session_id);

            //get merchant_id first from merchant_terminal_users then use it as user_reference in user_account to get its id
            $merchant_user_id = $this->get_user_id_by_terminal_username($clerk);
            $this->check_prefund_balance_for_billers($merchant_user_id);
            $balance = $this->response;
            $balance = $balance['ResponseMsg']['Balance'];

            if($balance<$total_amount)
            {
                return $this->failed("Your Account has insufficient balance.");
                die();
            }

            $param_data = array(
                'P01' => $session_id,
                'P02' => 'BILLPAY'
            );
            $this->_get_transaction_fees($param_data);
            $percentage = $this->response['ResponseMsg'];
            $percentage = $percentage[0]['percentage'];
            $amount_to_be_paid = $total_amount-$fee;
            $amount_to_be_paid = $amount_to_be_paid-($amount_to_be_paid * $percentage);

            $amount_to_be_paid = round($amount_to_be_paid, 2);


            //process paymaster paybill
            $billpay_params = array(
                'method' => 'pay_bill',
                'biller_id' => $biller_tag,
                'account_number' => $account_no,
                'payment_amount' => $amount_to_be_paid,
                'payment_currency' => $currency,
                'local_ref' => time(),
                'request_date' => date('Y-m-d H:i:s'),
                'trans_type' => $trans_type,
                'api_key' => PAYMASTERBILLPAY_APIKEY
            );

            $paymaster_result = $this->paymaster_get_curl_result($billpay_params);

            if($paymaster_result["respcode"] !="0000")
            {
                if (isset($paymaster_result["respmsg"])){
                    return $this->failed($paymaster_result["respmsg"]);
                } else {
                    return $this->failed("Transaction cannot be processed. Please try again.");
                }

            } else {

                if (!$this->_debit_credit_client_account($merchant_id, SUPPLIER_ID, $total_amount-$fee, 'Bill Pay ' . $biller_tag . ':' . $account_no, 'vas', 24, 25)) {
                    return false;
                } else {
                    $re = $paymaster_result["respmsg"];
                    $re = json_decode($re);

                    $settlement_info = $this->get_response();
                    $settlement_info = $settlement_info['ResponseMsg'];
                    $trans_type_id = 13;//pay bill

                    $debit_trans_id = $settlement_info['DebitTransID'];
                    $credit_trans_id = $settlement_info['CreditTransID'];

                    $this->_debit_credit_client_account($merchant_id, SUPPLIER_ID, $fee, 'Bill Pay fee', 'fee', 26, 27);
                    $settlement_info = $this->get_response();
                    $settlement_info = $settlement_info['ResponseMsg'];
                    $fee_trans_id = $settlement_info['DebitTransID'];
                    $fee_credit_trans_id = $settlement_info['CreditTransID'];

                    $billpay_info = array(
                        'merchant_id'           => $merchant_id,
                        'biller_code'           => $biller_tag,
                        'bill_account_no'       => $account_no,
                        'bill_amount'           => $total_amount-$fee,
                        'bill_fee_amount'            => $fee,
                        'product'    => $product,
                        'clerk'    => $clerk,
                        'location' => $location,
                        'bill_account_name' => $account_name,
                        'bill_account_phone' => $phone_number,
                        'transaction_id' => $re->bill->transaction_number,
                        'user_id' => $user_id,
                        'payment_type' => $payment_type,
                        'payment_remarks' => $payment_remarks,
                        //'mango_reference' => $mango_reference_no,
                        'mobile_assist_reference' => $re->bill->local_ref,
                        'branch_id' => $branch_id,
                    );
                    $log_id = $this->_log_transaction($billpay_info, 'paymaster_transactions');
                    $params['FeeTransID'] = $fee_trans_id;
                    $params['FeeCreditTransID'] = $fee_credit_trans_id;
                    $params['DebitTransID'] = $debit_trans_id;
                    $params['CreditTransID'] = $credit_trans_id;
                    $params['TransactionLogID'] = $log_id;
                }

                if($log_id>0)
                {
                    $data = array(
                        'transaction_type' => 'BILLPAY',
                        'P01' => $session_id,
                        'P02' => $total_amount,
                        'P03' => 'PAYMASTER Billspay',
                        'P04' => $re->bill->local_ref,
                        'P05' => $merchant_id
                    );
                    $this->debit_merchant_prefund_branch($data);
                    //$debit = $this->get_response();
                    //return $this->success($debit);
                    $response = array(
                        'message' => 'Your bill pay transaction is being processed.',
                        'transaction_id' => $re->bill->transaction_number,
                    );
                    //return $this->success('Your bill pay transaction is being processed.');
                    return $this->success($response);
                }else{
                    return $this->failed("Transaction cannot be processed. Please try again.");
                }
            }
        }
    }

    private function paymaster_get_curl_result($object_array)
    {

        $req = curl_init("http://dev.mysuncash.com/paymaster/api.php");
        curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($req, CURLOPT_POST, true );
        curl_setopt($req, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($req, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($req, CURLOPT_POSTFIELDS, http_build_query($object_array));
        curl_setopt($req, CURLOPT_CONNECTTIMEOUT ,0);

        $resp =curl_exec($req);
        $respCode = curl_getinfo($req, CURLINFO_HTTP_CODE);
        $resp = json_decode($resp, true);

        return $resp;

    }


     /* Merchant/Client Registration */
     protected function create_random_key() {
        $key = md5(uniqid(time(), true)) . md5(sha1(microtime(true)));
        return $key;
    }
    public function save_db_record($table, $insert_data) {
        if (is_array($insert_data) && count($insert_data) > 0) {
            $this->db->set($insert_data)->insert($table);
            if(! ($this->db->affected_rows() > 0)) {
                return $this->failed('Unable to save record to database');
            } else {
                $auto_id = $this->db->insert_id();
                return $auto_id;
            }
        }
    }
    private function send_mail_2($message, $email_address, $subject){
          //  include("mailer/class.phpmailer.php");
            $mail = new PHPMailer();
            // print_r($mail);
            // die();
            $mail->IsSMTP(); // telling the class to use SMTP
            //$mail->Timeout = 3600;   // 1 = errors and messages
        //              $mail->SMTPDebug  = 1;
                //1;  // enables SMTP debug information (for testing)
                                            // 2 = messages only
            $mail->SMTPAuth   = true;                  // enable SMTP authentication
            $mail->SMTPSecure = "ssl";                 // sets the prefix to the server

            $mail->Host       = settings::$email_recipients['smtp_host'];      // sets GMAIL as the SMTP server
            $mail->Port       = settings::$email_recipients['smtp_port'];                   // set the SMTP port for the GMAIL server
            $mail->SMTPSecure = 'tls'; // secure transfer enabled REQUIRED for GMail
            $mail->SMTPAutoTLS = false;
            $mail->Username   = settings::$email_recipients['smtp_user'];  // GMAIL username
            $mail->Password   = settings::$email_recipients['smtp_pass'];   

            $mail->From = settings::$email_recipients['from'];
            $mail->FromName = PROGRAM . ' Auto Mailer'; 
            $mail->addAddress($email_address);
            // $mail->addAddress($to2);

            $mail->WordWrap = 50;
            $mail->isHTML(true);
            $mail->Subject = $subject;
            $mail->Body    =  "<html><body><div>{$message}</div></body></html>";  
            $mail->Send();
    }

    private function client_availability($P1)
    {
            $values = array("clients.id AS client_record_id",
                            "client_id",
			                "user_name",
                            "client_prefund",
                            "client_settlement",
                            "client_status_id",
                            "client_status.status AS client_status");

            $this->db->select($values);
            $this->db->from("clients");
            $this->db->join("client_status", "client_status_id = client_status.id", "left");
            $this->db->where("suntag_shortcode", $P1);

            $query = $this->db->get();

            $this->response = "";
            $this->response["ServiceType"] = __FUNCTION__;

            if($query->num_rows() == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
   }
   public function suntag_shortcode_availability($P1)
    {
            $values = array("clients.id AS client_record_id",
                            "client_id",
                            "user_name",
                            "suntag_shortcode"
                          );

            $this->db->select($values);
            $this->db->from("clients");
            $this->db->join("client_status", "client_status_id = client_status.id", "left");
            $this->db->where("suntag_shortcode", $P1);

            $query = $this->db->get();

            $this->response = "";
            $this->response["ServiceType"] = __FUNCTION__;

            if($query->num_rows() == 0)
            {
             return $this->success('Sun Tag shortcode is available.');
            }
            else
            {
             return $this->failed("Sun Tag shortcode is already used.");
            }	
   }
  private function username_availability($R1)
   {
           $values = array("id",
                           "user_type_id",
                           "user_reference",
                           "user_name",
                           "password",
                           "user_status_id");
                        
           $this->db->select($values);
           $this->db->from("user_account");
           $this->db->where("user_name", $R1);
           $query = $this->db->get();

           $this->response = "";
           $this->response["ServiceType"] = __FUNCTION__;
           if($query->num_rows() == 0)
           {
             return true;
           }
           else
           {
               return false;
           }						 
   }
   private function email_availability($R1)
   {
           $values = array("id");
                        
           $this->db->select($values);
           $this->db->from("client_billpay_application");
           $this->db->where("business_email_address", $R1);
           $query = $this->db->get();

           $this->response = "";
           $this->response["ServiceType"] = __FUNCTION__;
           if($query->num_rows() == 0)
           {
             return true;
           }
           else
           {
               return false;
           }						 
   }
   public function verify_registration_charity($params){
    date_default_timezone_set('America/New_York');
    $email_address = $params['P01'];
    $verification_code =md5($params['P02']);
    
    $query_string = "select c.id,c.dba_name FROM clients c
    left join merchant_details md ON(c.id=md.client_record_id)
    where c.verification_code = '{$verification_code}' AND md.contactemail = '{$email_address}'";
    
    $query = $this->db->query($query_string);
    if (!($query->num_rows() > 0)) {
        return $this->failed("Please enter valid email and verification code.");
    }else{
        $row = $query->row();
        
        $update_data = array(
            'registration_status' => 'P',
            'verified_date'     =>date('Y-m-d H:i:s')
        );
        $this->db->set($update_data)
        ->where('id',$row->id)
        ->where('verification_code', $verification_code)
        ->update('clients');
        if (!($this->db->affected_rows() > 0)) {
            return $this->failed('Verification failed.');
        } else {
                //Welcome Email
                $welcome_emailmsg = "<p>Hi {$row->dba_name},</p><br>";
                $welcome_emailmsg .= "<p>Thank you for registering for SunCash Charity! We are processing your request,   and will send confirmation email once approval is complete.</p>";
                $welcome_emailmsg .= "<p><b>SunCash is <i>ALL YOU NEED!</i></b></p><br>";
                $welcome_emailmsg .= "<p>Thank you for using SunCash Charity services.</p><br>";
                $welcome_emailmsg .= "<p>All the Best!</p>";
                $welcome_emailmsg .= "<p>SunCash Charity Team</p>";
                $welcome_subject = "Welcome to Charity!";
                $this->send_mail_2($welcome_emailmsg,$email_address,$welcome_subject);
            return $this->success('Your account has been verified.');
        }
    }
}

   public function verify_registration($params){
        date_default_timezone_set('America/New_York');
        $email_address = $params['P01'];
        $verification_code =md5($params['P02']);
        
        $query_string = "select c.id,c.dba_name FROM clients c
        left join merchant_details md ON(c.id=md.client_record_id)
        where c.verification_code = '{$verification_code}' AND md.contactemail = '{$email_address}'";
        
        $query = $this->db->query($query_string);
        if (!($query->num_rows() > 0)) {
            return $this->failed("Please enter valid email and verification code.");
        }else{
            $row = $query->row();
            
            $update_data = array(
                'registration_status' => 'P',
                'verified_date'     =>date('Y-m-d H:i:s')
            );
            $this->db->set($update_data)
            ->where('id',$row->id)
            ->where('verification_code', $verification_code)
            ->update('clients');
            if (!($this->db->affected_rows() > 0)) {
                return $this->failed('Verification failed.');
            } else {
                    //Welcome Email
                    $welcome_emailmsg = "<p>Hi {$row->dba_name},</p><br>";
                    $welcome_emailmsg .= "<p>Thank you for registering for SunCash Business Bill Pay! We are processing your request,   and will send confirmation email once approval is complete.</p>";
                    $welcome_emailmsg .= "<p><b>SunCash is <i>ALL YOU NEED!</i></b></p><br>";
                    $welcome_emailmsg .= "<p>Thank you for using SunCash Business services.</p><br>";
                    $welcome_emailmsg .= "<p>All the Best!</p>";
                    $welcome_emailmsg .= "<p>SunCash Business Team</p>";
                    $welcome_subject = "Welcome to Business Bill Pay!";
                    $this->send_mail_2($welcome_emailmsg,$email_address,$welcome_subject);
                return $this->success('Your account has been verified.');
            }
        }
    }
    
    function register_merchant($params)
    {
        if (!(isset($params['P01'],$params['P02'],$params['P03'],$params['P04'],$params['P05'],$params['P06'],$params['P07'],$params['P08'],$params['P09'],
            $params['P10'],$params['P11'],$params['P12'],$params['P13'],$params['P14'],$params['P15'],$params['P16'],$params['P17']
        ))) {
            return $this->failed("Some of the required fields are missing.");
        }
        if (!$this->email_availability($params['P14'])) {
            return $this->failed("Email address already exist.");
        }  
        if (!$this->client_availability($params['P05'])) {
            return $this->failed("Sun Tag short code already exist.");
        } 
        if (!$this->username_availability($params['P01'])) {
            return $this->failed("Username already exist.");
        } 
       

    
        $user_name           =$params['P01'];
        $password            =md5($params['P02']);
        $dba_name            =isset($params['P03'])?$params['P03']:null;
        $merchant_key        =$this->create_random_key();
        $creation_date       =date('Y-m-d H:i:s');
        $client_status_id    =0; //Default Active       
        $sole_proprietorship       = isset($params['P04'])?$params['P04']:null;
        $suntag_shortcode          = isset($params['P05'])?$params['P05']:null;
        $service_categories        = isset($params['P06'])?$params['P06']:null; 
        $name_of_parent_company    = isset($params['P07'])?$params['P07']:null;
        $business_license_no       = isset($params['P08'])?$params['P08']:null;
        $business_shortcode        = isset($params['P09'])?$params['P09']:null;
        $company_address           = isset($params['P10'])?$params['P10']:null;
        $island                    = isset($params['P11'])?$params['P11']:null;
        $country                   = isset($params['P12'])?$params['P12']:null;
        $head_office_telephone_no1 = isset($params['P13'])?$params['P13']:null;
        $business_email_address    = isset($params['P14'])?$params['P14']:null;
        $primary_contact           = isset($params['P15'])?$params['P15']:null;
        $p_telephone_no            = isset($params['P16'])?$params['P16']:null;
        $p_email_address           = isset($params['P17'])?$params['P17']:null;
        $legal_name                =isset($params['P18'])?$params['P18']:'';

        $head_office_telephone_no2 = isset($params['P19'])?$params['P19']:null;
        $merchant_type             = isset($params['P20'])?$params['P20']:1;  //Default Merchant
        $business_website          = isset($params['P21'])?$params['P21']:null;
        $secondary_contact         = isset($params['P22'])?$params['P22']:null;
        $s_telephone_no            = isset($params['P23'])?$params['P23']:null;
        $s_email_address           = isset($params['P24'])?$params['P24']:null;
        $name_of_primary_guarantor = isset($params['P25'])?$params['P25']:null;
        $name_of_secondary_guarantor = isset($params['P26'])?$params['P26']:null;
 
        $verification_code = mt_rand(10001, 90001);
        $values = array(
            "client_id"             => $suntag_shortcode,
            "user_name"             => $user_name,
            "password"              => $password,
            "user_id_create"        => -1,
            "client_status_id"      => -1,
            "creation_date"         => $creation_date,
            "legal_name"            => $legal_name,
            "dba_name"              => $dba_name,
            "merchant_key"          => $merchant_key,
            "verification_code"     =>md5($verification_code),
            "merchant_type_id"      =>$merchant_type,
            "suntag_shortcode"      =>$suntag_shortcode
        );

            $this->db->set($values);
            if(!$this->db->insert('clients'))
            {
                return $this->failed("Cannot register merchant information.");
            }else
            {

                $client_id=$this->db->insert_id();
               $this->register_client_user($client_id,$user_name, $password,-1);
               $log_id = $this->admin_transaction_registration($client_id, "2", "0", "registered client account ".$business_shortcode,date('Y-m-d H:i:s'), -1);
               //$this->client_transaction_registration($client_id, "0",$log_id, "2", "0", "Your account has been registered",date('Y-m-d H:i:s'));
                
                $merchant_details = array(
                    'client_record_id'        => $client_id,
                    'contactemail'            => $business_email_address 
                );
                $client_billpay_application=array(
                    'client_id'                   =>$client_id,
                    'sole_proprietorship'         =>$sole_proprietorship,     
                    'name_of_parent_company'      =>$name_of_parent_company,   
                    'business_license_no'         =>$business_license_no,      
                    'business_shortcode'          =>$business_shortcode,       
                    'company_address'             =>$company_address,          
                    'island'                      =>$island,                   
                    'country'                     =>$country,                  
                    'head_office_telephone_no1'   =>$head_office_telephone_no1,
                    'head_office_telephone_no2'   =>$head_office_telephone_no2,
                    'business_email_address'      =>$business_email_address,  
                    'business_website'            =>$business_website,        
                    'primary_contact'             =>$primary_contact,         
                    'p_telephone_no'              =>$p_telephone_no,         
                    'p_email_address'             =>$p_email_address,            
                    'secondary_contact'           =>$secondary_contact,        
                    's_telephone_no'              =>$s_telephone_no,             
                    's_email_address'             =>$s_email_address,            
                    'name_of_primary_guarantor'   =>$name_of_primary_guarantor,
                    'name_of_secondary_guarantor' =>$name_of_secondary_guarantor,
                    'service_categories'          =>$service_categories
                );
                $record_id = $this->save_db_record('merchant_details', $merchant_details);
                $record_id = $this->save_db_record('client_billpay_application', $client_billpay_application);
              //  if (!isset($data['short_code'])) {
                //    $data['short_code'] = '';
                    $short_code_result = $this->save_short_code($client_id, '', '', 3);
                //}
                
                 //Email for Verification Code
                $email_message  ="<p>Hi {$dba_name},</p><br>";
                $email_message .= '<p>Your '.PROGRAM.' verification code is: ' .$verification_code. '</p>';
                $email_message .= "<p><b>SunCash is <i>ALL YOU NEED!</i></b></p><br>";
                $email_message .= "<p>Thank you for using SunCash Business services.</p><br>";
                $email_message .= "<p>All the Best!</p>";
                $email_message .= "<p>SunCash Business Team</p>";
                $email_subject = PROGRAM . ':Verification code';
                $this->send_mail_2($email_message,$business_email_address,$email_subject);
               // return  $this->success("Merchant information has been registered.");

                    return   $this->response = array(
                        'ResponseCode' => '0000',
                        //'ResponseMsg' => array('msg'=>'Merchant information has been registered','ClientID'=>$client_id)
                        'ResponseMsg' => 'Merchant information has been registered.'
                    );
                    
               

            }
        
    }



    function admin_transaction_registration($R1, $R2, $R3, $R4, $R5, $R6)
    {
        $values = array("client_record_id" => $R1,
                        "trans_type_id" =>$R2,
                        "amount" => $R3,
                        "description" => $R4,
                        "timestamp" => $R5,
                        "admin_user_id" => $R6);
        
        $this->db->set($values);
        $this->db->insert('admin_transactions');
        
        $this->response = "";
        $this->response['ServiceType'] = __FUNCTION__;
        
        if($this->db->affected_rows() > 0)
        {
            $this->response['ResponseId'] = "0000";
            $this->response['ResponseMsg'] = "registration successful";
            $this->response['ResponseData'] = array('TransactionId' => $this->db->insert_id());
        }
        else 
        {
            $this->response['ResponseId'] = "0001";
            $this->response['ResponseMsg'] = $this->db->_error_message();
        }

        $log_response = $this->get_response();
        
        if($log_response['ResponseId'] == "0000")
        {
            $transaction_id = $log_response['ResponseData']['TransactionId'];
        }
        
        return $transaction_id;
    }
    
    function register_client_user($R1, $R2, $R3, $R4)
	{
            date_default_timezone_set('America/New_York');
            
            $values = array("user_type_id" => "1",
                            "user_reference" => $R1,
                            "user_name" => $R2,
                            "password" => $R3,
                            "user_status_id" => "0",
                            "user_id_create" => $R4,
                            'require_pw_change' => 1,
                            'pw_expiration' => date('Y-m-d', strtotime('+90 days')),
                            "creation_date" => date('Y-m-d H:i:s'));

            $this->db->set($values);
            $this->db->insert('user_account');

            $this->response = "";
            $this->response['ServiceType'] = __FUNCTION__;

            if($this->db->affected_rows() > 0)
            {
                    $this->response['ResponseId'] = "0000";
                    $this->response['ResponseMsg'] = "registration successful";
                    $this->response['ResponseData'] = array('UserAccountId' => $this->db->insert_id());
            }
            else 
            {
                    $this->response['ResponseId'] = "0001";
                    $this->response['ResponseMsg'] = $this->db->_error_message();
            }					
    }
    
    

    function client_transaction_registration($R1, $R2, $R3, $R4, $R5, $R6, $R7)
    {
        $values = array("client_record_id" => $R1,
                        "user_type_id" =>$R2,
                        "ref_trans_id" => $R3,
                        "trans_type_id" => $R4,
                        "amount" => $R5,
                        "description" => $R6,
                        "timestamp" => $R7);
        
        $this->db->set($values);
        $this->db->insert('client_transactions');
        
        $this->response = "";
        $this->response['ServiceType'] = __FUNCTION__;
        
        if($this->db->affected_rows() > 0)
        {
            $this->response['ResponseId'] = "0000";
            $this->response['ResponseMsg'] = "registration successful";
            $this->response['ResponseData'] = array('TransactionId' => $this->db->insert_id());
        }
        else 
        {
            $this->response['ResponseId'] = "0001";
            $this->response['ResponseMsg'] = $this->db->_error_message();
        }
    }
    public function save_short_code($client_record_id, $short_code, $merchant_name, $entity_type) {
        $query = $this->db->where('client_record_id', $client_record_id);
        if (trim($short_code) != '' && $entity_type == 3) {//biller
            $query = $query->get('billers');
            if ($query->num_rows() > 0) {
                $result = $query->result_array();
                if (!($result[0]['biller_code'] != $short_code)) {
                    return false;
                } else {
                    $update_data = array(
                        'biller_code' => $short_code,
                        'biller_name' => $merchant_name,
                    );
                    $this->db->set($update_data)->where('client_record_id', $client_record_id)->update('billers');
                    $result = (bool)($this->db->affected_rows() > 0);
                }
            } else {
                $insert_data = array(
                    'client_record_id' => $client_record_id,
                    'biller_code' => $short_code,
                    'biller_name' => $merchant_name,
                    'fee_amount' => 0,
                );
                $this->db->set($insert_data)->insert('billers');
                $result = (bool)($this->db->affected_rows() > 0);
            }
            if ($this->db->where('client_record_id', $client_record_id)->get('charitable_institutions')->num_rows() > 0) {
                $this->db->where('client_record_id', $client_record_id)->delete('charitable_institutions');
            }
            return true;
        } elseif (trim($short_code) != '' && $entity_type == 4) { //charitable institutions
            $query = $query->get('charitable_institutions');
            if ($query->num_rows() > 0) {
                $result = $query->result_array();
                if (!($result[0]['charity_code'] != $short_code)) {
                    return false;
                } else {
                    $update_data = array(
                        'charity_code' => $short_code,
                        'charity_name' => $merchant_name,
                    );
                    $this->db->set($update_data)->where('client_record_id', $client_record_id)->update('charitable_institutions');
                    $result = (bool)($this->db->affected_rows() > 0);
                }
            } else {
                $insert_data = array(
                    'client_record_id' => $client_record_id,
                    'charity_code' => $short_code,
                    'charity_name' => $merchant_name,
                );
                $this->db->set($insert_data)->insert('charitable_institutions');
                $result = (bool)($this->db->affected_rows() > 0);
            }
            if ($this->db->where('client_record_id', $client_record_id)->get('billers')->num_rows() > 0) {
                $this->db->where('client_record_id', $client_record_id)->delete('billers');
            }
            return true;
        } else { //not biller/charitable inst, false
            if ($this->db->where('client_record_id', $client_record_id)->get('billers')->num_rows() > 0) {
                $this->db->where('client_record_id', $client_record_id)->delete('billers');
            }
            if ($this->db->where('client_record_id', $client_record_id)->get('charitable_institutions')->num_rows() > 0) {
                $this->db->where('client_record_id', $client_record_id)->delete('charitable_institutions');
            }
            return false;
        }
    }

    public function get_service_category_list(){
        $cmd = "select id,name from client_business_categories WHERE status='A'";
        $result = $this->db->query($cmd);
        if($result->num_rows() == 0){
            return $this->failed("No record found!");
        }else{
            return $this->success($result->result_array());
        }
        
       
    }
    public function get_merchant_type_list(){
        $cmd = "select id,name from merchant_type WHERE status='A'";
        $result = $this->db->query($cmd);
        if($result->num_rows() == 0){
            return $this->failed("No record found!");
        }else{
            return $this->success($result->result_array());
        }
        
       
    }
    public function get_business_type(){
        $cmd = "select id,name from client_sole_proprietorship WHERE status='A'";
        $result = $this->db->query($cmd);
        if($result->num_rows() == 0){
            return $this->failed("No record found!");
        }else{
            return $this->success($result->result_array());
        }
        
       
    }

    public function get_island()
    {
        $query = $this->db
        ->select(array('id','code', 'name'))
        ->from('island i')
        ->get();
        $island = array();
        if ($query->num_rows() > 0) $island = $query->result_array();
        return $this->success($island);
    }
    function get_countries() {
        $query = $this->db
            ->select(array('id','code', 'currency_code', 'name'))
            ->from('countries c')
            ->get();
        $countries = array();
        if ($query->num_rows() > 0) {$countries = $query->result_array();}
        return $this->success($countries);
    }

        //PP 2018/09/14 For Charity
        function register_charity($params)
        {
            if (!(isset($params['P01'],$params['P02'],$params['P03'],$params['P04'],$params['P05'],$params['P06'],$params['P07'],$params['P08'],$params['P09'],
                $params['P10'],$params['P11'],$params['P12'],$params['P13'],$params['P14'],$params['P15']
            ))) {
                return $this->failed("Some of the required fields are missing.");
            }
            if (!$this->client_availability($params['P09'])) {
                return $this->failed("Charity or Church ShortCode already exist.");
            } 
            if (!$this->username_availability($params['P01'])) {
                return $this->failed("Username already exist.");
            }   
    
        
            $user_name           =$params['P01'];
            $password            =md5($params['P02']);
            $dba_name            =isset($params['P03'])?$params['P03']:null;
            $merchant_key        =$this->create_random_key();
            $creation_date       =date('Y-m-d H:i:s');
            $client_status_id    =0; //Default Active       
            $sole_proprietorship       = isset($params['P04'])?$params['P04']:null;
            $suntag_shortcode          = isset($params['P05'])?$params['P05']:null;
            $name_of_parent_company    = isset($params['P06'])?$params['P06']:null;
            $business_shortcode        = isset($params['P07'])?$params['P07']:null;
            $company_address           = isset($params['P08'])?$params['P08']:null;
            $island                    = isset($params['P09'])?$params['P09']:null;
            $country                   = isset($params['P10'])?$params['P10']:null;
            $head_office_telephone_no1 = isset($params['P11'])?$params['P11']:null;
            $business_email_address    = isset($params['P12'])?$params['P12']:null;
            $primary_contact           = isset($params['P13'])?$params['P13']:null;
            $p_telephone_no            = isset($params['P14'])?$params['P14']:null;
            $p_email_address           = isset($params['P15'])?$params['P15']:null;
            $business_license_no       = isset($params['P16'])?$params['P16']:null;
            $legal_name                = isset($params['P17'])?$params['P17']:null;
            $head_office_telephone_no2 = isset($params['P18'])?$params['P18']:null;
            $merchant_type             = isset($params['P19'])?$params['P19']:1;  //Default Merchant
            $business_website          = isset($params['P20'])?$params['P20']:null;
            $secondary_contact         = isset($params['P21'])?$params['P21']:null;
            $s_telephone_no            = isset($params['P22'])?$params['P22']:null;
            $s_email_address           = isset($params['P23'])?$params['P23']:null;
            $name_of_primary_guarantor = isset($params['P24'])?$params['P24']:null;
            $name_of_secondary_guarantor = isset($params['P25'])?$params['P25']:null;
    
            $verification_code = mt_rand(10001, 90001);
            $values = array(
                "client_id"             => $suntag_shortcode,
                "user_name"             => $user_name,
                "password"              => $password,
                "user_id_create"        => -1,
                "creation_date"         => $creation_date,
                "legal_name"            => $legal_name,
                "dba_name"              => $dba_name,
                "merchant_key"          => $merchant_key,
                "verification_code"     =>md5($verification_code),
                "merchant_type_id"      =>$merchant_type,
                "suntag_shortcode"      =>$business_shortcode//$suntag_shortcode
            );
    
                $this->db->set($values);
                if(!$this->db->insert('clients'))
                {
                    return $this->failed("Cannot register merchant information.");
                }else
                {
    
                    $client_id=$this->db->insert_id();
                   $this->register_client_user($client_id,$user_name, $password,-1);
                   $log_id = $this->admin_transaction_registration($client_id, "2", "0", "registered client account ".$business_shortcode,date('Y-m-d H:i:s'), -1);
                   //$this->client_transaction_registration($client_id, "0",$log_id, "2", "0", "Your account has been registered",date('Y-m-d H:i:s'));
                    
                    $merchant_details = array(
                        'client_record_id'        => $client_id,
                        'contactemail'            => $business_email_address 
                    );
                    $client_billpay_application=array(
                        'client_id'                   =>$client_id,
                        'sole_proprietorship'         =>$sole_proprietorship,     
                        'name_of_parent_company'      =>$name_of_parent_company,   
                        'business_license_no'         =>$business_license_no,      
                        'business_shortcode'          =>$business_shortcode,       
                        'company_address'             =>$company_address,          
                        'island'                      =>$island,                   
                        'country'                     =>$country,                  
                        'head_office_telephone_no1'   =>$head_office_telephone_no1,
                        'head_office_telephone_no2'   =>$head_office_telephone_no2,
                        'business_email_address'      =>$business_email_address,  
                        'business_website'            =>$business_website,        
                        'primary_contact'             =>$primary_contact,         
                        'p_telephone_no'              =>$p_telephone_no,         
                        'p_email_address'             =>$p_email_address,            
                        'secondary_contact'           =>$secondary_contact,        
                        's_telephone_no'              =>$s_telephone_no,             
                        's_email_address'             =>$s_email_address,            
                        'name_of_primary_guarantor'   =>$name_of_primary_guarantor,
                        'name_of_secondary_guarantor' =>$name_of_secondary_guarantor,
                       
                    );
                    $record_id = $this->save_db_record('merchant_details', $merchant_details);
                    $record_id = $this->save_db_record('client_billpay_application', $client_billpay_application);
                  //  if (!isset($data['short_code'])) {
                    //    $data['short_code'] = '';
                        $short_code_result = $this->save_short_code($client_id, '', '', 3);
                    //}

                    // //Welcome Email
                    // $welcome_emailmsg = "<p>Thank you for registering for SunCash Charity! We are processing your request,   and will send confirmation email once approval is complete.</p>";
                    // $welcome_emailmsg .= "<p><b>SunCash is <i>ALL YOU NEED!</i></b></p>";
                    // $welcome_subject = "Welcome to Charity!";
                    // $this->send_mail_2($welcome_emailmsg,$business_email_address,$welcome_subject);

                     //Email for Verification Code
                $email_message  ="<p>Hi {$dba_name},</p><br>";
                $email_message .= '<p>Your '.PROGRAM.' verification code is: ' .$verification_code. '</p>';
                $email_message .= "<p><b>SunCash is <i>ALL YOU NEED!</i></b></p><br>";
                $email_message .= "<p>Thank you for using SunCash Charity services.</p><br>";
                $email_message .= "<p>All the Best!</p>";
                $email_message .= "<p>SunCash Charity Team</p>";

                $email_subject = PROGRAM . ':Verification code';
                $this->send_mail_2($email_message,$business_email_address,$email_subject);




                   
                    $email_message = 'Your '.PROGRAM.' verification code is: ' .$verification_code;
                    $email_subject = PROGRAM . ':Verification code';
                    $this->send_mail_2($email_message,$business_email_address,$email_subject);
                   // return  $this->success("Merchant information has been registered.");
    
                        return   $this->response = array(
                            'ResponseCode' => '0000',
                            //'ResponseMsg' => array('msg'=>'Merchant information has been registered','ClientID'=>$client_id)
                            'ResponseMsg' => 'Merchant information has been registered'
                        );
                        
                   
    
                }
            
        }

        public function merchant_universal_transaction_history($params){
            
            $values = array(
                "client_record_id"    => $params['client_record_id'],
                "system_services_code"=> $params['system_services_code'],
                "type"                => $params['type'],
                "source"              => $params['source'],
                "destination"         => $params['destination'],
                "description"         => $params['description'],
                "amount"              => isset($params['amount'])? $params['amount']: 0,
                "reference"           => isset($params['reference'])? $params['reference']: '',
            );
    
                $this->db->set($values);
                if(!$this->db->insert('merchant_transaction_history'))
                {
                    return FALSE;
                }else
                {
                    return TRUE;
                }
        } 

        public function get_merchant_transaction_history($params){
            $client_record_id=$params['P01'];
            $system_services_code=$params['P02'];
            $date_from=$params['P03'];
            $date_to=$params['P04'];
            if($system_services_code==''){
                $cmd = "select id,client_record_id,system_services_code,`type`,source,destination,description,DATE_FORMAT(TIMESTAMP,'%Y-%m-%d %h:%i:%s') AS 'timestamp',  amount,reference from merchant_transaction_history where  
                amount is not null and  client_record_id = {$client_record_id} and
                timestamp>='{$date_from}' and timestamp<=DATE_ADD('{$date_to}', INTERVAL 1 DAY) 
                order by timestamp desc";
            }else{
                $cmd = "select id,client_record_id,system_services_code,`type`,source,destination,description,DATE_FORMAT(TIMESTAMP,'%Y-%m-%d %h:%i:%s') AS 'timestamp',  amount,reference from merchant_transaction_history where  amount is not null and  system_services_code='{$system_services_code}' 
                 and  client_record_id = {$client_record_id}  and timestamp>='{$date_from}' and timestamp<=DATE_ADD('{$date_to}', INTERVAL 1 DAY) 
                 order by timestamp desc";
            }
            $result = $this->db->query($cmd);
            if($result->num_rows() == 0){
                return $this->failed("No record found!");
            }else{
                return $this->success($result->result_array());
            }
        }

        public function continue_pending_registration($params){
            $business_email_address=$params['P01'];
            $cmd = "SELECT c.suntag_shortcode,dba_name,cba.business_email_address,'Pending for Verification' AS registration_status FROM clients  c
            INNER JOIN `client_billpay_application` cba ON(cba.client_id=c.id)
            WHERE registration_status = 'F' AND client_status_id= -1 AND cba.business_email_address = '{$business_email_address}'";
             $result = $this->db->query($cmd);
             if($result->num_rows() == 0){
                 return $this->failed("No record found!");
             }else{
                 return $this->success($result->result_array());
             }
       } 

       public function resend_verification_code($params){
        $verification_code = mt_rand(10001, 90001);
        $email = $params['P01'];
        $query_string = "select c.id,c.dba_name FROM clients c
        left join merchant_details md ON(c.id=md.client_record_id)
        where c.registration_status = 'F' AND c.client_status_id= -1   and  md.contactemail = '{$email}'";
        
        
        $query = $this->db->query($query_string);
        if (!($query->num_rows() > 0)) {
            return $this->failed("Please enter valid email and verification code.");
        }else{
            $row = $query->row();
            
            $update_data = array(
                "verification_code"  =>md5($verification_code),
                'modification_date'  => date('Y-m-d H:i:s')
            );
            $this->db->set($update_data)
            ->where('id',$row->id)
            ->update('clients');
            if (!($this->db->affected_rows() > 0)) {
                return $this->failed('Unable to send verification code.'.$row->id);
            } else {
    
                //Email for Verification Code
                $email_message  ="<p>Hi {$row->dba_name},</p><br>";
                $email_message .= '<p>Your '.PROGRAM.' verification code is: ' .$verification_code. '</p>';
                $email_message .= "<p><b>SunCash is <i>ALL YOU NEED!</i></b></p><br>";
                $email_message .= "<p>Thank you for using SunCash Business services.</p><br>";
                $email_message .= "<p>All the Best!</p>";
                $email_message .= "<p>SunCash Business Team</p>";
    
                $email_subject = PROGRAM . ':Resend verification code';
                $this->send_mail_2($email_message,$email,$email_subject);
                 return $this->success('Verification code has been sent.');
                
    
            }
    
        }
    
       }
       //added by @h
       public function get_profile_via_suntag($Params)
        {
            //check auth
            //var_dump($Params);die;
            $query_auth=$this->verify_tokken($Params['P02']);
            if(!empty($query_auth)){
                $values = array("clients.id AS client_record_id",
                                "client_id",
                                "user_name",
                                "suntag_shortcode",
                                "dba_name",
                                "profile_pic",
                                "merchant_details.contactemail as email"
                              );

                $this->db->select($values);
                $this->db->from("clients");
                $this->db->join("client_status", "client_status_id = client_status.id", "left");
                $this->db->join("merchant_details", "merchant_details.client_record_id = clients.id", "left");
                $this->db->where("suntag_shortcode", $Params['P01']);

                $query = $this->db->get();

                $this->response = "";
                $this->response["ServiceType"] = __FUNCTION__;
                //delete auth before leaving module 
                
                $this->remove_tokken($Params['P02']);

                 if($query->num_rows() == 0){
                     return $this->failed("Sorry! We can't find that suncash.me profile.");
                 }else{
                     return $this->success($query->row_array());
                 }
            } else {
                return $this->failed("Authentication Failed.");
            }    
       }  
       public function get_temp_tokken($P1){

        if($P1=='$unC@sh'){
            $temp_auth = sha1(date('Y-m-d H:i:s').$P1.date('Y-m-d H:i:s'));
            $insert_data=[
                'temp_auth'=>$temp_auth,
            ];
            $this->db->set($insert_data);
            $this->db->insert('temp_tokken');

            return $this->success($insert_data);
        }  else {
            return $this->failed("Authentication Failed.");
        }  


       }     

       public function verify_tokken($tokken){
            $this->db->select("*");
            $this->db->from("temp_tokken");
            $this->db->where("temp_auth", $tokken);
            $query_auth = $this->db->get()->row_array();
            return $query_auth;       
       }

       public function remove_tokken($tokken){
            $this->db->where('temp_auth', $tokken);
            $this->db->delete('temp_tokken');        
       }

   
}
