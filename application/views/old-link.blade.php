<?php
//loading assets helper
defined('BASEPATH') OR exit('No direct script access allowed');
$assetHelper = new AssetHelper\AssetHelper();
$ci=&get_instance();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Save/Link Card</title>
	<link rel="stylesheet" href="{{base_url('assets_main/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/main.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/main_checkout.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/responsive.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/all.min.css')}}">
	<link rel="stylesheet" href="{{base_url('assets/plugins/global/global2.css')}}">
	<link href="https://unpkg.com/filepond/dist/filepond.css" rel="stylesheet">
	<link href="https://unpkg.com/filepond/dist/filepond.min.css" rel="stylesheet">
	<link href="https://unpkg.com/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.min.css" rel="stylesheet">	
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>assets/plugins/icon.ico">
	<style type="text/css" >
		.ïframecenpos{
			border:1px !important;
		}
		.layaoutPanelTokenCardForm .canvasbox {
		    margin-left: 10% !important;
		    margin-right:10% !important;
		    width: 100% !important;	

		}
		.Modern .canvasbox .row > span {
			/*webkit-font-smoothing: antialiased !important;*/
			webkit-font-smoothing: antialiased !important;
		    font-family: "Roboto", sans-serif !important;
		    font-weight: 400 !important;
		    font-size: 15px !important;
		    color: #292929 !important;
		    line-height: 1.5em !important;
		    margin-bottom: .5rem !important;
		}

		.Modern .canvasbox .row > input {
			/*webkit-font-smoothing: antialiased !important;*/
			webkit-font-smoothing: antialiased !important;
		    font-family: "Roboto", sans-serif !important;
		    font-weight: 400 !important;
		    font-size: 16px !important;
		    color: #292929 !important;
		    line-height: 1.5em !important;
		    margin-bottom: .5rem !important;
		}
	</style>

</head>
<body>
	<section class="" id="card_section">
		<div class="container  text-center">
			<div class="row d-flex justify-content-center align-items-center mb-5">
				<div class="user-card transaction-details" style="margin-top:1rem;margin-bottom:-34px !important;">
					<!-- <div class="header w-content" style="height:6rem !important;"></div> -->
					<div class="body">
						<form id="cashpayment_form" method="POST">
						<br>					
			        	<div id="card_info" style="border-top: 1px solid #F0F0F0;"><br>
						    <h3>Enter your card details</h3>
						    <span>We dont share your financial details with merchant.</span>
							<div>
							    
							    <form action="" class="text-left">
								<div id="NewCenposPlugin">
								</div>
								<div class="text-center"><button type="button" class="btn btn-orange full" id="submit">Save/Link Card</button></div><br>
								</form>
							</div>
						</div>
						<div id="success_section" style="display:none;">
							<hr>
					  		<div class="label text-center">Card Details</div>

						  	<div class="item">
							  <div class="row">
							  	<div class="col text-left text-details">Card Number: </div>
							  	<div class="col text-right"><span id="card_number"></span></div>
							  </div>
							</div>
							<div class="item">
							  <div class="row">
							  	<div class="col text-left text-details">Name on Card: </div>
							  	<div class="col text-right"><span id="name_on_card"></span></div>
							  </div>
							</div>
						  	<div class="item total">
							  <div class="row">
							  	<div class="col text-left text-details">Card Type: </div>
							  	<div class="col text-right amount"><span id="card_type"></span> </div>
							  </div>
						  	</div>

						  	<input type="hidden" name="tid" id ="tid" value="">
						  	<div class="payment_btn_section">
						  		<div class="text-center"><button type="button" class="btn btn-orange full" id="link_card">Save/Link Card</button></div>
						  	</div>
						  	<div class="cancel_section" style="display:none;">
						  		<div class="row">
				
						  			<div class="col text-center"><button type="button" class="btn btn-orange full" id="another_card">Use Another Card</button></div>						
						  		</div>	
						  	</div>
						</div>
						</form>
						<div class="modal fade" id="cardValidation" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">Credit Card Validation</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<form name="whitelist_form" id="whitelist_form" class="form-horizontal" method="POST" enctype="enctype/form-data">
									<div class="progress">
												<div class="progress-inner">
													<div class="progress-step active" data-step="1">
														<div class="number">1</div>
														<div class="label">Basic Information</div>
													</div>

													<div class="progress-step" data-step="2">
														<div class="number">2</div>
														<div class="label">Upload Documents <br>and Selfie</div>
													</div>

													<div class="progress-step" data-step="3">
														<div class="number">3</div>
														<div class="label">Confirmation</div>
													</div>
												</div>
											</div>

											<div class="progress-container">
												<div class="progress-content active" data-content="1">
													<div class="container px-4">
														<div class="row card-section-title align-items-center">
															<div class="col-12 col-lg-6 p-0">
																<span >Basic Information</span>
															</div>
														</div>

													<div class="form-group">
														<label for="cardNumber">Last 4 Digit Card Number:</label>
														<input type="text" class="form-control input-sm required" id="last4digits" name="last4digits"  readonly="" />
													</div>
													<div class="form-group two-input">
														<label for="type">Type:</label>
														<input type="text" class="form-control input-sm required" id="card_type_w" name="card_type_w" readonly=""   />
													</div>
													<div class="form-group">
														<label for="cardName">Name on Card:</label>
														<input type="text" class="form-control input-sm required" id="name_on_card" name="card_name" readonly=""  />
													</div>
													<div class="form-group">
														<label for="cardName">Gender:</label>
														<input type="text" class="form-control input-sm required" id="gender" name="gender" readonly=""  />
													</div>
													<div class="form-group">
														<label for="cardName">Address:</label>
														<input type="text" class="form-control input-sm required" id="address" name="address" readonly=""  />
													</div>
													<div class="form-group">
														<label for="cardName">City:</label>
														<input type="text" class="form-control input-sm required" id="city" name="city" readonly=""  />
													</div>
													<div class="form-group">
														<label for="cardName">Island:</label>
														<input type="text" class="form-control input-sm required" id="island" name="island" readonly=""  />
													</div>
													<div class="form-group">
														<label for="cardName">Country:</label>
														<input type="text" class="form-control input-sm required" id="country" name="country" readonly=""  />
													</div>													
													<div class="form-group">
														<label for="cardName">Birthday:</label>
														<input type="text" class="form-control input-sm required" id="card_name" name="card_name" readonly=""  />
													</div>
													<div class="form-group">
														<label for="cardID">* ID Number:</label>
														<input type="text" class="form-control input-sm required" id="card_id" name="card_id"   />
													</div>
													<div class="form-group">
														<label for="cardEmail">* Email:</label>
														<input type="email" class="form-control input-sm required" id="card_email" name="card_email"  />
													</div>
													<div class="form-group">
														<label for="cardMNumber">* Mobile Number:</label>
														<input type="text" class="form-control bfh-phone" id="card_mobile_number" name="card_mobile_number" data-format="1 (ddd) ddd-dddd" value ="1 242" >
													</div>
													</div>
												</div>
												<div class="progress-content" data-content="1">
													<div class="container px-4">
														<div class="row card-section-title align-items-center">
															<div class="col-12 col-lg-6 p-0">
																<span >Upload ID and Selfies</span>
															</div>
														</div>
													
														<div class="upload-container">
															<div class="form-group mb-5">
															<label>Upload a Scan of your ID</label>
															<div class="info"><i class="fas fa-exclamation-circle"></i> Please upload a Government issued ID such as  Passport, current Driver’s License, or National ID Card.</div>

															<div class="row">
																<div class="col-12 col-lg-6">
																		<input type="file" class="my-pond" name="filepond1" id="card_id_upload" accept="image/jpeg, image/png" />
																</div>
																<div class="col-12 col-lg-6">
																		<div class="illustration-image text-center">
																			<div>
																				<img src="{{base_url()}}assets_checkout/imgs/card-id.png" alt="">
																				<span class="d-block">Example</span>
																			</div>
																		</div>
																</div>
															</div>
														</div>
														<div class="form-group mb-5">
															<label>Upload a Scan of your Debit/Credit Card</label>
															<div class="info"><i class="fas fa-exclamation-circle"></i> Please upload the debit/credit card you used with a clear view of your last 4 digit card number, name and expiry date</div>
															<div class="row">
																<div class="col-12 col-lg-6">
																		<input type="file" class="my-pond" name="filepond2" id="credit_card_upload"/>
																</div>
																<div class="col-12 col-lg-6">
																		<div class="illustration-image text-center">
																			<div>
																				<img src="{{base_url()}}assets_checkout/imgs/debit-card.png" alt="">
																				<span class="d-block">Example</span>
																			</div>
																		</div>
																</div>
															</div>
														</div>
														<div class="form-group">
															<label>Upload a Credit Card with Card ID</label>
															<div class="info"><i class="fas fa-exclamation-circle"></i> Please upload a clear photo of you holding your credit/debit card.</div>

															<div class="row">
																<div class="col-12 col-lg-6">
																		<input type="file" class="my-pond" name="filepond3" id="cc_with_card_upload" accept="image/jpeg, image/png"/>
																</div>
																<div class="col-12 col-lg-6">
																		<div class="illustration-image text-center">
																			<div>
																				<img src="{{base_url()}}assets_checkout/imgs/user-hold-cards.png" alt="">
																				<span class="d-block">Example</span>
																			</div>
																		</div>
																</div>
															</div>
														</div>
														</div>
													</div>
												</div>
												<div class="progress-content success-screen" data-content="3">
													<div class="container px-4">
														<div class="row">
															<div class="col mt-2">
																<i class="far fa-thumbs-up"></i>
																<div class="validation-message">Validation submitted!</div>
																<center><button class="btn btn-primary" id="okay_btn"  type="button">OK</button></center>
															</div>

														</div>
													</div>
												</div>
											</div>

											<div class="container btn-container  py-5">
												<div class="row align-items-center">
													<div class="col-6 mb-1">
														<button type='button' class="btn btn-close" data-dismiss="modal">Close</button>
													</div>
													<div class="col-6 text-right">
														<input type="hidden" name="wid" id="wid" />
														<input type="hidden" name="merckey" id="merckey" value="{{$post_data['MerchantKey']}}"/>
														<button class="btn btn-primary btn-process" id="submit_btn"  type="button">Next</button>
													</div>
												</div>
											</div>
										</form>
							</div>
							</div>
						</div>
						</div>						
					</div>
				</div>				
			</div>
		</div>
	</section>



	<script src="{{base_url('assets_main/js/jquery-3.2.1.min.js')}}"></script>
	<script src="{{base_url('assets_main/js/bootstrap.min.js')}}"></script>
	{!!$assetHelper->link_plugins('js','bootstrap-3.3.7/js/bootstrap.min.js')!!}
	{!!$assetHelper->link_plugins('js','sidebar-nav/dist/sidebar-nav.min.js')!!}
	{!!$assetHelper->link_plugins('js','override/jquery.slimscroll.js')!!}
	{!!$assetHelper->link_plugins('js','override/waves.js')!!}
	{!!$assetHelper->link_plugins('js','override/custom.min.js')!!}
	{!!$assetHelper->link_plugins('js','sweetalert2-master/dist/sweetalert2.all.min.js')!!}
	{!!$assetHelper->link_plugins('js','bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js')!!}
	{!!$assetHelper->link_plugins('js','curioussolutions-datetimepicker/dist/DateTimePicker.min.js')!!}
    {!!$assetHelper->link_plugins('js','Inputmask-5.x/dist/jquery.inputmask.min.js')!!}
    {!!$assetHelper->link_plugins('js','bootstrap-formhelpers-phone/bootstrap-formhelpers-phone.js') !!}
    {!!$assetHelper->link_plugins('js','bootstrap-formhelpers-phone/bootstrap-formhelpers-phone.format.js') !!}
	{!!$assetHelper->link_plugins('js','dropzone-master/dist/dropzone.js')!!}
	{!!$assetHelper->link_plugins('js','global/global.js')!!}	
	<script src="https://www.cenpos.com/Plugins/porthole.min.js"></script>
	<script src="https://www.cenpos.com/Plugins/jquery.simplewebpay.js"></script> 
	<script src="https://unpkg.com/filepond/dist/filepond.min.js"></script>
	<script src="https://unpkg.com/jquery-filepond/filepond.jquery.js"></script>
	
	<script src="https://unpkg.com/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.min.js"></script>
	<script src="https://unpkg.com/filepond-plugin-image-exif-orientation/dist/filepond-plugin-image-exif-orientation.min.js"></script>
	<script src="https://unpkg.com/filepond-plugin-file-validate-type/dist/filepond-plugin-file-validate-type.js"></script>
	<script src="https://unpkg.com/filepond-plugin-file-encode/dist/filepond-plugin-file-encode.js"></script>	
	<script src="{{base_url('assets_main/js/main.js')}}"></script>
	<script  type="text/javascript" charset="utf-8">


	function CallbackSuccess(responseData) {
	alert(JSON.stringify(responseData));
	}
	function CallbackCancel(responseData) {
	alert(JSON.stringify(responseData));
	}

	$("#submit").click(function(){
		$("#NewCenposPlugin").submitAction();
	});

	$("#another_card").click(function(){
    	window.location.reload();        	
    });

	var merchant='{{CENPOST_MERCHANT_ID}}';
	var verify_params ='{{$verify_params["Data"]}}';
	$("#NewCenposPlugin").createWebpay({
	url: 'https://www.cenpos.net/simplewebpay/cards',
	params : "email=suncashme@gmail.com&verifyingpost="+verify_params+"&iscvv=true",
	height:'400px',
	// sessionToken:false,
	sessionToken:true,//for converycrypto
	isSameSite: 'Lax',

    beforeSend:function(xhr, textStatus) {
        //called when complete
	    loader.showPleaseWait();
        $("#submit").prop('disabled', true);
      },
    complete: function(xhr, textStatus) {
        //called when complete
        $("#submit").prop('disabled', true);
        $("#submit").text("please wait..");
        loader.hidePleaseWait();
      },
        success: function(data){
   
        	if(data.Result==0){


        	$("#card_number").text(data.ProtectedCardNumber);
        	$("#name_on_card").text(data.NameonCard);
        	$("#card_type").text(data.CardType);
        	$("#tid").val(data.RecurringSaleTokenId);
        	$("#success_section").show();
        	$("#card_info").hide();
        	} else {
        	$("#tid").val('');
        	swal(data.Message);	

        	}
        },
        cancel: function(response){
        	$("#tid").val('');
        	$("#submit").prop('disabled', false);
        	swal(response.Message);	
        	if(response.Message!="Error validation Captcha"){
        		swal(response.Message);	
        		$("#success_section").hide();
        		$("#card_info").show();
        	} 
        }	
	});

	$("#link_card").click(function(e){

		var form_data ={
		//merchant:merchant,	
		tokenid:$("#tid").val(),
		merchant_key:'{{$merchant_key}}',
		merchant_customer_id:'{{$merchant_customer_id}}',
		card_type:$("#card_type").text(),
		card_number:$("#card_number").text(),
		name_on_card:$("#name_on_card").text(),
		}   
		$.ajax({
		url: '{{base_url("checkout_card/link_card_process")}}',
		type: 'POST',
		dataType: 'json',
		data: form_data,
		beforeSend:function(){
			// loader2.showPleaseWait();
			$("#link_card").prop("disabled",true);
			$("#link_card").text("Processing... Please wait");
			loader.showPleaseWait();
		},
		complete: function(xhr, textStatus) {
			$("#link_card").prop("disabled",false);
			$("#link_card").text("");
			loader.hidePleaseWait();
		},
		success: function(data, textStatus, xhr) {
			//console.log();
			if(data.success){
					swal(data.msg);
					$("#success_section").hide();
					$("#cardValidation").show();
					// window.location.reload();
				// swal(response.Message);	
			} else {
				$("#link_card").prop("disabled",false);
				$("#link_card").text("Save/Link Card");
				loader.hidePleaseWait();
				swal(data.msg);
				$(".cancel_section").show();
				$(".payment_btn_section").hide();
				$("#tid").val('');
			}
		},
		error: function(xhr, textStatus, errorThrown) {
				$("#link_card").prop("disabled",false);
				$("#link_card").text("Save/Link Card");
				swal(data.msg);
				$(".cancel_section").show();
				$(".payment_btn_section").hide();
				$("#tid").val('');
		}
		});


});

	</script>

</body>
</html>