<?php
//loading assets helper
defined('BASEPATH') OR exit('No direct script access allowed');
$assetHelper = new AssetHelper\AssetHelper();
$ci = &get_instance();
//$ci->load->config('adminlte_themes')

//use
//[__3s18f_n]:__3s18f_h, when requesting ajax csrf token
?>
<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title">Verify Card</h4>
        <button type="button" class="close" data-dismiss="modal" form_id='' aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div><!-- /.modal-header -->
    <!-- <div class="content-wrapper"> -->

    <!-- <section class="content"> -->

      <form name="verify_form" id="verify_form" class="form-horizontal" method="POST" enctype="enctype/form-data">
      <div class="container-fluid">
        <div class="card-body">
        <p>
        This step will verify your card and confirm that the card is valid and that you are the card owner.  The name on card must match the SunCash account holder’s name.</p>

       <p>Send in a selfie (picture of yourself, showing your entire face) holding your card, covering the card number but showing the last 4 digits, the name and expiry date to kyc@mysuncash.com.   If you cannot make out the information on the card, please also send another photo of the card itself. Make sure the photo is clear.</p>

       <p>We will send you the credit card verification number once approved.</p>

      
        <img src="http://dev.suncash.me/customer/assets/imgs/card_verify_pic.png" width='90%'>
          <div class="col-12">
            <div class="message"><b>Verification Code</b></div>
            <input type="text" class="form-control" id="verification" name="verification" placeholder="Enter Code" required/>
          </div><!-- /.col -->

        </div>
      </div><!-- /.container-fluid -->

      <div class="modal-footer">
        <input type="hidden" id="c" name="c" value='{{$id}}'/>
        <input type="hidden" id="v" name="v" value='{{$v}}'/>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" name="submit" class="btn btn-primary submit">Process Transaction</button>
      </div>
      </form>
</div><!-- /.modal-content -->
<script>
$("#verify_form").submit(function(){
  $.ajax({
    type: "post",
    url: "{{base_url('customer/process_verify_card')}}",
    data: $(this).serializeArray(),
    dataType: "json",
    success: function (data) {
      if(data.success){
        swal(data.msg);
        window.location.reload();
      }else {
        swal(data.msg);
      }
    },
    error: function (data) {
      swal(data.msg);
    }
  });
  return false;
});
</script>
