<?php
//loading assets helper
defined('BASEPATH') OR exit('No direct script access allowed');
$assetHelper = new AssetHelper\AssetHelper();
$ci=&get_instance();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>SunCash.Me</title>
	<link rel="stylesheet" href="{{base_url('assets_main/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/main.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/main_checkout.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/responsive.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/all.min.css')}}">
	<link rel="stylesheet" href="{{base_url('assets/plugins/global/global2.css')}}">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>assets/plugins/icon.ico">
	<style type="text/css" >
		#preview{

		margin:0px auto;
 		 outline: 1px solid red;
		}
	</style>
</head>
<body>
	<nav class="navbar navbar-expand-lg sticky-top bg-white shadow-sm p-0">
	  <div class="container p-0" role="navigation">
	  	<a class="navbar-brand" href="<?php echo base_url();?>"><img src="{{base_url('assets_main/imgs/header-logo@2x.png')}}" alt="Suncash Logo" class="d-inline-block align-top" width="160"></a>
	  	<a href="#" class="menu-button"></a>
	  	<div class="justify-content-end main-menu">
				<ul class="navbar-nav d-flex align-items-center">


		    </ul>
		  </div>
	  </div>
	</nav>
	<section class="" id="sand_section">
		<div class="container  text-center">
			<div class="row d-flex justify-content-center align-items-center mb-5">
				<div class="user-card transaction-details" style="margin-top:1rem;margin-bottom:-34px !important;">
					<div class="header w-content" style="height:6rem !important;"></div>
					<div class="body">
						<form id="sanddollar_form" method="POST">
				        <div class="user-image ">
								@if(!empty($_SESSION['profile_pic']))
								<img src="{{$_SESSION['profile_pic']}}">
								@else
								<img src="{{base_url('assets_main/imgs/suncash-icon.png')}}">
								@endif
						</div>
						<div class="details" style="margin-bottom: -40px;">

							<div class="message">You're about to pay <?=ucfirst($_SESSION['suntag_shortcode']) ?></div>
							<select name="payment_method" id="payment_method" >
								<option value="">BSD</option>
							</select>
							<input class="input-amount" type="text" id="amount" name="amount" value="{{$payment_data['amount']}}" style="height: 100% !important"  readonly>
						</div>
						<div class="text-left">
							<div class="item">
								<input type="hidden" class="form-control" id="reference_num" name="reference_num"  value="{{$payment_data['reference_num']}}" placeholder="Reference # (Order, Quote, Invoice or Account)">
								<div class="form-group">
						          <input type="hidden" class="form-control" id="name" name="name" placeholder="Name" autocomplete="off" style="resize:none" value="{{$payment_data['name']}}" >
								</div>		
								<div class="form-group">
						          <input type="hidden" class="form-control" id="email" name="email" placeholder="Email" autocomplete="off" style="resize:none" value="{{$payment_data['email']}}" >
								</div>		
								<div class="form-group">
						          <input type="hidden" class="form-control" id="mobile" name="mobile" placeholder="mobile" autocomplete="off" style="resize:none" value="{{$payment_data['mobile']}}" >
								</div>																
							  	<input type="hidden"  class="form-control" id="notes" name="notes" placeholder="Note to Business" value="{{$payment_data['notes']}}">
							</div>
		
							<div class="item primary-border" >
								<div class="label text-center">Transaction Details:</div>
									<div class="item" style="margin-bottom: 0px !important;margin-top: 0px !important;">
										<div class="row text-details">
											<div class="col">Principal</div>
											<div class="col text-right ">$ <span class="amount_val">{{$payment_data['amount']}}</span></div>
										</div> 
									</div> 
									<div class="item" style="margin-bottom: 0px !important;margin-top: 0px !important;">
										<div class="row text-details">
										<div class="col">Transaction Fee</div>

										<div class="col text-right">$ <span class="fee_val">0.00</span></div>
										</div>
									</div>																						
								</div>						

								<div class="item total">
									<div class="row">
									<?php 
										$total = str_replace( ',', '', $payment_data['amount']);//+$fee_data['fee']+$fee_data['vat_charge'];
									?>
									<div class="col label">Total Due</div>
									<div class="col text-right amount"><span class="amount_total">{{$total}}</span> BSD</div>
									</div>
								</div>
								<br>

								How would you like to pay?
								<select class="form-control" name="type" id="type"  placeholder="">
									<option value="">-Select- </option>
									<option value="qr">Scan with SandDollar Enabled App </option>
									<option value="manual">Scan Receivers QRCode </option>
									<option value="custom_name">Custom Name</option>
								</select>

								<div class="qr" style="display:none">
									<!-- <h3> Scan QR </h3> -->

										<center>Scan with SandDollar Enabled App<br>

											<?php
												$custom_val='';
												if(isset($sand_data['amount'])){
													if($_SESSION['tag']=='CUSTOMER'){
														$qrcode = \qr::GenerateQr('nzia:qr/sinag+'.''.$sand_data['referenceid'].'@bs.nzia?amount='.$payment_data['amount'],'',300);
														$custom_val2 ="nzia:qr/sinag+".$sand_data['referenceid']."@bs.nzia?amount=".$payment_data['amount'];
														$custom_val ="suncash://suncash.me/".$sand_data['referenceid']."/".$payment_data['amount'];
														$custom_name ="sinag+".$sand_data['referenceid'];
													}elseif($_SESSION['tag']=='MERCHANT'){
														$qrcode = \qr::GenerateQr('nzia:qr/sinag+'.''.$sand_data['referenceid'].'@bs.nzia?amount='.$payment_data['amount'],'',300);
														$custom_val2="nzia:qr/sinag+".$sand_data['referenceid']."@bs.nzia?amount=".$payment_data['amount'];
														$custom_name ="sinag+".$sand_data['referenceid'];
														$custom_val ="suncash://suncash.me/".$sand_data['referenceid']."/".$payment_data['amount']; 
													}													
												} else {
													if($_SESSION['tag']=='CUSTOMER'){
														$qrcode = \qr::GenerateQr('nzia:qr/sinag+'.''.$sand_data['referenceid'].'@bs.nzia?amount='.$payment_data['amount'],'',300);
														$custom_val2 ="nzia:qr/sinag+".$sand_data['referenceid']."@bs.nzia?amount=".$payment_data['amount'];
														$custom_val ="suncash://suncash.me/".$sand_data['referenceid']."/".$payment_data['amount'];
														$custom_name ="sinag+".$sand_data['referenceid'];
													}elseif($_SESSION['tag']=='MERCHANT'){
														$qrcode = \qr::GenerateQr('nzia:qr/sinag+'.''.$sand_data['referenceid'].'@bs.nzia?amount='.$payment_data['amount'],'',300);
														$custom_val2="nzia:qr/sinag+".$sand_data['referenceid']."@bs.nzia?amount=".$payment_data['amount'];
														$custom_name ="sinag+".$sand_data['referenceid'];
														$custom_val ="suncash://suncash.me/".$sand_data['referenceid']."/".$payment_data['amount']; 
													}
												}

													$qrimage =' <img src="data:'.$qrcode->getContentType().';base64,'.$qrcode->generate().'" id="qrcode" />';
													echo $qrimage;
											
											?>
											<br>
											<br>
								
											@if($location =='app')
												<!-- <div class="input-group ">
													<input type="text" class="form-control  text_to_copy" id="custom_url" name="custom_url" placeholder="Enter your pin" autocomplete="off" value ="{{$custom_name}}" readonly>
													<div class="input-group-append form-div">
														<button type="button" class=" btn-sm btn-primary" id="copy_text" style="height: 48px !important;">
															<span class="fa fa-clipboard" id="copy" ></span>
														</button>							
													</div>
												</div>	 -->
													<div class="text-center"><button type="button" id="pay_custom"  name="pay_custom" class="btn btn-orange full">Pay</button></div><br>
													<center>  OR</center><br>
													<div class="input-group ">

														<input type="text" class="form-control  text_to_copy" id="custom_url" name="custom_url" placeholder="Enter your pin" autocomplete="off" value ="{{$custom_name}}" readonly>
														<div class="input-group-append form-div">
															<button type="button" class=" btn-sm btn-primary" id="copy_text1" style="height: 48px !important;">
																<span class="fa fa-clipboard" id="copy" ></span>
															</button>			
																			
														</div>
													</div>	
													Copy and paste custom name to sand Dollar Enabled App and manually enter the amount to pay



												<br>
											@else

												<center>  OR</center><br>
												<div class="input-group ">
													<input type="text" class="form-control  text_to_copy" id="custom_url" name="custom_url" placeholder="Enter your pin" autocomplete="off" value ="{{$custom_name}}" readonly>
													<div class="input-group-append form-div">
														<button type="button" class=" btn-sm btn-primary" id="copy_text" style="height: 48px !important;">
															<span class="fa fa-clipboard" id="copy" ></span>
														</button>							
													</div>
												</div>	
													Copy and paste custom name  to sand Dollar Enabled App and manually enter the amount to pay
											@endif
										
												

								
										</center>	
										<br>	
								</div>
								<div class="manual" style="display:none">				
									<div>
										<div class="qr_section" style="display:none">

										<div class="embed-responsive embed-responsive-4by3"> 
											<video  id="preview"  playsinline></video>
										</div> 
											<label for="sand_qr">Scan SandDollar Card QRCode</label>
											<div class="input-group ">
												<input type="text" class="form-control required" id="sand_qr" name="sand_qr" placeholder="" autocomplete="off"  required readonly>
												<div class="input-group-append form-div">
													<button type="button" class=" btn-sm btn-primary" id="clear_text1" onclick="clearFields()"  style="height: 48px !important;">
														<span class="fa fa-trash" id="copy" ></span>
													</button>							
												</div>
											</div>	
										</div>		
										<div class="custom_name_txt" style="display:none">
											<div class="form-group">
												<label for="sand_custom_name">Custom Name</label>
												<div class="form-div">
												<input type="text" class="form-control required" id="sand_custom_name" name="sand_custom_name" placeholder="Enter your Custom Name" autocomplete="off">
												</div>
											</div>	
										</div> 																			
										<div class="form-group">
											<label for="sand_otp">OTP</label>
											<div class="form-div">
											<input type="password" class="form-control required" id="sand_otp" name="sand_otp" placeholder="Enter your otp" autocomplete="off" maxlength="6" required>
											</div>
										</div>

										<div class="form-group">
											<label for="sand_pin">PIN</label>
											<div class="form-div">
											<input type="password" class="form-control required" id="sand_pin" name="sand_pin" placeholder="Enter your pin" autocomplete="off" maxlength="6"   required>
											</div>
										</div>
										<div class="text-center"><button type="submit" id="sand_process"  class="btn btn-orange full">Process Payment</button></div>
				
										<br>

									</div>
								</div>

								<div class="custom_name" style="display:none">		
									<div class="form-group">
										<div class="">
											<br>
											<center>								
												@if($location =='app')
													<div class="text-center"><button type="button" id="pay_custom1"  name="pay_custom1" class="btn btn-orange full">Pay</button></div><br>
													<center>  OR</center><br>
													<div class="input-group ">

														<input type="text" class="form-control  text_to_copy" id="custom_url1" name="custom_url1" placeholder="Enter your pin" autocomplete="off" value ="{{$custom_name}}" readonly>
														<div class="input-group-append form-div">
															<button type="button" class=" btn-sm btn-primary" id="copy_text1" style="height: 48px !important;">
																<span class="fa fa-clipboard" id="copy" ></span>
															</button>		
																	
														</div>
													</div>	
														Copy and paste custom name to sand Dollar Enabled App and manually enter the amount to pay		

												@else
												<div class="input-group ">

													<input type="text" class="form-control  text_to_copy" id="custom_url1" name="custom_url1" placeholder="Enter your pin" autocomplete="off" value ="{{$custom_name}}" readonly>
													<div class="input-group-append form-div">
														<button type="button" class=" btn-sm btn-primary" id="copy_text1" style="height: 48px !important;">
															<span class="fa fa-clipboard" id="copy" ></span>
														</button>							
													</div>
															Copy and paste custom name to sand Dollar Enabled App and manually enter the amount to pay
												</div>	
												@endif
											</center>

										</div>
	
									</div>

								</div>								
							</div>									
						</div>
						</form>
					</div>
				</div>				
			</div>
		</div>
		<footer class="py-3 footer">
			<div class="container">
				<div class="row">
					<div class="col-12 col-lg-6">
						<span class="txt-shit">© {{date('Y')}} SunCash.Me. All Rights Reserved.</span>
					</div>
					<div class="col-12 col-lg-6 ">
						<img class="img-x" src="{{base_url('assets_main/imgs/footer-logo.png')}}">
					</div>
				</div>
			</div>
		</footer>		
	</section>


	<section class="full" id="success_section_transaction" style=" display:none ;">
		<div class="container  text-center">
			<div class="row d-flex justify-content-center align-items-center mb-5">
				<div class="user-card success-card" style="margin-top:1rem;margin-bottom:60px !important;">
					<div class="header w-content" style="margin-top:1rem;margin-bottom:-34px !important;"></div>
					<div class="body">
						<div class="user-image">
							<img src="{{base_url('assets_main/imgs/success-icon.png')}}">
						</div>
						<div class="details" id = "transaction-details">
							<div class="message" style="font-size: 2rem;line-height: 2.1rem;width: 90%;margin: 0 auto;">You Paid <span class="amount_val" style="color: #FF8400;">${{$payment_data['amount']}}</span> to <?=ucfirst($_SESSION['suntag_shortcode'])?></div>
						</div>
						<!-- <form action="" class="text-left"> -->
						<div class="text-left">

					  	<div class="label text-center">Transaction Details</div>
						  <div class="item">
							  <div class="row">
				  				<div class="col">TransactionID : <span id="transaction_id"></span></div>
							  	<div class="col text-right">{{date("d M Y")}}</div>
							  </div>
							</div>
							<div class="item">
							  <div class="row">
							  	<div class="col">Payment Method</div>
							  	<div class="col text-right">SandDollar Payment via QR</div>
							  </div>
							</div>
							<div class="item">
							  <div class="row">
							  	<div class="col">Principal</div>
							  	<div class="col text-right">$ <span class="amount_val">{{$payment_data['amount']}}</span></div>
							  </div>
							</div>								
							<div class="item">
							  <div class="row">
							  	<div class="col">Transaction Fee</div>
							  	<div class="col text-right">$ <span class="fee_val">0.00</span></div>
								  <!-- {{number_format($fee_data['fee'], 2, '.', '')}} -->
							  </div>
							</div>														
						  <div class="item total">
							  <div class="row">
							  	<div class="col label">Total Due</div>
							  	<div class="col text-right amount"><span class="amount_total">{{$total}}</span> BSD</div>
							  	<input type="hidden" id="amount_total" name="amount_total" value="{{$total}}" />
								<input type="hidden" id="location" name="location" value="{{$location}}" />
							  	<!-- <input type="hidden" id="hid_fee" name="hid_fee" value="{{number_format($fee_data['fee'], 2, '.', '')}}" />
							  	<input type="hidden" id="hid_vat" name="hid_vat" value="{{$fee_data['vat_charge']}}" />
							  	<input type="hidden" id="hid_totalfee" name="hid_totalfee" value="0.00" />							  	 -->
							  </div>
						  </div>
						  
						</div>
						<!-- </form> -->

					</div>
				</div>
			</div>
		</div>
	</section>
	</div><!-- end -->


	<script src="//ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="{{base_url('assets_main/js/bootstrap.min.js')}}"></script>

	{!!$assetHelper->link_plugins('js','sidebar-nav/dist/sidebar-nav.min.js')!!}
	{!!$assetHelper->link_plugins('js','override/jquery.slimscroll.js')!!}
	{!!$assetHelper->link_plugins('js','override/waves.js')!!}
	{!!$assetHelper->link_plugins('js','override/custom.min.js')!!}
	{!!$assetHelper->link_plugins('js','sweetalert2-master/dist/sweetalert2.all.min.js')!!}
    {!!$assetHelper->link_plugins('js','Inputmask-5.x/dist/jquery.inputmask.min.js')!!}
    {!!$assetHelper->link_plugins('js','bootstrap-formhelpers-phone/bootstrap-formhelpers-phone.js') !!}
    {!!$assetHelper->link_plugins('js','bootstrap-formhelpers-phone/bootstrap-formhelpers-phone.format.js') !!}
	{!!$assetHelper->link_plugins('js','global/global.js')!!}	
	{!!$assetHelper->link_plugins('js','html5-qrcode.min.js')!!}
	<!-- {!!$assetHelper->link_plugins('js','qrcode-reader-master/dist/js/qrcode-reader.js')!!}								 -->
	<script src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>
	<script src="{{base_url('assets_main/js/main.js')}}"></script>
	<script  type="text/javascript" charset="utf-8">

	if(typeof(EventSource)!=="undefined"){
		//alert('s');
			var url = "{{base_url('payment/sd/status_check/').$sand_data['referenceid']}}";
			var source=new EventSource(url);
			source.onmessage=function(event){

				var dt = $.parseJSON(event.data);
				console.log(dt.status);
				if(dt.status=='processed'){			
					//window.location.href=event.data;
				
					$("#success_section_transaction").show();
					$("#sand_section").hide();
					$("#transaction_id").text(dt.cron_ref_id);
				}
				//document.getElementById("result").innerHTML += event.data + "<br>";
			};

	} else {
			document.getElementById("result").innerHTML="Sorry, your browser does not support server-sent events...";
	}
	  let scanner = new Instascan.Scanner({ video: document.getElementById('preview') });

    	// var tempVideo = document.getElementsByTagName("video")[0];
    	// tempVideo.width=scanner;
    	// tempVideo.height='100%';

	  scanner.addListener('scan', function (content) {



		$("#sand_qr").val(content);
	  });

	function copyToClipboard($input) {
			$input.focus();
			$input.select();
			try {  
				var successful = document.execCommand('copy');
		alert("text copied");  
			} catch(err) {  
				console.error('Unable to copy'); 
		alert("cant copied");
			}	

	}	
	$(document).ready(function(){

		if($("#location").val()=='web'){
			$(".qr").show();
			$("#type").val("qr").trigger("change");		
		}else{			
			$(".custom_name").show();	
			$("#type").val("custom_name").trigger("change");					
		}

    
		$("#copy_text").click(function(){
			copyToClipboard($(".text_to_copy"));
		});

		    
		$("#copy_text1").click(function(){
			copyToClipboard($(".text_to_copy"));
		});
	});
	$("#amount").change(function(){

		if($("#amount").val()<=0.00){
		  	swal("Amount is required.");

		  	return false;
		}

		$(".amount_total").text($("#amount").val());
		$("#amount_total").text($("#amount").val());
		$(".amount_val").text($("#amount").val());

		var currentURL=$(".user-link").text().split('?')[0];
		var rewrite_url = currentURL+"?amount="+$(this).val();


		let finalURL =
		'https://chart.googleapis.com/chart?cht=qr&chl=' +
		htmlEncode(rewrite_url) +
		'&chs=300x300&chld=L|0'
  
        // Replace the src of the image with
        // the QR code image
        $('#qrcode').attr('src', finalURL);
		$(".user-link").text(rewrite_url);

			
		var currentURL=$("#custom_url").val().split('?')[0];
		var rewrite_url = currentURL+"?amount="+$(this).val();
		$("#custom_url").val(rewrite_url);


		var currentURL1=$("#custom_url1").val().split('?')[0];
		var rewrite_url = currentURL1+"?amount="+$(this).val();
		$("#custom_url1").val(rewrite_url);



	});
	$("#type").change(function(){


		if($(this).val()=='qr'){
			 scanner.stop();
			$("#preview").html('');
			$(".qr").show();
			$(".qr_section").hide()
			$(".manual").hide();
			$(".custom_name").hide();
		}else if($(this).val()=='manual'){	
		  Instascan.Camera.getCameras().then(function (cameras) {
			if (cameras.length > 0) {
			 scanner.start(cameras[0]);
			} else {
			 console.error('No cameras found.');
			}
		  }).catch(function (e) {
			console.error(e);
		  });

			$(".qr").hide();
			$(".qr_section").show()			
			$(".manual").show();
			$(".custom_name").hide();
			$(".custom_name_txt").hide();
		}else if($(this).val()=='custom_name'){
			 scanner.stop();
			$(".qr").hide();
			$(".qr_section").hide()			
			$(".manual").hide();
			$(".custom_name_txt").hide();
			$(".custom_name").show();			
		}else{
		scanner.stop();
		$(".qr").show();
		$(".qr_section").hide()
		$(".manual").hide();
		$(".custom_name").hide();		
		}

		
	});	
	$("#sanddollar_form").submit(function(){
	   var form_data ={
		mobile:$("#mobile").val(),
		passcode:$("#sand_otp").val(),
		pin:$("#sand_pin").val(),
		custom_name:$("#sand_custom_name").val(),
		notes:$("#notes").val(),
		sand_qr:$("#sand_qr").val(),
		// reference_num:$("#reference_num").val(),
		name:$("#name").val(),
    	email:$("#email").val(),
		amount:$("#amount").val(),
		total_amount:$("#amount_total").val(),
		type:$("#type").val()
		}   
		$.ajax({
		url: '{{base_url("payment/process_sanddollar_payment")}}',
		type: 'POST',
		dataType: 'json',
		data: form_data,
		beforeSend:function(){
			loader.showPleaseWait();
			$("#sand_process").prop("disabled",true);
			$("#sand_process").text("Processing... Please wait");
			//loader.showPleaseWait();
		},
		complete: function(xhr, textStatus) {
			$("#sand_process").prop("disabled",false);
			$("#sand_process").text("Process Payment");
			//loader.hidePleaseWait();
		},
		success: function(response, textStatus, xhr) {
			//console.log();
			if(response.success){
				console.table(response.msg.data[0].paymentRequestId);
				loader.hidePleaseWait();
				$("#sand_process").prop("disabled",false);
				$("#sand_process").text("Process Payment");		
				$("#transaction_id").text(response.msg.data[0].paymentRequestId);
				$("#sand_section").hide();	
				$("#success_section_transaction").show();
						
			} else {
				loader.hidePleaseWait();
				$("#sand_process").prop("disabled",false);	   
				$("#sand_process").text("Process Payment");
              	swal(response.msg.type);
			}
		},
			error: function(xhr, textStatus, errorThrown) {
				console.table(xhr);
				$("#sand_process").prop("disabled",false);	   
				$("#sand_process").text("Process Payment");

				loader.hidePleaseWait();
				swal(xhr.msg);
			}
		});
	return false;
	});
	// var html5QrcodeScanner = new Html5QrcodeScanner(
	// 	"reader", { fps: 10, qrbox: 250 });
	// html5QrcodeScanner.render(onScanSuccess, onScanError);
	// function onScanSuccess(qrCodeMessage) {
	// 	// handle on success condition with the decoded message;
	// 	$("#sand_qr").val(qrCodeMessage);
	
	// 	html5QrcodeScanner.clear();

	// 	html5QrcodeScanner = new Html5QrcodeScanner(
	// 	"reader", { fps: 10, qrbox: 250 });
	// 	html5QrcodeScanner.render(onScanSuccess, onScanError);
	// 	//   html5QrcodeScanner.render(onScanSuccess);
	// }

	// function onScanError(errorMessage) {
	// 	// handle on error condition, with error message
	// 		//alert(errorMessage);
	// 	//html5QrcodeScanner.render(onScanSuccess, onScanError);  
	// }



	$("#pay_custom").click(function(){
		setTimeout(function() {
			window.location.replace("{{$custom_val2}}");
				// This is a fallback if the app is not installed.
				// It could direct to an app store or a website
				// telling user how to get the app
		}, 25);
		// window.location = "custom-uri://AppShouldListenForThis";
		window.location.replace("{{$custom_val}}");

	});
	$("#pay_custom1").click(function(){
		setTimeout(function() {
			window.location.replace("{{$custom_val2}}");
				// This is a fallback if the app is not installed.
				// It could direct to an app store or a website
				// telling user how to get the app
		}, 25);
		// window.location = "custom-uri://AppShouldListenForThis";
		window.location.replace("{{$custom_val}}");
	});
	function clearFields() {
		document.getElementById("sand_qr").value="";
		//html5QrcodeScanner.clear();

	}

	</script>

</body>
</html>