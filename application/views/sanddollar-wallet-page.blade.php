<?php
//loading assets helper
defined('BASEPATH') OR exit('No direct script access allowed');
$assetHelper = new AssetHelper\AssetHelper();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>SunCash.Me</title>
	<link rel="stylesheet" href="{{base_url('assets_main/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/main.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/responsive.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/all.min.css')}}">
	<link rel="stylesheet" href="{{base_url('assets/plugins/global/global2.css')}}">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>assets/plugins/icon.ico">
	<style type="text/css">
		@media only screen and (max-width: 768px)  {
				.img-x {
				  display: block;
				  margin-left: auto;
				  margin-right: auto;
				  //width: 100%;
				  min-width:128px;
				  min-height:43px;
				}
				.txt-shit{
					text-align:center !important;
				}
		}		
	</style>	
</head>
<body>
	<nav class="navbar navbar-expand-lg sticky-top bg-white shadow-sm p-0">
	  <div class="container p-0" role="navigation">
	  	<a class="navbar-brand" href="{{base_url('payment/wallet')}}"><img src="{{base_url('assets_main/imgs/header-logo@2x.png')}}" alt="Suncash Logo" class="d-inline-block align-top" width="160"></a>
	  	<a href="#" class="menu-button"></a>
	  	<div class="justify-content-end main-menu">
				<ul class="navbar-nav d-flex align-items-center">

					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle main-nav" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<span><?=ucwords(($_SESSION['sand_customer_info']['CustomerName'])) ?></span>
								@if(!empty($suntag_data['profile_pic']))
								<img src="<?=($_SESSION['sand_customer_info']['ImageURL']) ?>" alt="profile image" class="profile-image">
								
								@else
								<img src="{{base_url('assets/img/media/td.png')}}"" alt="profile image" class="profile-image">
								@endif
						</a>
					</li>

		      <li class="nav-item btn-logout">
		        <a class="nav-link" href="{{base_url('sanddollar/logout')}}">Logout</a>
		      </li>
		    </ul>
		  </div>
	  </div>
	</nav>
	<section class="" id="sand_section">
		<div class="container  text-center">
			<div class="row d-flex justify-content-center align-items-center mb-5">
				<div class="user-card transaction-details" style="margin-top:1rem;margin-bottom:-34px !important;">
					<div class="header w-content" style="height:6rem !important;"></div>
					<div class="body">
						<form id="sanddollar_form" method="POST">
				        <div class="user-image ">
								@if(!empty($_SESSION['profile_pic']))
								<img src="{{$_SESSION['profile_pic']}}">
								@else
								<img src="{{base_url('assets_main/imgs/suncash-icon.png')}}">
								@endif
						</div>
						<div class="details" style="margin-bottom: -40px;">

							<div class="message">You're about to pay <?=ucfirst($_SESSION['suntag_shortcode']) ?></div>
							<select name="payment_method" id="payment_method" >
								<option value="">BSD</option>
							</select>
							<input class="input-amount" type="text" id="amount" name="amount" value="{{$payment_data['amount']}}" style="height: 100% !important">
						</div>
						<div class="text-left">
							<div class="item">
								<input type="hidden" class="form-control" id="reference_num" name="reference_num"  value="{{$payment_data['reference_num']}}" placeholder="Reference # (Order, Quote, Invoice or Account)">
								<div class="form-group">
						          <input type="hidden" class="form-control" id="name" name="name" placeholder="Name" autocomplete="off" style="resize:none" value="{{$payment_data['name']}}" >
								</div>		
								<div class="form-group">
						          <input type="hidden" class="form-control" id="email" name="email" placeholder="Email" autocomplete="off" style="resize:none" value="{{$payment_data['email']}}" >
								</div>		
								<div class="form-group">
						          <input type="hidden" class="form-control" id="mobile" name="mobile" placeholder="mobile" autocomplete="off" style="resize:none" value="{{$payment_data['mobile']}}" >
								</div>																
							  	<input type=""  class="form-control" id="notes" name="notes" placeholder="Note to Business" value="{{$payment_data['notes']}}">
							</div>
							<!-- <div class="item">
							    <div class="label">You are paying with:</div>
							    <div class="paying">SunCash Account (<span class="text-primary f-medium"><?=number_format($Balance,2, '.', '') ?> BSD</span>)
							  </div>
							</div>							 -->
								<div class="item primary-border" >
								  	<div class="label text-center">Transaction Details:</div>
										<div class="item" style="margin-bottom: 0px !important;margin-top: 0px !important;">
										  <div class="row text-details">
											  	<div class="col">Principal</div>
											  	<div class="col text-right ">$ <span class="amount_val">{{$payment_data['amount']}}</span></div>
										  </div> 
										</div> 
										<!-- <div class="item" style="margin-bottom: 0px !important;margin-top: 0px !important;">
										  <div class="row text-details">
										  	<div class="col">Transaction Fee</div>

										    <div class="col text-right">$ <span class="fee_val">{{number_format($fee_data['fee'], 2, '.', '')}}</span></div>
										  </div>
										</div>																						 -->
									</div>						

									<div class="item total">
									  <div class="row">
									  	<?php 
									  		$total = str_replace( ',', '', $payment_data['amount']);
									  	?>
									  	<div class="col label">Total Due</div>
									  	<div class="col text-right amount"><span class="amount_total">{{$total}}</span> BSD</div>
									  </div>
									</div>
									<div id="card_info"></div>

									@if(!empty($sandcard))
										<H4>Linked Sand dollar Card</H4><br>
										<div class="form-group">
											<label for="mobile">Device Number</label>
											<div class="form-div">
												<input type="text" class="form-control" id="card_number" name="card_number" value="{{$sandcard['device_no']}}" readonly="" >
											</div>
										</div>
										<div class="form-group">
											<label for="mobile">Mobile Number</label>
											<div class="form-div">
												<input type="text" class="form-control" id="card_number" name="card_number" value="<?=($_SESSION['sand_customer_info']['MobileNumber']) ?>" readonly="" >
											</div>
										</div>

										<div>
										<!-- <div class="" id="type" type="hidden">Type:
											<select class="form-control s2" name="transtype" id="transtype" class="required">
												<option value="SEND">--SELECT--</option>
												<option value="SEND">SEND</option>
												<option value="RECIEVE">RECEIVE</option>
											</select>
										</div>			 -->
										<input type="hidden" class="form-control" id="qr_code" name="qr_code" placeholder="qr_code" autocomplete="off" style="resize:none" value="{{$sandcard['qr_code']}}" >
										<input type="hidden" class="form-control" id="card_pin" name="card_pin" placeholder="card_pin" autocomplete="off" style="resize:none" value="{{$sandcard['pin']}}" >
										<input type="hidden" class="form-control" id="auth_secret" name="auth_secret" placeholder="mobile" autocomplete="off" style="resize:none" value="{{$sandcard['auth_secret']}}" >
										<input type="hidden" class="form-control" id="custom_name" name="custom_name" placeholder="custom_name" autocomplete="off" style="resize:none" value="{{$sandcard['custom_name']}}" >
										<input type="hidden" class="form-control" id="device_id" name="device_id" placeholder="mobile" autocomplete="off" style="resize:none" value="{{$sandcard['device_id']}}" >
										<input type="hidden" class="form-control" id="device_no" name="device_no" placeholder="device_no" autocomplete="off" style="resize:none" value="{{$sandcard['device_no']}}" >
										<input type="hidden" class="form-control" id="can_setup_customname" name="can_setup_customname" placeholder="can_setup_customname" autocomplete="off" style="resize:none" value="{{$sandcard['can_setup_customname']}}" >										
										<div class="text-center"><button type="submit" id="sanddollar_usecard_process"  class="btn btn-orange full">Process Payment</button></div>									
									@else	
										<!-- <h3>Enter your card details</h3>
										<div>
											<div class="form-group">
												<label for="mobile">Card Number</label>
												<div class="form-div">
													<input type="text" class="form-control" id="card_number" name="card_number" placeholder="Enter your card number" autocomplete="off" required >
												</div>
											</div>
											<div class="form-group">
												<label for="pin">PIN</label>
												<div class="form-div">
												<input type="password" class="form-control required" id="pin" name="pin" placeholder="Enter your PIN" autocomplete="off" required>
												</div>
											</div>
											<div class="form-group">
												<label for="pin_voucher">OTP</label>
												<div class="form-div">
												<input type="number" class="form-control required" id="otp" name="otp" placeholder="Entetr your OTP" autocomplete="off" maxlength="5" required>
												</div>
											</div>
											<div class="text-center"><button type="submit" id="link_sandcard"  class="btn btn-orange full">Link Card</button></div>
											<br>
										</div> -->

										<p style="color:red">*Must link sand dollar card in customer app to use this feature.</p>
									@endif	
								</div>							
							</div>


						</form>
					</div>
				</div>				
			</div>
		</div>
		<footer class="py-3 footer">
			<div class="container">
				<div class="row">
					<div class="col-12 col-lg-6">
						<span class="txt-shit">© {{date('Y')}} SunCash.Me. All Rights Reserved.</span>
					</div>
					<div class="col-12 col-lg-6 ">
						<img class="img-x" src="{{base_url('assets_main/imgs/footer-logo.png')}}">
					</div>
				</div>
			</div>
		</footer>		
	</section>

	<section class="full" id="link_success_section" style=" display:none ;">
		<div class="container  text-center">
			<div class="row d-flex justify-content-center align-items-center mb-5">
				<div class="user-card success-card" style="margin-top:1rem;margin-bottom:60px !important;">
					<div class="header w-content" style="margin-top:1rem;margin-bottom:-34px !important;"></div>
					<div class="body">
						<div class="user-image">
							<img src="{{base_url('assets_main/imgs/success-icon.png')}}">
						</div>
					  	<div class="label text-center">Card Successfully linked</div>
						  <div class="item">
							  <div class="row">
							  	<div class="col">Device ID: <span id="linked_code"></span></div>
							  	<div class="col text-right">{{date("d M Y")}}</div>
							  </div>
							</div>  
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="full" id="success_section_transaction" style="display:none ;">
		<div class="container  text-center">
			<div class="row d-flex justify-content-center align-items-center mb-5">
				<div class="user-card success-card" style="margin-top:1rem;margin-bottom:60px !important;">
					<div class="header w-content" style="margin-top:1rem;margin-bottom:-34px !important;"></div>
					<div class="body">
						<div class="user-image">
							<img src="{{base_url('assets_main/imgs/success-icon.png')}}">
						</div>
						<div class="details" id = "transaction-details">
							<div class="message" style="font-size: 2rem;line-height: 2.1rem;width: 90%;margin: 0 auto;">You Paid <span class="amount_val" style="color: #FF8400;">${{$payment_data['amount']}}</span> to <?=ucfirst($_SESSION['suntag_shortcode'])?></div>
						</div>
						<div class="text-left">

					  	<div class="label text-center">Transaction Details</div>
						  <div class="item">
							  <div class="row">
							  	<div class="col">TransactionID : <span id="transaction_id"></span></div>
							  	<div class="col text-right">{{date("d M Y")}}</div>
							  </div>
							</div>
							<div class="item">
							  <div class="row">
							  	<div class="col">Payment Method</div>
							  	<div class="col text-right">Sanddollar Payment</div>
							  </div>
							</div>													 
						  <div class="item total">
							  <div class="row">
							  	<div class="col label">Total Due</div>
							  	<div class="col text-right amount"><span class="amount_total">{{$total}}</span> BSD</div>
							  	<input type="hidden" id="amount_total" name="amount_total" value="{{$total}}" />
							  	<input type="hidden" id="hid_fee" name="hid_fee" value="{{number_format($fee_data['fee'], 2, '.', '')}}" />
							  	<input type="hidden" id="hid_vat" name="hid_vat" value="{{$fee_data['vat_charge']}}" />
							  	<input type="hidden" id="hid_totalfee" name="hid_totalfee" value="0.00" />	
								<input type="hidden" id="customerid" name="customerid" value="{{$_SESSION['sand_customer_info']['CustomerId']}}" />							  	
							  </div>
						  </div>
						  
						</div>
						<!-- </form> -->

					</div>
				</div>
			</div>
		</div>
	</section>
	</div><!-- end -->


	<script src="{{base_url('assets_main/js/jquery-3.2.1.min.js')}}"></script>
	<script src="{{base_url('assets_main/js/bootstrap.min.js')}}"></script>

	{!!$assetHelper->link_plugins('js','sidebar-nav/dist/sidebar-nav.min.js')!!}
	{!!$assetHelper->link_plugins('js','override/jquery.slimscroll.js')!!}
	{!!$assetHelper->link_plugins('js','override/waves.js')!!}
	{!!$assetHelper->link_plugins('js','override/custom.min.js')!!}
	{!!$assetHelper->link_plugins('js','sweetalert2-master/dist/sweetalert2.all.min.js')!!}
    {!!$assetHelper->link_plugins('js','Inputmask-5.x/dist/jquery.inputmask.min.js')!!}
    {!!$assetHelper->link_plugins('js','bootstrap-formhelpers-phone/bootstrap-formhelpers-phone.js') !!}
    {!!$assetHelper->link_plugins('js','bootstrap-formhelpers-phone/bootstrap-formhelpers-phone.format.js') !!}
	{!!$assetHelper->link_plugins('js','global/global.js')!!}	
	<script src="{{base_url('assets_main/js/main.js')}}"></script>
	<script  type="text/javascript" charset="utf-8">


	function getcfee(amount){
		//e.preventDefault();
		//e.stopImmediatePropagation();				
	    // accordion is open
	    var total_billpay_fees = parseFloat($("#amount").val().replace(/,/g, ''))+parseFloat($("#hid_fee").val())+parseFloat($("#hid_vat").val());
	    //alert($("#amount").val()+"+"+$("#hid_fee").val()+"+"+$("#hid_vat").val());
	    $.ajax({
	      url: '{{base_url("payment/get_fees_payment")}}',
	      type: 'POST',
	      dataType: 'json',
	      data:{amount:total_billpay_fees,client_id:'{{$_SESSION["client_record_id"]}}'},
	   	  beforeSend:function(xhr, textStatus) {
	        //called when complete
		    //loader.showPleaseWait();
	        $("#submit").prop('disabled', true);
	      },		  
		  complete: function(xhr, textStatus) {
		    //called when complete
		  	//loader.hidePleaseWait();
		  },
	      success: function(data, textStatus, xhr) {
	        //called when successful
	        //paste values
	        if(data.success){
	        	$(".conviniecefee_val").text(data.fee);
	        	//$("#amount").text(data.total);
				var total= total_billpay_fees+parseFloat(data.fee);
				$(".amount_total").text(total.toFixed(2));
				$("#amount_total").val(total.toFixed(2));
				$("#hid_pfee").val(data.pf);
				$("#hid_tfee").val(data.tf);
				$("#hid_totalfee").val(data.fee);

		    	$("#submit").prop('disabled', false);
	        }  else {
	        	swal(data.msg);
		    	$("#submit").prop('disabled', false);
	        }

	      },
	      error: function(xhr, textStatus, errorThrown) {
	        //called when there is an error
        	swal(data.msg);
	    	$("#submit").prop('disabled', false);
	      }
	    });
	}

	$("#amount").change(function(){

		if($("#amount").val()<=0.00){
		  	swal("Amount is required.");

		  	return false;
		}

		$.ajax({
		  url: '<?=site_url("payment/process_fee")?>',
		  type: 'POST',
		  dataType: 'json',
		  // data: {amount: $("#amount").val()},
		  data: {amount: parseFloat($("#amount").val().replace(/,/g, ''))},
	   	  beforeSend:function(xhr, textStatus) {
	        //called when complete
		    loader.showPleaseWait();
	        $("#submit").prop('disabled', true);
	      },		  
		  complete: function(xhr, textStatus) {
		    //called when complete
		  loader.hidePleaseWait();
		  },
		  success: function(data, textStatus, xhr) {
		    if(data.success){
		    	//console.log(data.fee_data.fee);
		    	$(".amount_val").text($("#amount").val());
				$(".fee_val").text(data.fee_data.fee);
				$(".vat_val").text(data.fee_data.vat_charge);
				// var total = parseFloat($("#amount").val())+parseFloat(data.fee_data.fee)+parseFloat(data.fee_data.vat_charge);
				var total = parseFloat($("#amount").val().replace(/,/g, ''))+parseFloat(data.fee_data.fee)+parseFloat(data.fee_data.vat_charge);
				$(".amount_total").text(total.toFixed(2));
				$("#amount_total").val(total.toFixed(2));
				$("#hid_fee").val(data.fee_data.fee);
				$("#hid_vat").val(data.fee_data.vat_charge);
				// getcfee($("#amount").val());
				@if($_SESSION['tag']=='MERCHANT')
				getcfee($("#amount").val());
				@endif
				@if($_SESSION['tag']=='CUSTOMER')
				getcfee_customer($("#amount").val());
				@endif
				$("#submit").prop('disabled', false);
		    } else {
		    	swal(data.msg);
		    	$("#submit").prop('disabled', false);
		    }
		  },
		  error: function(xhr, textStatus, errorThrown) {
		    //called when there is an error
		   	swal(data.msg);
		    $("#submit").prop('disabled', false);

		  }
		});

	});

	function getcfee_customer(amount){
		//e.preventDefault();
		//e.stopImmediatePropagation();				
	    // accordion is open
	    var total_billpay_fees = parseFloat($("#amount").val().replace(/,/g, ''))+parseFloat($("#hid_fee").val())+parseFloat($("#hid_vat").val());
	    //alert($("#amount").val()+"+"+$("#hid_fee").val()+"+"+$("#hid_vat").val());
	    $.ajax({
	      url: '{{base_url("payment/get_fees_customer")}}',
	      type: 'POST',
	      dataType: 'json',
	      data:{amount:total_billpay_fees,client_id:'{{$_SESSION["client_record_id"]}}'},
	   	  beforeSend:function(xhr, textStatus) {
	        //called when complete
		    //loader.showPleaseWait();
	        $("#submit").prop('disabled', true);
	      },		  
		  complete: function(xhr, textStatus) {
		    //called when complete
		  	//loader.hidePleaseWait();
		  },
	      success: function(data, textStatus, xhr) {
	        //called when successful
	        //paste values
	        if(data.success){
	        	$(".conviniecefee_val").text(data.fee_data.fee);
	        	//$("#amount").text(data.total);
				var total= total_billpay_fees+parseFloat(data.fee_data.fee);
				$(".amount_total").text(total.toFixed(2));
				$("#amount_total").val(total.toFixed(2));
				$("#hid_pfee").val(data.fee_data.pf);
				$("#hid_tfee").val(data.fee_data.tf);
				$("#hid_totalfee").val(data.fee_data.fee);

		    	$("#submit").prop('disabled', false);
	        }  else {
	        	swal(data.msg);
		    	$("#submit").prop('disabled', false);
	        }

	      },
	      error: function(xhr, textStatus, errorThrown) {
	        //called when there is an error
        	swal(data.msg);
	    	$("#submit").prop('disabled', false);
	      }
	    });
	}
	$("#link_sandcard").click(function(){
	var form_data ={
		card_number:$("#card_number").val(),
		PIN:$("#pin").val(),
		OTP:$("#otp").val(),
		// notes:$("#notes").val(),
		// reference_num:$("#reference_num").val(),
		// name:$("#name").val(),
    	// email:$("#email").val(),
    	// mobile:$("#mobile").val(),
		amount:$("#amount").val(),
		total_amount:$("#amount_total").val(),
		hid_fee:$("#hid_fee").val(),
		hid_vat:$("#hid_vat").val(),
		customerid:$("#customerid").val(),
		};  
		$.ajax({
		url: '{{base_url("payment/link_sandcard")}}',
		type: 'POST',
		dataType: 'json',
		data: form_data,
		beforeSend:function(){
			loader.showPleaseWait();
			$("#link_sandcard").prop("disabled",true);
			$("#link_sandcard").text("Processing... Please wait");
			//loader.showPleaseWait();
		},
		complete: function(xhr, textStatus) {
			$("#link_sandcard").prop("disabled",false);
			$("#link_sandcard").text("Link Card");
			//loader.hidePleaseWait();
		},
		success: function(data, textStatus, xhr) {
			//console.log();
			if(data.success){
				// swal(data.msg.deviceIdDecrypted);
				swal(data.msg);
				loader.hidePleaseWait();
				$("#link_sandcard").prop("disabled",false);
				$("#link_sandcard").text("Link Card");				
				$("#transaction_id").text(data.msg.deviceNoDecrypted);
				$("#link_success_section").show();			
				
			} else {
				loader.hidePleaseWait();
				console.table(xhr);
				$("#link_sandcard").prop("disabled",false);
				$("#link_sandcard").text("Link Card");
				swal(data.msg.type);
				// window.location.reload()
			}
		},
			error: function(xhr, textStatus, errorThrown) {
				$("#link_sandcard").prop("disabled",false);	   
				$("#link_sandcard").text("Link Card");
				loader.hidePleaseWait();
				swal(xhr.msg);
			}
		});
	return false;
	});

	$("#sanddollar_usecard_process").click(function(){
	var form_data ={
		// type:$("#transtype").val(),
		qr_code:$("#qr_code").val(),
		card_pin:$("#card_pin").val(),
		can_setup_customname:$("#can_setup_customname").val(),
		custom_name:$("#custom_name").val(),

		notes:$("#notes").val(),
		reference_num:$("#reference_num").val(),
		name:$("#name").val(),
    	email:$("#email").val(),
    	mobile:$("#mobile").val(),
		amount:$("#amount").val(),
		total_amount:$("#amount_total").val(),
		hid_fee:$("#hid_fee").val(),
		hid_vat:$("#hid_vat").val(),
		}; 
		$.ajax({
		url: '{{base_url("payment/sanddollar_usecard_process")}}',
		type: 'POST',
		dataType: 'json',
		data: form_data,
		beforeSend:function(){
			loader.showPleaseWait();
			$("#sanddollar_usecard_process").prop("disabled",true);
			$("#sanddollar_usecard_process").text("Processing... Please wait");
			//loader.showPleaseWait();
		},
		complete: function(xhr, textStatus) {
			$("#sanddollar_usecard_process").prop("disabled",false);
			$("#sanddollar_usecard_process").text("Process Payment");
			//loader.hidePleaseWait();
		},
		success: function(response, textStatus, xhr) {
			//console.log();
			if(response.success){
				//console.table(response.msg.data[0].paymentRequestId);
				loader.hidePleaseWait();
				$("#sanddollar_usecard_process").prop("disabled",false);
				$("#sanddollar_usecard_process").text("Process Payment");		
				$("#transaction_id").text(response.msg.data[0].paymentRequestId);
				$("#sand_section").hide();	
				$("#success_section_transaction").show();
						
			} else {
				loader.hidePleaseWait();
				swal(xhr.msg);
			}
		},
			error: function(xhr, textStatus, errorThrown) {
				console.table(xhr);
				$("#sanddollar_usecard_process").prop("disabled",false);	   
				$("#sanddollar_usecard_process").text("Process Payment");

				loader.hidePleaseWait();
				swal(xhr.msg);
			}
		});
	return false;
	});
	var amount = "{{$payment_data['amount']}}";
	var balance= "{{number_format($Balance,2, '.', '')}}";
	if(parseInt(amount)>parseInt(balance)){
		$("#amount").val(0.00).trigger('change');
		swal('Sorry you do not have enough balance to continue this transaction. Please reload you wallet account');
		
	}

	</script>

</body>
</html>