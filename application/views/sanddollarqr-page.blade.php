<?php
//loading assets helper
defined('BASEPATH') OR exit('No direct script access allowed');
$assetHelper = new AssetHelper\AssetHelper();
$ci=&get_instance();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>SunCash.Me</title>
	<link rel="stylesheet" href="{{base_url('assets_main/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/main.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/main_checkout.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/responsive.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/all.min.css')}}">
	<link rel="stylesheet" href="{{base_url('assets/plugins/global/global2.css')}}">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>assets/plugins/icon.ico">

</head>
<body>
	<nav class="navbar navbar-expand-lg sticky-top bg-white shadow-sm p-0">
	  <div class="container p-0" role="navigation">
	  	<a class="navbar-brand" href="<?php echo base_url();?>"><img src="{{base_url('assets_main/imgs/header-logo@2x.png')}}" alt="Suncash Logo" class="d-inline-block align-top" width="160"></a>
	  	<a href="#" class="menu-button"></a>
	  	<div class="justify-content-end main-menu">
				<ul class="navbar-nav d-flex align-items-center">


		    </ul>
		  </div>
	  </div>
	</nav>
	<section class="" id="sand_section">
		<div class="container  text-center">
			<div class="row d-flex justify-content-center align-items-center mb-5">
				<div class="user-card transaction-details" style="margin-top:1rem;margin-bottom:-34px !important;">
					<div class="header w-content" style="height:6rem !important;"></div>
					<div class="body">
						<form id="sanddollar_form" method="POST">
				        <div class="user-image ">
								@if(!empty($_SESSION['profile_pic']))
								<img src="{{$_SESSION['profile_pic']}}">
								@else
								<img src="{{base_url('assets_main/imgs/suncash-icon.png')}}">
								@endif
						</div>
						<div class="details" style="margin-bottom: -40px;">

							<div class="message">You're about to pay <?=ucfirst($_SESSION['suntag_shortcode']) ?></div>
							<select name="payment_method" id="payment_method" >
								<option value="">BSD</option>
							</select>
							<input class="input-amount" type="text" id="amount" name="amount" value="{{$payment_data['amount']}}" style="height: 100% !important">
						</div>
						<div class="text-left">

								<div class="item primary-border" >
									<center>Show QR Code for the Sender to Scan<br>

										<?php

										$mobile =$payment_data['mobile'];

										$mobile = str_replace(' ', '', $mobile); // Replaces all spaces with hyphens.
										$mobile = preg_replace('/[^A-Za-z0-9]/', '', $mobile);

										// dd($mobile1);

										if($_SESSION['tag']=='CUSTOMER'){
											$qrcode = \qr::GenerateQr('nzia:qr/suncashvc2+'.'c-'.$_SESSION['mobile_suntag'].'-suncashme'.'@bs.nzia?amount='.$payment_data['amount'],'',300);
										}elseif($_SESSION['tag']=='MERCHANT'){
											// $qrcode = \qr::GenerateQr('nzia:qr/suncash+'.'b-'.$_SESSION['suntag_shortcode'].'-suncashme'.'@bs.nzia?amount='.$payment_data['amount'],'',300);
											$qrcode = \qr::GenerateQr('nzia:qr/suncashvc2+'.'b-'.$_SESSION['client_record_id'].'-s-'.$mobile.'@bs.nzia?amount='.$payment_data['amount'],'',300);
										}
											$qrimage =' <img src="data:'.$qrcode->getContentType().';base64,'.$qrcode->generate().'" />';
											echo $qrimage;
										// nzia:qr/suncashvc2+639954754833@bs.nzia?amount=120.00
										// dd($qrcode);
										?></a>
									</center>	
									<br>																									
								  	<div class="label text-center">Transaction Details:</div>
										<div class="item" style="margin-bottom: 0px !important;margin-top: 0px !important;">
										  <div class="row text-details">
											  	<div class="col">Principal</div>
											  	<div class="col text-right ">$ <span class="amount_val">{{$payment_data['amount']}}</span></div>
										  </div> 
										</div> 
										<div class="item" style="margin-bottom: 0px !important;margin-top: 0px !important;">
										  <div class="row text-details">
										  	<div class="col">Transaction Fee</div>

										    <div class="col text-right">$ <span class="fee_val">0.00</span></div>
										  </div>
										</div>																						
									</div>						

									<div class="item total">
									  <div class="row">
									  	<?php 
									  		$total = str_replace( ',', '', $payment_data['amount']);//+$fee_data['fee']+$fee_data['vat_charge'];
									  	?>
									  	<div class="col label">Total Due</div>
									  	<div class="col text-right amount"><span class="amount_total">{{$total}}</span> BSD</div>
									  </div>
									</div>
													
							</div>

						</form>
					</div>
				</div>				
			</div>
		</div>
		<footer class="py-3 footer">
			<div class="container">
				<div class="row">
					<div class="col-12 col-lg-6">
						<span class="txt-shit">© {{date('Y')}} SunCash.Me. All Rights Reserved.</span>
					</div>
					<div class="col-12 col-lg-6 ">
						<img class="img-x" src="{{base_url('assets_main/imgs/footer-logo.png')}}">
					</div>
				</div>
			</div>
		</footer>		
	</section>



	</div><!-- end -->


	<script src="{{base_url('assets_main/js/jquery-3.2.1.min.js')}}"></script>
	<script src="{{base_url('assets_main/js/bootstrap.min.js')}}"></script>

	{!!$assetHelper->link_plugins('js','sidebar-nav/dist/sidebar-nav.min.js')!!}
	{!!$assetHelper->link_plugins('js','override/jquery.slimscroll.js')!!}
	{!!$assetHelper->link_plugins('js','override/waves.js')!!}
	{!!$assetHelper->link_plugins('js','override/custom.min.js')!!}
	{!!$assetHelper->link_plugins('js','sweetalert2-master/dist/sweetalert2.all.min.js')!!}
    {!!$assetHelper->link_plugins('js','Inputmask-5.x/dist/jquery.inputmask.min.js')!!}
    {!!$assetHelper->link_plugins('js','bootstrap-formhelpers-phone/bootstrap-formhelpers-phone.js') !!}
    {!!$assetHelper->link_plugins('js','bootstrap-formhelpers-phone/bootstrap-formhelpers-phone.format.js') !!}
	{!!$assetHelper->link_plugins('js','global/global.js')!!}	
	{!!$assetHelper->link_plugins('js','html5-qrcode.min.js')!!}
	<script src="{{base_url('assets_main/js/main.js')}}"></script>
	<script  type="text/javascript" charset="utf-8">


	$("#amount").change(function(){

		if($("#amount").val()<=0.00){
		  	swal("Amount is required.");

		  	return false;
		}
		$(".amount_total").text(total.toFixed(2));
		$("#amount_total").val(total.toFixed(2));
		$(".amount_val").text($("#amount").val());


	});

	$("#sanddollar_form").submit(function(){
	   var form_data ={
		mobile:$("#sand_mobile").val(),
		passcode:$("#sand_otp").val(),
		pin:$("#sand_pin").val(),
		notes:$("#notes").val(),
		sand_qr:$("#sand_qr").val(),
		// reference_num:$("#reference_num").val(),
		name:$("#sand_name").val(),
    	email:$("#sand_email").val(),
		amount:$("#amount").val(),
		total_amount:$("#amount_total").val(),
		// hid_fee:$("#hid_fee").val(),
		// hid_vat:$("#hid_vat").val(),
		}   
		$.ajax({
		url: '{{base_url("payment/process_sanddollar_payment")}}',
		type: 'POST',
		dataType: 'json',
		data: form_data,
		beforeSend:function(){
			loader.showPleaseWait();
			$("#sand_process").prop("disabled",true);
			$("#sand_process").text("Processing... Please wait");
			//loader.showPleaseWait();
		},
		complete: function(xhr, textStatus) {
			$("#sand_process").prop("disabled",false);
			$("#sand_process").text("Process Payment");
			//loader.hidePleaseWait();
		},
		success: function(response, textStatus, xhr) {
			//console.log();
			if(response.success){
				console.table(response.msg.data[0].paymentRequestId);
				loader.hidePleaseWait();
				$("#sand_process").prop("disabled",false);
				$("#sand_process").text("Process Payment");		
				$("#transaction_id").text(response.msg.data[0].paymentRequestId);
				$("#sand_section").hide();	
				$("#success_section_transaction").show();
						
			} else {
				loader.hidePleaseWait();
				$("#sand_process").prop("disabled",false);	   
				$("#sand_process").text("Process Payment");
              	swal(response.msg.type);
			}
		},
			error: function(xhr, textStatus, errorThrown) {
				console.table(xhr);
				$("#sand_process").prop("disabled",false);	   
				$("#sand_process").text("Process Payment");

				loader.hidePleaseWait();
				swal(xhr.msg);
			}
		});
	return false;
	});

	function onScanSuccess(qrCodeMessage) {
		// handle on success condition with the decoded message;
		$("#sand_qr").val(qrCodeMessage);
		html5QrcodeScanner.clear();
	}

	function onScanError(errorMessage) {
		// handle on error condition, with error message
			//alert(errorMessage);
		//html5QrcodeScanner.render(onScanSuccess, onScanError);  
	}

	var html5QrcodeScanner = new Html5QrcodeScanner(
		"reader", { fps: 10, qrbox: 250 });
	html5QrcodeScanner.render(onScanSuccess, onScanError);
	</script>

</body>
</html>