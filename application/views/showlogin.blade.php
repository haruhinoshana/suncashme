<?php
//loading assets helper
defined('BASEPATH') OR exit('No direct script access allowed');
$assetHelper = new AssetHelper\AssetHelper();
$ci = &get_instance();
?>
<div class="modal-content" style="height:100% !important;">
    <div class="modal-header">
        <h4 class="modal-title">Login Customer Account</h4>
        <!---- <button type="button" class="close {{$is_encrypted==1 ? 'form_btn' : '' }}" data-dismiss="modal" form_id='{{$trans_id}}'  aria-label="Close"> -->
            <span aria-hidden="true">×</span>
        </button>
    </div><!-- /.modal-header -->
    <!-- <div class="content-wrapper"> -->

    <!-- <section class="content"> -->

      <form name="customer_login_form" id="customer_login_form" class="form-horizontal" method="POST" enctype="enctype/form-data">
      <div class="container-fluid">
        <div class="card-body">
          <div class="col-12">
            <!-- accepted payments column -->
            <div class="form-group">
                <input type="text" class="form-control" id="u" placeholder="Login"/>
            </div>
            <div class="form-group">
                <input type="password" class="form-control" id="p" placeholder="Password"/>
            </div>
          </div><!-- /.col -->
        </div>
      </div><!-- /.container-fluid -->

      <div class="modal-footer">
      <button type="button" class="btn btn-primary" data-dismiss="modal"  aria-label="Close">Close</button>
      <button type="submit" class="btn btn-primary" id="login_customer">Login</button>
      </div>
      </form>
</div><!-- /.modal-content -->
<script>
// $(document).ready(function(){
//   //form submit
//   sel2();
//   $(document).on('click','.form_btn',function(){
//   //Pace.restart(function(){
//       var attr_id =$(this).attr('form_id');
//       //alert('{{$t_type}}');
//       @if($t_type=='SEND')
//       var url_path = '{{base_url("transactions/form")}}';
//       @else
//       var url_path = '{{base_url("transactions/receive_form_request")}}';
//       @endif
//       /*p1 button , id , url , size: small,meduim,large,xlarge*/
//       LoadModal($(this),attr_id,url_path,'large');
//   //});
//    });  
//   $("#cancel_form").submit(function(e){

//       e.preventDefault();
//       var btn = $(document.activeElement).val();

//       Swal.fire({
//         title: '',
//         text:'Are you sure you want to cancel this request?',
//         icon: 'warning',
//         showCancelButton: true,
//         confirmButtonColor: '#3085d6',
//         cancelButtonColor: '#d33',
//         confirmButtonText: 'Yes'
//       }).then((result) => {
//           if (result.value) {

//           var url = "{{base_url('transaction/receive_process')}}";
//           var formData = $(this).serializeArray();
//           formData.push({ name: "submit", value: btn });

//           var button = $("#save-btn");

//           ProcessForm(formData,url,button,function(data) {
//             // Code that depends on 'result'
//             if(data.success){

//               Swal.fire({
//                 icon: 'success',
//                 title: 'Process Completed',
//                 text: data.msg,
//               }).then((result) => {
//                 // Reload the Page
//                 hideModal('medium');
//                 Pace.track(function(){
//                 // $('#alterations_tbl').DataTable().ajax.reload();
//                 // $('#refund_tbl').DataTable().ajax.reload();
//                 window.location.reload();
//                 });
//               });
//             } else {
//               swal.fire(data.msg);
//             }
//           });
//         } else {
//           // //window.location.reload();
//         }
//       });
//       return false;
//   });
// }); 
</script>
