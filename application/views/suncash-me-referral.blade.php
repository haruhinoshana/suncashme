<?php
//loading assets helper
defined('BASEPATH') OR exit('No direct script access allowed');
$assetHelper = new AssetHelper\AssetHelper();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>SunCash.Me</title>
	<link rel="stylesheet" href="{{base_url('assets_main/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/main.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/all.min.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/responsive.css')}}">
	{!!$assetHelper->link_plugins('css','select2-4.0.6-rc.1/dist/css/select2.min.css')!!}
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>assets/plugins/icon.ico">
	<style type="text/css">

				.img-x {
				  display: block;
				  margin-left: auto;
				  margin-right: auto;
				  //width: 100%;
				  min-width:128px;
				  min-height:43px;
				}
				.txt-shit{
					text-align:center !important;
				}
				.app-button {
					/* width: 250px;
					height: 80px; */
					border-radius: 13px;
					/* padding-left: 28px; */
					padding-top: 10px;
					box-shadow: 0 0 40px rgba(51, 51, 51, .1)
				}

				.app-button i {
					font-size: 40px;
					margin-right: 10px
				}
			
	</style>
</head>
<body>
	<nav class="navbar navbar-expand-lg sticky-top bg-white shadow-sm p-0">
	  <div class="container p-0" role="navigation">
	  	<a class="navbar-brand" href="<?php echo base_url();?>"><img src="{{base_url('assets_main/imgs/header-logo@2x.png')}}" alt="Suncash Logo" class="d-inline-block align-top" width="200"></a>
	  	<a href="#" class="menu-button"><span></span></a>
	  	<div class="justify-content-end main-menu">
				<ul class="navbar-nav d-flex">
		      <li class="nav-item active">
		        <a class="nav-link" href="{{base_url('info/faq')}}">FAQ <span class="sr-only">(current)</span></a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="{{base_url('info/contacts')}}">Contact Us</a>
		      </li>
		      <li class="nav-item">
				@if(!empty($suntag_data))
		      	@if($suntag_data['tag']=='MERCHANT')
		        <a class="nav-link active" href="{{base_url('business/login')}}">Login</a>
		        @else
		        <a class="nav-link active" href="{{base_url('wallet/login')}}">Login</a>
				@endif
				@endif
		      </li>
		    </ul>
		  </div>
	  </div>
	</nav>

	<div class="row-fluid">
		<div class="col">	
		<section class="full" >
			<div class="container  text-center" style="margin-height:50px;min-height: 80vh">
				<div class="row d-flex justify-content-center align-items-center">

				<div class="user-card transaction-details" style="margin-top:1rem;margin-bottom:15px !important;">
	
				    <div class="header w-content" style="height:5rem !important;"></div>
						<div class="body" style="padding-top:0px !important">
							<div class="user-image" style="margin-bottom: 20px;">
								@if(!empty($suntag_data['profile_pic']))
									<!-- <img src="{{$suntag_data['profile_pic']}}"> -->
									<img src="{{base_url('assets_main/imgs/suncash-icon.png')}}">
								@else
								<img src="{{base_url('assets_main/imgs/suncash-icon.png')}}">
								@endif
							</div>
							
							<div class="details md-3" style="margin-bottom: -30px;">
								<div class="user-name"><b>{{!empty($suntag_data) ? $suntag_data['dba_name']:''}} has invited you to <br>SunCash</b>
								</div> 
							</div>		

							<div class="user-name"><b>Experience the convenience of the mobile wallet transactions. Get started now!</b></div>


								<div class="row d-flex justify-content-center">
									<button id="apl" class="btn btn-dark app-button mb-2 d-flex flex-row"> <i class="fa fa-apple"></i>
										<div class="d-flex flex-column"> 									
											<span style="font-size: 10px;">Download on the</span>
											<p class="mb-0">App Store</p>
										</div>
									</button>
								</div>



							<hr>

							<br><div class=" " style="margin-top: 30px;">Don't Want to download the app now?</div>
							<div class="">Enter your phone number to create yout account right here.</div><br>				
								<form class="form-horizontal" style="margin-bottom: -30px;" id="referral_registration" name ="referral_registration" method="POST" >

								<center>
								<div class=''>
									<div class="input-group">
										<select  class="form-control"  id="countries" name="country" height="width:100%;" style="border-radius: 100px;width: 150px;">
											<option value=''>--Select--</option>
											@if(!empty($country))
												@foreach($country as $country_val)
												<option  value="{{$country_val['name']}}" mp="{{$country_val['mobile_prefix']}}" img="https://www.countryflags.io/{{$country_val['code']}}/flat/24.png" title =" {{$country_val['name']}}" code="{{$country_val['code']}}">{{$country_val['name']}}</option>								
												@endforeach
											@endif
										</select>
		
											<input type="tel" class="form-control "  name="mobile" id="mobile"   style="height: 30px;"><!-- bfh-phone data-format="1 (ddd) ddd-dddd" value ="1 242"  -->
				
									</div>
								</div><br>
								</center>
									<input type="hidden" value ="{{$referer}}" name="referer" id ="referer" />
									<input type="hidden" value ="" name="hc" id ="hc" />
									
									<button type="submit" id="create_customer_account" class="btn btn-white full" style="margin-top:0px !important">Create account</button>
								</form>

						</div>
					</div>

				</div>
			</div>
				<footer class="py-3 footer" style="">
					<div class="container">
						<div class="row">
							<div class="col-12 col-lg-6">
								<span class="txt-shit">© {{date('Y')}} SunCash.Me. All Rights Reserved.</span>
							</div>
							<div class="col-12 col-lg-6 text-right">
								<img class="img-x" src="{{base_url('assets_main/imgs/footer-logo.png')}}">
							</div>
						</div>
					</div>
				</footer>
		</section>
		</div>


	</div>



	<script src="{{base_url('assets_main/js/jquery-3.2.1.min.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
	<script src="{{base_url('assets_main/js/bootstrap.min.js')}}"></script>
	<script src="{{base_url('assets_main/js/main.js')}}"></script>
	{!!$assetHelper->link_plugins('js','global/global.js')!!}
	{!!$assetHelper->link_plugins('js','jquery-1.11.3.min.js')!!}
	{!!$assetHelper->link_plugins('js','bootstrap-3.3.7/js/bootstrap.min.js')!!}
	{!!$assetHelper->link_plugins('js','sidebar-nav/dist/sidebar-nav.min.js')!!}
	{!!$assetHelper->link_plugins('js','override/jquery.slimscroll.js')!!}
	<!-- {!!$assetHelper->link_plugins('js','override/waves.js')!!} -->
	{!!$assetHelper->link_plugins('js','override/custom.min.js')!!}
	{!!$assetHelper->link_plugins('js','sweetalert2-master/dist/sweetalert2.all.min.js')!!}
	<!-- {!!$assetHelper->link_plugins('js','bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js')!!} -->
	<!-- {!!$assetHelper->link_plugins('js','curioussolutions-datetimepicker/dist/DateTimePicker.min.js')!!} -->
	<!-- {!!$assetHelper->link_plugins('js','dropzone-master/dist/dropzone.js')!!}	 -->
	{!!$assetHelper->link_plugins('js','Inputmask-5.x/dist/jquery.inputmask.min.js')!!}
    {!!$assetHelper->link_plugins('js','bootstrap-formhelpers-phone/bootstrap-formhelpers-phone.js') !!}
    {!!$assetHelper->link_plugins('js','bootstrap-formhelpers-phone/bootstrap-formhelpers-phone.format.js') !!}
	{!!$assetHelper->link_plugins('js','select2-4.0.6-rc.1/dist/js/select2.full.min.js')!!}
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<script>
	function formatOptions (state) {

	if (!state.id) { return state.text; }

	var img_url=$(state.element).attr('img');
	var $state = $(
	'<span ><img sytle="display: inline-block;" src="'+img_url+'" class="rounded-circle" width="35" height="35"/> ' + state.text + '</span>'
	);
	return $state;
	}

	$(document).ready(function(){

		$('.select2-selection').css('border-radius','70')	
		$("#apl").click(function(){
			window.location.href='https://apps.apple.com/app/suncash-customer/id1425940898';
		});
		$("#countries").val('Bahamas').trigger('change');
		$("#countries").select2({
		templateResult: formatOptions,
			templateSelection: function (formatOptions) {
				var code=$("#countries").find(":selected").attr('code');
				var title=$("#countries").find(":selected").attr('title');
				var $span = $("<span><img src='https://www.countryflags.io/"+code+"/flat/24.png'/> " +  title + "</span>");
				return $span;
			}
		});
		
		$("#referral_registration").submit(function(e){

				//var function_ctrl = "process_check" ;
				var form_data = $("#referral_registration").serializeArray();
				$.ajax({
				url: "<?=site_url("customers/referral_process")?>",
				type: 'POST',
				dataType: 'json',
				data: form_data,
				beforeSend:function(xhr, textStatus) {
					//called when complete
					$("#create_customer_account").prop('disabled', true);
				},
				complete: function(xhr, textStatus) {
					//called when complete
					$("#create_customer_account").prop('disabled', false);
				},
				success: function(data) {
					if(data.success){
					//alert(data.url);
					window.location.href=data.link;

						//otable.ajax.reload();
						//$("#transfer_modal").modal('hide');
						$("#create_customer_account").prop('disabled', false);
					} else {
						//swal(data.msg);
						$("#create_customer_account").prop('disabled', false);
					}
				},
				error: function(data, textStatus, errorThrown) {
					//called when there is an error
					//swal(data.msg);
					$("#create_customer_account").prop('disabled', false);
				}
				});


		return false;
			
		});
		$("#countries").change(function(){
			$("#mobile").val($( "#countries option:selected" ).attr('mp'));
			$("#hc").val($( "#countries option:selected" ).attr('mp'));
		});
		
	});


	</script>
</body>
	@include('layout/modal')
</html>