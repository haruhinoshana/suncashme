<?php
//loading assets helper
defined('BASEPATH') OR exit('No direct script access allowed');
$assetHelper = new AssetHelper\AssetHelper();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>SunCash.Me</title>
	<link rel="stylesheet" href="{{base_url('assets_main/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/main.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/all.min.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/responsive.css')}}">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>assets/plugins/icon.ico">
	<style type="text/css">
		@media only screen and (max-width: 768px)  {
				.img-x {
				  display: block;
				  margin-left: auto;
				  margin-right: auto;
				  //width: 100%;
				  min-width:128px;
				  min-height:43px;
				}
				.txt-shit{
					text-align:center !important;
				}
		}		
	</style>
</head>
<body>
	<nav class="navbar navbar-expand-lg sticky-top bg-white shadow-sm p-0">
	  <div class="container p-0" role="navigation">
	  	<a class="navbar-brand" href="<?php echo base_url();?>"><img src="{{base_url('assets_main/imgs/header-logo@2x.png')}}" alt="Suncash Logo" class="d-inline-block align-top" width="200"></a>
	  	<a href="#" class="menu-button"><span></span></a>
	  	<div class="justify-content-end main-menu">
				<ul class="navbar-nav d-flex">
		      <li class="nav-item active">
		        <a class="nav-link" href="{{base_url('info/faq')}}">FAQ <span class="sr-only">(current)</span></a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="{{base_url('info/contacts')}}">Contact Us</a>
		      </li>
		      <li class="nav-item">
				@if(!empty($suntag_data))
		      	@if($suntag_data['tag']=='MERCHANT')
		        <a class="nav-link active" href="{{base_url('business/login')}}">Login</a>
		        @else
		        <a class="nav-link active" href="{{base_url('wallet/login')}}">Login</a>
				@endif
				@endif
		      </li>
		    </ul>
		  </div>
	  </div>
	</nav>

	<div class="row-fluid">
		<div class="col">	
		<section class="full" >
			<div class="container  text-center" style="margin-height:50px;min-height: 80vh">
				<div class="row d-flex justify-content-center align-items-center">

				<div class="user-card transaction-details" style="margin-top:1rem;margin-bottom:15px !important;">
						<?php if(!empty($suntag_data) && $sts=='Approved'){ ?>
						
				        <div class="header w-content" style="height:5rem !important;"></div>
<!-- 						<div class="header" style="padding:1rem 2rem 1.5rem 2rem !important">

							<div class="form-div">
							<select name="cash_type" id="cash_type" class="required">
								<option value="bsd">BSD </option>
								<option value="wallet">PH </option>
							</select>
							</div>
							<div class="form-div">
							<input type="text" id="amount" name="amount" value="0.00">
							</div>
						</div> -->
						<div class="body" style="padding-top:0px !important">
						<form id="payment_form" method="POST" >
							<div class="user-image">
								@if(!empty($suntag_data['profile_pic']))
								<img src="{{$suntag_data['profile_pic']}}">
								@else
								<!-- <img src="{{base_url('assets_main/imgs/user-image.jpeg')}}"> -->
								<img src="{{base_url('assets_main/imgs/suncash-icon.png')}}">
								
								@endif
							</div>

							<div class="details" style="margin-bottom: -30px;">
								<div class="user-name">
								<button type="button" class="btn btn-sm scan_qr_page" id_name="" style="padding: 0px; margin-left: 2px; border:none;; margin-top:0px !important;;margin-top: 0px !important">   
						             <?php 
										$amt= !empty($amount_to_process) ? '/'.$amount_to_process : '' ;
						                $qrcode = \qr::GenerateQr($suntag_data['qrcode'].$amt,'',50);
										//  dd($qrcode);
						                $qrimage =' <img src="data:'.$qrcode->getContentType().';base64,'.$qrcode->generate().'" />';
						                echo $qrimage;
						               					            
						            ?> 
								</button>
								</div>
								<div class="user-name">{{!empty($suntag_data) ? $suntag_data['dba_name']:''}}</div>
								<div class="user-link">suncash.me/{{!empty($suntag_data) ? $suntag_data['suntag_shortcode']: ''}}</div>
							</div>		
				
							<select name="cash_type" id="cash_type" class="select-currency">
								<option value="bsd">BSD </option>
								<!-- <option value="wallet">PH </option> -->
							</select>
							<input class="input-amount" type="text" id="amount" name="amount" value="{{!empty($amount_to_process) ? $amount_to_process : 0.00}}" style="height: 100% !important; margin-bottom:10px !important;margin-top: 0px !important">		

						            <div class="form-group my-select p-0" style="margin-bottom: 0px !important;">

								        <select class="form-control" name="payment_method" id="payment_method"  placeholder="">
						                    <option selected disabled hidden>Select Payment Method</option>
											@if($suntag_data['tag']=='MERCHANT')
												@if(!empty($selected_settings_data['system_services_ids']))
													<?php
														//$_1='none';//suncash account
														//$_2='none';//credit card/debit card
														//$_3='none';//cash payment
														$set_option=explode(',',$selected_settings_data['system_services_ids']);
														foreach($set_option as $value) {
															if($value=='1'){
															 echo '<option value="wallet">SunCash Accounts</option>';				 
															}
															if($value=='2'){
															echo '<option value="card">Debit/Credit Card</option>';
															}
															if($value=='3'){
															 // echo '<option value="cash">Cash Payments</option>';
															}	
															if($value=='4'){
															 echo '<option value="voucher">Voucher Payment</option>';
															}																
															if($value=='5'){
															 echo '<option value="sanddollar">SandDollar Payment</option>';
															//  echo '<option value="sanddollarqr">SandDollar QR</option>';
															}		
															if($value=='6'){
															 echo '<option value="amazon">Amazon Payment</option>';
															}														
															
															if($value=='7'){
															 echo '<option value="paypal">Paypal Payment</option>';
															}	
														}
													?>
												@endif
											@else
												<option value="wallet">SunCash Accounts</option>
												@if($_SESSION['tag']=='CUSTOMER')
													@if($_SESSION['kyc_type']=='full')
													<option value="card">Debit/Credit Card</option>
													@endif
												@else
													<option value="card">Debit/Credit Card</option>
												@endif
												<option value="voucher">Voucher Payment</option>
												<option value="sanddollar">SandDollar Card</option>
												<option value="amazon">Amazon Payment</option>
												<option value="paypal">Paypal Payment</option>
											@endif
						                </select>
						            </div>
									<div class="nsd_section">
										<div class="form-group" style="margin-bottom: 0rem !important;">
										<input type="text" class="form-control" id="reference_num" name="reference_num" placeholder="Reference # (Order, Quote, Invoice or Account)" value="" autocomplete="off" required>
										</div>
									</div>
									<div class="merchant_section" style="display:none">
										<div class="form-group" style="margin-bottom:0px !important;">
											<input type="text" class="form-control" id="name" name="name" placeholder="Full Name" autocomplete="off" style="resize:none;" >
										</div>		
										<div class="form-group" style="margin-bottom:0px !important;">
											<input type="email" class="form-control" id="email" name="email" placeholder="Email Address" autocomplete="off" style="resize:none;" >
										</div>	

									</div>		
									<div class="mobile_section" style="display:none">	
										<div class="form-group" style="margin-bottom:0px !important;">
											<input type="text" class="form-control bfh-phone" id="mobile" name="mobile" placeholder="Enter your mobile number"  data-format="1 (ddd) ddd-dddd" value ="1 242"  autocomplete="off" style="resize:none;">

										</div>
									</div>																		
									<div class="notes_section"  style="display:none">
										<div class="form-group" style="margin-bottom: 0rem !important ; ">
											<textarea maxlength="100" type="text" class="form-control b" id="notes" name="notes" placeholder="Notes" autocomplete="off"  style="resize:none;"></textarea>
										</div>
									</div>	
									<button type="submit" id="process_payment" class="btn btn-orange full" style="margin-top:0px !important">Next</button>
							
						</div>
						</form>

						<?php } else if($sts=='Pending') {?>
							<center>Sorry! suncash.me profile is still pending for approval.</center>
						<?php } else if($sts=='Rejected') {?>
							<center>Sorry! suncash.me profile is rejected.</center>
						<?php } else if($sts=='Inactive') {?>
							<center>Sorry! suncash.me profile is inactive.</center>		
						<?php } else if($sts=='For Verification') {?>
							<center>Please continue your pending registration. Verify your Account.</center>		
						<?php } else if($sts=='Store Deactivated') {?>
							<center>Sorry! suncash.me profile is Deactivated</center>																			
						<?php } else {?>
							<center>Sorry! We can't find that suncash.me profile.</center>
						<?php } ?>	

					</div>

				</div>
<!-- 				<div class=" mt-5">
					<a href="#" class="text-link">Report This Link</a>
				</div> -->

			</div>
				<footer class="py-3 footer" style="">
					<div class="container">
						<div class="row">
							<div class="col-12 col-lg-6">
								<span class="txt-shit">© {{date('Y')}} SunCash.Me. All Rights Reserved.</span>
							</div>
							<div class="col-12 col-lg-6 text-right">
								<img class="img-x" src="{{base_url('assets_main/imgs/footer-logo.png')}}">
							</div>
						</div>
					</div>
				</footer>
		</section>
		</div>


	</div>



	<script src="{{base_url('assets_main/js/jquery-3.2.1.min.js')}}"></script>
	<script src="{{base_url('assets_main/js/bootstrap.min.js')}}"></script>
	<script src="{{base_url('assets_main/js/main.js')}}"></script>
	{!!$assetHelper->link_plugins('js','global/global.js')!!}
	<!-- {!!$assetHelper->link_plugins('js','jquery-1.11.3.min.js')!!} -->
	<!-- {!!$assetHelper->link_plugins('js','bootstrap-3.3.7/js/bootstrap.min.js')!!} -->
	{!!$assetHelper->link_plugins('js','sidebar-nav/dist/sidebar-nav.min.js')!!}
	{!!$assetHelper->link_plugins('js','override/jquery.slimscroll.js')!!}
	<!-- {!!$assetHelper->link_plugins('js','override/waves.js')!!} -->
	{!!$assetHelper->link_plugins('js','override/custom.min.js')!!}
	{!!$assetHelper->link_plugins('js','sweetalert2-master/dist/sweetalert2.all.min.js')!!}
	<!-- {!!$assetHelper->link_plugins('js','bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js')!!} -->
	<!-- {!!$assetHelper->link_plugins('js','curioussolutions-datetimepicker/dist/DateTimePicker.min.js')!!} -->
	<!-- {!!$assetHelper->link_plugins('js','dropzone-master/dist/dropzone.js')!!}	 -->
	{!!$assetHelper->link_plugins('js','Inputmask-5.x/dist/jquery.inputmask.min.js')!!}
    {!!$assetHelper->link_plugins('js','bootstrap-formhelpers-phone/bootstrap-formhelpers-phone.js') !!}
    {!!$assetHelper->link_plugins('js','bootstrap-formhelpers-phone/bootstrap-formhelpers-phone.format.js') !!}
	<script>
	$(document).ready(function(){
    $(document).on('click','.scan_qr_page',function(){
        var url_path = '{{base_url("view/scan_qr")}}';
        var attr_id =$(this).attr('id_name');
		var other_data ={
			'amount_to_process':'{{$amount_to_process}}'
		};
        //function found on global.js
        //P1=button,P2=attr_id,P3=url of module, modal_size ex:large,meduim,small
        LoadModal($(this),attr_id,url_path,'medium',other_data);
    });
    $('#reference_num').keyup(function() {
            if (this.value.match(/[^a-zA-Z0-9]/g)) {
                this.value = this.value.replace(/[^a-zA-Z0-9]/g, '');
            }
    });

	$("#amount").inputmask({ 'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true, 'digits': 2, 'digitsOptional': false, 'placeholder': '0.00', rightAlign : false,clearMaskOnLostFocus: !1,   });

	@if($_SESSION['tag']=='MERCHANT')
	  $(".merchant_section").show();
	  $(".nsd_section").show();
	  $(".notes_section").show();
      $("#amount").attr('required', true);
      $("#countries").attr('required', true);
      $("#sender_fname").attr('required', true);
	@else
		$(".merchant_section").hide();
		$(".nsd_section").show();
	    $(".notes_section").show();
		$("#amount").attr('required', false);
		$("#countries").attr('required', false);
		$("#sender_fname").attr('required', false);
	@endif
	$("#payment_form").on('submit',function(e){

    	e.preventDefault();
		  $('.hb').remove();
		  $('.form-div').removeClass('has-error').removeClass('has-success');

		  //check validation
		  var err_count = 0;
		  var to_req=[];
		  //jquery blank validation..
		  $(".required").each(function(){
		      var field_id = $(this).attr("id");
		      var data=[];
		      if($(this).val()==""){
		        data['id']=field_id;      
		        to_req.push(data);
		        err_count++;
		      }
		  });

		  /*if($("#password").val()!=$("#cpassword").val()){
		    swal("Youre password and confirmation password do not match.");
		    return false;
		  }*/


		  if(err_count>0){
		    swal(
		    'Oops...',
		    "Please do check required fields.",
		    'error'
		    );  
/*		    $.each(to_req, function( index, value ) {
		      $("#"+value.id).parent().after('<div class="hb"><div class="help-block" style="color:#f38989; font-size:7pt;margin-bottom:0px !important;">Required filed.</div></div>').addClass('has-error');
		    });   */     
		    return false;
		  }
		 

		  if($("#amount").val()<=0.00){
		  	swal("Amount is required.");
		  	return false;
		  }

      		//var function_ctrl = "process_check" ;
	        var form_data = {
	        	payment_method:$("#payment_method").val(),
				amount:$("#amount").val(),
				notes:$("#notes").val(),
				reference_num:$("#reference_num").val(),
				name:$("#name").val(),
				email:$("#email").val(),
				mobile:$("#mobile").val(),
	        };
	        $.ajax({
	          url: "<?=site_url("payment/index")?>",
	          type: 'POST',
	          dataType: 'json',
	          data: form_data,
	          beforeSend:function(xhr, textStatus) {
	            //called when complete
	            $("#process_payment").prop('disabled', true);
	          },
	          complete: function(xhr, textStatus) {
	            //called when complete
	             $("#process_payment").prop('disabled', false);
	          },
	          success: function(data) {
	            if(data.success){
	            //alert(data.url);
	            window.location.href=data.url;

	                //otable.ajax.reload();
	                //$("#transfer_modal").modal('hide');
	                $("#process_payment").prop('disabled', false);
	            } else {
	            	swal(data.msg);
	            	$("#process_payment").prop('disabled', false);
	            }
	          },
	          error: function(data, textStatus, errorThrown) {
	            //called when there is an error
	             swal(data.msg);
	             $("#process_payment").prop('disabled', false);
	          }
	        });


		});
	});
	
	$('#notes').on('keypress', function (event) {
		var regex = new RegExp("^[a-zA-Z0-9 ]+$");
		var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
		if (!regex.test(key)) {
		event.preventDefault();
		return false;
		}
	});
	$("#payment_method").change(function(){
		if($(this).val()=='sanddollar'){
			$(".nsd_section").hide();
			$("#reference_num").removeAttr('required');
			$(".merchant_section").show();
			$(".notes_section").show()
			$(".mobile_section").show();
		}else if($(this).val()=='sanddollarqr'){
			$(".nsd_section").hide();
			$(".notes_section").hide();
			$(".merchant_section").hide();
			$("#reference_num").removeAttr('required');
			$(".mobile_section").show();
		}else if($(this).val()=='card'){
			$(".nsd_section").show();
			$(".notes_section").show();
			$(".merchant_section").hide();
			$("#reference_num").removeAttr('required');
			$(".mobile_section").hide();		
		}else {
			$(".nsd_section").show();
			$(".merchant_section").show();
			$("#reference_num").attr('required','');
			$(".notes_section").show()
			$(".mobile_section").show();
		}
	});	
	</script>
</body>
	@include('layout/modal')
</html>