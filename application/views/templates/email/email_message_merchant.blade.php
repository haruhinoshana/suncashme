<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Customer Email Confirming Transfer</title>
	<link rel="stylesheet" href="./assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="./assets/css/main.css">
	<style type="text/css" >
@import url("fonts.css");
body {
  -webkit-font-smoothing: antialiased !important;
  font-family: "Roboto", sans-serif;
  font-weight: 400;
  font-size: 15px;
  color: #292929;
  line-height: 1.5em;
  padding: 3rem; }

a.btn {
  box-shadow: 0px 2px 6px 0px rgba(82, 78, 60, 0.34);
  padding: 1rem 3rem;
  min-width: 10rem;
  margin-top: 2rem; }
  a.btn.btn-primary {
    border-color: #fc6414;
    background-color: #fc6414; }
    a.btn.btn-primary:hover, a.btn.btn-primary:focus {
      border-color: rgba(252, 100, 20, 0.9);
      background-color: rgba(252, 100, 20, 0.9); }
  a.btn.btn-secondary {
    border-color: #ffae00;
    background-color: #ffae00; }
    a.btn.btn-secondary:hover, a.btn.btn-secondary:focus {
      border-color: rgba(255, 174, 0, 0.9);
      background-color: rgba(255, 174, 0, 0.9); }
  a.btn.full {
    width: 100%; }

.text-link-primary {
  color: #FF8400; }
  .text-link-primary:hover {
    text-decoration: none;
    color: rgba(255, 132, 0, 0.9); }

.btn.btn-primary {
  border-color: #FF8400;
  background-color: #FF8400; }
  .btn.btn-primary:hover, .btn.btn-primary:active, .btn.btn-primary:focus {
    border-color: rgba(255, 132, 0, 0.9) !important;
    background-color: rgba(255, 132, 0, 0.9) !important; }

.content {
  max-width: 600px;
  margin: 0 auto;
  overflow: hidden; }
  .content .content-header {
    background-color: #F5F7FC;
    padding: 2rem;
    background-image: url(<?=base_url()?>assets/img/media/store-vector@3x.png);
    background-repeat: no-repeat;
    background-size: 100%;
    background-position: center bottom;
    height: 60px; }
    .content .content-header .recipient {
      font-size: 1.1rem; }
  .content .content-body {
    background-color: #FAFBFD;
    position: relative; }
    .content .content-body:after {
      content: "";
      background-color: #FAFBFD;
      top: -10px;
      height: 67px;
      left: -25px;
      position: absolute;
      transform: rotate(-2deg);
      width: 650px;
      z-index: 1; }
  .content .content-footer {
    color: #909090; }
    .content .content-footer .nav-footer {
      text-align: center;
      padding: 2rem 1rem; }
      .content .content-footer .nav-footer .nav, .content .content-footer .nav-footer li {
        display: inline-block !important; }
      .content .content-footer .nav-footer li {
        margin: 1rem;
        color: #FF8400; }
  .content .main-content {
    position: relative;
    top: -40px; }
    .content .main-content .row {
      margin: 0; }
    .content .main-content .large-text {
      text-align: center;
      max-width: 400px;
      margin: 0 auto;
      margin-bottom: 3rem; }
      .content .main-content .large-text h2 {
        font-weight: 400; }
      .content .main-content .large-text .amount {
        color: #FF8400; }
    .content .main-content .item {
      padding: 1rem;
      border-top: 1px solid #F0F0F0; }
      .content .main-content .item.button {
        /*padding-top: 3rem;*/
        padding-bottom: 1rem; }
        .content .main-content .item.button .btn {
          margin-top: 0.75rem;
          /*margin-bottom: 0.75rem;*/
          min-width: 380px;
          min-height: 3rem; }
      .content .main-content .item label {
        color: #6C6C6C;
        font-size: 0.8rem;
        text-transform: uppercase;
        margin-bottom: 0.5rem; }
        .content .main-content .item label.big {
          color: #292929;
          font-size: inherit;
          text-transform: inherit; }
      .content .main-content .item .inner-item {
        margin-bottom: 1rem; }
        .content .main-content .item .inner-item:last-child {
          margin-bottom: 2rem; }
      .content .main-content .item .details {
        padding-top: 1rem; }
        .content .main-content .item .details .source-fund {
          color: #6C6C6C;
          font-size: 1rem; }
          .content .main-content .item .details .source-fund span {
            color: #FF8400;
            font-weight: 500; }
        .content .main-content .item .details .inner-item {
          border-bottom: 1px solid #F0F0F0;
          margin-bottom: 0.75rem; }
          .content .main-content .item .details .inner-item div {
            margin-bottom: 0.75rem; }
          .content .main-content .item .details .inner-item .table {
            margin-bottom: 0.25rem; }
            .content .main-content .item .details .inner-item .table:last-child {
              margin-bottom: 0.75rem; }
        .content .main-content .item .details .table {
          color: #6C6C6C; }
          .content .main-content .item .details .table div, .content .main-content .item .details .table .col {
            margin: 0;
            padding: 0; }
          .content .main-content .item .details .table .col:last-child {
            text-align: right; }
        .content .main-content .item .details .total-amount {
          font-weight: 500;
          text-align: right;
          color: #292929; }
        .content .main-content .item .details .amount-receive .total-amount {
          font-size: 1.4rem;
          font-weight: 500;
          text-align: right;
          color: #FF8400; }
    .content .main-content .transaction-id,
    .content .main-content .transaction-date {
      color: #6C6C6C;
      font-size: 0.8rem; }
    .content .main-content .transaction-date {
      text-align: right; }
    .content .main-content .message {
      font-size: 1.5rem;
      padding: 3rem 1rem;
      text-align: center;
      line-height: 2rem;
      max-width: 400px;
      margin: 0 auto; }
      .content .main-content .message .status {
        color: #FF8400; }
  .content .user-image {
    width: 7rem;
    height: 7rem;
    border-radius: 100%;
    overflow: hidden;
    text-align: center;
    margin: 0 auto;
    position: relative;
    z-index: 2;
    top: -60px;
    box-shadow: 0px 1px 6.44px 0.56px rgba(0, 0, 0, 0.17);
    background-color: #fff; }
    .content .user-image img {
      width: 100%;
      position: relative; }
  .content .logo {
    text-align: center;
    padding-top: 3rem !important;
    padding-bottom: 3rem !important; }
    .content .logo img {
      width: 60px; }

.no-border {
  border: none !important; }
  .no-border .table {
    margin-bottom: 2rem !important; }
.col, .col-1, .col-10, .col-11, .col-12, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-auto, .col-lg, .col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-lg-auto, .col-md, .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-auto, .col-sm, .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-auto, .col-xl, .col-xl-1, .col-xl-10, .col-xl-11, .col-xl-12, .col-xl-2, .col-xl-3, .col-xl-4, .col-xl-5, .col-xl-6, .col-xl-7, .col-xl-8, .col-xl-9, .col-xl-auto {
    position: relative;
    width: 100%;
    min-height: 1px;
    padding-right: 15px;
    padding-left: 15px;
}
.table {
    width: 100%;
    margin-bottom: 1rem;
    background-color: transparent;
}
label {
    display: inline-block;
    margin-bottom: .5rem;
}
*, ::after, ::before {
    box-sizing: border-box;
}
/*# sourceMappingURL=main.css.map */
		
	</style>
</head>
<body>
	
	<div class="content">
		<div class="content-header">
			<div class="recipient">Dear <span>{customer_name}</span>,</div>
		</div>

		<div class="content-body">
			<div class="user-image">
				<img src="<?=base_url()?>assets/img/media/suncash-icon.png" />
			</div>
			<div class="main-content">
				<div class="row">
					<div class="col large-text">
						{msg_above}
					</div>
				</div>
				<!-- <div class="row item table"> -->
        <div class="row item table"> <!--  class="row item table"-->
          <div class="col transaction-id" style="color: #6C6C6C !important; font-size: 0.8rem !important;">
						<span class="text-uppercase">Transaction ID:  {transaction_id}</span>
					</div>
          <div class="col transaction-date" style="color: #6C6C6C !important; font-size: 0.8rem !important; text-align: right !important;">
						<span>{creation_date}</span>
					</div>
				</div>
				<div class="row item">
					<div class="col">
						<div class="inner-item">
							<label>Reference/Invoice No.</label>
							<div>{reference_num}</div>
						</div>

						<div class="inner-item">
							<label>Notes</label>
							<div>{notes}.</div>
						</div>
					</div>
				</div>
<!-- 				<div class="row item">
					<div class="col details">
						<div class="inner-item no-border">
							<div class="row table amount-receive">
								<label class="big col">Amount you will receive:</label>
								<div class="total-amount col">{total} BSD</div>
							</div>
						</div>
					</div>
				</div> -->
				<div class="row item button">
					<div class="col details text-center" style="text-align: center !important;">
						<p style="margin:1px !important;">Please login to your Suncash Account to accept the payment.<br><br>
            <a href="<?=base_url()?>business" class="btn btn-primary" style=" border-color: #FF8400 !important; background-color: #FF8400 !important; color: white !important; text-decoration: none !important;">Login Here</a><br><br>
            </p>

        </div>

     		<div class="row item logo" style="padding:20px !important;">
					<div class="col">
						<img src="<?=base_url()?>assets/img/media/suncash-logo.png">
					</div>
				</div>

			</div>
		</div>

		<div class="content-footer">

			<div class="nav-footer" style="padding:0px !important;">
				<ul class="nav">
          <li><a class="nav-item" style="color:#ff8400 !important; text-decoration: none !important;" href="<?=base_url()?>">About</a></li>
          <li><a class="nav-item" style="color:#ff8400 !important ; text-decoration: none !important;"  href="<?=base_url()?>info/terms">Terms</a></li>
          <li><a class="nav-item" style="color:#ff8400 !important; text-decoration: none !important;"   href="<?=base_url()?>info/contacts">Contact</a></li>
          <li><a class="nav-item" style="color:#ff8400 !important; text-decoration: none !important;"  href="<?=base_url()?>info/faq">FAQ</a></li>
				</ul>
			</div>

			<p>Suncash is committed to preventing fraudulent emails. Emails from Suncash will always contain your full name.</p>
			<p>Please don't reply to this email. To get in touch with us, click <a style="color:#ff8400 !important; text-decoration: none !important;"  href="<?=base_url()?>info/contacts" class="text-link-primary">Help & Contact.</a></p>
			<p>Copyright © <?=date('Y')?> Suncash. All rights reserved.</p>
		</div>
	</div>
	

</body>
</html>