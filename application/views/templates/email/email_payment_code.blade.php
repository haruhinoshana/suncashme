<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Merchant Email Incoming-payment</title>
	<link rel="stylesheet" href="./assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="./assets/css/main.css">
	<link rel="stylesheet" href="./assets/css/all.min.css">
	<link rel="stylesheet" href="./assets/css/responsive.css">
<!-- 	<link rel="stylesheet" href="./assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="./assets/css/main.css"> -->
	<!-- <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet"> -->
	<!-- <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet"> -->
	<style>
		.content .main-content .large-text h2,body{font-weight:400}body{-webkit-font-smoothing:antialiased!important;font-family:'Roboto',sans-serif;font-size:15px;color:#292929;line-height:1.5em;padding:3rem}
		.text-link-primary{color:#FF8400}
		.text-link-primary:hover{text-decoration:none;color:rgba(255,132,0,.9)}
		.btn.btn-primary{border-color:#FF8400;background-color:#FF8400}
		.btn.btn-primary:active,
		.btn.btn-primary:focus,
		.btn.btn-primary:hover{border-color:rgba(255,132,0,.9)!important;background-color:rgba(255,132,0,.9)!important}
		.content{max-width:600px;margin:0 auto;overflow:hidden}
		.content .content-header{background-color:#F5F7FC;padding:2rem;background-image:url(./assets/img/media/store-vector@3x.png);background-repeat:no-repeat;background-size:100%;background-position:center bottom;height:200px}
		.content .content-header .recipient{font-size:1.1rem}
		.content .content-body{background-color:#FAFBFD;position:relative}
		.content .content-body:after{content:"";background-color:#FAFBFD;top:-10px;height:67px;left:-25px;position:absolute;transform:rotate(-2deg);width:650px;z-index:1}
		.content .content-footer{color:#909090}
		.content .content-footer .nav-footer{text-align:center;padding:2rem 1rem}
		.content .content-footer .nav-footer .nav,.content .content-footer .nav-footer li{display:inline-block!important}
		.content .content-footer .nav-footer li{margin:1rem;color:#FF8400}
		.content .main-content{position:relative;top:-40px}
		.content .main-content .row{margin:0}.content .main-content .large-text{text-align:center;max-width:400px;margin:0 auto 3rem}.content .main-content .large-text .amount{color:#FF8400}
		.content .main-content .item{padding:1rem;border-top:1px solid #F0F0F0}
		.content .main-content .item.button{padding-top:3rem;padding-bottom:3rem}
		.content .main-content .item.button .btn{margin-top:.75rem;margin-bottom:.75rem;min-width:380px;min-height:3rem}
		.content .main-content .item label{color:#6C6C6C;font-size:.8rem;text-transform:uppercase;margin-bottom:.5rem}
		.content .main-content .item label.big{color:#292929;font-size:inherit;text-transform:inherit}
		.content .main-content .item .inner-item{margin-bottom:1rem}
		.content .main-content .item .inner-item:last-child{margin-bottom:2rem}
		.content .main-content .item .details{padding-top:1rem}
		.content .main-content .item .details .source-fund{color:#6C6C6C;font-size:1rem}
		.content .main-content .item .details .source-fund span{color:#FF8400;font-weight:500}
		.content .main-content .item .details .inner-item{border-bottom:1px solid #F0F0F0;margin-bottom:.75rem}
		.content .main-content .item .details .inner-item div{margin-bottom:.75rem}
		.content .main-content .item .details .inner-item .table{margin-bottom:.25rem}
		.content .main-content .item .details .inner-item .table:last-child{margin-bottom:.75rem}
		.content .main-content .item .details .table{color:#6C6C6C}
		.content .main-content .item .details .table .col,
		.content .main-content .item .details .table div{margin:0;padding:0}
		.content .main-content .item .details .table .col:last-child{text-align:right}
		.content .main-content .item .details .total-amount{font-weight:500;text-align:right;color:#292929}
		.content .main-content .item .details .amount-receive .total-amount{font-size:1.4rem;font-weight:500;text-align:right;color:#FF8400}.content .main-content .transaction-date,
		.content .main-content .transaction-id{color:#6C6C6C;font-size:.8rem}
		.content .main-content .transaction-date{text-align:right}
		.content .main-content .message{font-size:1.5rem;padding:3rem 1rem;text-align:center;line-height:2rem;max-width:400px;margin:0 auto}
		.content .main-content .message 
		.status{color:#FF8400}
		.content .user-image{width:7rem;height:7rem;border-radius:100%;overflow:hidden;text-align:center;margin:0 auto;position:relative;z-index:2;top:-60px;box-shadow:0 1px 6.44px .56px rgba(0,0,0,.17);background-color:#fff}
		.content .user-image img{width:100%;position:relative}
		.content .logo{text-align:center;padding-top:3rem!important;padding-bottom:3rem!important}
		.content .logo img{width:180px}
		.no-border{border:none!important}
		.no-border .table{margin-bottom:2rem!important}


.qr-code-image {
    width: 13rem;
    height: 13rem;
    overflow: hidden;
    text-align: center;
    margin: 0 auto;
    position: relative;
    z-index: 1;
    top: -100px;
    box-shadow: 0px 1px 6.44px 0.56px rgba(0, 0, 0, 0.17);
}
.qr-details .message {
    font-size: 1.2rem;
}
.qr-details {
	text-align: center;
    position: relative;
    top: -60px;
    margin-bottom: 1.5rem;
}
.qr-details .message .code {
    color: #FF8400;
    font-weight: 500;
    font-size: 2rem;
    margin-top: 0.75rem;
}
.qr-details .barcode {
    width: 200px;
    margin-top: 1.5rem;
    margin-bottom: 3rem;
}
.transaction-details .checkbox-container ~ input {
  margin-left: 35px;
  width: calc(100% - 35px);
  margin-bottom: 1rem !important; }
.transaction-details .transaction-details-summary {
  color: #6C6C6C;
  font-size: 0.9rem; }

.text-primary {
    color: #FF8400 !important;
}
.transaction-details-summary{
    font-family: "Roboto", sans-serif !important;
}
.text-center {
    text-align: center!important;
}
.user-card .header {
    padding: 2rem;
    background-image: url(<?=base_url()?>assets/img/media/store-vector@3x.png);
    background-repeat: no-repeat;
    background-size: 110%;
    background-color: #f5f7fc;
    background-position: center bottom;
}
.user-card .body {
    position: relative;
    padding: 0 3rem 3rem 3rem;
}
.user-card .body .total .amount {
	font-weight: 500;
	font-size: 1.1rem; }
.user-card .body .item {
	margin-bottom: 1rem;
	margin-top: 1rem; }
.user-card .body .item:not(:last-child) {
	border-bottom: 1px solid #F0F0F0;
	padding-bottom: 1rem; }
.user-card .body .item.primary-border {
	border-bottom: 1px solid #FF8400; }
.body:before {
    content: "";
    background-color: #fff;
    top: -20px;
    height: 67px;
    left: 0;
    position: absolute;
    transform: rotate(-2deg);
    width: 110%;
    z-index: 1;
}
.user-card.message-card, .user-card.transaction-details {
    font-family: "Roboto", sans-serif !important; }
.transaction-details .checkbox-container ~ input {
  margin-left: 35px;
  width: calc(100% - 35px);
  margin-bottom: 1rem !important; }
.transaction-details .transaction-details-summary {
  color: #6C6C6C;
  font-size: 0.9rem; }
.modal .row {
  margin: 0; 
}
.table {
    width: 100%;
    margin-bottom: 1rem;
    background-color: transparent;
    border-collapse: collapse;
}
  .user-card .header {
    padding: 2rem;
    background-image: url(<?=base_url()?>assets/img/media/store-vector@3x.png);
    background-repeat: no-repeat;
    background-size: 110%;
    background-color: #f5f7fc;
    background-position: center bottom; }
    .user-card .header.w-content {
      height: 1rem; }
.container {
    width: 100%;
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;

    margin-left: auto;
    max-width: 540px;
}

.user-card {
    min-width: 100%;
    max-width: 350px;
    background-color: #fff;
    text-align: center;
    box-shadow: 0px 1px 3.68px 0.32px rgba(0, 0, 0, 0.13);
    margin-top: 4rem;
    overflow: hidden;

}
	</style>
</head>
<body>
	<div class="transaction-details">
		<div class="container  text-center">
			<div class="row d-flex justify-content-center align-items-center mb-5">
				<div class="user-card transaction-details">
					<div class="header w-content"></div>
						<div class="body">
							<div class="qr-code-image">
								<img src="cid:qr_code" >
							</div>
							<div class="qr-details">
								<div class="message">Your Payment Code <div class="code">{transaction_id}</div></div>
								<img src="cid:barcode" class="barcode">

								<p>Please visit the nearest <br>Suncash Store or Kiosk to pay.</p>
				<!-- 				<button class="btn btn-primary mb-5" data-toggle="modal" data-target="#storeLocator">Store Locator</button> -->

								<div class="contact ">We also sent this code to your <br>mobile number (<span class="text-primary">{mobile}</span>) and your <br>email (<span class="text-primary" stye="  color: #FF8400 !important;">{email}</span>). </div>
							</div>

							<div class="transaction-details-summary">
							  <div class="label text-center"><h3><b>Transaction Details</b></h3></div>
							  <table class="table" width="100%" >
							  	<tbody>
							  		<tr>
							  			<td style="text-align:left ;">Transaction ID: {transaction_id}</td>
							  			<td style="text-align:right ;">{creation_date}</td>
							  		</tr>
							  		<tr>
							  			<td style="text-align:left ;">Payment Method</td>
							  			<td style="text-align:right ;">Cash</td>
							  		</tr>	
							  		<tr>
							  			<td style="text-align:left ;">Amount</td>
							  			<td style="text-align:right ;">{amount}</td>
							  		</tr>	
							  		<tr>
							  			<td style="text-align:left ;">Fee</td>
							  			<td style="text-align:right ;">{fee}</td>
							  		</tr>							  								  		
							  		<tr>
							  			<td style="text-align:left ;">VAT</td>
							  			<td style="text-align:right ;">{vat}</td>
							  		</tr>
							  									  			
							  		<tr>
							  			<td style="text-align:left ;">Total Payment</td>
							  			<td style="text-align:right ;"><b>{total_amount} BSD</b></td>
							  		</tr>				  			  		
							  	</tbody>
							  </table>
							</div>
<!-- 						<div class="transaction-details-summary">
						  <div class="label">Transaction Details</div>
							<div class="item">
							  <div class="row">
								  <div class="col-12 col-lg-6 text-left">Transaction ID: 23nfuuueyui</div>
								  <div class="col-12 col-lg-6 text-right">05 Nov 2018</div>
							  </div>
							</div>
							<div class="item">
							  <div class="row">
								  <div class="col-12 col-lg-6 text-left">Payment Method</div>
								  <div class="col-12 col-lg-6 text-right">Cash</div>
							  </div>
							</div>
							<div class="item total">
							  <div class="row">
							  	<div class="col label text-left">Total Payment</div>
							  	<div class="col text-right amount">27.00 BSD</div>
							  </div>
						  </div>
						</div> -->
						</div>
				</div>
			</div>
		</div>
	</div>

	
</body>
</html>