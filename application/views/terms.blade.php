<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>SunCash.Me</title>
	<link rel="stylesheet" href="{{base_url('assets_main/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/main.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/responsive.css')}}">
</head>
<body>
	<nav class="navbar navbar-expand-lg sticky-top bg-white shadow-sm p-0">
	  <div class="container p-0" role="navigation">
	  	<a class="navbar-brand" href="{{base_url('')}}"><img src="{{base_url('assets_main/imgs/header-logo@2x.png')}}" alt="Suncash Logo" class="d-inline-block align-top" width="200"></a>
	  	<a href="#" class="menu-button"><span></span></a>
	  	<div class="justify-content-end main-menu">
				<ul class="navbar-nav d-flex">
		      <li class="nav-item">
		        <a class="nav-link" href="{{base_url('')}}">About</a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link " href="{{base_url('info/faq')}}">FAQ </a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link active" href="{{base_url('info/terms')}}">Terms</a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="{{base_url('info/contacts')}}">Contact Us</a>
		      </li>
		      <li class="nav-item button-item">
		        <a class="nav-link btn-link btn-primary" href="http://dev.suncash.me/business/">For Business</a>
		      </li>
		      <li class="nav-item button-item">
		        <a class="nav-link btn-link btn-secondary" href="http://dev.suncash.me/customer/">For Customers</a>
		      </li>
		    </ul>
		  </div>
	  </div>
	</nav>

	<section>
		<div class="container">
			<div class="row">
				<div class="page-title">
					<h2 class="dark">Terms & Condition</h2>
					<hr>
				</div>
			</div>
		</div>
	</section>

	<section class="content-block terms">
		<div class="container">
			<div class="card">
				<p>The Suncash.Me service (“Service”) is provided to you by Suncash Inc. (“Suncash”), and allows you to create a personalized Suncash.Me URL (”PPM Link”), which allows you to create a personalized Suncash.Me page that will allow others to find, recognize and transact with you (each a “PPM Page”). Your PPM Page will display your PPM Link, name or business name, profile picture or logo, and the city associated with your Suncash account (“PPM Details”). Your PPM Details will be visible to anyone visiting your PPM Page so that they can more easily verify that they are sending money to the right recipient.</p>

				<p>The Suncash.Me service (“Service”) is provided to you by Suncash Inc. (“Suncash”), and allows you to create a personalized Suncash.Me URL (”PPM Link”), which allows you to create a personalized Suncash.Me page that will allow others to find, recognize and transact with you (each a “PPM Page”). Your PPM Page will display your PPM Link, name or business name, profile picture or logo, and the city associated with your Suncash account (“PPM Details”). Your PPM Details will be visible to anyone visiting your PPM Page so that they can more easily verify that they are sending money to the right recipient.</p>

				<p>Suncash.Me is a social feature that enables you to transact with peers more easily. Other Suncash users may be shown your PPM Link if they sync their address book and you are listed among their contacts, or if they attempt to send you money within Suncash by searching for your PPM Details or PPM Link.</p>

				<p>WHEN YOU SIGN UP TO THE SERVICE, YOU AUTHORIZE Suncash TO PUBLICLY DISPLAY YOUR PPM DETAILS, PPM LINK(S) AND PPM PAGE(S) AND UNDERSTAND THAT THEY WILL BE VISIBLE TO ANYONE ACCESSING THE CORRESPONDING PPM DETAILS.</p>

				<p>You can update your preferences and turn off your PPM Link at any time by adjusting the settings in your Suncash account online or in the Suncash App. If you turn off your PPM Link, your PPM Link, PPM Page and PPM Details will no longer be publicly findable and you cannot use your PPM Link to receive funds through Suncash. Personal or Commercial Purposes</p>

				<p>When you sign up for the Service and set up a PPM Link, you will be asked to specify whether the primary use of your PPM Link will be transacting with friends and family (“Personal” use) or receiving payments from customers for goods and services (“Commerical” use). While you can reclassify a Personal PPM Link for Commercial use, and may be able to reclassify certain Commercial PPM Links as Personal, YOU MAY NOT USE A PERSONAL PPM LINK TO RECEIVE PAYMENTS FOR COMMERCIAL PURPOSES, NOR MAY YOU USE A COMMERCIAL PPM LINK FOR PERSONAL TRANSACTIONS.</p>

				<p>If you have a Suncash Business Account, any and all PPM Links associated with that account will be identified as Commercial PPM Links. If Suncash determines, in its sole discretion, that you have received payments for goods and services on a Personal PPM Link, Suncash may retroactively charge you the appropriate fees and prospectively reclassify the PPM Link as Commerical. Please review the Suncash User Agreement for information on how fees are assessed. Suncash.Me User Guidelines</p>

				<p>In addition to complying with the Agreements, we’ve established a few ground rules for you to follow when using the Service.</p>

				<p>You MAY NOT provide your Suncash password or other credentials to any other person to enable such other party to manage your PPM Link(s), nor may you use any other account holder’s username and password to do the same.</p>

				<p>In addition, you MAY NOT ENGAGE in any activity, post any content, or register and/or use a URL to establish a PPM Link, which is, or includes material that:</p>
			</div>
		</div>
	</section>

	<section class="cta-banner">
		<div class="container text-center">
			<div class="row">
				<div class="col-12">
					<div class="banner-text-container">
						<h1 class="banner-header">Start using SunCash.Me today.</h1>
						<p>SunCash.Me is the fastest, safest and simple way for your family, friends or customers to pay.</p>
						<a href="#" class="btn btn-secondary">Create Your SunCash.Me Link</a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<footer class="py-3">
		<div class="container">
			<div class="row">
				<div class="col-12 col-lg-6 d-flex align-self-center text-left">
					<span>© 2016 SunCash.Me. All Rights Reserved.</span>
				</div>
				<div class="col-12 col-lg-6 text-right">
					<img src="{{base_url('assets_main/imgs/footer-logo.png')}}">
				</div>
			</div>
		</div>
	</footer>

	<script src="{{base_url('assets_main/js/jquery-3.2.1.min.js')}}"></script>
	<script src="{{base_url('assets_main/js/bootstrap.min.js')}}"></script>
	<script src="{{base_url('assets_main/js/main.js')}}"></script>
</body>
</html>