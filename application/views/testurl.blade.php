<?php
//loading assets helper
defined('BASEPATH') OR exit('No direct script access allowed');
$assetHelper = new AssetHelper\AssetHelper();
$ci=&get_instance();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Save/Link Card</title>
	<link rel="stylesheet" href="{{base_url('assets_main/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/main.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/main_checkout.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/responsive.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/all.min.css')}}">
	<link rel="stylesheet" href="{{base_url('assets/plugins/global/global2.css')}}">
	<link href="https://unpkg.com/filepond/dist/filepond.css" rel="stylesheet">
	<link href="https://unpkg.com/filepond/dist/filepond.min.css" rel="stylesheet">
	<link href="https://unpkg.com/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.min.css" rel="stylesheet">	
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>assets/plugins/icon.ico">

</head>
<body>

	<div class="container-fluid">
		<div class="row">
 				<!-- <h4 class="mb-3">Payment</h4> -->
				<div class="d-block my-3">
					<iframe src="{{base_url('merchant_checkout/checkoutv2/YTE1ZDQxMWJkYmRmZmZhNDZjNTBjYjU3OTU4YmQ2OGZlZjM1ZDVlYWNiZmNhYmEwZGMyMWE4Y2FmMDNhYzk5NXx8MTU5NjY0MzAwMXx8Mg==')}}" title="Checkoutv2" style="border:none;width: 153%;height: 501%;"></iframe>
				</div>

		</div>
	</div>

</body>
</html>