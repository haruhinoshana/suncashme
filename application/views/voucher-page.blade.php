<?php
//loading assets helper
defined('BASEPATH') OR exit('No direct script access allowed');
$assetHelper = new AssetHelper\AssetHelper();
$ci=&get_instance();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>SunCash.Me</title>
	<link rel="stylesheet" href="{{base_url('assets_main/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/main.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/main_checkout.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/responsive.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/all.min.css')}}">
	<link rel="stylesheet" href="{{base_url('assets/plugins/global/global2.css')}}">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>assets/plugins/icon.ico">

</head>
<body>
	<nav class="navbar navbar-expand-lg sticky-top bg-white shadow-sm p-0">
	  <div class="container p-0" role="navigation">
	  	<a class="navbar-brand" href="<?php echo base_url();?>"><img src="{{base_url('assets_main/imgs/header-logo@2x.png')}}" alt="Suncash Logo" class="d-inline-block align-top" width="160"></a>
	  	<a href="#" class="menu-button"></a>
	  	<div class="justify-content-end main-menu">
				<ul class="navbar-nav d-flex align-items-center">


		    </ul>
		  </div>
	  </div>
	</nav>
	<section class="" id="voucher_section">
		<div class="container  text-center">
			<div class="row d-flex justify-content-center align-items-center mb-5">
				<div class="user-card transaction-details" style="margin-top:1rem;margin-bottom:-34px !important;">
					<div class="header w-content" style="height:6rem !important;"></div>
					<div class="body">
						<form id="suncash_voucher_form" method="POST">
				        <div class="user-image ">
								@if(!empty($_SESSION['profile_pic']))
								<img src="{{$_SESSION['profile_pic']}}">
								@else
								<img src="{{base_url('assets_main/imgs/suncash-icon.png')}}">
								@endif
						</div>
						<div class="details" style="margin-bottom: -40px;">

							<div class="message">You're about to pay <?=ucfirst($_SESSION['suntag_shortcode']) ?></div>
							<select name="payment_method" id="payment_method" >
								<option value="">BSD</option>
							</select>
							<input class="input-amount" type="text" id="amount" name="amount" value="{{$payment_data['amount']}}" style="height: 100% !important">
						</div>
						<div class="text-left">
							<div class="item">
								<input type="hidden" class="form-control" id="reference_num" name="reference_num"  value="{{$payment_data['reference_num']}}" placeholder="Reference # (Order, Quote, Invoice or Account)">
								<div class="form-group">
						          <input type="hidden" class="form-control" id="name" name="name" placeholder="Name" autocomplete="off" style="resize:none" value="{{$payment_data['name']}}" >
								</div>		
								<div class="form-group">
						          <input type="hidden" class="form-control" id="email" name="email" placeholder="Email" autocomplete="off" style="resize:none" value="{{$payment_data['email']}}" >
								</div>		
								<div class="form-group">
						          <input type="hidden" class="form-control" id="mobile" name="mobile" placeholder="mobile" autocomplete="off" style="resize:none" value="{{$payment_data['mobile']}}" >
								</div>																
							  	<input type="hidden"  class="form-control" id="notes" name="notes" placeholder="Note to Business" value="{{$payment_data['notes']}}">
							</div>
							
								<div class="item primary-border" >

									<?php 
										if($_SESSION['tag']=='MERCHANT' && $_SESSION['registration_type']!='Charity'){
											$tfees = number_format($fee_data['fee'], 2, '.', '')+$fee_data['vat_charge'];
										}else{
											$tfees = "0.00";
										}
									?>

								  	<div class="label text-center">Transaction Details:</div>
										<div class="item" style="margin-bottom: 0px !important;margin-top: 0px !important;">
										  <div class="row text-details">
											  	<div class="col">Principal</div>
											  	<div class="col text-right ">$ <span class="amount_val">{{$payment_data['amount']}}</span></div>
										  </div> 
										</div> 
										@if($_SESSION['tag']=='MERCHANT' && $_SESSION['registration_type']!='Charity')
										<div class="item" style="margin-bottom: 0px !important;margin-top: 0px !important;">
										  <div class="row text-details">
										  	<div class="col">Transaction Fee</div>

										    <div class="col text-right">$ <span class="fee_val">{{$tfees}}</span></div>
										  </div>
										</div>		
										@endif																				
									</div>						

									<div class="item total">
									  <div class="row">
									  
									  	<?php 
										 //dd($_SESSION['tag']);
										  	if($_SESSION['tag']=='MERCHANT' && $_SESSION['registration_type']!='Charity'){
												$total = str_replace( ',', '', $payment_data['amount'])+$fee_data['fee']+$fee_data['vat_charge'];
											}else{
												$total = str_replace( ',', '', $payment_data['amount']);
											}
									  	?>

									  	<div class="col label">Total Due</div>
									  	<div class="col text-right amount"><span class="amount_total">{{$total}}</span> BSD</div>
									  </div>
									</div>

									<div id="card_info"></div><br>
										<h3>Enter your voucher details</h3>
										<div>
											<!-- <form id="suncash_voucher_form" class="text-left"> -->
													<!-- <div class="form-group">
														<label for="mobile">Mobile Number</label>
														<div class="form-div">
															<input type="text" class="form-control bfh-phone" id="voucher_mobile_number" name="voucher_mobile_number" data-format="1 (ddd) ddd-dddd" value ="1 242" required >
														</div>
													</div> -->
													<div class="form-group">
														<label for="voucher_number">Voucher Number</label>
														<div class="form-div">
														<input type="text" class="form-control required" id="voucher_number" name="voucher_number" placeholder="Enter your voucher number" autocomplete="off" required>
														</div>
													</div>
													<div class="form-group">
														<label for="pin_voucher">PIN</label>
														<div class="form-div">
														<input type="password" class="form-control required" id="voucher_pin" name="voucher_pin" placeholder="Enter your pin" autocomplete="off" maxlength="5" required>
														</div>
													</div>
													@if($_SESSION['tag']=='MERCHANT' && $_SESSION['registration_type']!='Charity')
													<div class="text-center"><button type="submit" id="voucher_process"  class="btn btn-orange full">Process Payment</button></div>
													@else
													<div class="text-center"><button type="submit" id="voucher_process"  class="btn btn-orange full">Donate</button></div>
													@endif
											<!-- </form> -->
											<br>
											Note: This system does not give change for excess voucher amount.
										</div>
									</div>									
							</div>


						</form>
					</div>
				</div>				
			</div>
		</div>
		<footer class="py-3 footer">
			<div class="container">
				<div class="row">
					<div class="col-12 col-lg-6">
						<span class="txt-shit">© {{date('Y')}} SunCash.Me. All Rights Reserved.</span>
					</div>
					<div class="col-12 col-lg-6 ">
						<img class="img-x" src="{{base_url('assets_main/imgs/footer-logo.png')}}">
					</div>
				</div>
			</div>
		</footer>		
	</section>


	<section class="full" id="success_section_transaction" style=" display:none ;">
		<div class="container  text-center">
			<div class="row d-flex justify-content-center align-items-center mb-5">
				<div class="user-card success-card" style="margin-top:1rem;margin-bottom:60px !important;">
					<div class="header w-content" style="margin-top:1rem;margin-bottom:-34px !important;"></div>
					<div class="body">
						<div class="user-image">
							<img src="{{base_url('assets_main/imgs/success-icon.png')}}">
						</div>
						<div class="details" id = "transaction-details">
							<div class="message" style="font-size: 2rem;line-height: 2.1rem;width: 90%;margin: 0 auto;">You Paid <span class="amount_val" style="color: #FF8400;">${{$payment_data['amount']}}</span> to <?=ucfirst($_SESSION['suntag_shortcode'])?></div>
						</div>
						<!-- <form action="" class="text-left"> -->
						<div class="text-left">

					  	<div class="label text-center">Transaction Details</div>
						  <div class="item">
							  <div class="row">
							  	<div class="col">Transaction ID: <b><span id="transaction_id"></span></b></div>
							  	<div class="col text-right">{{date("d M Y")}}</div>
							  </div>
							</div>
							<div class="item">
							  <div class="row">
							  	<div class="col">Payment Method</div>
							  	<div class="col text-right">Voucher Payment</div>
							  </div>
							</div>
							<div class="item">
							  <div class="row">
							  	<div class="col">Principal</div>
							  	<div class="col text-right">$ <span class="amount_val">{{$payment_data['amount']}}</span></div>
							  </div>
							</div>		
							@if($_SESSION['tag']=='MERCHANT' && $_SESSION['registration_type']!='Charity')						
							<div class="item">
							  <div class="row">
							  	<div class="col">Transaction Fee</div>
							  	<div class="col text-right">$ <span class="fee_val">{{number_format($tfees, 2, '.', '')}}</span></div>
							  </div>
							</div>
							@endif														
						  <div class="item total">
							  <div class="row">
							  	<div class="col label">Total Due</div>
							  	<div class="col text-right amount"><span class="amount_total">{{$total}}</span> BSD</div>
							  	<input type="hidden" id="amount_total" name="amount_total" value="{{$total}}" />
							  	<input type="hidden" id="hid_fee" name="hid_fee" value="{{number_format($tfees, 2, '.', '')}}" />
							  	<input type="hidden" id="hid_vat" name="hid_vat" value="{{$fee_data['vat_charge']}}" />
							  	<input type="hidden" id="hid_totalfee" name="hid_totalfee" value="0.00" />							  	
							  </div>
						  </div>
						  
						</div>
						<!-- </form> -->

					</div>
				</div>
			</div>
		</div>
	</section>
	</div><!-- end -->


	<script src="{{base_url('assets_main/js/jquery-3.2.1.min.js')}}"></script>
	<script src="{{base_url('assets_main/js/bootstrap.min.js')}}"></script>

	{!!$assetHelper->link_plugins('js','sidebar-nav/dist/sidebar-nav.min.js')!!}
	{!!$assetHelper->link_plugins('js','override/jquery.slimscroll.js')!!}
	{!!$assetHelper->link_plugins('js','override/waves.js')!!}
	{!!$assetHelper->link_plugins('js','override/custom.min.js')!!}
	{!!$assetHelper->link_plugins('js','sweetalert2-master/dist/sweetalert2.all.min.js')!!}
    {!!$assetHelper->link_plugins('js','Inputmask-5.x/dist/jquery.inputmask.min.js')!!}
    {!!$assetHelper->link_plugins('js','bootstrap-formhelpers-phone/bootstrap-formhelpers-phone.js') !!}
    {!!$assetHelper->link_plugins('js','bootstrap-formhelpers-phone/bootstrap-formhelpers-phone.format.js') !!}
	{!!$assetHelper->link_plugins('js','global/global.js')!!}	
	<script src="{{base_url('assets_main/js/main.js')}}"></script>
	<script  type="text/javascript" charset="utf-8">


	function getcfee(amount){
		//e.preventDefault();
		//e.stopImmediatePropagation();				
	    // accordion is open
	    var total_billpay_fees = parseFloat($("#amount").val().replace(/,/g, ''))+parseFloat($("#hid_fee").val())+parseFloat($("#hid_vat").val());
	    //alert($("#amount").val()+"+"+$("#hid_fee").val()+"+"+$("#hid_vat").val());
	    $.ajax({
	      url: '{{base_url("payment/get_fees_payment")}}',
	      type: 'POST',
	      dataType: 'json',
	      data:{amount:total_billpay_fees,client_id:'{{$_SESSION["client_record_id"]}}'},
	   	  beforeSend:function(xhr, textStatus) {
	        //called when complete
		    //loader.showPleaseWait();
	        $("#submit").prop('disabled', true);
	      },		  
		  complete: function(xhr, textStatus) {
		    //called when complete
		  	//loader.hidePleaseWait();
		  },
	      success: function(data, textStatus, xhr) {
	        //called when successful
	        //paste values
	        if(data.success){
	        	$(".conviniecefee_val").text(data.fee);
	        	//$("#amount").text(data.total);
				var total= total_billpay_fees+parseFloat(data.fee);
				$(".amount_total").text(total.toFixed(2));
				$("#amount_total").val(total.toFixed(2));
				$("#hid_pfee").val(data.pf);
				$("#hid_tfee").val(data.tf);
				$("#hid_totalfee").val(data.fee);

		    	$("#submit").prop('disabled', false);
	        }  else {
	        	swal(data.msg);
		    	$("#submit").prop('disabled', false);
	        }

	      },
	      error: function(xhr, textStatus, errorThrown) {
	        //called when there is an error
        	swal(data.msg);
	    	$("#submit").prop('disabled', false);
	      }
	    });
	}

	$("#amount").change(function(){

		if($("#amount").val()<=0.00){
		  	swal("Amount is required.");

		  	return false;
		}

		$.ajax({
		  url: '<?=site_url("payment/process_fee")?>',
		  type: 'POST',
		  dataType: 'json',
		  // data: {amount: $("#amount").val()},
		  data: {amount: parseFloat($("#amount").val().replace(/,/g, ''))},
	   	  beforeSend:function(xhr, textStatus) {
	        //called when complete
		    loader.showPleaseWait();
	        $("#submit").prop('disabled', true);
	      },		  
		  complete: function(xhr, textStatus) {
		    //called when complete
		  loader.hidePleaseWait();
		  },
		  success: function(data, textStatus, xhr) {
		    if(data.success){
		    	//console.log(data.fee_data.fee);
				@if($_SESSION['tag']=='MERCHANT' && $_SESSION['registration_type']!='Charity')
					$(".amount_val").text($("#amount").val());
					$(".fee_val").text(data.fee_data.fee);
					$(".vat_val").text(data.fee_data.vat_charge);
					// var total = parseFloat($("#amount").val())+parseFloat(data.fee_data.fee)+parseFloat(data.fee_data.vat_charge);
					var total = parseFloat($("#amount").val().replace(/,/g, ''))+parseFloat(data.fee_data.fee)+parseFloat(data.fee_data.vat_charge);
					$(".amount_total").text(total.toFixed(2));
					$("#amount_total").val(total.toFixed(2));
					$("#hid_fee").val(data.fee_data.fee);
					$("#hid_vat").val(data.fee_data.vat_charge);
				@endif

				@if($_SESSION['tag']=='CUSTOMER' || $_SESSION['registration_type']=='Charity')
					$(".amount_val").text($("#amount").val());
					$(".fee_val").text("0.00");
					$(".vat_val").text("0.00")
					// var total = parseFloat($("#amount").val())+parseFloat(data.fee_data.fee)+parseFloat(data.fee_data.vat_charge);
					var total = parseFloat($("#amount").val().replace(/,/g, ''));
					$(".amount_total").text(total.toFixed(2));
					$("#amount_total").val(total.toFixed(2));
					$("#hid_fee").val(0.00);
					$("#hid_vat").val(0.00);
				@endif
				// getcfee($("#amount").val());
				// @if($_SESSION['tag']=='MERCHANT')
				// getcfee($("#amount").val());
				// @endif
				// @if($_SESSION['tag']=='CUSTOMER')
				// getcfee_customer($("#amount").val());
				// @endif
				$("#submit").prop('disabled', false);
		    } else {
		    	swal(data.msg);
		    	$("#submit").prop('disabled', false);
		    }
		  },
		  error: function(xhr, textStatus, errorThrown) {
		    //called when there is an error
		   	swal(data.msg);
		    $("#submit").prop('disabled', false);

		  }
		});

	});

	function getcfee_customer(amount){
		//e.preventDefault();
		//e.stopImmediatePropagation();				
	    // accordion is open
	    var total_billpay_fees = parseFloat($("#amount").val().replace(/,/g, ''))+parseFloat($("#hid_fee").val())+parseFloat($("#hid_vat").val());
	    //alert($("#amount").val()+"+"+$("#hid_fee").val()+"+"+$("#hid_vat").val());
	    $.ajax({
	      url: '{{base_url("payment/get_fees_customer")}}',
	      type: 'POST',
	      dataType: 'json',
	      data:{amount:total_billpay_fees,client_id:'{{$_SESSION["client_record_id"]}}'},
	   	  beforeSend:function(xhr, textStatus) {
	        //called when complete
		    //loader.showPleaseWait();
	        $("#submit").prop('disabled', true);
	      },		  
		  complete: function(xhr, textStatus) {
		    //called when complete
		  	//loader.hidePleaseWait();
		  },
	      success: function(data, textStatus, xhr) {
	        //called when successful
	        //paste values
	        if(data.success){
	        	$(".conviniecefee_val").text(data.fee_data.fee);
	        	//$("#amount").text(data.total);
				var total= total_billpay_fees+parseFloat(data.fee_data.fee);
				$(".amount_total").text(total.toFixed(2));
				$("#amount_total").val(total.toFixed(2));
				$("#hid_pfee").val(data.fee_data.pf);
				$("#hid_tfee").val(data.fee_data.tf);
				$("#hid_totalfee").val(data.fee_data.fee);

		    	$("#submit").prop('disabled', false);
	        }  else {
	        	swal(data.msg);
		    	$("#submit").prop('disabled', false);
	        }

	      },
	      error: function(xhr, textStatus, errorThrown) {
	        //called when there is an error
        	swal(data.msg);
	    	$("#submit").prop('disabled', false);
	      }
	    });
	}
	$("#suncash_voucher_form").submit(function(){
	var form_data ={
		voucher_mobile_number:$("#voucher_mobile_number").val(),
		voucher_number:$("#voucher_number").val(),
		voucher_pin:$("#voucher_pin").val(),
		notes:$("#notes").val(),
		reference_num:$("#reference_num").val(),
		name:$("#name").val(),
    	email:$("#email").val(),
    	mobile:$("#mobile").val(),
		amount:$("#amount").val(),
		total_amount:$("#amount_total").val(),
		hid_fee:$("#hid_fee").val(),
		hid_vat:$("#hid_vat").val(),
		}   
		$.ajax({
		url: '{{base_url("payment/process_suncashme_voucher_payment")}}',
		type: 'POST',
		dataType: 'json',
		data: form_data,
		beforeSend:function(){
			loader2.showPleaseWait();
			$("#voucher_card_next").prop("disabled",true);
			$("#voucher_card_next").text("Processing... Please wait");
			//loader.showPleaseWait();
		},
		complete: function(xhr, textStatus) {
			$("#voucher_card_next").prop("disabled",false);
			$("#voucher_card_next").text("Process Payment");
			//loader.hidePleaseWait();
		},
		success: function(data, textStatus, xhr) {
			//console.log();
			if(data.success){
				loader2.hidePleaseWait();
				$("#voucher_card_next").prop("disabled",false);
				$("#voucher_card_next").text("Process Payment");
				$("#transaction_id").text(data.reference);
				$("#voucher_section").hide();	
				$("#success_section_transaction").show();
			} else {
				loader2.hidePleaseWait();
				swal(data.msg);
			}
		},
		error: function(xhr, textStatus, errorThrown) {
			$("#voucher_card_next").prop("disabled",false);	   
			$("#voucher_card_next").text("Process Payment");

				loader2.hidePleaseWait();
				swal(data.msg);
		}
		});
	return false;
	});



	</script>

</body>
</html>