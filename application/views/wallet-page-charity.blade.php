<?php
//loading assets helper
defined('BASEPATH') OR exit('No direct script access allowed');
$assetHelper = new AssetHelper\AssetHelper();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>SunCash.Me</title>
	<link rel="stylesheet" href="{{base_url('assets_main/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/main.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/responsive.css')}}">
	<link rel="stylesheet" href="{{base_url('assets_main/css/all.min.css')}}">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>assets/plugins/icon.ico">
	<style type="text/css">
		@media only screen and (max-width: 768px)  {
				.img-x {
				  display: block;
				  margin-left: auto;
				  margin-right: auto;
				  //width: 100%;
				  min-width:128px;
				  min-height:43px;
				}
				.txt-shit{
					text-align:center !important;
				}
		}		
	</style>	
</head>
<body>
	<nav class="navbar navbar-expand-lg sticky-top bg-white shadow-sm p-0">
	  <div class="container p-0" role="navigation">
	  	<a class="navbar-brand" href="{{base_url('payment/wallet')}}"><img src="{{base_url('assets_main/imgs/header-logo@2x.png')}}" alt="Suncash Logo" class="d-inline-block align-top" width="160"></a>
	  	<a href="#" class="menu-button"></a>
	  	<div class="justify-content-end main-menu">
				<ul class="navbar-nav d-flex align-items-center">

					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle main-nav" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<span><?=ucwords(($_SESSION['customer_info']['CustomerName'])) ?></span>
								@if(!empty($suntag_data['profile_pic']))
								<img src="<?=($_SESSION['customer_info']['ImageURL']) ?>" alt="profile image" class="profile-image">
								
								@else
								<img src="{{base_url('assets/img/media/td.png')}}"" alt="profile image" class="profile-image">
								<!-- <img src="{{$suntag_data['profile_pic']}}" alt="profile image" class="profile-image" > -->
								@endif
						</a>
					</li>

		      <li class="nav-item btn-logout">
		        <a class="nav-link" href="{{base_url('wallet/logout')}}">Logout</a>
		      </li>
		    </ul>
		  </div>
	  </div>
	</nav>

	<section class="" id="wallet_section" >
		<div class="container  text-center">
			<div class="row d-flex justify-content-center align-items-center mb-5">
				<div class="user-card transaction-details" style="margin-top:1rem;margin-bottom: -34px !important;">
					<div class="header w-content" style="height:6rem !important;"></div>
					<div class="body">
						<form id="wallet" method="POST">
				        <div class="user-image ">
								@if(!empty($_SESSION['profile_pic']))
								<img src="{{$_SESSION['profile_pic']}}">
								@else
								<img src="{{base_url('assets_main/imgs/suncash-icon.png')}}">
								@endif
							</div>
						<div class="details" style="margin-bottom: -40px;">

							<div class="message">You're about to Donate <?=ucfirst($_SESSION['suntag_shortcode']) ?></div>
							<select name="payment_method" id="payment_method" >
								<option value="">BSD</option>
							</select>
							<input type="text" id="amount" name="amount" value="{{$payment_data['amount']}}" >
						</div>

						<div class="text-left">
							<div class="item">
								<input type="hidden" class="form-control" id="reference_num" name="reference_num"  value="{{$payment_data['reference_num']}}" placeholder="Reference # (Order, Quote, Invoice or Account)">
								<input type="hidden" class="form-control" id="notes" name="notes" placeholder="Note to Business" value="{{$payment_data['notes']}}">
								<input type="hidden" class="form-control" id="name" name="name" placeholder="Name" autocomplete="off" style="resize:none" value="{{$payment_data['name']}}" >
								<input type="hidden" class="form-control" id="email" name="email" placeholder="Email" autocomplete="off" style="resize:none" value="{{$payment_data['email']}}" >
								<input type="hidden" class="form-control" id="mobile" name="mobile" placeholder="mobile" autocomplete="off" style="resize:none" value="{{$payment_data['mobile']}}" >	
							</div>
							<div class="item">
							    <div class="label">You are paying with:</div>
							    <div class="paying">SunCash Account (<span class="text-primary f-medium"><?=number_format($Balance,2, '.', '') ?> BSD</span>)
							  </div>
							</div>
						  <div class="item primary-border">
						  	<div class="label">Transaction Details:</div>
							  <div class="row">
							  	<div class="col">Principal</div>
							  	<div class="col text-right ">$ <span class="amount_val">{{$payment_data['amount']}}</span></div>
							  </div>
<!-- 							  <div class="row">
							  	<div class="col">Transaction Fee</div>
							  	<div class="col text-right ">$ <span class="fee_val">{{$fee_data['fee']}}</span></div>
							  </div> -->
<!-- 							  <div class="row">
							  	<div class="col">VAT</div>
							  	<div class="col text-right ">$ <span class="vat_val">{{$fee_data['vat_charge']}}</span></div>
							  </div> -->
						  </div>
						  <div class="item total">
							  <div class="row">
							  	<?php 
							  		$total = str_replace( ',', '', $payment_data['amount']);
							  	?>
							  	<div class="col label">Total Due</div>
							  	<div class="col text-right "><span class="amount_total">{{$total}}</span> BSD</div>
							  	<input type="hidden" name="amount_total" id="amount_total" value="{{$payment_data['amount']}}">
							  	<input type="hidden" name="balance" id="balance" value="{{number_format($Balance,2)}}">
							  	<input type="hidden" name="hid_fee" id="hid_fee" value="{{$fee_data['fee']}}">
							  	<input type="hidden" name="hid_vat" id="hid_vat" value="{{$fee_data['vat_charge']}}">
							  </div>
						  </div>
						</div>

						<button type="submit" id="process_payment"  class="btn btn-orange full btn-process" data-loading-text="Loading...">Donate</button>
						</form>
					</div>
				</div>
			</div>
		</div>
		
		<footer class="py-2 footer">
			<div class="container">
				<div class="row">
					<div class="col-12 col-lg-6">
						<span class="txt-shit">© {{date('Y')}} SunCash.Me. All Rights Reserved.</span>
					</div>
					<div class="col-12 col-lg-6 text-right">
						<img class="img-x" src="{{base_url('assets_main/imgs/footer-logo.png')}}">
					</div>
				</div>
			</div>
		</footer>		
	</section>

	<section class="full" id="success_section" style=" display:none ;">
		<div class="container  text-center">
			<div class="row d-flex justify-content-center align-items-center mb-5">
				<div class="user-card success-card" style="margin-bottom:60px !important;margin-top:1rem;">
					<div class="header w-content" style="margin-top:1rem;margin-bottom:-34px !important;"></div>
					<div class="body">
						<div class="user-image">
							<img src="{{base_url('assets_main/imgs/success-icon.png')}}">
						</div>
						<div class="details" id ="transaction-details">
							<div class="message" style="font-size: 2rem;line-height: 2.1rem;width: 90%;margin: 0 auto;">You've Donated <span class="amount_val" style="color: #FF8400;">${{$payment_data['amount']}}  </span> to <?=ucfirst($_SESSION['suntag_shortcode'])?></div>
						</div>
						<!-- <form action="" class="text-left"> -->
						<div class="text-left">

					  	<div class="label text-center">Transaction Details</div>
						  <div class="item">
							  <div class="row">
							  	<div class="col">Transaction ID: <span id="transaction_code"></span></div>
							  	<div class="col text-right">{{date("d M Y")}}</div>
							  </div>
							</div>
							<div class="item">
							  <div class="row">
							  	<div class="col">Payment Method</div>
							  	<div class="col text-right">SunCash Account</div>
							  </div>
							</div>

						    <div class="item">
							  <div class="row">
							  	<div class="col">Principal</div>
							  	<div class="col text-right"><span class="amount_val">${{$payment_data['amount']}}</span> BSD</div>
							  </div>
							</div>								
<!-- 							<div class="item">
							  <div class="row">
							  	<div class="col">Transaction Fee</div>
							  	<div class="col text-right"><span class="fee_val">${{$fee_data['fee']}}</span></div>
							  </div>
							</div> -->
<!-- 							<div class="item">
							  <div class="row">
							  	<div class="col">Vat</div>
							  	<div class="col text-right"><span class="vat_val">${{$fee_data['vat_charge']}}</div>
							  </div>
							</div>	 -->
						  <div class="item total">
							  <div class="row">
							  	<div class="col label">Total Due</div>
							  	<div class="col text-right amount"><span class="amount_total">{{$total}}</span> BSD</div>
							  </div>
						  </div>
						  <!-- <a href="{{base_url($_SESSION['suntag_shortcode'])}}" class="btn btn-orange full">Make Another Payment</a> -->
						  <!-- <a href="{{base_url($_SESSION['suntag_shortcode'])}}" class="btn btn-orange full">Make Another Payment</a> -->
						</div>
						<!-- </form> -->

					</div>
				</div>
			</div>
		</div>
	</section>


	<section class="full" id="reload_section" style=" display:none ;">
		<div class="container  text-center">
			<div class="row d-flex justify-content-center align-items-center mb-5">
				<div class="user-card message-card reload-card">
					<div class="header w-content"></div>
					<div class="body">
						<div class="user-image">
							<img src="{{base_url('assets_main/imgs/reload-icon.png')}}">
						</div>
						<div class="details">
							<div class="message error_msg"></div>
						</div>
						<div class="text-center border-top msg_area">


						</div>
						 <a href="{{base_url('paymentchariy/wallet')}}" class="btn btn-orange full">Back</a>
					</div>
				</div>
			</div>
		</div>

	</section>


	<script src="{{base_url('assets_main/js/jquery-3.2.1.min.js')}}"></script>
	<script src="{{base_url('assets_main/js/bootstrap.min.js')}}"></script>
	<script src="{{base_url('assets_main/js/main.js')}}"></script>
    <script src="{{base_url('assets_main/js/select.min.js')}}"></script>
	{!!$assetHelper->link_plugins('js','jquery-1.11.3.min.js')!!}
	{!!$assetHelper->link_plugins('js','bootstrap-3.3.7/js/bootstrap.min.js')!!}
	{!!$assetHelper->link_plugins('js','sidebar-nav/dist/sidebar-nav.min.js')!!}
	{!!$assetHelper->link_plugins('js','override/jquery.slimscroll.js')!!}
	{!!$assetHelper->link_plugins('js','override/waves.js')!!}
	{!!$assetHelper->link_plugins('js','override/custom.min.js')!!}
	{!!$assetHelper->link_plugins('js','sweetalert2-master/dist/sweetalert2.all.min.js')!!}
	{!!$assetHelper->link_plugins('js','bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js')!!}
	{!!$assetHelper->link_plugins('js','curioussolutions-datetimepicker/dist/DateTimePicker.min.js')!!}
	{!!$assetHelper->link_plugins('js','Inputmask-5.x/dist/jquery.inputmask.min.js')!!}

	<!-- {!!$assetHelper->link_plugins('js','dropzone-master/dist/dropzone.js')!!}	 -->
	<script  type="text/javascript" charset="utf-8">
	$("#amount").inputmask({ 'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true, 'digits': 2, 'digitsOptional': false, 'placeholder': '0.00', rightAlign : false,clearMaskOnLostFocus: !1 });


	//$("#amount").inputmask('999,999.99');
	$("#wallet").on('submit',function(e){


		    swal({
		      title: 'Please confirm payment details.',
		      text: "",
		      type: 'warning',
		      showCancelButton: true,
		      confirmButtonColor: '#3085d6',
		      cancelButtonColor: '#d33',
		      confirmButtonText: 'Yes!'
		    }).then((result) => {
		      if (result.value) {
			 	e.preventDefault();
					  $('.hb').remove();
					  $('.form-div').removeClass('has-error').removeClass('has-success');

					  //check validation
					  var err_count = 0;
					  var to_req=[];
					  //jquery blank validation..
					  $(".required").each(function(){
					      var field_id = $(this).attr("id");
					      var data=[];
					      if($(this).val()==""){
					        data['id']=field_id;      
					        to_req.push(data);
					        err_count++;
					      }
					  });

					  /*if($("#password").val()!=$("#cpassword").val()){
					    swal("Youre password and confirmation password do not match.");
					    return false;
					  }*/


					  if(err_count>0){
					    swal(
					    'Opps...',
					    "Please do check required fields.",
					    'error'
					    );  
			/*		    $.each(to_req, function( index, value ) {
					      $("#"+value.id).parent().after('<div class="hb"><div class="help-block" style="color:#f38989; font-size:7pt;margin-bottom:0px !important;">Required filed.</div></div>').addClass('has-error');
					    });   */     
					    return false;
					  }
			      		//var function_ctrl = "process_check" ;
				        var form_data = {
				        	notes:$("#notes").val(),
				        	name:$("#name").val(),
				        	email:$("#email").val(),
				        	mobile:$("#mobile").val(),
				        	reference_num:$("#reference_num").val(),
							amount:$("#amount").val(),
							amount_val:$("#amount_total").val(),
							hid_fee:$("#hid_fee").val(),
							hid_vat:$("#hid_vat").val(),
							balance:$("#balance").val()

				        };
				        $.ajax({
				          url: "<?=site_url("paymentcharity/process_wallet")?>",
				          type: 'POST',
				          dataType: 'json',
				          data: form_data,
				          beforeSend:function(xhr, textStatus) {
				            //called when complete
				            $("#process_payment").prop('disabled', true);
							$("#process_payment").text("processing... please wait");	
				          },
				          complete: function(xhr, textStatus) {
				            //called when complete
							$("#process_payment").prop("disabled",false);
							$("#process_payment").text("Donate");
				          },
				          success: function(data) {

				            if(data.success){
				                //alert(data.msg);
				                var str="<div class='alert alert-dismissible alert-success'><strong>Thank you for your payment ! Please do copy the payment code to present to the nearest suncash store. </strong>.</div>";
				                $("#msg").html(str);
				                $("#success_section").show();
				                $("#wallet_section").hide();

				               	$("#transaction_code").text(data.msg);
				               	$("#notes_value").text($("#notes").val());
				               	$("#ref_value").text($("#reference_num").val());
				                $("#amount_value").text($("#amount").val());
				               	// $("#transaction_section").show();
				               	$(".transaction_details").show();
				               	
				                //otable.ajax.reload();
				                //$("#transfer_modal").modal('hide');
				             	$("#process_payment").prop('disabled', false);
				            } else {
				            	if(data.current_balance!=''){
				                //var str="<div class='alert alert-dismissible alert-danger'><strong> "+data.msg+" </strong>.</div>";
				                $("#error_msg").html(data.msg);
				                $("#wallet_section").hide();
				                $("#reload_section").show();
								$("#transaction_details").hide();
								$(".error_msg").text(data.msg);
								var lvl = '<div class="label">Your current balance:</div><div class="item"><div class="row"><div class="col">SunCash Account (<span class="text-primary balance_lbl">'+data.current_balance+'</span>)</div></div></div>';
								$(".msg_area").html(lvl);
								} else if (data.current_balance=='') {
				                $("#error_msg").html(data.msg);
				                $("#wallet_section").hide();
				                $("#reload_section").show();
								$("#transaction_details").hide();	
								$(".error_msg").text(data.msg);								
					  			var lvl='<div class="label">Support Contact Details</div><div class="item"><div class="row"><div class="col text-primary">support@suncash.me</div></div></div>';
					  			$(".msg_area").html(lvl);
								}

				               	// $("#transaction_code").val('');
				               	// $("#transaction_section").hide();
				   
				               	
				             $("#process_payment").prop('disabled', false);		
				            }
				          },
				          error: function(data, textStatus, errorThrown) {
				            //called when there is an error
				            //alert(data.msg);
				             $("#process_payment").prop('disabled', false);
				          }
				        });


		      }
		    });

		    return false;
   
		});
	$("#back_to_form").click(function(){
		$("#cash_section").show();
		$("#success_section").hide();
	});
	$("#amount").change(function(){

		$(".amount_val").text($("#amount").val());
		$(".amount_total").text($("#amount").val());
		$("#amount_total").val($("#amount").val());

/*		$.ajax({
		  url: '<?=site_url("payment/process_fee")?>',
		  type: 'POST',
		  dataType: 'json',
		  data: {amount: parseFloat($("#amount").val().replace(/,/g, ''))},
		  complete: function(xhr, textStatus) {
		    //called when complete
		  },
		  success: function(data, textStatus, xhr) {
		    if(data.success){
		    	//console.log(data.fee_data.fee);
		    	$(".amount_val").text($("#amount").val());
				$(".fee_val").text(data.fee_data.fee);
				$(".vat_val").text(data.fee_data.vat_charge);
				var total = parseFloat($("#amount").val().replace(/,/g, ''))+parseFloat(data.fee_data.fee)+parseFloat(data.fee_data.vat_charge);
				$(".amount_total").text(total);
				$("#amount_total").val(total);
				$("#hid_fee").val(data.fee_data.fee);
				$("#hid_vat").val(data.fee_data.vat_charge);
		    }
		  },
		  error: function(xhr, textStatus, errorThrown) {
		    //called when there is an error
		  }
		});
		*/
	});
      // $("wallet")[0].reset();
	</script>
</body>
</html>