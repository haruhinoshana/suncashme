<?php
//loading assets helper
defined('BASEPATH') OR exit('No direct script access allowed');
$assetHelper = new AssetHelper\AssetHelper();

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Card Revalidation</title>
  <link rel="stylesheet" href="{{base_url('assets_main/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{base_url('assets_main/css/main_checkout.css')}}">
  <link rel="stylesheet" href="{{base_url('assets_main/css/all.min.css')}}">
  <link rel="stylesheet" href="{{base_url('assets/plugins/global/global2.css')}}">
  <link href="https://unpkg.com/filepond/dist/filepond.css" rel="stylesheet">
  <link href="https://unpkg.com/filepond/dist/filepond.min.css" rel="stylesheet">
  <link href="https://unpkg.com/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.min.css" rel="stylesheet"> 
  <style type="text/css" >
    .ïframecenpos{
      border:0px !important;
    }
    .layaoutPanelTokenCardForm .canvasbox {
        margin-left: 10% !important;
        margin-right:10% !important;
        width: 100% !important;     
    }
    .Modern .canvasbox .row > span {
      /*webkit-font-smoothing: antialiased !important;*/
      webkit-font-smoothing: antialiased !important;
        font-family: "Roboto", sans-serif !important;
        font-weight: 400 !important;
        font-size: 15px !important;
        color: #292929 !important;
        line-height: 1.5em !important;
        margin-bottom: .5rem !important;
    }

    .Modern .canvasbox .row > input {
      /*webkit-font-smoothing: antialiased !important;*/
      webkit-font-smoothing: antialiased !important;
        font-family: "Roboto", sans-serif !important;
        font-weight: 400 !important;
        font-size: 15px !important;
        color: #292929 !important;
        line-height: 1.5em !important;
        margin-bottom: .5rem !important;
    }
    .has-error-border{
      border-color:red !important ;
    }
  
  </style>
</head>
<body>
  <nav class="navbar navbar-light bg-light">
    <a class="navbar-brand" href="#">
      <img src="{{base_url('assets_main/imgs/suncash-logo.png')}}">
    </a>
  </nav>
  <main class="v4">
    <div class="container">
      <div class="row main">
        <div class="col-lg-12">
<!--           <div class="order-summary">
            
          </div> -->
        </div>
      </div>
    </div>
  </main>



  <div class="modal fade" id="cardValidation" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Credit Card Validation</h5>
<!--           <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button> -->
        </div>
        <div class="modal-body">
          <form name="whitelist_form" id="whitelist_form" class="form-horizontal" method="POST" enctype="enctype/form-data">
            <div class="progress">
              <div class="progress-inner">
                <div class="progress-step active" data-step="1">
                  <div class="number">1</div>
                  <div class="label">Basic Information</div>
                </div>

                <div class="progress-step" data-step="2">
                  <div class="number">2</div>
                  <div class="label">Upload Documents <br>and Selfie</div>
                </div>

                <div class="progress-step" data-step="3">
                  <div class="number">3</div>
                  <div class="label">Confirmation</div>
                </div>
              </div>
            </div>

            <div class="progress-container">
              <div class="progress-content active" data-content="1">
                <div class="container px-4">
                  <div class="row card-section-title align-items-center">
                    <div class="col-12 col-lg-6 p-0">
                      <span >Basic Information</span>
                    </div>
                  </div>

                 <div class="form-group">
                    <label for="cardNumber">Last 4 Digit Card Number:</label>
                    <input type="text" class="form-control input-sm required" id="last4digits" name="last4digits"  readonly="" value='{{!empty($validation_data) ? $validation_data["card_last4digits"]:"" }}' />
                  </div>
                  <div class="form-group two-input">
                    <label for="type">Type:</label>
                    <input type="text" class="form-control input-sm required" id="card_type_w" name="card_type_w" readonly="" value='{{!empty($validation_data) ? $validation_data["card_type"]:"" }}'   />
                  </div>
                  <div class="form-group">
                    <label for="cardName">Name on Card:</label>
                    <input type="text" class="form-control input-sm required" id="card_name" name="card_name" readonly="" value='{{!empty($validation_data) ? $validation_data["card_name"]:"" }}'  />
                  </div>
                  <div class="form-group">
                    <label for="cardID">* ID Number:</label>
                    <input type="text" class="form-control input-sm required" id="card_id" name="card_id" value='{{!empty($validation_data) ? $validation_data["customer_id"]:"" }}'  />
                  </div>
                  <div class="form-group">
                    <label for="cardEmail">* Email:</label>
                    <input type="email" class="form-control input-sm required" id="card_email" name="card_email"value='{{!empty($validation_data) ? $validation_data["email"]:"" }}'  />
                  </div>
                  <div class="form-group">
                    <label for="cardMNumber">* Mobile Number:</label>
                  <input type="text" class="form-control bfh-phone" id="card_mobile_number" name="card_mobile_number" data-format="1 (ddd) ddd-dddd" value ='{{!empty($validation_data) ? $validation_data["mobile_number"]:"1 242" }}' >
                  </div>
                </div>
              </div>
              <div class="progress-content" data-content="2">
                <div class="container px-4">
                  <div class="row card-section-title align-items-center">
                    <div class="col-12 col-lg-6 p-0">
                      <span >Upload ID and Selfies</span>
                    </div>
                  </div>
                
                  <div class="upload-container">
                    <div class="form-group mb-5">
                      <label>Upload a Scan of your ID</label>
                      <div class="info"><i class="fas fa-exclamation-circle"></i> Please upload a Government issued ID such as  Passport, current Driver’s License, or National ID Card.</div>

                      <div class="row">
                        <div class="col-12 col-lg-6">
                          <input type="file" class="my-pond" name="filepond1" id="card_id_upload" accept="image/jpeg, image/png" />
                        </div>
                        <div class="col-12 col-lg-6">
                          <div class="illustration-image text-center">
                            <div>
                              <img src="{{base_url()}}assets_checkout/imgs/card-id.png" alt="">
                              <span class="d-block">Example</span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="form-group mb-5">
                      <label>Upload a Scan of your Debit/Credit Card</label>
                      <div class="info"><i class="fas fa-exclamation-circle"></i> Please upload the debit/credit card you used with a clear view of your last 4 digit card number, name and expiry date</div>
                      <div class="row">
                        <div class="col-12 col-lg-6">
                          <input type="file" class="my-pond" name="filepond2" id="credit_card_upload"/>
                        </div>
                        <div class="col-12 col-lg-6">
                          <div class="illustration-image text-center">
                            <div>
                              <img src="{{base_url()}}assets_checkout/imgs/debit-card.png" alt="">
                              <span class="d-block">Example</span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label>Upload a Credit Card with Card ID</label>
                      <div class="info"><i class="fas fa-exclamation-circle"></i> Please upload a clear photo of you with the debit/credit card and your Card ID.</div>

                      <div class="row">
                        <div class="col-12 col-lg-6">
                          <input type="file" class="my-pond" name="filepond3" id="cc_with_card_upload" accept="image/jpeg, image/png"/>
                        </div>
                        <div class="col-12 col-lg-6">
                          <div class="illustration-image text-center">
                            <div>
                              <img src="{{base_url()}}assets_checkout/imgs/user-hold-cards.png" alt="">
                              <span class="d-block">Example</span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="progress-content success-screen" data-content="3">
                <div class="container px-4">
                  <div class="row">
                    <div class="col mt-2">
                      <i class="far fa-thumbs-up"></i>
                      <div class="validation-message">Validation submitted!</div>

                      <center><button class="btn btn-primary" id="okay_btn"  type="button">OK</button></center>
                    </div>

                  </div>
                </div>
              </div>
            </div>

            <div class="container btn-container  py-5">
              <div class="row align-items-center">
                <div class="col-6 mb-1">
                  <!-- <button type='button' class="btn btn-close" data-dismiss="modal">Close</button> -->
                </div>
                <div class="col-6 text-right">
                  <input type="hidden" name="wid" id="wid" value='{{!empty($validation_data) ? $validation_data["id"]:"" }}' />
                      <input type="hidden" name="merckey" id="merckey" value='{{!empty($validation_data) ? $validation_data["merchant_key"]:"" }}'/>
                  <button class="btn btn-primary btn-process" id="submit_btn"  type="button">Next</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>


<!--  <footer>
    <div class="container">
      <div class="row d-flex align-items-center">
        <div class="col-12 col-lg-6">
          <div class="copyright">© Copyright Suncash 2018. All Rights Reserved.</div>
          <div class="link">
            <a href="#">Privacy Policy</a><span>|</span><a href="#">Terms & Conditions</a>
          </div>
        </div>
        <div class="col-12 col-lg-6 logo">
          <img src="{{base_url('assets_main/imgs/suncash-logo.png')}}">
        </div>
      </div>
    </div>
  </footer> -->

  <script src="{{base_url('assets_main/js/jquery-3.2.1.min.js')}}"></script>
  <script src="{{base_url('assets_main/js/bootstrap.min.js')}}"></script>
    <script src="{{base_url('assets_main/js/select.min.js')}}"></script>
<!--  {!!$assetHelper->link_plugins('js','jquery-1.11.3.min.js')!!}
  {!!$assetHelper->link_plugins('js','bootstrap-3.3.7/js/bootstrap.min.js')!!} -->
  {!!$assetHelper->link_plugins('js','sidebar-nav/dist/sidebar-nav.min.js')!!}
  {!!$assetHelper->link_plugins('js','override/jquery.slimscroll.js')!!}
  {!!$assetHelper->link_plugins('js','override/waves.js')!!}
  {!!$assetHelper->link_plugins('js','override/custom.min.js')!!}
  {!!$assetHelper->link_plugins('js','sweetalert2-master/dist/sweetalert2.all.min.js')!!}
  {!!$assetHelper->link_plugins('js','bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js')!!}
  {!!$assetHelper->link_plugins('js','curioussolutions-datetimepicker/dist/DateTimePicker.min.js')!!}
  {!!$assetHelper->link_plugins('js','Inputmask-5.x/dist/jquery.inputmask.min.js')!!}
  {!!$assetHelper->link_plugins('js','bootstrap-formhelpers-phone/bootstrap-formhelpers-phone.js') !!}
  {!!$assetHelper->link_plugins('js','bootstrap-formhelpers-phone/bootstrap-formhelpers-phone.format.js') !!}
  {!!$assetHelper->link_plugins('js','dropzone-master/dist/dropzone.js')!!} 
  {!!$assetHelper->link_plugins('js','global/global.js')!!} 
  <script src="https://www.cenpos.com/Plugins/porthole.min.js"></script>
  <script src="https://www.cenpos.com/Plugins/jquery.simplewebpay.js"></script> 
  <script src="https://unpkg.com/filepond/dist/filepond.min.js"></script>
  <script src="https://unpkg.com/jquery-filepond/filepond.jquery.js"></script>
  
  <script src="https://unpkg.com/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.min.js"></script>
  <script src="https://unpkg.com/filepond-plugin-image-exif-orientation/dist/filepond-plugin-image-exif-orientation.min.js"></script>
  <script src="https://unpkg.com/filepond-plugin-file-validate-type/dist/filepond-plugin-file-validate-type.js"></script>
  <script src="https://unpkg.com/filepond-plugin-file-encode/dist/filepond-plugin-file-encode.js"></script> 
  <script src="{{base_url('assets_main/js/main.js')}}"></script>

  <script  type="text/javascript" charset="utf-8">
  @if($validation_data['status']!='rejected')
    swal("Card not validated yet.");
    window.location.href='{{base_url()}}';
  @else
    $("#cardValidation").modal('show');
  @endif

  
  function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
  }

  //for modal validation of card
  $('.btn-process').on('click', function() {
    //alert("safari af");
    $(this).html('Submit');
    $progressContentDone = $('.progress-content.active').data('content');
    //alert($progressContentDone);
    if($progressContentDone==1){
      if($("#card_id").val()=='' || $("#card_email").val()=='' || $("#card_mobile_number").val()=='' ){
        swal('Fill up all required field/s.');
        return false;
      }
    }
  if(!isEmail($("#card_email").val())){
        swal('Invalid Email format.');
        return false;   
  }


    if($progressContentDone==2){
      console.log('if');
    $('.btn-gray').css({"opacity": "1"});     
    $('.hb').remove();
    $('.required').removeClass('has-error-border').removeClass('has-success');

    //check validation
    var err_count = 0;
    var to_req=[];
    //jquery blank validation..
    $("#whitelist_form .required").each(function(){
        var field_id = $(this).attr("id");
        var data=[];
        if($(this).val()==""){
          data['id']=field_id;      
          to_req.push(data);
          err_count++;
        }
    });

    if(err_count>0){
      swal(
      'Oops...',
      "Please do check required fields.",
      'error'
      );  
      $.each(to_req, function( index, value ) {
        $("#"+value.id).addClass('has-error-border');//.after('<div class="hb"><div class="help-block" style="color:#f38989; font-size:7pt;margin-bottom:0px !important;">Required filed.</div></div>')
      });        
      return false;
    }    
    //
    //var form = new FormData();

    if($("#card_id_upload").find('[name=filepond]').val()=='' || $("#credit_card_upload").find('[name=filepond]').val()=='' || $("#cc_with_card_upload").find('[name=filepond]').val()==''){
      swal(
      'Opps...',
      "All photo are required",
      'error'
      ); 
      return false;
    }



    var url = '{{base_url("payment/update_whitelist")}}';
    //var formData = new FormData($("#whitelist_form")[0]);
    //var formData = $("#whitelist_form").serializeArray();
    var file1 = JSON.parse($("#card_id_upload").find('[name=filepond]').val());
    //formData.append('card_id_upload', file1.data);
    var file2 = JSON.parse($("#credit_card_upload").find('[name=filepond]').val());
    //formData.append('credit_card_upload', file2.data);
    var file3 = JSON.parse($("#cc_with_card_upload").find('[name=filepond]').val());
    //formData.append('cc_with_card_upload', file3.data); 

/*    formData.push({name: 'card_id_upload', value: file1.data});
    formData.push({name: 'credit_card_upload', value: file2.data});
    formData.push({name: 'cc_with_card_upload', value: file3.data});*/

    //for (var i = 0; i < $('.my-pond input').length; i++) {
  
    //}
        var formData = {
      last4digits:$("#last4digits").val(),
      card_type_w:$("#card_type_w").val(),
      card_name:$("#card_name").val(),
      card_id:$("#card_id").val(),
      card_email:$("#card_email").val(),
      card_mobile_number:$("#card_mobile_number").val(),
      card_id_upload:file1.data,
      credit_card_upload:file2.data,
      cc_with_card_upload:file3.data,
      wid:$("#wid").val(),
      merckey:$("#wid").val(),
        };



/*    formData.append('card_id_upload', $('#card_id_upload')[0].files[0]); 
    formData.append('credit_card_upload', $('#credit_card_upload')[0].files[0]); 
    formData.append('cc_with_card_upload', $('#cc_with_card_upload')[0].files[0]); */
    var button = $("#submit_btn");

      $.ajax({
        url: url,
        type: 'POST',
        dataType: 'json',
        data: formData,
        //contentType: false,       
        //cache: false,             
        //processData:false,       
        beforeSend: function(xhr, textStatus) {
          //called when complete
          //clearValidationArray();
          loader.showPleaseWait();
          $(button).prop('disabled',true);        
        },          
        complete: function(xhr, textStatus) {
          //called when complete
          loader.hidePleaseWait();
          $(button).prop('disabled',false);        
        },
        success: function(data) {
          //called when successful
          if(data.success){
              // swal(
              // '',
              // data.msg,
              // 'success'
              // );  

        $('.progress-step[data-step="' + $progressContentDone + '"]').addClass('done').removeClass('active');
        $('.progress-step.done .number').html('<i class="fas fa-check"></i>');
        $('.progress-step[data-step="' + $progressContentDone + '"]').next().addClass('active');

        $('.progress-content[data-content="'+$progressContentDone+'"]').addClass('done').removeClass('active');
        $('.progress-content[data-content="'+$progressContentDone+'"]').next().addClass('active');

              //hideModal("large");
          $('#cardValidation .btn-container').remove();
        $('.progress-content[data-content="3"]').addClass('done').removeClass('active');
        $('.progress-step[data-step="3"]').addClass('done').removeClass('active');
        $('.progress-step.done .number').html('<i class="fas fa-check"></i>');
        $('.progress-content[data-content="3"]').addClass('done');

          } else {
              swal(
              'Opps...',
              data.msg,
              'error'
              );  
          }     
        },
        error: function(xhr, textStatus, errorThrown) {
          swal('something went wrong.');
        }
      });

    }
    //return false;
    if($progressContentDone!=2){
    $('.progress-step[data-step="' + $progressContentDone + '"]').addClass('done').removeClass('active');
    $('.progress-step.done .number').html('<i class="fas fa-check"></i>');
    $('.progress-step[data-step="' + $progressContentDone + '"]').next().addClass('active');

    $('.progress-content[data-content="'+$progressContentDone+'"]').addClass('done').removeClass('active');
    $('.progress-content[data-content="'+$progressContentDone+'"]').next().addClass('active');
    }

  });

  // $(window).resize(function(){
  //   progressHeight();
  // });

  // setTimeout(function() {
   //  progressHeight();
  // }, 2000);

  // function progressHeight() {
  //   $progressContent = $('.progress-content.active').height();
  //   $('.progress-container').css({
  //     "height": $progressContent
  //   });
  // }

  $('.my-pond').filepond();
  $.fn.filepond.registerPlugin(FilePondPluginImagePreview);
  $.fn.filepond.registerPlugin(FilePondPluginFileValidateType);
  $.fn.filepond.registerPlugin(FilePondPluginFileEncode);
  // Turn input element into a pond with configuration options
  $('.my-pond').filepond({
      allowMultiple: true,
      // labelIdle: '<button type="button" class="btn btn-upload"><i class="fas fa-upload"></i> Upload</button>',
      acceptedFileTypes: [
        'image/jpg',
        'image/jpeg',
        'image/png',
      ]
  });

  // Set allowMultiple property to true
  $('.my-pond').filepond('allowMultiple', false);

  // Listen for addfile event
  $('.my-pond').on('FilePond:addfile', function(e) {
      console.log('file added event', e);
  });


 $("#okay_btn").click(function(){
  window.location.href='{{base_url()}}';
 });


  </script>
</body>
</html>