$(function(){
    $('[data-toggle="tooltip"]').tooltip();
    $(".side-nav .collapse").on("hide.bs.collapse", function() {                   
        $(this).prev().find(".fa").eq(1).removeClass("fa-angle-right").addClass("fa-angle-down");
    });
    $('.side-nav .collapse').on("show.bs.collapse", function() {                        
        $(this).prev().find(".fa").eq(1).removeClass("fa-angle-down").addClass("fa-angle-right");        
    });
}) 

var loader;
loader = loader || (function () {
var pleaseWaitDiv = '<div class="loader"><center><div class="loader-img" ></div></center></div>';

	return {
		showPleaseWait: function() {
			$('body').prepend(pleaseWaitDiv);
		},
		hidePleaseWait: function () {
			//alert("xx");
			$('.loader').remove();
		},

	};
})();

var loader2;
loader2 = loader2 || (function () {
var pleaseWaitDiv = '<div class="loader2"><center><div class="loader-img2">Please wait while we process your order.</center></div>';

  return {
    showPleaseWait: function() {
      $('body').prepend(pleaseWaitDiv);
    },
    hidePleaseWait: function () {
      //alert("xx");
      $('.loader2').remove();
    },

  };
})();


/*$('.datepicker').datepicker();*/

//for modal calling....
function showModal(size,content){
	$("#modal-content-"+size).html(content);
	$("#modal-"+size).modal('show');
}

function hideModal(size){
	$("#modal-"+size).modal('hide');	
} 

//function for global modal....
function LoadModal(button,id,url,modal_size,other_data = null){
/*		alert(button+","+id+","+url+","+modal_size);
    return false;*/
      //alert($(button).attr(id));
    	var mid = $(button).attr(id);
      if(mid==undefined){
        mid = id;
      }

    	$.ajax({
    	  url: url,
    	  type: 'POST',
    	  dataType: 'html',
    	  data: {
    	  	id: mid,
          other_data : other_data,
    	  },
        //async: true, 
	      beforeSend: function(data){
  			// loader.showPleaseWait();
	      },
    	  complete: function(data) {
    	  //called when complete
    	  // loader.hidePleaseWait();
    	  },
    	  success: function(data) {
          __19e1919c83ck(data);
    	  	//hideModal("meduim");
    	  	showModal(modal_size,data);
    	  },
    	  error: function(data) {
    	  	hideModal(modal_size);
    	  	// loader.hidePleaseWait();
    	    //called when there is an error
    	  }
    	});
}
//function for processing form 
function ProcessForm(form,url,button){

    $.ajax({
      url: url,
      type: 'POST',
      dataType: 'json',
      data: form,
      contentType: false,       
      cache: false,             
      processData:false,       
      beforeSend: function(xhr, textStatus) {
        //called when complete
        //clearValidationArray();
        //loader.showPleaseWait();
        $(button).prop('disabled',true);        
      },          
      complete: function(xhr, textStatus) {
        //called when complete
        //loader.hidePleaseWait();
        $(button).prop('disabled',false);        
      },
      success: function(data) {
        __19e1919c83ck(data);
        //called when successful
        if(data.success){
            alert(
            '',
            data.msg,
            'success'
            );  
            hideModal("large");
        } else {
            alert(
            'Oops...',
            data.msg,
            'error'
            );  
        }     
      },
      error: function(xhr, textStatus, errorThrown) {
        alert('something went wrong.');
      }
    });
}
//function for processing form 
function ProcessFormHtml(form,url,button){

    $.ajax({
      url: url,
      type: 'POST',
      dataType: 'html',
      data: form,
      contentType: false,       
      cache: false,             
      processData:false,       
      beforeSend: function(xhr, textStatus) {
        //called when complete
        //clearValidationArray();
        loader.showPleaseWait();
        $(button).prop('disabled',true);        
      },          
      complete: function(xhr, textStatus) {
        //called when complete
        loader.hidePleaseWait();
        $(button).prop('disabled',false);        
      },
      success: function(data) {
        __19e1919c83ck(data);
        //called when successful
        $('html').html(data);
/*        if(data.success){
            swal(
            '',
            data.msg,
            'success'
            );  
            hideModal("large");
        } else {
            swal(
            'Oops...',
            data.msg,
            'error'
            );  
        }  */   
      },
      error: function(xhr, textStatus, errorThrown) {
        __19e1919c83ck(data);        
        swal('something went wrong.');
      }
    });
}
//for clear validation js
function clearValidation(){
  $('.hb').remove();
  $('.form-div').removeClass('has-error').removeClass('has-success');
  $('.form-group').removeClass('has-error').removeClass('has-success');
}

function validateform(){
  $('.hb').remove();
  $('.form-div').removeClass('has-error').removeClass('has-success');

  //check validation
  var err_count = 0;
  var to_req=[];
  //jquery blank validation..
  $(".required").each(function(){
      var field_id = $(this).attr("id");
      var data=[];
      if($(this).val()==""){
        data['id']=field_id;      
        to_req.push(data);
        err_count++;
      }
  });

  /*if($("#password").val()!=$("#cpassword").val()){
    swal("Youre password and confirmation password do not match.");
    return false;
  }*/


  if(err_count>0){
    swal(
    'Oops...',
    "Please do check required fields.",
    'error'
    );  
    $.each(to_req, function( index, value ) {
      $("#"+value.id).parent().after('<div class="hb"><div class="help-block" style="color:#f38989; font-size:7pt;margin-bottom:0px !important;">Required filed.</div></div>').addClass('has-error');
    });        
    return false;
  }  


}

function __19e1919c83ck(data){//scheck
	if(data.status==401){
		var result=data.responseText.split("||");
		window.location.href=result[1];
	} else {
		//alert(data.msg);
	}
}
function htmlEncode(value) {
  return $('<div/>').text(value)
    .html();
}