$(document).ready(function () {
	$(".menu-button").on("click",function() {
		$(".main-menu").toggleClass("open");
		$(this).toggleClass("open");
	});

	$( ".card-header" ).each(function( index ) {
	  $(this).attr("href", "#collapse" + index);
	  $(this).next(".card-body").attr("id", "collapse" + index);
	});

	$(".select-items").find("div:first-child").remove();

	$(".btn-process").on("click", function() {
		$(this).addClass("processing");
	  var $this = $(this);
    // var loadingText = 'Processing...';
    var loadingText = $(this).data("loading-text");
    if ($(this).html() !== loadingText) {
      $this.data("original-text", $(this).html());
      $this.html(loadingText);
    }
    setTimeout(function() {
      $this.html($this.data("original-text"));
			$(".btn-process").removeClass("processing");
    }, 2000);
	});

	$(".store").on("click", function() {
		$(".store").removeClass("selected");
		$(this).toggleClass("selected");
	});



});