// function af(){
// 	//alert("function");
    
// 	//proceed to the internal process.
// 	var form_data ={
// 	//merchant:merchant,
// 	name:$("#name").val(),
// 	email:$("#email").val(),
// 	mobile:$("#mobile").val(),
// 	notes:$("#notes").val(),
// 	reference_num:$("#reference_num").val(),	
// 	tokenid:$("#tid").val(),
// 	card_type:$("#card_type").text(),
// 	card_number:$("#card_number").text(),
// 	name_on_card:$("#name_on_card").text(),
// 	// notes:$("#notes").val(),
// 	// reference_num:$("#notes").val(),
// 	amount:$("#amount").val(),
// 	amount_val:$("#amount_total").val(),
// 	hid_fee:$("#hid_fee").val(),
// 	hid_vat:$("#hid_vat").val(),
// 	hid_pfee:$("#hid_pfee").val(),
// 	hid_tfee:$("#hid_tfee").val(),
// 	total_fee:total_fees,
// 	cp_return:$("#cp_return").val(),
//     }; 
//     console.table(form_data);

// 	$.ajax({
// 	  url: '<?=site_url("payment/process_card_credit")?>',
// 	  type: 'POST',
// 	  dataType: 'json',
// 	  data: form_data,
// 	  async: false,
//       beforeSend:function(){
//       	loader.showPleaseWait();
//        // $("#payment").prop("disabled",true);
//         //$("#payment").text("processing... please wait");

//       },
//       complete: function(xhr, textStatus) {
//       	loader.hidePleaseWait();
//         //$("#payment").prop("disabled",false);
//         //$("#payment").text("Process Payment");

//       },
// 	  success: function(data, textStatus, xhr) {
//         if(data.success){
//         	$("#transaction_code").text(data.reference);
//             $("#success_section_transaction").show();
// 			$("#card_section").hide();
//         } else {
//             //swal(data.msg);
//           	$(".cancel_section").show();
// 	    	$(".payment_btn_section").hide();
// 	    	$("#success_section_transaction").hide();
// 	    	$("#payment").prop("disabled",false);
//         	$("#payment").text("Process Payment");
            
//         }
// 	  },
// 	  error: function(xhr, textStatus, errorThrown) {
// 	    //called when there is an error
// 	  }
// 	});
// }

function handleResponse(response){//response is already json so no nid to convert
  //var responseObj = JSON.parse(response);
  //alert(response.Result);
  if(response.Result == 21){
    $("#secure_modal").modal('show');
    $("#CardinalCommerceView3D").html(response.View3D);
    $("#cenposPaySecureId").css({
        height: '550px',
    });
  }else{
    handle3DSecure(response);
  }
}

function handle3DSecure(msg){//response is already json so no nid to convert
    var responseObj = JSON.parse(msg);
    //alert(msg);
    //console.log(responseObj);
    $("#cp_return").val(responseObj);
    af();
    $("#secure_modal").modal('hide');
}

function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}

function check_ccwhitelist(card_info){
    // alert("x");
    //
    var mkey = "{{$_SESSION['suntag_shortcode']}}";
    @if($_SESSION['tag']=='MERCHANT')
    mkey = "{{$_SESSION['merchant_key']}}";
    @endif
    var formdata={
        ProtectedCardNumber:card_info.ProtectedCardNumber,
        NameonCard:card_info.NameonCard,
        CardType:card_info.CardType,
        merchant_key:mkey,
        source:'payment',
        amount:$("#amount_total").val(),
    };

    $.ajax({
      url: '{{base_url("payment/check_whitelist")}}',
      type: 'POST',
      dataType: 'json',
      data: formdata,
      beforeSend: function(xhr, textStatus) {
        //called when complete
        //$("#card_info").hide();
        loader.showPleaseWait();
      },		  
      complete: function(xhr, textStatus) {
        //called when complete
        loader.hidePleaseWait();
      },
      success: function(data, textStatus, xhr) {
        //called when successful
        if(!data.success){
            //alert("repeat data");
            if(data.status=='active'){
            swal({
              title: 'We need more information to process this order.',
              // text: "We need more information to verify your card ending with "+card_info.ProtectedCardNumber+" to continue this order.",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#FF8400',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Proceed'
            }).then((result) => {
              if (result.value) {
                  //alert("validate");

                $("#last4digits").val(card_info.ProtectedCardNumber);
                $("#card_type_w").val(card_info.CardType);
                $("#card_name").val(card_info.NameonCard);	
                $("#wid").val(data.data.id);			      	

                  $("#cardValidation").modal('show');

              } /*else{
                  alert("cancelled");
              }*/	
            });
            } else if (data.status=='rejected'){
                swal("Unfortunately, your card was rejected and not authorized to continue this transaction.");
            } else if (data.status=='for_approval'){
                swal("Card not validated yet");
            }

            $("#card_info").show();
        } else {
        $("#card_number").text(card_info.ProtectedCardNumber);
        $("#name_on_card").text(card_info.NameonCard);
        $("#card_type").text(card_info.CardType);
        $("#tid").val(card_info.RecurringSaleTokenId);
        $("#success_section").show();
        $("#card_info").hide();
        }
      },
      error: function(xhr, textStatus, errorThrown) {
        //called when there is an error
      }
    });
    
}

function getcfee(amount){
    //e.preventDefault();
    //e.stopImmediatePropagation();				
    // accordion is open
    var total_billpay_fees = parseFloat($("#amount").val().replace(/,/g, ''))+parseFloat($("#hid_fee").val())+parseFloat($("#hid_vat").val());
    //alert($("#amount").val()+"+"+$("#hid_fee").val()+"+"+$("#hid_vat").val());
    $.ajax({
      url: '{{base_url("payment/get_fees_payment")}}',
      type: 'POST',
      dataType: 'json',
      data:{amount:total_billpay_fees,client_id:'{{$_SESSION["client_record_id"]}}'},
         beforeSend:function(xhr, textStatus) {
        //called when complete
        loader.showPleaseWait();
        $("#submit").prop('disabled', true);
      },		  
      complete: function(xhr, textStatus) {
        //called when complete
          loader.hidePleaseWait();
      },
      success: function(data, textStatus, xhr) {
        //called when successful
        //paste values
        if(data.success){
            $(".conviniecefee_val").text(data.fee);
            //$("#amount").text(data.total);
            var total= total_billpay_fees+parseFloat(data.fee);
            $(".amount_total").text(total.toFixed(2));
            $("#amount_total").val(total.toFixed(2));
            $("#hid_pfee").val(data.pf);
            $("#hid_tfee").val(data.tf);
            $("#hid_totalfee").val(data.fee);

            $("#submit").prop('disabled', false);
        }  else {
            swal(data.msg);
            $("#submit").prop('disabled', false);
        }

      },
      error: function(xhr, textStatus, errorThrown) {
        //called when there is an error
        swal(data.msg);
        $("#submit").prop('disabled', false);
      }
    });
}
function getcfee_customer(amount){
    //e.preventDefault();
    //e.stopImmediatePropagation();				
    // accordion is open
    var total_billpay_fees = parseFloat($("#amount").val().replace(/,/g, ''))+parseFloat($("#hid_fee").val())+parseFloat($("#hid_vat").val());
    //alert($("#amount").val()+"+"+$("#hid_fee").val()+"+"+$("#hid_vat").val());
    $.ajax({
      url: '{{base_url("payment/get_fees_customer")}}',
      type: 'POST',
      dataType: 'json',
      data:{amount:total_billpay_fees,client_id:'{{$_SESSION["client_record_id"]}}'},
         beforeSend:function(xhr, textStatus) {
        //called when complete
        loader.showPleaseWait();
        $("#submit").prop('disabled', true);
      },		  
      complete: function(xhr, textStatus) {
        //called when complete
          loader.hidePleaseWait();
      },
      success: function(data, textStatus, xhr) {
        //called when successful
        //paste values
        if(data.success){
            $(".conviniecefee_val").text(data.fee_data.fee);
            //$("#amount").text(data.total);
            var total= total_billpay_fees+parseFloat(data.fee_data.fee);
            $(".amount_total").text(total.toFixed(2));
            $("#amount_total").val(total.toFixed(2));
            $("#hid_pfee").val(data.fee_data.pf);
            $("#hid_tfee").val(data.fee_data.tf);
            $("#hid_totalfee").val(data.fee_data.fee);

            $("#submit").prop('disabled', false);
        }  else {
            swal(data.msg);
            $("#submit").prop('disabled', false);
        }

      },
      error: function(xhr, textStatus, errorThrown) {
        //called when there is an error
        swal(data.msg);
        $("#submit").prop('disabled', false);
      }
    });
}

@if($_SESSION['tag']=='MERCHANT')
getcfee('{{$payment_data["amount"]}}');
@endif
@if($_SESSION['tag']=='CUSTOMER')
getcfee_customer('{{$payment_data["amount"]}}');
@endif
function CallbackSuccess(responseData) {
alert(JSON.stringify(responseData));
}
function CallbackCancel(responseData) {
alert(JSON.stringify(responseData));
}

$("#submit").click(function(){
    $("#NewCenposPlugin").submitAction();
});

var merchant='{{CENPOST_MERCHANT_ID}}';
$("#NewCenposPlugin").createWebpay({
url: 'https://www.cenpos.net/simplewebpay/cards',
params : "merchantid="+merchant+"&iscvv=true",
height:'400px',
//isCvv :true&SecretKey=a0c70a0d5aa451bfc02f17e9199e41e6&verifyingpost="+vp
sessionToken:false,
isSameSite: 'Lax',
type3d:'FunctionAuto',
cardinalReturn:"handle3DSecure",

beforeSend:function(xhr, textStatus) {
    //called when complete
    loader.showPleaseWait();
    $("#submit").prop('disabled', true);
  },
complete: function(xhr, textStatus) {
    //called when complete
    $("#submit").prop('disabled', true);
    $("#submit").text("please wait..");
    loader.hidePleaseWait();
  },
    success: function(data){
        //console.log(data);
        //console.log(data.ProtectedCardNumber);
        //if(data.success){
        // $("#card_number").text(data.ProtectedCardNumber);
        // $("#name_on_card").text(data.NameonCard);
        // $("#card_type").text(data.CardType);
        // $("#tid").val(data.RecurringSaleTokenId);
        // $("#success_section").show();
        // $("#card_info").hide();

        if(data.Result==0){
            // alert("AF");
        check_ccwhitelist(data);

/*        	$("#card_number").text(data.ProtectedCardNumber);
        $("#name_on_card").text(data.NameonCard);
        $("#card_type").text(data.CardType);
        $("#tid").val(data.RecurringSaleTokenId);
        $("#success_section").show();
        $("#card_info").hide();*/
        } else {
        $("#tid").val('');
        swal(data.Message);	

        }
    },
    cancel: function(response){
        $("#tid").val('');
        $("#submit").prop('disabled', false);
        swal(response.Message);	
        if(response.Message!="Error validation Captcha"){
            swal(response.Message);	
            $("#success_section").hide();
            $("#card_info").show();
        } 

/*        	$("#success_section").hide();
        $("#card_info").show();*/
/*            if (isDefined(CallbackCancel) && CallbackCancel) window[CallbackCancel]("Error");
        else{

        }*/
    }	
});



$("#amount").change(function(){
    //alert("x");
/*		    var err_count = 0;
        var to_req=[];
        //jquery blank validation..
        $(".required").each(function(){
            var field_id = $(this).attr("id");
            var data=[];
            if($(this).val()==""){
              data['id']=field_id;      
              to_req.push(data);
              err_count++;
            }
        });

        if(err_count>0){
          swal(
          'Oops...',
          "Please do check required fields.",
          'error'
          );  
          $.each(to_req, function( index, value ) {
            $("#"+value.id).parent().after('<div class="hb"><div class="help-block" style="color:#f38989; font-size:7pt;margin-bottom:0px !important;"></div></div>').addClass('has-error');
          });        
          return false;
        }*/

    if($("#amount").val()<=0.00){
          swal("Amount is required.");

          return false;
    }

    $.ajax({
      url: '<?=site_url("payment/process_fee")?>',
      type: 'POST',
      dataType: 'json',
      // data: {amount: $("#amount").val()},
      data: {amount: parseFloat($("#amount").val().replace(/,/g, ''))},
         beforeSend:function(xhr, textStatus) {
        //called when complete
        loader.showPleaseWait();
        $("#submit").prop('disabled', true);
      },		  
      complete: function(xhr, textStatus) {
        //called when complete
      loader.hidePleaseWait();
      },
      success: function(data, textStatus, xhr) {
        if(data.success){
            //console.log(data.fee_data.fee);
            $(".amount_val").text($("#amount").val());
            $(".fee_val").text(data.fee_data.fee);
            $(".vat_val").text(data.fee_data.vat_charge);
            // var total = parseFloat($("#amount").val())+parseFloat(data.fee_data.fee)+parseFloat(data.fee_data.vat_charge);
            var total = parseFloat($("#amount").val().replace(/,/g, ''))+parseFloat(data.fee_data.fee)+parseFloat(data.fee_data.vat_charge)+parseFloat($(".conviniecefee_val").text());
            $(".amount_total").text(total.toFixed(2));
            $("#amount_total").val(total.toFixed(2));
            $("#hid_fee").val(data.fee_data.fee);
            $("#hid_vat").val(data.fee_data.vat_charge);
            getcfee($("#amount").val());
            @if($_SESSION['tag']=='MERCHANT')
            getcfee($("#amount").val());
            @endif
            @if($_SESSION['tag']=='CUSTOMER')
            getcfee_customer($("#amount").val());
            @endif
            $("#submit").prop('disabled', false);
        } else {
            swal(data.msg);
            $("#submit").prop('disabled', false);
        }
      },
      error: function(xhr, textStatus, errorThrown) {
        //called when there is an error
           swal(data.msg);
        $("#submit").prop('disabled', false);

      }
    });

});


//process_fee_customer


$("#another_card").click(function(){
    window.location.reload(); 

    // $("#success_section").hide();
    // $("#card_info").show();
    // $(".cancel_section").hide();
    // $(".payment_btn_section").show();        	
});

$("#whitelist_form").submit(function(e) {
    /* Act on the event */
    e.preventDefault();

    //reset validation
    $('.hb').remove();
    $('.required').removeClass('has-error-border').removeClass('has-success');

    //check validation
    var err_count = 0;
    var to_req=[];
    //jquery blank validation..
    $("#whitelist_form .required").each(function(){
        var field_id = $(this).attr("id");
        var data=[];
        if($(this).val()==""){
          data['id']=field_id;      
          to_req.push(data);
          err_count++;
        }
    });

    if(err_count>0){
      swal(
      'Oops...',
      "Please do check required fields.",
      'error'
      );  
      $.each(to_req, function( index, value ) {
        $("#"+value.id).addClass('has-error-border');//.after('<div class="hb"><div class="help-block" style="color:#f38989; font-size:7pt;margin-bottom:0px !important;">Required filed.</div></div>')
      });        
      return false;
    }    
    //
    var url = '{{base_url("payment/update_whitelist")}}';
    var formData = new FormData($("#whitelist_form")[0]);
    formData.append('card_id_upload', $('#card_id_upload')[0].files[0]); 
    formData.append('credit_card_upload', $('#credit_card_upload')[0].files[0]); 
    formData.append('cc_with_card_upload', $('#cc_with_card_upload')[0].files[0]); 
    var button = $("#submit_btn");

    //@global .js
    //P1 form obj P2 url form process to.
    ProcessForm(formData,url,button);
    $("#modal_validate_card").modal('hide');



    return false;
});
    //for cenpos process payment
$("#payment").click(function(e){

    //get total fees tf pf bfee vat
    var total_fees = parseFloat($("#hid_fee").val())+parseFloat($("#hid_vat").val())+parseFloat($("#hid_pfee").val())+parseFloat($("#hid_tfee").val());
    var form_data ={
    //merchant:merchant,
    notes:$("#notes").val(),
    reference_num:$("#reference_num").val(),	
    tokenid:$("#tid").val(),
    card_type:$("#card_type").text(),
    card_number:$("#card_number").text(),
    name_on_card:$("#name_on_card").text(),
    // notes:$("#notes").val(),
    // reference_num:$("#notes").val(),
    amount:$("#amount").val(),
    amount_val:$("#amount_total").val(),
    hid_fee:$("#hid_fee").val(),
    hid_vat:$("#hid_vat").val(),
    hid_pfee:$("#hid_pfee").val(),
    hid_tfee:$("#hid_tfee").val(),
    total_fee:total_fees,
    name:$("#name").val(),
    email:$("#email").val(),
    mobile:$("#mobile").val(),
    }  

    if($("#amount").val()<=0.00){
          swal("Amount is required.");

          return false;
    }

    //console.log(form_data); 
    $.ajax({
      url: '{{base_url("payment/process_card")}}',
      type: 'POST',
      dataType: 'json',
      data: form_data,
      //async: false,
      beforeSend:function(){
          loader.showPleaseWait();
        $("#payment").prop("disabled",true);
        $("#payment").text("processing... please wait");

      },
      complete: function(xhr, textStatus) {
          loader.hidePleaseWait();
        $("#payment").prop("disabled",false);
        $("#payment").text("Process Payment");

      },
      success: function(data, textStatus, xhr) {
        //console.log();
        if(data.success=='success'){
            $("#transaction_code").text(data.reference);
            $("#success_section_transaction").show();
            $("#card_section").hide();
        } else if(data.success=='3d'){
            //alert("x");
            handleResponse(data.data);
            //
/*            	$("#transaction_code").text(data.reference);
            $("#success_section_transaction").show();
            $("#card_section").hide();1*/
        } else {
            swal(data.msg);
              $(".cancel_section").show();
            $(".payment_btn_section").hide();
            $("#success_section_transaction").hide();
            $("#payment").prop("disabled",false);
            $("#payment").text("Process Payment");
            
        }
      },
      error: function(xhr, textStatus, errorThrown) {
   //          swal(data.msg);
   //        	$(".cancel_section").show();
            // $(".payment_btn_section").hide();
            // $("#success_section_transaction").hide();
            // $("#payment").prop("disabled",false);
   //      	$("#payment").text("Process Payment");
      }
    });


});


$('.btn-process').on('click', function() {
$(this).html('Submit');
$progressContentDone = $('.progress-content.active').data('content');
//alert($progressContentDone);
if($progressContentDone==1){
    if($("#card_id").val()=='' || $("#card_email").val()=='' || $("#card_mobile_number").val()=='' ){
        swal('Fill up all required field/s.');
        return false;
    }
}
if(!isEmail($("#card_email").val())){
        swal('Invalid Email format.');
        return false;		
}


if($progressContentDone==2){
    console.log('if');
    $('.btn-gray').css({"opacity": "1"});    	
    $('.hb').remove();
    $('.required').removeClass('has-error-border').removeClass('has-success');

    //check validation
    var err_count = 0;
    var to_req=[];
    //jquery blank validation..
    $("#whitelist_form .required").each(function(){
        var field_id = $(this).attr("id");
        var data=[];
        if($(this).val()==""){
          data['id']=field_id;      
          to_req.push(data);
          err_count++;
        }
    });

    if(err_count>0){
      swal(
      'Oops...',
      "Please do check required fields.",
      'error'
      );  
      $.each(to_req, function( index, value ) {
        $("#"+value.id).addClass('has-error-border');//.after('<div class="hb"><div class="help-block" style="color:#f38989; font-size:7pt;margin-bottom:0px !important;">Required filed.</div></div>')
      });        
      return false;
    }    
    //
    var form = new FormData();

    if($("#card_id_upload").find('[name=filepond]').val()=='' || $("#credit_card_upload").find('[name=filepond]').val()=='' || $("#cc_with_card_upload").find('[name=filepond]').val()==''){
      swal(
      'Opps...',
      "All photo is requires",
      'error'
      ); 
      return false;
    }



    var url = '{{base_url("payment/update_whitelist")}}';
    //var formData = new FormData($("#whitelist_form")[0]);
    var formData = $("#whitelist_form").serializeArray();
    var file1 = JSON.parse($("#card_id_upload").find('[name=filepond]').val());
    //formData.append('card_id_upload', file1.data);
    var file2 = JSON.parse($("#credit_card_upload").find('[name=filepond]').val());
    //formData.append('credit_card_upload', file2.data);
    var file3 = JSON.parse($("#cc_with_card_upload").find('[name=filepond]').val());
    //formData.append('cc_with_card_upload', file3.data);	

    formData.push({name: 'card_id_upload', value: file1.data});
    formData.push({name: 'credit_card_upload', value: file2.data});
    formData.push({name: 'cc_with_card_upload', value: file3.data});

    //for (var i = 0; i < $('.my-pond input').length; i++) {

    //}

/*		formData.append('card_id_upload', $('#card_id_upload')[0].files[0]); 
    formData.append('credit_card_upload', $('#credit_card_upload')[0].files[0]); 
    formData.append('cc_with_card_upload', $('#cc_with_card_upload')[0].files[0]); */
    var button = $("#submit_btn");

    $.ajax({
      url: url,
      type: 'POST',
      dataType: 'json',
      data: formData,
      //contentType: false,       
      //cache: false,             
      //processData:false,       
      beforeSend: function(xhr, textStatus) {
        //called when complete
        //clearValidationArray();
        loader.showPleaseWait();
        $(button).prop('disabled',true);        
      },          
      complete: function(xhr, textStatus) {
        //called when complete
        loader.hidePleaseWait();
        $(button).prop('disabled',false);        
      },
      success: function(data) {
        //called when successful
        if(data.success){
            // swal(
            // '',
            // data.msg,
            // 'success'
            // );  

        $('.progress-step[data-step="' + $progressContentDone + '"]').addClass('done').removeClass('active');
        $('.progress-step.done .number').html('<i class="fas fa-check"></i>');
        $('.progress-step[data-step="' + $progressContentDone + '"]').next().addClass('active');

        $('.progress-content[data-content="'+$progressContentDone+'"]').addClass('done').removeClass('active');
        $('.progress-content[data-content="'+$progressContentDone+'"]').next().addClass('active');

            //hideModal("large");
          $('#cardValidation .btn-container').remove();
           $('.progress-content[data-content="3"]').addClass('done').removeClass('active');
           $('.progress-step[data-step="3"]').addClass('done').removeClass('active');
        $('.progress-step.done .number').html('<i class="fas fa-check"></i>');
        $('.progress-content[data-content="3"]').addClass('done');

        } else {
            swal(
            'Opps...',
            data.msg,
            'error'
            );  
        }     
      },
      error: function(xhr, textStatus, errorThrown) {
        swal('something went wrong.');
      }
    });

}
//return false;
if($progressContentDone!=2){
$('.progress-step[data-step="' + $progressContentDone + '"]').addClass('done').removeClass('active');
$('.progress-step.done .number').html('<i class="fas fa-check"></i>');
$('.progress-step[data-step="' + $progressContentDone + '"]').next().addClass('active');

$('.progress-content[data-content="'+$progressContentDone+'"]').addClass('done').removeClass('active');
$('.progress-content[data-content="'+$progressContentDone+'"]').next().addClass('active');
}

});

// $(window).resize(function(){
//   progressHeight();
// });

// setTimeout(function() {
 //  progressHeight();
// }, 2000);

// function progressHeight() {
//   $progressContent = $('.progress-content.active').height();
//   $('.progress-container').css({
//     "height": $progressContent
//   });
// }

$('.my-pond').filepond();
$.fn.filepond.registerPlugin(FilePondPluginImagePreview);
$.fn.filepond.registerPlugin(FilePondPluginFileValidateType);
$.fn.filepond.registerPlugin(FilePondPluginFileEncode);
// Turn input element into a pond with configuration options
$('.my-pond').filepond({
  allowMultiple: true,
  // labelIdle: '<button type="button" class="btn btn-upload"><i class="fas fa-upload"></i> Upload</button>',
  acceptedFileTypes: [
    'image/jpg',
    'image/jpeg',
    'image/png',
    ]
});

// Set allowMultiple property to true
$('.my-pond').filepond('allowMultiple', false);

// Listen for addfile event
$('.my-pond').on('FilePond:addfile', function(e) {
  console.log('file added event', e);
});

$("#okay_btn").click(function(){
 window.location.reload();
});

