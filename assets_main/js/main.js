$(document).ready(function () {
	$(".menu-button").on("click",function() {
		$(".main-menu").toggleClass("open");
		$(this).toggleClass("open");
	});

	$( ".card-header" ).each(function( index ) {
	  $(this).attr("href", "#collapse" + index);
	  $(this).next(".card-body").attr("id", "collapse" + index);
	});
});