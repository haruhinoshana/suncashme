-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.9-MariaDB-log - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5174
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for ticketing_app
CREATE DATABASE IF NOT EXISTS `base_temp` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `base_temp`;

-- Dumping structure for table ticketing_app.activity_log
CREATE TABLE IF NOT EXISTS `activity_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `action` text,
  `details` text,
  `user` varchar(255) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `terminal` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='record all events do by the user.';

-- Dumping data for table ticketing_app.activity_log: ~0 rows (approximately)
/*!40000 ALTER TABLE `activity_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `activity_log` ENABLE KEYS */;

-- Dumping structure for table ticketing_app.main_menu
CREATE TABLE IF NOT EXISTS `main_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(255) DEFAULT NULL,
  `menu_route` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `icon` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='navbar main menu';

-- Dumping data for table ticketing_app.main_menu: ~0 rows (approximately)
/*!40000 ALTER TABLE `main_menu` DISABLE KEYS */;
/*!40000 ALTER TABLE `main_menu` ENABLE KEYS */;

-- Dumping structure for table ticketing_app.sub_menu
CREATE TABLE IF NOT EXISTS `sub_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `main_id` int(11) DEFAULT NULL,
  `menu_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `menu_route` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `level` bigint(20) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='sub menu.';

-- Dumping data for table ticketing_app.sub_menu: ~0 rows (approximately)
/*!40000 ALTER TABLE `sub_menu` DISABLE KEYS */;
/*!40000 ALTER TABLE `sub_menu` ENABLE KEYS */;

-- Dumping structure for table ticketing_app.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_role` enum('admin_user','program_user','merchant_user','branch_user') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'admin_user',
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `full_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `middle_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` enum('1','0') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1' COMMENT '1=active 0=inactive',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table ticketing_app.users: ~0 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table ticketing_app.value_sets_global
CREATE TABLE IF NOT EXISTS `value_sets_global` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `set_type` varchar(100) DEFAULT NULL COMMENT 'type of values',
  `set_name` varchar(100) NOT NULL COMMENT 'list of value',
  `description` text,
  `attribute1` varchar(100) DEFAULT NULL,
  `attribute2` varchar(100) DEFAULT NULL,
  `attribute3` varchar(100) DEFAULT NULL,
  `attribute4` varchar(100) DEFAULT NULL,
  `attribute5` varchar(100) DEFAULT NULL,
  `attribute6` varchar(100) DEFAULT NULL,
  `attribute7` varchar(100) DEFAULT NULL,
  `attribute8` varchar(100) DEFAULT NULL,
  `attribute9` varchar(100) DEFAULT NULL,
  `attribute10` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='table holds global values needed on a program.';

-- Dumping data for table ticketing_app.value_sets_global: ~0 rows (approximately)
/*!40000 ALTER TABLE `value_sets_global` DISABLE KEYS */;
/*!40000 ALTER TABLE `value_sets_global` ENABLE KEYS */;

-- Dumping structure for table ticketing_app.value_sets_lookup
CREATE TABLE IF NOT EXISTS `value_sets_lookup` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `set_type` varchar(100) DEFAULT NULL COMMENT 'type of values',
  `code` varchar(100) DEFAULT NULL,
  `set_name` varchar(100) NOT NULL COMMENT 'list of value',
  `description` text,
  `attribute1` varchar(100) DEFAULT NULL,
  `attribute2` varchar(100) DEFAULT NULL,
  `attribute3` varchar(100) DEFAULT NULL,
  `attribute4` varchar(100) DEFAULT NULL,
  `attribute5` varchar(100) DEFAULT NULL,
  `attribute6` varchar(100) DEFAULT NULL,
  `attribute7` varchar(100) DEFAULT NULL,
  `attribute8` varchar(100) DEFAULT NULL,
  `attribute9` varchar(100) DEFAULT NULL,
  `attribute10` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COMMENT='table holds general list needed on a program.';

-- Dumping data for table ticketing_app.value_sets_lookup: ~0 rows (approximately)
/*!40000 ALTER TABLE `value_sets_lookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `value_sets_lookup` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
